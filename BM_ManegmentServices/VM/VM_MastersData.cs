﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VM_MastersData
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Role { get; set; }
        public List<string> pageName { get; set; }
        public long CustomerId { get; set; }
    }
}