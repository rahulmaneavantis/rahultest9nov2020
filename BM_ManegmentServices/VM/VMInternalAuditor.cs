﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMInternalAuditor
    {
        public long Id { get; set; }
        public long AuditorMappingId { get; set; }
        public int CustomerId { get; set; }
        public int EntityId { get; set; }
        public string Isemployee { get; set; }
        [Required (ErrorMessage = "Please select Internal Auditor Category")]
         public int InternalAuditor_CategoryId { get; set; }
        [Required(ErrorMessage = "Please select Internal Auditor")]
        public int Auditor_Id { get; set; }
        public string InternalAuditor_Category { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please Enter Date of Appointment")]
        public DateTime? Date_of_appointment { get; set; }
        public string EmployeeNo { get; set; }
        public int Number_OF_InternalAuditor { get; set; }
        public string AuditorName { get; set; }
        public string AddressOfAuditor { get; set; }
        public string AuditorAppointedForFY { get; set; }
        public bool errorMessage { get; set; }
        public bool successMessage { get; set; }
        public string successerrorMessage { get; set; }
        public int UserId { get; set; }

        public bool IsMappingActive { get; set; }

        //Used in Template field
        public string RegistrationNo { get; set; }
        public string DateOfAppointment { get; set; }
        public long AgendaID { get; set; }
        public long MeetingAgengaMappingID { get; set; }
    }

}