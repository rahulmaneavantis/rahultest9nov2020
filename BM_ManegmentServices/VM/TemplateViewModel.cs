﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BM_ManegmentServices.VM
{
    public class TemplateViewModel
    {
        [ScaffoldColumn(false)]
        public long TemplateID
        {
            get;
            set;
        }

        [Required]
        [DisplayName("Template Name")]
        [MaxLength(50,ErrorMessage = "Template name can have a max of {1} characters")]
        //[RegularExpression(@"^\S*$", ErrorMessage = "Name Cannot Have Spaces")] // Don't Remove/Comment Regular Expression (@"^\S*$") for Avoid Blank Spaces 
        [RegularExpression(@"(^\S*$)", ErrorMessage = "Name can not have spaces")]
        public string TemplateName
        {
            get;
            set;
        }
        [Required]
        [DisplayName("Template Label")]
        [MaxLength(100, ErrorMessage = "Template label can have a max of {1} characters")]
        public string TemplateLabel
        {
            get;
            set;
        }

        [UIHint("CategoryType")]
        public CategoryViewModel Category
        {
            get;
            set;
        }

        public int? CategoryID { get; set; }

        public long? AgendaID { get; set; }

        public int UserId { get; set; }

        public bool IsTemplateField { get; set; }
        public bool IsUIFormColumn { get; set; }
    }
    public class CategoryViewModel
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDetails { get; set; }
    }

    public class ImportTemplateViewModel
    {
        public long TemplateID
        {
            get;
            set;
        }
        public string TemplateName
        {
            get;
            set;
        }
        public string TemplateLabel
        {
            get;
            set;
        }
        public int? CategoryID { get; set; }
        public string CategoryName { get; set; }

        public bool? IsSelected { get; set; }

    }
}