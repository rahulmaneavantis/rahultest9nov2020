﻿namespace BM_ManegmentServices.VM
{
    public class DocumentViewer
    {
        public string Document { get; set; }
        public int Height { get; set; }
        public bool Resizable { get; set; }
        public int Width { get; set; }
    }
}