﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class CustomerVM
    {
        public int Cust_Id { get; set; }
        [Required(ErrorMessage = "Please Enter CustomerName.")]
        public string CustomerName { get; set; }
        [Required (ErrorMessage ="Please Enter BuyerName.")]
        public string BuyerName { get; set; }
        [Required (ErrorMessage = "Please Enter BuyerDesignation")]
        public string BuyerDesignation { get; set; }
        [Required (ErrorMessage ="Please Enter Mobile Number.")]
        public long Mobile { get; set; }
        [Required (ErrorMessage ="Please Enter EmailId")]
        public string EmailId { get; set; }

    }

    public class Customer_VM
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
    }

    public class ServiceProviderLimit_VM
    {
        public int customerID { get; set; }
        public int totalCustomerCreated { get; set; }
        public int totalEntityCreated { get; set; }

        public int totalCustomerLimit { get; set; }
        public int totalEntityLimit { get; set; }
    }
}