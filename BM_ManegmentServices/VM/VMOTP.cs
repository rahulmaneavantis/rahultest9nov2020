﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMOTP:IMessage
    {
        public string EmilId { get; set; }
        public long MeetingId { get; set; }
        public long otp { get; set; }
    }
}