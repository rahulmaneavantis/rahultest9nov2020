﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMPublicListedSherholders
    {
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        [DisplayName("Allowtment Date")]
        [Required (ErrorMessage ="Please Select Allowtment Date")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string Allowtment_Date { get; set; }
        [DisplayName("Upload File")]
        public string FilePath { get; set; }
        [Required]
        public HttpPostedFileBase File { get; set; }
    }
}