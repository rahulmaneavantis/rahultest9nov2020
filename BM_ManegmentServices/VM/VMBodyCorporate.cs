﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMBodyCorporate
    {
        public long Id { get; set; }
        public string EntityName { get; set; }
        [Required (ErrorMessage ="Please Enter Security Name")]
        public string Security_Name { get; set; }
        [Required(ErrorMessage = "Please Enter Security Number")]
        public long Securities_No { get; set; }
        [Required(ErrorMessage = "Please Enter Security  value")]
        public string Securities_Nomval { get; set; }
        public long TotValSecurities { get; set; }
        [Required(ErrorMessage = "Please Enter Currency")]
        public string Currency { get; set; }
        public bool IslistedOnStockExchange { get; set; }
        public List<int> StockExchange { get; set; }
        public int EntityId { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string SuccessErrorMsg { get; set; }
    }
}