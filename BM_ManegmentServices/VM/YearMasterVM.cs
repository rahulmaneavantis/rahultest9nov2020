﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class YearMasterVM
    {
        public long FYID { get; set; }
        public string Type { get; set; }
        public string FYText { get; set; }
    }
}