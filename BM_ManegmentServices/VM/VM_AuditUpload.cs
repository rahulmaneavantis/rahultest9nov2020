﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VM_AuditUpload
    {
        public string FilePath { get; set; }
        [Required]
        public HttpPostedFileBase File { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string successErrorMessage { get; set; }
    }
}