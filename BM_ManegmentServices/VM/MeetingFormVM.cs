﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class FormsVM
    {
        public long MeetingFormMappingId { get; set; }
        public long MeetingId { get; set; }
        public long MeetingAgendaMappingId { get; set; }
        public long ComplianceId { get; set; }

        public long FormId { get; set; }
        public string FormName { get; set; }
        public string EForm { get; set; }

        public bool IsEForm { get; set; }
        public bool CanGenerate { get; set; }
        public bool ForMultipleEvents { get; set; }
        public int EventCount { get; set; }
        public string EventMessage { get; set; }
    }

    public class MeetingFormsVM
    {
        public long MeetingId { get; set; }
        public long MeetingAgendaMappingId { get; set; }
        public long ComplianceId { get; set; }
    }

    public class EFormVM : IMessage
    {
        public long MeetingFormMappingId { get; set; }
        public string FormName { get; set; }
        public byte[] FormData { get; set; }

        public string FullFileName { get; set; }
        public long DirectorID { get; set; }
    }

    public class EFormFieldVM
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class MultipleFormsVM
    {
        public long MeetingFormMappingId { get; set; }
        public long MeetingId { get; set; }
        public long MeetingAgendaMappingId { get; set; }
        public long? RefMasterID { get; set; }
        public long? UIFormID { get; set; }
    }
}