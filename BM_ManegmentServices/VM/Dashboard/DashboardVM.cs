﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM.Dashboard
{
    public class DashboardVM
    {
    }

    public class Dashboard_MeetingVM
    {
        public List<MeetingListVM> Todays { get; set; }
        public List<MeetingListVM> Upcoming { get; set; }
        public List<MeetingListVM> Past { get; set; }
        public List<MeetingVM> Temp { get; set; }

       
    }

    public class MeetingListVM
    {
        public long MeetingID { get; set; }
        public string Stage { get; set; }
        public string EntityName { get; set; }
        public int? MeetingSrNo { get; set; }
        public int MeetingTypeId { get; set; }
        public string MeetingTypeName { get; set; }
        public string Quarter_ { get; set; }
        public bool IsShorter { get; set; }
        public long EntityId { get; set; }
        public string MeetingTitle { get; set; }

        public DateTime? MeetingstartDate { get; set; }

        public DateTime? MeetingDate { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string DayName { get; set; }
        public string YearName { get; set; }
        public string MeetingTime { get; set; }
        public string MeetingVenue { get; set; }
        public string ReasonforRSPV { get; set; }
        public string RSPV { get; set; }
        public string FY { get; set; }
        public int? srno { get; set; }
        public string sirialnumber { get; set; }
       public long participantId { get; set; }
        //for not permanet we have to change 27th of May 2020
        public long AgendaID { get; set; }
        public long MappingID { get; set; }
        public long? DraftCirculationID { get; set; }
        public bool? IsCirculate { get; set; }
        public bool? IsFinalized { get; set; }
        public bool? IsMeetingStarted { get; set; }
        public bool CanStartMeeting { get; set; }
        public bool IsConcluded { get; set; }
        public string Attendance { get; set; }
        public string Present { get; set; }
        public string StartMeetingTime { get; set; }
        public string EndMeetingTime { get; set; }
    }

    public class DraftMeetingListVM : MeetingListVM
    {
        public long DraftCirculationID { get; set; }
        public long MinutesDetailsID { get; set; }

        public bool? IsDelivered { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsApproved { get; set; }
        public DateTime? DateOfDraftCirculation { get; set; }
        public DateTime? DueDateOfDraftCirculation { get; set; }
    }

    public class Dashboard_CompliancesCountVM
    {
        public PerformerCountVM Performer { get; set; }
        public ReviewerCountVM Reviewer { get; set; }
    }

    public partial class Dashboard_ComplianceCount_ResultVM
    {
        public string Name { get; set; }
        public int RoleID { get; set; }
        public string MappingType { get; set; }
    }
    public class PerformerCountVM
    {
        public int OverDue_Statutory { get; set; }
        public int OverDue_Meeting { get; set; }
        public int OverDue_Director { get; set; }

        public int Upcoming_Statutory { get; set; }
        public int Upcoming_Meeting { get; set; }
        public int Upcoming_Director { get; set; }

        public int Rejected_Statutory { get; set; }
        public int Rejected_Meeting { get; set; }
        public int Rejected_Director { get; set; }

        public int PendingForReview_Statutory { get; set; }
        public int PendingForReview_Meeting { get; set; }
        public int PendingForReview_Director { get; set; }
    }

    public class ReviewerCountVM
    {
        public int DueButNotSubmitted_Statutory { get; set; }
        public int DueButNotSubmitted_Meeting { get; set; }
        public int DueButNotSubmitted_Director { get; set; }

        public int Rejected_Statutory { get; set; }
        public int Rejected_Meeting { get; set; }
        public int Rejected_Director { get; set; }

        public int PendingForReview_Statutory { get; set; }
        public int PendingForReview_Meeting { get; set; }
        public int PendingForReview_Director { get; set; }
    }
}