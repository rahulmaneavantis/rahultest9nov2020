﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class AdditionalCompliances_VM:IMessage
    {
        public long Id { get; set; }
        public string SEBI { get; set; }
        public string NonCompliance { get; set; }
        //[RegularExpression(@"^\$?\d+(\.\d{2})?$", ErrorMessage = "Please Enter Compliance Number")]
        public string ComplianceNumber { get; set; }
        public int ComplianceId { get; set; }
        public string Type { get; set; }
    }
    public class ComplianceList_VM
    {
        public long? ComplianceId { get; set; }
        public long AdditionalCompId { get; set; }
        public string ShortDesc { get; set; }
        public string Type { get; set; }
    }

}