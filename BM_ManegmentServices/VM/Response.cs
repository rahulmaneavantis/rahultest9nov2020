﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class Response
    {
        public bool Success { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
    }

    public class IMessage
    {
        public bool Success { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
    }
    public class Message : IMessage
    {

    }
}