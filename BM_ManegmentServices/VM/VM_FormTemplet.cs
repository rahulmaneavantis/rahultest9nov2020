﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BM_ManegmentServices.VM
{
    public class VM_FormTemplet
    {
        public long FormId { get; set; }
        public string FormName { get; set; }
        public string Details { get; set; }
        public string FormFormate { get; set; }
        public long MeetingId { get; set; }
        public long? MappingAgendaId { get; set; }
        public DateTime? StartDate { get; set; }
        public bool IsEForm { get; set; }
        public bool IsReadonly { get; set; }
        public long ComplianceId { get; set; }

        public FormTemplateDetails FormTemplatedetail { get; set; }
    }

    public class FormTemplateDetails:IMessage
    {
        public long FormId { get; set; }
        public bool IsEForm { get; set; }
        public string EForm { get; set; }
        [Required (ErrorMessage ="Please Enter Form Name" )]
        public string FormName { get; set; }
        [AllowHtml]
        public string FormFormat { get; set; }
        public string Details { get; set; }
        public int UserId { get; set;}
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage ="Select Start Date")]
        public DateTime? StartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }
    }

    public class MasterDetails
    {
        public long ID { get; set; }
        public string MasterName { get; set; }
    }

    public class MasterFormates
    {
        public long ID { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
    }

}