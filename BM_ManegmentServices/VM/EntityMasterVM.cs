﻿using BM_ManegmentServices.VM.Attributes;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class EntityMasterVM
    {
        public int Id { get; set; }
        public int Entity_Type { get; set; }
        public string RocCode { get; set; }
        public string EntityName { get; set; }
        public string Type { get; set; }
        public string LLPIN { get; set; }
        public string LLPI_CIN { get; set; }
        public string RegistrationNO { get; set; }
        public string CIN { get; set; }
        public string PAN { get; set; }
        public bool Islisted { get; set; }
        public bool? isDemate { get; set; }
        public bool? IsPartialorComplited { get; set; }
        public int? CustomerId { get; set; }
        public string CompanyCategory { get; set; }
        public string CompanysubCategory { get; set; }
        public string classofCompany { get; set; }
        public string RegisterAddress { get; set; }
        public string emailId { get; set; }
        public DateTime dateofIncorporaion { get; set; }
        public Pravate_PublicVM Pravate_PublicVM { get; set; }
        public LLPVM LLPVM { get; set; }
        public TrustVM TrustVM { get; set; }
        public BodyCorporateVM BodyCorporateVM { get; set; }
        public bool CanAdd  { get; set; }
        public bool CanEdit { get; set; }
public bool? DefaultVirtualMeeting { get; set; }
    }

    #region Public Private ViewModal
    public class Pravate_PublicVM
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Company Name")]
      
        [RegularExpression(@"^[a-zA-Z0-9\s-_]*$", ErrorMessage = "Please Enter valid Entity Name")]
        public string EntityName { get; set; }
        public int Entity_Type { get; set; }
        public string Type { get; set; }
        [Required(ErrorMessage = "Please Enter CIN")]
        [RegularExpression(@"[U,L]{1}\d{5}[A-Z]{2}\d{4}[PTC,PLC,SGC,OPC,NPL,FTC,GOI]{3}\d{6}", ErrorMessage = "Please Enter valid CIN")]
        public string CIN { get; set; }
        [RegularExpression(@"[a-zA-Z0-9]{13}", ErrorMessage = "Please Enter Valid GLN")]
        public string GLN { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [RegularExpression(@"^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$", ErrorMessage = "Please Enter Correct Date")]
        public string IncorporationDate { get; set; }
        [Required(ErrorMessage = "Select ROC")]
        public int ROC_Code { get; set; }
        public string Registration_No { get; set; }
        [Required(ErrorMessage = "Please Enter Address Line 1")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter valid Address Line 1")]
        public string Regi_Address_Line1 { get; set; }
        [Required(ErrorMessage = "Please Enter Address Line 2")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter valid Address Line 2")]
        public string Regi_Address_Line2 { get; set; }

        [Required(ErrorMessage = "Please Select State")]
        public int Regi_StateId { get; set; }
        [Required(ErrorMessage = "Please Select City")]
        public int Regi_CityId { get; set; }
        [RegularExpression(@"[0-9]{6}", ErrorMessage = "Please Enter valid PIN")]
        public string Regi_PINCode { get; set; }
        public bool IsCorp_Office { get; set; }
        public bool Is_SameAddress { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter valid Address")]
        public string Corp_Address_Line1 { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter valid Address")]
        public string Corp_Address_Line2 { get; set; }
        public int? Corp_StateId { get; set; }
        public int? Corp_CityId { get; set; }
        [RegularExpression(@"[0-9]{6}", ErrorMessage = "Please Enter valid PIN")]
        public string Corp_PINCode { get; set; }
        [Required(ErrorMessage = "Select Company Category")]
        public int CompanyCategory_Id { get; set; }

        public int? CompanyCategorySub_Id { get; set; }
        public bool IS_Listed { get; set; }
        public bool Is_SubCompany { get; set; }
        [RegularExpression(@"[A-Z]{3}\d{3}[A-Z]{1}\d{5}", ErrorMessage = "Please Enter valid ISIN")]
        public string ISIN { get; set; }
        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please Enter Valid Email")]
        public string Email_Id { get; set; }
        [RegularExpression(@"[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]", ErrorMessage = "Please Enter Valid Website")]
        public string WebSite { get; set; }
        [DisplayName("PAN Number")]
        [Required(ErrorMessage = "Please Enter PAN No.")]

        [RegularExpression(@"[A-Z]{3}[C]{1}[A-Z]{1}\d{4}[A-Z]{1}", ErrorMessage = "Please Enter valid PAN")]
        public string PAN { get; set; }
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Please Enter valid NIC Code")]
     
        public long? NICCode { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9\s-_]*$", ErrorMessage = "Please Enter valid Description")]
      
        public string Description { get; set; }
        [RegularExpression(@"\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d{1}[A-Z]{1}[A-Z0-9]{1}", ErrorMessage = "Please Enter valid GSTN")]
        public string GST { get; set; }
        //public int[] stockexc { get; set; }
        [Display(Name = "Stock Exchange", ResourceType = typeof(JSError))]
        public List<int> stockexc { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string successErrorMessage { get; set; }
        [Required(ErrorMessage = "Please Enter FY")]
        public string FY { get; set; }
        public ListofstockExchange StockExchangeDetails { get; set; }
        public int CustomerBranchId { get; set; }
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please Enter Valid RTA Email")]
        public string RTA_EmailId { get; set; }
        [RegularExpression(@"[U,L]{1}\d{5}[A-Z]{2}\d{4}[PTC,PLC,SGC,OPC,NPL,FTC,GOI]{3}\d{6}", ErrorMessage = "Please Enter Valid RTA CIN")]
        public string RTA_CIN { get; set; }
       
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter valid RTA Address")]
        public string RTA_Address { get; set; }
      
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter valid RTA Name")]
        public string RTA_Name { get; set; }
        public string SEBI_RegNo { get; set; }
        [RegularExpression(@"\d{10}", ErrorMessage = "Please enter valid Mobile Number")]
        public string PhoneNo { get; set; }

    }
    #endregion

    public class SubCompanyType
    {
        public long SubcompanyId { get; set; }
        public long EntityID { get; set; }
        public int CustomerId { get; set; }
     
        [RegularExpression(@"^[a-zA-Z\s]*$", ErrorMessage = "Please Enter valid Entity Name")]
        [Required(ErrorMessage = "Please Enter Entity Name")]
        public string SubCompanyName { get; set; }
        public string ParentCIN { get; set; }
        [Required(ErrorMessage = "Please Enter CIN")]
        public string CIN { get; set; }
        [Required(ErrorMessage = "Please select Association Type")]
        public int subcompanyTypeID { get; set; }
        public string subcompanyTypeName { get; set; }
        [Range(0, 100, ErrorMessage = "Value for Shares Held must be between {1} and {2}")]
        public decimal? PersentageofShareholding { get; set; }
        [UIHint("_SubEntityType")]
        public AssosiateType AssociateCompany { get; set; }
    }

    public class AssosiateType
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
    public class ListofstockExchange
    {
        public long Id { get; set; }
        public int EntityId { get; set; }
        [Required (ErrorMessage = "Please Select Stock Exchange")]
        public int staockExchange { get; set; }
        public string stockExchangeName { get; set; }
        public string scriptCode { get; set; }
        public string scriptSymbol { get; set; }
    }

    #region LLP Master ViewModel
    public class LLPVM
    {
        public int Id { get; set; }
        public int Entity_Type { get; set; }
        [Required(ErrorMessage = "Please Enter Company Name")]
       
        [RegularExpression(@"^[a-zA-Z0-9\s-_]*$", ErrorMessage = "Please Enter valid Entity Name")]
        public string EntityName { get; set; }
        [Required(ErrorMessage = "Please Enter LLPIN")]
        [RegularExpression(@"[A-Z]{3}[-]{1}\d{4}", ErrorMessage = "Please Enter valid LLPIN")]
        public string LLPIN { get; set; }
        [RegularExpression(@"[a-zA-Z0-9]{13}", ErrorMessage = "Please Enter valid GLN")]
        public string llpGLN { get; set; }
        [Required(ErrorMessage = "Please Select Date of Incorporation")]
        [RegularExpression(@"^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$", ErrorMessage = "Please Enter Correct Date")]
        public string llpIncorporationDate { get; set; }
        [RegularExpression(@"\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d{1}[A-Z]{1}\d{1}", ErrorMessage = "Please Enter Valid GSTN")]
        public string llpGST { get; set; }
        [Required(ErrorMessage = "Please Select ROC")]
        public int llpROC_Code { get; set; }
       
        [RegularExpression(@"^[a-zA-Z0-9_]*$", ErrorMessage = "Please Enter valid Registration Number")]
        public string llpRegistration_No { get; set; }

        [Required(ErrorMessage = "Please Enter Address Line 1")]
        public string llpRegi_Address_Line1 { get; set; }
        [Required(ErrorMessage = "Please Enter Address Line 2")]
        public string llpRegi_Address_Line2 { get; set; }

        [Required(ErrorMessage = "Please Select State")]
        public int llpRegi_StateId { get; set; }
        [Required(ErrorMessage = "Please Select City")]
        public int llpRegi_CityId { get; set; }
        [RegularExpression(@"[0-9]{6}", ErrorMessage = "Please Enter Valid PIN")]
        public string llpRegi_PINCode { get; set; }
        public string llpIsCorp_Office { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter Valid Address")]
        public string llpCorp_Address_Line1 { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter Valid Address")]
        public string llpCorp_Address_Line2 { get; set; }
        public int? llpCorp_StateId { get; set; }
        public int? llpCorp_CityId { get; set; }
        [RegularExpression(@"[0-9]{6}", ErrorMessage = "Please Enter Valid PIN")]
        public string llpCorp_PINCode { get; set; }
        public int? llpNo_Of_Partners { get; set; }
        public int? llpNo_Of_DesignatedPartners { get; set; }
        public decimal? llpObligation_Of_Contribution { get; set; }
        [Required(ErrorMessage = "Please Enter Email-Id")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please Enter Valid Email")]
        public string llpEmail_Id { get; set; }
        public int? llpbusinessActivityID { get; set; }
        public bool llpIscorporateforllp { get; set; }
        public int? llpbusinessADesc { get; set; }
        [RegularExpression(@"[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]", ErrorMessage = "Please Enter Valid Website")]
        public string llpWebSite { get; set; }
        public bool Is_SameAddressllp { get; set; }
        [DisplayName("PAN Number")]
        [Required(ErrorMessage = "Please Enter PAN No.")]
        [RegularExpression(@"[A-Z]{3}[E]{1}[A-Z]{1}\d{4}[A-Z]{1}", ErrorMessage = "Please Enter Valid PAN")]
        public string llpPAN { get; set; }

        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string successErrorMessage { get; set; }
        [Required(ErrorMessage = "Please Enter FY")]
        public string llpFY { get; set; }
        public int CustomerBranchId { get; set; }
    }
    #endregion

    #region Trust Master ViewModal
    public class TrustVM
    {
        public int Id { get; set; }
        public int Entity_Type { get; set; }
        [Required(ErrorMessage = "Please Enter Trust Name")]
      
        [RegularExpression(@"^[a-zA-Z0-9\s-_]*$", ErrorMessage = "Please Enter Valid Trust Name")]
        public string trustEntityName { get; set; }
        public int trustEntity_Type { get; set; }
        [Required(ErrorMessage = "Please Enter Trust Type")]
        public int trusttType { get; set; }
        public int trustNumber { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9_]*$", ErrorMessage = "Please Enter Valid Registration Number")]
       
        public string trustRegistration_No { get; set; }
        [Required(ErrorMessage = "Please Enter Address Line 1")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter Valid Address")]
        public string trustRegi_Address_Line1 { get; set; }
        [Required(ErrorMessage = "Please Enter Address Line 2")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter Valid Address")]
        public string trustRegi_Address_Line2 { get; set; }

        [Required(ErrorMessage = "Please Select State")]
        public int trustRegi_StateId { get; set; }
        [Required(ErrorMessage = "Please Select City")]
        public int trustRegi_CityId { get; set; }

        [RegularExpression(@"[0-9]{6}", ErrorMessage = "Please Enter valid PIN")]
        public string trustRegi_PINCode { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [RegularExpression(@"^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$", ErrorMessage = "Please Enter Correct Date")]
        public string trustRegistrationDate { get; set; }
        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please Enter Valid Email")]
        public string trustEmail_Id { get; set; }
        [RegularExpression(@"[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]", ErrorMessage = "Please Enter Valid Website")]
        public string trustWebSite { get; set; }
        [DisplayName("PAN")]
        [Required(ErrorMessage = "Please Enter PAN")]
        [RegularExpression(@"[A-Z]{3}[T]{1}[A-Z]{1}\d{4}[A-Z]{1}", ErrorMessage = "Please Enter valid PAN")]
        public string trustPAN { get; set; }

        public long trustNICCode { get; set; }
        public string trustDescription { get; set; }
        [RegularExpression(@"\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d{1}[A-Z]{1}\d{1}", ErrorMessage = "Please Enter Valid GSTN")]
        public string trustGST { get; set; }
        public int? trustNo_Of_Trustees { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string successErrorMessage { get; set; }
        [Required(ErrorMessage = "Please Enter FY")]
        public string TrustFY { get; set; }

        public int CustomerBranchId { get; set; }
    }
    #endregion

    #region BodyCorporate ViewModal
    public class BodyCorporateVM
    {
        public int Id { get; set; }
        public int Entity_Type { get; set; }
        [Required(ErrorMessage = "Please Enter Body Corporate Name")]
      
        [RegularExpression(@"^[a-zA-Z0-9\s-_]*$", ErrorMessage = "Please Enter valid Body Corporate Name")]
        public string BodyCorporateEntityName { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [RegularExpression(@"^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$", ErrorMessage = "Please Enter correct Date")]
        public string BodyCorporateDateofIncorporate { get; set; }
        [Required(ErrorMessage = "Please Enter Registration Number")]
        [RegularExpression(@"^[a-zA-Z0-9_]*$", ErrorMessage = "Please Enter Valid Registration Number")]
       
        public string BodyCorporateRegistration_No { get; set; }
        [Required(ErrorMessage = "Please Enter Address Line 1")]
       
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter Valid Address line 1")]
        public string BodyCorporateRegi_Address_Line1 { get; set; }
        [Required(ErrorMessage = "Please Enter Address Line 2")]
        
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter Valid Address line 2")]
        public string BodyCorporateRegi_Address_Line2 { get; set; }
        [RegularExpression(@"[a-zA-Z0-9]{13}", ErrorMessage = "Please Enter Valid GLN")]
        public string BodyCorporateGLN { get; set; }
        [Required(ErrorMessage = "Please Select State")]
        public int BodyCorporateRegi_CountryID { get; set; }
        [Required(ErrorMessage = "Please Select City")]
        public int BodyCorporateRegi_CityId { get; set; }
        [RegularExpression(@"[0-9]{6}", ErrorMessage = "Please Enter Valid PIN")]
        public string BodyCorporateRegi_PINCode { get; set; }
        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please Enter Valid Email")]
        public string BodyCorporateEmail_Id { get; set; }
        [RegularExpression(@"[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]", ErrorMessage = "Please Enter valid Website")]
        public string BodyCorporateWebSite { get; set; }
        [Required(ErrorMessage = "Please Enter Regulatory Jurisdiction")]
        public string BodyCorporatRegulatoryJurisdiction { get; set; }
        [Required(ErrorMessage = "Please Enter TIN No.")]
        [RegularExpression(@"[0-9]{11}", ErrorMessage = "Please Enter Valid TIN")]
        public string TIN { get; set; }
        public int? GoverningBody { get; set; }
        public int? Gov_Body_Members { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string successErrorMessage { get; set; }
        [Required(ErrorMessage = "Please Enter FY")]
        public string BCFY { get; set; }

        public int CustomerBranchId { get; set; }
    }
    #endregion

    public class EntityTypeVM
    {
        public int EntityTypeId { get; set; }
        public string EntityTypeName { get; set; }
    }

    public class EntityAddressVM
    {
        public int EntityId { get; set; }
        public string EntityAddressType { get; set; }
        public string EntityAddressTypeName { get; set; }
        public string EntityAddress { get; set; }

    }
    public class VM_EntityTypeSetting
    {
        public int CustID { get; set; }
        public int EntityTypeID { get; set; }        
        public int Value { get; set; }
        public int UserID { get; set; }
    }

    public class ParentEntitySelectVM
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public bool IsCheked { get; set; }
    }
}
