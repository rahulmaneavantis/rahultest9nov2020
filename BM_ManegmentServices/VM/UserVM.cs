﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class UserVM
    {
        public long UserID { get; set; }
        public int CustomerID { get; set; }
        [Required(ErrorMessage = "Please enter first name")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_]*$", ErrorMessage = "Please Enter valid First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please enter first last name")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_]*$", ErrorMessage = "Please Enter valid Last Name")]
        public string LastName { get; set; }

        public string FullName { get; set; }
        public string Password { get; set; }
        [Required(ErrorMessage = "Please enter Email")]
        //[Em(ErrorMessage = "Please Enter valid Email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please Enter valid Email")]
        public string Email { get; set; }
        public string EmailBody { get; set; }
        
        [Required(ErrorMessage = "Please select Role")]
        [UIHint("SecretarialRoleId")]
        public int? SecretarialRoleId { get; set; }
        public string RoleName { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByText { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Please Enter valid Mobile number.")]
        public string ContactNumber { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()]*$", ErrorMessage = "Please Enter valid Address")]
        public string Address { get; set; }

        public Response Message { get; set; }
        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }

        [UIHint("ComplianceRoleId")]
        public int? ComplianceRoleId { get; set; }
        [UIHint("AuditRoleId")]
        public int? AuditRoleId { get; set; }
        [UIHint("HRRoleId")]
        public int? HRRoleId { get; set; }

        public bool accessToDirector { get; set; }
    }

    public class RoleVM
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string RoleCode { get; set; }
    }

    public class UserforDropdown
    {
       public long Id { get; set; }
       public string Name { get; set; }
            
    }

    public class UserPageVM
    {
        public bool ShowComplianceRole { get; set; }
        public bool ShowAuditRole { get; set; }
        public bool ShowHRRole { get; set; }
    }
}