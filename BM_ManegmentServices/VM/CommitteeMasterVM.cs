﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class CommitteeMasterVM
    {
        public long Id { get; set; }
        [UIHint("EntityId")]
        public int Entity_Id { get; set; }
        public string EntityName { get; set; }
        [UIHint("CommitteeId")]
        public int Committee_Id { get; set; }
        public string CommitteeName { get; set; }
        public string NameOfOtherCommittee { get; set; }

        [UIHint("DirectorId")]
        public long Director_Id { get; set; }
        public string DirectorName { get; set; }
        public string ImagePath { get; set; }
        public int Designation_Id { get; set; }
        public string Designation_Name { get; set; }
        public int Committee_Type { get; set; }
        public string Committee_Type_Name { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class CM_Director
    {
        public long DirectorId { get; set; }
        public string DirectorName { get; set; }
        public string ImagePath { get; set; }
    }

    public class CM_Entity
    {
        public long EntityId { get; set; }
        public string EntityName { get; set; }
    }

    public class CM_Designation
    {
        public long DesignationId { get; set; }
        public string Designation { get; set; }
    }

    public class CM_Committee_Type
    {
        public long CommitteeTypeId { get; set; }
        public string CommitteeType { get; set; }
    }
}