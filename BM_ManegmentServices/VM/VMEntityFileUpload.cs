﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMEntityFileUpload
    {
        public Response Response { get; set; }
        public long EntityId { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public string FilePath { get; set; }
        [Required]
        public HttpPostedFileBase File { get; set; }
    }
}