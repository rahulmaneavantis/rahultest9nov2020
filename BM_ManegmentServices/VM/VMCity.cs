﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMCity
    {
        public int Reg_cityId { get; set; }
        public string Name { get; set; }
        public int stateId { get; set; }
        public int countryId { get; set; }
        public string stateName { get; set; }
        public bool error { get; set; }
        public bool success { get; set; }
        public string Message { get; set; }
    }
}