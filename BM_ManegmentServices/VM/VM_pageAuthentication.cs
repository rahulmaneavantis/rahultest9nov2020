﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VM_pageAuthentication
    {
        public int? parentId { get; set; }
        public long Id { get; set; }
       
        public string ParentName { get; set; }
        public string PageName { get; set; }
        public int pageId { get; set; }

        public bool Pageview { get; set; }
        public bool Editview { get; set; }
        public bool Addview { get; set; }
        public bool DeleteView { get; set; }
        public int UserId { get; set; }
       
        public int EntityId { get; set; }
        public string successerrorMessage { get; set; }
        public bool success { get; set; }
        public bool error { get; set; }
        public bool hasChildren { get; set; }
        public bool CanEdit { get; set; }
        public bool CanAdd { get; set; }

    }
}