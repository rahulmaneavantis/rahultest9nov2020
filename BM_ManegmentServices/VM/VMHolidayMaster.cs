﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMHolidayMaster
    {
        public int Id { get; set; }
        public int? StateId { get; set; }
        public int? StockExchangeId { get; set; }
        public string stateName_StockExchangeName { get; set; }
        public string StateName { get; set; }
        public string StockExchange { get; set; }
        [Required]
        public string HolidaysDate { get; set; }
        public bool Message { get; set; }
        public string SuccessErrorMsg { get; set; }
        public bool errorMessage { get; set; }
        [Required]
        public string Holiday_Discription { get; set; }
        public bool IsStateorStockex { get; set; }

        public string FilePath { get; set; }
        [Required]
        public HttpPostedFileBase File { get; set; }
    }
}