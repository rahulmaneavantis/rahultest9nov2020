﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class Auditor_ResignationVM : IMessage
    {
        public int SA_ID { get; set; }
        public int statutory_Id { get; set; }
        public int _AuditorId { get; set; }
        public int AuditorMappingId { get; set; }
        public string AuditorName { get; set; }
        [Required(ErrorMessage = "Please Select Leave Type")]
        public string LeaveType { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required (ErrorMessage ="Please Select Resignation Date")]
        public DateTime? DateOfResignation { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfResignationSubmit { get; set; }
        public string textareaResonForLeave { get; set; }

        [ValidateFile(false, ErrorMessage = "Please upload File")]
        public HttpPostedFileBase Resignation_Doc { get; set; }
        public string Resignation_DocName { get; set; }
        public long? FileDataId { get; set; }

        public string AuditorType { get; set; }
        public int EntityId { get; set; }
        public long AgendaID { get; set; }
        public long MappingID { get; set; }
        public string Auditor_Name { get; set; }
        public string RegistrationNo { get; set; }
        public string Date_Of_Resignation { get; set; }
        public string Date_Of_Resignation_Submit { get; set; }

        public string AgendaFlow { get; set; }
    }

    public class Auditor_ResignationReviewVM : IMessage
    {
        public string ResignationFormat { get; set; }
    }
}