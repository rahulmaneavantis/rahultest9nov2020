﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class NatureOfInterestVM
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public bool IsDirector { get; set; }
        public bool IsMNGT { get; set; }
    }
}