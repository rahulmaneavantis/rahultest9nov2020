﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VM_NewMeetingModule
    {
        public int Id { get; set; }
        public int Entity { get; set; }
        public string Entity_Name { get; set; }
        public string FY { get; set; }
        public int BoardMeeting { get; set; }
        public string MeetingType { get; set; }
        public string Type { get; set; }
        public int customerId { get; set; }
        public bool errorMessage { get; set; }
        public bool successMessage { get; set; }
        public string Message { get; set; }
    }
}