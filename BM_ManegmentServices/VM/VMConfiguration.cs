﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMConfiguration
    {
        public int Id { get; set; }
        public int conId { get; set; }
        // [Required (ErrorMessage ="Please select Entity")]
        public int EntityId { get; set; }
        public bool? DefaultVirtualMeeting { get; set; }
        public long? FinancialYear { get; set; }

        public long? FinancialYearforCircural { get; set; }
        public string EntityName { get; set; }
        //[Required(ErrorMessage = "Please select Year Type")]
        public string YearType { get; set; }

        [Required(ErrorMessage = "Please select Meeting Type")]
        public int MeetingType { get; set; }
        public string Meeting_TypeName { get; set; }
        public int? Minutes_Pageno { get; set; }
        public int? MeetingNo { get; set; }
        public string MeetingNoFormate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? MeetingDate { get; set; }
        public string MeetingNoType { get; set; }
        public int CustomerId { get; set; }
        public bool errorMessage { get; set; }
        public bool successMessage { get; set; }
        public string errorsuccessMessage { get; set; }
        public bool IsCircular { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CircularDate { get; set; }
        public int? CircularNo { get; set; }
        public string CircularNoformate { get; set; }
        public string CircularNoType { get; set; }
        public int? LastCircularMinutePageNo { get; set; }
    }
}