﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMEntityType
    {
        public int Id { get; set; }
        public string EntityType { get; set; }
    }
}