//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_ShareholderListedEntityData
    {
        public long Id { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> EntityId { get; set; }
        public string FOLIO_NO { get; set; }
        public string SHARE_HOLDER_NAME { get; set; }
        public Nullable<long> SHARES { get; set; }
        public string JOINT_HOLDER1 { get; set; }
        public string JOINT_HOLDER2 { get; set; }
        public string JOINT_HOLDER3 { get; set; }
        public string FATHER_HUSBANDNAME { get; set; }
        public string ADDRESSLINE1 { get; set; }
        public string ADDRESSLINE2 { get; set; }
        public string ADDRESSLINE3 { get; set; }
        public string ADDRESSLINE4 { get; set; }
        public Nullable<int> PINCODE { get; set; }
        public string EMAILID { get; set; }
        public Nullable<long> PHONENO { get; set; }
        public string PANCARDNO { get; set; }
        public string SecondHolderPAN_No { get; set; }
        public string ThirdHolderPAN_No { get; set; }
        public string CATEGORY { get; set; }
        public string STATUS { get; set; }
        public string OCCUPATION { get; set; }
        public string BANK_AC_NO { get; set; }
        public string BANKNAME { get; set; }
        public string BANK_ADDRESS_LINE1 { get; set; }
        public string BANK_ADDRESS_LINE2 { get; set; }
        public string BANK_ADDRESS_LINE3 { get; set; }
        public string BANK_ADDRESS_LINE4 { get; set; }
        public Nullable<int> BANK_PINCODE { get; set; }
        public string BANK_AC_TYPE { get; set; }
        public string MICR_CODE { get; set; }
        public string IFSC { get; set; }
        public string NOM_NAME { get; set; }
        public string GAUARIDAN_NM { get; set; }
        public Nullable<long> RowNumber { get; set; }
        public Nullable<System.DateTime> Updatedby { get; set; }
        public Nullable<int> Updateon { get; set; }
        public Nullable<System.DateTime> Createdon { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> allotment_Date { get; set; }
    }
}
