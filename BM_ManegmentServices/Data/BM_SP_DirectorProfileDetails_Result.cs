//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    
    public partial class BM_SP_DirectorProfileDetails_Result
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string MiddleName { get; set; }
        public System.DateTime DOB { get; set; }
        public string MobileNo { get; set; }
        public string EmailId_Personal { get; set; }
        public string EmailId_Official { get; set; }
        public string DIN { get; set; }
        public string Photo_Doc { get; set; }
    }
}
