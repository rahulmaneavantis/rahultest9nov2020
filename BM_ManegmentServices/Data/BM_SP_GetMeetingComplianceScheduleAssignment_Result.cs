//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    
    public partial class BM_SP_GetMeetingComplianceScheduleAssignment_Result
    {
        public long MeetingID { get; set; }
        public long ScheduleOnID { get; set; }
        public Nullable<System.DateTime> ScheduleOn { get; set; }
        public string MappingType { get; set; }
        public int PartId { get; set; }
        public int SrNo { get; set; }
        public string AgendaItemHeading { get; set; }
        public string ShortDescription { get; set; }
        public int AssignmentID { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public string Role { get; set; }
        public string UserName { get; set; }
    }
}
