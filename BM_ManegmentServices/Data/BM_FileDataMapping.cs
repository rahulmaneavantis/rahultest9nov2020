//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_FileDataMapping
    {
        public long FileDataMappingID { get; set; }
        public long TransactionID { get; set; }
        public Nullable<long> ScheduledOnID { get; set; }
        public long FileID { get; set; }
        public Nullable<short> FileType { get; set; }
    }
}
