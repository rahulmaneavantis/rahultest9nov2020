﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.CustomerMaster
{
    public interface ICustomerMasters
    {
        List<CustomerVM> getCustomerDtls();
    }
}
