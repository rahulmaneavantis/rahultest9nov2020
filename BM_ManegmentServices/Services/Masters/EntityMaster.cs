﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using BM_ManegmentServices.VM;
using System.Data.Entity.SqlServer;
using System.Reflection;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text;
using System.IO;
using System.Web.Hosting;

using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using BM_ManegmentServices.Services.Setting;

namespace BM_ManegmentServices.Services.Masters
{
    public class EntityMaster : IEntityMaster
    {
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        ISettingService objISettingService;
        EntityMasterVM obj = new EntityMasterVM();
        bool CheckCustomerBranchLimit = true;
        int? serviceProviderId = 0;

        public EntityMaster(ISettingService objSettingService)
        {
            objISettingService = objSettingService;
        }
        public List<Customer_VM> GetCustomers()
        {
            return (from row in entities.Customers
                    orderby row.Name ascending
                    select new Customer_VM
                    {
                        CustomerId = row.ID,
                        Name = row.Name
                    }).ToList();
        }
        public string GetEntityName(int EntityID, int CustomerID)
        {
            return (from row in entities.BM_EntityMaster
                    where row.Id == EntityID && row.Customer_Id == CustomerID && row.Is_Deleted == false
                    select row.CompanyName).FirstOrDefault();
        }

        public string AddEntityMasterdtls(EntityMasterVM _objentity, int CustomerId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {

                    return "Entity Saved Successfully.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Something went wrong.";
            }
        }

        public void Dispose()
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                entities.Dispose();
            }
        }

        #region Role based entity Details
        public List<EntityMasterVM> GetAllEntityMaster(int customerID)
        {
            try
            {
                var customerBranches = (from row in entities.BM_EntityMaster
                                        join rows in entities.BM_EntityType
                                        on row.Entity_Type equals (rows.Id)
                                        where row.Is_Deleted == false && row.Customer_Id == customerID
                                        orderby row.CompanyName ascending
                                        select new EntityMasterVM
                                        {
                                            Id = row.Id,
                                            EntityName = row.CompanyName,
                                            Type = rows.EntityName,
                                            LLPI_CIN = row.CIN_LLPIN != "0" ? row.CIN_LLPIN : row.Registration_No,
                                            RegistrationNO = row.Registration_No,
                                            CIN = row.CIN_LLPIN,
                                            Entity_Type = row.Entity_Type,
                                            PAN = row.PAN,
                                            Islisted = row.IS_Listed,
                                        });
                return customerBranches.ToList();
            }
            catch (Exception ex)
            {
                List<EntityMasterVM> obj = new List<EntityMasterVM>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;

            }
        }
        public List<EntityMasterVM> GetAllEntityMaster(int customerID, int userID, string role)
        {
            try
            {
                if (role == SecretarialConst.Roles.CS)
                {
                    return (from row in entities.BM_EntityMaster
                            join rows in entities.BM_EntityType on row.Entity_Type equals (rows.Id)
                            join view in entities.BM_AssignedEntitiesView on row.Id equals view.EntityId
                            where row.Is_Deleted == false && row.Customer_Id == customerID &&
                            view.userId == userID
                            orderby row.CompanyName ascending
                            select new EntityMasterVM
                            {
                                Id = row.Id,
                                EntityName = row.CompanyName,
                                Type = rows.EntityName,
                                LLPI_CIN = row.CIN_LLPIN != "" ? row.CIN_LLPIN : row.Registration_No,
                                RegistrationNO = row.Registration_No,
                                CIN = row.CIN_LLPIN,
                                Entity_Type = row.Entity_Type,
                                PAN = row.PAN,
                                Islisted = row.IS_Listed,
                                CustomerId = row.Customer_Id,
                            }).ToList();
                }
                else
                {
                    return (from row in entities.BM_EntityMaster
                            join rows in entities.BM_EntityType
                            on row.Entity_Type equals (rows.Id)
                            where row.Is_Deleted == false && row.Customer_Id == customerID
                            orderby row.CompanyName ascending
                            select new EntityMasterVM
                            {
                                Id = row.Id,
                                EntityName = row.CompanyName,
                                Type = rows.EntityName,
                                LLPI_CIN = row.CIN_LLPIN != "" ? row.CIN_LLPIN : row.Registration_No,
                                RegistrationNO = row.Registration_No,
                                CIN = row.CIN_LLPIN,
                                Entity_Type = row.Entity_Type,
                                PAN = row.PAN,
                                Islisted = row.IS_Listed,
                                CustomerId = row.Customer_Id,
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                List<EntityMasterVM> obj = new List<EntityMasterVM>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public List<EntityMasterVM> GetEntityForTask(int customerID, int userID, string role)
        {
            List<EntityMasterVM> objentitydetails = new List<EntityMasterVM>();
            try
            {
                if (role == SecretarialConst.Roles.CS)
                {
                    objentitydetails = (from row in entities.BM_SP_EntityListforTaskUser(userID, customerID)
                                        select new EntityMasterVM
                                        {
                                            EntityName = row.CompanyName,
                                            Id = row.Id
                                        }).ToList();

                }
                else if (role == SecretarialConst.Roles.DRCTR)
                {
                    objentitydetails = (from row in entities.BM_SP_EntityListforBOD(userID, customerID)
                                        select new EntityMasterVM
                                        {
                                            EntityName = row.CompanyName,
                                            Id = row.ID
                                        }).ToList();
                }
                else if (role == SecretarialConst.Roles.HDCS)
                {
                    objentitydetails = (from row in entities.BM_EntityMaster
                                        where row.Customer_Id == customerID
                                        select new EntityMasterVM
                                        {
                                            EntityName = row.CompanyName,
                                            Id = row.Id
                                        }).ToList();
                }
                return objentitydetails;
            }
            catch (Exception ex)
            {

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return objentitydetails;
            }
        }

        //public List<EntityMasterVM> GetEntityForTask(int customerID, int userID, string role)
        //{
        //    List<EntityMasterVM> objentitydetails = new List<EntityMasterVM>();
        //    try
        //    {
        //        if (role == SecretarialConst.Roles.CS || role == SecretarialConst.Roles.HDCS)
        //        {
        //            objentitydetails = (from row in entities.BM_SP_EntityListforTaskUser(userID, customerID)
        //                                select new EntityMasterVM
        //                                {
        //                                    EntityName = row.CompanyName,
        //                                    Id = row.Id
        //                                }).ToList();

        //        }
        //        else if (role == SecretarialConst.Roles.DRCTR)
        //        {
        //            objentitydetails = (from row in entities.BM_SP_EntityListforBOD(userID, customerID)
        //                                select new EntityMasterVM
        //                                {
        //                                    EntityName = row.CompanyName,
        //                                    Id = row.ID
        //                                }).ToList();
        //        }
        //        return objentitydetails;
        //    }
        //    catch (Exception ex)
        //    {

        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return objentitydetails;
        //    }
        //}

        public List<EntityMasterVM> GetAllEntityMasterForMeeting(int customerID)
        {
            try
            {
                var customerBranches = (from row in entities.BM_EntityMaster
                                        join rows in entities.BM_EntityType
                                        on row.Entity_Type equals (rows.Id)
                                        where row.Is_Deleted == false && row.Customer_Id == customerID && rows.IsForMeeting == true
                                        orderby row.CompanyName ascending
                                        select new EntityMasterVM
                                        {
                                            Id = row.Id,
                                            EntityName = row.CompanyName,
                                            Type = rows.EntityName,
                                            LLPI_CIN = row.CIN_LLPIN != "0" ? row.CIN_LLPIN : row.Registration_No,
                                            RegistrationNO = row.Registration_No,
                                            CIN = row.CIN_LLPIN,
                                            Entity_Type = row.Entity_Type,
                                            PAN = row.PAN,
                                            Islisted = row.IS_Listed,
                                        });
                return customerBranches.ToList();
            }
            catch (Exception ex)
            {
                List<EntityMasterVM> obj = new List<EntityMasterVM>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;

            }
        }
        public List<EntityMasterVM> GetAllEntityMasterForMeeting(int customerID, int userID, string role)
        {
            try
            {
                if (role == SecretarialConst.Roles.CS)
                {
                    return (from row in entities.BM_EntityMaster
                            join rows in entities.BM_EntityType on row.Entity_Type equals (rows.Id)
                            join view in entities.BM_AssignedEntitiesView on row.Id equals view.EntityId
                            where row.Is_Deleted == false && row.Customer_Id == customerID && rows.IsForMeeting == true &&
                            view.userId == userID
                            orderby row.CompanyName ascending
                            select new EntityMasterVM
                            {
                                Id = row.Id,
                                Entity_Type = row.Entity_Type,
                                EntityName = row.CompanyName,
                                Type = rows.EntityName,
                                Islisted = row.IS_Listed,
                                DefaultVirtualMeeting =  false //row.DefaultVirtualMeeting
                            }).ToList();
                }
                else
                {
                    return (from row in entities.BM_EntityMaster
                            join rows in entities.BM_EntityType
                            on row.Entity_Type equals (rows.Id)
                            where row.Is_Deleted == false && row.Customer_Id == customerID && rows.IsForMeeting == true
                            orderby row.CompanyName ascending
                            select new EntityMasterVM
                            {
                                Id = row.Id,
                                EntityName = row.CompanyName,
                                Type = rows.EntityName,
                                LLPI_CIN = row.CIN_LLPIN != "0" ? row.CIN_LLPIN : row.Registration_No,
                                RegistrationNO = row.Registration_No,
                                CIN = row.CIN_LLPIN,
                                Entity_Type = row.Entity_Type,
                                PAN = row.PAN,
                                Islisted = row.IS_Listed,
                                DefaultVirtualMeeting = false //row.DefaultVirtualMeeting
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                List<EntityMasterVM> obj = new List<EntityMasterVM>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }
        public List<EntityMasterVM> GetAllEntityMasterForMeetingNew(int customerID, int userID, string role)
        {
            try
            {
                var result = GetAllEntityMasterForMeeting(customerID, userID, role);
                //var accessToDirector = objISettingService.GetAccessToDirector(customerID);
                var defaultVirtualMeeting = objISettingService.CheckDefaultVirtualMeeting(customerID);
                
                if (result != null && defaultVirtualMeeting)
                {
                    result.ForEach(k => k.DefaultVirtualMeeting = true);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new List<EntityMasterVM>();
            }
        }
        #endregion

        public List<EntityAddressVM> GetEntityAddressForMeeting(int Entity_Id, int customerID)
        {
            List<EntityAddressVM> obj = new List<EntityAddressVM>();
            try
            {
                var entity = (from row in entities.BM_EntityMaster
                              from city in entities.Cities.Where(k => k.ID == row.Regi_CityId).DefaultIfEmpty()
                              from state in entities.States.Where(k => k.ID == row.Regi_StateId).DefaultIfEmpty()
                              from corp_city in entities.Cities.Where(k => k.ID == row.Corp_CityId).DefaultIfEmpty()
                              from corp_state in entities.States.Where(k => k.ID == row.Corp_StateId).DefaultIfEmpty()
                              where row.Id == Entity_Id && row.Is_Deleted == false
                              select new
                              {
                                  row.Regi_Address_Line1,
                                  row.Regi_Address_Line2,
                                  Regi_City = city.Name,
                                  Regi_State = state.Name,
                                  row.Regi_PINCode,

                                  row.IsCorp_Office,

                                  row.Corp_Address_Line1,
                                  row.Corp_Address_Line2,
                                  Corp_City = corp_city.Name,
                                  Corp_State = corp_state.Name,
                                  row.Corp_PINCode,
                              }).FirstOrDefault();

                if (entity != null)
                {
                    obj = (from row in entities.BM_SP_GetAllMeetingVenue(Entity_Id, customerID)
                           select new EntityAddressVM
                           {
                               EntityId = Entity_Id,
                               EntityAddressType = row.ID,
                               EntityAddressTypeName = row.MeetingVenue,
                               EntityAddress = row.MeetingVenue
                           }).ToList();

                    if (obj == null)
                    {
                        obj = new List<EntityAddressVM>();
                    }

                    #region Registered Office
                    var cityName = "";
                    if (!string.IsNullOrWhiteSpace(entity.Regi_City))
                    {
                        cityName = ", " + entity.Regi_City;
                    }

                    var stateName = "";
                    if (!string.IsNullOrWhiteSpace(entity.Regi_State))
                    {
                        stateName = ", " + entity.Regi_State;
                    }
                    var pinCode = "";
                    if (!string.IsNullOrEmpty(entity.Regi_PINCode))
                    {
                        pinCode = " - " + entity.Regi_PINCode;
                    }

                    var item = new EntityAddressVM() { EntityId = Entity_Id, EntityAddressType = "R", EntityAddressTypeName = "Registered Office", EntityAddress = entity.Regi_Address_Line1 + " " + entity.Regi_Address_Line2 + cityName + stateName + pinCode };
                    obj.Insert(0, item);
                    #endregion

                    #region Co-Office
                    if (entity.IsCorp_Office)
                    {
                        cityName = "";
                        if (!string.IsNullOrWhiteSpace(entity.Corp_City))
                        {
                            cityName = ", " + entity.Corp_City;
                        }

                        stateName = "";
                        if (!string.IsNullOrWhiteSpace(entity.Corp_State))
                        {
                            stateName = ", " + entity.Corp_State;
                        }
                        pinCode = "";
                        if (!string.IsNullOrEmpty(entity.Corp_PINCode))
                        {
                            pinCode = " - " + entity.Regi_PINCode;
                        }

                        item = new EntityAddressVM() { EntityId = Entity_Id, EntityAddressType = "C", EntityAddressTypeName = "Corporate Office", EntityAddress = entity.Corp_Address_Line1 + " " + entity.Corp_Address_Line2 + cityName + stateName + pinCode };
                        obj.Insert(1, item);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            obj.Add(new EntityAddressVM() { EntityId = Entity_Id, EntityAddressType = "O", EntityAddressTypeName = "Other", EntityAddress = "" });
            return obj;
        }

        public IEnumerable<BM_StockExchange> GetAllStockExchange(bool addNew)
        {
            try
            {

                var ListofStockExchange = (from row in entities.BM_StockExchange
                                           where row.isActive == true
                                           orderby row.Name ascending
                                           select row).ToList();
                if (ListofStockExchange == null)
                {
                    ListofStockExchange = new List<BM_StockExchange>();
                }



                return ListofStockExchange;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<BM_BusinessActivity> GetBusinessActivityCode()
        {
            try
            {

                var ListofBusinessCode = (from row in entities.BM_BusinessActivity.OrderBy(x => x.Description) select row).ToList();

                return ListofBusinessCode;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<VMCity> GetCityList(int? stateId)
        {
            try
            {
                var ListofCity = (dynamic)null;
                if (stateId != null && stateId > 0)
                {

                    ListofCity = (from row in entities.Cities.OrderBy(x => x.Name)
                                  where row.StateId == stateId
                                  select new VMCity
                                  {
                                      Reg_cityId = row.ID,
                                      Name = row.Name,
                                  }).ToList();
                    if (ListofCity == null)
                    {
                        ListofCity = new List<VMCity>();
                    }

                    ListofCity.Insert(0, new VMCity() { Reg_cityId = 0, Name = "Select" });
                    return ListofCity;
                }

                else
                {
                    if (ListofCity == null)
                    {
                        ListofCity = new List<VMCity>();
                    }

                    ListofCity.Insert(0, new VMCity() { Reg_cityId = 0, Name = "Select" });
                    return ListofCity;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<BM_CompanyCategory> GetCompanyCategory()
        {
            try
            {

                var ListofCompCategory = (from row in entities.BM_CompanyCategory.OrderBy(x => x.Category)
                                          select row).ToList();
                if (ListofCompCategory == null)
                {
                    ListofCompCategory = new List<BM_CompanyCategory>();
                }

                ListofCompCategory.Insert(0, new BM_CompanyCategory() { Id = 0, Category = "Select" });
                return ListofCompCategory;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<BM_EntityType> GetCompanyType()
        {
            try
            {

                var ListofCompType = (from row in entities.BM_EntityType
                                      where row.ForEntityMaster == true
                                      orderby row.EntityName
                                      select row
                                      ).ToList();
                if (ListofCompType == null)
                {
                    ListofCompType = new List<BM_EntityType>();
                }

                ListofCompType.Insert(0, new BM_EntityType() { Id = 0, EntityName = "Select" });
                return ListofCompType;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<EntityTypeVM> GetCompanyTypeVM()
        {
            try
            {

                var ListofCompType = (from row in entities.BM_EntityType
                                      where row.ForEntityMaster == true
                                      orderby row.srno
                                      select new EntityTypeVM
                                      {
                                          EntityTypeId = row.Id,
                                          EntityTypeName = row.EntityName
                                      }).ToList();
                if (ListofCompType == null)
                {
                    ListofCompType = new List<EntityTypeVM>();
                }


                //ListofCompType.Insert(0, new EntityTypeVM() { EntityTypeId = 0, EntityTypeName = "All" });
                return ListofCompType;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<EntityTypeVM> EntityTypeVMForMeeting()
        {
            try
            {

                var ListofCompType = (from row in entities.BM_EntityType
                                      where row.IsForMeeting == true
                                      orderby row.EntityName
                                      select new EntityTypeVM
                                      {
                                          EntityTypeId = row.Id,
                                          EntityTypeName = row.EntityName
                                      }).ToList();
                if (ListofCompType == null)
                {
                    ListofCompType = new List<EntityTypeVM>();
                }
                return ListofCompType;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<BM_ROC_code> GetRocCode()
        {
            try
            {

                var ListofRocCode = (from row in entities.BM_ROC_code.OrderBy(x => x.Name)
                                     where row.isDeleted == false
                                     select row).ToList();
                if (ListofRocCode == null)
                {
                    ListofRocCode = new List<BM_ROC_code>();
                }

                ListofRocCode.Insert(0, new BM_ROC_code() { Id = 0, Name = "Select" });
                return ListofRocCode;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<VMState> GetStateList()
        {
            try
            {

                var Listofstate = (from row in entities.States.OrderBy(x => x.Name)
                                   select new VMState
                                   {
                                       SIdPublic = row.ID,
                                       Name = row.Name
                                   }).ToList();
                if (Listofstate == null)
                {
                    Listofstate = new List<VMState>();
                }

                Listofstate.Insert(0, new VMState() { SIdPublic = 0, Name = "Select State" });
                return Listofstate;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public string UpdateEntity(EntityMasterVM _objentity)
        {
            try
            {

                var _objUpdatebyType = (from row in entities.BM_EntityMaster
                                        where row.CompanyCategory_Id == _objentity.Entity_Type
                                        && row.Is_Deleted == false
                                        select row).FirstOrDefault();

                if (_objUpdatebyType != null)
                {
                    if (_objUpdatebyType.CompanyCategory_Id == 1)
                    {
                        return "Updated successfully";
                    }
                }

                return "Updated successfully";
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Something went Wrong";
            }
        }

        public IEnumerable<BM_BusinessActivity> GetBusinessActivDesc(int businessId)
        {
            try
            {
                if (businessId > 0)
                {


                    var objgetBADesc = (from row in entities.BM_BusinessActivity.OrderBy(x => x.Description)
                                        where row.Id == businessId
                                        select row).ToList();

                    return objgetBADesc;
                }


                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<EntityMasterVM> GetAllEntityMasterForCommittee(long Director_Id)
        {
            var customerBranches = (from row in entities.BM_EntityMaster
                                    join rows in entities.BM_Directors_DetailsOfInterest
                                    on row.Id equals (rows.EntityId)
                                    where rows.Director_Id == Director_Id && row.Is_Deleted == false && rows.IsDeleted == false && rows.IsActive == true && (rows.NatureOfInterest == 2 || rows.NatureOfInterest == 9 || rows.NatureOfInterest == 10)
                                    select new EntityMasterVM
                                    {
                                        Id = row.Id,
                                        EntityName = row.CompanyName
                                    });
            return customerBranches.ToList();

        }

        public Pravate_PublicVM CreatePublicPrivate(Pravate_PublicVM _objentity, int customerId,int UserId)
        {
            var ICSI_MODE = Convert.ToInt32(ConfigurationManager.AppSettings["AVASEC_IS_ICSI_MODE"]);

            //int? serviceProviderId = GetServiseProviderId(customerId);
            int? parentID = GetParentID(customerId);
            if (ICSI_MODE == 1 && parentID > 0)
            {
                CheckCustomerBranchLimit = ICIAManagement.CheckCustomerEntityLimitByDistributor(Convert.ToInt32(parentID));
                //CheckCustomerBranchLimit = ICIAManagement.CheckCustoemrBranchLimit(Convert.ToInt32(parentID), customerId);
            }
            if (CheckCustomerBranchLimit)
            {
                #region Upper Case
                try
                {
                    if (_objentity.EntityName != null)
                    {
                        _objentity.EntityName = _objentity.EntityName.ToUpper();
                    }

                    if (_objentity.CIN != null)
                    {
                        _objentity.CIN = _objentity.CIN.ToUpper();
                    }

                    if (_objentity.PAN != null)
                    {
                        _objentity.PAN = _objentity.PAN.ToUpper();
                    }

                    if (_objentity.GST != null)
                    {
                        _objentity.GST = _objentity.GST.ToUpper();
                    }

                    if (_objentity.Regi_Address_Line1 != null)
                    {
                        _objentity.Regi_Address_Line1 = _objentity.Regi_Address_Line1.ToUpper();
                    }

                    if (_objentity.Regi_Address_Line2 != null)
                    {
                        _objentity.Regi_Address_Line2 = _objentity.Regi_Address_Line2.ToUpper();
                    }

                    if (_objentity.Corp_Address_Line1 != null)
                    {
                        _objentity.Corp_Address_Line1 = _objentity.Corp_Address_Line1.ToUpper();
                    }

                    if (_objentity.Corp_Address_Line2 != null)
                    {
                        _objentity.Corp_Address_Line2 = _objentity.Corp_Address_Line2.ToUpper();
                    }


                }
                catch (Exception ex)
                {
                    LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                #endregion

                string Message = "";
                #region Check EntityFor Avacom 
                com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                {
                    ID = _objentity.CustomerBranchId,
                    Name = _objentity.EntityName,
                    //Type = Convert.ToByte(ddlType.SelectedValue),
                    //ComType = Convert.ToByte(comType),
                    AddressLine1 = _objentity.Regi_Address_Line1,
                    AddressLine2 = _objentity.Regi_Address_Line2,
                    StateID = _objentity.Regi_StateId,
                    CityID = _objentity.Regi_CityId,
                    Others = "",
                    PinCode = _objentity.Regi_PINCode,
                    ContactPerson = "",
                    Landline = "",
                    Mobile = "",
                    EmailID = _objentity.Email_Id,
                    CustomerID = customerId,
                    //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                };

                customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, _objentity.IS_Listed, out Message);
                #endregion

                if (customerBranch != null)
                {
                    if (customerBranch.ID > 0)
                    {
                        #region Check Entity For AvaSec if Customer Branch Id is greater then zero
                        try
                        {

                            var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                    where row.Is_Deleted == false
                                                    && row.CIN_LLPIN == _objentity.CIN
                                                    && row.Customer_Id == customerId
                                                    select row).FirstOrDefault();
                            if (_objCreateEntity == null)
                            {
                                BM_EntityMaster _bmentity = new BM_EntityMaster();


                                if (_objentity.Entity_Type == 1 || _objentity.Entity_Type == 2 || _objentity.Entity_Type == 9 || _objentity.Entity_Type == 10)
                                {

                                    #region Assign Values
                                    if (!(string.IsNullOrEmpty(_objentity.EntityName)))
                                        _bmentity.CompanyName = _objentity.EntityName;
                                    if (_objentity.Entity_Type > 0)
                                        _bmentity.Entity_Type = _objentity.Entity_Type;
                                    if (!string.IsNullOrEmpty(_objentity.CIN))
                                        _bmentity.CIN_LLPIN = _objentity.CIN;
                                    if (!string.IsNullOrEmpty(_objentity.GLN))
                                    {
                                        _bmentity.GLN = _objentity.GLN;
                                    }
                                    else
                                    {
                                        _bmentity.GLN = "";
                                    }
                                    if (!string.IsNullOrEmpty(_objentity.FY))
                                        _bmentity.FY_CY = _objentity.FY;
                                    if (_objentity.IncorporationDate != null)
                                        _bmentity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.IncorporationDate);
                                    if (!string.IsNullOrEmpty(_objentity.Regi_Address_Line1))
                                        _bmentity.Regi_Address_Line1 = _objentity.Regi_Address_Line1;
                                    if (!string.IsNullOrEmpty(_objentity.Regi_Address_Line2))
                                        _bmentity.Regi_Address_Line2 = _objentity.Regi_Address_Line2;
                                    if (_objentity.Regi_StateId > 0)
                                        _bmentity.Regi_StateId = _objentity.Regi_StateId;
                                    if (_objentity.Regi_CityId > 0)
                                        _bmentity.Regi_CityId = _objentity.Regi_CityId;
                                    if (!string.IsNullOrEmpty(_objentity.Regi_PINCode))
                                        _bmentity.Regi_PINCode = _objentity.Regi_PINCode;
                                    if (_objentity.ROC_Code > 0)
                                        _bmentity.ROC_Code = _objentity.ROC_Code;
                                    if (_objentity.IsCorp_Office)
                                    {
                                        if (!string.IsNullOrEmpty(_objentity.Corp_Address_Line1))
                                            _bmentity.Corp_Address_Line1 = _objentity.Corp_Address_Line1;
                                        if (!string.IsNullOrEmpty(_objentity.Corp_Address_Line2))
                                            _bmentity.Corp_Address_Line2 = _objentity.Corp_Address_Line2;
                                        if (_objentity.Corp_StateId > 0)
                                            _bmentity.Corp_StateId = _objentity.Corp_StateId;
                                        if (_objentity.Corp_CityId > 0)
                                            _bmentity.Corp_CityId = _objentity.Corp_CityId;
                                        if (!string.IsNullOrEmpty(_objentity.Corp_PINCode))
                                            _bmentity.Corp_PINCode = _objentity.Corp_PINCode;
                                        _bmentity.IsCorp_Office = _objentity.IsCorp_Office;
                                        _bmentity.IsCorporateSameAddress = _objentity.Is_SameAddress;
                                    }
                                    else
                                    {
                                        _bmentity.Corp_Address_Line1 = "";
                                        _bmentity.Corp_Address_Line2 = "";
                                        _bmentity.Corp_StateId = 0;
                                        _bmentity.Corp_CityId = 0;
                                        _bmentity.Corp_PINCode = "";
                                        _bmentity.IsCorp_Office = _objentity.IsCorp_Office;
                                        _bmentity.IsCorporateSameAddress = _objentity.Is_SameAddress;
                                    }
                                    if (!string.IsNullOrEmpty(_objentity.Registration_No))
                                        _bmentity.Registration_No = _objentity.Registration_No;
                                    if (!string.IsNullOrEmpty(_objentity.Registration_No))
                                        _bmentity.Registration_No = _objentity.Registration_No;
                                    if (!string.IsNullOrEmpty(_objentity.Registration_No))
                                        _bmentity.Registration_No = _objentity.Registration_No;
                                    if (!string.IsNullOrEmpty(_objentity.Email_Id))
                                        _bmentity.Email_Id = _objentity.Email_Id;
                                    if (!string.IsNullOrEmpty(_objentity.PAN))
                                        _bmentity.PAN = _objentity.PAN;
                                    if (!string.IsNullOrEmpty(_objentity.WebSite))
                                        _bmentity.WebSite = _objentity.WebSite;
                                    if (!string.IsNullOrEmpty(_objentity.GST))
                                        _bmentity.GST = _objentity.GST;
                                    if (!string.IsNullOrEmpty(_objentity.GST))
                                        _bmentity.GST = _objentity.GST;
                                    if (_objentity.CompanyCategory_Id > 0)
                                        _bmentity.CompanyCategory_Id = _objentity.CompanyCategory_Id;
                                    if (_objentity.NICCode > 0)
                                    {
                                        _bmentity.NIC_Code = _objentity.NICCode;
                                    }
                                    else
                                    {
                                        _bmentity.NIC_Code = 0;
                                    }
                                    if (!string.IsNullOrEmpty(_objentity.Description))
                                    {
                                        _bmentity.NIC_Discription = _objentity.Description;
                                    }
                                    else
                                    {
                                        _bmentity.NIC_Discription = "";
                                    }
                                    _bmentity.IS_Listed = _objentity.IS_Listed;
                                    if (_objentity.IS_Listed)
                                    {
                                        _bmentity.ISIN = _objentity.ISIN;
                                        _bmentity.RTA_CIN = _objentity.RTA_CIN;
                                        _bmentity.RTA_Address = _objentity.RTA_Address;
                                        _bmentity.RTA_CompanyName = _objentity.RTA_Name;
                                    }
                                    else
                                    {
                                        _bmentity.ISIN = "";
                                    }
                                    _bmentity.CompanySubCategory = _objentity.CompanyCategorySub_Id;
                                    _bmentity.Is_Deleted = false;
                                    _bmentity.Customer_Id = customerId;
                                    _bmentity.CustomerBranchId = customerBranch.ID;
                                    _bmentity.CreatedBy = UserId;
                                    _bmentity.CreatedOn = DateTime.Now;
                                    #endregion


                                    entities.BM_EntityMaster.Add(_bmentity);
                                    entities.SaveChanges();
                                    _objentity.Id = _bmentity.Id;
                                    _objentity.Message = true;
                                    _objentity.successErrorMessage = "Entity Saved Successfully";

                                }
                                else
                                {
                                    _objentity.errorMessage = true;
                                    _objentity.successErrorMessage = "Please Select Entity Type";
                                    return _objentity;
                                }
                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Entity/Company with same CIN already exists";
                                return _objentity;
                            }
                        }
                        catch (Exception ex)
                        {

                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Server Error Occur";
                            LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            return _objentity;
                        }
                        #endregion
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong !Please Check Customer Branch ";

                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Something went wrong !Please Check Customer Branch ";

                }
            }
            else
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "You are not allowed to create more entity/company, Contact Avantis to allow to create more";
            }
            return _objentity;
        }

        public Pravate_PublicVM UpdatePublicPrivateEntity(Pravate_PublicVM _objentity, int customerId,int UserId)
        {
            try
            {
                string Message = "";
                var checkForEntity = (from row in entities.BM_EntityMaster where row.Id == _objentity.Id && row.Is_Deleted == false select row).FirstOrDefault();

                if (checkForEntity != null)
                {
                    if (_objentity.Entity_Type == 1 || _objentity.Entity_Type == 2 || _objentity.Entity_Type == 9 || _objentity.Entity_Type == 10)
                    {
                        #region Check customer branch ID
                        if(checkForEntity.CustomerBranchId == null || checkForEntity.CustomerBranchId == 0)
                        {
                            #region Check EntityFor Avacom 
                            com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                            {
                                ID = _objentity.CustomerBranchId,
                                Name = _objentity.EntityName,
                                //Type = Convert.ToByte(ddlType.SelectedValue),
                                //ComType = Convert.ToByte(comType),
                                AddressLine1 = _objentity.Regi_Address_Line1,
                                AddressLine2 = _objentity.Regi_Address_Line2,
                                StateID = _objentity.Regi_StateId,
                                CityID = _objentity.Regi_CityId,
                                Others = "",
                                PinCode = _objentity.Regi_PINCode,
                                ContactPerson = "",
                                Landline = "",
                                Mobile = "",
                                EmailID = _objentity.Email_Id,
                                CustomerID = customerId,
                                //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                                //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                            };

                            customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, _objentity.IS_Listed, out Message);
                            #endregion

                            if(customerBranch.ID > 0)
                            {
                                checkForEntity.CustomerBranchId = customerBranch.ID;
                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Something went wrong";
                                return _objentity;
                            }
                        }
                        #endregion

                        #region Assign Values

                        checkForEntity.CompanyName = _objentity.EntityName;

                        checkForEntity.Entity_Type = _objentity.Entity_Type;

                        checkForEntity.CIN_LLPIN = _objentity.CIN;
                        if (!string.IsNullOrEmpty(_objentity.GLN))
                        {
                            checkForEntity.GLN = _objentity.GLN;
                        }
                        else
                        {
                            checkForEntity.GLN = "";
                        }
                        if (!string.IsNullOrEmpty(_objentity.FY))
                            checkForEntity.FY_CY = _objentity.FY;
                        if (_objentity.IncorporationDate != null)
                            checkForEntity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.IncorporationDate);
                        if (!string.IsNullOrEmpty(_objentity.Regi_Address_Line1))
                            checkForEntity.Regi_Address_Line1 = _objentity.Regi_Address_Line1;
                        if (!string.IsNullOrEmpty(_objentity.Regi_Address_Line2))
                            checkForEntity.Regi_Address_Line2 = _objentity.Regi_Address_Line2;
                        if (_objentity.Regi_StateId > 0)
                            checkForEntity.Regi_StateId = _objentity.Regi_StateId;
                        if (_objentity.Regi_CityId > 0)
                            checkForEntity.Regi_CityId = _objentity.Regi_CityId;
                        if (!string.IsNullOrEmpty(_objentity.Regi_PINCode))
                            checkForEntity.Regi_PINCode = _objentity.Regi_PINCode;
                        if (_objentity.ROC_Code > 0)
                            checkForEntity.ROC_Code = _objentity.ROC_Code;
                        if (_objentity.IsCorp_Office)
                        {
                            if (!string.IsNullOrEmpty(_objentity.Corp_Address_Line1))
                                checkForEntity.Corp_Address_Line1 = _objentity.Corp_Address_Line1;
                            if (!string.IsNullOrEmpty(_objentity.Corp_Address_Line2))
                                checkForEntity.Corp_Address_Line2 = _objentity.Corp_Address_Line2;
                            if (_objentity.Corp_StateId > 0)
                                checkForEntity.Corp_StateId = _objentity.Corp_StateId;
                            if (_objentity.Corp_CityId > 0)
                                checkForEntity.Corp_CityId = _objentity.Corp_CityId;
                            if (!string.IsNullOrEmpty(_objentity.Corp_PINCode))
                                checkForEntity.Corp_PINCode = _objentity.Corp_PINCode;
                            checkForEntity.IsCorp_Office = _objentity.IsCorp_Office;
                            checkForEntity.IsCorporateSameAddress = _objentity.Is_SameAddress;
                        }
                        else
                        {
                            checkForEntity.Corp_Address_Line1 = "";
                            checkForEntity.Corp_Address_Line2 = "";
                            checkForEntity.Corp_StateId = 0;
                            checkForEntity.Corp_CityId = 0;
                            checkForEntity.Corp_PINCode = "";
                            checkForEntity.IsCorp_Office = _objentity.IsCorp_Office;
                            checkForEntity.IsCorporateSameAddress = _objentity.Is_SameAddress;
                        }
                        if (!string.IsNullOrEmpty(_objentity.Registration_No))
                            checkForEntity.Registration_No = _objentity.Registration_No;
                        if (!string.IsNullOrEmpty(_objentity.Registration_No))
                            checkForEntity.Registration_No = _objentity.Registration_No;
                        if (!string.IsNullOrEmpty(_objentity.Registration_No))
                            checkForEntity.Registration_No = _objentity.Registration_No;
                        if (!string.IsNullOrEmpty(_objentity.Email_Id))
                            checkForEntity.Email_Id = _objentity.Email_Id;
                        if (!string.IsNullOrEmpty(_objentity.PAN))
                            checkForEntity.PAN = _objentity.PAN;
                        if (!string.IsNullOrEmpty(_objentity.WebSite))
                            checkForEntity.WebSite = _objentity.WebSite;
                        if (!string.IsNullOrEmpty(_objentity.GST))
                            checkForEntity.GST = _objentity.GST;
                        if (!string.IsNullOrEmpty(_objentity.GST))
                            checkForEntity.GST = _objentity.GST;
                        if (_objentity.CompanyCategory_Id > 0)
                            checkForEntity.CompanyCategory_Id = _objentity.CompanyCategory_Id;
                        if (_objentity.NICCode > 0)
                        {
                            checkForEntity.NIC_Code = _objentity.NICCode;
                        }
                        else
                        {
                            checkForEntity.NIC_Code = 0;
                        }
                        if (!string.IsNullOrEmpty(_objentity.Description))
                        {
                            checkForEntity.NIC_Discription = _objentity.Description;
                        }
                        else
                        {
                            checkForEntity.NIC_Discription = "";
                        }
                        checkForEntity.IS_Listed = _objentity.IS_Listed;
                        if (_objentity.IS_Listed)
                        {
                            checkForEntity.ISIN = _objentity.ISIN;
                            checkForEntity.RTA_CIN = _objentity.RTA_CIN;
                            checkForEntity.RTA_Address = _objentity.RTA_Address;
                            checkForEntity.RTA_CompanyName = _objentity.RTA_Name;
                        }
                        else
                        {
                            checkForEntity.ISIN = "";
                        }
                        checkForEntity.CompanySubCategory = _objentity.CompanyCategorySub_Id;
                        checkForEntity.Is_Deleted = false;
                        checkForEntity.Customer_Id = customerId;

                        checkForEntity.UpdatedBy = UserId;
                        checkForEntity.UpdatedOn = DateTime.Now;
                        //checkForEntity.CustomerBranchId = customerBranch.ID;

                        #endregion
                        entities.SaveChanges();
                        _objentity.Message = true;
                        _objentity.successErrorMessage = "Entity updated successfully";

                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Please Select Entity Type";
                        return _objentity;
                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Something went wrong";
                    return _objentity;
                }
            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "Server Error Occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objentity;
        }


        public LLPVM CreateLLP(LLPVM _objentity, int customerId,int UserId)
        {

            try
            {
                var ICSI_MODE = Convert.ToInt32(WebConfigurationManager.AppSettings["AVASEC_IS_ICSI_MODE"]);

                int? parentID = GetParentID(customerId);
                if (ICSI_MODE == 1 && parentID > 0)
                {
                    CheckCustomerBranchLimit = ICIAManagement.CheckCustomerEntityLimitByDistributor(Convert.ToInt32(parentID));
                    //CheckCustomerBranchLimit = ICIAManagement.CheckCustoemrBranchLimit(Convert.ToInt32(parentID), customerId);
                }
                if (CheckCustomerBranchLimit)
                {
                    string Message = "";
                    com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                    {
                        ID = _objentity.CustomerBranchId,
                        Name = _objentity.EntityName,
                        //Type = Convert.ToByte(ddlType.SelectedValue),
                        //ComType = Convert.ToByte(comType),
                        AddressLine1 = _objentity.llpRegi_Address_Line1,
                        AddressLine2 = _objentity.llpRegi_Address_Line2,
                        StateID = _objentity.llpRegi_StateId,
                        CityID = _objentity.llpRegi_CityId,
                        Others = "",
                        PinCode = _objentity.llpRegi_PINCode,
                        Landline = "",
                        Mobile = "",
                        EmailID = _objentity.llpEmail_Id,
                        CustomerID = customerId,
                        //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                    };

                    customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                    if (customerBranch != null)
                    {
                        if (customerBranch.ID > 0)
                        {
                            if (_objentity.EntityName != null)
                            {
                                _objentity.EntityName = _objentity.EntityName.ToUpper();
                            }

                            if (_objentity.LLPIN != null)
                            {
                                _objentity.LLPIN = _objentity.LLPIN.ToUpper();
                            }

                            if (_objentity.llpPAN != null)
                            {
                                _objentity.llpPAN = _objentity.llpPAN.ToUpper();
                            }

                            if (_objentity.llpGST != null)
                            {
                                _objentity.llpGST = _objentity.llpGST.ToUpper();
                            }

                            if (_objentity.llpRegi_Address_Line1 != null)
                            {
                                _objentity.llpRegi_Address_Line1 = _objentity.llpRegi_Address_Line1.ToUpper();
                            }

                            if (_objentity.llpRegi_Address_Line2 != null)
                            {
                                _objentity.llpRegi_Address_Line2 = _objentity.llpRegi_Address_Line2.ToUpper();
                            }

                            if (_objentity.llpCorp_Address_Line1 != null)
                            {
                                _objentity.llpCorp_Address_Line1 = _objentity.llpCorp_Address_Line1.ToUpper();
                            }

                            if (_objentity.llpCorp_Address_Line2 != null)
                            {
                                _objentity.llpCorp_Address_Line2 = _objentity.llpCorp_Address_Line2.ToUpper();
                            }
                            var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                    where row.Is_Deleted == false
                                                    && row.CompanyName == _objentity.EntityName
                                                    && row.Entity_Type == _objentity.Entity_Type
                                                    && row.CIN_LLPIN == _objentity.LLPIN
                                                    && row.Email_Id == _objentity.llpEmail_Id
                                                    && row.Customer_Id == customerId
                                                    select row).FirstOrDefault();
                            if (_objCreateEntity == null)
                            {
                                if (_objentity.Entity_Type == 3)
                                {
                                    BM_EntityMaster _bmentity = new BM_EntityMaster();
                                    if (!(string.IsNullOrEmpty(_objentity.EntityName)))
                                        _bmentity.CompanyName = _objentity.EntityName;
                                    if (_objentity.Entity_Type > 0)
                                        _bmentity.Entity_Type = _objentity.Entity_Type;
                                    if (!string.IsNullOrEmpty(_objentity.llpFY))
                                        _bmentity.FY_CY = _objentity.llpFY;
                                    if (!string.IsNullOrEmpty(_objentity.LLPIN))
                                        _bmentity.CIN_LLPIN = _objentity.LLPIN;
                                    if (!string.IsNullOrEmpty(_objentity.llpGLN))
                                        _bmentity.GLN = _objentity.llpGLN;
                                    if (_objentity.llpIncorporationDate != null)
                                        _bmentity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.llpIncorporationDate);
                                    if (!string.IsNullOrEmpty(_objentity.llpRegi_Address_Line1))
                                        _bmentity.Regi_Address_Line1 = _objentity.llpRegi_Address_Line1;
                                    if (!string.IsNullOrEmpty(_objentity.llpRegi_Address_Line2))
                                        _bmentity.Regi_Address_Line2 = _objentity.llpRegi_Address_Line2;
                                    if (_objentity.llpRegi_StateId > 0)
                                        _bmentity.Regi_StateId = _objentity.llpRegi_StateId;
                                    if (_objentity.llpRegi_CityId > 0)
                                        _bmentity.Regi_CityId = _objentity.llpRegi_CityId;
                                    if (!string.IsNullOrEmpty(_objentity.llpRegi_PINCode))
                                        _bmentity.Regi_PINCode = _objentity.llpRegi_PINCode;
                                    if (_objentity.llpROC_Code > 0)
                                        _bmentity.ROC_Code = _objentity.llpROC_Code;
                                    if (_objentity.llpNo_Of_Partners > 0)
                                        _bmentity.No_Of_Partners = _objentity.llpNo_Of_Partners;
                                    if (_objentity.llpNo_Of_DesignatedPartners > 0)
                                        _bmentity.No_Of_DesignatedPartners = _objentity.llpNo_Of_DesignatedPartners;
                                    if (_objentity.llpObligation_Of_Contribution > 0)
                                        _bmentity.Obligation_Of_Contribution = _objentity.llpObligation_Of_Contribution;

                                    if (_objentity.llpIscorporateforllp)
                                    {
                                        if (!string.IsNullOrEmpty(_objentity.llpCorp_Address_Line1))
                                            _bmentity.Corp_Address_Line1 = _objentity.llpCorp_Address_Line1;
                                        if (!string.IsNullOrEmpty(_objentity.llpCorp_Address_Line2))
                                            _bmentity.Corp_Address_Line2 = _objentity.llpCorp_Address_Line2;
                                        if (_objentity.llpCorp_StateId > 0)
                                            _bmentity.Corp_StateId = _objentity.llpCorp_StateId;
                                        if (_objentity.llpCorp_CityId > 0)
                                            _bmentity.Corp_CityId = _objentity.llpCorp_CityId;
                                        if (!string.IsNullOrEmpty(_objentity.llpCorp_PINCode))
                                            _bmentity.Corp_PINCode = _objentity.llpCorp_PINCode;
                                        _bmentity.IsCorp_Office = _objentity.llpIscorporateforllp;
                                        _bmentity.IsCorporateSameAddress = _objentity.Is_SameAddressllp;

                                    }
                                    else
                                    {
                                        _bmentity.Corp_Address_Line1 = "";
                                        _bmentity.Corp_Address_Line2 = "";
                                        _bmentity.Corp_StateId = 0;
                                        _bmentity.Corp_CityId = 0;
                                        _bmentity.Corp_PINCode = "";
                                        _bmentity.IsCorp_Office = _objentity.llpIscorporateforllp;
                                        _bmentity.IsCorporateSameAddress = _objentity.Is_SameAddressllp;
                                    }
                                    if (!string.IsNullOrEmpty(_objentity.llpEmail_Id))
                                        _bmentity.Email_Id = _objentity.llpEmail_Id;
                                    if (!string.IsNullOrEmpty(_objentity.llpPAN))
                                        _bmentity.PAN = _objentity.llpPAN;
                                    if (!string.IsNullOrEmpty(_objentity.llpWebSite))
                                        _bmentity.WebSite = _objentity.llpWebSite;
                                    if (!string.IsNullOrEmpty(_objentity.llpGST))
                                        _bmentity.GST = _objentity.llpGST;
                                    _bmentity.Registration_No = "0";
                                    _bmentity.NIC_Discription = "0";
                                    _bmentity.Is_Deleted = false;
                                    _bmentity.Customer_Id = customerId;
                                    _bmentity.CustomerBranchId = customerBranch.ID;
                                    _bmentity.Maindivisonbusiness = _objentity.llpbusinessActivityID;
                                    _bmentity.CreatedOn = DateTime.Now;
                                    _bmentity.CreatedBy = UserId;


                                    entities.BM_EntityMaster.Add(_bmentity);
                                    entities.SaveChanges();
                                    _objentity.Id = _bmentity.Id;
                                }
                                _objentity.Message = true;
                                _objentity.successErrorMessage = "Saved Successfully.";

                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Entity/Comapny with same LLPIN already exists";
                            }
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong!Please check Customer Branch";

                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong!Please check Customer Branch";

                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "You are not allowed to create more entity/company, Contact Avantis to allow to create more";
                }

            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "Server Error Occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objentity;
        }

        public LLPVM UpdateLLP(LLPVM _objentity, int customerId,int UserId)
        {
            try
            {
                string Message = "";
                #region Check EntityFor Avacom 
                com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                {
                    ID = _objentity.CustomerBranchId,
                    Name = _objentity.EntityName,
                    //Type = Convert.ToByte(ddlType.SelectedValue),
                    //ComType = Convert.ToByte(comType),
                    AddressLine1 = _objentity.llpRegi_Address_Line1,
                    AddressLine2 = _objentity.llpRegi_Address_Line2,
                    StateID = _objentity.llpRegi_StateId,
                    CityID = _objentity.llpRegi_CityId,
                    Others = "",
                    PinCode = _objentity.llpRegi_PINCode,
                    ContactPerson = "",
                    Landline = "",
                    Mobile = "",
                    EmailID = _objentity.llpEmail_Id,
                    CustomerID = customerId,
                    //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                };

                customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                #endregion

                if (customerBranch != null)
                {
                    if (customerBranch.ID > 0)
                    {
                        if (_objentity.EntityName != null)
                            _objentity.EntityName = _objentity.EntityName.ToUpper();
                        if (_objentity.LLPIN != null)
                            _objentity.LLPIN = _objentity.LLPIN.ToUpper();

                        if (_objentity.llpPAN != null)
                            _objentity.llpPAN = _objentity.llpPAN.ToUpper();

                        if (_objentity.llpGST != null)
                            _objentity.llpGST = _objentity.llpGST.ToUpper();

                        if (_objentity.llpRegi_Address_Line1 != null)
                            _objentity.llpRegi_Address_Line1 = _objentity.llpRegi_Address_Line1.ToUpper();

                        if (_objentity.llpRegi_Address_Line2 != null)

                            _objentity.llpRegi_Address_Line2 = _objentity.llpRegi_Address_Line2.ToUpper();

                        if (_objentity.llpCorp_Address_Line1 != null)
                            _objentity.llpCorp_Address_Line1 = _objentity.llpCorp_Address_Line1.ToUpper();
                        if (_objentity.llpCorp_Address_Line2 != null)

                            _objentity.llpCorp_Address_Line2 = _objentity.llpCorp_Address_Line2.ToUpper();

                        var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                where row.Is_Deleted == false
                                                && row.Entity_Type == _objentity.Entity_Type
                                                && row.Id == _objentity.Id
                                                select row).FirstOrDefault();
                        if (_objCreateEntity != null)
                        {
                            if (_objCreateEntity.Entity_Type == 3)
                            {

                                _objCreateEntity.CompanyName = _objentity.EntityName;
                                _objCreateEntity.Entity_Type = _objentity.Entity_Type;
                                _objCreateEntity.FY_CY = _objentity.llpFY;
                                _objCreateEntity.CIN_LLPIN = _objentity.LLPIN;

                                _objCreateEntity.GLN = _objentity.llpGLN;
                                if (_objentity.llpIncorporationDate != null)
                                    _objCreateEntity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.llpIncorporationDate);

                                _objCreateEntity.Regi_Address_Line1 = _objentity.llpRegi_Address_Line1;

                                _objCreateEntity.Regi_Address_Line2 = _objentity.llpRegi_Address_Line2;
                                if (_objentity.llpRegi_StateId > 0)
                                    _objCreateEntity.Regi_StateId = _objentity.llpRegi_StateId;
                                if (_objentity.llpRegi_CityId > 0)
                                    _objCreateEntity.Regi_CityId = _objentity.llpRegi_CityId;
                                if (!string.IsNullOrEmpty(_objentity.llpRegi_PINCode))
                                    _objCreateEntity.Regi_PINCode = _objentity.llpRegi_PINCode;
                                if (_objentity.llpROC_Code > 0)
                                    _objCreateEntity.ROC_Code = _objentity.llpROC_Code;
                                if (_objentity.llpNo_Of_Partners > 0)
                                    _objCreateEntity.No_Of_Partners = _objentity.llpNo_Of_Partners;
                                if (_objentity.llpNo_Of_DesignatedPartners > 0)
                                    _objCreateEntity.No_Of_DesignatedPartners = _objentity.llpNo_Of_DesignatedPartners;
                                if (_objentity.llpObligation_Of_Contribution > 0)
                                    _objCreateEntity.Obligation_Of_Contribution = _objentity.llpObligation_Of_Contribution;

                                if (_objentity.llpIscorporateforllp)
                                {
                                    if (!string.IsNullOrEmpty(_objentity.llpCorp_Address_Line1))
                                        _objCreateEntity.Corp_Address_Line1 = _objentity.llpCorp_Address_Line1;
                                    if (!string.IsNullOrEmpty(_objentity.llpCorp_Address_Line2))
                                        _objCreateEntity.Corp_Address_Line2 = _objentity.llpCorp_Address_Line2;
                                    if (_objentity.llpCorp_StateId > 0)
                                        _objCreateEntity.Corp_StateId = _objentity.llpCorp_StateId;
                                    if (_objentity.llpCorp_CityId > 0)
                                        _objCreateEntity.Corp_CityId = _objentity.llpCorp_CityId;
                                    if (!string.IsNullOrEmpty(_objentity.llpCorp_PINCode))
                                        _objCreateEntity.Corp_PINCode = _objentity.llpCorp_PINCode;
                                    _objCreateEntity.IsCorp_Office = _objentity.llpIscorporateforllp;
                                    _objCreateEntity.IsCorporateSameAddress = _objentity.Is_SameAddressllp;
                                }
                                else
                                {
                                    _objCreateEntity.Corp_Address_Line1 = "";
                                    _objCreateEntity.Corp_Address_Line2 = "";
                                    _objCreateEntity.Corp_StateId = 0;
                                    _objCreateEntity.Corp_CityId = 0;
                                    _objCreateEntity.Corp_PINCode = "";
                                    _objCreateEntity.IsCorp_Office = _objentity.llpIscorporateforllp;
                                    _objCreateEntity.IsCorporateSameAddress = _objentity.Is_SameAddressllp;
                                }
                                if (!string.IsNullOrEmpty(_objentity.llpEmail_Id))
                                    _objCreateEntity.Email_Id = _objentity.llpEmail_Id;
                                if (!string.IsNullOrEmpty(_objentity.llpPAN))
                                    _objCreateEntity.PAN = _objentity.llpPAN;
                                if (!string.IsNullOrEmpty(_objentity.llpWebSite))
                                    _objCreateEntity.WebSite = _objentity.llpWebSite;
                                if (!string.IsNullOrEmpty(_objentity.llpGST))
                                    _objCreateEntity.GST = _objentity.llpGST;
                                _objCreateEntity.Registration_No = "0";
                                _objCreateEntity.NIC_Discription = "";
                                _objCreateEntity.Is_Deleted = false;
                                _objCreateEntity.Customer_Id = customerId;
                                _objCreateEntity.CustomerBranchId = customerBranch.ID;
                                _objCreateEntity.Maindivisonbusiness = _objentity.llpbusinessActivityID;
                                entities.SaveChanges();
                            }
                            _objentity.Message = true;
                            _objentity.successErrorMessage = "LLP Update successfully";
                            return _objentity;
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "LLP Data Not Found";
                            return _objentity;

                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong";
                        return _objentity;
                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Something went wrong";
                    return _objentity;
                }

            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "Server Error Occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objentity;
            }
        }

        public TrustVM Createtrusts(TrustVM _objentity, int customerId,int UserId)
        {
            string Message = "";
            try
            {
                var ICSI_MODE = Convert.ToInt32(WebConfigurationManager.AppSettings["AVASEC_IS_ICSI_MODE"]);

                int? parentID = GetParentID(customerId);
                if (ICSI_MODE == 1 && serviceProviderId > 0)
                {
                    CheckCustomerBranchLimit = ICIAManagement.CheckCustomerEntityLimitByDistributor(Convert.ToInt32(parentID));
                    //CheckCustomerBranchLimit = ICIAManagement.CheckCustoemrBranchLimit(Convert.ToInt32(parentID), customerId);
                }
                if (CheckCustomerBranchLimit)
                {
                    com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                    {
                        ID = _objentity.CustomerBranchId,
                        Name = _objentity.trustEntityName,
                        //Type = Convert.ToByte(ddlType.SelectedValue),
                        //ComType = Convert.ToByte(comType),
                        AddressLine1 = _objentity.trustRegi_Address_Line1,
                        AddressLine2 = _objentity.trustRegi_Address_Line2,
                        StateID = _objentity.trustRegi_StateId,
                        CityID = _objentity.trustRegi_CityId,
                        Others = "",
                        PinCode = _objentity.trustRegi_PINCode,
                        ContactPerson = "",
                        Landline = "",
                        Mobile = "",
                        EmailID = _objentity.trustEmail_Id,
                        CustomerID = customerId,
                        //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                    };

                    customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                    if (customerBranch != null)
                    {
                        if (customerBranch.ID > 0)
                        {
                            if (_objentity.trustEntityName != null)
                            {
                                _objentity.trustEntityName = _objentity.trustEntityName.ToUpper();
                            }
                            if (_objentity.trustPAN != null)
                            {
                                _objentity.trustPAN = _objentity.trustPAN.ToUpper();
                            }
                            if (_objentity.trustGST != null)
                            {
                                _objentity.trustGST = _objentity.trustGST.ToUpper();
                            }

                            if (_objentity.trustRegi_Address_Line1 != null)
                            {
                                _objentity.trustRegi_Address_Line1 = _objentity.trustRegi_Address_Line1.ToUpper();
                            }
                            if (_objentity != null)
                            {
                                var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                        where row.Is_Deleted == false
                                                         && row.Entity_Type == _objentity.Entity_Type
                                                         && row.Registration_No == _objentity.trustRegistration_No
                                                         && row.PAN == _objentity.trustPAN
                                                         && row.Customer_Id == customerId
                                                        select row).FirstOrDefault();
                                if (_objCreateEntity == null)
                                {
                                    if (_objentity.Entity_Type == 4)
                                    {
                                        BM_EntityMaster _bmentity = new BM_EntityMaster();
                                        if (!(string.IsNullOrEmpty(_objentity.trustEntityName)))
                                            _bmentity.CompanyName = _objentity.trustEntityName;
                                        if (_objentity.Entity_Type > 0)
                                            _bmentity.Entity_Type = _objentity.Entity_Type;

                                        if (!string.IsNullOrEmpty(_objentity.trustRegistration_No))
                                            _bmentity.Registration_No = _objentity.trustRegistration_No;

                                        if (_objentity.trustRegi_Address_Line1 != null)
                                            _bmentity.Regi_Address_Line1 = _objentity.trustRegi_Address_Line1;
                                        if (!string.IsNullOrEmpty(_objentity.trustRegi_Address_Line2))
                                            _bmentity.Regi_Address_Line2 = _objentity.trustRegi_Address_Line2;
                                        if (_objentity.trustRegi_StateId > 0)
                                            _bmentity.Regi_StateId = _objentity.trustRegi_StateId;
                                        if (_objentity.trustRegi_CityId > 0)
                                            _bmentity.Regi_CityId = _objentity.trustRegi_CityId;

                                        if (!string.IsNullOrEmpty(_objentity.trustRegi_PINCode))
                                            _bmentity.Regi_PINCode = _objentity.trustRegi_PINCode;

                                        if (!(string.IsNullOrEmpty(_objentity.trustEmail_Id)))
                                            _bmentity.Email_Id = _objentity.trustEmail_Id;
                                        if (!string.IsNullOrEmpty(_objentity.trustWebSite))
                                            _bmentity.WebSite = _objentity.trustWebSite;
                                        if (!string.IsNullOrEmpty(_objentity.trustPAN))
                                            _bmentity.PAN = _objentity.trustPAN;
                                        if (_objentity.trustNICCode > 0)
                                            _bmentity.NIC_Code = _objentity.trustNICCode;
                                        if (!string.IsNullOrEmpty(_objentity.trustDescription))
                                            _bmentity.NIC_Discription = _objentity.trustDescription;
                                        if (!string.IsNullOrEmpty(_objentity.trustGST))
                                            _bmentity.GST = _objentity.trustGST;
                                        if (_objentity.trustNo_Of_Trustees > 0)
                                            _bmentity.No_Of_Trustees = _objentity.trustNo_Of_Trustees;
                                        if (_objentity.trustRegistrationDate != null)
                                            _bmentity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.trustRegistrationDate);
                                        if (_objentity.trusttType > 0)
                                            _bmentity.trust_Type = _objentity.trusttType;

                                        if (!string.IsNullOrEmpty(_objentity.TrustFY))
                                            _bmentity.FY_CY = _objentity.TrustFY;
                                        _bmentity.IsCorporateSameAddress = false;
                                        _bmentity.Is_Deleted = false;
                                        _bmentity.Customer_Id = customerId;
                                        _bmentity.CIN_LLPIN = "";
                                        _bmentity.GLN = "";
                                        _bmentity.NIC_Discription = "";
                                        _bmentity.CustomerBranchId = customerBranch.ID;
                                        _bmentity.CreatedBy = UserId;
                                        _bmentity.CreatedOn = DateTime.Now;
                                        entities.BM_EntityMaster.Add(_bmentity);
                                        entities.SaveChanges();
                                        _objentity.Id = _bmentity.Id;
                                    }
                                    _objentity.Message = true;
                                    _objentity.successErrorMessage = "Saved Successfully.";

                                }
                                else
                                {
                                    _objentity.errorMessage = true;
                                    //_objentity.successErrorMessage = "Trust Data not Found";
                                    _objentity.successErrorMessage = "Trust with same Registration_No already exists";
                                }
                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Something went wrong";

                            }
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong!Please Check Customer Branch Data";

                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong!Please Check Customer Branch Data";

                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "You are not allowed to create more entity/company, Contact Avantis to allow to create more";
                }

            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "Server Error Occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objentity;
        }

        public TrustVM Updatetrusts(TrustVM _objentity, int customerId,int userID)
        {
            try
            {
                string Message = "";
                #region Check EntityFor Avacom 
                com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                {
                    ID = _objentity.CustomerBranchId,
                    Name = _objentity.trustEntityName,
                    //Type = Convert.ToByte(ddlType.SelectedValue),
                    //ComType = Convert.ToByte(comType),
                    AddressLine1 = _objentity.trustRegi_Address_Line1,
                    AddressLine2 = _objentity.trustRegi_Address_Line2,
                    StateID = _objentity.trustRegi_StateId,
                    CityID = _objentity.trustRegi_CityId,
                    Others = "",
                    PinCode = _objentity.trustRegi_PINCode,
                    ContactPerson = "",
                    Landline = "",
                    Mobile = "",
                    EmailID = _objentity.trustEmail_Id,
                    CustomerID = customerId,
                    //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                };

                customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                #endregion

                if (customerBranch != null)
                {
                    if (customerBranch.ID > 0)
                    {
                        if (_objentity.trustEntityName != null)
                            _objentity.trustEntityName = _objentity.trustEntityName.ToUpper();
                        if (_objentity.trustPAN != null)
                            _objentity.trustPAN = _objentity.trustPAN.ToUpper();
                        if (_objentity.trustGST != null)
                            _objentity.trustGST = _objentity.trustGST.ToUpper();
                        if (_objentity.trustRegi_Address_Line1 != null)
                            _objentity.trustRegi_Address_Line1 = _objentity.trustRegi_Address_Line1.ToUpper();
                        if (_objentity != null)
                        {
                            var _objUpdateEntity = (from row in entities.BM_EntityMaster
                                                    where row.Is_Deleted == false
                                                     && row.Id == _objentity.Id
                                                    select row).FirstOrDefault();
                            if (_objUpdateEntity != null)
                            {
                                if (_objentity.Entity_Type == 4)
                                {

                                    _objUpdateEntity.CompanyName = _objentity.trustEntityName;
                                    _objUpdateEntity.Entity_Type = _objentity.Entity_Type;
                                    _objUpdateEntity.Registration_No = _objentity.trustRegistration_No;
                                    _objUpdateEntity.Regi_Address_Line1 = _objentity.trustRegi_Address_Line1;
                                    _objUpdateEntity.Regi_Address_Line2 = _objentity.trustRegi_Address_Line2;
                                    if (_objentity.trustRegi_StateId > 0)
                                        _objUpdateEntity.Regi_StateId = _objentity.trustRegi_StateId;
                                    if (_objentity.trustRegi_CityId > 0)
                                        _objUpdateEntity.Regi_CityId = _objentity.trustRegi_CityId;
                                    if (!string.IsNullOrEmpty(_objentity.trustRegi_PINCode))
                                        _objUpdateEntity.Regi_PINCode = _objentity.trustRegi_PINCode;
                                    _objUpdateEntity.Email_Id = _objentity.trustEmail_Id;
                                    _objUpdateEntity.WebSite = _objentity.trustWebSite;

                                    _objUpdateEntity.PAN = _objentity.trustPAN;
                                    if (_objentity.trustNICCode > 0)
                                        _objUpdateEntity.NIC_Code = _objentity.trustNICCode;
                                    _objUpdateEntity.NIC_Discription = _objentity.trustDescription;
                                    _objUpdateEntity.GST = _objentity.trustGST;
                                    if (_objentity.trustNo_Of_Trustees > 0)
                                        _objUpdateEntity.No_Of_Trustees = _objentity.trustNo_Of_Trustees;
                                    if (_objentity.trustRegistrationDate != null)
                                        _objUpdateEntity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.trustRegistrationDate);
                                    _objUpdateEntity.trust_Type = _objentity.trusttType;
                                    if (!string.IsNullOrEmpty(_objentity.TrustFY))
                                        _objUpdateEntity.FY_CY = _objentity.TrustFY;
                                    _objUpdateEntity.Is_Deleted = false;
                                    _objUpdateEntity.Customer_Id = customerId;
                                    _objUpdateEntity.CIN_LLPIN = "";
                                    _objUpdateEntity.GLN = "";
                                    _objUpdateEntity.NIC_Discription = "";
                                    _objUpdateEntity.CustomerBranchId = customerBranch.ID;
                                    _objUpdateEntity.IsCorporateSameAddress = false;
                                    _objUpdateEntity.UpdatedOn = DateTime.Now;
                                    _objUpdateEntity.UpdatedBy = userID;
                                    entities.SaveChanges();

                                }
                                _objentity.Message = true;
                                _objentity.successErrorMessage = "Trust update successfully";
                                return _objentity;
                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Trust already exist";
                                return _objentity;
                            }
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong";
                            return _objentity;
                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong";
                        return _objentity;
                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Something went wrong";
                    return _objentity;
                }
            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "Server Error Occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objentity;
            }
        }

        public BodyCorporateVM Createbody_Corporate(BodyCorporateVM _objentity, int customerId,int UserId)
        {
            try
            {
                var ICSI_MODE = Convert.ToInt32(WebConfigurationManager.AppSettings["AVASEC_IS_ICSI_MODE"]);

                int? parentID = GetParentID(customerId);
                if (ICSI_MODE == 1 && parentID > 0)
                {
                    CheckCustomerBranchLimit = ICIAManagement.CheckCustomerEntityLimitByDistributor(Convert.ToInt32(parentID));
                    //CheckCustomerBranchLimit = ICIAManagement.CheckCustoemrBranchLimit(Convert.ToInt32(parentID), customerId);
                }
                if (CheckCustomerBranchLimit)
                {
                    #region Create Entity 
                    string Message = "";
                    com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                    {
                        ID = _objentity.CustomerBranchId,
                        Name = _objentity.BodyCorporateEntityName,
                        //Type = Convert.ToByte(ddlType.SelectedValue),
                        //ComType = Convert.ToByte(comType),
                        AddressLine1 = _objentity.BodyCorporateRegi_Address_Line1,
                        AddressLine2 = _objentity.BodyCorporateRegi_Address_Line2,
                        //StateID = _objentity,
                        CityID = _objentity.BodyCorporateRegi_CityId,
                        Others = "",
                        PinCode = _objentity.BodyCorporateRegi_PINCode,
                        ContactPerson = "",
                        Landline = "",
                        Mobile = "",
                        EmailID = _objentity.BodyCorporateEmail_Id,
                        CustomerID = customerId,
                        //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                        //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                    };

                    customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                    if (customerBranch != null)
                    {
                        if (customerBranch.ID > 0)
                        {
                            #endregion
                            if (_objentity.BodyCorporateEntityName != null)

                                _objentity.BodyCorporateEntityName = _objentity.BodyCorporateEntityName.ToUpper();

                            if (_objentity.BodyCorporateRegi_Address_Line1 != null)

                                _objentity.BodyCorporateRegi_Address_Line1 = _objentity.BodyCorporateRegi_Address_Line1.ToUpper();

                            if (_objentity.BodyCorporateRegi_Address_Line2 != null)

                                _objentity.BodyCorporateRegi_Address_Line2 = _objentity.BodyCorporateRegi_Address_Line2.ToUpper();

                            if (_objentity != null)
                            {
                                var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                        where row.Is_Deleted == false
                                                         && row.Entity_Type == _objentity.Entity_Type
                                                         && row.Email_Id == _objentity.BodyCorporateEmail_Id
                                                         && row.Registration_No == _objentity.BodyCorporateRegistration_No
                                                         && row.Customer_Id == customerId
                                                        select row).FirstOrDefault();
                                if (_objCreateEntity == null)
                                {
                                    if (_objentity.Entity_Type == 5)
                                    {
                                        BM_EntityMaster _bmentity = new BM_EntityMaster();
                                        _bmentity.CompanyName = _objentity.BodyCorporateEntityName;
                                        if (!string.IsNullOrEmpty(_objentity.BCFY))
                                            _bmentity.FY_CY = _objentity.BCFY;
                                        _bmentity.Entity_Type = _objentity.Entity_Type;
                                        _bmentity.Registration_No = _objentity.BodyCorporateRegistration_No;
                                        _bmentity.GLN = _objentity.BodyCorporateGLN;
                                        _bmentity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.BodyCorporateDateofIncorporate);
                                        _bmentity.Regi_Address_Line1 = _objentity.BodyCorporateRegi_Address_Line1;
                                        _bmentity.Regi_Address_Line2 = _objentity.BodyCorporateRegi_Address_Line2;
                                        _bmentity.Regi_CityId = _objentity.BodyCorporateRegi_CityId;
                                        _bmentity.Regi_PINCode = _objentity.BodyCorporateRegi_PINCode;
                                        _bmentity.CountryId = _objentity.BodyCorporateRegi_CountryID;
                                        _bmentity.Email_Id = _objentity.BodyCorporateEmail_Id;
                                        _bmentity.WebSite = _objentity.BodyCorporateWebSite;
                                        _bmentity.GoverningBody = _objentity.GoverningBody;
                                        _bmentity.Gov_Body_Members = _objentity.Gov_Body_Members;
                                        _bmentity.TIN = _objentity.TIN;
                                        _bmentity.CIN_LLPIN = "";
                                        _bmentity.PAN = "";
                                        _bmentity.GST = "";
                                        _bmentity.NIC_Discription = "";
                                        _bmentity.Customer_Id = customerId;
                                        _bmentity.Is_Deleted = false;
                                        _bmentity.CustomerBranchId = customerBranch.ID;
                                        _bmentity.IsCorporateSameAddress = false;
                                        _bmentity.CreatedBy = UserId;
                                        _bmentity.CreatedOn = DateTime.Now;
                                        entities.BM_EntityMaster.Add(_bmentity);
                                        entities.SaveChanges();
                                        _objentity.Id = _bmentity.Id;
                                        _objentity.Message = true;
                                        _objentity.successErrorMessage = "Body Corporate Saved Successfully";

                                    }
                                    else
                                    {
                                        _objentity.errorMessage = true;
                                        _objentity.successErrorMessage = "Something went wrong";

                                    }

                                }
                                else
                                {
                                    _objentity.errorMessage = true;
                                    _objentity.successErrorMessage = "Boby corporate with Same Registration No already exists";

                                }
                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Something went wrong";

                            }
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong";

                        }
                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong";

                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "You are not allowed to create more entity/company, Contact Avantis to allow to create more";
                }
            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "Server Error occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objentity;
        }

        public BodyCorporateVM UpdateBody_Corporate(BodyCorporateVM _objentity, int customerId,int userId)
        {
            try
            {
                string Message = "";
                com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                {
                    ID = _objentity.CustomerBranchId,
                    Name = _objentity.BodyCorporateEntityName,
                    //Type = Convert.ToByte(ddlType.SelectedValue),
                    //ComType = Convert.ToByte(comType),
                    AddressLine1 = _objentity.BodyCorporateRegi_Address_Line1,
                    AddressLine2 = _objentity.BodyCorporateRegi_Address_Line2,
                    //StateID = _objentity,
                    CityID = _objentity.BodyCorporateRegi_CityId,
                    Others = "",
                    PinCode = _objentity.BodyCorporateRegi_PINCode,
                    ContactPerson = "",
                    Landline = "",
                    Mobile = "",
                    EmailID = _objentity.BodyCorporateEmail_Id,
                    CustomerID = customerId,
                    //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                };

                customerBranch = CreateCustomerBranch(customerBranch, _objentity.Entity_Type, false, out Message);
                if (customerBranch != null)
                {
                    if (customerBranch.ID > 0)
                    {
                        if (_objentity.BodyCorporateEntityName != null)

                            _objentity.BodyCorporateEntityName = _objentity.BodyCorporateEntityName.ToUpper();

                        if (_objentity.BodyCorporateRegi_Address_Line1 != null)

                            _objentity.BodyCorporateRegi_Address_Line1 = _objentity.BodyCorporateRegi_Address_Line1.ToUpper();

                        if (_objentity.BodyCorporateRegi_Address_Line2 != null)

                            _objentity.BodyCorporateRegi_Address_Line2 = _objentity.BodyCorporateRegi_Address_Line2.ToUpper();

                        if (_objentity != null)
                        {
                            var _objCreateEntity = (from row in entities.BM_EntityMaster
                                                    where row.Is_Deleted == false
                                                     && row.Entity_Type == _objentity.Entity_Type
                                                      && row.Id == _objentity.Id
                                                    select row).FirstOrDefault();
                            if (_objCreateEntity != null)
                            {
                                if (_objCreateEntity.Entity_Type == 5)
                                {

                                    _objCreateEntity.CompanyName = _objentity.BodyCorporateEntityName;
                                    if (!string.IsNullOrEmpty(_objentity.BCFY))
                                        _objCreateEntity.FY_CY = _objentity.BCFY;
                                    _objCreateEntity.Entity_Type = _objentity.Entity_Type;
                                    _objCreateEntity.Registration_No = _objentity.BodyCorporateRegistration_No;
                                    _objCreateEntity.GLN = _objentity.BodyCorporateGLN;
                                    _objCreateEntity.IncorporationDate = DateTimeExtensions.GetDate(_objentity.BodyCorporateDateofIncorporate);
                                    _objCreateEntity.Regi_Address_Line1 = _objentity.BodyCorporateRegi_Address_Line1;
                                    _objCreateEntity.Regi_Address_Line2 = _objentity.BodyCorporateRegi_Address_Line2;
                                    _objCreateEntity.Regi_CityId = _objentity.BodyCorporateRegi_CityId;
                                    _objCreateEntity.Regi_PINCode = _objentity.BodyCorporateRegi_PINCode;
                                    _objCreateEntity.CountryId = _objentity.BodyCorporateRegi_CountryID;
                                    _objCreateEntity.Email_Id = _objentity.BodyCorporateEmail_Id;
                                    _objCreateEntity.WebSite = _objentity.BodyCorporateWebSite;
                                    _objCreateEntity.GoverningBody = _objentity.GoverningBody;
                                    _objCreateEntity.Gov_Body_Members = _objentity.Gov_Body_Members;
                                    _objCreateEntity.TIN = _objentity.TIN;
                                    _objCreateEntity.CIN_LLPIN = "";
                                    _objCreateEntity.PAN = "";
                                    _objCreateEntity.GST = "";
                                    _objCreateEntity.NIC_Discription = "";
                                    _objCreateEntity.Customer_Id = customerId;
                                    _objCreateEntity.Is_Deleted = false;
                                    _objCreateEntity.CustomerBranchId = customerBranch.ID;
                                    _objCreateEntity.IsCorporateSameAddress = false;
                                    _objCreateEntity.UpdatedBy = userId;
                                    _objCreateEntity.UpdatedOn = DateTime.Now;
                                    entities.SaveChanges();
                                    _objentity.Message = true;
                                    _objentity.successErrorMessage = "Body Corporate update successfully";
                                    return _objentity;
                                }
                                else
                                {
                                    _objentity.errorMessage = true;
                                    _objentity.successErrorMessage = "Something went wrong";
                                    return _objentity;
                                }

                            }
                            else
                            {
                                _objentity.errorMessage = true;
                                _objentity.successErrorMessage = "Fields already exist";
                                return _objentity;
                            }
                        }
                        else
                        {
                            _objentity.errorMessage = true;
                            _objentity.successErrorMessage = "Something went wrong";
                            return _objentity;
                        }

                    }
                    else
                    {
                        _objentity.errorMessage = true;
                        _objentity.successErrorMessage = "Something went wrong";
                        return _objentity;
                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage = "Something went wrong";
                    return _objentity;
                }
            }
            catch (Exception ex)
            {
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "Server Error occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objentity;
            }
        }

        public IEnumerable<VMCountry> GetAllCountryDtls()
        {
            try
            {

                var Listofcountry = (from row in entities.Countries.OrderBy(x => x.Name)
                                     where row.IsDeleted == false
                                     select new VMCountry
                                     {
                                         CountryId = row.ID,
                                         CountryName = row.Name,
                                         Nationality = row.Nationality
                                     }).ToList();
                Listofcountry.Insert(0, new VMCountry() { CountryId = 0, CountryName="Select Country", Nationality = "Select Nationality" });
                return Listofcountry;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public Pravate_PublicVM GetprivatepublicDtls(int Id, int EntityTypeId)
        {
            try
            {
                if (Id > 0 && EntityTypeId > 0)
                {
                    var objgetentitydtls = (from row in entities.BM_EntityMaster
                                            where row.Entity_Type == EntityTypeId
                                                  && row.Id == Id
                                                  && row.Is_Deleted == false
                                            select new Pravate_PublicVM
                                            {
                                                Id = row.Id,
                                                Entity_Type = row.Entity_Type,
                                                EntityName = row.CompanyName,
                                                PAN = row.PAN,
                                                CompanyCategory_Id = row.CompanyCategory_Id,
                                                CIN = row.CIN_LLPIN,
                                                Corp_Address_Line1 = row.Corp_Address_Line1,
                                                Corp_Address_Line2 = row.Corp_Address_Line2,
                                                Corp_CityId = row.Corp_CityId,
                                                Corp_StateId = row.Corp_StateId,
                                                Corp_PINCode = row.Corp_PINCode,
                                                IsCorp_Office = row.IsCorp_Office,
                                                Email_Id = row.Email_Id,
                                                GLN = row.GLN,
                                                GST = row.GST,
                                                ISIN = row.ISIN,
                                                CompanyCategorySub_Id = row.CompanySubCategory,
                                                Description = row.NIC_Discription,
                                                IncorporationDate = SqlFunctions.DateName("day", row.IncorporationDate) + "/" + SqlFunctions.DateName("month", row.IncorporationDate) + "/" + SqlFunctions.DateName("year", row.IncorporationDate),
                                                IS_Listed = row.IS_Listed,
                                                Is_SameAddress = row.IsCorporateSameAddress,
                                                Registration_No = row.Registration_No,
                                                Regi_Address_Line1 = row.Regi_Address_Line1,
                                                Regi_Address_Line2 = row.Regi_Address_Line2,
                                                Regi_CityId = row.Regi_CityId,
                                                Regi_PINCode = row.Regi_PINCode,
                                                Regi_StateId = row.Regi_StateId,
                                                ROC_Code = row.ROC_Code,
                                                NICCode = row.NIC_Code,
                                                WebSite = row.WebSite,
                                                FY = row.FY_CY,
                                                RTA_CIN = row.RTA_CIN,
                                                RTA_Name = row.RTA_CompanyName,
                                                RTA_Address = row.RTA_Address,


                                                CustomerBranchId = row.CustomerBranchId != null ? (int)row.CustomerBranchId : 0
                                            }).FirstOrDefault();
                    //objgetentitydtls.SubCompanyDetails = new List<SubCompanyType>();

                    return objgetentitydtls;
                }
                else
                {
                    Pravate_PublicVM obj = new Pravate_PublicVM();
                    obj.errorMessage = true;
                    obj.successErrorMessage = "Something went wrong";
                    return obj;
                }
            }
            catch (Exception ex)
            {
                Pravate_PublicVM obj = new Pravate_PublicVM();
                obj.errorMessage = true;
                obj.successErrorMessage = "Server error occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public LLPVM GetLLPDtls(int Id, int EntityTypeId)
        {
            try
            {
                var objgetentitydtls = (from row in entities.BM_EntityMaster
                                        where row.Entity_Type == EntityTypeId
                                        && row.Id == Id
                                        && row.Is_Deleted == false

                                        select new LLPVM
                                        {
                                            Id = row.Id,
                                            EntityName = row.CompanyName,
                                            Entity_Type = row.Entity_Type,
                                            llpEmail_Id = row.Email_Id,
                                            llpGLN = row.GLN,
                                            llpGST = row.GST,
                                            LLPIN = row.CIN_LLPIN,
                                            llpIncorporationDate = SqlFunctions.DateName("day", row.IncorporationDate) + "/" + SqlFunctions.DateName("month", row.IncorporationDate) + "/" + SqlFunctions.DateName("year", row.IncorporationDate),
                                            llpNo_Of_DesignatedPartners = row.No_Of_DesignatedPartners,
                                            llpNo_Of_Partners = row.No_Of_Partners,
                                            llpCorp_Address_Line1 = row.Corp_Address_Line1,
                                            llpCorp_Address_Line2 = row.Corp_Address_Line2,
                                            Is_SameAddressllp = row.IsCorporateSameAddress,
                                            llpCorp_StateId = row.Corp_StateId,
                                            llpCorp_CityId = row.Corp_CityId,
                                            llpCorp_PINCode = row.Corp_PINCode,
                                            llpPAN = row.PAN,
                                            llpObligation_Of_Contribution = row.Obligation_Of_Contribution,
                                            llpRegistration_No = row.Registration_No,
                                            llpRegi_Address_Line1 = row.Regi_Address_Line1,
                                            llpRegi_Address_Line2 = row.Regi_Address_Line2,
                                            llpRegi_CityId = row.Regi_CityId,
                                            llpIscorporateforllp = row.IsCorp_Office,
                                            llpRegi_PINCode = row.Regi_PINCode,
                                            llpRegi_StateId = row.Regi_StateId,
                                            llpROC_Code = row.ROC_Code,
                                            llpWebSite = row.WebSite,
                                            llpFY = row.FY_CY,
                                            llpbusinessActivityID = row.Maindivisonbusiness,
                                            llpbusinessADesc = row.Maindivisonbusiness,
                                            CustomerBranchId = row.CustomerBranchId != 0 ? (int)row.CustomerBranchId : 0
                                        }).FirstOrDefault();

                return objgetentitydtls;

            }
            catch (Exception ex)
            {
                LLPVM obj = new LLPVM();
                obj.errorMessage = true;
                obj.successErrorMessage = "Server Error occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;

            }
        }

        public IEnumerable<VMCity> GetAllCountryCitydtlsForBodyCorporate(int? CountryId)
        {
            try
            {
                var getcityforbodyCorporate = (from row in entities.Countries.OrderBy(x => x.Name)
                                               join rows in entities.States.Where(x => x.CountryID == CountryId)
                                               on row.ID equals (rows.CountryID)
                                               join rowss in entities.Cities
                                               on rows.ID equals (rowss.StateId)
                                               select new VMCity
                                               {
                                                   Reg_cityId = rowss.ID,
                                                   Name = rowss.Name,
                                               });
                return getcityforbodyCorporate;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public TrustVM GetTustDtls(int Id, int EntityTypeId)
        {
            try
            {
                var objgetentitydtls = (from row in entities.BM_EntityMaster
                                        where row.Entity_Type == EntityTypeId
                                        && row.Id == Id
                                        && row.Is_Deleted == false

                                        select new TrustVM
                                        {
                                            Id = row.Id,
                                            trustEntityName = row.CompanyName,
                                            Entity_Type = row.Entity_Type,
                                            //trustEntity_Type=row.,
                                            trustRegistration_No = row.Registration_No,
                                            trustRegistrationDate = SqlFunctions.DateName("day", row.IncorporationDate) + "/" + SqlFunctions.DateName("month", row.IncorporationDate) + "/" + SqlFunctions.DateName("year", row.IncorporationDate),
                                            trustRegi_Address_Line1 = row.Regi_Address_Line1,
                                            trustRegi_Address_Line2 = row.Regi_Address_Line2,
                                            trustRegi_StateId = row.Regi_StateId,
                                            trustRegi_CityId = row.Regi_CityId,
                                            trustRegi_PINCode = row.Regi_PINCode,
                                            trustNo_Of_Trustees = row.No_Of_Trustees,
                                            trustEmail_Id = row.Email_Id,
                                            trustWebSite = row.WebSite,
                                            trustPAN = row.PAN,
                                            trustGST = row.GST,
                                            trusttType = (int)row.trust_Type,
                                            TrustFY = row.FY_CY,
                                            CustomerBranchId = row.CustomerBranchId != null ? (int)row.CustomerBranchId : 0//Change by Ruchi on Dec 02 2019
                                        }).FirstOrDefault();

                return objgetentitydtls;
            }
            catch (Exception ex)
            {
                TrustVM obj = new TrustVM();
                obj.errorMessage = true;
                obj.successErrorMessage = "Server error occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public BodyCorporateVM GetBodyCorporate(int Id, int entityTypeId)
        {
            try
            {
                var objgetentitydtls = (from row in entities.BM_EntityMaster
                                        where row.Entity_Type == entityTypeId
                                        && row.Id == Id
                                        && row.Is_Deleted == false
                                        select new BodyCorporateVM
                                        {
                                            Id = row.Id,
                                            BodyCorporateEntityName = row.CompanyName,
                                            Entity_Type = row.Entity_Type,
                                            BodyCorporateRegistration_No = row.Registration_No,
                                            BodyCorporateGLN = row.GLN,
                                            BodyCorporateDateofIncorporate = SqlFunctions.DateName("day", row.IncorporationDate) + "/" + SqlFunctions.DateName("month", row.IncorporationDate) + "/" + SqlFunctions.DateName("year", row.IncorporationDate),
                                            BodyCorporateRegi_Address_Line1 = row.Regi_Address_Line1,
                                            BodyCorporateRegi_Address_Line2 = row.Regi_Address_Line2,
                                            BodyCorporateRegi_CountryID = (int)row.CountryId,
                                            BodyCorporateRegi_CityId = row.Regi_CityId,
                                            BodyCorporateRegi_PINCode = row.Regi_PINCode,
                                            BodyCorporatRegulatoryJurisdiction = row.RegulatoryJurisdiction,
                                            BodyCorporateEmail_Id = row.Email_Id,
                                            BodyCorporateWebSite = row.WebSite,
                                            TIN = row.TIN,
                                            GoverningBody = row.GoverningBody,
                                            Gov_Body_Members = row.Gov_Body_Members,
                                            BCFY = row.FY_CY,
                                            CustomerBranchId = row.CustomerBranchId > 0 ? (int)row.CustomerBranchId : 0
                                        }).FirstOrDefault();
                //obj.Message = true;
                return objgetentitydtls;
            }
            catch (Exception ex)
            {
                BodyCorporateVM obj = new BodyCorporateVM();
                obj.errorMessage = true;
                obj.successErrorMessage = "Server error occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        #region Applicability

        public decimal GetCapital(int Entity_Id, int customerId)
        {
            decimal result = 0;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var PreferenceShare = entities.BM_CapitalMaster.Where(k => k.Entity_Id == Entity_Id && k.CustomerId == customerId && k.IsActive == true).Select(k => k.TotamtNoPaidupCapital + k.Pri_TotamtNoPaidupCapital).FirstOrDefault();
                    var getApplicabilitydetails = entities.BM_EntityMaster.Where(k => k.Id == Entity_Id && k.Customer_Id == customerId && k.Is_Deleted == false).FirstOrDefault();
                    if (getApplicabilitydetails != null)
                    {
                        if (getApplicabilitydetails.AmountInVariable != null)
                        {
                            if (getApplicabilitydetails.AmountInVariable == "L")
                            {
                                result = Math.Round(PreferenceShare / 100000, 4);
                            }
                            else
                            {
                                result = Math.Round(PreferenceShare / 10000000, 4);
                            }
                        }
                        else
                        {
                            result = Math.Round(PreferenceShare / 10000000, 4);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public BM_Rule GetBM_Applicability(int Entity_Id, decimal PreferenceShare)
        {
            var result = new BM_Rule() { Entity_Id = Entity_Id };
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                result = (from row in entities.BM_EntityMaster
                          where row.Id == Entity_Id
                          select new BM_Rule
                          {
                              Entity_Id = row.Id,
                              Entity_Type = row.Entity_Type,
                              PaidupShareCapital = PreferenceShare, //row.PaidupCapital,
                              Turnover = row.Turnover,
                              NetWorth = row.NetWorth,
                              NetProfit = row.NetProfit,
                              Borrowings = row.Borrowings,
                              Deposit = row.DepositFromMember,
                              ShareHolders = row.ShareHolders,
                              AmountIn = row.AmountIn,
                              AmountInvariable = row.AmountInVariable,
                          }).FirstOrDefault();

                if (result != null)
                {
                    if (result.AmountInvariable != null)
                    {
                        if (result.AmountInvariable == "L")
                        {
                            result.NetProfit = Math.Round(Convert.ToDecimal(result.NetProfit * Convert.ToDecimal(100)), 2);
                            result.Turnover = Math.Round(Convert.ToDecimal(result.Turnover * Convert.ToDecimal(100)), 2);
                            result.NetWorth = Math.Round(Convert.ToDecimal(result.NetWorth * Convert.ToDecimal(100)), 2);
                            result.Borrowings = Math.Round(Convert.ToDecimal(result.Borrowings * Convert.ToDecimal(100)), 2);
                            result.Deposit = Math.Round(Convert.ToDecimal(result.Deposit * Convert.ToDecimal(100)), 2);
                        }
                        else if (result.AmountInvariable == "T")
                        {
                            result.NetProfit = Math.Round(Convert.ToDecimal(result.NetProfit * Convert.ToDecimal(10000)), 2);
                            result.Turnover = Math.Round(Convert.ToDecimal(result.Turnover * Convert.ToDecimal(10000)), 2);
                            result.NetWorth = Math.Round(Convert.ToDecimal(result.NetWorth * Convert.ToDecimal(10000)), 2);
                            result.Borrowings = Math.Round(Convert.ToDecimal(result.Borrowings * Convert.ToDecimal(10000)), 2);
                            result.Deposit = Math.Round(Convert.ToDecimal(result.Deposit * Convert.ToDecimal(10000)), 2);
                        }
                    }
                }
            }

            return GetBM_Applicability(result);
        }

        public BM_Rule GetBM_Applicability(BM_Rule objBM_Rule)
        {
            double Crore = 0.01;
            double thousand = 0.0001;

            BM_Rule obj = new BM_Rule();
            obj.Rules = new List<VM.Rules>();

            obj.Entity_Id = objBM_Rule.Entity_Id;
            obj.Entity_Type = objBM_Rule.Entity_Type;
            if (objBM_Rule.PaidupShareCapital > 0)
            {
                obj.PaidupShareCapital = Math.Round((Convert.ToDecimal(objBM_Rule.PaidupShareCapital)), 2);
            }
            if (objBM_Rule.Turnover > 0)
            {
                obj.Turnover = Math.Round((Convert.ToDecimal(objBM_Rule.Turnover)), 2);
            }
            obj.Turnover_rule = objBM_Rule.Turnover;
            if (objBM_Rule.NetWorth > 0)
            {
                obj.NetWorth = Math.Round((Convert.ToDecimal(objBM_Rule.NetWorth)), 2);
            }

            obj.NetWorth_rule = objBM_Rule.NetWorth;
            if (objBM_Rule.NetProfit > 0)
            {
                obj.NetProfit = Math.Round((Convert.ToDecimal(objBM_Rule.NetProfit)), 2);
            }
            obj.NetProfit_rule = objBM_Rule.NetProfit;
            if (objBM_Rule.Borrowings > 0)
            {
                obj.Borrowings = Math.Round((Convert.ToDecimal(objBM_Rule.Borrowings)), 2);
            }

            obj.Borrowings_rule = objBM_Rule.Borrowings;

            obj.ShareHolders = objBM_Rule.ShareHolders;

            obj.ShareHolders_rule = objBM_Rule.ShareHolders;
            if (objBM_Rule.Deposit > 0)
            {
                obj.Deposit = Math.Round((Convert.ToDecimal(objBM_Rule.Deposit)), 2);
            }
            obj.Deposit_rule = objBM_Rule.Deposit_rule;
            obj.AmountInvariable = objBM_Rule.AmountInvariable;
            if (obj.AmountInvariable == "L")
            {
                obj.AmountIn = 100000;
                obj.NetProfit_rule = obj.NetProfit_rule * Convert.ToDecimal(Crore);
                obj.Turnover_rule = obj.Turnover_rule * Convert.ToDecimal(Crore);
                obj.NetWorth_rule = obj.NetWorth_rule * Convert.ToDecimal(Crore);
                obj.Borrowings_rule = obj.Borrowings_rule * Convert.ToDecimal(Crore);
                obj.ShareHolders_rule = obj.ShareHolders_rule * Convert.ToDecimal(Crore);
                obj.Deposit_rule = obj.Deposit_rule * Convert.ToDecimal(Crore);
            }
            else if (obj.AmountInvariable == "T")
            {
                obj.AmountIn = 1000;
                obj.NetProfit_rule = obj.NetProfit_rule * Convert.ToDecimal(thousand);
                obj.Turnover_rule = obj.Turnover_rule * Convert.ToDecimal(thousand);
                obj.NetWorth_rule = obj.NetWorth_rule * Convert.ToDecimal(thousand);
                obj.Borrowings_rule = obj.Borrowings_rule * Convert.ToDecimal(thousand);
                obj.ShareHolders_rule = obj.ShareHolders_rule * Convert.ToDecimal(thousand);
                obj.Deposit_rule = obj.Deposit_rule * Convert.ToDecimal(thousand);
            }
            else
            {
                obj.AmountIn = 10000000;
            }
            var PreferenceShare = entities.BM_CapitalMaster.Where(k => k.Entity_Id == obj.Entity_Id && k.IsActive == true).Select(k => k.TotamtNoPaidupCapital + k.Pri_TotamtNoPaidupCapital).FirstOrDefault();
            if (obj.AmountIn > 0)
            {
                obj.PaidupShareCapital = Math.Round((PreferenceShare / Convert.ToDecimal(obj.AmountIn)), 2);
            }
            else
            {

                obj.PaidupShareCapital = Math.Round((PreferenceShare / 10000000), 2);
            }

            obj.PaidupShareCapital_rule = Math.Round((PreferenceShare / 10000000), 2);

            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = (from row in entities.BM_Applicability
                              join rows in entities.BM_Applicability_Entity on row.Id equals rows.ApplicabilityId into app_entity
                              from r in app_entity.Where(k => k.EntityId == objBM_Rule.Entity_Id).DefaultIfEmpty()
                                  //where objBM_Rule.Entity_Type == 1 ? row.Entity_Private == true : row.Entity_Public == true
                              where objBM_Rule.Entity_Type == 1 ? row.Entity_Private == true : objBM_Rule.Entity_Type == 2 ? row.Entity_Public == true : objBM_Rule.Entity_Type == 10 ? row.Entity_Listed == true : 1 == 2
                              select new BM_ApplicabilityVM
                              {
                                  SrNo = r.Id,
                                  Id = row.Id,
                                  Applicability = row.Applicability,
                                  PaidupCapital = row.PaidupCapital,
                                  ApplicabilityValue = r.ApplicabilityValue == null ? false : (r.ApplicabilityValue == true ? true : false),
                                  Turnover = row.Turnover,
                                  NetWorth = row.NetWorth,
                                  NetProfit = row.NetProfit,
                                  Borrowings = row.Borrowings,
                                  DepositFromMember = row.DepositFromMember,
                                  ShareHolders = row.ShareHolders,
                                  Formula = objBM_Rule.Entity_Type == 10 ? row.FormulaListed : row.Formula,
                                  AmountIn = objBM_Rule.AmountIn
                              }).ToList();

                foreach (var item in result)
                {
                    bool prevFlag = false, flag = false;
                    //bool defaultValue = objBM_Rule != null ? (objBM_Rule.Rules !=null ? (objBM_Rule.Rules.Where(k=>k.ApplicabilityId == item.Id).Select(k => k.Answer).FirstOrDefault()) : false): false;
                    bool defaultValue = objBM_Rule != null ? (objBM_Rule.Rules != null ? (objBM_Rule.Rules.Where(k => k.ApplicabilityId == item.Id).Select(k => k.Answer).FirstOrDefault()) : item.ApplicabilityValue) : item.ApplicabilityValue;

                    if (!string.IsNullOrEmpty(item.Formula))
                    {
                        int i = 0;
                        string prevOperator = "";
                        #region Conditions
                        foreach (var condition in item.Formula.Trim(',').Split(','))
                        {
                            switch (condition)
                            {
                                case "C":
                                    flag = obj.PaidupShareCapital_rule >= item.PaidupCapital ? true : false;
                                    break;
                                case "T":
                                    flag = obj.Turnover_rule >= item.Turnover ? true : false;
                                    break;
                                case "W":
                                    flag = obj.NetWorth_rule >= item.NetWorth ? true : false;
                                    break;
                                case "P":
                                    flag = obj.NetProfit_rule >= item.NetProfit ? true : false;
                                    break;
                                case "B":
                                    flag = obj.Borrowings_rule >= item.Borrowings ? true : false;
                                    break;
                                case "D":
                                    flag = obj.Deposit_rule >= item.DepositFromMember ? true : false;
                                    break;
                                case "H":
                                    flag = obj.ShareHolders_rule >= item.ShareHolders ? true : false;
                                    break;
                                case "&":
                                    prevOperator = "&";
                                    //prevFlag = prevFlag && flag;
                                    break;
                                case "|":
                                    prevOperator = "|";
                                    //prevFlag = prevFlag || flag;
                                    break;
                                default:
                                    break;
                            }

                            if (i == 0)
                            {
                                prevFlag = flag;
                                i++;
                            }

                            if (condition == "|" || condition == "&")
                                continue;

                            switch (prevOperator)
                            {
                                case "&":
                                    prevFlag = prevFlag && flag;
                                    prevOperator = "";
                                    break;
                                case "|":
                                    prevFlag = prevFlag || flag;
                                    prevOperator = "";
                                    break;
                                default:
                                    break;
                            }

                        }
                        #endregion

                        obj.Rules.Add(new Rules() { SrNo = item.SrNo, ApplicabilityId = item.Id, Name = item.Applicability, Editable = !prevFlag, Answer = prevFlag ? true : defaultValue });
                    }
                    else
                    {
                        if (item.Formula == string.Empty)
                        {
                            obj.Rules.Add(new Rules() { SrNo = item.SrNo, ApplicabilityId = item.Id, Name = item.Applicability, Editable = false, Answer = true });
                        }
                        else
                        {
                            obj.Rules.Add(new Rules() { SrNo = item.SrNo, ApplicabilityId = item.Id, Name = item.Applicability, Editable = true, Answer = defaultValue });
                        }
                    }

                }
            }

            return obj;
        }

        public bool UpdateBM_Applicability(BM_Rule objBM_Rule)
        {
            try
            {
                double Crore = 0.01;
                double thousand = 0.0001;
                BM_EntityMaster objBM_EntityMaster = new BM_EntityMaster();
                objBM_EntityMaster.Id = objBM_Rule.Entity_Id;

                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    // entities.BM_EntityMaster.Attach(objBM_EntityMaster);
                    objBM_EntityMaster = entities.BM_EntityMaster.Where(k => k.Id == objBM_Rule.Entity_Id && k.Is_Deleted == false).FirstOrDefault();

                    //Find(objBM_Rule.Entity_Id);

                    objBM_EntityMaster.Turnover = objBM_Rule.Turnover;
                    objBM_EntityMaster.NetWorth = objBM_Rule.NetWorth;
                    objBM_EntityMaster.NetProfit = objBM_Rule.NetProfit;
                    objBM_EntityMaster.Borrowings = objBM_Rule.Borrowings;
                    objBM_EntityMaster.DepositFromMember = objBM_Rule.Deposit;
                    objBM_EntityMaster.AmountInVariable = objBM_Rule.AmountInvariable;
                    if (objBM_Rule.AmountInvariable == "L")
                    {
                        objBM_EntityMaster.AmountIn = 100000;

                        objBM_EntityMaster.NetProfit = objBM_EntityMaster.NetProfit * Convert.ToDecimal(Crore);
                        objBM_EntityMaster.Turnover = objBM_EntityMaster.Turnover * Convert.ToDecimal(Crore);
                        objBM_EntityMaster.NetWorth = objBM_EntityMaster.NetWorth * Convert.ToDecimal(Crore);
                        objBM_EntityMaster.Borrowings = objBM_EntityMaster.Borrowings * Convert.ToDecimal(Crore);
                        objBM_EntityMaster.DepositFromMember = objBM_EntityMaster.DepositFromMember * Convert.ToDecimal(Crore);


                    }
                    else if(objBM_Rule.AmountInvariable == "T")
                    {
                        objBM_EntityMaster.AmountIn = 1000;

                        objBM_EntityMaster.NetProfit = objBM_EntityMaster.NetProfit * Convert.ToDecimal(thousand);
                        objBM_EntityMaster.Turnover = objBM_EntityMaster.Turnover * Convert.ToDecimal(thousand);
                        objBM_EntityMaster.NetWorth = objBM_EntityMaster.NetWorth * Convert.ToDecimal(thousand);
                        objBM_EntityMaster.Borrowings = objBM_EntityMaster.Borrowings * Convert.ToDecimal(thousand);
                        objBM_EntityMaster.DepositFromMember = objBM_EntityMaster.DepositFromMember * Convert.ToDecimal(thousand);
                    }
                    else
                    {
                        objBM_EntityMaster.AmountIn = 10000000;
                        //objBM_EntityMaster.NetProfit = objBM_EntityMaster.NetProfit * Convert.ToDecimal(100);
                        //objBM_EntityMaster.Turnover = objBM_EntityMaster.Turnover * Convert.ToDecimal(100);
                        //objBM_EntityMaster.NetWorth = objBM_EntityMaster.NetWorth * Convert.ToDecimal(100);
                        //objBM_EntityMaster.Borrowings = objBM_EntityMaster.Borrowings * Convert.ToDecimal(100);
                        //objBM_EntityMaster.DepositFromMember = objBM_EntityMaster.DepositFromMember * Convert.ToDecimal(100);
                    }


                    //entities.Entry(objBM_EntityMaster).Property(K => K.Turnover).IsModified = true;
                    //entities.Entry(objBM_EntityMaster).Property(K => K.NetWorth).IsModified = true;
                    //entities.Entry(objBM_EntityMaster).Property(K => K.NetProfit).IsModified = true;
                    //entities.Entry(objBM_EntityMaster).Property(K => K.Borrowings).IsModified = true;
                    //entities.Entry(objBM_EntityMaster).Property(K => K.DepositFromMember).IsModified = true;

                    entities.SaveChanges();

                    entities.BM_Applicability_Entity.Where(k => k.EntityId == objBM_Rule.Entity_Id).ToList().ForEach(k => entities.BM_Applicability_Entity.Remove(k));
                    entities.SaveChanges();

                    foreach (var item in objBM_Rule.Rules)
                    {
                        var data = new BM_Applicability_Entity() { EntityId = objBM_Rule.Entity_Id, ApplicabilityId = item.ApplicabilityId, ApplicabilityValue = item.Answer };
                        if (item.SrNo <= 0)
                        {
                            entities.Entry(data).State = System.Data.Entity.EntityState.Modified;
                            entities.SaveChanges();
                        }
                        else
                        {
                            data.Id = Convert.ToInt64(item.SrNo);
                            entities.BM_Applicability_Entity.Add(data); // new BM_Applicability_Entity() {EntityId= objBM_Rule.Entity_Id, ApplicabilityId = item.Id, ApplicabilityValue = item.Answer  });
                            entities.SaveChanges();
                        }

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        public bool DeleteEntityInfo(int Id, int customerId)
        {
            try
            {
                if (Id > 0)
                {
                    var _objEntityDlt = (from row in entities.BM_EntityMaster
                                         where row.Id == Id && row.Is_Deleted == false
                                         && row.Customer_Id == customerId
                                         select row).FirstOrDefault();
                    if (_objEntityDlt != null)
                    {
                        _objEntityDlt.Is_Deleted = true;
                        _objEntityDlt.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        //obj.Message = true;
                        //obj.successErrorMessage = "Deleted Successfully";
                        return true;
                    }
                    else
                    {
                        //obj.errorMessage = true;
                        //obj.successErrorMessage = "Some error Occure in Deleting Fields";
                        return false;

                    }
                }
                else
                {
                    //obj.errorMessage = true;
                    //obj.successErrorMessage = "Some error Occure in Deleting Fields";
                    return false;
                }
            }
            catch (Exception ex)
            {
                //obj.successErrorMessage = "Some error Occure in Deleting Fields";
                //obj.errorMessage = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public EntityMaster GetEntityMaster(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<VMState> GetStateListforMaster(int countryId)
        {
            try
            {

                var Listofstate = (from row in entities.States.OrderBy(x => x.Name)
                                   where row.CountryID == countryId
                                   select new VMState
                                   {
                                       SIdPublic = row.ID,
                                       Name = row.Name
                                   }).ToList();
                if (Listofstate == null)
                {
                    Listofstate = new List<VMState>();
                }

                Listofstate.Insert(0, new VMState() { SIdPublic = 0, Name = "Select State" });
                return Listofstate;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public IEnumerable<BM_Nationality> GetNationality()
        {
            try
            {

                var ListofNationality = (from row in entities.BM_Nationality.OrderBy(x => x.Nationality)
                                         where row.IsDeleted == false
                                         select row).ToList();

                return ListofNationality;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public bool CreateEntityFromLegal(CustomerBranch obj)
        {
            var result = false;

            try
            {
                BM_EntityMaster _obj = new BM_EntityMaster();

                _obj.CompanyName = obj.Name.ToUpper();
                _obj.Entity_Type = 0;
                _obj.CIN_LLPIN = "0";
                _obj.IncorporationDate = DateTime.Now;
                _obj.ROC_Code = 0;

                _obj.Regi_Address_Line1 = obj.AddressLine1.ToUpper();
                _obj.Regi_Address_Line2 = obj.AddressLine2.ToUpper();
                _obj.Regi_StateId = obj.StateID;
                _obj.Regi_CityId = obj.CityID;
                _obj.IsCorp_Office = false;
                _obj.CompanyCategory_Id = 0;

                _obj.IS_Listed = false;
                _obj.Email_Id = obj.EmailID;
                _obj.PAN = "";

                _obj.Customer_Id = obj.CustomerID;
                _obj.CustomerBranchId = obj.ID;

                _obj.Is_Deleted = false;
                _obj.CreatedOn = DateTime.Now;

                _obj.Entity_Type = 0;

                #region set Entity Type from Company Type

                if (obj.ComType == 2)
                {
                    _obj.Entity_Type = 1;
                }
                else if (obj.ComType == 1)
                {
                    _obj.Entity_Type = 2;
                }
                else if (obj.ComType == 3)
                {
                    _obj.Entity_Type = 2;
                    _obj.IS_Listed = true;
                }

                #endregion

                #region set Entity Type from Legal entity Type
                if (obj.LegalEntityTypeID == 6)//Private Limited Company
                {
                    _obj.Entity_Type = 1;
                }
                else if (obj.LegalEntityTypeID == 4)  //Limited Liability Partnership(LLP)
                {
                    _obj.Entity_Type = 3;
                }
                else if (obj.LegalEntityTypeID == 2)  //Public Limited Company (Listed)
                {
                    _obj.Entity_Type = 10;
                    _obj.IS_Listed = true;
                }
                else if (obj.LegalEntityTypeID == 3)  //Public Limited Company (Unlisted)
                {
                    _obj.Entity_Type = 2;
                    _obj.IS_Listed = false;
                }
                else if (obj.LegalEntityTypeID == 1)//Partnership
                {
                    _obj.Entity_Type = 7;
                }
                else if (obj.LegalEntityTypeID == 5)//Proprietorship
                {
                    _obj.Entity_Type = 6;
                }
                else if (obj.LegalEntityTypeID == 8)//One Person Company (O.P.C.)
                {
                    _obj.Entity_Type = 8;
                }
                #endregion

                if (_obj.Entity_Type > 0)
                {
                    entities.BM_EntityMaster.Add(_obj);
                    entities.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public bool UpdateEntityFromLegal(CustomerBranch obj)
        {
            var result = false;

            try
            {
                BM_EntityMaster _obj = entities.BM_EntityMaster.Where(k => k.CustomerBranchId == obj.ID && k.Customer_Id == obj.CustomerID && k.Is_Deleted == false).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.CompanyName = obj.Name.ToUpper();
                    _obj.Entity_Type = 0;
                    _obj.IncorporationDate = DateTime.Now;

                    _obj.Regi_Address_Line1 = obj.AddressLine1.ToUpper();
                    _obj.Regi_Address_Line2 = obj.AddressLine2.ToUpper();
                    _obj.Regi_StateId = obj.StateID;
                    _obj.Regi_CityId = obj.CityID;
                    _obj.Email_Id = obj.EmailID;

                    _obj.Is_Deleted = false;
                    _obj.UpdatedOn = DateTime.Now;

                    _obj.Entity_Type = 0;

                    #region set Entity Type from Company Type

                    if (obj.ComType == 2)
                    {
                        _obj.Entity_Type = 1;
                    }
                    else if (obj.ComType == 1)
                    {
                        _obj.Entity_Type = 2;
                    }
                    else if (obj.ComType == 3)
                    {
                        _obj.Entity_Type = 2;
                        _obj.IS_Listed = true;
                    }

                    #endregion

                    #region set Entity Type from Legal entity Type
                    if (obj.LegalEntityTypeID == 6)//Private Limited Company
                    {
                        _obj.Entity_Type = 1;
                    }
                    else if (obj.LegalEntityTypeID == 4)  //Limited Liability Partnership(LLP)
                    {
                        _obj.Entity_Type = 3;
                    }
                    else if (obj.LegalEntityTypeID == 2)  //Public Limited Company (Listed)
                    {
                        _obj.Entity_Type = 10;
                        _obj.IS_Listed = true;
                    }
                    else if (obj.LegalEntityTypeID == 3)  //Public Limited Company (Unlisted)
                    {
                        _obj.Entity_Type = 2;
                        _obj.IS_Listed = false;
                    }
                    else if (obj.LegalEntityTypeID == 1)//Partnership
                    {
                        _obj.Entity_Type = 7;
                    }
                    else if (obj.LegalEntityTypeID == 5)//Proprietorship
                    {
                        _obj.Entity_Type = 6;
                    }
                    else if (obj.LegalEntityTypeID == 8)//One Person Company (O.P.C.)
                    {
                        _obj.Entity_Type = 8;
                    }
                    #endregion

                    if (_obj.Entity_Type > 0)
                    {
                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                        result = true;
                    }
                }

            }
            catch (Exception ex)
            {
                result = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public string GetFY(long entityId)
        {
            try
            {
                var getFyEntitywise = (from row in entities.BM_EntityMaster where row.Id == entityId && row.Is_Deleted == false select row.FY_CY).FirstOrDefault();
                return getFyEntitywise;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VM_QuestionForShareholding SaveSharesDetails(VM_QuestionForShareholding objsharesholder)
        {
            try
            {
                var savequestionsrelatedShares = (from row in entities.BM_EntityMaster where row.Id == objsharesholder.EntityId select row).FirstOrDefault();
                if (savequestionsrelatedShares != null)
                {
                    savequestionsrelatedShares.IsDematerialisedForm = objsharesholder.rdDematerialised;
                    savequestionsrelatedShares.SharesIsComplitedorPatial = objsharesholder.rdIsdemat;
                    entities.SaveChanges();

                }
                return objsharesholder;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return objsharesholder;
            }
        }

        #region Listed stockExchange
        public IEnumerable<ListofstockExchange> readListedStockExchangeData(int entityID)
        {
            try
            {
                return GetAllstockExchangerelatedvalue(entityID);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        private IEnumerable<ListofstockExchange> GetAllstockExchangerelatedvalue(int entityID)
        {
            try
            {
                var getAllstockexchangeMappingDetails = (from row in entities.BM_EntityStockMapping
                                                         where row.EntityId == entityID && row.IsDeleted == false
                                                         select new ListofstockExchange
                                                         {
                                                             EntityId = entityID,
                                                             Id = row.Id,
                                                             staockExchange = row.StockExchangeId,
                                                             scriptCode = row.ScriptCode,
                                                             scriptSymbol = row.ScriptSymbol,
                                                             stockExchangeName = (from st in entities.BM_StockExchange where st.Id == row.StockExchangeId select st.Name).FirstOrDefault()

                                                         }
                                                         ).ToList();
                return getAllstockexchangeMappingDetails;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        public void Create(ListofstockExchange objlistofstockexc)
        {
            try
            {
                var IsExist = (from row in entities.BM_EntityStockMapping where row.StockExchangeId == objlistofstockexc.staockExchange && row.ScriptCode == objlistofstockexc.scriptCode select row).FirstOrDefault();
                if (IsExist == null)
                {
                    BM_EntityStockMapping obj = new BM_EntityStockMapping();
                    obj.StockExchangeId = objlistofstockexc.staockExchange;
                    obj.EntityId = objlistofstockexc.EntityId;
                    obj.ScriptCode = objlistofstockexc.scriptCode;
                    obj.ScriptSymbol = objlistofstockexc.scriptSymbol;
                    obj.IsDeleted = false;
                    entities.BM_EntityStockMapping.Add(obj);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public void Update(ListofstockExchange objlistofstockexc)
        {
            try
            {
                var IsExist = (from row in entities.BM_EntityStockMapping where row.Id == objlistofstockexc.Id select row).FirstOrDefault();
                if (IsExist != null)
                {

                    IsExist.StockExchangeId = objlistofstockexc.staockExchange;
                    IsExist.ScriptCode = objlistofstockexc.scriptCode;
                    IsExist.ScriptSymbol = objlistofstockexc.scriptSymbol;
                    IsExist.EntityId = objlistofstockexc.EntityId;
                    IsExist.IsDeleted = false;

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void Destroy(ListofstockExchange objlistofstockexc)
        {
            try
            {
                var IsExist = (from row in entities.BM_EntityStockMapping where row.Id == objlistofstockexc.Id select row).FirstOrDefault();
                if (IsExist != null)
                {


                    IsExist.IsDeleted = true;

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        #region File Uploading
        public int getCountryId(string country)
        {
            try
            {

                var getCountryId = (from row in entities.Countries
                                    where row.Name.ToLower() == country.ToLower()
                                    && row.IsDeleted == false
                                    select row.ID).FirstOrDefault();
                return getCountryId;
            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public int getCityId(string regi_City)
        {
            try
            {

                var _objcity = (from row in entities.Cities
                                where row.Name.ToLower() == regi_City.ToLower()
                                && row.IsDeleted == false
                                select row.ID).FirstOrDefault();
                return _objcity;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public bool CheckSaveorUpdateforEntity(BM_EntityMaster objentity)
        {
            try
            {

                var checkEntity_Data = (from row in entities.BM_EntityMaster
                                        where row.CIN_LLPIN == objentity.CIN_LLPIN
                                        && row.CompanyName == objentity.CompanyName
                                        && row.Is_Deleted == false
                                        select row).FirstOrDefault();
                if (checkEntity_Data != null)
                {

                    objentity.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
                else
                {
                    if (objentity.Entity_Type == 4 || objentity.Entity_Type == 5)
                    {
                        objentity.CIN_LLPIN = "0";
                    }
                    objentity.Is_Deleted = false;
                    objentity.CreatedOn = DateTime.Now;
                    entities.BM_EntityMaster.Add(objentity);
                    entities.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public int getStateId(string regi_State)
        {
            try
            {

                var _objstateID = (from row in entities.States
                                   where row.Name.ToLower() == regi_State.ToLower()
                                   && row.IsDeleted == false
                                   select row.ID).FirstOrDefault();
                return _objstateID;
            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public int GetRocCode(string ROC_Code)
        {
            try
            {

                var getRocData = (from row in entities.BM_ROC_code
                                  where row.Name.ToLower() == ROC_Code.ToLower()
                                  && row.isDeleted == false
                                  select row.Id).FirstOrDefault();
                return getRocData;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public int GetCompanyCategory(string companyCategory)
        {
            try
            {

                var _objgetcompanyCategoryId = (from row in entities.BM_CompanyCategory
                                                where row.Category == companyCategory
                                                select row.Id).FirstOrDefault();
                return _objgetcompanyCategoryId;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public VMEntityFileUpload ImportDatafrom_MGT7(VMEntityFileUpload objfileupload)
        {
            objfileupload.Response = new Response();
            try
            {
                var ICSI_MODE = Convert.ToInt32(WebConfigurationManager.AppSettings["AVASEC_IS_ICSI_MODE"]);

                int? parentID = GetParentID(objfileupload.CustomerId);
                if (ICSI_MODE == 1 && parentID > 0)
                {
                    CheckCustomerBranchLimit = ICIAManagement.CheckCustomerEntityLimitByDistributor(Convert.ToInt32(parentID));
                    //CheckCustomerBranchLimit = ICIAManagement.CheckCustoemrBranchLimit(Convert.ToInt32(parentID), objfileupload.CustomerId);
                }
                if (CheckCustomerBranchLimit)
                {

                    #region variable define for Mgt-7 Read for Entity
                    long EntityID = -1;
                    string Message = "";
                    string EntityName = string.Empty;
                    string CIN = string.Empty;
                    bool IsListed = false;
                    string PAN = string.Empty;
                    string GLN = string.Empty;
                    string RegisterAddress = string.Empty;
                    string RegisterAddressLine1 = string.Empty;
                    string RegisterAddressLine2 = string.Empty;
                    int stateId = -1;
                    int CityId = -1;
                    string PIN = string.Empty;
                    string EmilId = string.Empty;
                    long PhoneNo = 0;
                    DateTime dateofIncorporate = DateTime.Now;
                    string Website = string.Empty;
                    string TypeofCompany = string.Empty;
                    int Type = -1;
                    string CategoryofCompany = string.Empty;
                    string CategoryofCompanySub = string.Empty;
                    int CompanyCategoryType = -1;
                    int CompanyCategorysubType = -1;
                    string RegistrationNumber = string.Empty;
                    string FY = string.Empty;
                    #region Company is Listed
                    string RTA_CIN = string.Empty;
                    string RTA_Name = string.Empty;
                    string RTA_Address = string.Empty;
                    int noofsubcompany = -1;

                    List<SubCompanyType> objlistofsubcompany = new List<SubCompanyType>();
                    #endregion Company is Listed


                    #endregion

                    #region variable define for Mgt7 for Capital Master
                    string IsCapitalShares = string.Empty;
                    decimal TotNoofEquAuthorized = 0;
                    decimal TotNoofEquIssuedAuthorized = 0;
                    decimal TotNoofEquSubscribed = 0;
                    decimal TotNoofEqupaidUp = 0;

                    decimal TotAmtofEquAuthorized = 0;
                    decimal TotAmtofEquIssued = 0;
                    decimal TotAmtofEquSubscribe = 0;
                    decimal TotAmtofEquPaidup = 0;

                    int NoofClass = 1;
                    long NoofEquAuthorized = 0;
                    long NoofEquIssued = 0;
                    long NoofSubscribed = 0;
                    long NoofPaidup = 0;

                    long NomvalperSahreEquAuthorized = 0;
                    long NomvalperSahreEquIssued = 0;
                    long NomvalperSahreEquSubscribed = 0;
                    long NomvalperSahreEquPaidup = 0;
                    int j = 0;
                    #endregion


                    if (objfileupload.File != null)
                    {
                        if (objfileupload.File.ContentLength > 0)
                        {
                            string myFilePath = objfileupload.File.FileName;
                            string ext = System.IO.Path.GetExtension(myFilePath);
                            if (ext == ".pdf")
                            {
                                string excelfileName = string.Empty;
                                string path = "~/Areas/BM_Management/Documents/EntityMaster/" + objfileupload.CustomerId + "/";
                                string _file_Name = System.IO.Path.GetFileName(objfileupload.File.FileName);
                                string _path = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(path), _file_Name);
                                bool exists = System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath(path));
                                if (!exists)
                                {
                                    System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(path));
                                }
                                DirectoryInfo di = new DirectoryInfo(System.Web.Hosting.HostingEnvironment.MapPath(path));
                                FileInfo[] TXTFiles = di.GetFiles(_file_Name);
                                if (TXTFiles.Length == 0)
                                {
                                    objfileupload.File.SaveAs(_path);
                                }
                                PdfReader pdfReader = new PdfReader(_path);
                                SubCompanyType objcompanysubtype = new SubCompanyType();
                                if (pdfReader.AcroFields.Xfa.DatasetsSom != null)
                                {
                                    foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                    {
                                        #region capture entity name
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NAME_OF_COMPANY[0]")
                                        {
                                            if (de.Value.InnerText != null)
                                            {
                                                EntityName = (de.Value.InnerText).Trim();
                                            }
                                        }
                                        #endregion

                                        #region capture cin and entitytype
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].CIN[0]")
                                        {
                                            if (de.Value.InnerText != null)
                                            {
                                                CIN = (de.Value.InnerText).Trim();
                                                if (CIN.IndexOf("L") == 0)
                                                {
                                                    IsListed = true;

                                                    Type = 10;
                                                }
                                                else if (CIN.IndexOf("U") == 0)
                                                {
                                                    IsListed = false;
                                                }

                                                RegistrationNumber = CIN.Substring(15);
                                            }
                                        }
                                        #endregion

                                        #region capture PAN
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].IT_PAN_OF_COMPNY[0]")
                                        {

                                            if (de.Value.InnerText != null)
                                            {
                                                PAN = de.Value.InnerText;
                                            }
                                        }
                                        #endregion

                                        #region capture GLN
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].GLN[0]")
                                        {

                                            if (de.Value.InnerText != null)
                                            {
                                                GLN = de.Value.InnerText;
                                            }
                                        }
                                        #endregion

                                        #region capture address
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].REG_OFFC_ADDRESS[0]")
                                        {
                                            string pattern = @"^[1-9][0-9]{5}$";
                                            if (de.Value.InnerText != null)
                                            {
                                                RegisterAddress = de.Value.InnerText;

                                                List<string> address = RegisterAddress.Split('\n').ToList();
                                                if (address.Count() > 0)
                                                {
                                                    foreach (var item in address)
                                                    {
                                                        var matchTuple = Regex.Match(item, pattern, RegexOptions.IgnoreCase);
                                                        if (matchTuple.Success)
                                                        {
                                                            PIN = matchTuple.ToString();
                                                        }
                                                    }
                                                    if (address[0] != null)
                                                    {
                                                        RegisterAddressLine1 = address[0].ToString();
                                                    }
                                                    if (address.Count() > 1)
                                                    {
                                                        if (address[1] != null)
                                                        {
                                                            RegisterAddressLine2 = address[1].ToString();
                                                        }
                                                        if (address[4] != null)
                                                        {
                                                            stateId = GetRegisterStateId(address[4]);
                                                            if (stateId == 0)
                                                            {
                                                                stateId = GetRegisterStateId(address[3]);
                                                            }
                                                            if (stateId == 0)
                                                            {
                                                                stateId = GetRegisterStateId(address[2]);
                                                            }
                                                        }
                                                        if (address[2] != null && stateId > 0)
                                                        {
                                                            CityId = getCityRegisterID(stateId, address[2].ToString());
                                                            if (CityId == 0)
                                                            {
                                                                CityId = getCityRegisterID(stateId, address[1].ToString());
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                        #endregion

                                        #region capture email
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].EMAIL_ID_COMPANY[0]")
                                        {

                                            if (de.Value.InnerText != null)
                                            {
                                                EmilId = de.Value.InnerText;
                                            }
                                        }
                                        #endregion

                                        #region capture phone No
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].PHONE_NUMBER[0]")
                                        {

                                            if (de.Value.InnerText != null)
                                            {
                                                PhoneNo = Convert.ToInt64(de.Value.InnerText);
                                            }
                                        }
                                        #endregion

                                        #region capture date of Incorporation
                                        if (de.Key.ToString() == "data[0].INCORPORATION_DATE[0]")
                                        {

                                            if (de.Value.InnerText != null)
                                            {
                                                dateofIncorporate = Convert.ToDateTime(de.Value.InnerText);
                                            }
                                        }
                                        #endregion

                                        #region capture website
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].WEBSITE[0]")
                                        {

                                            if (de.Value.InnerText != null)
                                            {
                                                Website = de.Value.InnerText.Trim();
                                            }
                                        }
                                        #endregion

                                        #region capture company Type
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TYPE_OF_COMPANY[0]" && (Type == 0 || Type == -1))
                                        {

                                            if (de.Value.InnerText != null)
                                            {
                                                TypeofCompany = de.Value.InnerText.Trim();
                                                if (TypeofCompany.Trim() == "PRIV")
                                                {
                                                    Type = 1;
                                                }
                                                else if (TypeofCompany.Trim() == "PUBC")
                                                {
                                                    Type = 2;
                                                }

                                            }
                                        }
                                        #endregion

                                        #region capture company cateory
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].CATEGORY_COMPANY[0]")
                                        {
                                            if (de.Value.InnerText != null)
                                            {
                                                CategoryofCompany = de.Value.InnerText;
                                            }
                                            if (CategoryofCompany == "CLAG")
                                            {
                                                CompanyCategoryType = 2;
                                            }
                                            else if (CategoryofCompany == "ULCM")
                                            {
                                                CompanyCategoryType = 3;
                                            }
                                            else
                                            {
                                                CompanyCategoryType = 1;
                                            }
                                        }
                                        #endregion

                                        #region capture sub company
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].SUB_CATEGORY_COM[0]")
                                        {

                                            if (de.Value.InnerText != null)
                                            {
                                                CategoryofCompanySub = de.Value.InnerText;
                                            }
                                            if (CategoryofCompanySub == "NGOV")
                                            {
                                                CompanyCategorysubType = 1;
                                            }
                                            else if (CategoryofCompanySub == "UGCM")
                                            {
                                                CompanyCategorysubType = 2;
                                            }
                                            else if (CategoryofCompanySub == "STGM")
                                            {
                                                CompanyCategorysubType = 3;
                                            }
                                            else if (CategoryofCompanySub == "GACM")
                                            {
                                                CompanyCategorysubType = 4;
                                            }
                                            else if (CategoryofCompanySub == "SUFR")
                                            {
                                                CompanyCategorysubType = 5;
                                            }
                                        }
                                        #endregion

                                        #region capture FY
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].FY_FROM_DATE[0]" || de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].FY_END_DATE[0]")
                                        {
                                            if (de.Value.InnerText != null)
                                            {
                                                DateTime FYDate = Convert.ToDateTime(de.Value.InnerText);

                                                int Month = FYDate.Month;
                                                if (Month == 3 || Month == 4)
                                                {
                                                    FY = "FY";
                                                }
                                                else if (Month == 1 || Month == 12)
                                                {
                                                    FY = "CY";
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Details Fill for RTA For Listed Company
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].CIN_REG_TA[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                RTA_CIN = de.Value.InnerText.Trim();
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NAME_REG_TA[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                RTA_Name = de.Value.InnerText.Trim();
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].OFFICE_ADDRESS[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                RTA_Address = de.Value.InnerText.Trim();
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NO_COMPANIES[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                noofsubcompany = Convert.ToInt32(de.Value.InnerText.Trim());
                                            }
                                        }

                                        #endregion

                                        #region Capital Shares Details
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].RB_SHARE_CAPITAL[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                IsCapitalShares = de.Value.InnerText.Trim();
                                            }
                                        }


                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_A_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                TotNoofEquAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_I_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                TotNoofEquIssuedAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_S_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                TotNoofEquSubscribed = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_NO_ES_P_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                TotNoofEqupaidUp = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                            Console.WriteLine("Total number of equity shares Paid up" + " : " + de.Value.InnerText);
                                        }

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_A_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                TotAmtofEquAuthorized = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_I_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                TotAmtofEquIssued = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_S_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                TotAmtofEquSubscribe = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].TOT_AMT_ES_P_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                TotAmtofEquPaidup = Convert.ToDecimal(de.Value.InnerText.Trim());
                                            }
                                        }

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_MGT_7[0].NO_OF_CLASSES_ES[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                NoofClass = Convert.ToInt16(de.Value.InnerText.Trim());
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_A_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                NoofEquAuthorized = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }

                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_I_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                NoofEquIssued = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_S_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                NoofSubscribed = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NO_ES_P_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                NoofPaidup = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_A_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                NomvalperSahreEquAuthorized = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_I_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                NomvalperSahreEquIssued = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_S_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                NomvalperSahreEquSubscribed = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S4I_A[0].DATA[0].NOM_VAL_ES_P_CAP[0]")
                                        {
                                            if (de.Value.InnerText.Trim() != null)
                                            {
                                                NomvalperSahreEquPaidup = (long)Convert.ToDecimal(de.Value.InnerText);
                                            }
                                        }

                                        #endregion
                                    }

                                    if (!String.IsNullOrEmpty(CIN))
                                    {
                                        com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch()
                                        {
                                            //ID = _objentity.CustomerBranchId,
                                            Name = EntityName,
                                            //Type = Convert.ToByte(ddlType.SelectedValue),
                                            // ComType = Convert.ToByte(comType),
                                            //AddressLine1 = _objentity.Regi_Address_Line1,
                                            //AddressLine2 = _objentity.Regi_Address_Line2,
                                            //StateID = _objentity.Regi_StateId,
                                            //CityID = _objentity.Regi_CityId,
                                            //Others = "",
                                            //PinCode = _objentity.Regi_PINCode,
                                            ContactPerson = "",
                                            Landline = "",
                                            Mobile = "",
                                            EmailID = EmilId,
                                            CustomerID = objfileupload.CustomerId,
                                            //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                                            //Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                                        };

                                        customerBranch = CreateCustomerBranch(customerBranch, Type, IsListed, out Message);

                                        if (customerBranch.ID > 0)
                                        {
                                            BM_SubEntityMapping objsubentity = new BM_SubEntityMapping(); 
                                            var checkEntity = (from row in entities.BM_EntityMaster where row.CIN_LLPIN == CIN && row.Is_Deleted == false && row.Customer_Id == objfileupload.CustomerId select row).FirstOrDefault();
                                            //var checkEntity = (from row in entities.BM_EntityMaster where row.CIN_LLPIN == CIN && row.Is_Deleted == false select row).FirstOrDefault();
                                            if (checkEntity == null)
                                            {

                                                BM_EntityMaster objentityMaster = new BM_EntityMaster
                                                {
                                                    CompanyName = EntityName,

                                                    CIN_LLPIN = CIN,
                                                    GLN = GLN,
                                                    IncorporationDate = dateofIncorporate,
                                                    Registration_No = RegistrationNumber,
                                                    Regi_Address_Line1 = RegisterAddressLine1,
                                                    Regi_Address_Line2 = RegisterAddressLine2,
                                                    Regi_StateId = stateId,
                                                    Regi_CityId = CityId,
                                                    Regi_PINCode = PIN,
                                                    CompanySubCategory = CompanyCategorysubType,
                                                    CompanyCategory_Id = CompanyCategoryType,
                                                    IS_Listed = IsListed,
                                                    Email_Id = EmilId,
                                                    FY_CY = FY,
                                                    WebSite = Website,
                                                    PAN = PAN,
                                                    CreatedOn = DateTime.Now,
                                                    CreatedBy = objfileupload.UserId,
                                                    Is_Deleted = false,
                                                    Customer_Id = objfileupload.CustomerId,
                                                    CustomerBranchId = customerBranch.ID,
                                                };
                                                if (IsListed)
                                                {
                                                    objentityMaster.RTA_CompanyName = RTA_Name;
                                                    objentityMaster.RTA_Address = RTA_Name;
                                                    objentityMaster.RTA_CIN = RTA_CIN;
                                                    objentityMaster.Entity_Type = 10;
                                                }
                                                else
                                                {
                                                    objentityMaster.Entity_Type = Type;
                                                }
                                                entities.BM_EntityMaster.Add(objentityMaster);
                                                entities.SaveChanges();
                                                if (objentityMaster.Id > 0)
                                                {
                                                    #region for add subsidary Company joint Company 
                                                    while (j < noofsubcompany)
                                                    {
                                                        foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                                        {

                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].NAME_COMPANY[0]")
                                                            {

                                                                if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
                                                                {
                                                                    objcompanysubtype.SubCompanyName = de.Value.InnerText.Trim();


                                                                }
                                                            }

                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].CIN_FCRN[0]")
                                                            {
                                                                if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
                                                                {
                                                                    objcompanysubtype.CIN = de.Value.InnerText.Trim();


                                                                }
                                                            }



                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].HOLD_SUB_ASSOC[0]")
                                                            {
                                                                if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
                                                                {
                                                                    if (de.Value.InnerText.Trim() == "SUBS")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 3;
                                                                    }
                                                                    else if (de.Value.InnerText.Trim() == "JVEN")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 4;
                                                                    }
                                                                    else if (de.Value.InnerText.Trim() == "ASSC")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 1;
                                                                    }
                                                                    else
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 2;
                                                                    }
                                                                }
                                                            }

                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].PERCENT_SHARE[0]")
                                                            {
                                                                if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
                                                                {
                                                                    objcompanysubtype.PersentageofShareholding = Convert.ToDecimal(de.Value.InnerText.Trim());
                                                                }
                                                            }
                                                        }

                                                        if (objcompanysubtype.CIN != null && objcompanysubtype.CIN != "")
                                                        {
                                                            var checkdetailsofsubcompany = (from s in entities.BM_SubEntityMapping where s.CIN == objcompanysubtype.CIN select s).FirstOrDefault();
                                                            if (checkdetailsofsubcompany == null)
                                                            {
                                                                objsubentity.EntityId = objentityMaster.Id;
                                                                objsubentity.CIN = objcompanysubtype.CIN;
                                                                objsubentity.ParentCIN = objentityMaster.CIN_LLPIN;
                                                                objsubentity.NameofCompany = objcompanysubtype.SubCompanyName;
                                                                objsubentity.CompanysubType = objcompanysubtype.subcompanyTypeID;
                                                                objsubentity.PerofShareHeld = Convert.ToInt64(objcompanysubtype.PersentageofShareholding);
                                                                objsubentity.isActive = true;
                                                                objsubentity.Createdon = DateTime.Now;
                                                                objsubentity.CustomerId = (int)objentityMaster.Customer_Id;
                                                                entities.BM_SubEntityMapping.Add(objsubentity);
                                                                entities.SaveChanges();
                                                            }
                                                            else
                                                            {
                                                                checkdetailsofsubcompany.EntityId = objentityMaster.Id;
                                                                checkdetailsofsubcompany.CIN = objcompanysubtype.CIN;
                                                                checkdetailsofsubcompany.ParentCIN = objentityMaster.CIN_LLPIN;
                                                                checkdetailsofsubcompany.NameofCompany = objcompanysubtype.SubCompanyName;
                                                                checkdetailsofsubcompany.CompanysubType = objcompanysubtype.subcompanyTypeID;
                                                                checkdetailsofsubcompany.PerofShareHeld = Convert.ToInt64(objcompanysubtype.PersentageofShareholding);
                                                                checkdetailsofsubcompany.isActive = true;
                                                                checkdetailsofsubcompany.Createdon = DateTime.Now;
                                                                checkdetailsofsubcompany.CustomerId = (int)objentityMaster.Customer_Id;
                                                                //entities.BM_SubEntityMapping.Add(objsubentity);
                                                                entities.SaveChanges();

                                                            }
                                                        }
                                                        j++;
                                                    }
                                                    #endregion

                                                    EntityID = objentityMaster.Id;

                                                }
                                            }
                                            else
                                            {
                                                checkEntity.CompanyName = EntityName;
                                                checkEntity.Entity_Type = Type;
                                                checkEntity.CIN_LLPIN = CIN;
                                                checkEntity.GLN = GLN;
                                                checkEntity.IncorporationDate = dateofIncorporate;
                                                checkEntity.Registration_No = RegistrationNumber;
                                                checkEntity.Regi_Address_Line1 = RegisterAddressLine1;
                                                checkEntity.Regi_Address_Line2 = RegisterAddressLine2;
                                                checkEntity.Regi_StateId = stateId;
                                                checkEntity.Regi_CityId = CityId;
                                                checkEntity.CompanyCategory_Id = CompanyCategorysubType;
                                                checkEntity.IS_Listed = IsListed;
                                                checkEntity.Email_Id = EmilId;
                                                checkEntity.WebSite = Website;
                                                checkEntity.PAN = PAN;
                                                checkEntity.UpdatedOn = DateTime.Now;
                                                checkEntity.UpdatedBy = objfileupload.UserId;
                                                // checkEntity.CustomerBranchId = customerBranch.ID;
                                                checkEntity.Is_Deleted = false;
                                                checkEntity.Customer_Id = objfileupload.CustomerId;
                                                checkEntity.FY_CY = FY;
                                                if (IsListed)
                                                {
                                                    checkEntity.RTA_CompanyName = RTA_Name;
                                                    checkEntity.RTA_Address = RTA_Address;
                                                    checkEntity.RTA_CIN = RTA_CIN;
                                                }
                                                entities.SaveChanges();
                                                if (checkEntity.Id > 0)
                                                {
                                                    #region for add subsidary Company joint Company 
                                                    while (j < noofsubcompany)
                                                    {
                                                        foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                                        {

                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].NAME_COMPANY[0]")
                                                            {

                                                                if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
                                                                {
                                                                    objcompanysubtype.SubCompanyName = de.Value.InnerText.Trim();


                                                                }
                                                            }

                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].CIN_FCRN[0]")
                                                            {
                                                                //if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
                                                                //{
                                                                objcompanysubtype.CIN = de.Value.InnerText.Trim();

                                                                //}
                                                            }



                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].HOLD_SUB_ASSOC[0]")
                                                            {
                                                                if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
                                                                {
                                                                    if (de.Value.InnerText.Trim() == "SUBS")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 3;
                                                                    }
                                                                    else if (de.Value.InnerText.Trim() == "JVEN")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 4;
                                                                    }
                                                                    else if (de.Value.InnerText.Trim() == "ASSC")
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 1;
                                                                    }
                                                                    else
                                                                    {
                                                                        objcompanysubtype.subcompanyTypeID = 2;
                                                                    }

                                                                }
                                                            }
                                                            if (de.Key.ToString() == "data[0].T_ZNCA_MGT_7_S3[0].DATA[" + j + "].PERCENT_SHARE[0]")
                                                            {
                                                                if (de.Value.InnerText.Trim() != null && de.Value.InnerText.Trim() != "")
                                                                {
                                                                    objcompanysubtype.PersentageofShareholding = Convert.ToDecimal(de.Value.InnerText.Trim());


                                                                }
                                                            }
                                                        }
                                                        if (objcompanysubtype.CIN != null && objcompanysubtype.CIN != "")
                                                        {
                                                            var checkdetailsofsubcompany = (from s in entities.BM_SubEntityMapping where s.CIN == objcompanysubtype.CIN select s).FirstOrDefault();
                                                            if (checkdetailsofsubcompany == null)
                                                            {
                                                                objsubentity.EntityId = checkEntity.Id;
                                                                objsubentity.CIN = objcompanysubtype.CIN;
                                                                objsubentity.ParentCIN = checkEntity.CIN_LLPIN;
                                                                objsubentity.NameofCompany = objcompanysubtype.SubCompanyName;
                                                                objsubentity.CompanysubType = objcompanysubtype.subcompanyTypeID;
                                                                objsubentity.PerofShareHeld = objcompanysubtype.PersentageofShareholding;
                                                                objsubentity.isActive = true;
                                                                objsubentity.Createdon = DateTime.Now;
                                                                objsubentity.CustomerId = (int)checkEntity.Customer_Id;
                                                                entities.BM_SubEntityMapping.Add(objsubentity);
                                                                entities.SaveChanges();
                                                            }
                                                            else
                                                            {
                                                                checkdetailsofsubcompany.EntityId = checkEntity.Id;
                                                                checkdetailsofsubcompany.CIN = objcompanysubtype.CIN;
                                                                checkdetailsofsubcompany.ParentCIN = checkEntity.CIN_LLPIN;
                                                                checkdetailsofsubcompany.NameofCompany = objcompanysubtype.SubCompanyName;
                                                                checkdetailsofsubcompany.CompanysubType = objcompanysubtype.subcompanyTypeID;
                                                                checkdetailsofsubcompany.PerofShareHeld = Convert.ToInt64(objcompanysubtype.PersentageofShareholding);
                                                                checkdetailsofsubcompany.isActive = true;
                                                                checkdetailsofsubcompany.Createdon = DateTime.Now;
                                                                checkdetailsofsubcompany.CustomerId = (int)checkEntity.Customer_Id;
                                                                //entities.BM_SubEntityMapping.Add(objsubentity);
                                                                entities.SaveChanges();

                                                            }
                                                        }
                                                        j++;
                                                    }
                                                    #endregion
                                                    EntityID = checkEntity.Id;
                                                }
                                            }
                                            if (EntityID > 0)
                                            {
                                                BM_CapitalMaster objCapitalMaster = new BM_CapitalMaster();
                                                var checkforCapitalMaster = (from C in entities.BM_CapitalMaster where C.Entity_Id == EntityID select C).FirstOrDefault();
                                                if (checkforCapitalMaster == null)
                                                {
                                                    objCapitalMaster.Entity_Id = (int)EntityID;
                                                    objCapitalMaster.CustomerId = objfileupload.CustomerId;
                                                    objCapitalMaster.IsDebenger = false;
                                                    objCapitalMaster.IsPrefrence = false;
                                                    objCapitalMaster.IsUnclassified = false;
                                                    objCapitalMaster.TotNoAuthorizedCapita = TotNoofEquAuthorized;
                                                    objCapitalMaster.TotNoIssuedCapital = TotNoofEquIssuedAuthorized;
                                                    objCapitalMaster.TotNoSubscribCapital = TotNoofEquSubscribed;
                                                    objCapitalMaster.TotNoPaidupCapital = TotNoofEqupaidUp;
                                                    objCapitalMaster.TotamtAuthorizedCapita = TotAmtofEquAuthorized;
                                                    objCapitalMaster.TotamtNoIssuedCapital = TotAmtofEquIssued;
                                                    objCapitalMaster.TotamtNoSubscribCapital = TotAmtofEquSubscribe;
                                                    objCapitalMaster.TotamtNoPaidupCapital = TotAmtofEquPaidup;
                                                    objCapitalMaster.Pri_TotNoIssuedCapital = 0;
                                                    objCapitalMaster.Pri_TotNoSubscribCapital = 0;
                                                    objCapitalMaster.Pri_TotNoPaidupCapital = 0;
                                                    objCapitalMaster.Pri_TotamtAuthorizedCapita = 0;
                                                    objCapitalMaster.Pri_TotamtNoIssuedCapital = 0;
                                                    objCapitalMaster.Pri_TotamtNoSubscribCapital = 0;
                                                    objCapitalMaster.Pri_TotamtNoPaidupCapital = 0;
                                                    objCapitalMaster.Unclassified_AuhorisedCapital = 0;
                                                    objCapitalMaster.IsActive = true;
                                                    objCapitalMaster.Createdby = objfileupload.UserId;
                                                    objCapitalMaster.CreatedOn = DateTime.Now;
                                                    objCapitalMaster.AuthorizedCapital = objCapitalMaster.TotamtAuthorizedCapita + objCapitalMaster.Pri_TotamtAuthorizedCapita;
                                                    entities.BM_CapitalMaster.Add(objCapitalMaster);
                                                    entities.SaveChanges();
                                                    if (objCapitalMaster.Id > 0 && EntityID > 0)
                                                    {
                                                        var checkforCapitalMasterShares = (from CS in entities.BM_Share where CS.CapitalMasterId == objCapitalMaster.Id select CS).ToList();
                                                        if (checkforCapitalMasterShares.Count == 0)
                                                        {
                                                            BM_Share objshares = new BM_Share();
                                                            objshares.CapitalMasterId = (int)objCapitalMaster.Id;
                                                            objshares.CustomerId = objCapitalMaster.CustomerId;
                                                            objshares.Sharetype = "E";
                                                            objshares.Shares_Class = 1;
                                                            objshares.No_AuhorisedCapital = NoofEquAuthorized;
                                                            objshares.No_IssuedCapital = NoofEquIssued;
                                                            objshares.No_SubscribedCapital = NoofSubscribed;
                                                            objshares.No_Paid_upCapital = NoofPaidup;
                                                            objshares.Nominalper_AuhorisedCapital = NomvalperSahreEquAuthorized;
                                                            objshares.Nominalper_IssuedCapital = NomvalperSahreEquIssued;
                                                            objshares.Nominalper_SubscribedCapital = NomvalperSahreEquSubscribed;
                                                            objshares.Nominalper_Paid_upCapital = NomvalperSahreEquPaidup;
                                                            objshares.totamt_AuhorisedCapital = NoofEquAuthorized * NomvalperSahreEquAuthorized;
                                                            objshares.totamt_IssuedCapital = NoofEquIssued * NomvalperSahreEquIssued;
                                                            objshares.totamt_SubscribedCapital = NoofSubscribed * NomvalperSahreEquSubscribed;
                                                            objshares.totamt_upCapital = NoofPaidup * NomvalperSahreEquPaidup;
                                                            objshares.IsActive = true;
                                                            entities.BM_Share.Add(objshares);
                                                            entities.SaveChanges();

                                                        }
                                                    }
                                                }

                                                objfileupload.Response.Success = true;
                                                objfileupload.Response.Message = "MGT7 Upload successfully";
                                            }
                                        }
                                        else
                                        {
                                            objfileupload.Response.Error = true;
                                            objfileupload.Response.Message = "Entity already exist";
                                        }
                                    }
                                    else
                                    {
                                        objfileupload.Response.Error = true;
                                        objfileupload.Response.Message = "CIN can't  null";
                                    }
                                }
                                else
                                {
                                    objfileupload.Response.Error = true;
                                    objfileupload.Response.Message = "Please check uploaded form is MGT-7";
                                }
                            }
                            else
                            {
                                objfileupload.Response.Error = true;
                                objfileupload.Response.Message = "Please check uploaded form is MGT-7";
                            }
                        }
                    }
                    else
                    {
                        objfileupload.Response.Error = true;
                        objfileupload.Response.Message = "Form not found";
                    }
                }
                else
                {
                    objfileupload.Response.Error = true;
                    objfileupload.Response.Message = "You are not allowed to create more entity/company, Contact Avantis to allow to create more";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objfileupload.Response.Error = true;
                objfileupload.Response.Message = "Server error occurred";
            }
            return objfileupload;
        }

        private int getCityRegisterID(int stateId, string CityName)
        {
            var cityId = (from row in entities.Cities where row.Name.Contains(CityName) && row.StateId == stateId select row.ID).FirstOrDefault();
            return cityId;
        }

        private int GetRegisterStateId(string registerAddress)
        {
            var stateId = (from row in entities.States where row.Name.Contains(registerAddress) select row.ID).FirstOrDefault();
            return stateId;
        }

        //private CustomerBranch CreateCustomerBranch(CustomerBranch customerBranch, int Entity_Type, bool isListed, int customerId, out string message)
        //{
        //    throw new NotImplementedException();
        //}

        public com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch CreateCustomerBranch(com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch, int Entity_Type, bool IsListed, out string Message)
        {
            Message = "";
            customerBranch.LegalEntityTypeID = 1;
            customerBranch.Status = 1;
            customerBranch.Landline = "0";
            customerBranch.Mobile = "0";
            customerBranch.Type = 1;

            customerBranch.ContactPerson = " ";

            #region set Company Type
            customerBranch.ComType = null;

            if (IsListed == true)
            {
                customerBranch.ComType = 3;
            }
            else if (Entity_Type == 1 || Entity_Type == 9)
            {
                customerBranch.ComType = 2;
            }
            else if (Entity_Type == 2)
            {
                customerBranch.ComType = 1;
            }
            else
            {
                customerBranch.ComType = 4;
            }
            #endregion

            #region set Legal Entity type

            if (Entity_Type == 1)
            {
                customerBranch.LegalEntityTypeID = 6; //Private Limited Company
                customerBranch.LegalEntityTypeID = null;
            }
            else if (Entity_Type == 3)
            {
                customerBranch.LegalEntityTypeID = 4; //Limited Liability Partnership(LLP)
            }
            else if (Entity_Type == 2 && IsListed == true)
            {
                customerBranch.LegalEntityTypeID = 2; //Public Limited Company (Listed)
            }
            else if (Entity_Type == 2 && IsListed == false)
            {
                customerBranch.LegalEntityTypeID = 3; //Public Limited Company (Unlisted)
            }
            else if (Entity_Type == 10 && IsListed == true)
            {
                customerBranch.LegalEntityTypeID = 2; //Public Limited Company (Listed)
            }
            else if (Entity_Type == 10 && IsListed == false)
            {
                customerBranch.LegalEntityTypeID = 3; //Public Limited Company (Unlisted)
            }
            else if (Entity_Type == 7)
            {
                customerBranch.LegalEntityTypeID = 1; //Partnership
            }
            else if (Entity_Type == 6)
            {
                customerBranch.LegalEntityTypeID = 5; //Proprietorship
            }
            else if (Entity_Type == 8)
            {
                customerBranch.LegalEntityTypeID = 8; //One Person Company (O.P.C.)
            }
            else
            {
                customerBranch.LegalEntityTypeID = null;
            }
            #endregion

            #region Create Entity


            long CheckCustomerBranchIdExist = ExistsCustomerBranch_Compliance(customerBranch, customerBranch.CustomerID);


            com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranch1 = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch()
            {
                ID = customerBranch.ID,
                Name = customerBranch.Name,
                Type = customerBranch.Type,
                ComType = customerBranch.ComType,
                AddressLine1 = customerBranch.AddressLine1,
                AddressLine2 = customerBranch.AddressLine2,
                StateID = customerBranch.StateID,
                CityID = customerBranch.CityID,
                Others = customerBranch.Others,
                PinCode = customerBranch.PinCode,
                ContactPerson = customerBranch.ContactPerson,
                Landline = customerBranch.Landline,
                Mobile = customerBranch.Mobile,
                EmailID = customerBranch.EmailID,
                CustomerID = customerBranch.CustomerID,
                //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                Status = customerBranch.Status
            };

            long checkCustomerBranchExist1 = ExistsCustomerBranch_Audit(customerBranch1, customerBranch1.CustomerID);
            if (CheckCustomerBranchIdExist > 0 && checkCustomerBranchExist1 > 0)
            {
                //CustomerBranchManagement.UpdateFromSecretarial(customerBranch);

                //CustomerBranchManagement.Update1FromSecretarial(customerBranch1);
                customerBranch.ID = (int)CheckCustomerBranchIdExist;


            }
            else
            {
                if (customerBranch.ID <= 0)
                {
                    int BranchId = CustomerBranchManagement.Create(customerBranch);
                    customerBranch.ID = BranchId;

                    CustomerBranchManagement.Create1(customerBranch1);
                }
            }
            #endregion

            return customerBranch;
        }

        private long ExistsCustomerBranch_Compliance(com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch customerBranch, int customerID)
        {
            var query = (from row in entities.CustomerBranches
                         where row.IsDeleted == false
                         && row.Name.Equals(customerBranch.Name) && row.CustomerID == customerID
                         select row).FirstOrDefault();

            if (query != null)
            {
                return query.ID;
            }
            else
            {
                return 0;
            }

        }

        public long ExistsCustomerBranch_Audit(com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch mst_CustomerBranch, int CustomerId)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false
                             && row.Name.Equals(mst_CustomerBranch.Name) && row.CustomerID == CustomerId
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return query.ID;
                }
                else
                {
                    return 0;
                }
            }
        }

        public IEnumerable<BM_CompanySubCategory> GetDropDownforCompanySubCategory()
        {
            try
            {
                var getCompanysubcategory = (from row in entities.BM_CompanySubCategory select row).ToList();
                return getCompanysubcategory;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<SubCompanyType> GetSubEntityDetails(long entityID, long customerID)
        {
            List<SubCompanyType> _objSubCompanyType = new List<SubCompanyType>();
            try
            {
                _objSubCompanyType = (from row in entities.BM_SubEntityMapping
                                      where row.EntityId == entityID && row.CustomerId == customerID
&& row.isActive == true
                                      select new SubCompanyType
                                      {
                                          EntityID = row.EntityId,
                                          SubcompanyId = row.Id,
                                          subcompanyTypeID = row.CompanysubType,
                                          SubCompanyName = row.NameofCompany,
                                          PersentageofShareholding = row.PerofShareHeld,
                                          subcompanyTypeName = (from x in entities.BM_CompanySubType where x.Id == row.CompanysubType select x.Name).FirstOrDefault(),
                                          CIN = row.CIN,
                                          ParentCIN = (from e in entities.BM_EntityMaster where e.Id == entityID select e.CIN_LLPIN).FirstOrDefault(),
                                          CustomerId = (int)customerID,
                                          //AssociateCompany = new 
                                          //{
                                          //    Id = row.CompanysubType,
                                          //    Name = (from x in entities.BM_CompanySubType where x.Id == row.CompanysubType select x.Name).FirstOrDefault(),
                                          //}
                                          AssociateCompany = new AssosiateType
                                          {
                                              Id = row.Id,
                                              Name = (from x in entities.BM_CompanySubType where x.Id == row.CompanysubType select x.Name).FirstOrDefault(),
                                          }
                                      }).ToList();
                return _objSubCompanyType;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objSubCompanyType;
            }
        }

        public List<AssosiateType> GetAllEntityMaster()
        {
            try
            {
                var getCompanysubType = (from row in entities.BM_CompanySubType
                                         where row.IsActive == true
                                         orderby row.Name
                                         select new AssosiateType
                                         {
                                             Id = row.Id,
                                             Name = row.Name
                                         }

                                         ).ToList();
                return getCompanysubType;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public void addCompanySubType(SubCompanyType items)
        {
            string CIN = String.Empty;
            long MappingId = 0;

            try
            {
                if (items.CIN != null)
                {
                    CIN = items.CIN.Trim();
                    var CheckIsCompanyExist = (from row in entities.BM_EntityMaster where row.CIN_LLPIN == CIN && row.Customer_Id == items.CustomerId && row.Is_Deleted == false select row).FirstOrDefault();
                    if (CheckIsCompanyExist != null)
                    {
                        #region Check Holding and Subsidiary
                        if (items.AssociateCompany.Id == 3 || items.AssociateCompany.Id == 2)
                        {

                      

                            var checkExist = (from x in entities.BM_SubEntityMapping where x.EntityId == items.EntityID && x.CIN == CIN && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                            #region Create or Update Holding and Subsidiary
                            if (checkExist == null)
                            {
                                BM_SubEntityMapping objsubentitymapping = new BM_SubEntityMapping();
                                objsubentitymapping.EntityId = items.EntityID;
                                objsubentitymapping.ParentCIN = items.ParentCIN;
                                objsubentitymapping.CIN = CIN;
                                objsubentitymapping.CompanysubType = (int)items.AssociateCompany.Id;
                                objsubentitymapping.NameofCompany = items.SubCompanyName;
                                objsubentitymapping.PerofShareHeld = items.PersentageofShareholding;
                                objsubentitymapping.CustomerId = items.CustomerId;
                                objsubentitymapping.Createdon = DateTime.Now;
                                objsubentitymapping.isActive = true;
                                entities.BM_SubEntityMapping.Add(objsubentitymapping);
                                entities.SaveChanges();
                                MappingId = objsubentitymapping.Id;
                            }
                            else
                            {
                                checkExist.EntityId = items.EntityID;
                                checkExist.ParentCIN = items.ParentCIN;
                                checkExist.CIN = CIN;
                                checkExist.CompanysubType = (int)items.AssociateCompany.Id;
                                checkExist.NameofCompany = items.SubCompanyName;
                                checkExist.PerofShareHeld = items.PersentageofShareholding;
                                checkExist.CustomerId = items.CustomerId;
                                checkExist.Createdon = DateTime.Now;
                                checkExist.isActive = true;
                                entities.SaveChanges();
                                MappingId = checkExist.Id;
                            }
                            if (MappingId > 0)
                            {
                                var checkcomexist = (from x in entities.BM_SubEntityMapping where x.EntityId == CheckIsCompanyExist.Id && x.CIN == CheckIsCompanyExist.CIN_LLPIN && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                                if (checkcomexist == null)
                                {
                                    BM_SubEntityMapping objsubentitymapping1 = new BM_SubEntityMapping();
                                    objsubentitymapping1.EntityId = CheckIsCompanyExist.Id;
                                    objsubentitymapping1.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    objsubentitymapping1.CIN = CIN;
                                    if (items.AssociateCompany.Id == 3)
                                    {
                                        objsubentitymapping1.CompanysubType = 2;
                                    }
                                    else
                                    {
                                        objsubentitymapping1.CompanysubType = 3;
                                    }
                                    objsubentitymapping1.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID select a.CompanyName).FirstOrDefault();
                                    objsubentitymapping1.PerofShareHeld = items.PersentageofShareholding;
                                    objsubentitymapping1.CustomerId = items.CustomerId;
                                    objsubentitymapping1.Createdon = DateTime.Now;
                                    objsubentitymapping1.isActive = true;
                                    entities.BM_SubEntityMapping.Add(objsubentitymapping1);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkcomexist.EntityId = CheckIsCompanyExist.Id;
                                    checkcomexist.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    checkcomexist.CIN = CIN;
                                    if (items.AssociateCompany.Id == 3)
                                    {
                                        checkcomexist.CompanysubType = 2;
                                    }
                                    else
                                    {
                                        checkcomexist.CompanysubType = 3;
                                    }
                                    checkcomexist.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID select a.CompanyName).FirstOrDefault();
                                    checkcomexist.PerofShareHeld = items.PersentageofShareholding;
                                    checkcomexist.CustomerId = items.CustomerId;
                                    checkcomexist.Createdon = DateTime.Now;
                                    checkcomexist.isActive = true;

                                    entities.SaveChanges();
                                }
                            }
                        }

                        #endregion
                        #endregion

                        #region check Associate and Investing
                        else if (items.AssociateCompany.Id == 1 || items.AssociateCompany.Id == 5)
                        {
                            var checkExist = (from x in entities.BM_SubEntityMapping where x.CIN == CIN && x.EntityId == items.EntityID && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                            if (checkExist == null)
                            {
                                BM_SubEntityMapping objsubentitymapping = new BM_SubEntityMapping();
                                objsubentitymapping.EntityId = items.EntityID;
                                objsubentitymapping.ParentCIN = items.ParentCIN;
                                objsubentitymapping.CIN = CIN;
                                objsubentitymapping.CompanysubType = (int)items.AssociateCompany.Id;
                                objsubentitymapping.NameofCompany = items.SubCompanyName;
                                objsubentitymapping.PerofShareHeld = items.PersentageofShareholding;
                                objsubentitymapping.CustomerId = items.CustomerId;
                                objsubentitymapping.Createdon = DateTime.Now;
                                objsubentitymapping.isActive = true;
                                entities.BM_SubEntityMapping.Add(objsubentitymapping);
                                entities.SaveChanges();
                                MappingId = objsubentitymapping.Id;
                            }
                            else
                            {
                                checkExist.EntityId = items.EntityID;
                                checkExist.ParentCIN = items.ParentCIN;
                                checkExist.CIN = CIN;
                                checkExist.CompanysubType = (int)items.AssociateCompany.Id;
                                checkExist.NameofCompany = items.SubCompanyName;
                                checkExist.PerofShareHeld = items.PersentageofShareholding;
                                checkExist.CustomerId = items.CustomerId;
                                checkExist.Createdon = DateTime.Now;
                                checkExist.isActive = true;
                                entities.SaveChanges();
                                MappingId = checkExist.Id;
                            }

                            if (MappingId > 0)
                            {
                                var checkcomexist = (from x in entities.BM_SubEntityMapping where x.EntityId == CheckIsCompanyExist.Id && x.CIN == CheckIsCompanyExist.CIN_LLPIN && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                                if (checkcomexist == null)
                                {
                                    BM_SubEntityMapping objsubentitymapping1 = new BM_SubEntityMapping();
                                    objsubentitymapping1.EntityId = CheckIsCompanyExist.Id;
                                    objsubentitymapping1.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    objsubentitymapping1.CIN = CIN;

                                    if (items.AssociateCompany.Id == 1)
                                    {
                                        objsubentitymapping1.CompanysubType = 5;
                                    }
                                    else
                                    {
                                        objsubentitymapping1.CompanysubType = 1;
                                    }
                                    objsubentitymapping1.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID select a.CompanyName).FirstOrDefault();
                                    objsubentitymapping1.PerofShareHeld = items.PersentageofShareholding;
                                    objsubentitymapping1.CustomerId = items.CustomerId;
                                    objsubentitymapping1.Createdon = DateTime.Now;
                                    objsubentitymapping1.isActive = true;
                                    entities.BM_SubEntityMapping.Add(objsubentitymapping1);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkcomexist.EntityId = CheckIsCompanyExist.Id;
                                    checkcomexist.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    checkcomexist.CIN = CIN;

                                    if (items.AssociateCompany.Id == 1)
                                    {
                                        checkcomexist.CompanysubType = 5;
                                    }
                                    else
                                    {
                                        checkcomexist.CompanysubType = 1;
                                    }
                                    checkcomexist.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID select a.CompanyName).FirstOrDefault();
                                    checkcomexist.PerofShareHeld = items.PersentageofShareholding;
                                    checkcomexist.CustomerId = items.CustomerId;
                                    checkcomexist.Createdon = DateTime.Now;
                                    checkcomexist.isActive = true;

                                    entities.SaveChanges();
                                }
                            }

                        }
                        #endregion

                        #region JointVenture
                        else if (items.AssociateCompany.Id == 4)
                        {

                            var checkExist = (from x in entities.BM_SubEntityMapping where x.CIN == CIN && x.EntityId == items.EntityID && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                            if (checkExist == null)
                            {
                                BM_SubEntityMapping objsubentitymapping = new BM_SubEntityMapping();
                                objsubentitymapping.EntityId = items.EntityID;
                                objsubentitymapping.ParentCIN = items.ParentCIN;
                                objsubentitymapping.CIN = CIN;
                                objsubentitymapping.CompanysubType = (int)items.AssociateCompany.Id;
                                objsubentitymapping.NameofCompany = items.SubCompanyName;
                                objsubentitymapping.PerofShareHeld = items.PersentageofShareholding;
                                objsubentitymapping.CustomerId = items.CustomerId;
                                objsubentitymapping.Createdon = DateTime.Now;
                                objsubentitymapping.isActive = true;
                                entities.BM_SubEntityMapping.Add(objsubentitymapping);
                                entities.SaveChanges();
                                MappingId = objsubentitymapping.Id;
                            }
                            else
                            {
                             
                                checkExist.EntityId = items.EntityID;
                                checkExist.ParentCIN = items.ParentCIN;
                                checkExist.CIN = CIN;
                                checkExist.CompanysubType = (int)items.AssociateCompany.Id;
                                checkExist.NameofCompany = items.SubCompanyName;
                                checkExist.PerofShareHeld = items.PersentageofShareholding;
                                checkExist.CustomerId = items.CustomerId;
                                checkExist.Updatedon = DateTime.Now;
                                checkExist.isActive = true;
                            
                                entities.SaveChanges();
                                MappingId = checkExist.Id;

                                //BM_SubEntityMapping objsubentitymapping = new BM_SubEntityMapping();
                                //objsubentitymapping.EntityId = items.EntityID;
                                //objsubentitymapping.ParentCIN = items.ParentCIN;
                                //objsubentitymapping.CIN = CIN;
                                //objsubentitymapping.CompanysubType = (int)items.AssociateCompany.Id;
                                //objsubentitymapping.NameofCompany = items.SubCompanyName;
                                //objsubentitymapping.PerofShareHeld = items.PersentageofShareholding;
                                //objsubentitymapping.CustomerId = items.CustomerId;
                                //objsubentitymapping.Createdon = DateTime.Now;
                                //objsubentitymapping.isActive = true;
                                //entities.BM_SubEntityMapping.Add(objsubentitymapping);
                                //entities.SaveChanges();
                                //MappingId = objsubentitymapping.Id;
                            }


                            if (MappingId > 0)
                            {
                                var checkcomexist = (from x in entities.BM_SubEntityMapping where x.EntityId == CheckIsCompanyExist.Id && x.CIN == CheckIsCompanyExist.CIN_LLPIN && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                                if (checkcomexist == null)
                                {
                                    BM_SubEntityMapping objsubentitymapping1 = new BM_SubEntityMapping();
                                    objsubentitymapping1.EntityId = CheckIsCompanyExist.Id;
                                    objsubentitymapping1.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    objsubentitymapping1.CIN = CIN;
                                    objsubentitymapping1.CompanysubType = 4;
                                    objsubentitymapping1.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID && a.Customer_Id == items.CustomerId select a.CompanyName).FirstOrDefault();
                                    objsubentitymapping1.PerofShareHeld = items.PersentageofShareholding;
                                    objsubentitymapping1.CustomerId = items.CustomerId;
                                    objsubentitymapping1.Createdon = DateTime.Now;
                                    objsubentitymapping1.isActive = true;
                                    entities.BM_SubEntityMapping.Add(objsubentitymapping1);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkcomexist.EntityId = CheckIsCompanyExist.Id;
                                    checkcomexist.ParentCIN = CheckIsCompanyExist.CIN_LLPIN;
                                    checkcomexist.CIN = CIN;
                                    checkcomexist.CompanysubType = 4;
                                    checkcomexist.NameofCompany = (from a in entities.BM_EntityMaster where a.Id == items.EntityID && a.Customer_Id == items.CustomerId select a.CompanyName).FirstOrDefault();
                                    checkcomexist.PerofShareHeld = items.PersentageofShareholding;
                                    checkcomexist.CustomerId = items.CustomerId;
                                    checkcomexist.Createdon = DateTime.Now;
                                    checkcomexist.isActive = true;

                                    entities.SaveChanges();
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        var checkIsExistsubcompany = (from x in entities.BM_SubEntityMapping where x.EntityId == items.EntityID && x.CIN == items.CIN && x.CustomerId == items.CustomerId select x).FirstOrDefault();
                        if (checkIsExistsubcompany == null)
                        {
                            BM_SubEntityMapping objsubentitymapping = new BM_SubEntityMapping();
                            objsubentitymapping.EntityId = items.EntityID;
                            objsubentitymapping.ParentCIN = items.ParentCIN;
                            objsubentitymapping.CIN = CIN;
                            objsubentitymapping.CompanysubType = (int)items.AssociateCompany.Id;
                            objsubentitymapping.NameofCompany = items.SubCompanyName;
                            objsubentitymapping.PerofShareHeld = items.PersentageofShareholding;
                            objsubentitymapping.CustomerId = items.CustomerId;
                            objsubentitymapping.Createdon = DateTime.Now;
                            objsubentitymapping.isActive = true;
                            entities.BM_SubEntityMapping.Add(objsubentitymapping);
                            entities.SaveChanges();
                        }
                        else
                        {
                            checkIsExistsubcompany.EntityId = items.EntityID;
                            checkIsExistsubcompany.ParentCIN = items.ParentCIN;
                            checkIsExistsubcompany.CIN = CIN;
                            checkIsExistsubcompany.CompanysubType = (int)items.AssociateCompany.Id;
                            checkIsExistsubcompany.NameofCompany = items.SubCompanyName;
                            checkIsExistsubcompany.PerofShareHeld = items.PersentageofShareholding;
                            checkIsExistsubcompany.CustomerId = items.CustomerId;
                            checkIsExistsubcompany.Createdon = DateTime.Now;
                            checkIsExistsubcompany.isActive = true;

                            entities.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DeleteSubEntityMapping(SubCompanyType items)
        {
            try
            {
                var CheckIsCompanyExist = (from row in entities.BM_SubEntityMapping where row.CIN == items.CIN.Trim() && row.isActive == true && row.Id == items.SubcompanyId select row).FirstOrDefault();
                if (CheckIsCompanyExist != null)
                {
                    CheckIsCompanyExist.isActive = false;
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public SubCompanyType getDetailsofentity(long entityId, string cIN)
        {
            try
            {
                var getentityDetils = (from row in entities.BM_EntityMaster
                                       where row.CIN_LLPIN == cIN
                                       select new SubCompanyType
                                       {
                                           CIN = row.CIN_LLPIN,
                                           EntityID = entityId,
                                           ParentCIN = cIN,
                                           SubCompanyName = row.CompanyName,

                                       }).FirstOrDefault();
                return getentityDetils;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #region Entity Details According to BOD Created by Ruchi
        public List<EntityMasterVM> GetAllEntityForBOD(int customerId, int userID)
        {
            try
            {
                var getlistofEntity = (from row in entities.BM_SP_EntityListforBOD(userID, customerId)
                                       orderby row.CompanyName
                                       select new EntityMasterVM
                                       {
                                           Id = row.ID,
                                           EntityName = row.CompanyName,
                                           LLPI_CIN = row.CIN_LLPIN,
                                           Type = row.EntityType
                                       }).ToList();
                return getlistofEntity;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<EntityMasterVM> EntityListdirectorwise(int userID)
        {
            try
            {
                var getentityforCommittee = (from row in entities.BM_SP_EntityListDirectorwiseforcommitee(userID)
                                             select new EntityMasterVM
                                             {
                                                 Id = (int)row.Id,
                                                 EntityName = row.CompanyName
                                             }).ToList();
                return getentityforCommittee;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public List<EntityMasterVM> GetAllEntityMasterbyid(int customerId, int userID, string role, long entityId)
        {
            DateTime date;
            var customerBranches = (from row in entities.BM_EntityMaster
                                    join rows in entities.BM_EntityType
                                    on row.Entity_Type equals (rows.Id)
                                    where row.Is_Deleted == false && row.Customer_Id == customerId && row.Id == entityId
                                    orderby row.CompanyName ascending
                                    select new EntityMasterVM
                                    {
                                        Id = row.Id,
                                        EntityName = row.CompanyName,
                                        Type = rows.EntityName,
                                        LLPI_CIN = row.CIN_LLPIN != "0" ? row.CIN_LLPIN : row.Registration_No,
                                        RegistrationNO = row.Registration_No,
                                        CIN = row.CIN_LLPIN,
                                        Entity_Type = row.Entity_Type,
                                        dateofIncorporaion = row.IncorporationDate,
                                        PAN = row.PAN,
                                        Islisted = row.IS_Listed,
                                        RocCode = (from r in entities.BM_ROC_code where r.Id == row.ROC_Code select r.Name).FirstOrDefault(),
                                        CompanyCategory = (from c in entities.BM_CompanyCategory where c.Id == row.CompanyCategory_Id select c.Category).FirstOrDefault(),
                                        CompanysubCategory = (from cs in entities.BM_CompanySubCategory where cs.Id == row.CompanySubCategory select cs.SubCategoryName).FirstOrDefault(),
                                        classofCompany = rows.EntityName,
                                        RegisterAddress = row.Regi_Address_Line1 + " " + row.Regi_Address_Line2,
                                        emailId = row.Email_Id

                                    });
            return customerBranches.ToList();
        }

        public List<EntityMasterVM> GetAssignedEntities(int customerID, int userID, string userRole)
        {
            try
            {
                var lstEntities = (from row in entities.BM_SP_GetAssignedEntities(userID, customerID, userRole)
                                   select new EntityMasterVM
                                   {
                                       Id = row.ID,
                                       EntityName = row.CompanyName,
                                       LLPI_CIN = row.CIN_LLPIN,
                                       Type = row.EntityType
                                   }).ToList();
                return lstEntities;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<EntityMasterVM> GetEntitiesDropDown(int customerID, int userID, string userRole)
        {
            try
            {
                var lstEntities = (from row in entities.BM_SP_GetEntitiesDropDown(userID, customerID, userRole)
                                   select new EntityMasterVM
                                   {
                                       Id = row.ID,
                                       EntityName = row.CompanyName,
                                       LLPI_CIN = row.CIN_LLPIN,
                                       Type = row.EntityType
                                   }).ToList();
                return lstEntities;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }


        #endregion

        #endregion
        #endregion
        public int? GetServiseProviderId(int CustomerId)
        {
            int? ServiceproviderId = 0;
            try
            {

                ServiceproviderId = (from cust in entities.Customers
                                     where cust.ID == CustomerId
                                     select cust.ServiceProviderID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return ServiceproviderId;
        }

        public int? GetParentID(int custID)
        {
            int? ParentID = 0;
            try
            {
                ParentID = (from cust in entities.Customers
                            where cust.ID == custID
                            select cust.ParentID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return ParentID;
        }

	    #region Import Entity from AVACOM
        public List<ParentEntitySelectVM> GetEntityListForImport(int customerId)
        {
            var result = new List<ParentEntitySelectVM>();
            try
            {
                result = (from row in entities.BM_SP_EntityListForImportFromAVACOM(customerId)
                          select new ParentEntitySelectVM
                          {
                              ID = row.ID,
                              Name = row.Name,
                              Address = row.AddressLine1,
                              Email = row.EmailID,
                              IsCheked = false
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public IEnumerable<ParentEntitySelectVM> ImportEntityFromAVACOM(IEnumerable<ParentEntitySelectVM> lstEntities, int createdBy)
        {
            try
            {
                foreach (var item in lstEntities)
                {
                    BM_EntityMaster _obj = (from row in entities.BM_EntityMaster
                                            where row.CustomerBranchId == item.ID
                                            select row).FirstOrDefault();
                    if(_obj == null)
                    {
                        var objCustomerBranche = (from row in entities.CustomerBranches
                                                  where row.ID == item.ID && row.IsDeleted == false
                                                  select row).FirstOrDefault();
                        if(objCustomerBranche != null)
                        {
                            _obj = new BM_EntityMaster();

                            _obj.CompanyName = objCustomerBranche.Name.ToUpper();
                            _obj.Entity_Type = 0;
                            _obj.CIN_LLPIN = "0";
                            _obj.IncorporationDate = DateTime.Now;
                            _obj.ROC_Code = 0;

                            _obj.Regi_Address_Line1 = objCustomerBranche.AddressLine1.ToUpper();

                            if (objCustomerBranche.AddressLine2 != "")
                                _obj.Regi_Address_Line2 = objCustomerBranche.AddressLine2.ToUpper();
                            else
                                _obj.Regi_Address_Line2 = "NA";

                           _obj.Regi_StateId = objCustomerBranche.StateID;
                            _obj.Regi_CityId = objCustomerBranche.CityID;
                            _obj.IsCorp_Office = false;
                            _obj.CompanyCategory_Id = 0;

                            _obj.IS_Listed = false;
                            _obj.Email_Id = objCustomerBranche.EmailID;
                            _obj.PAN = "";

                            _obj.Customer_Id = objCustomerBranche.CustomerID;
                            _obj.CustomerBranchId = objCustomerBranche.ID;

                            _obj.Is_Deleted = false;
                            _obj.CreatedBy = createdBy;
                            _obj.CreatedOn = DateTime.Now;

                            _obj.Entity_Type = 0;

                            #region set Entity Type from Company Type

                            if (objCustomerBranche.ComType == 2)
                            {
                                _obj.Entity_Type = 1;
                            }
                            else if (objCustomerBranche.ComType == 1)
                            {
                                _obj.Entity_Type = 2;
                                _obj.IS_Listed = false;
                            }
                            else if (objCustomerBranche.ComType == 3)
                            {
                                _obj.Entity_Type = 10;
                                _obj.IS_Listed = true;
                            }

                            #endregion

                            #region set Entity Type from Legal entity Type
                            if (objCustomerBranche.LegalEntityTypeID == 6)//Private Limited Company
                            {
                                _obj.Entity_Type = 1;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 4)  //Limited Liability Partnership(LLP)
                            {
                                _obj.Entity_Type = 3;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 2)  //Public Limited Company (Listed)
                            {
                                _obj.Entity_Type = 10;
                                _obj.IS_Listed = true;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 3)  //Public Limited Company (Unlisted)
                            {
                                _obj.Entity_Type = 2;
                                _obj.IS_Listed = false;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 1)//Partnership
                            {
                                _obj.Entity_Type = 7;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 5)//Proprietorship
                            {
                                _obj.Entity_Type = 6;
                            }
                            else if (objCustomerBranche.LegalEntityTypeID == 8)//One Person Company (O.P.C.)
                            {
                                _obj.Entity_Type = 8;
                            }
                          
                            #endregion

                            if (_obj.Entity_Type > 0)
                            {
                                entities.BM_EntityMaster.Add(_obj);
                                entities.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return lstEntities;
        }
        #endregion
    }
}