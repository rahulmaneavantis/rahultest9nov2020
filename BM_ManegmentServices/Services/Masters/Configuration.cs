﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class Configuration : IConfiguration
    {
        public VMConfiguration CreateMeeting(VMConfiguration _objmeeting)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var checkdata = (from row in entities.BM_ConfigurationMaster where row.IsActive == true && row.EntityID == _objmeeting.EntityId && row.MeetingType == _objmeeting.MeetingType select row).FirstOrDefault();
                    if (checkdata == null)
                    {
                        BM_ConfigurationMaster objconf = new BM_ConfigurationMaster();
                        objconf.EntityID = _objmeeting.EntityId;
                        objconf.MeetingType = _objmeeting.MeetingType;

                        if (_objmeeting.MeetingNoType != null && _objmeeting.MeetingNoType == "C")
                        {
                            objconf.MeetingNoType = "C";
                        }
                        else if (_objmeeting.MeetingNoType != null && _objmeeting.MeetingNoType == "FY")
                        {
                            objconf.MeetingNoType = "FY";
                        }
                        if (_objmeeting.MeetingNoType == "FY_wise")
                        {
                            objconf.MeetingNo_Format = "/FY";
                        }
                        else
                        {
                            objconf.MeetingNo_Format = "";
                        }
                        objconf.LastMeetingNo = _objmeeting.MeetingNo;
                        objconf.LastMeetingDate = _objmeeting.MeetingDate;
                        objconf.MeetingFY = _objmeeting.FinancialYear;
                        objconf.IsActive = true;
                        objconf.LastMeetingMinutePageNo = _objmeeting.Minutes_Pageno;
                        objconf.IsCircularMeeting = _objmeeting.IsCircular;
                        if (_objmeeting.IsCircular)
                        {
                            objconf.LastCircularDate = _objmeeting.CircularDate;
                            objconf.LastCircularMinutePageNo = _objmeeting.LastCircularMinutePageNo;
                            objconf.LastCircularNo = _objmeeting.CircularNo;
                            if (_objmeeting.CircularNoType != null && _objmeeting.CircularNoType == "C")
                            {
                                objconf.CircularNoType = "C";
                            }
                            else if (_objmeeting.CircularNoType != null && _objmeeting.CircularNoType == "FY")
                            {
                                objconf.CircularNoType = "FY";
                            }
                            if (_objmeeting.CircularNoType == "FY")
                            {
                                objconf.CircularNo_Format = "/FY";
                            }
                            else
                            {
                                objconf.MeetingNo_Format = "";
                            }

                        }
                        objconf.CircularFY = _objmeeting.FinancialYearforCircural;
                        objconf.CreatedOn = DateTime.Now;
                        entities.BM_ConfigurationMaster.Add(objconf);
                        entities.SaveChanges();

                        #region Update Default virtual meeting in entity Master
                        var _objEntity = (from row in entities.BM_EntityMaster
                                          where row.Id == _objmeeting.EntityId && row.Is_Deleted == false
                                          select row
                                         ).FirstOrDefault();
                        if(_objEntity != null)
                        {
                            _objEntity.DefaultVirtualMeeting = _objmeeting.DefaultVirtualMeeting;
                            entities.SaveChanges();
                        }
                        #endregion

                        _objmeeting.successMessage = true;
                        _objmeeting.errorsuccessMessage = "Saved Successfully.";
                        return _objmeeting;
                    }
                    else
                    {
                        _objmeeting.errorMessage = true;
                        _objmeeting.errorsuccessMessage = "Data already exist";
                        return _objmeeting;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objmeeting.errorMessage = true;
                _objmeeting.errorsuccessMessage = "Server error occurred";
                return _objmeeting;
            }
        }

        public VMConfiguration UpdateMeeting(VMConfiguration _objmeeting)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                   
                        var checkdata = (from row in entities.BM_ConfigurationMaster where row.IsActive == true && row.ID == _objmeeting.conId && row.EntityID == _objmeeting.EntityId && row.MeetingType == _objmeeting.MeetingType select row).FirstOrDefault();
                        if (checkdata != null)
                        {
                            checkdata.EntityID = _objmeeting.EntityId;
                            checkdata.MeetingType = _objmeeting.MeetingType;

                            if (_objmeeting.MeetingNoType != null && _objmeeting.MeetingNoType == "C")
                            {
                                checkdata.MeetingNoType = "C";
                            }
                            else if (_objmeeting.MeetingNoType != null && _objmeeting.MeetingNoType == "FY")
                            {
                                checkdata.MeetingNoType = "FY";
                            }
                            if (_objmeeting.MeetingNoType == "FY")
                            {
                                checkdata.MeetingNo_Format = "/FY";
                            }
                            else
                            {
                                checkdata.MeetingNo_Format = "";
                            }
                            checkdata.LastMeetingNo = _objmeeting.MeetingNo;
                            checkdata.LastMeetingDate = _objmeeting.MeetingDate;
                            checkdata.IsActive = true;
                            checkdata.LastMeetingMinutePageNo = _objmeeting.Minutes_Pageno;
                            checkdata.MeetingFY = _objmeeting.FinancialYear;
                            checkdata.IsCircularMeeting = _objmeeting.IsCircular;
                            if (_objmeeting.IsCircular)
                            {
                                checkdata.CircularFY = _objmeeting.FinancialYearforCircural;
                                checkdata.LastCircularDate = _objmeeting.CircularDate;
                                checkdata.LastCircularMinutePageNo = _objmeeting.LastCircularMinutePageNo;
                                checkdata.LastCircularNo = _objmeeting.CircularNo;
                                if (_objmeeting.CircularNoType != null && _objmeeting.CircularNoType == "C")
                                {
                                    checkdata.CircularNoType = "C";
                                }
                                else if (_objmeeting.CircularNoType != null && _objmeeting.CircularNoType == "FY")
                                {
                                    checkdata.CircularNoType = "FY";
                                }
                                if (_objmeeting.CircularNoType == "FY")
                                {
                                    checkdata.CircularNo_Format = "/FY";
                                }
                                else
                                {
                                    checkdata.MeetingNo_Format = "";
                                }

                            }

                            checkdata.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();

                            #region Update Default virtual meeting in entity Master
                            var _objEntity = (from row in entities.BM_EntityMaster
                                              where row.Id == _objmeeting.EntityId && row.Is_Deleted == false
                                              select row
                                             ).FirstOrDefault();
                            if (_objEntity != null)
                            {
                                _objEntity.DefaultVirtualMeeting = _objmeeting.DefaultVirtualMeeting;
                                entities.SaveChanges();
                            }
                            #endregion

                            _objmeeting.successMessage = true;
                            _objmeeting.errorsuccessMessage = "Updated Successfully.";
                            return _objmeeting;
                        }
                        else
                        {
                            _objmeeting.errorMessage = true;
                            _objmeeting.errorsuccessMessage = "Something went wrong";
                            return _objmeeting;
                        }
                    
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objmeeting.errorMessage = true;
                _objmeeting.errorsuccessMessage = "Server error occured!Please try again";
                return _objmeeting;
            }
        }

        public List<VMConfiguration> getMeetingConfiguration(int EntityId, int custID)
        {
            try
            {
                List<VMConfiguration> _objgetdata = new List<VMConfiguration>();
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (EntityId == 0)
                    {
                        _objgetdata = (from row in entities.BM_EntityMaster
                                       join rows in entities.BM_ConfigurationMaster on row.Id equals rows.EntityID
                                       where row.Is_Deleted == false && rows.IsActive == true
                                       && row.Customer_Id== custID
                                       select new VMConfiguration
                                       {
                                           EntityId = row.Id,
                                           EntityName = row.CompanyName,
                                           YearType = row.FY_CY != "FY" ? "1 Jan - 31 Dec" : "1 Apr - 31 Mar",
                                       }).Distinct().ToList();
                    }
                    else
                    {
                        _objgetdata = (from row in entities.BM_EntityMaster
                                       where row.Is_Deleted == false
                                       where row.Id == EntityId
                                       && row.Customer_Id == custID
                                       select new VMConfiguration
                                       {
                                           EntityId = row.Id,
                                           EntityName = row.CompanyName,
                                           YearType = row.FY_CY != "FY" ? "1 Jan - 31 Dec" : "1 Apr - 31 Mar",
                                       }).Distinct().ToList();
                    }
                    return _objgetdata;
                }
            }
            catch (Exception ex)
            {
                List<VMConfiguration> obj = new List<VMConfiguration>();

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public List<VMConfiguration> getCircularConfiguration()
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _objgetdata = (from row in entities.BM_ConfigurationMaster
                                       where row.IsActive == true
                                       select new VMConfiguration
                                       {
                                           //YearType = row.FYType,
                                           Minutes_Pageno = row.LastMeetingMinutePageNo,
                                           MeetingNoType = row.MeetingNoType,
                                           MeetingNo = row.LastMeetingNo,
                                           MeetingDate = row.LastMeetingDate,
                                           CircularNoType = row.CircularNoType,
                                           EntityName = (from x in entities.BM_EntityMaster where x.Id == row.EntityID && row.IsActive == true select x.CompanyName).FirstOrDefault(),
                                           Meeting_TypeName = (from y in entities.BM_CommitteeComp where y.Id == row.MeetingType select y.MeetingTypeName).FirstOrDefault(),
                                       }).ToList();
                    return _objgetdata;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMConfiguration GetMeetingbyId(int Id)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getmeetingData = (from row in entities.BM_ConfigurationMaster
                                          where row.ID == Id && row.IsActive == true

                                          select new VMConfiguration
                                          {
                                              conId = row.ID,
                                              Id = row.ID,
                                              EntityId = row.EntityID,
                                              MeetingDate = row.LastMeetingDate,
                                              MeetingType = row.MeetingType,
                                              Minutes_Pageno = row.LastMeetingMinutePageNo,
                                              MeetingNoType = row.MeetingNoType,
                                              CircularNoType = row.CircularNoType,
                                              MeetingNo = row.LastMeetingNo,
                                              MeetingNoFormate = row.MeetingNo_Format,
                                              CircularNoformate=row.CircularNo_Format,
                                              FinancialYearforCircural=row.CircularFY,
                                              FinancialYear=row.MeetingFY,
                                              IsCircular = row.IsCircularMeeting,
                                              CircularDate = row.LastCircularDate,
                                              CircularNo = row.LastCircularNo,
                                              LastCircularMinutePageNo=row.LastCircularMinutePageNo,
                                          })
                                          .FirstOrDefault();
                    return getmeetingData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMConfiguration GetCircularbyId(int Id)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getcircularData = (from row in entities.BM_ConfigurationMaster where row.ID == Id && row.IsActive == true select new VMConfiguration { EntityId = row.EntityID, MeetingDate = row.LastMeetingDate, MeetingType = row.MeetingType, MeetingNo = row.LastMeetingNo }).FirstOrDefault();
                    return getcircularData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<CommitteeCompVM> GetAllMeetingType(int entityId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var result = new List<CommitteeCompVM>();

                    result = (from row in entities.BM_CommitteeComp
                              join rows in entities.BM_ConfigurationMaster on row.Id equals rows.MeetingType into gj

                              from x in gj.DefaultIfEmpty()
                              where x.EntityID != entityId
                              orderby row.SerialNo
                              select new CommitteeCompVM()
                              {
                                  Id = row.Id,
                                  Name = row.Name,
                                  MeetingTypeName = row.MeetingTypeName
                              }
                             ).Distinct().ToList();
                    return result;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<VMConfiguration> getConfiguration(int EntityId)
        {
            try
            {
                List<VMConfiguration> _objgetdata = new List<VMConfiguration>();
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {

                    _objgetdata = (from row in entities.BM_ConfigurationMaster
                                   where row.IsActive == true && row.LastMeetingDate != null
                                   && row.EntityID == EntityId
                                   select new VMConfiguration
                                   {
                                       conId = row.ID,
                                       Id = row.ID,
                                       EntityId = row.EntityID,
                                       MeetingDate = row.LastMeetingDate,
                                       EntityName = (from x in entities.BM_EntityMaster where x.Id == row.EntityID && row.IsActive == true select x.CompanyName).FirstOrDefault(),
                                       Meeting_TypeName = (from y in entities.BM_CommitteeComp where y.Id == row.MeetingType select y.MeetingTypeName).FirstOrDefault(),
                                       Minutes_Pageno = row.LastMeetingMinutePageNo,
                                       MeetingNo = row.LastMeetingNo,
                                     
                                       MeetingNoFormate = row.MeetingNo_Format,
                                       FinancialYearforCircural = row.CircularFY != 0 ? row.CircularFY : 0,
                                       FinancialYear = row.MeetingFY != 0 ?row.MeetingFY : 0,
                                       MeetingNoType = row.MeetingNoType,
                                       CircularNoType = row.CircularNoType,
                                   }).Distinct().ToList();
                }
                return _objgetdata;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<VMConfiguration> lastCircular(int EntityId)
        {
            try
            {
                List<VMConfiguration> _objgetdata = new List<VMConfiguration>();
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (EntityId == 0)
                    {
                        _objgetdata = (from row in entities.BM_ConfigurationMaster
                                       where row.IsActive == true && row.IsCircularMeeting == true && row.LastCircularDate != null
                                       select new VMConfiguration
                                       {
                                           conId = row.ID,
                                           Id = row.ID,
                                           EntityId = row.EntityID,
                                           MeetingDate = row.LastMeetingDate,
                                           EntityName = (from x in entities.BM_EntityMaster where x.Id == row.EntityID && row.IsActive == true select x.CompanyName).FirstOrDefault(),
                                           Meeting_TypeName = (from y in entities.BM_CommitteeComp where y.Id == row.MeetingType select y.MeetingTypeName).FirstOrDefault(),
                                           Minutes_Pageno = row.LastCircularMinutePageNo,
                                           //YearType = row.FYType,
                                           MeetingNo = row.LastMeetingNo,
                                           MeetingNoFormate = row.MeetingNo_Format,
                                           IsCircular = row.IsCircularMeeting,
                                           CircularDate = row.LastCircularDate,
                                           CircularNo = row.LastCircularNo
                                       }).ToList();
                    }
                    else
                    {
                        _objgetdata = (from row in entities.BM_ConfigurationMaster
                                       where row.IsActive == true
                                       && row.EntityID == EntityId && row.IsCircularMeeting == true
                                       select new VMConfiguration
                                       {
                                           conId = row.ID,
                                           Id = row.ID,
                                           EntityId = row.EntityID,
                                           MeetingDate = row.LastMeetingDate,
                                           EntityName = (from x in entities.BM_EntityMaster where x.Id == row.EntityID && row.IsActive == true select x.CompanyName).FirstOrDefault(),
                                           Meeting_TypeName = (from y in entities.BM_CommitteeComp where y.Id == row.MeetingType select y.MeetingTypeName).FirstOrDefault(),
                                           Minutes_Pageno = row.LastMeetingMinutePageNo,
                                           //YearType = row.FYType,
                                           MeetingNo = row.LastMeetingNo,
                                           MeetingNoFormate = row.MeetingNo_Format,
                                           IsCircular = row.IsCircularMeeting,
                                           CircularDate = row.LastCircularDate,
                                           CircularNo = row.LastCircularNo
                                       }).Distinct().ToList();
                    }
                    return _objgetdata;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<YearMasterVM> GetAll_FYByEntityID(int EntityID)
        {
            var result = new List<YearMasterVM>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from BEM in entities.BM_EntityMaster
                              join BYM in entities.BM_YearMaster
                              on BEM.FY_CY equals BYM.Type
                              where BYM.IsDeleted == false
                              && BEM.Id == EntityID
                              orderby BYM.FYText descending
                              select new YearMasterVM
                              {
                                  FYID = BYM.FYID,
                                  FYText = BYM.FYText,
                                  Type = BYM.Type
                              }).ToList();

                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return result;
            }
        }

        public bool? GetIsDefaultVirtualMeetingByEntityId(int EntityID)
        {
            bool? result = false;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_EntityMaster
                              where row.Id == EntityID && row.Is_Deleted == false
                              select row.DefaultVirtualMeeting).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
    }
}

//var data = (from u in Users
//            join g in Groups.Where(a => a.UserId == (from gp in Groups.Select(r => r.UserId).Distinct() ))
//           on u.UserId equals g.UserId into outer
//            from x in outer.DefaultIfEmpty()
//            select u);