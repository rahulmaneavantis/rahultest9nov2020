﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface ICompliance
    {
        List<ComplianceVM> GetAllSecretarialCompliances();
        List<ComplianceStatusVM> GetComplianceUpdateStatus(long scheduleOnID, int userID);
        List<ComplianceStatusVM> GetComplianceUpdateStatus(long scheduleOnID, int userID, int? roleID);
        void AddFormitem(FormDetails complianceItem, int userID, int customerID);
        void AddFormDetailsItem(FormDetails complianceItem, int userID, int customerID);
        List<FormDetails> GetFormComplianceMapping(long complianceID);
        void DeleteFormComlianceDelete(FormDetails formItem, int userID, int customerID);


    }
}
