﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Masters
{
    public class AgendaMasterTemplate : IAgendaMasterTemplate
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public IEnumerable<CategoryViewModel> GetCategory()
        {
            var result = entities.BM_TemplateCategory.Where(category => category.IsDeleted == false).Select(category => new CategoryViewModel
            {
                CategoryID = category.CategoryID,
                CategoryName = category.CategoryName,
                CategoryDetails = category.Details

            }).ToList();
            return result;
        }

        public IEnumerable<ImportTemplateViewModel> ImportTemplates(long? AgendaMasterId)
        {
            if(AgendaMasterId == null)
            {
                var test = entities.BM_AgendaMasterTemplate.Where(template => template.IsDeleted == false).Select(template => new ImportTemplateViewModel
                {
                    TemplateName = template.TemplateName,
                    TemplateLabel = template.TemplateLabel,
                    CategoryID = template.CategoryID,
                    CategoryName = template.BM_TemplateCategory.CategoryName,
                    IsSelected = false
                }).Distinct().ToString();

                var result = entities.BM_AgendaMasterTemplate.Where(template => template.IsDeleted == false).Select(template => new ImportTemplateViewModel
                {
                    TemplateName = template.TemplateName,
                    TemplateLabel = template.TemplateLabel,
                    CategoryID = template.CategoryID,
                    CategoryName = template.BM_TemplateCategory.CategoryName,
                    IsSelected = false
                }).Distinct().ToList();
                return result;
            }
            else
            {
                var result = entities.BM_AgendaMasterTemplate.Where(template => template.AgendaID == AgendaMasterId && template.IsDeleted == false).Select(template => new ImportTemplateViewModel
                {
                    TemplateName = template.TemplateName,
                    TemplateLabel = template.TemplateLabel,
                    CategoryID = template.CategoryID,
                    CategoryName = template.BM_TemplateCategory.CategoryName,
                    IsSelected = false
                }).ToList();
                return result;
            }
        }

        public IEnumerable<TemplateViewModel> GetAll(long AgendaMasterId)
        {
                var result = (from template in entities.BM_AgendaMasterTemplate
                             where template.AgendaID == AgendaMasterId &&
                                   template.IsDeleted == false
                             select new TemplateViewModel
                                {
                                    TemplateID = template.TemplateID,
                                    TemplateName = template.TemplateName,
                                    TemplateLabel = template.TemplateLabel,
                                    AgendaID = template.AgendaID,
                                    Category = new CategoryViewModel()
                                    {
                                        CategoryID = template.BM_TemplateCategory.CategoryID,
                                        CategoryName = template.BM_TemplateCategory.CategoryName,
                                        CategoryDetails = template.BM_TemplateCategory.Details
                                    },
                                    IsTemplateField = template.IsTemplateField,
                                    IsUIFormColumn = template.IsUIFormColumn
                                }).ToList();
            return result;
        }

        public long? GetParentId(long? AgendaMasterId)
        {
            try
            {
                return  (from row in entities.BM_AgendaMaster
                        where row.BM_AgendaMasterId == AgendaMasterId && row.IsDeleted == false
                        select row.Parent_Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public TemplateViewModel Create(TemplateViewModel template)
        {
            try
            {
                var isExists = entities.BM_AgendaMasterTemplate.Where(k => k.TemplateID != template.TemplateID && k.TemplateName == template.TemplateName && k.AgendaID == template.AgendaID && k.IsDeleted == false).FirstOrDefault();
                if(isExists == null)
                {
                    var entity = new BM_AgendaMasterTemplate();

                    entity.TemplateName = template.TemplateName;
                    entity.TemplateLabel = template.TemplateLabel;
                    entity.CategoryID = template.CategoryID;
                    entity.AgendaID = template.AgendaID;
                    entity.IsDeleted = false;
                    entity.CreatedBy = template.UserId;
                    entity.CreatedOn = DateTime.Now;

                    entity.IsTemplateField = true;
                    entity.IsUIFormColumn = false;
                    entity.UIFormColumnsID = 0;

                    if (template.Category != null)
                    {
                        entity.CategoryID = template.Category.CategoryID;
                    }

                    entities.BM_AgendaMasterTemplate.Add(entity);
                    entities.SaveChanges();
                    template.TemplateID = entity.TemplateID;
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return template;
        }
        public TemplateViewModel Update(TemplateViewModel template)
        {
            try
            {
                var isExists = entities.BM_AgendaMasterTemplate.Where(k => k.TemplateID != template.TemplateID && k.TemplateName == template.TemplateName && k.AgendaID == template.AgendaID && k.IsDeleted == false).FirstOrDefault();

                if(isExists == null)
                {
                    var obj = entities.BM_AgendaMasterTemplate.Where(k => k.TemplateID == template.TemplateID && k.IsDeleted == false && k.UIFormColumnsID == 0).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.TemplateName = template.TemplateName;
                        obj.TemplateLabel = template.TemplateLabel;
                        obj.CategoryID = template.CategoryID;
                        obj.AgendaID = template.AgendaID;
                        obj.IsDeleted = false;
                        obj.UpdatedBy = template.UserId;
                        obj.UpdatedOn = DateTime.Now;

                        if (template.Category != null)
                        {
                            obj.CategoryID = template.Category.CategoryID;
                        }

                        entities.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return template;
        }
        public TemplateViewModel Delete(TemplateViewModel template)
        {
            try
            {
                var obj = entities.BM_AgendaMasterTemplate.Where(k => k.TemplateID == template.TemplateID && k.IsDeleted == false).FirstOrDefault();

                if (obj != null)
                {
                    if(obj.UIFormColumnsID == 0)
                    {
                        obj.IsDeleted = true;
                        obj.UpdatedBy = template.UserId;
                        obj.UpdatedOn = DateTime.Now;

                        entities.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return template;
        }
    }
}