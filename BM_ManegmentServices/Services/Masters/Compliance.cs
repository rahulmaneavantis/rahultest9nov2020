﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;
using BM_ManegmentServices.VM.Compliance;

namespace BM_ManegmentServices.Services.Masters
{
    public class Compliance : ICompliance
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public void AddFormDetailsItem(FormDetails complianceItem, int userID, int customerID)
        {
            try
            {
                var checkForm = (from row in entities.BM_ComplianceFormMapping
                                 where row.FormID == complianceItem.FormId
                                    && row.ComplianceId == complianceItem.ComplianceId
                                 select row).FirstOrDefault();
                if (checkForm == null)
                {
                    BM_ComplianceFormMapping objcompianceformmapping = new BM_ComplianceFormMapping();
                    objcompianceformmapping.FormID = complianceItem.FormId;
                    objcompianceformmapping.ComplianceId = complianceItem.ComplianceId;
                    objcompianceformmapping.IsDeleted = false;
                    objcompianceformmapping.CreatedBy = userID;
                    objcompianceformmapping.CreatedOn = DateTime.Now;
                    entities.BM_ComplianceFormMapping.Add(objcompianceformmapping);
                    entities.SaveChanges();
                }
                else
                {
                    checkForm.FormID = complianceItem.FormId;
                    checkForm.ComplianceId = complianceItem.ComplianceId;
                    checkForm.IsDeleted = false;
                    checkForm.UpdatedBy = userID;
                    checkForm.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void AddFormitem(FormDetails complianceItem, int userID, int customerID)
        {
            try
            {
                var checkExist = (from row in entities.BM_ComplianceFormMapping
                                  where row.FormID == complianceItem.FormId && row.ComplianceId == complianceItem.ComplianceId && row.IsDeleted == false
                                  select row).FirstOrDefault();
                if (checkExist == null)
                {
                    BM_ComplianceFormMapping objFormMapping = new BM_ComplianceFormMapping();
                    objFormMapping.ComplianceId = complianceItem.ComplianceId;
                    objFormMapping.FormID = (long)complianceItem.FormId;
                    objFormMapping.CreatedOn = DateTime.Now;
                    objFormMapping.CreatedBy = userID;
                    objFormMapping.IsDeleted = false;
                    entities.BM_ComplianceFormMapping.Add(objFormMapping);
                    entities.SaveChanges();
                }
                else
                {
                    checkExist.ComplianceId = complianceItem.ComplianceId;
                    checkExist.FormID = (long)complianceItem.FormId;
                    checkExist.UpdatedOn = DateTime.Now;
                    checkExist.UpdatedBy = userID;
                    checkExist.IsDeleted = false;
                    entities.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

      

        public List<ComplianceVM> GetAllSecretarialCompliances()
        {
            try
            {
                var result = (from row in entities.BM_SP_GetComplianceForAgendaItem()
                              select new ComplianceVM
                              {
                                  ComplianceId = row.ID,
                                  Description = row.ShortDescription
                              }
                          ).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<ComplianceStatusVM> GetComplianceUpdateStatus(long scheduleOnID, int userID)
        {
            try
            {
                var roleID = (from schedule in entities.ComplianceScheduleOns
                              join instance in entities.ComplianceInstances on schedule.ComplianceInstanceID equals instance.ID
                              join assingment in entities.ComplianceAssignments on instance.ID equals assingment.ComplianceInstanceID
                              where schedule.ID == scheduleOnID && assingment.UserID == userID
                              select assingment.RoleID).Max();

                return GetComplianceUpdateStatus(scheduleOnID, userID, roleID);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<ComplianceStatusVM> GetComplianceUpdateStatus(long scheduleOnID, int userID, int? roleID)
        {
            try
            {
                var initialStautID = entities.BM_ComplianceScheduleOnNew.Where(k => k.ScheduleOnID == scheduleOnID).Select(k => k.StatusId).FirstOrDefault();

                var isComplianceAssinged = (from schedule in entities.ComplianceScheduleOns
                                        join instance in entities.ComplianceInstances on schedule.ComplianceInstanceID equals instance.ID
                                        join assingment in entities.ComplianceAssignments on instance.ID equals assingment.ComplianceInstanceID
                                        where schedule.ID == scheduleOnID && assingment.UserID == userID && assingment.RoleID == roleID
                                        select assingment.RoleID).Any();

                if (roleID > 0 && initialStautID > 0 && isComplianceAssinged == true)
                {
                    var result = (from row in entities.ComplianceStatusTransitions
                                  join status in entities.ComplianceStatus on row.FinalStateID equals status.ID
                                  where row.RoleID == roleID && row.InitialStateID == initialStautID
                                  select new ComplianceStatusVM
                                  {
                                      StatusId = status.ID,
                                      StatusName = status.Name
                                  }
                          ).Distinct().ToList();
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<FormDetails> GetFormComplianceMapping(long complianceID)
        {
            try
            {
                var getFormComplianceDetails = (from row in entities.BM_ComplianceFormMapping
                                                where row.ComplianceId == complianceID &&
  row.IsDeleted == false
                                                select new FormDetails
                                                {
                                                    MappingID=row.ComplianceFormMappingID,
                                                    FormId = row.FormID,
                                                    ComplianceId = complianceID,
                                                    FormName = (from F in entities.BM_FormMaster where F.FormID == row.FormID select F.FormName).FirstOrDefault()
                                                }
                                              ).ToList();
                return getFormComplianceDetails;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public void DeleteFormComlianceDelete(FormDetails formItem, int userID, int customerID)
        {
            try
            {
                var checkFormMapping = (from row in entities.BM_ComplianceFormMapping where row.ComplianceFormMappingID == formItem.MappingID select row).FirstOrDefault();
                if(checkFormMapping!=null)
                {
                    checkFormMapping.IsDeleted = true;
                    checkFormMapping.UpdatedOn = DateTime.Now;
                    checkFormMapping.UpdatedBy = userID;
                    entities.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
    }
}