﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IPageAuthentication
    {
        List<VM_pageAuthentication> GetPageAuthentication(int userId,int customerId,int EntityId);
        List<VM_pageAuthentication> UpdateAuthentication(List<VM_pageAuthentication> newComponentTypes,int CustomerId);
        IEnumerable<EntityMasterVM> GetEntityuserWise(int UserId);
        
    }
}
