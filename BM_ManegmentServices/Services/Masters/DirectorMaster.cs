﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.IO;
using System.Reflection;
using iTextSharp.text.pdf;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM.UIForms;
using System.Globalization;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.Compliance;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Configuration;
using BM_ManegmentServices.Services.Setting;

namespace BM_ManegmentServices.Services.Masters
{
    public class DirectorMaster : IDirectorMaster
    {
        IFileData_Service objIFileData_Service;
        IDirectorCompliances objIDirectorCompliances;
        ISettingService objISettingService;
        IUser objIUser;
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public DirectorMaster(IFileData_Service objFileData_Service, IDirectorCompliances objDirectorCompliances, ISettingService objSettingService, IUser objUser)
        {
            objIFileData_Service = objFileData_Service;
            objIDirectorCompliances = objDirectorCompliances;
            objISettingService = objSettingService;
            objIUser = objUser;
        }

        public bool CheckDIN(int customerId, long id, string DIN)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                return entities.BM_DirectorMaster.Where(k => k.Id != id && k.DIN == DIN && k.Customer_Id == customerId && k.Is_Deleted == false && k.IsActive == true).Any();
            }
        }

        public bool CheckofficealEmailID(int customerId, long id, string EmailId_Official)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                return entities.BM_DirectorMaster.Where(k => k.Id != id && k.EmailId_Official == EmailId_Official && k.Customer_Id == customerId && k.Is_Deleted == false).Any();
            }
        }

        public bool CheckEmailID(int customerId, long id, string EmailId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                return entities.BM_DirectorMaster.Where(k => k.Id != id && k.EmailId_Personal == EmailId && k.Customer_Id == customerId && k.Is_Deleted == false).Any();
            }
        }

        public bool CheckMobileNo(int customerId, long id, string MobileNo)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                return entities.BM_DirectorMaster.Where(k => k.Id != id && k.MobileNo == MobileNo && k.Customer_Id == customerId && k.Is_Deleted == false).Any();
            }
        }

        public bool CheckPanNo(int customerId, long id, string Pannumber)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                return entities.BM_DirectorMaster.Where(k => k.Id != id && k.PAN == Pannumber && k.Customer_Id == customerId && k.Is_Deleted == false).Any();
            }
        }

        public List<Director_MasterVM> GetDirectors(int customerId)
        {
            var result = (from row in entities.BM_DirectorMaster
                          where row.Customer_Id == customerId 
                          //&& row.IsDirecor == true 
                          && row.Is_Deleted == false
                          select new Director_MasterVM
                          {
                              #region Assign Values
                              ID = row.Id,
                              Salutation = row.Salutation,

                              DIN = row.DIN,
                              FirstName = row.FirstName,
                              MiddleName = row.MiddleName,
                              LastName = row.LastName,
                              FullName = row.Salutation + " " + row.FirstName + " " + (row.MiddleName == "" ? "" : row.MiddleName) + " " + (row.LastName == null ? "" : row.LastName),
                              DateOfBirth = row.DOB,
                              MobileNo = row.MobileNo,
                              EmailId = row.EmailId_Personal,
                              EmailId_Official = row.EmailId_Official,

                              PAN = row.PAN,
                              Adhaar = row.Aadhaar,
                              PassportNo = row.PassportNo,
                              Photo_Doc_Name = row.Photo_Doc,
                              PAN_Doc_Name = row.PAN_Doc,
                              Aadhaar_Doc_Name = row.Aadhar_Doc,
                              Passport_Doc_Name = row.Passport_Doc,
                              FatherFirstName = row.Father,
                              FatherMiddleName = row.FatherMiddleName,
                              FatherLastName = row.FatherLastName,
                              DESC_ExpiryDate = row.DSC_ExpiryDate,
                              #endregion
                          }
                      ).ToList();
            return result;
        }

        public bool Create(BM_DirectorMaster obj, out bool hasError)
        {
            hasError = false;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    entities.BM_DirectorMaster.Add(obj);
                    int count = entities.SaveChanges();
                    if (count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public Director_MasterVM GetDirectorVM(int customerId, long id)
        {
            var result = (from row in entities.BM_DirectorMaster
                          where row.Id == id && row.Customer_Id == customerId && row.Is_Deleted == false
                          select new Director_MasterVM
                          {
                              #region Assign Values
                              ID = row.Id,
                              Salutation = row.Salutation,

                              DIN = row.DIN,
                              FirstName = row.FirstName,
                              MiddleName = row.MiddleName,
                              LastName = row.LastName,
                              DateOfBirth = row.DOB,
                              Gender = row.Gender,
                              MobileNo = row.MobileNo,
                              EmailId = row.EmailId_Personal,
                              EmailId_Official = row.EmailId_Official,
                              MotherName=row.Mother,
                              #region Residential Address
                              Permenant_Address_Line1 = row.Permenant_Address_Line1,
                              Permenant_Address_Line2 = row.Permenant_Address_Line2,
                              Permenant_StateId = row.Permenant_StateId,
                              Permenant_CityId = row.Permenant_CityId,
                              Permenant_PINCode = row.Permenant_PINCode,

                              IsSameAddress = row.IsSameAddress,

                              Present_Address_Line1 = row.Present_Address_Line1,
                              Present_Address_Line2 = row.Present_Address_Line2,
                              Present_StateId = row.Present_StateId,
                              Present_CityId = row.Present_CityId,
                              Present_PINCode = row.Present_PINCode,
                              #endregion

                              EducationalQualification = row.EducationalQualification,
                              OtherQualification = row.OtherQualification,
                              Occupation = row.Occupation,
                              AreaOfOccupation = row.AreaOfOccupation,
                              OtherOccupation = row.OtherOccupation,
                              ResidentInIndia = row.ResidentInIndia,
                              Nationality = row.Nationality,
                              PAN = row.PAN,
                              Adhaar = row.Aadhaar,
                              PassportNo = row.PassportNo,
                              Photo_Doc_Name = row.Photo_Doc,
                              PAN_Doc_Name = row.PAN_Doc,
                              Aadhaar_Doc_Name = row.Aadhar_Doc,
                              Passport_Doc_Name = row.Passport_Doc,
                              FSalutations = row.FSalutations,
                              FatherFirstName = row.Father,
                              FatherMiddleName = row.FatherMiddleName,
                              FatherLastName = row.FatherLastName,
                              DESC_ExpiryDate = row.DSC_ExpiryDate,
                              IsActive = row.IsActive,
                              IsDetailsOfIntrest = false

                              #endregion
                          }
                          ).FirstOrDefault();
            if (result != null)
            {
                result.IsDetailsOfIntrest = GetDetailsofIntrest(id);
            }

            var checkForMapping = (from x in entities.BM_DirectorTypeOfChanges
                                   join y in entities.BM_DirectorTypeChangesMapping
                                   on x.Id equals y.TypeChangeMaapingID
                                   where x.IsActive == true && y.IsActive == true && x.DirectorId == id && y.DirectorId == id
                                   select new { x, y }).FirstOrDefault();
            if (checkForMapping != null)
            {
                if (checkForMapping.x.Name_of_Director)
                {
                    result.FirstName = checkForMapping.y.FirstName;
                    result.MiddleName = checkForMapping.y.MiddleName;
                    result.LastName = checkForMapping.y.LastName;
                }
                if (checkForMapping.x.FatherName)
                {
                    result.FatherFirstName = checkForMapping.y.FatherFirstName;
                    result.FatherMiddleName = checkForMapping.y.FatherMiddleName;
                    result.FatherLastName = checkForMapping.y.FatherLastName;
                }
                if (checkForMapping.x.DateofBirth)
                {
                    result.DateOfBirth = checkForMapping.y.DOB;

                }
                if (checkForMapping.x.Nationality)
                {
                    result.Nationality = checkForMapping.y.Nationality;

                }
                if (checkForMapping.x.Gender)
                {
                    result.Gender = checkForMapping.y.Gender;

                }
                if (checkForMapping.x.PAN)
                {
                    result.PAN = checkForMapping.y.PAN;

                }
                if (checkForMapping.x.Passport)
                {
                    result.PassportNo = checkForMapping.y.PassportNo;

                }

                if (checkForMapping.x.EmailID)
                {
                    result.EmailId_Official = checkForMapping.y.EmailId_Official;
                    result.EmailId = checkForMapping.y.EmailId_Personal;

                }
                if (checkForMapping.x.Mobile)
                {
                    result.MobileNo = checkForMapping.y.MobileNo;

                }
                if (checkForMapping.x.Parmanent_Address)
                {
                    result.Permenant_Address_Line1 = checkForMapping.y.Permenant_Address_Line1;
                    result.Permenant_Address_Line2 = checkForMapping.y.Permenant_Address_Line2;
                    result.Permenant_StateId = checkForMapping.y.Permenant_StateId;

                    result.Permenant_CityId = checkForMapping.y.Permenant_CityId;
                    result.Permenant_PINCode = checkForMapping.y.Permenant_PINCode;
                }
                if (checkForMapping.x.Present_Address)
                {
                    result.Present_Address_Line1 = checkForMapping.y.Permenant_Address_Line1;
                    result.Present_Address_Line2 = checkForMapping.y.Permenant_Address_Line2;
                    result.Present_StateId = checkForMapping.y.Present_StateId;

                    result.Present_CityId = checkForMapping.y.Present_CityId;
                    result.Present_PINCode = checkForMapping.y.Present_PINCode;
                }
                if (checkForMapping.x.AadharNumber)
                {
                    result.Adhaar = checkForMapping.y.Aadhaar;

                }
                if (checkForMapping.x.Rasidential_Status)
                {
                    result.ResidentInIndia = (bool)checkForMapping.y.ResidentInIndia;

                }

            }
            return result;
        }

        public Director_MasterVM GetManegmentDetails(int customerId, long id)
        {
            var result = (from row in entities.BM_DirectorMaster
                              //join rows in entities.BM_Directors_DetailsOfInterest on row.Id equals rows.Director_Id
                          where row.Id == id && row.Customer_Id == customerId && row.Is_Deleted == false
                          //&& rows.EntityId== EntityId
                          //from designation in entities.BM_Directors_Designation.Where(x => x.Id == rows.IfDirector).DefaultIfEmpty()

                          select new Director_MasterVM
                          {
                              #region Assign Values
                              ID = row.Id,
                              Salutation = row.Salutation,

                              DIN = row.DIN,
                              FirstName = row.FirstName,
                              MiddleName = row.MiddleName,
                              LastName = row.LastName,
                              DateOfBirth = row.DOB,
                              Gender = row.Gender,
                              MobileNo = row.MobileNo,
                              EmailId = row.EmailId_Personal,
                              EmailId_Official = row.EmailId_Official,



                              #region Residential Address
                              Permenant_Address_Line1 = row.Permenant_Address_Line1,
                              Permenant_Address_Line2 = row.Permenant_Address_Line2,
                              Permenant_StateId = row.Permenant_StateId,
                              Permenant_CityId = row.Permenant_CityId,
                              Permenant_PINCode = row.Permenant_PINCode,

                              IsSameAddress = row.IsSameAddress,

                              Present_Address_Line1 = row.Present_Address_Line1,
                              Present_Address_Line2 = row.Present_Address_Line2,
                              Present_StateId = row.Present_StateId,
                              Present_CityId = row.Present_CityId,
                              Present_PINCode = row.Present_PINCode,
                              #endregion

                              EducationalQualification = row.EducationalQualification,
                              OtherQualification = row.OtherQualification,
                              Occupation = row.Occupation,
                              AreaOfOccupation = row.AreaOfOccupation,
                              OtherOccupation = row.OtherOccupation,
                              ResidentInIndia = row.ResidentInIndia,
                              Nationality = row.Nationality,
                              PAN = row.PAN,
                              Adhaar = row.Aadhaar,
                              PassportNo = row.PassportNo,
                              Photo_Doc_Name = row.Photo_Doc,
                              PAN_Doc_Name = row.PAN_Doc,
                              Aadhaar_Doc_Name = row.Aadhar_Doc,
                              Passport_Doc_Name = row.Passport_Doc,
                              FSalutations = row.FSalutations,
                              FatherFirstName = row.Father,
                              FatherMiddleName = row.FatherMiddleName,
                              FatherLastName = row.FatherLastName,
                              DESC_ExpiryDate = row.DSC_ExpiryDate,
                              IsActive = row.IsActive,

                              #endregion
                          }).FirstOrDefault();

            return result;
        }

        public bool Update_DirectorDetails(BM_DirectorMaster obj, out bool hasError)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    entities.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    int count = entities.SaveChanges();
                    if (count > 0)
                    {
                        hasError = false;
                        return true;
                    }
                    else
                    {
                        hasError = false;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public bool SetUserId(long DirectorID, long UserId, out bool hasError)
        {
            var value = false;
            hasError = false;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var result = entities.BM_DirectorMaster.Find(DirectorID);
                    if (result != null)
                    {
                        result.UserID = UserId;
                        result.UpdatedOn = DateTime.Now;
                        entities.Entry(result).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                        value = true;
                    }
                    else
                    {
                        hasError = true;
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return value;
        }
        public bool Update_DirectorDoc(BM_DirectorMaster obj)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                entities.BM_DirectorMaster.Attach(obj);
                if (!string.IsNullOrEmpty(obj.Photo_Doc))
                {
                    entities.Entry(obj).Property(k => k.Photo_Doc).IsModified = true;
                }
                if (!string.IsNullOrEmpty(obj.PAN_Doc))
                {
                    entities.Entry(obj).Property(k => k.PAN_Doc).IsModified = true;
                }
                if (!string.IsNullOrEmpty(obj.Aadhar_Doc))
                {
                    entities.Entry(obj).Property(k => k.Aadhar_Doc).IsModified = true;
                }
                if (!string.IsNullOrEmpty(obj.Passport_Doc))
                {
                    entities.Entry(obj).Property(k => k.Passport_Doc).IsModified = true;
                }

                int count = entities.SaveChanges();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public BM_DirectorMaster GetDirector(int customerId, long id)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                return entities.BM_DirectorMaster.Where(k => k.Id == id && k.Customer_Id == customerId && k.Is_Deleted == false).FirstOrDefault();
            }
        }

        public bool Delete(long id, out bool hasError)
        {
            var value = false;
            hasError = false;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var result = entities.BM_DirectorMaster.Find(id);
                    if (result != null)
                    {
                        result.Is_Deleted = true;
                        result.UpdatedOn = DateTime.Now;
                        entities.Entry(result).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                        value = true;
                    }
                    else
                    {
                        hasError = true;
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return value;
        }

        public List<DirectorMasterListVM> DirectorMasterList(int customerId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                return (from row in entities.BM_DirectorMaster
                        where row.Customer_Id == customerId
                        && row.IsDirecor == true
                        && row.Is_Deleted == false
                        orderby row.FirstName
                        select new DirectorMasterListVM
                        {
                            ID = row.Id,
                            Name = (row.Salutation == null ? "" : row.Salutation + " ") + row.FirstName + (row.LastName == null ? "" : " " + row.LastName)
                        }).ToList();
            }
        }

        public List<DirectorMasterListVM> BOD(int EntityId, int customerId, bool showAllOption)
        {
            var result = new List<DirectorMasterListVM>();

            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                result = (from director in entities.BM_DirectorMaster
                          join DOI in entities.BM_Directors_DetailsOfInterest on director.Id equals DOI.Director_Id
                          join NOI in entities.BM_Directors_NatureOfInterest on DOI.NatureOfInterest equals NOI.Id
                          //join designation in entities.BM_Directors_Designation on row.IfDirector equals designation.Id
                          where DOI.EntityId == EntityId
                          && director.Customer_Id == customerId
                          && NOI.IsDirector == true
                          //&& (row.NatureOfInterest == 2 || row.NatureOfInterest == 9 || row.NatureOfInterest == 10)
                          && director.Is_Deleted == false
                          && DOI.IsDeleted == false
                          && DOI.IsActive == true
                          select new DirectorMasterListVM
                          {
                              ID = DOI.Director_Id,
                              Name = (director.Salutation == null ? "" : director.Salutation + " ")
                                    + director.FirstName
                                    + (director.MiddleName == null ? "" : " " + director.MiddleName)
                                    + (director.LastName == null ? "" : " " + director.LastName)
                          }).ToList();

                if (showAllOption)
                    result.Insert(0, new DirectorMasterListVM { ID = 0, Name = "All" });
            }

            return result;
        }

        public List<DirectorMasterListVM> GetManagementEntityWise(int entityID, int customerID, bool showAllOption)
        {
            var result = new List<DirectorMasterListVM>();

            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                result = (from director in entities.BM_SP_GetManagementEntityWise(entityID, customerID)                          
                          orderby director.FullName
                          select new DirectorMasterListVM
                          {
                              //ID = director.Director_Id,
                              ID = director.DetailsOfInterestID,                              
                              Name = director.FullName + " - [" + director.DesignationName + "]"                              
                          }).ToList();

                if (showAllOption)
                    result.Insert(0, new DirectorMasterListVM { ID = 0, Name = "All" });
            }

            return result;
        }

        #region Get Relatives
        public List<Relatives> GetRelatives(int id)
        {
            return new List<Relatives>();
        }

        public List<Relatives> GetRelative_Son(long id)
        {
            return GetRelative(id, "So");
        }

        public List<Relatives> GetRelative_Daughter(long id)
        {
            return GetRelative(id, "D");
        }

        public List<Relatives> GetRelative_Brother(long id)
        {
            return GetRelative(id, "B");
        }

        public List<Relatives> GetRelative_Sister(long id)
        {
            return GetRelative(id, "Si");
        }

        public List<Relatives> GetRelative(long id, string type)
        {
            List<Relatives> result = new List<VM.Relatives>();
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var obj_BM_Directors_Relatives = entities.BM_Directors_Relatives.Where(k => k.Director_Id == id && k.Relative_Type == type).OrderBy(k => k.Id);
                if (obj_BM_Directors_Relatives != null)
                {
                    foreach (var item in obj_BM_Directors_Relatives)
                    {
                        result.Add(new Relatives() { Id = item.Id, Director_ID = item.Director_Id, Minor_or_Adult = item.IsMarried, Name = item.Name, MaritalStatus = item.IsMarried, Spouse = item.Spouse_Name });
                    }
                }
            }
            return result;
        }

        public IEnumerable<Relatives> GetAllRelativeId(long Director_Id)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var ListofRelative = (from row in entities.BM_Directors_Relatives
                                      where row.Director_Id == Director_Id && row.IsDeleted == false
                                      select new Relatives
                                      {
                                          Id = row.Id,
                                          Name = row.Name
                                      }
                                      ).ToList();

                return ListofRelative;
            }
        }

        public IEnumerable<Relatives> GetRelativeForDropdown(long id)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var ListofRelative = (from row in entities.BM_Directors_Relatives
                                      where row.Director_Id == id && row.IsDeleted == false
                                      orderby row.Name
                                      select new Relatives
                                      {
                                          Id = row.Id,
                                          Name = row.Name
                                      }
                                      ).ToList();
                if (ListofRelative == null)
                {
                    ListofRelative = new List<Relatives>();
                }
                //ListofRelative.Insert(0, new Relatives() { Id = 0, Name = "Select" });

                return ListofRelative;
            }
        }

        #endregion

        #region Create or Update
        public bool UpdateRelative(long Director_Id, string huf, string Spouse, string Father, string Mother, out bool hasError)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var obj_Director = entities.BM_DirectorMaster.Where(k => k.Id == Director_Id).FirstOrDefault();
                    if (obj_Director != null)
                    {
                        obj_Director.HUF = huf;
                        obj_Director.Spouse = Spouse;
                        obj_Director.Father = Father;
                        obj_Director.Mother = Mother;
                        entities.Entry(obj_Director).State = System.Data.Entity.EntityState.Modified;
                        hasError = false;
                        return (entities.SaveChanges() > 0) ? true : false;
                    }
                    else
                    {
                        hasError = false;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public bool CreateRelative_Son(List<Relatives> objlstRelatives, long Director_Id, out bool hasError)
        {
            return CreateRelative(objlstRelatives, Director_Id, "So", out hasError);
        }
        public bool CreateRelative_Daughter(List<Relatives> objlstRelatives, long Director_Id, out bool hasError)
        {
            return CreateRelative(objlstRelatives, Director_Id, "D", out hasError);
        }
        public bool CreateRelative_Brother(List<Relatives> objlstRelatives, long Director_Id, out bool hasError)
        {
            return CreateRelative(objlstRelatives, Director_Id, "B", out hasError);
        }
        public bool CreateRelative_Sister(List<Relatives> objlstRelatives, long Director_Id, out bool hasError)
        {
            return CreateRelative(objlstRelatives, Director_Id, "Si", out hasError);
        }
        public bool CreateRelative(List<Relatives> objlstRelatives, long Director_Id, string type, out bool hasError)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    foreach (var item in objlstRelatives)
                    {
                        BM_Directors_Relatives obj = new BM_Directors_Relatives();
                        obj.Director_Id = Director_Id;
                        obj.Id = item.Id;
                        obj.Name = item.Name;
                        obj.IsMinor = item.Minor_or_Adult;
                        obj.IsMarried = item.MaritalStatus;
                        obj.Spouse_Name = item.Spouse;
                        obj.Relative_Type = type;
                        if (obj.Id < 1)
                        {
                            entities.BM_Directors_Relatives.Add(obj);
                            entities.SaveChanges();
                        }
                        else
                        {
                            entities.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                            entities.SaveChanges();
                        }
                    }
                }
                hasError = false;
                return true;
            }
            catch (Exception ex)
            {
                hasError = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }

        public bool DeleteRelative(long id, long Director_Id)
        {
            var result = false;
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var obj = entities.BM_Directors_Relatives.Where(k => k.Id == id && k.Director_Id == Director_Id).FirstOrDefault();
                if (obj != null)
                {
                    entities.Entry(obj).State = System.Data.Entity.EntityState.Deleted;
                    entities.SaveChanges();
                    result = true;
                }
            }
            return result;
        }
        #endregion

        public bool UpdateIsPositionHeld(long Director_Id, bool value)
        {
            var result = false;
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var obj = entities.BM_DirectorMaster.Where(k => k.Id == Director_Id).FirstOrDefault();
                obj.IsPositionHeld = value;
                entities.SaveChanges();
                result = true;
            }
            return result;
        }

        public string CheckChairpersonExists(DetailsOfCommiteePosition item, long Director_ID, out bool hasError)
        {
            hasError = false;
            var data = "";
            var result = false;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = entities.BM_Directors_DetailsOfCommiteePosition.Where(k => k.Committee_Id == item.Committee_Id && k.EntityId == item.Entity_Id_Committee && k.Director_Id == item.Director_ID && k.Id != item.Id && k.IsDeleted == false).Any();
                    if (result)
                    {
                        data = "Director already Present in Committee.";
                    }
                    else if (item.Designation_Id == 1)
                    {
                        result = entities.BM_Directors_DetailsOfCommiteePosition.Where(k => k.Committee_Id == item.Committee_Id && k.EntityId == item.Entity_Id_Committee && k.Designation_Id == 1 && k.Id != item.Id && k.IsDeleted == false).Any();
                        if (result)
                        {
                            data = "Chairperson Present in Committee.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                data = "Server error ocurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return data;
        }
        public bool CreateDetailsOfCommitteePosition(DetailsOfCommiteePosition item, long Director_ID, out bool hasError)
        {
            hasError = false;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    BM_Directors_DetailsOfCommiteePosition obj = new BM_Directors_DetailsOfCommiteePosition();
                    obj.Id = item.Id;

                    if (obj.Id < 1)
                    {
                        obj.Director_Id = Director_ID;
                        obj.EntityId = item.Entity_Id_Committee;
                        obj.Committee_Id = item.Committee_Id;
                        obj.NameOfOtherCommittee = item.NameOfOtherCommittee;
                        obj.Designation_Id = item.Designation_Id;
                        obj.Committee_Type = item.Committee_Type;

                        obj.IsDeleted = false;
                        obj.CreatedBy = item.CreatedBy;
                        obj.CreatedOn = item.CreatedOn;

                        entities.BM_Directors_DetailsOfCommiteePosition.Add(obj);
                        entities.SaveChanges();
                    }
                    else
                    {
                        obj = entities.BM_Directors_DetailsOfCommiteePosition.Where(k => k.Id == item.Id && k.IsDeleted == false).FirstOrDefault();

                        obj.Director_Id = Director_ID;
                        obj.EntityId = item.Entity_Id_Committee;
                        obj.Committee_Id = item.Committee_Id;
                        obj.NameOfOtherCommittee = item.NameOfOtherCommittee;
                        obj.Designation_Id = item.Designation_Id;
                        obj.Committee_Type = item.Committee_Type;

                        obj.IsDeleted = false;
                        obj.UpdatedBy = item.UpdatedBy;
                        obj.UpdatedOn = item.UpdatedOn;

                        entities.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
            return true;
        }

        public bool DeleteDetailsOfCommitteePosition(long id, int UserId, out bool hasError)
        {
            var value = false;
            hasError = false;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var result = entities.BM_Directors_DetailsOfCommiteePosition.Find(id);
                    if (result != null)
                    {
                        result.IsDeleted = true;
                        result.UpdatedOn = DateTime.Now;
                        result.UpdatedBy = UserId;
                        entities.Entry(result).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                        value = true;
                    }
                    else
                    {
                        hasError = true;
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return value;
        }

        public List<DirectorList_ForEntity> GetDirectorForEntity(int customerId, int Entity_Id, int? CommitteeId, int? DetailsOfCommitteeId)
        {
            List<DirectorList_ForEntity> result = new List<DirectorList_ForEntity>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    DetailsOfCommitteeId = DetailsOfCommitteeId == null ? 0 : DetailsOfCommitteeId;

                    if (DetailsOfCommitteeId != 0)
                    {
                        result = (from director in entities.BM_DirectorMaster
                                  join row in entities.BM_Directors_DetailsOfInterest on director.Id equals row.Director_Id
                                  join NOI in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals NOI.Id
                                  join designation in entities.BM_Directors_Designation on row.IfDirector equals designation.Id
                                  where row.EntityId == Entity_Id
                                  && director.Customer_Id == customerId
                                  //&& (row.NatureOfInterest == 2 || row.NatureOfInterest == 9 || row.NatureOfInterest == 10)
                                  && NOI.IsDirector == true
                                  && director.Is_Deleted == false
                                  && row.IsDeleted == false
                                  && row.IsActive == true
                                  && row.OriginalDirector_Id >= 0
                                  select new DirectorList_ForEntity
                                  {
                                      Entity_Id = row.EntityId,
                                      Director_ID = row.Director_Id,
                                      UserID = director.UserID,
                                      DIN = director.DIN,
                                      Salutation = director.Salutation,
                                      FirstName = director.FirstName,
                                      MiddleName = director.MiddleName != null ? director.MiddleName : "",
                                      LastName = director.LastName,
                                      Designation = designation.Name,
                                      DesignationId = designation.Id,
                                      PositionId = (row.IfDirector == 1 || row.IfDirector == 4) ? 1 : 2,
                                      ImagePath = director.Photo_Doc,
                                      DSC_ExpiryDate = director.DSC_ExpiryDate
                                  }).ToList();
                    }
                    else
                    {
                        result = (from director in entities.BM_DirectorMaster
                                  join row in entities.BM_Directors_DetailsOfInterest on director.Id equals row.Director_Id
                                  join NOI in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals NOI.Id
                                  join designation in entities.BM_Directors_Designation on row.IfDirector equals designation.Id
                                  from commitee in entities.BM_Directors_DetailsOfCommiteePosition.Where(k => k.EntityId == Entity_Id && k.Committee_Id == CommitteeId && k.Director_Id == row.Director_Id && k.IsDeleted == false).DefaultIfEmpty()
                                  where row.EntityId == Entity_Id
                                  && director.Customer_Id == customerId
                                  //&& (row.NatureOfInterest == 2 || row.NatureOfInterest == 9 || row.NatureOfInterest == 10)
                                  && NOI.IsDirector == true
                                  && director.Is_Deleted == false
                                  && row.IsDeleted == false
                                  && row.IsActive == true
                                  select new DirectorList_ForEntity
                                  {
                                      Entity_Id = commitee.IsDeleted == null ? 0 : row.EntityId,
                                      Director_ID = row.Director_Id,
                                      UserID = director.UserID,
                                      DIN = director.DIN,
                                      Salutation = director.Salutation,
                                      FirstName = director.FirstName,
                                      MiddleName = director.MiddleName != null ? director.MiddleName : "",
                                      LastName = director.LastName,
                                      Designation = designation.Name,
                                      DesignationId = designation.Id,
                                      PositionId = (row.IfDirector == 1 || row.IfDirector == 4) ? 1 : 2,
                                      ImagePath = director.Photo_Doc,
                                      DSC_ExpiryDate = director.DSC_ExpiryDate
                                  }).Where(k => k.Entity_Id == 0).ToList();
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<DirectorList_ForEntity> GetCommitteeMember(int customerId, int Entity_Id, int CommitteeId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = (from director in entities.BM_DirectorMaster
                              join details in entities.BM_Directors_DetailsOfInterest on director.Id equals details.Director_Id
                              join nature in entities.BM_Directors_NatureOfInterest on details.NatureOfInterest equals nature.Id
                              join row in entities.BM_Directors_DetailsOfCommiteePosition on director.Id equals row.Director_Id
                              join row_e in entities.BM_EntityMaster on row.EntityId equals row_e.Id
                              join committee in entities.BM_CommitteeComp on row.Committee_Id equals committee.Id
                              join designation in entities.BM_Directors_Designation on row.Committee_Type equals designation.Id
                              where row.EntityId == Entity_Id
                              && director.Customer_Id == customerId
                              && row.Committee_Id == CommitteeId
                              && director.Is_Deleted == false
                              && details.IsActive == true
                              && row.IsDeleted == false
                              && nature.IsDirector == true
                              orderby row.Designation_Id
                              select new DirectorList_ForEntity
                              {
                                  Entity_Id = row.EntityId,
                                  Director_ID = director.Id,
                                  UserID = director.UserID,

                                  DIN = director.DIN,
                                  Salutation = director.Salutation,
                                  FirstName = director.FirstName,
                                  MiddleName = director.MiddleName,
                                  LastName = director.LastName,
                                  Designation = designation.Name,
                                  DesignationId = designation.Id,
                                  PositionId = row.Designation_Id, // 1 Chairman / 2 Person
                                  ImagePath = director.Photo_Doc,
                                  DSC_ExpiryDate = (DateTime)director.DSC_ExpiryDate
                              }).ToList();
                return result;
            }
        }

        public List<CommitteeList_ForEntity> GetCommitteeForEntity(int customerId, int Entity_Id)
        {
            List<CommitteeList_ForEntity> lstDetailsOfCommittee_Grid = new List<CommitteeList_ForEntity>();
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = from director in entities.BM_DirectorMaster
                             join details in entities.BM_Directors_DetailsOfInterest on director.Id equals details.Director_Id
                             join row in entities.BM_Directors_DetailsOfCommiteePosition on director.Id equals row.Director_Id
                             join committee in entities.BM_CommitteeComp on row.Committee_Id equals committee.Id
                             where row.EntityId == Entity_Id && director.Customer_Id == customerId
                                && director.Is_Deleted == false && details.IsActive == true && row.IsDeleted == false
                             select new CommitteeList_ForEntity
                             {
                                 Id = row.Id,
                                 Entity_Id = row.EntityId,
                                 Committee_Id = committee.Id,
                                 CommitteeName = committee.Name,
                                 Director_ID = row.Director_Id,
                                 DIN = director.DIN,
                                 Salutation = director.Salutation,
                                 FirstName = director.FirstName,
                                 MiddleName = director.MiddleName,
                                 LastName = director.LastName,
                                 Designation = row.Designation_Id.ToString(),
                                 ImagePath = director.Photo_Doc,
                                 DSC_ExpiryDate = (DateTime)director.DSC_ExpiryDate,

                                 Designation_Id = row.Designation_Id,
                                 Committee_Type = row.Committee_Type
                             };
                foreach (var item in result)
                {
                    lstDetailsOfCommittee_Grid.Add(item);
                }
            }
            return lstDetailsOfCommittee_Grid;
        }

        public List<CommitteeMasterVM> GetCommitteeMasterNew(int customerId, int Entity_Id)
        {
            //List<CommitteeMasterVM> lstDetailsOfCommittee_Grid = new List<CommitteeMasterVM>();
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = (from director in entities.BM_DirectorMaster
                              join details in entities.BM_Directors_DetailsOfInterest on director.Id equals details.Director_Id
                              join row in entities.BM_Directors_DetailsOfCommiteePosition on director.Id equals row.Director_Id
                              join row_e in entities.BM_EntityMaster on row.EntityId equals row_e.Id
                              join committee in entities.BM_CommitteeComp on row.Committee_Id equals committee.Id
                              where row.EntityId == Entity_Id && director.Customer_Id == customerId
                                 && director.Is_Deleted == false && details.IsActive == true && row.IsDeleted == false
                              select new CommitteeMasterVM
                              {
                                  Id = row.Id,
                                  Entity_Id = row.EntityId,
                                  EntityName = row_e.CompanyName,
                                  Director_Id = row.Director_Id,
                                  DirectorName = (string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation + " ") + director.FirstName + (string.IsNullOrEmpty(director.LastName) ? "" : " " + director.LastName),
                                  ImagePath = director.Photo_Doc,
                                  Designation_Id = row.Designation_Id,
                                  Committee_Id = row.Committee_Id,
                                  CommitteeName = committee.Name,
                                  Committee_Type = row.Committee_Type
                              }).ToList();
                //foreach (var item in result)
                //{
                //    lstDetailsOfCommittee_Grid.Add(item);
                //}
                return result;
            }
            //return lstDetailsOfCommittee_Grid;
        }

        public List<CommitteeMasterVM> GetCommitteeMasterNew(int customerId, int Entity_Id, int CommitteeId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = (from row in entities.BM_SP_GetCommitteeOrCompositionEntitywise(Entity_Id, customerId, CommitteeId)
                              select new CommitteeMasterVM
                              {
                                  Id = row.Id,
                                  Entity_Id = row.EntityId,
                                  EntityName = row.CompanyName,
                                  Director_Id = row.Director_Id,
                                  DirectorName = row.FullName,
                                  //DirectorName = (string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation + " ") + "  " + director.FirstName + " " + (string.IsNullOrEmpty(director.MiddleName) ? "" : director.MiddleName) + " " + director.LastName,
                                  ImagePath = row.Photo_Doc,
                                  Designation_Id = row.Designation_Id,
                                  Committee_Id = row.Committee_Id,
                                  CommitteeName = row.Name,
                                  Committee_Type = row.Committee_Type,
                                  Committee_Type_Name = row.Name,
                                  FirstName = row.FirstName,
                                  LastName = row.LastName
                              }).Distinct().ToList();
                return result;

                //var result = (from director in entities.BM_DirectorMaster
                //              join row in entities.BM_Directors_DetailsOfCommiteePosition on director.Id equals row.Director_Id
                //              join details in entities.BM_Directors_DetailsOfInterest on director.Id equals details.Director_Id
                //              join row_e in entities.BM_EntityMaster on row.EntityId equals row_e.Id
                //              join committee in entities.BM_CommitteeComp on row.Committee_Id equals committee.Id
                //              join designation in entities.BM_Directors_Designation on row.Committee_Type equals designation.Id
                //              where row.EntityId == Entity_Id && director.Customer_Id == customerId && row.Committee_Id == CommitteeId
                //                    && director.Is_Deleted == false && details.IsActive == true && row.IsDeleted == false
                //              orderby row.Designation_Id
                //              select new CommitteeMasterVM
                //              {
                //                  Id = row.Id,
                //                  Entity_Id = row.EntityId,
                //                  EntityName = row_e.CompanyName,
                //                  Director_Id = row.Director_Id,
                //                  DirectorName = (string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation + " ") + "  " + director.FirstName + " " + (string.IsNullOrEmpty(director.MiddleName) ? "" : director.MiddleName) + " " + director.LastName,
                //                  ImagePath = director.Photo_Doc,
                //                  Designation_Id = row.Designation_Id,
                //                  Committee_Id = row.Committee_Id,
                //                  CommitteeName = committee.Name,
                //                  Committee_Type = row.Committee_Type,
                //                  Committee_Type_Name = designation.Name,
                //                  FirstName = director.FirstName,
                //                  LastName = director.LastName
                //              }).Distinct().ToList();
                //return result;
            }
        }



        public string AddMember(CommitteeMasterVM _obj, int userID, int customerId)
        {
            string Message = string.Empty;
            var hasError = false;
            try
            {
                DetailsOfCommiteePosition d = new DetailsOfCommiteePosition();

                if (_obj.Committee_Id == -1 && _obj.NameOfOtherCommittee != null)
                {
                    var checkothercommitee = (from row in entities.BM_CommitteeComp where row.Name == _obj.NameOfOtherCommittee && row.Customer_Id == customerId select row).FirstOrDefault();
					//var checkothercommitee = (from row in entities.BM_CommitteeComp where row.Name == _obj.NameOfOtherCommittee select row).FirstOrDefault();
                    if (checkothercommitee == null)
                    {
                        BM_CommitteeComp _objbmcommitycom = new BM_CommitteeComp();
                        _objbmcommitycom.Name = _obj.NameOfOtherCommittee;
                        _objbmcommitycom.IsDeleted = false;
                        _objbmcommitycom.Customer_Id = customerId;
                        _objbmcommitycom.CreatedOn = DateTime.Now;
                        _objbmcommitycom.CreatedBy = userID;
                        entities.BM_CommitteeComp.Add(_objbmcommitycom);
                        entities.SaveChanges();
                    }
                    else
                    {

                    }
                }
                if (_obj.Committee_Id == -1 && _obj.NameOfOtherCommittee != null)
                {
                    var getotherscommiteeId = (from row in entities.BM_CommitteeComp where row.Name == _obj.NameOfOtherCommittee && row.Customer_Id == customerId && row.IsDeleted == false select row.Id).FirstOrDefault();

                    d.Entity_Id_Committee = _obj.Entity_Id;
                    d.Committee_Id = getotherscommiteeId;
                    if (getotherscommiteeId > 0)
                        d.Director_ID = _obj.Director_Id;
                    d.Designation_Id = _obj.Designation_Id;
                    d.Committee_Type = _obj.Committee_Type;
                    d.IsDeleted = false;
                    d.CreatedBy = userID;
                    d.CreatedOn = DateTime.Now;
                }
                else
                {
                    d.Entity_Id_Committee = _obj.Entity_Id;
                    d.Committee_Id = _obj.Committee_Id;
                    d.Director_ID = _obj.Director_Id;
                    d.Designation_Id = _obj.Designation_Id;
                    d.Committee_Type = _obj.Committee_Type;
                    d.IsDeleted = false;
                    d.CreatedBy = userID;
                    d.CreatedOn = DateTime.Now;
                }

                var data = CheckChairpersonExists(d, _obj.Director_Id, out hasError);

                if (hasError)
                {
                    Message = data;

                }
                else if (string.IsNullOrEmpty(data))
                {
                    hasError = false;
                    var result = CreateDetailsOfCommitteePosition(d, _obj.Director_Id, out hasError);

                    if (hasError)
                    {
                        Message = data;

                    }
                    else if (result)
                    {
                        Message = "Saved Successfully.";

                    }
                    else
                    {
                        Message = data;

                    }
                }
                else
                {
                    Message = data;

                }

                return Message;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Message = "Server Error occurred";
            }
        }


        IEnumerable<NatureOfInterestVM> IDirectorMaster.GetNatureOfInterestByEntityType(int entityType)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var result = (from natureOfInterest in entities.BM_Directors_NatureOfInterest
                              join E_natureOfInterest in entities.BM_EntityType_NatureOfInterest on natureOfInterest.Id equals E_natureOfInterest.NatureOfInterestId
                              where E_natureOfInterest.EntityType == entityType
                              orderby natureOfInterest.Name
                              select new NatureOfInterestVM
                              {
                                  Id = natureOfInterest.Id,
                                  Name = natureOfInterest.Name,
                                  IsDirector = natureOfInterest.IsDirector,
                                  IsMNGT = natureOfInterest.IsMNGT
                              }).ToList();
                return result;
            }
        }

        #region Details of Intrest
        public List<DetailsOfInterest> DetailsOfIntrestList(long id, int directorId)
        {
            try
            {
                var result = (from row in entities.BM_Directors_DetailsOfInterest
                              join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                              join rows in entities.BM_EntityType on entity.Entity_Type equals rows.Id
                              //join rows in entities.BM_EntityType on row.Entity_Type equals rows.Id
                              join nature in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals nature.Id
                              //from entity in entities.BM_EntityMaster.Where(k => k.Id == row.EntityId).DefaultIfEmpty()
                              from ifDir in entities.BM_Directors_Designation.Where(k => k.Id == row.IfDirector).DefaultIfEmpty()
                              from ifRelative in entities.BM_Directors_Relatives.Where(k => k.Id == row.IfInterestThroughRelatives && k.Director_Id == row.Director_Id && k.IsActive == true && k.IsDeleted == false).DefaultIfEmpty()
                              from designation in entities.BM_DesignationMaster.Where(k => k.ID == row.DirectorDesignationId).DefaultIfEmpty()
                              where row.Director_Id == id && row.IsDeleted == false 
                              //Commented on 25 Sep 2020 due to display resigned director till take note on agenda in board.
                              //&& row.IsActive == true
                              orderby row.NatureOfInterest == 7 ? 1 : 0, row.EntityId == -1 ? row.NameOfOther : entity.CompanyName
                              select new DetailsOfInterest
                              {
                                  Director_ID = row.Director_Id,
                                  Id = row.Id,
                                  EntityName = row.EntityId == -1 ? row.NameOfOther : entity.CompanyName,
                                  EntityType = rows.EntityName,
                                  CIN = row.EntityId == -1 ? row.CIN : entity.CIN_LLPIN,
                                  PAN = row.EntityId == -1 ? row.PAN : entity.PAN,
                                  NatureofIntrest = nature.Name,

                                  Entity_Id = row.EntityId,
                                  IsOtherEntity = entity.Customer_Id == null ? true : false,
                                  TypeOfEntity = entity.Entity_Type,
                                  NatureOfInterest = row.NatureOfInterest,
                                  IfDirector = row.IfDirector,
                                  IfInterestThroughRelatives = row.IfInterestThroughRelatives,
                                  PercentagesOfShareHolding = row.PercentagesOfShareHolding,
                                  Arosed = row.ArosedOrChangedDate,
                                  BoardofResolution = row.DateOfResolution,
                                  DateofAppointment = row.DateOfAppointment,
                                  IfDirectorName = ifDir.Name,
                                  IfInterestThroughRelativesName = ifRelative.Name,
                                  NumberofShares = row.NumberofSharesHeld,
                                  DirectorDesignationId = row.DirectorDesignationId,
                                  DirectorDesignationName = designation.DesignationName,
                                  IsResigned = row.IsResigned,
                                  CessationEffectFrom = row.CessationEffectFrom,
                                  IsActivated = row.IsActive
                              }).ToList();

                foreach (var item in result)
                {
                    var data = (from row in entities.BM_Directors_Relatives
                                join details in entities.BM_Directors_DetailsOfInterest_RelativeMapping on row.Id equals details.RelativeId
                                where details.DetailsOfInterestID == item.Id && details.IsDeleted == false
                                select new Relatives
                                {
                                    Id = row.Id,
                                    Director_ID = row.Director_Id,
                                    Name = row.Name,
                                    Minor_or_Adult = row.IsMinor,
                                    MaritalStatus = row.IsMarried,
                                }).ToList();
                    item.IfRelatives = data;
                    var relativenames = "";
                    foreach (var s in data)
                    {
                        relativenames += s.Name + ",";
                    }
                    item.IfInterestThroughRelativesName = relativenames.TrimEnd(',');
                }
                return result;
            }
            catch (Exception ex)
            {
                List<DetailsOfInterest> obj = new List<DetailsOfInterest>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public Pravate_PublicVM CreateOtherEntity(Pravate_PublicVM _objentity)
        {
            try
            {
                if (_objentity.EntityName != null)
                {
                    _objentity.EntityName = _objentity.EntityName.ToUpper();
                }

                if (_objentity.CIN != null)
                {
                    _objentity.CIN = _objentity.CIN.ToUpper();
                }

                if (_objentity.PAN != null)
                {
                    _objentity.PAN = _objentity.PAN.ToUpper();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            try
            {
                var _objCreateEntity = (from row in entities.BM_EntityMaster
                                        where row.Is_Deleted == false
                                        && row.CompanyName == _objentity.EntityName
                                        && row.Entity_Type == _objentity.Entity_Type
                                        && row.CIN_LLPIN == _objentity.CIN
                                        && row.PAN == _objentity.PAN
                                        && row.Customer_Id == null
                                        select row).FirstOrDefault();
                if (_objCreateEntity == null)
                {
                    BM_EntityMaster _bmentity = new BM_EntityMaster();

                    _bmentity.CompanyName = _objentity.EntityName;
                    _bmentity.Entity_Type = _objentity.Entity_Type;
                    _bmentity.CIN_LLPIN = _objentity.CIN;
                    _bmentity.PAN = _objentity.PAN;

                    _bmentity.IncorporationDate = DateTime.Now;
                    _bmentity.ROC_Code = 0;
                    _bmentity.Registration_No = "0";
                    _bmentity.Regi_Address_Line1 = "NA";
                    _bmentity.Regi_Address_Line2 = "NA";
                    _bmentity.Regi_StateId = 0;
                    _bmentity.Regi_CityId = 0;
                    _bmentity.IsCorp_Office = false;
                    _bmentity.CompanyCategory_Id = 0;
                    _bmentity.IS_Listed = false;

                    _bmentity.Email_Id = "NA";
                    _bmentity.NIC_Code = 0;
                    _bmentity.NIC_Discription = "";
                    _bmentity.Is_Deleted = false;

                    entities.BM_EntityMaster.Add(_bmentity);
                    entities.SaveChanges();

                    _objentity.Id = _bmentity.Id;
                    _objentity.Message = true;
                    _objentity.successErrorMessage = "Saved Successfully.";

                    return _objentity;
                }
                else
                {
                    _objentity.Id = _objCreateEntity.Id;
                    _objentity.Message = true;
                    return _objentity;
                }
            }
            catch (Exception ex)
            {

                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objentity;
            }
        }

        public List<EntityMasterVM> GetAllEntityMaster(int customerID, long Director_Id)
        {
            try
            {

                var customerBranches = (from row in entities.BM_EntityMaster.OrderBy(x => x.CompanyName)
                                        join rows in entities.BM_EntityType
                                        on row.Entity_Type equals (rows.Id)
                                        where row.Is_Deleted == false && row.Customer_Id == customerID
                                        select new EntityMasterVM
                                        {
                                            Id = row.Id,
                                            EntityName = row.CompanyName,
                                            CIN = row.CIN_LLPIN,
                                            Entity_Type = row.Entity_Type,
                                            PAN = row.PAN
                                        })
                                        .Union
                                        (from row in entities.BM_EntityMaster.OrderBy(x => x.CompanyName)
                                         join rows in entities.BM_Directors_DetailsOfInterest
                                         on row.Id equals (rows.EntityId)
                                         where row.Is_Deleted == false && rows.Director_Id == Director_Id && row.Customer_Id == null
                                         select new EntityMasterVM
                                         {
                                             Id = row.Id,
                                             EntityName = row.CompanyName,
                                             CIN = row.CIN_LLPIN,
                                             Entity_Type = row.Entity_Type,
                                             PAN = row.PAN
                                         });

                return customerBranches.ToList();
            }
            catch (Exception ex)
            {
                List<EntityMasterVM> obj = new List<EntityMasterVM>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public bool CheckForChairmanMDExists(List<int> designationIDs, int entityID, long directorID)
        {
            bool chairmanMDExists = false;

            try
            {
                var chairmanRecordExists = (from DOI in entities.BM_Directors_DetailsOfInterest
                                            join dirDes in entities.BM_Directors_Designation on DOI.IfDirector equals dirDes.Id
                                            where DOI.EntityId == entityID
                                            && designationIDs.Contains(dirDes.Id)
                                            && DOI.IsActive == true
                                            && DOI.IsDeleted == false
                                            select DOI).FirstOrDefault();

                if (chairmanRecordExists != null)
                {
                    if (chairmanRecordExists.Director_Id != null)
                    {
                        if (chairmanRecordExists.Director_Id != directorID)
                            chairmanMDExists = true;
                        else
                            chairmanMDExists = false;
                    }
                }

                return chairmanMDExists;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return chairmanMDExists;
            }
        }

        public bool CheckKMPExists(long detailsID, int designationId, int entityID)
        {
            bool isKMPExists = false;
            try
            {
                isKMPExists = (from DOI in entities.BM_Directors_DetailsOfInterest
                                where DOI.EntityId == entityID
                                && DOI.DirectorDesignationId == designationId
                                && DOI.Id != detailsID
                                && DOI.IsActive == true
                                && DOI.IsDeleted == false
                                select DOI).Any();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return isKMPExists;
        }

        public DetailsOfInterest CreateDetailsOfInterest(DetailsOfInterest item, int customerId, int userID)
        {
            item.Response = new Response();
            Pravate_PublicVM objEntity = new Pravate_PublicVM();
            try
            {
                var natureOfInterest = (from row in entities.BM_Directors_NatureOfInterest
                                        where row.Id == item.NatureOfInterest
                                        select row).FirstOrDefault();

                if (natureOfInterest != null)
                {
                    if (natureOfInterest.IsDirector == true)
                    {
                        item.IfInterestThroughRelatives = 0;
                        item.IfRelatives = null;

                        if (item.DirectorDesignationId <= 0)
                        {
                            item.Response.Message = "Please select Designation";
                            item.Response.Error = true;
                            return item;
                        }
                        if (item.IfDirector <= 0)
                        {
                            item.Response.Message = "Please select Type of Directorship";
                            item.Response.Error = true;
                            return item;
                        }

                        //string DIN = string.Empty;
                        //DIN = (from x in entities.BM_DirectorMaster where x.Id == item.Director_ID select x.DIN).FirstOrDefault();
                        //if (string.IsNullOrEmpty(DIN))
                        //{
                        //    item.Response.Error = true;
                        //    item.Response.Message = "Please update DIN before Create/Update Details of Interest as Director";
                        //    return item;
                        //}
                    }
                    else if (natureOfInterest.IsMNGT == true)
                    {
                        item.IfDirector = 0;
                        if (item.DirectorDesignationId <= 0)
                        {
                            item.Response.Message = "Please select Designation";
                            item.Response.Error = true;
                            return item;
                        }
                    }
                    else if (item.NatureOfInterest == 7)
                    {
                        item.IfDirector = 0;
                        item.DirectorDesignationId = 0;
                        if (item.IfRelatives == null)
                        {
                            item.Response.Message = "Please select Interest through Relatives";
                            item.Response.Error = true;
                            return item;
                        }
                        else if (item.IfRelatives.Count <= 0)
                        {
                            item.Response.Message = "Please select Interest through Relatives";
                            item.Response.Error = true;
                            return item;
                        }
                    }
                }
                else if (item.NatureOfInterest == 7)
                {
                    item.IfDirector = 0;
                    item.DirectorDesignationId = 0;
                    if (item.IfRelatives == null)
                    {
                        item.Response.Message = "Please select Interest through Relatives";
                        item.Response.Error = true;
                        return item;
                    }
                    else if (item.IfRelatives.Count <= 0)
                    {
                        item.Response.Message = "Please select Interest through Relatives";
                        item.Response.Error = true;
                        return item;
                    }
                }

                if (item.PercentagesOfShareHolding == null)
                {
                    item.PercentagesOfShareHolding = 0;
                }

                if (item.Entity_Id == -1)
                {
                    if (item.TypeOfEntity <= 0)
                    {
                        item.Response.Message = "Please select Entity Type";
                        item.Response.Error = true;
                        return item;
                    }

                    objEntity.Id = 0;
                    objEntity.EntityName = item.NameOfOther;
                    objEntity.Entity_Type = item.TypeOfEntity;
                    objEntity.CIN = item.CIN;
                    objEntity.PAN = item.PAN != null ? item.PAN : "";

                    objEntity = CreateOtherEntity(objEntity);

                    if (objEntity.Message == true)
                    {
                        item.Entity_Id = objEntity.Id;
                        item.NameOfOther = null;
                    }
                }

                //Check incorporation date
                var entity = entities.BM_EntityMaster.Where(k => k.Id == item.Entity_Id && k.Customer_Id != null).FirstOrDefault();
                if (entity != null)
                {
                    if (entity.IncorporationDate > item.Arosed)
                    {
                        item.Response.Message = "Date of interest arosed should be on or after Date of Incorporation of the Entity";
                        item.Response.Error = true;
                        return item;
                    }

                    item.TypeOfEntity = entity.Entity_Type;
                    objEntity.Entity_Type = entity.Entity_Type;
                }
                //End

                if (item.IfDirector != null)
                {
                    bool chairmanExists = false;
                    List<int> designationIDs = new List<int>();

                    //Check for already Chairman exists
                    designationIDs.Add(1);
                    designationIDs.Add(4);
                    designationIDs.Add(9);

                    if (designationIDs.Contains(item.IfDirector))
                    {
                        chairmanExists = CheckForChairmanMDExists(designationIDs, item.Entity_Id, item.Director_ID);
                        if (chairmanExists)
                        {
                            item.Response.Message = "Chairman for Entity/Company already exists";
                            item.Response.Error = true;
                            return item;
                        }
                    }

                    //Check KMP Exists
                    designationIDs.Clear();
                    designationIDs.Add(2);
                    designationIDs.Add(3);
                    designationIDs.Add(4);
                    designationIDs.Add(5);

                    if (designationIDs.Contains(item.DirectorDesignationId))
                    {
                        chairmanExists = CheckKMPExists(item.Id, item.DirectorDesignationId, item.Entity_Id);
                        if (chairmanExists)
                        {
                            item.Response.Error = true;
                            item.Response.Message = "KMP for Entity/Company already exists";
                            return item;
                        }
                    }
                }

                //var _objcheckDetails_Intrestdata = (from row in entities.BM_Directors_DetailsOfInterest
                //                                    .Where(k => k.Id != item.Id && ((k.EntityId != -1 && k.EntityId == item.Entity_Id) || (k.EntityId == -1 && k.CIN == item.CIN)) 
                //                                    && k.Director_Id == item.Director_ID && k.IsDeleted == false && k.IsActive == true)
                //                                    select row).FirstOrDefault();
                var checkDetailsExists = false;

                if (natureOfInterest != null)
                {
                    if (natureOfInterest.IsMNGT == true)
                    {
                        checkDetailsExists = (from row in entities.BM_Directors_DetailsOfInterest
                                              join nature in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals nature.Id
                                              where row.Id != item.Id 
                                              && row.EntityId == item.Entity_Id
                                              && row.Director_Id == item.Director_ID 
                                              && row.IsDeleted == false 
                                              && row.IsActive == true &&
                                              nature.IsMNGT == true 
                                              && row.DirectorDesignationId == item.DirectorDesignationId
                                              select row.Id).Any();
                    }
                    else
                    {
                        checkDetailsExists = (from row in entities.BM_Directors_DetailsOfInterest
                                              join nature in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals nature.Id
                                              where row.Id != item.Id 
                                              && row.EntityId == item.Entity_Id
                                              && row.Director_Id == item.Director_ID 
                                              && row.IsDeleted == false 
                                              && row.IsActive == true
                                              && nature.IsMNGT == false
                                              select row.Id).Any();
                    }
                }

                if (checkDetailsExists == false)
                {
                    BM_Directors_DetailsOfInterest obj = new BM_Directors_DetailsOfInterest();
                    obj.Id = item.Id;
                    obj.Director_Id = item.Director_ID;
                    obj.EntityId = item.Entity_Id;
                    obj.Entity_Type = item.TypeOfEntity;
                    obj.NameOfOther = item.NameOfOther;
                    obj.CIN = item.CIN;
                    obj.PAN = item.PAN != null ? item.PAN : "";                    
                    obj.NatureOfInterest = item.NatureOfInterest;
                    obj.IfDirector = item.IfDirector;
                    obj.IfInterestThroughRelatives = item.IfInterestThroughRelatives;
                    obj.PercentagesOfShareHolding = (decimal)item.PercentagesOfShareHolding;
                    obj.ArosedOrChangedDate = item.Arosed;
                    obj.IsDeleted = false;
                    obj.CreatedOn = DateTime.Now;
                    obj.CreatedBy = userID;
                    obj.IsActive = true;
                    obj.DateOfResolution = item.BoardofResolution;
                    obj.DateOfAppointment = item.DateofAppointment;
                    obj.DirectorDesignationId = item.DirectorDesignationId;
                    obj.OriginalDirector_Id = 0;
                    obj.NumberofSharesHeld = item.NumberofShares;

                    entities.BM_Directors_DetailsOfInterest.Add(obj);
                    entities.SaveChanges();

                    #region Add director security details(Add by Ruchi on 1st of june 2020)

                    if (natureOfInterest != null)
                    {
                        if (natureOfInterest.IsDirector)
                        {
                            var checkSecurityDetailsExists = (from row in entities.BM_SecurityDetail
                                                              where row.CIN == item.CIN
                                                              && row.Director_KmpId == item.Director_ID
                                                              select row).FirstOrDefault();

                            if (checkSecurityDetailsExists == null)
                            {
                                BM_SecurityDetail _objsecuritydetails = new BM_SecurityDetail();
                                _objsecuritydetails.CIN = item.CIN != null ? item.CIN : (from cin in entities.BM_EntityMaster where cin.Id == item.Entity_Id select cin.CIN_LLPIN).FirstOrDefault(); ;
                                _objsecuritydetails.Director_KmpId = (int)item.Director_ID;
                                _objsecuritydetails.CompanyName = (from cn in entities.BM_EntityMaster where cn.Id == item.Entity_Id select cn.CompanyName).FirstOrDefault();
                                _objsecuritydetails.CustomerId = customerId;
                                _objsecuritydetails.Createdby = userID;
                                _objsecuritydetails.Createdon = DateTime.Now;
                                _objsecuritydetails.IsDeleted = false;

                                entities.BM_SecurityDetail.Add(_objsecuritydetails);
                                entities.SaveChanges();
                            }
                        }
                    }

                    ////if ((item.NatureOfInterest == 1) || (item.NatureOfInterest == 5) || (item.NatureOfInterest == 8) || (item.NatureOfInterest == 9) || (item.NatureOfInterest == 10) || (item.NatureOfInterest == 11))
                    //if (item.NatureOfInterest == 1 || item.NatureOfInterest == 8 || item.NatureOfInterest == 9
                    //   || (item.NatureOfInterest == 10) || (item.NatureOfInterest == 11) || (item.NatureOfInterest == 14) || (item.NatureOfInterest == 15))
                    //{
                    //    var checksecurity = (from row in entities.BM_SecurityDetail
                    //                         where row.CIN == item.CIN
                    //                         && row.Director_KmpId == item.Director_ID
                    //                         select row).FirstOrDefault();
                    //    if (checksecurity == null)
                    //    {
                    //        BM_SecurityDetail _objsecuritydetails = new BM_SecurityDetail();
                    //        _objsecuritydetails.CIN = item.CIN != null ? item.CIN : (from cin in entities.BM_EntityMaster where cin.Id == item.Entity_Id select cin.CIN_LLPIN).FirstOrDefault(); ;
                    //        _objsecuritydetails.Director_KmpId = (int)item.Director_ID;
                    //        _objsecuritydetails.CompanyName = (from cn in entities.BM_EntityMaster where cn.Id == item.Entity_Id select cn.CompanyName).FirstOrDefault();
                    //        _objsecuritydetails.CustomerId = customerId;
                    //        _objsecuritydetails.Createdby = userID;
                    //        _objsecuritydetails.Createdon = DateTime.Now;
                    //        _objsecuritydetails.IsDeleted = false;

                    //        entities.BM_SecurityDetail.Add(_objsecuritydetails);
                    //        entities.SaveChanges();
                    //    }
                    //}

                    #endregion Add director security details(Add by Ruchi on 1st of june 2020)

                    //Add Multiple Relative Details
                    entities.BM_Directors_DetailsOfInterest_RelativeMapping.Where(k => k.DetailsOfInterestID == item.Id).ToList().ForEach(k => k.IsDeleted = true);
                    entities.SaveChanges();

                    if (item.IfRelatives != null)
                    {
                        foreach (var relative in item.IfRelatives)
                        {
                            if (entities.BM_Directors_DetailsOfInterest_RelativeMapping.Where(k => k.DetailsOfInterestID == item.Id && k.RelativeId == relative.Id).Any())
                            {
                                entities.BM_Directors_DetailsOfInterest_RelativeMapping.Where(k => k.DetailsOfInterestID == item.Id && k.RelativeId == relative.Id).ToList().ForEach(k => { k.IsDeleted = false; k.UpdatedBy = customerId; k.UpdatedOn = DateTime.Now; });
                                entities.SaveChanges();
                            }
                            else
                            {
                                BM_Directors_DetailsOfInterest_RelativeMapping r = new BM_Directors_DetailsOfInterest_RelativeMapping()
                                {
                                    DetailsOfInterestID = obj.Id,
                                    RelativeId = relative.Id,
                                    IsDeleted = false,
                                    CreatedBy = customerId,
                                    CreatedOn = DateTime.Now,
                                };
                                entities.BM_Directors_DetailsOfInterest_RelativeMapping.Add(r);
                                entities.SaveChanges();
                            }
                        }
                    }

                    #region Activate user
                    if(natureOfInterest != null)
                    {
                        if(natureOfInterest.IsDirector)
                        {
                            var director = (from row in entities.BM_DirectorMaster
                                            where row.Id == obj.Director_Id
                                            select row).FirstOrDefault();
                            if(director != null)
                            {
                                if(director.IsDirecor == false)
                                {
                                    director.IsDirecor = true;
                                    director.UpdatedBy = userID;
                                    director.UpdatedOn = DateTime.Now;
                                    entities.SaveChanges();
                                }

                                if(director.UserID > 0)
                                {
                                    var user = (from row in entities.Users
                                                where row.ID == director.UserID && row.IsActive == false
                                                select row).FirstOrDefault();
                                    if (user != null)
                                    {
                                        user.IsActive = true;
                                        entities.SaveChanges();
                                    }

                                    objIUser.SetIsActiveRiskUser(userID, true);
                                }
                            }
                        }

                        if (natureOfInterest.IsMNGT)
                        {
                            var director = (from row in entities.BM_DirectorMaster
                                            where row.Id == obj.Director_Id
                                            select row).FirstOrDefault();
                            if (director != null)
                            {
                                if (director.IsKMP == false)
                                {
                                    director.IsKMP = true;
                                    director.UpdatedBy = userID;
                                    director.UpdatedOn = DateTime.Now;
                                    entities.SaveChanges();
                                }
                            }
                        }
                    }
                    #endregion
                    //End
                    item.Response.Success = true;
                    item.Response.Message = SecretarialConst.Messages.saveSuccess;
                }
                else
                {
                    item.Response.Error = true;
                    item.Response.Message = "Same Record Already Exists";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                item.Response.Message = SecretarialConst.Messages.serverError;
            }
            return item;
        }

        public string UpdateDetailsOfInterest(DetailsOfInterest item, int customerId, int userID)
        {
            Pravate_PublicVM objEntity = new Pravate_PublicVM();
            try
            {
                #region Details of interest
                var natureOfInterest = (from row in entities.BM_Directors_NatureOfInterest
                                        where row.Id == item.NatureOfInterest
                                        select row).FirstOrDefault();

                if (natureOfInterest != null)
                {
                    if (natureOfInterest.IsDirector == true)
                    {
                        item.IfInterestThroughRelatives = 0;
                        item.IfRelatives = null;

                        if (item.DirectorDesignationId <= 0)
                        {
                            return "Please select Designation.";
                        }
                        if (item.IfDirector <= 0)
                        {
                            return "Please select Type of Directorship.";
                        }

                        //string DIN = string.Empty;
                        //DIN = (from x in entities.BM_DirectorMaster where x.Id == item.Director_ID select x.DIN).FirstOrDefault();
                        //if (string.IsNullOrEmpty(DIN))
                        //{
                        //    item.Response.Error = true;
                        //    item.Response.Message = "Please update DIN before Create/Update Details of Interest as Director";
                        //}
                    }
                    else if (natureOfInterest.IsMNGT == true)
                    {
                        item.IfDirector = 0;
                        if (item.DirectorDesignationId <= 0)
                        {
                            return "Please select Designation.";
                        }
                    }
                    else if (item.NatureOfInterest == 7)
                    {
                        item.IfDirector = 0;
                        item.DirectorDesignationId = 0;
                        if (item.IfRelatives == null)
                        {
                            return "Please select Interest through Relatives.";
                        }
                        else if (item.IfRelatives.Count <= 0)
                        {
                            return "Please select Interest through Relatives.";
                        }
                    }
                }
                else if (item.NatureOfInterest == 7)
                {
                    item.IfDirector = 0;
                    item.DirectorDesignationId = 0;
                    if (item.IfRelatives == null)
                    {
                        return "Please select Interest through Relatives.";
                    }
                    else if (item.IfRelatives.Count <= 0)
                    {
                        return "Please select Interest through Relatives.";
                    }
                }
                #endregion

                if (item.PercentagesOfShareHolding == null)
                {
                    item.PercentagesOfShareHolding = 0;
                }
                if (item.Entity_Id == -1)
                {
                    if (item.TypeOfEntity <= 0)
                    {
                        return "Please select Entity Type";
                    }
                    objEntity.Id = 0;
                    objEntity.EntityName = item.NameOfOther;
                    objEntity.Entity_Type = item.TypeOfEntity;
                    objEntity.CIN = item.CIN;
                    objEntity.PAN = item.PAN != null ? item.PAN : "";

                    objEntity = CreateOtherEntity(objEntity);

                    if (objEntity.Message == true)
                    {
                        item.Entity_Id = objEntity.Id;
                        item.NameOfOther = null;
                    }
                }

                if (item.IfDirector != null)
                {
                    bool chairmanExists = false;
                    List<int> designationIDs = new List<int>();

                    //Check for already Chairman exists
                    designationIDs.Add(1);
                    designationIDs.Add(4);
                    designationIDs.Add(9);

                    if (designationIDs.Contains(item.IfDirector))
                    {
                        chairmanExists = CheckForChairmanMDExists(designationIDs, item.Entity_Id, item.Director_ID);
                        if (chairmanExists)
                            return "Chairman for Entity already exists";
                    }

                    //Check KMP Exists
                    designationIDs.Clear();
                    designationIDs.Add(2);
                    designationIDs.Add(3);
                    designationIDs.Add(4);
                    designationIDs.Add(5);

                    if (designationIDs.Contains(item.DirectorDesignationId))
                    {
                        chairmanExists = CheckKMPExists(item.Id, item.DirectorDesignationId, item.Entity_Id);
                        if (chairmanExists)
                            return "KMP for Entity already exists";
                    }
                }

                //Check incorporation date
                var entity = entities.BM_EntityMaster.Where(k => k.Id == item.Entity_Id && k.Customer_Id != null).FirstOrDefault();
                if (entity != null)
                {
                    if (entity.IncorporationDate > item.Arosed)
                    {
                        return "Date of interest arosed should be on or after date of incorporation of the entity";
                    }
                    item.TypeOfEntity = entity.Entity_Type;
                    objEntity.Entity_Type = entity.Entity_Type;
                }
                //End

                //var _objcheckDetails_Intrestdata = (from row in entities.BM_Directors_DetailsOfInterest.Where(k => k.Id != item.Id && ((k.EntityId != -1 && k.EntityId == item.Entity_Id) || (k.EntityId == -1 && k.CIN == item.CIN)) && k.Director_Id == item.Director_ID && k.IsDeleted == false) select row).FirstOrDefault();

                var checkDetailsExists = false;

                if (natureOfInterest != null)
                {
                    if (natureOfInterest.IsMNGT == true)
                    {
                        checkDetailsExists = (from row in entities.BM_Directors_DetailsOfInterest
                                              join nature in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals nature.Id
                                              where row.Id != item.Id && row.EntityId == item.Entity_Id
                                              && row.Director_Id == item.Director_ID && row.IsDeleted == false && row.IsActive == true &&
                                              nature.IsMNGT == true && row.DirectorDesignationId == item.DirectorDesignationId
                                              select row.Id).Any();
                    }
                    else
                    {
                        checkDetailsExists = (from row in entities.BM_Directors_DetailsOfInterest
                                              join nature in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals nature.Id
                                              where row.Id != item.Id && row.EntityId == item.Entity_Id
                                              && row.Director_Id == item.Director_ID && row.IsDeleted == false && row.IsActive == true
                                              && nature.IsMNGT == false
                                              select row.Id).Any();
                    }
                }

                if (checkDetailsExists == false)
                {
                    BM_Directors_DetailsOfInterest obj = entities.BM_Directors_DetailsOfInterest.Where(k => k.Id == item.Id && k.IsDeleted == false && k.IsActive == true).FirstOrDefault();

                    if (obj != null)
                    {
                        if (obj.IsActive == true)
                        {
                            obj.EntityId = item.Entity_Id;
                            obj.Entity_Type = item.TypeOfEntity;
                            obj.NameOfOther = item.NameOfOther;
                            obj.CIN = item.CIN;
                            obj.PAN = item.PAN != null ? item.PAN : "";                            
                            obj.NatureOfInterest = item.NatureOfInterest;
                            obj.IfDirector = item.IfDirector;
                            obj.IfInterestThroughRelatives = item.IfInterestThroughRelatives;
                            obj.PercentagesOfShareHolding = (decimal)item.PercentagesOfShareHolding;
                            obj.ArosedOrChangedDate = item.Arosed;
                            obj.UpdatedOn = DateTime.Now;
                            obj.UpdatedBy = userID;
                            obj.DateOfAppointment = item.DateofAppointment;
                            obj.DateOfResolution = item.BoardofResolution;
                            obj.DirectorDesignationId = item.DirectorDesignationId;
                            obj.NumberofSharesHeld = item.NumberofShares;
                            
                            entities.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                            entities.SaveChanges();

                            //Add Multiple Relative Details
                            entities.BM_Directors_DetailsOfInterest_RelativeMapping.Where(k => k.DetailsOfInterestID == item.Id).ToList().ForEach(k => k.IsDeleted = true);
                            entities.SaveChanges();
                            if (item.IfRelatives != null)
                            {
                                foreach (var relative in item.IfRelatives)
                                {
                                    if (entities.BM_Directors_DetailsOfInterest_RelativeMapping.Where(k => k.DetailsOfInterestID == item.Id && k.RelativeId == relative.Id).Any())
                                    {
                                        entities.BM_Directors_DetailsOfInterest_RelativeMapping.Where(k => k.DetailsOfInterestID == item.Id && k.RelativeId == relative.Id).ToList().ForEach(k => { k.IsDeleted = false; k.UpdatedBy = customerId; k.UpdatedOn = DateTime.Now; });
                                        entities.SaveChanges();
                                    }
                                    else
                                    {
                                        BM_Directors_DetailsOfInterest_RelativeMapping r = new BM_Directors_DetailsOfInterest_RelativeMapping()
                                        {
                                            DetailsOfInterestID = obj.Id,
                                            RelativeId = relative.Id,
                                            IsDeleted = false,
                                            CreatedBy = customerId,
                                            CreatedOn = DateTime.Now,
                                        };
                                        entities.BM_Directors_DetailsOfInterest_RelativeMapping.Add(r);
                                        entities.SaveChanges();
                                    }
                                }
                            }

                            #region Add director security details(Add by Ruchi on 1st of june 2020)

                            if (natureOfInterest != null)
                            {
                                if (natureOfInterest.IsDirector)
                                {
                                    var checkSecurityDetailsExists = (from row in entities.BM_SecurityDetail
                                                                      where row.CIN == item.CIN
                                                                      && row.Director_KmpId == item.Director_ID
                                                                      select row).FirstOrDefault();

                                    if (checkSecurityDetailsExists == null)
                                    {
                                        BM_SecurityDetail _objsecuritydetails = new BM_SecurityDetail();
                                        _objsecuritydetails.CIN = item.CIN != null ? item.CIN : (from cin in entities.BM_EntityMaster where cin.Id == item.Entity_Id select cin.CIN_LLPIN).FirstOrDefault(); ;
                                        _objsecuritydetails.Director_KmpId = (int)item.Director_ID;
                                        _objsecuritydetails.CompanyName = (from cn in entities.BM_EntityMaster where cn.Id == item.Entity_Id select cn.CompanyName).FirstOrDefault();
                                        _objsecuritydetails.CustomerId = customerId;
                                        _objsecuritydetails.Createdby = userID;
                                        _objsecuritydetails.Createdon = DateTime.Now;
                                        _objsecuritydetails.IsDeleted = false;

                                        entities.BM_SecurityDetail.Add(_objsecuritydetails);
                                        entities.SaveChanges();
                                    }
                                }
                            }

                            ////if ((item.NatureOfInterest == 1) || (item.NatureOfInterest == 5) || (item.NatureOfInterest == 8) || (item.NatureOfInterest == 9) || (item.NatureOfInterest == 10) || (item.NatureOfInterest == 11))
                            //if (item.NatureOfInterest == 1 || item.NatureOfInterest == 8 || item.NatureOfInterest == 9
                            //   || (item.NatureOfInterest == 10) || (item.NatureOfInterest == 11) || (item.NatureOfInterest == 14) || (item.NatureOfInterest == 15))
                            //{
                            //    var checksecurity = (from row in entities.BM_SecurityDetail
                            //                         where row.CIN == item.CIN
                            //                         && row.Director_KmpId == item.Director_ID
                            //                         select row).FirstOrDefault();
                            //    if (checksecurity == null)
                            //    {
                            //        BM_SecurityDetail _objsecuritydetails = new BM_SecurityDetail();
                            //        _objsecuritydetails.CIN = item.CIN != null ? item.CIN : (from cin in entities.BM_EntityMaster where cin.Id == item.Entity_Id select cin.CIN_LLPIN).FirstOrDefault(); ;
                            //        _objsecuritydetails.Director_KmpId = (int)item.Director_ID;
                            //        _objsecuritydetails.CompanyName = (from cn in entities.BM_EntityMaster where cn.Id == item.Entity_Id select cn.CompanyName).FirstOrDefault();
                            //        _objsecuritydetails.CustomerId = customerId;
                            //        _objsecuritydetails.Createdby = userID;
                            //        _objsecuritydetails.Createdon = DateTime.Now;
                            //        _objsecuritydetails.IsDeleted = false;

                            //        entities.BM_SecurityDetail.Add(_objsecuritydetails);
                            //        entities.SaveChanges();
                            //    }
                            //}

                            #endregion Add director security details(Add by Ruchi on 1st of june 2020)
                            //End

                            #region Activate user
                            if (natureOfInterest != null)
                            {
                                if (natureOfInterest.IsDirector)
                                {
                                    var director = (from row in entities.BM_DirectorMaster
                                                    where row.Id == obj.Director_Id
                                                    select row).FirstOrDefault();
                                    if (director != null)
                                    {
                                        if (director.IsDirecor == false)
                                        {
                                            director.IsDirecor = true;
                                            director.UpdatedBy = userID;
                                            director.UpdatedOn = DateTime.Now;
                                            entities.SaveChanges();
                                        }

                                        if (director.UserID > 0)
                                        {
                                            var user = (from row in entities.Users
                                                        where row.ID == director.UserID && row.IsActive == false
                                                        select row).FirstOrDefault();
                                            if (user != null)
                                            {
                                                user.IsActive = true;
                                                entities.SaveChanges();

                                                objIUser.SetIsActiveRiskUser(userID, true);
                                            }
                                        }
                                    }
                                }

                                if (natureOfInterest.IsMNGT)
                                {
                                    var director = (from row in entities.BM_DirectorMaster
                                                    where row.Id == obj.Director_Id
                                                    select row).FirstOrDefault();
                                    if (director != null)
                                    {
                                        if (director.IsKMP == false)
                                        {
                                            director.IsKMP = true;
                                            director.UpdatedBy = userID;
                                            director.UpdatedOn = DateTime.Now;
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                            }
                            #endregion
                            return SecretarialConst.Messages.updateSuccess;
                        }
                        else
                        {
                            return "Director Details of Interest is not active";
                        }
                    }
                    else
                    {
                        return SecretarialConst.Messages.serverError;
                    }
                }
                else
                {
                    return "Same Record Already Exists";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return SecretarialConst.Messages.serverError;
            }
        }

        public string DeleteDetailsOfInterest(DetailsOfInterest item, int userId)
        {
            try
            {
                var _obj = (from row in entities.BM_Directors_DetailsOfInterest
                            where row.Id == item.Id
                            && row.IsDeleted == false
                            && row.IsActive == true
                            select row).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.IsDeleted = true;
                    _obj.IsActive = false;
                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    return SecretarialConst.Messages.deleteSuccess;
                }
                else
                {
                    return SecretarialConst.Messages.serverError;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return SecretarialConst.Messages.serverError;
            }
        }

        #endregion

        public IEnumerable<BM_DirectorPosition> GetDirectorPosition()
        {
            try
            {
                var getDirectorPosition = (from row in entities.BM_DirectorPosition where row.IsDeleted == false select row).ToList();
                return getDirectorPosition;
            }
            catch (Exception ex)
            {
                List<BM_DirectorPosition> obj = new List<BM_DirectorPosition>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        #region Details of Committee Position
        public List<CommitteeMasterVM> GetCommitteeMasterdtls(int customerId, int director_Id)
        {
            try
            {
                var result = (from director in entities.BM_DirectorMaster
                              join details in entities.BM_Directors_DetailsOfInterest on director.Id equals details.Director_Id
                              join row in entities.BM_Directors_DetailsOfCommiteePosition on director.Id equals row.Director_Id
                              join row_e in entities.BM_EntityMaster on row.EntityId equals row_e.Id
                              join committee in entities.BM_CommitteeComp on row.Committee_Id equals committee.Id
                              join designation in entities.BM_DirectorPosition on row.Designation_Id equals designation.Id
                              join committeeType in entities.BM_Directors_Designation on row.Committee_Type equals committeeType.Id
                              where director.Customer_Id == customerId && director.Id == director_Id
                                    && director.Is_Deleted == false && details.IsActive == true && row.IsDeleted == false
                              orderby row.Designation_Id
                              select new CommitteeMasterVM
                              {
                                  Id = row.Id,
                                  Entity_Id = row.EntityId,
                                  EntityName = row_e.CompanyName,
                                  Director_Id = row.Director_Id,
                                  DirectorName = (string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation + " ") + director.FirstName + (string.IsNullOrEmpty(director.LastName) ? "" : " " + director.LastName),
                                  Designation_Name = designation.Name,
                                  Designation_Id = row.Designation_Id,
                                  Committee_Id = row.Committee_Id,
                                  CommitteeName = committee.Name,
                                  Committee_Type = row.Committee_Type,
                                  Committee_Type_Name = committeeType.Name,
                              }
                             ).ToList();
                return result;
            }
            catch (Exception ex)
            {
                List<CommitteeMasterVM> obj = new List<CommitteeMasterVM>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }
        public CommitteeMasterVM CreateCommitteeDtls(int customerId, CommitteeMasterVM _objcommity)
        {
            try
            {
                return _objcommity;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objcommity;
            }
        }
        public CommitteeMasterVM UpdateCommitteeDtls(int customerId, CommitteeMasterVM _objcommity)
        {
            try
            {
                return _objcommity;
            }
            catch
            {
                return _objcommity;
            }
        }
        #endregion

        public List<Directors> DirectorDetails(int EntityId, int customerId)
        {
            var result = new List<Directors>();
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                result = (from director in entities.BM_SP_DirectorDetails(EntityId, 0, customerId)

                          select new Directors
                          {
                              DirectorId = director.DirectorId,
                              FullName = director.DirectorFullName,
                              ParmanentAddress = director.PermanentAddress,
                              EmailId = director.EmailId_Official,
                              Occupation = director.Occupation,
                              PAN = director.PAN,
                          }).ToList();
            }
            return result;
        }

        public bool CreateDirector_Security(BM_SecurityDetail obj_Kmpsecurity)
        {
            bool Success = false;
            try
            {
                if (obj_Kmpsecurity != null)
                {
                    var checkKMP_Security = (from row in entities.BM_SecurityDetail
                                             where row.Director_KmpId == obj_Kmpsecurity.Director_KmpId
                                              && row.CompanyName == obj_Kmpsecurity.CompanyName
                                             select row).FirstOrDefault();

                    if (checkKMP_Security == null)
                    {
                        entities.BM_SecurityDetail.Add(obj_Kmpsecurity);
                        entities.SaveChanges();
                        Success = true;
                    }
                    else
                    {
                        Success = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                Success = false;
            }
            return Success;
        }

        public VMEntityFileUpload ImportDataFormDIR3(VMEntityFileUpload objfileupload)
        {
            objfileupload.Response = new Response();
            try
            {
                #region variable define for Dir-3 Read
                string Message = "";
                string DIN = string.Empty;
                string Director_FirstName = string.Empty;
                string Director_MiddleName = string.Empty;
                string Director_LastName = string.Empty;
                string FatherFirstName = string.Empty;
                string FatherMiddleName = string.Empty;
                string FatherLastName = string.Empty;
                string IsCitizenofIndia = string.Empty;
                string Nationality = string.Empty;
                string ResidentInIndia = string.Empty;
                bool IsResidence = false;
                DateTime DateofBirth = DateTime.Now;
                string Gender = string.Empty;
                int GenderId = -1;
                string Saltation = string.Empty;
                string IncomeTaxPAN = string.Empty;
                string VoterIdCard = string.Empty;
                string IsPassport = string.Empty;
                string PassportNumber = string.Empty;
                string DrivingLicence = string.Empty;
                string AadhaarNumber = string.Empty;
                long MobileCode = -1;
                string MobileNumber = string.Empty;
                string EmailId = string.Empty;
                string ParAddressLine1 = string.Empty;
                string ParAddressLine2 = string.Empty;
                int ParStateId = -1;
                int ParCityID = -1;
                string ParPin = string.Empty;
                string isParsameprasentAddress = string.Empty;
                bool issameprasentorParmanent = false;
                string preAddressLine1 = string.Empty;
                string PreAddressLine2 = string.Empty;
                int preStateId = -1;
                int PreCityId = -1;
                string PrePin = string.Empty;
                #endregion

                if (objfileupload.File != null)
                {
                    if (objfileupload.File.ContentLength > 0)
                    {
                        string myFilePath = objfileupload.File.FileName;
                        string ext = Path.GetExtension(myFilePath);
                        if (ext == ".pdf")
                        {
                            string excelfileName = string.Empty;
                            string path = "~/Areas/BM_Management/Documents/DirectorMaster/" + objfileupload.CustomerId;
                            string _file_Name = System.IO.Path.GetFileName(objfileupload.File.FileName);
                            string _path = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(path), _file_Name);
                            bool exists = System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            if (!exists)
                            {
                                System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            }
                            DirectoryInfo di = new DirectoryInfo(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            FileInfo[] TXTFiles = di.GetFiles(_file_Name);
                            if (TXTFiles.Length == 0)
                            {
                                objfileupload.File.SaveAs(_path);
                            }
                            PdfReader pdfReader = new PdfReader(_path);
                            if (pdfReader.AcroFields.Xfa.DatasetsSom != null)
                            {
                                foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                {
                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].DIN[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            DIN = (de.Value.InnerText).Trim();
                                        }
                                    }
                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].FIRST_NAME[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            Director_FirstName = de.Value.InnerText;
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].MIDDLE_NAME[0]")
                                    {

                                        if (de.Value.InnerText != null)
                                        {
                                            Director_MiddleName = de.Value.InnerText;
                                        }
                                    }
                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].LAST_NAME[0]")
                                    {

                                        if (de.Value.InnerText != null)
                                        {
                                            Director_LastName = de.Value.InnerText;
                                        }
                                    }
                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].FATH_FIRST_NAME[0]")
                                    {

                                        if (de.Value.InnerText != null)
                                        {
                                            FatherFirstName = de.Value.InnerText;
                                        }
                                    }
                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].FATH_MIDDLE_NAME[0]")
                                    {

                                        if (de.Value.InnerText != null)
                                        {
                                            FatherMiddleName = de.Value.InnerText;
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].FATH_LAST_NAME[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            FatherLastName = de.Value.InnerText;
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].RB_CITIZEN_INDIA[0]")
                                    {

                                        if (de.Value.InnerText != null)
                                        {
                                            IsCitizenofIndia = de.Value.InnerText;
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].NATIONALITY[0]")
                                    {

                                        if (de.Value.InnerText != null)
                                        {
                                            Nationality = de.Value.InnerText.Trim();
                                            if (Nationality == "IN")
                                            {

                                            }
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].RB_RESIDENT_IND[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            ResidentInIndia = de.Value.InnerText.Trim();
                                            if (ResidentInIndia == "YES")
                                            {
                                                IsResidence = true;
                                            }
                                            else if (ResidentInIndia == "NO")
                                            {
                                                IsResidence = false;
                                            }
                                        }
                                    }
                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].DATE_OF_BIRTH[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            DateofBirth = Convert.ToDateTime(de.Value.InnerText);
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].RB_GENDER[0]")
                                    {

                                        if (de.Value.InnerText != null)
                                        {
                                            Gender = de.Value.InnerText;
                                            if (Gender.Trim() == "MALE")
                                            {
                                                Saltation = "Mr.";
                                                GenderId = 1;
                                            }
                                            else if (Gender.Trim() == "Female")
                                            {
                                                Saltation = "Ms";
                                                GenderId = 2;
                                            }

                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].IT_PAN[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            IncomeTaxPAN = de.Value.InnerText;
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].VOTER_ID_NUMBER[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            VoterIdCard = de.Value.InnerText.Trim();
                                        }
                                    }
                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PASSPORT_NUMBER[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            PassportNumber = de.Value.InnerText.Trim();
                                        }
                                    }
                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].DRIVING_LIC_NUM[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            DrivingLicence = de.Value.InnerText.Trim();
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].AADHAR_NUMBER[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            AadhaarNumber = de.Value.InnerText.Trim();
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PERSONAL_MOB_COD[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            MobileCode = Convert.ToInt32(de.Value.InnerText.Trim());
                                        }
                                    }
                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PERSONAL_MOB_NUM[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            MobileNumber = de.Value.InnerText.Trim();
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PERSONAL_EMAILID[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            EmailId = de.Value.InnerText.Trim();
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PERM_ADD_LINE1[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            ParAddressLine1 = de.Value.InnerText.Trim();
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PERM_ADD_LINE2[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            ParAddressLine2 = de.Value.InnerText.Trim();
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PERM_ADD_STATE[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            int stateId = GetstateId(de.Value.InnerText);
                                            //ParAddressLine2 = de.Value.InnerText.Trim();
                                            if (stateId > 0)
                                            {
                                                ParStateId = stateId;
                                            }
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PERM_ADD_CITY[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            int CityId = GetcityId(de.Value.InnerText);

                                            if (CityId > 0)
                                            {
                                                ParCityID = CityId;
                                            }
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PERM_ADD_PINCODE[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            ParPin = de.Value.InnerText;
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].RB_PRES_PERM_ADD[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            isParsameprasentAddress = de.Value.InnerText;
                                            if (isParsameprasentAddress == "YES")
                                            {
                                                issameprasentorParmanent = true;
                                            }
                                            else if (isParsameprasentAddress == "NO")
                                            {
                                                issameprasentorParmanent = false;
                                            }
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PRES_ADD_LINE1[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            preAddressLine1 = de.Value.InnerText;
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PRES_ADD_LINE2[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            PreAddressLine2 = de.Value.InnerText;
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PRES_ADD_CITY[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            int CityId = GetcityId(de.Value.InnerText);

                                            if (CityId > 0)
                                            {
                                                PreCityId = CityId;
                                            }
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PRES_ADD_STATE[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            int stateId = GetstateId(de.Value.InnerText);
                                            //ParAddressLine2 = de.Value.InnerText.Trim();
                                            if (stateId > 0)
                                            {
                                                preStateId = stateId;
                                            }
                                        }
                                    }

                                    if (de.Key.ToString() == "data[0].ZNCA_DIR3_KYC[0].PRES_ADD_PINCODE[0]")
                                    {
                                        if (de.Value.InnerText != null)
                                        {
                                            PrePin = de.Value.InnerText;
                                        }
                                    }

                                }

                                if (!string.IsNullOrEmpty(DIN))
                                {
                                    long DirectorId = -1;
                                    var prevRecordExists = (from row in entities.BM_DirectorMaster 
                                                       where row.DIN == DIN 
                                                       && row.PAN == IncomeTaxPAN 
                                                       && row.Is_Deleted == false 
                                                       && row.Customer_Id == objfileupload.CustomerId 
                                                       select row).FirstOrDefault();
                                    if (prevRecordExists == null)
                                    {
                                        BM_DirectorMaster _objDirector = new BM_DirectorMaster
                                        {
                                            DIN = DIN,
                                            Salutation = Saltation,
                                            FirstName = Director_FirstName,
                                            MiddleName = Director_MiddleName,
                                            LastName = Director_LastName,
                                            DOB = DateofBirth,
                                            Gender = GenderId,
                                            MobileNo = MobileNumber,
                                            EmailId_Personal = EmailId,
                                            EmailId_Official = EmailId,
                                            ResidentInIndia = IsResidence,
                                            Nationality = 1,
                                            PAN = IncomeTaxPAN,
                                            Aadhaar = AadhaarNumber,
                                            PassportNo = PassportNumber,
                                            Permenant_Address_Line1 = ParAddressLine1,
                                            Permenant_Address_Line2 = ParAddressLine2,
                                            Permenant_StateId = ParStateId,
                                            Permenant_CityId = ParCityID,
                                            Permenant_PINCode = ParPin,
                                            IsSameAddress = issameprasentorParmanent,
                                            Present_Address_Line1 = preAddressLine1,
                                            Present_Address_Line2 = PreAddressLine2,
                                            Present_StateId = preStateId,
                                            Present_CityId = PreCityId,
                                            FSalutations = "Mr",
                                            Present_PINCode = PrePin,
                                            Father = FatherFirstName,
                                            FatherMiddleName = FatherMiddleName,
                                            FatherLastName = FatherLastName,
                                            Customer_Id = objfileupload.CustomerId,
                                            IsActive = true,
                                            Is_Deleted = false,
                                            IsDirecor = true,
                                            CreatedBy = objfileupload.UserId,
                                            CreatedOn = DateTime.Now,
                                        };

                                        #region new code
                                        if (!CheckDIN(objfileupload.CustomerId, _objDirector.Id, _objDirector.DIN))
                                        {
                                            var createUserOfDirector = true;
                                            //var accessToDirector = objISettingService.GetAccessToDirector(objfileupload.CustomerId);
                                            
                                            //Create User with Director Role
                                            UserVM user = new UserVM();
                                            user.UserID = 0;

                                            if (createUserOfDirector == true)
                                            {
                                                user.Email = _objDirector.EmailId_Official;
                                                user.FirstName = _objDirector.FirstName;
                                                user.LastName = _objDirector.LastName;
                                                user.CustomerID = objfileupload.CustomerId;
                                                user.ContactNumber = _objDirector.MobileNo;
                                                user.Address = _objDirector.Present_Address_Line1;

                                                user.CreatedBy = objfileupload.UserId;

                                                var tempUserId = GetUserIdForCreateNewDirector(_objDirector.EmailId_Official, objfileupload.CustomerId);
                                                if (tempUserId > 0)
                                                {
                                                    user.UserID = (long)tempUserId;
                                                }

                                                if (user.UserID == 0)
                                                {
                                                    user = CreateUser(user, false, false);
                                                }
                                                //else
                                                //{
                                                //    user = UpdateUser(user, obj.EmailId_Official);
                                                //}
                                            }
                                            //End

                                            if (user.UserID > 0 || createUserOfDirector == false)
                                            {
                                                bool hasError = false;
                                                _objDirector.UserID = user.UserID;
                                                var result = Create(_objDirector, out hasError);
                                            }
                                        }

                                        #endregion

                                        //entities.BM_DirectorMaster.Add(_objDirector);
                                        //entities.SaveChanges();
                                        if (_objDirector.Id > 0)
                                        {
                                            DirectorId = _objDirector.Id;
                                        }
                                    }
                                    else
                                    {
                                        prevRecordExists.DIN = DIN;
                                        prevRecordExists.Salutation = Saltation;
                                        prevRecordExists.FirstName = Director_FirstName;
                                        prevRecordExists.MiddleName = Director_MiddleName;
                                        prevRecordExists.LastName = Director_LastName;

                                        prevRecordExists.DOB = DateofBirth;
                                        prevRecordExists.Gender = GenderId;
                                        prevRecordExists.MobileNo = MobileNumber;
                                        prevRecordExists.EmailId_Personal = EmailId;
                                        prevRecordExists.ResidentInIndia = IsResidence;
                                        prevRecordExists.Nationality = 1;
                                        prevRecordExists.PAN = IncomeTaxPAN;
                                        prevRecordExists.Aadhaar = AadhaarNumber;
                                        prevRecordExists.PassportNo = PassportNumber;

                                        prevRecordExists.Permenant_Address_Line1 = ParAddressLine1;
                                        prevRecordExists.Permenant_Address_Line2 = ParAddressLine2;
                                        prevRecordExists.Permenant_StateId = ParStateId;
                                        prevRecordExists.Permenant_CityId = ParCityID;
                                        prevRecordExists.Permenant_PINCode = ParPin;

                                        prevRecordExists.Present_Address_Line1 = preAddressLine1;
                                        prevRecordExists.Present_Address_Line2 = PreAddressLine2;
                                        prevRecordExists.Present_StateId = preStateId;
                                        prevRecordExists.Present_CityId = PreCityId;
                                        prevRecordExists.Present_PINCode = PrePin;

                                        prevRecordExists.FSalutations = "Mr";                                        
                                        prevRecordExists.Father = FatherFirstName;
                                        prevRecordExists.FatherMiddleName = FatherMiddleName;
                                        prevRecordExists.FatherLastName = FatherLastName;

                                        prevRecordExists.Customer_Id = objfileupload.CustomerId;
                                        prevRecordExists.IsActive = true;
                                        prevRecordExists.Is_Deleted = false;
                                        prevRecordExists.IsDirecor = true;
                                        prevRecordExists.UpdatedOn = DateTime.Now;
                                        prevRecordExists.UpdatedBy = objfileupload.UserId;

                                        entities.SaveChanges();
                                        if (prevRecordExists.Id > 0)
                                        {
                                            DirectorId = prevRecordExists.Id;
                                        }
                                    }
                                    if (DirectorId > 0)
                                    {
                                        objfileupload.Response.Success = true;
                                        objfileupload.Response.Message = "DIR-3-KYC Uploaded Successfully.";
                                    }
                                }
                            }
                            else
                            {
                                objfileupload.Response.Error = true;
                                objfileupload.Response.Message = "Please check uploaded file is DIR-3-KYC";
                            }
                        }
                        else
                        {
                            objfileupload.Response.Error = true;
                            objfileupload.Response.Message = "Please check uploaded file is DIR-3-KYC";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objfileupload.Response.Error = true;
                objfileupload.Response.Message = SecretarialConst.Messages.serverError;
            }
            return objfileupload;
        }

        public UserVM CreateUser(UserVM obj, bool accessToDirector = true, bool isActive = true)
        {
            try
            {
                var hasError = false;

                //string Mailbody = getEmailMessage(obj, obj.Password, out hasError);
                if (hasError == true)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = SecretarialConst.Messages.serverError;
                }
                else
                {
                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                    IUser objIUser = new BM_ManegmentServices.Services.Masters.User();
                    obj.SecretarialRoleId = objIUser.GetRoleIdFromCode(SecretarialConst.Roles.DRCTR);

                    obj.accessToDirector = accessToDirector;
                    var result = objIUser.Create(obj, SenderEmailAddress, isActive);

                    //if (result.Message.Success)
                    //{
                    //    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { obj.Email }), null, null, "AVACOM Account Created.", Mailbody);
                    //}
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        private int GetcityId(string innerText)
        {
            int cityId = 0;
            try
            {
                if (innerText != null)
                {
                    cityId = (from row in entities.Cities where row.Name.ToUpper() == innerText.ToUpper() select row.ID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return cityId;
        }

        private int GetstateId(string innerText)
        {
            int stateId = 0;
            try
            {
                if (innerText != null)
                {
                    if (innerText == "AN")
                    {
                        innerText = "Andaman & Nicobar Islands";
                    }
                    else if (innerText == "AN")
                    {
                        innerText = "Andhra Pradesh";
                    }
                    else if (innerText == "AP")
                    {
                        innerText = "Arunachal Pradesh";
                    }
                    else if (innerText == "AS")
                    {
                        innerText = "Assam";
                    }
                    else if (innerText == "BR")
                    {
                        innerText = "Bihar";
                    }
                    else if (innerText == "CH")
                    {
                        innerText = "Chandigarh";
                    }
                    else if (innerText == "CT")
                    {
                        innerText = "Chhattisgarh";
                    }
                    else if (innerText == "DN")
                    {
                        innerText = "Dadra & Nagar Haveli";
                    }
                    else if (innerText == "AN")
                    {
                        innerText = "Andaman & Nicobar Islands";
                    }
                    else if (innerText == "DD")
                    {
                        innerText = "Daman and Diu";
                    }
                    else if (innerText == "DL")
                    {
                        innerText = "Delhi";
                    }
                    else if (innerText == "GA")
                    {
                        innerText = "Goa";
                    }
                    else if (innerText == "GJ")
                    {
                        innerText = "Gujarat";
                    }
                    else if (innerText == "HR")
                    {
                        innerText = "Haryana";

                    }
                    else if (innerText == "HP")
                    {
                        innerText = "Himachal Pradesh";
                    }
                    else if (innerText == "JK")
                    {
                        innerText = "Jammu & Kashmir";
                    }
                    else if (innerText == "JH")
                    {
                        innerText = "Jharkhand";
                    }
                    else if (innerText == "KA")
                    {
                        innerText = "Karnataka";
                    }
                    else if (innerText == "KL")
                    {
                        innerText = "Kerala";
                    }
                    else if (innerText == "LH")
                    {
                        innerText = "Ladakh";
                    }
                    else if (innerText == "LD")
                    {
                        innerText = "Lakshadweep";
                    }
                    else if (innerText == "KL")
                    {
                        innerText = "Kerala";
                    }
                    else if (innerText == "MP")
                    {
                        innerText = "Madhya Pradesh";
                    }
                    else if (innerText == "MH")
                    {
                        innerText = "Maharashtra";
                    }
                    else if (innerText == "MN")
                    {
                        innerText = "Manipur";
                    }
                    else if (innerText == "ML")
                    {
                        innerText = "Meghalaya";
                    }
                    else if (innerText == "MZ")
                    {
                        innerText = "Mizoram";
                    }
                    else if (innerText == "NL")
                    {
                        innerText = "Nagaland";
                    }
                    else if (innerText == "OR")
                    {
                        innerText = "Orissa";
                    }
                    else if (innerText == "PY")
                    {
                        innerText = "Pondicherry";
                    }
                    else if (innerText == "PB")
                    {
                        innerText = "Punjab";
                    }
                    else if (innerText == "RJ")
                    {
                        innerText = "Rajasthan";
                    }
                    else if (innerText == "SK")
                    {
                        innerText = "Sikkim";
                    }
                    else if (innerText == "TN")
                    {
                        innerText = "Tamil Nadu";
                    }
                    else if (innerText == "TN")
                    {
                        innerText = "Tamil Nadu";
                    }
                    else if (innerText == "TG")
                    {
                        innerText = "Telangana";
                    }
                    else if (innerText == "TR")
                    {
                        innerText = "Tripura";
                    }
                    else if (innerText == "UR")
                    {
                        innerText = "Uttarakhand";
                    }
                    else if (innerText == "UP")
                    {
                        innerText = "Uttar Pradesh";
                    }
                    else if (innerText == "WB")
                    {
                        innerText = "West Bengal";
                    }
                    stateId = (from row in entities.States where row.Name.ToUpper() == innerText.ToUpper() select row.ID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return stateId;
        }



        public long Update_DirectorDetails(BM_DirectorTypeChangesMapping obj, out bool hasError)
        {
            try
            {
                var CheckisAcceptorReject = (from rows in entities.BM_DirectorTypeChangesMapping where rows.DirectorId == obj.DirectorId && rows.IsActive == true select rows).FirstOrDefault();
                if (CheckisAcceptorReject == null)
                {
                    entities.BM_DirectorTypeChangesMapping.Add(obj);
                    entities.SaveChanges();

                    if (obj != null && obj.Id > 0)
                    {
                        var CheckDirectordtls = (from x in entities.BM_DirectorMaster where x.Id == obj.DirectorId select x).FirstOrDefault();
                        CheckDirectordtls.isChangesDone = true;
                        CheckDirectordtls.NumberofChanges = CheckDirectordtls.NumberofChanges + 1;
                        entities.SaveChanges();
                        obj.IsActive = true;
                    }
                }
                else
                {
                    entities.SaveChanges();
                    obj.Id = CheckisAcceptorReject.Id;
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //return false;
            }
            hasError = false;
            return obj.Id;
        }
        public OnEditingDirector SaveDirectorTypeChanges(OnEditingDirector _objonEditingDirector)
        {
            try
            {
                var CheckExist = (from row in entities.BM_DirectorTypeOfChanges where row.DirectorId == _objonEditingDirector.DirectorID && row.IsActive == true select row).FirstOrDefault();
                if (CheckExist == null)
                {
                    BM_DirectorTypeOfChanges obj = new BM_DirectorTypeOfChanges();
                    obj.DirectorId = _objonEditingDirector.DirectorID;
                    obj.UserID = _objonEditingDirector.UserId;
                    obj.Name_of_Director = _objonEditingDirector.Name;
                    obj.FatherName = _objonEditingDirector.FatherName;
                    obj.AadharNumber = _objonEditingDirector.Aadhaar;
                    obj.DateofBirth = _objonEditingDirector.DateofBirth;
                    obj.EmailID = _objonEditingDirector.EmailId;
                    obj.Gender = _objonEditingDirector.Gender;
                    obj.Mobile = _objonEditingDirector.MobileNumber;
                    obj.Nationality = _objonEditingDirector.Nationality;
                    obj.PAN = _objonEditingDirector.PAN;
                    obj.Parmanent_Address = _objonEditingDirector.PermanentAddress;
                    obj.Passport = _objonEditingDirector.PassportNumber;
                    obj.PhotoGraph_of_Director = _objonEditingDirector.Photograph;
                    obj.Present_Address = _objonEditingDirector.PresentAddress;
                    obj.Rasidential_Status = _objonEditingDirector.ResidentialStatus;
                    obj.Changedby = _objonEditingDirector.UserId;
                    obj.ChangeOn = DateTime.Now;
                    obj.IsActive = true;
                    obj.IsApproved = false;
                    obj.IsDisapproved = false;
                    obj.IsPending = true;

                    entities.BM_DirectorTypeOfChanges.Add(obj);
                    entities.SaveChanges();
                    _objonEditingDirector.IsChangedbyDirector = (from change in entities.BM_DirectorMaster where obj.DirectorId == change.Id && obj.UserID == change.UserID select change).Any();
                    _objonEditingDirector.Success = true;
                    _objonEditingDirector.Message = "Edit of Director Profile changes done";


                }
                else
                {
                    CheckExist.DirectorId = _objonEditingDirector.DirectorID;
                    //CheckExist.UserID = _objonEditingDirector.UserId;
                    CheckExist.Name_of_Director = _objonEditingDirector.Name;
                    CheckExist.FatherName = _objonEditingDirector.FatherName;
                    CheckExist.AadharNumber = _objonEditingDirector.Aadhaar;
                    CheckExist.DateofBirth = _objonEditingDirector.DateofBirth;
                    CheckExist.EmailID = _objonEditingDirector.EmailId;
                    CheckExist.Gender = _objonEditingDirector.Gender;
                    CheckExist.Mobile = _objonEditingDirector.MobileNumber;
                    CheckExist.Nationality = _objonEditingDirector.Nationality;
                    CheckExist.PAN = _objonEditingDirector.PAN;
                    CheckExist.Parmanent_Address = _objonEditingDirector.PermanentAddress;
                    CheckExist.Passport = _objonEditingDirector.PassportNumber;
                    CheckExist.PhotoGraph_of_Director = _objonEditingDirector.Photograph;
                    CheckExist.Present_Address = _objonEditingDirector.PresentAddress;
                    CheckExist.Rasidential_Status = _objonEditingDirector.ResidentialStatus;
                    CheckExist.Updatedby = _objonEditingDirector.UserId;
                    CheckExist.UpdatedOn = DateTime.Now;
                    CheckExist.IsActive = true;
                    CheckExist.IsApproved = false;
                    CheckExist.IsDisapproved = false;
                    CheckExist.IsPending = true;
                    entities.SaveChanges();
                    _objonEditingDirector.IsChangedbyDirector = (from change in entities.BM_DirectorMaster where CheckExist.DirectorId == change.Id && CheckExist.UserID == change.UserID select change).Any();
                    _objonEditingDirector.Success = true;
                    _objonEditingDirector.Message = "Edit of Director Profile changes update done";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objonEditingDirector.Error = true;
                _objonEditingDirector.Message = "Server error occur";
            }

            return _objonEditingDirector;
        }

        public OnEditingDirector getDirectorTypeChanges(long directorId)
        {
            OnEditingDirector getdata = new OnEditingDirector();
            try
            {
                getdata = (from row in entities.BM_DirectorTypeOfChanges
                           where row.DirectorId == directorId && row.IsActive == true
                           select new OnEditingDirector

                           {
                               Name = row.Name_of_Director,
                               FatherName = row.FatherName,
                               Aadhaar = row.AadharNumber,
                               PAN = row.PAN,
                               EmailId = row.EmailID,
                               DateofBirth = row.DateofBirth,
                               Gender = row.Gender,
                               MobileNumber = row.Mobile,
                               Nationality = row.Nationality,
                               PassportNumber = row.Passport,
                               PermanentAddress = row.Parmanent_Address,
                               PresentAddress = row.Present_Address,
                               Photograph = row.PhotoGraph_of_Director,
                               ResidentialStatus = row.Rasidential_Status,


                           }).FirstOrDefault();
                if (getdata == null)
                {
                    getdata = new OnEditingDirector();
                }

            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return getdata;
        }

        public BM_DirectorTypeOfChanges CheckIsChangeDone(long iD)
        {
            BM_DirectorTypeOfChanges Cheack = new BM_DirectorTypeOfChanges();
            try
            {
                Cheack = (from row in entities.BM_DirectorTypeOfChanges where row.DirectorId == iD && row.IsActive == true select row).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Cheack;
        }

        public List<Director_MasterVM> GetDirectorsHistory(long DirectorID)
        {
            List<string> a = new List<string>();
            var result = (from row in entities.BM_DirectorTypeChangesMapping
                          join rows in entities.BM_DirectorMaster
                          on row.DirectorId equals rows.Id
                          where rows.Id == DirectorID && row.IsActive == false
                          select new Director_MasterVM
                          {
                              #region Assign Values
                              ID = rows.Id,
                              MappingID = row.Id,
                              Salutation = rows.Salutation,
                              DIN = rows.DIN,
                              FirstName = row.FirstName,
                              MiddleName = row.MiddleName,
                              LastName = row.LastName,
                              FullName = row.Salutation + " " + row.FirstName + " " + (row.MiddleName == "" ? "" : row.MiddleName) + " " + (row.LastName == null ? "" : row.LastName),
                              DateOfBirth = row.DOB,
                              MobileNo = row.MobileNo,
                              EmailId = row.EmailId_Personal,
                              EmailId_Official = row.EmailId_Official,
                              CreateonDate = row.UpdatedOn,
                              ChangedBy = (from x in entities.Users where x.ID == row.UpdatedBy select x.FirstName + "  " + x.LastName).FirstOrDefault(),
                              PAN = row.PAN,
                              Adhaar = row.Aadhaar,
                              PassportNo = row.PassportNo,
                              Photo_Doc_Name = row.Photo_Doc,
                              PAN_Doc_Name = row.PAN_Doc,
                              Aadhaar_Doc_Name = row.Aadhar_Doc,
                              Passport_Doc_Name = row.Passport_Doc,
                              FatherFirstName = row.FatherFirstName,
                              FatherMiddleName = row.FatherMiddleName,
                              FatherLastName = row.FatherLastName,
                              DESC_ExpiryDate = row.DSC_ExpiryDate,
                              IsApproved = (from x in entities.BM_DirectorTypeOfChanges where x.DirectorId == row.Id && x.IsActive == true select x.IsApproved).FirstOrDefault(),
                              IsDisapprove = (from x in entities.BM_DirectorTypeOfChanges where x.DirectorId == row.Id && x.IsActive == true select x.IsDisapproved).FirstOrDefault(),
                              Ispending = (from x in entities.BM_DirectorTypeOfChanges where x.DirectorId == row.Id && x.IsActive == true select x.IsPending).FirstOrDefault(),
                              status = "Approved",
                              #endregion
                          }

                      ).ToList();

            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    a.Clear();
                    var checkfordetailsedit = (from x in entities.BM_DirectorTypeOfChanges
                                               join
        y in entities.BM_DirectorTypeChangesMapping on x.Id equals y.TypeChangeMaapingID
                                               where x.DirectorId == item.ID
                                               select new
                                               {
                                                   x,
                                                   y
                                               }).FirstOrDefault();

                    if (checkfordetailsedit.y.FirstName != null || checkfordetailsedit.y.MiddleName != null || checkfordetailsedit.y.LastName != null)
                    {
                        item.Salutation = checkfordetailsedit.y.Salutation;
                        item.FirstName = checkfordetailsedit.y.FirstName;
                        item.MiddleName = checkfordetailsedit.y.MiddleName;
                        item.LastName = checkfordetailsedit.y.LastName;
                        a.Add("Name");
                    }
                    if (checkfordetailsedit.y.FatherFirstName != null || checkfordetailsedit.y.FatherMiddleName != null || checkfordetailsedit.y.FatherLastName != null)
                    {
                        item.FatherFirstName = checkfordetailsedit.y.FatherFirstName;
                        item.FatherMiddleName = checkfordetailsedit.y.FatherMiddleName;
                        item.FatherLastName = checkfordetailsedit.y.FatherLastName;
                        a.Add("Father's Name");
                    }
                    if (checkfordetailsedit.y.DOB != null)
                    {
                        item.DateOfBirth = checkfordetailsedit.y.DOB;
                        a.Add("Date of Birth");
                    }
                    if (checkfordetailsedit.y.PAN != null)
                    {
                        item.PAN = checkfordetailsedit.y.PAN;
                        a.Add("PAN");
                    }
                    //item.NumberofTypeChange = checkdirectorDetails.NumberofChanges;
                    item.TypeofChange = string.Join(",", a);
                    if (item.Ispending)
                    {
                        item.status = "Pending";
                    }
                    else if (item.IsApproved)
                    {
                        item.status = "Approved";
                    }
                    else if (item.IsDisapprove)
                    {
                        item.status = "Disapproved";
                    }
                }
            }
            return result;
        }

        public Director_MasterVM GetDirectorHistory(long id)
        {
            var objresult = (from row in entities.BM_DirectorMaster
                             join rows in entities.BM_DirectorTypeChangesMapping
                             on row.Id equals rows.DirectorId
                             where rows.Id == id
                             select new Director_MasterVM
                             {
                                 #region Assign Values
                                 ID = row.Id,
                                 Salutation = row.Salutation,

                                 DIN = row.DIN,
                                 FirstName = rows.FirstName,
                                 MiddleName = rows.MiddleName,
                                 LastName = rows.LastName,
                                 DateOfBirth = row.DOB,
                                 Gender = row.Gender,
                                 MobileNo = rows.MobileNo,
                                 EmailId = rows.EmailId_Personal,
                                 EmailId_Official = row.EmailId_Official,

                                 #region Residential Address
                                 Permenant_Address_Line1 = row.Permenant_Address_Line1,
                                 Permenant_Address_Line2 = row.Permenant_Address_Line2,
                                 Permenant_StateId = row.Permenant_StateId,
                                 Permenant_CityId = row.Permenant_CityId,
                                 Permenant_PINCode = row.Permenant_PINCode,

                                 IsSameAddress = row.IsSameAddress,

                                 Present_Address_Line1 = row.Present_Address_Line1,
                                 Present_Address_Line2 = row.Present_Address_Line2,
                                 Present_StateId = row.Present_StateId,
                                 Present_CityId = row.Present_CityId,
                                 Present_PINCode = row.Present_PINCode,
                                 #endregion

                                 EducationalQualification = row.EducationalQualification,
                                 OtherQualification = row.OtherQualification,
                                 Occupation = row.Occupation,
                                 AreaOfOccupation = row.AreaOfOccupation,
                                 OtherOccupation = row.OtherOccupation,
                                 ResidentInIndia = row.ResidentInIndia,
                                 Nationality = row.Nationality,
                                 PAN = row.PAN,
                                 Adhaar = row.Aadhaar,
                                 PassportNo = row.PassportNo,
                                 Photo_Doc_Name = row.Photo_Doc,
                                 PAN_Doc_Name = row.PAN_Doc,
                                 Aadhaar_Doc_Name = row.Aadhar_Doc,
                                 Passport_Doc_Name = row.Passport_Doc,
                                 FatherFirstName = row.Father,
                                 FatherMiddleName = row.FatherMiddleName,
                                 FatherLastName = row.FatherLastName,
                                 DESC_ExpiryDate = row.DSC_ExpiryDate,
                                 #endregion
                             }
                             ).FirstOrDefault();
            return objresult;
        }

        public ResignationofDirector DirectorResignation(ResignationofDirector _objResignation)
        {
            try
            {
                var chechIsExist = (from row in entities.BM_Directors_DetailsOfInterest
                                    where row.Director_Id == _objResignation.DirectorID
                                    && row.EntityId == _objResignation.EntityID
                                    select row).FirstOrDefault();
                if (chechIsExist != null)
                {

                    chechIsExist.IsResigned = true;
                    chechIsExist.ResignedDate = _objResignation.ResignationDate;
                    chechIsExist.CESSATION = _objResignation.CESSATION;
                    entities.SaveChanges();
                    _objResignation.isResign = (bool)chechIsExist.IsResigned;
                    _objResignation.Success = true;
                    _objResignation.Message = "Resign Successfully";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objResignation.Error = true;
                _objResignation.Message = "Server error occured";
            }
            return _objResignation;
        }

        public string GetDirectorResignation(long directorID, long entityID)
        {
            try
            {
                var getResignationForm = (from row in entities.BM_SP_DirectorResignationFormGenerate(directorID, entityID) select row).FirstOrDefault();
                return getResignationForm;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public int CountProfileUpdate(int userID)
        {
            int totalCountofProfileupdate = 0;
            try
            {
                var countProfile = (from row in entities.BM_DirectorMaster
                                    join rows in entities.BM_DirectorTypeChangesMapping on row.Id equals rows.DirectorId
                                    where rows.IsActive == true && row.UserID == userID
                                    select row).ToList();
                if (countProfile.Count > 0)
                {
                    totalCountofProfileupdate = countProfile.Count();
                }
                else
                {
                    totalCountofProfileupdate = 0;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return totalCountofProfileupdate;
        }

        public List<DirectorList_ForEntity> BODforEntity(int userID, int entityId, int CustomerId, int v2)
        {
            List<DirectorList_ForEntity> result = new List<DirectorList_ForEntity>();

            result = (from director in entities.BM_SP_GetManagementEntityWise(entityId, CustomerId)
                      orderby director.FullName
                      select new DirectorList_ForEntity
                      {
                          interestType = director.InterestType,
                          Entity_Id = entityId,
                          Director_ID = director.Director_Id,
                          UserID = director.UserID,
                          DIN = director.DIN,
                          Salutation = director.Salutation,
                          FirstName = director.FirstName,
                          LastName = director.LastName,
                          FullName = director.FullName,
                          itemlist = 0,
                          Designation = director.Designation,
                          DesignationId = director.DesignationId,
                          //PositionId = (row.IfDirector == 1 || row.IfDirector == 4) ? 1 : 2,
                          ImagePath = director.ImagePath,
                          DSC_ExpiryDate = director.DSC_ExpiryDate,
                          DateofAppointment = director.DateOfAppointment,
                          DesignationName = director.DesignationName
                      }).ToList();

            //result = (from director in entities.BM_DirectorMaster
            //          join row in entities.BM_Directors_DetailsOfInterest on director.Id equals row.Director_Id
            //          join nature in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals nature.Id
            //          from ifDir in entities.BM_Directors_Designation.Where(k => k.Id == row.IfDirector).DefaultIfEmpty()
            //          from designation in entities.BM_DesignationMaster.Where(k => k.ID == row.DirectorDesignationId).DefaultIfEmpty()
            //          where row.EntityId == entityId
            //          && director.Customer_Id == CustomerId
            //          && (nature.IsDirector == true || nature.IsMNGT == true)
            //          && director.Is_Deleted == false
            //          && row.IsDeleted == false                      
            //          && row.IsActive == true
            //          select new DirectorList_ForEntity
            //          {
            //              Entity_Id = entityId,
            //              Director_ID = row.Director_Id,
            //              UserID = director.UserID,
            //              DIN = director.DIN,
            //              Salutation = director.Salutation,
            //              FirstName = director.FirstName + "  " + director.LastName,
            //              LastName = director.LastName,
            //              FullName = (string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation) +
            //                            (string.IsNullOrEmpty(director.FirstName) ? "" : " " + director.FirstName) +
            //                            (string.IsNullOrEmpty(director.MiddleName) ? "" : " " + director.MiddleName) +
            //                            (string.IsNullOrEmpty(director.LastName) ? "" : " " + director.LastName),
            //              itemlist = 0,
            //              Designation = ifDir.Name,
            //              DesignationId = ifDir.Id,
            //              PositionId = (row.IfDirector == 1 || row.IfDirector == 4) ? 1 : 2,
            //              ImagePath = director.Photo_Doc,
            //              DSC_ExpiryDate = director.DSC_ExpiryDate,
            //              DateofAppointment = row.DateOfAppointment,
            //              DesignationName = designation.DesignationName
            //          }).Distinct().ToList();

            //result = (from director in entities.BM_DirectorMaster
            //          join row in entities.BM_Directors_DetailsOfInterest on director.Id equals row.Director_Id
            //          join nature in entities.BM_Directors_NatureOfInterest on row.NatureOfInterest equals nature.Id
            //          from ifDir in entities.BM_Directors_Designation.Where(k => k.Id == row.IfDirector).DefaultIfEmpty()
            //          where row.EntityId == entityId
            //          && director.Customer_Id == CustomerId

            //          && (nature.Id == 2 || nature.Id == 9 || nature.Id == 10)
            //          && director.Is_Deleted == false
            //          && row.IsDeleted == false
            //          select new DirectorList_ForEntity
            //          {
            //              Entity_Id = entityId,
            //              Director_ID = row.Director_Id,
            //              UserID = director.UserID,
            //              DIN = director.DIN,
            //              Salutation = director.Salutation,
            //              FirstName = director.FirstName + "  " + director.LastName,
            //              LastName = director.LastName,
            //              itemlist = 0,
            //              Designation = ifDir.Name,
            //              DesignationId = ifDir.Id,
            //              PositionId = (row.IfDirector == 1 || row.IfDirector == 4) ? 1 : 2,
            //              ImagePath = director.Photo_Doc,
            //              DSC_ExpiryDate = (DateTime)director.DSC_ExpiryDate
            //          }).Distinct().ToList();

            foreach (var item in result)
            {
                if (item.UserID == userID)
                {
                    item.itemlist = 1;
                    item.FirstName = item.FirstName + "" + "(me)";
                }
                else
                {
                    item.itemlist = 0;
                }
            }

            result = result.OrderByDescending(s => s.itemlist).ToList();
            return result;
        }

        #region Appointment/Cessation/Change in designation
        public List<DirectorMasterListVM> DirectorListForAppointmentCessation(int entityId, int customerId, int? designationId, long? detailsId, string listFor)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                return (from row in entities.BM_SP_DirectorListForApptCess(entityId, customerId, designationId, detailsId, listFor)
                        select new DirectorMasterListVM
                        {
                            ID = row.ID,
                            Name = row.DirectorName
                        }).ToList();
            }
        }
        #endregion

        #region UI Form

        public UIForm_DirectorMasterVM AppointmentOfDirector(UIForm_DirectorMasterVM obj, int userId, int customerId)
        {
            try
            {
                var natureOfInterst = 2;
                if (obj.DirectorDesignationId == 2 || obj.DirectorDesignationId == 3 || obj.DirectorDesignationId == 4 || obj.DirectorDesignationId == 5)
                {
                    natureOfInterst = 13;
                }

                if (obj.RefMasterID > 0)
                {
                    var _obj = (from details in entities.BM_Directors_DetailsOfInterest
                                where details.Id == obj.RefMasterID && details.IsDeleted == false && details.IsActive == false
                                select details).FirstOrDefault();
                    if (_obj != null)
                    {
                        DateTime appointment_Date, TenureOfAppointment;

                        if (!string.IsNullOrEmpty(obj.DateOfAppointment))
                        {
                            if (!DateTime.TryParseExact(obj.DateOfAppointment, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out appointment_Date))
                            {
                                _obj.DateOfAppointment = null;
                            }
                            else
                            {
                                _obj.DateOfAppointment = appointment_Date;
                                //_obj.IsActive = false;
                            }
                        }

                        if (!string.IsNullOrEmpty(obj.TenureOfAppointment))
                        {
                            if (!DateTime.TryParseExact(obj.TenureOfAppointment, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TenureOfAppointment))
                            {
                                _obj.TenureOfAppointment = null;
                                obj.TenureOfAppointment = "";
                            }
                            else
                            {
                                _obj.TenureOfAppointment = TenureOfAppointment;
                            }
                        }
                        _obj.DirectorDesignationId = obj.DirectorDesignationId;
                        _obj.Director_Id = obj.Director_Id;
                        //_obj.EntityId = obj.EntityId;
                        //_obj.Entity_Type = 0;
                        //_obj.NatureOfInterest = 2; //Director
                        //_obj.IfDirector = 5;//Executive
                        //_obj.IfInterestThroughRelatives = 0;
                        //_obj.PercentagesOfShareHolding = 0;
                        //_obj.IsDeleted = false;
                        //_obj.IsActive = false;

                        if (_obj.DirectorDesignationId == 10)
                        {
                            _obj.CompanyOrInstitutionForNominee = obj.CompanyOrInstitutionForNominee;
                            //_obj.EmailIdOfDirectorForNominee = obj.EmailIdOfDirectorForNominee;
                        }

                        if (_obj.DirectorDesignationId == 11)
                        {
                            _obj.OriginalDirector_Id = obj.OriginalDirector_Id;
                        }
                        else
                        {
                            _obj.OriginalDirector_Id = 0;
                        }
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();
                        obj.Success = true;
                        obj.Message = "Saved Successfully.";

                        if (_obj.DirectorDesignationId == 11)
                        {
                            //if(obj.OriginalDirector_Id > 0)
                            //{
                            //    var _objOriginalDirector = (from details in entities.BM_Directors_DetailsOfInterest
                            //                where details.EntityId == obj.EntityId && details.Director_Id == obj.OriginalDirector_Id && 
                            //                details.IsDeleted == false && details.IsActive == true
                            //                select details).FirstOrDefault();

                            //    _objOriginalDirector.OriginalDirector_Id = -1;
                            //    entities.SaveChanges();
                            //}
                        }
                    }
                }
                else
                {
                    var _obj = new BM_Directors_DetailsOfInterest();
                    _obj.DirectorDesignationId = obj.DirectorDesignationId;
                    _obj.Director_Id = obj.Director_Id;
                    _obj.EntityId = obj.EntityId;
                    _obj.Entity_Type = entities.BM_EntityMaster.Where(k => k.Id == obj.EntityId).Select(k => k.Entity_Type).FirstOrDefault();
                    _obj.NatureOfInterest = natureOfInterst; //2; //Director
                    _obj.IfDirector = obj.TypeOfDirectorShipId; //5;//Executive
                    _obj.IfInterestThroughRelatives = 0;
                    _obj.PercentagesOfShareHolding = 0;
                    _obj.IsDeleted = false;
                    _obj.IsActive = false;


                    DateTime DateOfAppointment;
                    DateTime TenureOfAppointment;

                    if (!string.IsNullOrEmpty(obj.DateOfAppointment))
                    {
                        if (!DateTime.TryParseExact(obj.DateOfAppointment, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateOfAppointment))
                        {
                            _obj.DateOfAppointment = null;
                            obj.DateOfAppointment = "";
                            _obj.ArosedOrChangedDate = DateTime.Now;
                        }
                        else
                        {
                            _obj.DateOfAppointment = DateOfAppointment;
                            _obj.ArosedOrChangedDate = DateOfAppointment;
                        }
                    }

                    if (!string.IsNullOrEmpty(obj.TenureOfAppointment))
                    {
                        if (!DateTime.TryParseExact(obj.TenureOfAppointment, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TenureOfAppointment))
                        {
                            _obj.TenureOfAppointment = null;
                            obj.TenureOfAppointment = "";
                        }
                        else
                        {
                            _obj.TenureOfAppointment = TenureOfAppointment;
                        }
                    }

                    if (_obj.DirectorDesignationId == 10)
                    {
                        _obj.CompanyOrInstitutionForNominee = obj.CompanyOrInstitutionForNominee;
                        //_obj.EmailIdOfDirectorForNominee = obj.EmailIdOfDirectorForNominee;
                    }

                    if (_obj.DirectorDesignationId == 11)
                    {
                        _obj.OriginalDirector_Id = obj.OriginalDirector_Id;
                    }
                    else
                    {
                        _obj.OriginalDirector_Id = 0;
                    }

                    _obj.CreatedBy = userId;
                    _obj.CreatedOn = DateTime.Now;

                    entities.BM_Directors_DetailsOfInterest.Add(_obj);
                    entities.SaveChanges();

                    obj.Success = true;
                    obj.Message = "Saved Successfully.";

                    obj.RefMasterID = _obj.Id;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public UIForm_DirectorMasterCessationVM CessationOfDirector(UIForm_DirectorMasterCessationVM obj, int userId, int customerId)
        {
            try
            {
                if (obj.RefMasterID > 0)
                {
                    var _obj = (from details in entities.BM_Directors_DetailsOfInterest
                                where details.Id == obj.RefMasterID && details.IsDeleted == false && details.IsActive == true
                                select details).FirstOrDefault();
                    if (_obj != null)
                    {
                        DateTime cessationEffectFrom;

                        if (!string.IsNullOrEmpty(obj.CessationEffectFrom))
                        {
                            if (!DateTime.TryParseExact(obj.CessationEffectFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out cessationEffectFrom))
                            {
                                _obj.CessationEffectFrom = null;
                            }
                            else
                            {
                                _obj.CessationEffectFrom = cessationEffectFrom;
                                //_obj.IsActive = false;
                            }
                        }

                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Resignation Of Director
        public Director_ResignationVM ResignationOfDirector(long id)
        {
            var obj = new Director_ResignationVM();
            try
            {
                var _obj = (from row in entities.BM_Directors_DetailsOfInterest
                            join dir in entities.BM_DirectorMaster on row.Director_Id equals dir.Id
                            where row.Id == id
                            select new
                            {
                                row.Id,
                                row.Entity_Type,
                                row.Director_Id,
                                dir.Salutation,
                                dir.FirstName,
                                dir.MiddleName,
                                dir.LastName,
                                row.CessationEffectFrom,
                                row.ResignedDate,

                                row.ResignationDoc,
                                row.ResignationFileDataId,

                                row.IsResigned
                            }).FirstOrDefault();

                if (_obj != null)
                {
                    obj.IntrestedId = _obj.Id;
                    obj.Entity_TypeForResignation = _obj.Entity_Type;
                    obj.DirectorID = _obj.Director_Id;
                    obj.DirectorName = (string.IsNullOrEmpty(_obj.Salutation) ? "" : _obj.Salutation) +
                                        (string.IsNullOrEmpty(_obj.FirstName) ? "" : " " + _obj.FirstName) +
                                        (string.IsNullOrEmpty(_obj.MiddleName) ? "" : " " + _obj.MiddleName) +
                                        (string.IsNullOrEmpty(_obj.LastName) ? "" : " " + _obj.LastName);

                    obj.DateOfResignation = _obj.CessationEffectFrom;
                    obj.DateOfResignationSubmit = _obj.ResignedDate;

                    obj.Resignation_DocName = _obj.ResignationDoc;
                    obj.FileDataId = _obj.ResignationFileDataId;
                    obj.IsResigned = _obj.IsResigned;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public Director_ResignationVM SaveResignationOfDirector(Director_ResignationVM obj, int userId)
        {
            try
            {
                if (obj.Resignation_Doc == null)
                {
                    obj.Error = true;
                    obj.Message = "Please upload resignation.";
                    return obj;
                }
                var _obj = (from details in entities.BM_Directors_DetailsOfInterest
                            where details.Id == obj.IntrestedId && details.IsDeleted == false
                            select details).FirstOrDefault();
                if (_obj != null)
                {
                    var entitydetails = (from entity in entities.BM_EntityMaster
                                         where entity.Id == _obj.EntityId
                                         select new
                                         {
                                             entity.Entity_Type,
                                             entity.Customer_Id
                                         }).FirstOrDefault();
                    if (entitydetails != null)
                    {
                        if (entitydetails.Entity_Type == SecretarialConst.EntityTypeID.LISTED)
                        {
                            if (obj.Performer_ID <= 0 || obj.Performer_ID == null)
                            {
                                obj.Error = true;
                                obj.Message = "Please select Performer";
                                return obj;
                            }
                            else if (obj.Reviewer_ID <= 0 || obj.Reviewer_ID == null)
                            {
                                obj.Error = true;
                                obj.Message = "Please select Reviewer";
                                return obj;
                            }

                        }

                        #region Upload Resignation Letter
                        if (obj.Resignation_Doc != null)
                        {
                            string path = "~/Areas/BM_Management/Documents/" + obj.DirectorID;
                            if (obj.Resignation_Doc.ContentLength > 0)
                            {
                                MemoryStream target = new MemoryStream();
                                obj.Resignation_Doc.InputStream.CopyTo(target);

                                int fileVersion = 0;

                                var objFileData = new FileDataVM()
                                {
                                    FileData = target.ToArray(),
                                    FileName = obj.Resignation_Doc.FileName,
                                    FilePath = path
                                };

                                fileVersion = 1;

                                objFileData.Version = Convert.ToString(fileVersion);
                                objFileData = objIFileData_Service.Save(objFileData, userId);

                                if (objFileData.FileID > 0)
                                {
                                    obj.FileDataId = objFileData.FileID;
                                    obj.Resignation_DocName = objFileData.FileName;
                                }
                            }
                        }
                        #endregion

                        obj.DirectorID = _obj.Director_Id;
                        _obj.CessationEffectFrom = obj.DateOfResignation;
                        _obj.ResignedDate = obj.DateOfResignationSubmit;
                        _obj.ResignedTime = obj.ResignedTime;

                        _obj.ResignationDoc = obj.Resignation_DocName;
                        _obj.ResignationFileDataId = obj.FileDataId;

                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;
                        _obj.IsActive = false;
                        _obj.IsResigned = true;
                        obj.IsResigned = true;
                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = "Saved Successfully.";

                        #region Create Open Agenda (Resignation of Director)
                        long agendaId = 0;
                        long uiFormId = 0;
                        int? fileId = null;
                        string refMasterName = "";

                        switch (_obj.DirectorDesignationId)
                        {
                            case 1:
                                if (_obj.IfDirector == 1 || _obj.IfDirector == 5)
                                {
                                    uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR;
                                    refMasterName = "Resignation of Director (Executive)";
                                }
                                else if (_obj.IfDirector == 4 || _obj.IfDirector == 6)
                                {
                                    uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE;
                                    refMasterName = "Resignation of Director (Non-Executive)";
                                }
                                else if (_obj.IfDirector == 7 || _obj.IfDirector == 9)
                                {
                                    uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE_INDEPENDANT;
                                    refMasterName = "Resignation of Director (Independent)";
                                }
                                break;
                            case 2:
                                uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_MANAGER;
                                refMasterName = "Resignation of Manager";
                                break;
                            case 3:
                                uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_CS;
                                refMasterName = "Resignation of CS";
                                break;
                            case 4:
                                uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_CEO;
                                refMasterName = "Resignation of CEO";
                                break;
                            case 5:
                                uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_CFO;
                                refMasterName = "Resignation of CFO";
                                break;
                            case 6:
                                uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_MD;
                                refMasterName = "Resignation of MD";
                                break;
                            case 7:
                                uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_WTD;
                                refMasterName = "Resignation of Whole-Time Director";
                                break;
                            case 8:
                                if (_obj.IfDirector == 7 || _obj.IfDirector == 9)
                                {
                                    uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_ADDD_INDEPENDANT;
                                    refMasterName = "Resignation of Additional Director (Independent)";
                                }
                                else
                                {
                                    uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_ADDD;
                                    refMasterName = "Resignation of Additional Director";
                                }
                                break;
                            case 9:
                                uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_CAVD;
                                refMasterName = "Resignation of Director Appointed in Casual vacancy";
                                break;
                            case 10:
                                uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_NOMD;
                                refMasterName = "Resignation of Nominee director";
                                break;
                            case 11:
                                uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_ALTD;
                                refMasterName = "Resignation of Alternate director";
                                break;
                            default:
                                break;
                        }

                        if (uiFormId > 0)
                        {
                            agendaId = (from agenda in entities.BM_AgendaMaster
                                        where agenda.UIFormID == uiFormId && agenda.EntityType == entitydetails.Entity_Type && agenda.IsDeleted == false && agenda.MeetingTypeId == 10
                                        select agenda.BM_AgendaMasterId
                                   ).FirstOrDefault();

                            if (agendaId > 0)
                            {
                                var _objOpenAgenda = new BM_MeetingOpenAgendaOnCompliance();
                                _objOpenAgenda.EntityId = _obj.EntityId;
                                _objOpenAgenda.RefMasterName = refMasterName; // "Resignation of Director";
                                _objOpenAgenda.RefMaster = "D";
                                _objOpenAgenda.RefMasterID = _obj.Id;
                                _objOpenAgenda.ScheduleOnID = 0;
                                _objOpenAgenda.AgendaId = agendaId;
                                _objOpenAgenda.MeetingTypeId = 10;
                                _objOpenAgenda.IsNoted = false;
                                _objOpenAgenda.IsDeleted = false;
                                _objOpenAgenda.CreatedBy = userId;
                                _objOpenAgenda.CreatedOn = DateTime.Now;
                                entities.BM_MeetingOpenAgendaOnCompliance.Add(_objOpenAgenda);
                                entities.SaveChanges();
                            }
                        }

                        #region Compliance activation on resignation of director for Listed company.
                        if (entitydetails.Entity_Type == SecretarialConst.EntityTypeID.LISTED)
                        {
                            if (_obj.ResignationFileDataId > 0)
                            {
                                fileId = (int)_obj.ResignationFileDataId;
                            }

                            DateTime? scheduleOn = null;

                            if (obj.DateOfResignationSubmit.HasValue && !string.IsNullOrEmpty(obj.ResignedTime))
                            {
                                scheduleOn = Convert.ToDateTime(Convert.ToDateTime(obj.DateOfResignationSubmit).ToString("dd/MMM/yyyy") + " " + obj.ResignedTime);
                            }
                            else
                            {
                                scheduleOn = Convert.ToDateTime(obj.DateOfResignationSubmit);
                            }
                            //objIDirectorCompliances.GenerateScheduleOnForEntityOnEvent((int)entitydetails.Customer_Id, _obj.EntityId, SecretarialConst.ComplianceAutoCompleteOn.RESIGNATION_OF_DIRECTOR, DateTime.Now, 1, "Admin", fileId, obj.Performer_ID, obj.Reviewer_ID, scheduleOn, obj.DirectorID, "D", obj.IntrestedId);
                            objIDirectorCompliances.GenerateScheduleOnForEntityOnEvent((int)entitydetails.Customer_Id, _obj.EntityId, SecretarialConst.ComplianceAutoCompleteOn.RESIGNATION_OF_DIRECTOR, DateTime.Now, 1, "Admin", fileId, obj.Performer_ID, obj.Reviewer_ID, scheduleOn, 0, "D", obj.IntrestedId);
                        }
                        #endregion

                        #endregion
                    }

                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public Message RevertResignationOfDirector(long id, int userId)
        {
            var obj = new Message();
            try
            {
                var _obj = (from details in entities.BM_Directors_DetailsOfInterest
                            where details.Id == id 
                            select details).FirstOrDefault();

                if(_obj != null)
                {
                    #region check Agenda Noted
                    var openAgenda = (from row in entities.BM_MeetingOpenAgendaOnCompliance
                                      where row.RefMasterID == id && row.RefMaster == "D" && row.IsDeleted == false
                                      select new
                                      {
                                          row.Id,
                                          row.IsNoted
                                      }).FirstOrDefault();
                    if(openAgenda != null)
                    {
                        if(openAgenda.IsNoted == true)
                        {
                            var meetingDetails = (from row in entities.BM_MeetingAgendaMapping
                                                join meeting in entities.BM_Meetings on row.MeetingID equals meeting.MeetingID
                                                where row.RefOpenAgendaId == openAgenda.Id && row.IsDeleted == false && meeting.IsDeleted == false
                                                select new
                                                {
                                                    row.MeetingAgendaMappingID,
                                                    meeting.MeetingTitle,
                                                }).FirstOrDefault();
                            if(meetingDetails != null)
                            {
                                obj.Error = true;
                                obj.Message = "Agenda is used in "+ meetingDetails.MeetingTitle;
                                return obj;
                            }
                        }
                    }
                    #endregion

                    #region Check Compliance Closed or not
                    var entityType = (from row in entities.BM_EntityMaster
                                      where row.Id == _obj.EntityId
                                      select row.Entity_Type).FirstOrDefault();
                    if(entityType == SecretarialConst.EntityTypeID.LISTED)
                    {
                        var lstComplianceSchedule = (from row in entities.BM_ComplianceScheduleOnNew
                                                  where row.RefMasterId == id && row.RefMaster == "D" && row.IsDeleted == false
                                                  select row).ToList();
                        if(lstComplianceSchedule != null)
                        {
                            foreach (var complianceSchedule in lstComplianceSchedule)
                            {
                                if (complianceSchedule.StatusId == 4 || complianceSchedule.StatusId == 5)
                                {
                                    obj.Error = true;
                                    obj.Message = "Resignation related compliances already closed.";
                                    return obj;
                                }
                            }

                            foreach (var complianceSchedule in lstComplianceSchedule)
                            {
                                complianceSchedule.IsActive = false;
                                complianceSchedule.IsDeleted = true;
                                complianceSchedule.UpdatedBy = userId;
                                complianceSchedule.UpdatedOn = DateTime.Now;
                                entities.SaveChanges();

                                var comSchedule = (from row in entities.ComplianceScheduleOns
                                                   where row.ID == complianceSchedule.ScheduleOnID && row.IsActive == true
                                                   select row).FirstOrDefault();
                                if (comSchedule != null)
                                {
                                    comSchedule.IsActive = false;
                                    entities.SaveChanges();
                                }
                            }
                        }
                    }
                    #endregion

                    #region Revert Resignation
                    if(openAgenda != null)
                    {
                        var openAgendaDetails = (from row in entities.BM_MeetingOpenAgendaOnCompliance
                                                 where row.Id == openAgenda.Id && row.IsDeleted == false
                                                 select row
                                            ).FirstOrDefault();
                        if (openAgendaDetails != null)
                        {
                            openAgendaDetails.IsDeleted = true;
                            openAgendaDetails.UpdatedBy = userId;
                            openAgendaDetails.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }
                    }

                    _obj.CessationEffectFrom = null;
                    _obj.ResignedDate = null;
                    _obj.ResignedTime = null;

                    _obj.ResignationDoc = null;
                    _obj.ResignationFileDataId = null;

                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;
                    _obj.IsActive = true;
                    _obj.IsDeleted = false;
                    _obj.IsResigned = null;
                    entities.SaveChanges();

                    obj.Success = true;
                    obj.Message = "Saved Successfully.";
                    #endregion
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Add by Ruchi on 19th of June 2020
        public List<Director_MasterVM> GetDirectorListinCompany(int customerId, int userId)
        {
            var getDirector = (from dir in entities.BM_SP_DirectorProfileDetails(userId)

                               select new Director_MasterVM
                               {
                                   #region Assign Values
                                   ID = dir.Id,
                                   Salutation = dir.Salutation,

                                   DIN = dir.DIN,
                                   FirstName = dir.FirstName,
                                   MiddleName = dir.MiddleName,
                                   LastName = dir.LastName,
                                   FullName = dir.Salutation + " " + dir.FirstName + " " + (dir.MiddleName == "" ? "" : dir.MiddleName) + " " + (dir.LastName == null ? "" : dir.LastName),
                                   DateOfBirth = dir.DOB,
                                   MobileNo = dir.MobileNo,
                                   EmailId = dir.EmailId_Personal,
                                   EmailId_Official = dir.EmailId_Official,
                                   Photo_Doc_Name = dir.Photo_Doc,

                                   #endregion


                               }).ToList();
            return getDirector;

        }

        //public bool Update_Director(BM_DirectorMaster obj, out bool hasError)
        //{
        //    try
        //    {
        //        using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
        //        {
        //            var checkIsDirectorExist = (from row in entities.BM_DirectorMaster where row.Id == obj.Id && row.Customer_Id == obj.Customer_Id select row).FirstOrDefault();
        //            if (checkIsDirectorExist != null)
        //            {
        //                entities.Entry(obj).State = System.Data.Entity.EntityState.Modified;
        //                int count = entities.SaveChanges();
        //                if (count > 0)
        //                {
        //                    hasError = false;
        //                    return true;
        //                }
        //                else
        //                {
        //                    hasError = false;
        //                    return false;
        //                }
        //            }
        //            else
        //            {
        //                hasError = false;
        //                return false;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        hasError = true;
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return false;
        //    }
        //}


        public bool Update_Director(BM_DirectorMaster obj, out bool hasError)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var checkIsDirectorExist = (from row in entities.BM_DirectorMaster where row.Id == obj.Id && row.Customer_Id == obj.Customer_Id select row).FirstOrDefault();
                    if (checkIsDirectorExist != null)
                    {
                        //entities.Entry(obj).State = System.Data.Entity.EntityState.Modified;

                        #region Assign Values
                        checkIsDirectorExist.Salutation = obj.Salutation;

                        checkIsDirectorExist.DIN = obj.DIN;
                        checkIsDirectorExist.Salutation = obj.Salutation;
                        checkIsDirectorExist.FirstName = obj.FirstName;
                        checkIsDirectorExist.MiddleName = obj.MiddleName;
                        checkIsDirectorExist.LastName = string.IsNullOrEmpty(obj.LastName) ? "" : obj.LastName;
                        checkIsDirectorExist.DOB = (DateTime)obj.DOB;
                        checkIsDirectorExist.Gender = (int)obj.Gender;
                        checkIsDirectorExist.MobileNo = obj.MobileNo;
                        checkIsDirectorExist.EmailId_Personal = obj.EmailId_Personal;
                        checkIsDirectorExist.EmailId_Official = obj.EmailId_Official;
                        checkIsDirectorExist.FSalutations = obj.FSalutations;
                        checkIsDirectorExist.Father = obj.Father;
                        checkIsDirectorExist.FatherMiddleName = obj.FatherMiddleName;
                        checkIsDirectorExist.FatherLastName = obj.FatherLastName;

                        checkIsDirectorExist.Mother = obj.Mother;

                        #region Residential Address

                        checkIsDirectorExist.Permenant_Address_Line1 = obj.Permenant_Address_Line1;
                        checkIsDirectorExist.Permenant_Address_Line2 = obj.Permenant_Address_Line2;
                        checkIsDirectorExist.Permenant_StateId = obj.Permenant_StateId;
                        checkIsDirectorExist.Permenant_CityId = (int)obj.Permenant_CityId;
                        checkIsDirectorExist.Permenant_PINCode = obj.Permenant_PINCode;

                        checkIsDirectorExist.IsSameAddress = obj.IsSameAddress;

                        checkIsDirectorExist.Present_Address_Line1 = obj.Present_Address_Line1;
                        checkIsDirectorExist.Present_Address_Line2 = obj.Present_Address_Line2;
                        checkIsDirectorExist.Present_StateId = (int)obj.Present_StateId;
                        checkIsDirectorExist.Present_CityId = (int)obj.Present_CityId;
                        checkIsDirectorExist.Present_PINCode = obj.Present_PINCode;

                        #endregion

                        checkIsDirectorExist.EducationalQualification = obj.EducationalQualification;
                        checkIsDirectorExist.OtherQualification = obj.OtherQualification;
                        checkIsDirectorExist.Occupation = obj.Occupation;
                        checkIsDirectorExist.AreaOfOccupation = obj.AreaOfOccupation;
                        checkIsDirectorExist.OtherOccupation = obj.OtherOccupation;
                        checkIsDirectorExist.ResidentInIndia = obj.ResidentInIndia;
                        checkIsDirectorExist.Nationality = (int)obj.Nationality;
                        checkIsDirectorExist.PAN = obj.PAN;
                        checkIsDirectorExist.Aadhaar = obj.Aadhaar;
                        checkIsDirectorExist.PassportNo = obj.PassportNo;
                        //obj_BM_DirectorMaster.PAN_Doc = obj.PAN_Doc;
                        //obj_BM_DirectorMaster.Aadhar_Doc = obj.Aadhar_Doc;
                        //obj_BM_DirectorMaster.Passport_Doc = obj.Passport_Doc;
                        //obj_BM_DirectorMaster.Photo_Doc = obj.Photo_Doc;
                        checkIsDirectorExist.DSC_ExpiryDate = obj.DSC_ExpiryDate;

                        checkIsDirectorExist.Customer_Id = obj.Customer_Id;
                        checkIsDirectorExist.Is_Deleted = false;

                        checkIsDirectorExist.UpdatedBy = obj.CreatedBy;
                        checkIsDirectorExist.UpdatedOn = DateTime.Now;

                        checkIsDirectorExist.IsDirecor = true;
                        checkIsDirectorExist.IsActive = false;
                        #endregion

                        int count = entities.SaveChanges();
                        if (count > 0)
                        {
                            hasError = false;
                            return true;
                        }
                        else
                        {
                            hasError = false;
                            return false;
                        }
                    }
                    else
                    {
                        hasError = false;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                hasError = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }


        public OnEditingDirector checkIscheckedornot(long directorID)
        {
            try
            {
                var checkcheckedornot = (from row in entities.BM_DirectorTypeOfChanges
                                         where row.DirectorId == directorID && row.IsActive == true
                                         select new OnEditingDirector

                                         {
                                             Name = row.Name_of_Director,
                                             FatherName = row.FatherName,
                                             Nationality = row.Nationality,
                                             PAN = row.PAN,
                                             PassportNumber = row.Passport,
                                             Photograph = row.PhotoGraph_of_Director,

                                             DateofBirth = row.DateofBirth,
                                             Gender = row.Gender,
                                             EmailId = row.EmailID,
                                             MobileNumber = row.Mobile,
                                             PermanentAddress = row.Parmanent_Address,
                                             PresentAddress = row.Present_Address,
                                             ResidentialStatus = row.Rasidential_Status,
                                             Aadhaar = row.AadharNumber,
                                         }
                                         ).FirstOrDefault();
                return checkcheckedornot;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                OnEditingDirector obj = new OnEditingDirector();
                return obj;
            }
        }

        public bool checkchangebydirectorornot(int userID)
        {
            try
            {
                var checkedDirector = (from row in entities.BM_DirectorTypeOfChanges
                                       join director in entities.BM_DirectorMaster
                                       on row.DirectorId equals director.Id
                                       where row.UserID == userID && row.DirectorId == director.Id && row.IsActive == true
                                       select row).FirstOrDefault();
                if (checkedDirector != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }


        #endregion


        #region Get Director User Id
        public long? GetUserId(long directorId)
        {
            try
            {
                var userID = (from director in entities.BM_DirectorMaster
                              where director.Id == directorId && director.Is_Deleted == false
                              select director.UserID).FirstOrDefault();
                return userID;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Get Director Email
        public string GetEmail(long directorId)
        {
            try
            {
                var mailId = (from director in entities.BM_DirectorMaster
                              where director.Id == directorId && director.Is_Deleted == false
                              select director.EmailId_Official).FirstOrDefault();
                return mailId;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Check user already exists in case of old user
        public long? GetUserIdForCreateNewDirector(string emailId, int customerId)
        {
            try
            {
                var directorId = (from dir in entities.BM_DirectorMaster
                                  where dir.Customer_Id == customerId 
                                  && dir.EmailId_Official == emailId 
                                  && dir.Is_Deleted == false
                                  select dir.Id).FirstOrDefault();

                if (directorId > 0)
                {
                    return 0;
                }
                else
                {
                    var userID = (from u in entities.Users
                                  where u.Email == emailId 
                                  && u.CustomerID == customerId 
                                  && u.IsDeleted == false
                                  select u.ID).FirstOrDefault();

                    return userID;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion
        public bool Update_DirectorDocInfo(BM_DirectorTypeChangesMapping obj)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    entities.BM_DirectorTypeChangesMapping.Attach(obj);
                    if (!string.IsNullOrEmpty(obj.Photo_Doc))
                    {
                        entities.Entry(obj).Property(k => k.Photo_Doc).IsModified = true;
                    }
                    if (!string.IsNullOrEmpty(obj.PAN_Doc))
                    {
                        entities.Entry(obj).Property(k => k.PAN_Doc).IsModified = true;
                    }
                    if (!string.IsNullOrEmpty(obj.Aadhar_Doc))
                    {
                        entities.Entry(obj).Property(k => k.Aadhar_Doc).IsModified = true;
                    }
                    if (!string.IsNullOrEmpty(obj.Passport_Doc))
                    {
                        entities.Entry(obj).Property(k => k.Passport_Doc).IsModified = true;
                    }

                    int count = entities.SaveChanges();
                    if (count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return false;
            }
        }

        public bool GetDetailsofIntrest(long id)
        {
            bool success = false;
            try
            {
                var getdetailofIntrest = (from DOI in entities.BM_Directors_DetailsOfInterest
                                          join NOI in entities.BM_Directors_NatureOfInterest on DOI.NatureOfInterest equals NOI.Id
                                          where DOI.Director_Id == id
                                          && DOI.IsActive == true
                                          && DOI.IsDeleted == false
                                          && NOI.IsDirector == true
                                          select DOI).FirstOrDefault();
                if (getdetailofIntrest != null)
                {
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }
        
    }
}