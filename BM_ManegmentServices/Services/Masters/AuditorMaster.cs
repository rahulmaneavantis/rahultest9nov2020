﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using BM_ManegmentServices.Data;
using System.IO;
using BM_ManegmentServices.Services.DocumentManagenemt;

namespace BM_ManegmentServices.Services.Masters
{
    public class AuditorMaster : IAuditorMaster
    {
        IFileData_Service objIFileData_Service;
        public AuditorMaster(IFileData_Service objFileData_Service)
        {
            objIFileData_Service = objFileData_Service;
        }

        #region Auditor Maserter
        public VMAuditor AddAuditData(VMAuditor _vmauditor)
        {
            bool Success = false;
            try
            {
                if (_vmauditor != null)
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        var checkEmailId = (from row in entities.BM_Auditor_Master
                                            where row.Email_Id == _vmauditor.Email_Id 
                                            && row.IsActive == true
                                            && row.CustomerId == _vmauditor.CustomerId
                                            select row).FirstOrDefault();

                        var CheckPanCard = (from row in entities.BM_Auditor_Master
                                            where row.PAN == _vmauditor.Pan_Number 
                                            && row.IsActive == true
                                            && row.CustomerId == _vmauditor.CustomerId
                                            select row).FirstOrDefault();

                        var CheckMembernumber = (from row in entities.BM_Auditor_Master
                                                 where row.Reg_MemberNumber == _vmauditor.AuditorFirmRegistration_Number 
                                                 && row.IsActive == true
                                                 && row.CustomerId == _vmauditor.CustomerId
                                                 select row).FirstOrDefault();
                        if (checkEmailId != null || CheckPanCard != null || CheckMembernumber != null)
                        {
                            Success = false;
                        }
                        else
                        {
                            Success = true;
                        }

                        if (Success)
                        {
                            BM_Auditor_Master obj = new BM_Auditor_Master();
                            obj.Entity_Id = _vmauditor.Entity_Id;
                            obj.CustomerId = _vmauditor.CustomerId;
                            obj.Email_Id = _vmauditor.Email_Id;
                            obj.MobileNo = _vmauditor.MobileNo;
                            obj.FullName_FirmName = _vmauditor.Audit_FirmName_FullName;
                            obj.Audit_Type = _vmauditor.Auditor_Type;
                            obj.Category_Id = _vmauditor.Audit_CategoryId;
                            obj.AddressLine1 = _vmauditor.AddressLine1;
                            obj.AddressLine2 = _vmauditor.AddressLine2;
                            obj.Country_Id = _vmauditor.country;
                            obj.city_Id = _vmauditor.city;
                            obj.state_Id = _vmauditor.state;
                            obj.PAN = _vmauditor.Pan_Number;
                            obj.Pin_Number = _vmauditor.PinNumber;
                            obj.Reg_MemberNumber = _vmauditor.AuditorFirmRegistration_Number;
                            obj.CustomerId = _vmauditor.CustomerId;
                            obj.IsActive = true;
                            obj.CreatedOn = DateTime.Now;
                            obj.Createdby = _vmauditor.logedinuserId;
                            entities.BM_Auditor_Master.Add(obj);
                            entities.SaveChanges();

                            _vmauditor.successMessage = true;
                            _vmauditor.showMessage = "Auditor/Firm saved successfully";
                        }
                        else
                        {
                            if (checkEmailId != null)
                            {
                                _vmauditor.errorMessage = true;
                                _vmauditor.showMessage = "Auditor/Firm with Same Email already exist";
                            }
                            else if (CheckPanCard != null)
                            {
                                _vmauditor.errorMessage = true;
                                _vmauditor.showMessage = "Auditor/Firm with Same PAN already exist";
                            }
                            else if (CheckMembernumber != null)
                            {
                                _vmauditor.errorMessage = true;
                                _vmauditor.showMessage = "Auditor/Firm with Same Member/Registration already exist";
                            }
                        }
                    }
                }
                else
                {
                    _vmauditor.errorMessage = true;
                    _vmauditor.showMessage = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                _vmauditor.errorMessage = true;
                _vmauditor.showMessage = "Server error occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _vmauditor;
        }

        public VMAuditor UpdateAuditData(VMAuditor _vmauditor)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    bool Success = false;
                    var CheckAuditorEmail = (from AU in entities.BM_Auditor_Master 
                                             where AU.Id != _vmauditor.Id 
                                             && AU.Email_Id == _vmauditor.Email_Id
                                             && AU.CustomerId == _vmauditor.CustomerId
                                             && AU.IsActive == true
                                             select AU).ToList();

                    var CheckAuditorPan = (from AU in entities.BM_Auditor_Master
                                           where AU.Id != _vmauditor.Id
                                           && AU.PAN == _vmauditor.Pan_Number
                                           && AU.CustomerId == _vmauditor.CustomerId
                                           && AU.IsActive == true
                                           select AU).ToList();

                    var CheckAuditorRegistratinNumber = (from AU in entities.BM_Auditor_Master 
                                                         where AU.Id != _vmauditor.Id 
                                                         && AU.Reg_MemberNumber == _vmauditor.AuditorFirmRegistration_Number
                                                         && AU.CustomerId == _vmauditor.CustomerId
                                                         && AU.IsActive == true
                                                         select AU).ToList();

                    var CheckAuditorMobileno = (from AU in entities.BM_Auditor_Master 
                                                where AU.Id != _vmauditor.Id 
                                                && AU.MobileNo == _vmauditor.MobileNo
                                                && AU.CustomerId == _vmauditor.CustomerId
                                                && AU.IsActive == true
                                                select AU).ToList();
                    
                    if (CheckAuditorEmail.Count > 0)
                    {
                        Success = false;
                    }
                    else if (CheckAuditorPan.Count > 0)
                    {
                        Success = false;
                    }
                    else if (CheckAuditorRegistratinNumber.Count > 0)
                    {
                        Success = false;
                    }
                    else if (CheckAuditorMobileno.Count > 0)
                    {
                        Success = false;
                    }
                    else
                    {
                        Success = true;
                    }
                    if (Success)
                    {
                        var checkdataforAudit = (from row in entities.BM_Auditor_Master
                                                 where row.Reg_MemberNumber == _vmauditor.AuditorFirmRegistration_Number
                                                 && row.Id == _vmauditor.Id 
                                                 && row.CustomerId== _vmauditor.CustomerId
                                                 select row).FirstOrDefault();
                        if (checkdataforAudit != null)
                        {
                            checkdataforAudit.Email_Id = _vmauditor.Email_Id;
                            checkdataforAudit.FullName_FirmName = _vmauditor.Audit_FirmName_FullName;
                            checkdataforAudit.Category_Id = _vmauditor.Audit_CategoryId;
                            checkdataforAudit.Audit_Type = _vmauditor.Auditor_Type;
                            checkdataforAudit.AddressLine1 = _vmauditor.AddressLine1;
                            checkdataforAudit.AddressLine2 = _vmauditor.AddressLine2;
                            checkdataforAudit.Country_Id = _vmauditor.country;
                            checkdataforAudit.city_Id = _vmauditor.city;
                            checkdataforAudit.state_Id = _vmauditor.state;
                            checkdataforAudit.PAN = _vmauditor.Pan_Number;
                            checkdataforAudit.MobileNo = _vmauditor.MobileNo;
                            checkdataforAudit.Reg_MemberNumber = _vmauditor.AuditorFirmRegistration_Number;
                            checkdataforAudit.IsActive = true;
                            checkdataforAudit.UpdatedOn = DateTime.Now;
                            checkdataforAudit.Updatedby = _vmauditor.logedinuserId;
                            checkdataforAudit.CustomerId = _vmauditor.CustomerId;
                            entities.SaveChanges();

                            _vmauditor.successMessage = true;
                            _vmauditor.showMessage = "Auditor/Firm updated successfully";

                        }
                        else
                        {
                            _vmauditor.errorMessage = true;
                            _vmauditor.showMessage = "Auditor/Firm data not found";

                        }
                    }
                    else
                    {
                        if (CheckAuditorEmail.Count > 0)
                        {
                            _vmauditor.errorMessage = true;
                            _vmauditor.showMessage = "Auditor/Firm EmilId already exist";

                        }
                        else if (CheckAuditorPan.Count > 0)
                        {
                            _vmauditor.errorMessage = true;
                            _vmauditor.showMessage = "Auditor/Firm PAN already exist";

                        }
                        else if (CheckAuditorRegistratinNumber.Count > 0)
                        {
                            _vmauditor.errorMessage = true;
                            _vmauditor.showMessage = "Auditor/Firm Member/Registration Number already exist";

                        }

                    }
                }

            }
            catch (Exception ex)
            {
                _vmauditor.errorMessage = true;
                _vmauditor.showMessage = "Server error occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _vmauditor;
        }

        public List<VMAuditor> GetAuditorDetails(int customerId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _objgetAuditordtls = (from row in entities.BM_Auditor_Master
                                              where row.IsActive == true
                                              && row.CustomerId == customerId
                                              select new VMAuditor
                                              {
                                                  Id = row.Id,
                                                  Name = row.FullName_FirmName,
                                                  Email_Id = row.Email_Id,
                                                  Pan_Number = row.PAN,
                                                  AuditorFirmRegistration_Number = row.Reg_MemberNumber
                                              }).ToList();
                    return _objgetAuditordtls;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<VMAuditor> obj = new List<VMAuditor>();
                return obj;

            }
        }

        public VMAuditor GetAuditorbyId(int id)
        {
            VMAuditor obj = new VMAuditor();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (id > 0)
                    {
                        obj = (from row in entities.BM_Auditor_Master
                               where row.Id == id && row.IsActive == true
                             

                               select new VMAuditor
                               {
                                   Id = row.Id,
                                   Audit_FirmName_FullName = row.FullName_FirmName,

                                   Audit_CategoryId = (int)row.Category_Id,
                                   Auditor_Type = row.Audit_Type,
                                   Pan_Number = row.PAN,
                                   Email_Id = row.Email_Id,
                                   AuditorFirmRegistration_Number = row.Reg_MemberNumber,
                                   AddressLine1 = row.AddressLine1,
                                   AddressLine2 = row.AddressLine2,
                                   country = row.Country_Id,
                                   state = row.state_Id,
                                   city = row.city_Id,
                                   MobileNo = row.MobileNo,
                                   PinNumber = row.Pin_Number,
                                   IsMapped = false,
                               }).FirstOrDefault();
                        if(obj!=null)
                        {
                            var checkforMapping = (from x in entities.BM_StatutoryAuditor join y in entities.BM_StatutoryAuditor_Mapping on x.Id equals y.Statutory_AuditorId where y.Auditor_ID == obj.Id && y.IsActive == true && x.IsActive == true  select y).FirstOrDefault();
                            if(checkforMapping!=null)
                            {
                                obj.IsMapped = false;
                            }
                            else
                            {
                                obj.IsMapped = true;
                            }
                        }
                        return obj;
                    }
                    else
                    {
                        obj.errorMessage = true;
                        obj.showMessage = "Something went wrong";
                        return obj;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.errorMessage = true;
                obj.showMessage = "Server error occurred";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public IEnumerable<BM_EntityMaster> GetDropDownforEntityType()
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getEntityName = (from row in entities.BM_EntityMaster
                                         where row.Is_Deleted == false
                                         select row).ToList();
                    return getEntityName;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;

            }
        }

        #endregion

        #region Statutory Audito
        public IEnumerable<BM_Nature_of_Appointment> GetDropDownNatureofAppointment()
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _objget_nature_of_appointment = (from row in entities.BM_Nature_of_Appointment
                                                         where row.IsActive == true
                                                         select row).ToList();
                    return _objget_nature_of_appointment;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public IEnumerable<VMAuditorforDropdown> GetDropDownAuditors(int criteriaId, int type, int EntityId, int CustomerId)
        {
            try
            {
                var _objgetall_AduditorDetails = (dynamic)null;
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (EntityId > 0)
                    {
                        _objgetall_AduditorDetails = (from row in entities.BM_Auditor_Master
                                                      join rows in entities.BM_StatutoryAuditor_Mapping
                                                      on row.Id equals rows.Auditor_ID into joined
                                                      from j in joined.DefaultIfEmpty().Where(k => k.EntityId != EntityId)
                                                      where row.IsActive == true && row.Category_Id == criteriaId && row.Audit_Type == type
                                                      && row.CustomerId == CustomerId

                                                      select new VMAuditorforDropdown
                                                      {
                                                          Id = row.Id,
                                                          Name = row.FullName_FirmName

                                                      }).Distinct().ToList();
                    }
                    else if (EntityId == 0)
                    {
                        _objgetall_AduditorDetails = (from row in entities.BM_Auditor_Master
                                                          //join rows in entities.BM_StatutoryAuditor_Mapping
                                                          //on row.Id equals rows.Auditor_ID into joined
                                                          //from j in joined.DefaultIfEmpty().Where(k => k.EntityId != EntityId)
                                                      where row.IsActive == true && row.Category_Id == criteriaId && row.Audit_Type == type
                                                      && row.CustomerId == CustomerId

                                                      select new VMAuditorforDropdown
                                                      {
                                                          Id = row.Id,
                                                          Name = row.FullName_FirmName

                                                      }).Distinct().ToList();
                    }
                    return _objgetall_AduditorDetails;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<VMStatutory_Auditor> GetStatutoryAuditor(int customerId, long EntityId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {

                    var getStatutotyAuditor = (from row in entities.BM_SP_StatutoryAuditorDetails(EntityId)

                                               select new VMStatutory_Auditor
                                               {
                                                   Id = row.Id,
                                                   AuditorMappingId = row.MappingId,
                                                   EntityId = (int)EntityId,
                                                   Nature_of_Appointment = row.Nature_of_Appointment,
                                                   AuditorId = row.Auditor_ID,
                                                   appointed_FromDate = row.Appointed_FromDate,
                                                   appointed_ToDate = row.Appointed_DueDate,
                                                   jointAuditor = row.IsJoin_Auditor_Appointed == null ? "NO" : row.IsJoin_Auditor_Appointed == true ? "YES" : "No",
                                                   AudtitorCriteria = row.Category,
                                                   AuditorNames = row.FullName_FirmName,
                                                   IsMappingActive = row.IsMappingActive
                                               }).ToList();
                    return getStatutotyAuditor;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMStatutory_Auditor AddStatutoryAuditor(VMStatutory_Auditor _objstatutory_auditor)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    int CountAuditor = 0;
                    //var _objcheckexist = (dynamic)null;
                    if (_objstatutory_auditor.AuditDeatilsList.Count > 0)
                    {
                        var duplicates = _objstatutory_auditor.AuditDeatilsList
                        .GroupBy(i => i.Auditor_Id)
                        .Where(g => g.Count() > 1)
                        .Select(g => g.Key).ToList();
                        if (duplicates.Count() == 0)
                        {

                            //_objcheckexist = (from row in entities.BM_StatutoryAuditor
                            //                      join rows in entities.BM_StatutoryAuditor_Mapping
                            //                      where row.IsActive == true && row.CustomerId == _objstatutory_auditor.CustomerId
                            //                      && row.EntityId == _objstatutory_auditor.EntityId && row.Nature_of_Appointment == _objstatutory_auditor.Nature_of_Appointment
                            //                      //&& row.Date_AGM == _objstatutory_auditor.Date_AGM
                            //                      select row).FirstOrDefault();
                            //if (_objcheckexist == null)
                            //{
                            if (_objstatutory_auditor.AuditDeatilsList != null)
                            {
                                if (_objstatutory_auditor.AuditDeatilsList.Count > 0)
                                {
                                    foreach (var au in _objstatutory_auditor.AuditDeatilsList)
                                    {
                                        var checkAuditor = (from auditorsmapping in entities.BM_StatutoryAuditor_Mapping
                                                            join auditor in entities.BM_StatutoryAuditor on auditorsmapping.Statutory_AuditorId equals auditor.Id
                                                            where auditorsmapping.Auditor_ID == au.Auditor_Id && auditor.EntityId == _objstatutory_auditor.EntityId
                                                            && auditorsmapping.IsActive == true
                                                            && auditorsmapping.IsDeleted==false
                                                          //  && auditor.CustomerId==au.cu
                                                            && auditor.IsDeleted==false
                                                            && auditor.IsActive==true
                                                            select auditorsmapping).FirstOrDefault();
                                        if (checkAuditor != null)
                                        {
                                            CountAuditor++;
                                        }

                                    }
                                }
                            }
                            if (CountAuditor == 0)
                            {

                                BM_StatutoryAuditor objstatutoryaudito = new BM_StatutoryAuditor();
                                objstatutoryaudito.EntityId = _objstatutory_auditor.EntityId;
                                objstatutoryaudito.SRN = _objstatutory_auditor.SRN;
                                objstatutoryaudito.CustomerId = _objstatutory_auditor.CustomerId;
                                objstatutoryaudito.Class_of_Company_sec139 = _objstatutory_auditor.Class_of_Company_sec139;
                                objstatutoryaudito.Nature_of_Appointment = _objstatutory_auditor.Nature_of_Appointment;
                                objstatutoryaudito.IsJoin_Auditor_Appointed = _objstatutory_auditor.IsJoin_Auditor_Appointed;
                                objstatutoryaudito.Auditor_Number = _objstatutory_auditor.Auditor_Number;
                                objstatutoryaudito.Auditor_AGM = _objstatutory_auditor.Auditor_AGM;
                                if (_objstatutory_auditor.Auditor_AGM)//Check Whether auditor(s) has been appointed in the annual general meeting (AGM): is true
                                    objstatutoryaudito.Date_AGM = _objstatutory_auditor.Date_AGM;

                                objstatutoryaudito.Date_Appointment = (DateTime)_objstatutory_auditor.Date_Appointment;


                                if (_objstatutory_auditor.Nature_of_Appointment == 4)//Whether auditor is appointed due to casual vacancy in the office of auditor :is true
                                {
                                    objstatutoryaudito.SRN_relevent_form = _objstatutory_auditor.SRN_relevent_form;
                                    objstatutoryaudito.Person_vacated_office = _objstatutory_auditor.Person_vacated_office;
                                    objstatutoryaudito.Membership_Number_or_Registrationfirm = _objstatutory_auditor.Membership_Number_or_Registrationfirm;
                                    objstatutoryaudito.Date_vacancy = _objstatutory_auditor.Date_vacancy;
                                    objstatutoryaudito.Reasons_casual_vacancy = _objstatutory_auditor.Reasons_casual_vacancy;
                                    objstatutoryaudito.IsAuditor_Appointed_casualvacancy = _objstatutory_auditor.IsAuditor_Appointed_casualvacancy;
                                }
                                else
                                {
                                    objstatutoryaudito.IsAuditor_Appointed_casualvacancy = false;
                                }
                                objstatutoryaudito.Flag = "SA";

                                if (_objstatutory_auditor.MeetingAgengaMappingID > 0)
                                {

                                }
                                else
                                {
                                    objstatutoryaudito.IsActive = true;
                                }
                                objstatutoryaudito.Createdon = DateTime.Now;

                                objstatutoryaudito.AGMNo_TillAppointmentTerminate = _objstatutory_auditor.AGMNo_TillAppointmentTerminate;
                                objstatutoryaudito.IsDeleted = false;
                                entities.BM_StatutoryAuditor.Add(objstatutoryaudito);
                                entities.SaveChanges();

                                _objstatutory_auditor.Id = objstatutoryaudito.Id;

                                if (objstatutoryaudito.Id > 0)
                                {
                                    BM_StatutoryAuditor_Mapping obj = new BM_StatutoryAuditor_Mapping();
                                    if (_objstatutory_auditor.AuditDeatilsList.Count > 0)
                                    {
                                        foreach (var item in _objstatutory_auditor.AuditDeatilsList)
                                        {
                                            var _objChecke_Auditordtls = (from x in entities.BM_StatutoryAuditor_Mapping where x.Auditor_ID == item.Auditor_Id && x.Statutory_AuditorId == objstatutoryaudito.Id && x.IsActive == true && x.IsDeleted==false  select x).FirstOrDefault();
                                            if (_objChecke_Auditordtls == null)
                                            {
                                                BM_StatutoryAuditor_Mapping objStatutoryAuditor_Mapping = new BM_StatutoryAuditor_Mapping();
                                                objStatutoryAuditor_Mapping.CategoryId = item.Auditor_CriteriaId;
                                                objStatutoryAuditor_Mapping.Auditor_ID = item.Auditor_Id;
                                                objStatutoryAuditor_Mapping.Appointed_FromDate = item.AppointedFromdate;
                                                objStatutoryAuditor_Mapping.Appointed_DueDate = item.dueDate;
                                                objStatutoryAuditor_Mapping.Appointed_No_of_FY = (int)item.Number_of_appointedFY;
                                                objStatutoryAuditor_Mapping.StartFY = item.AppointedFirstFYDate;
                                                objStatutoryAuditor_Mapping.EndFY = item.AppointedLastFYDate;
                                                objStatutoryAuditor_Mapping.IsActive = true;
                                                objStatutoryAuditor_Mapping.IsDeleted = false;
                                                objStatutoryAuditor_Mapping.flag = "SA";
                                                objStatutoryAuditor_Mapping.EntityId = _objstatutory_auditor.EntityId;
                                                objStatutoryAuditor_Mapping.Statutory_AuditorId = (int)objstatutoryaudito.Id;
                                                entities.BM_StatutoryAuditor_Mapping.Add(objStatutoryAuditor_Mapping);
                                                entities.SaveChanges();
                                            }
                                            else
                                            {
                                                _objChecke_Auditordtls.CategoryId = item.Auditor_CriteriaId;
                                                _objChecke_Auditordtls.Auditor_ID = item.Auditor_Id;
                                                _objChecke_Auditordtls.Appointed_FromDate = item.AppointedFromdate;
                                                _objChecke_Auditordtls.Appointed_DueDate = item.dueDate;
                                                _objChecke_Auditordtls.Appointed_No_of_FY = (int)item.Number_of_appointedFY;
                                                _objChecke_Auditordtls.StartFY = item.AppointedFirstFYDate;
                                                _objChecke_Auditordtls.EndFY = item.AppointedLastFYDate;
                                                _objChecke_Auditordtls.IsActive = true;
                                                _objChecke_Auditordtls.IsDeleted = false;
                                                _objChecke_Auditordtls.flag = "SA";
                                                _objChecke_Auditordtls.EntityId = _objstatutory_auditor.EntityId;
                                                _objChecke_Auditordtls.Statutory_AuditorId = (int)objstatutoryaudito.Id;
                                                entities.SaveChanges();

                                            }
                                        }
                                    }
                                }
                                _objstatutory_auditor.successMessage = true;
                                _objstatutory_auditor.showMessage = "Statutory auditor save successfully";

                            }
                            else
                            {
                                _objstatutory_auditor.errorMessage = true;
                                _objstatutory_auditor.showMessage = "Statutory auditor data already exist";

                            }
                        }
                        else
                        {
                            _objstatutory_auditor.errorMessage = true;
                            _objstatutory_auditor.showMessage = "Select Individual Statutory Auditor";

                        }

                    }
                    else
                    {
                        _objstatutory_auditor.errorMessage = true;
                        _objstatutory_auditor.showMessage = "Auditor/Auditor Firm can't be duplicate";

                    }
                    //}
                    //else
                    //{
                    //    _objstatutory_auditor.errorMessage = true;
                    //    _objstatutory_auditor.showMessage = "Please select Auditor/Auditor Firm can't";
                    //}
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objstatutory_auditor.errorMessage = true;
                _objstatutory_auditor.showMessage = "Server error occurred";

            }
            return _objstatutory_auditor;
        }

        public VMStatutory_Auditor UpdateStatutoryAuditor(VMStatutory_Auditor _objstatutory_auditor)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    int CountAuditor = 0;

                    if (_objstatutory_auditor.AuditDeatilsList.Count > 0)
                    {
                        var duplicates = _objstatutory_auditor.AuditDeatilsList
                        .GroupBy(i => i.Auditor_Id)
                        .Where(g => g.Count() > 1)
                        .Select(g => g.Key).ToList();
                        if (duplicates.Count() == 0)
                        {
                            var _objcheckexist = (from row in entities.BM_StatutoryAuditor
                                                  where //row.IsActive == true && 
                                                  row.Id == _objstatutory_auditor.Id &&
                                                  row.CustomerId == _objstatutory_auditor.CustomerId &&
                                                  row.Flag == "SA"
                                                  select row).FirstOrDefault();

                            if (_objcheckexist != null)
                            {
                                if (_objstatutory_auditor.AuditDeatilsList != null)
                                {
                                    if (_objstatutory_auditor.AuditDeatilsList.Count > 0)
                                    {
                                        foreach (var au in _objstatutory_auditor.AuditDeatilsList)
                                        {
                                            var checkAuditor = (from auditorsmapping in entities.BM_StatutoryAuditor_Mapping
                                                                join auditor in entities.BM_StatutoryAuditor on auditorsmapping.Statutory_AuditorId equals auditor.Id
                                                                where auditorsmapping.Auditor_ID == au.Auditor_Id && auditor.EntityId == _objstatutory_auditor.EntityId
                                                                && auditorsmapping.IsActive == true && auditorsmapping.Id != au.Id
                                                                select auditorsmapping).FirstOrDefault();
                                            if (checkAuditor != null)
                                            {
                                                CountAuditor++;
                                            }

                                        }
                                    }
                                }
                                if (CountAuditor == 0)
                                {
                                    _objcheckexist.EntityId = _objstatutory_auditor.EntityId;
                                    _objcheckexist.CustomerId = _objstatutory_auditor.CustomerId;
                                    _objcheckexist.SRN = _objstatutory_auditor.SRN;
                                    _objcheckexist.Class_of_Company_sec139 = _objstatutory_auditor.Class_of_Company_sec139;
                                    _objcheckexist.Nature_of_Appointment = _objstatutory_auditor.Nature_of_Appointment;
                                    _objcheckexist.IsJoin_Auditor_Appointed = _objstatutory_auditor.IsJoin_Auditor_Appointed;
                                    _objcheckexist.Auditor_Number = _objstatutory_auditor.Auditor_Number;
                                    _objcheckexist.Auditor_AGM = _objstatutory_auditor.Auditor_AGM;
                                    _objcheckexist.Date_AGM = _objstatutory_auditor.Date_AGM;
                                    _objcheckexist.Date_Appointment = (DateTime)_objstatutory_auditor.Date_Appointment;
                                    _objcheckexist.IsAuditor_Appointed_casualvacancy = _objstatutory_auditor.IsAuditor_Appointed_casualvacancy;
                                    _objcheckexist.SRN_relevent_form = _objstatutory_auditor.SRN_relevent_form;
                                    _objcheckexist.Person_vacated_office = _objstatutory_auditor.Person_vacated_office;
                                    _objcheckexist.Membership_Number_or_Registrationfirm = _objstatutory_auditor.Membership_Number_or_Registrationfirm;
                                    _objcheckexist.Date_vacancy = _objstatutory_auditor.Date_vacancy;
                                    _objcheckexist.Reasons_casual_vacancy = _objstatutory_auditor.Reasons_casual_vacancy;
                                    _objcheckexist.IsAuditor_Appointed_casualvacancy = _objstatutory_auditor.IsAuditor_Appointed_casualvacancy;
                                    
                                    if (_objstatutory_auditor.MeetingAgengaMappingID > 0)
                                    {

                                    }
                                    else
                                    {
                                        _objcheckexist.IsActive = true;
                                    }
                                    _objcheckexist.Createdon = DateTime.Now;
                                    _objcheckexist.Flag = "SA";
                                    _objcheckexist.AGMNo_TillAppointmentTerminate = _objstatutory_auditor.AGMNo_TillAppointmentTerminate;
                                    entities.SaveChanges();

                                    if (_objcheckexist.Id > 0)
                                    {
                                        var getdataAuditor = (from row in entities.BM_StatutoryAuditor_Mapping where row.Statutory_AuditorId == _objcheckexist.Id select row).ToList();
                                        if (getdataAuditor.Count > 0)
                                        {
                                            foreach (var item in getdataAuditor)
                                            {
                                                item.IsActive = false;
                                                entities.SaveChanges();
                                            }
                                        }
                                        if (_objstatutory_auditor.AuditDeatilsList.Count > 0)
                                        {
                                            foreach (var ites in _objstatutory_auditor.AuditDeatilsList)
                                            {
                                                var getauditid = (from row in entities.BM_StatutoryAuditor_Mapping where row.Statutory_AuditorId == _objcheckexist.Id && row.Id == ites.Id select row).FirstOrDefault();
                                                if (getauditid != null)
                                                {
                                                    getauditid.CategoryId = ites.Auditor_CriteriaId;
                                                    getauditid.Auditor_ID = ites.Auditor_Id;
                                                    getauditid.Appointed_FromDate = ites.AppointedFromdate;
                                                    getauditid.Appointed_DueDate = ites.dueDate;
                                                    getauditid.Appointed_No_of_FY = (int)ites.Number_of_appointedFY;
                                                    getauditid.StartFY = ites.AppointedFirstFYDate;
                                                    getauditid.EndFY = ites.AppointedLastFYDate;
                                                    getauditid.Statutory_AuditorId = (int)_objcheckexist.Id;
                                                    getauditid.IsActive = true;
                                                    getauditid.IsDeleted = false;
                                                    getauditid.flag = "SA";
                                                    getauditid.EntityId = _objstatutory_auditor.EntityId;
                                                    entities.SaveChanges();
                                                }
                                                else
                                                {
                                                    BM_StatutoryAuditor_Mapping obj = new BM_StatutoryAuditor_Mapping();
                                                    obj.CategoryId = ites.Auditor_CriteriaId;
                                                    obj.Auditor_ID = ites.Auditor_Id;
                                                    obj.Appointed_FromDate = ites.AppointedFromdate;
                                                    obj.Appointed_DueDate = ites.dueDate;
                                                    obj.Appointed_No_of_FY = (int)ites.Number_of_appointedFY;
                                                    obj.StartFY = ites.AppointedFirstFYDate;
                                                    obj.EndFY = ites.AppointedLastFYDate;
                                                    obj.Statutory_AuditorId = (int)_objcheckexist.Id;
                                                    obj.IsActive = true;
                                                    obj.IsDeleted = false;
                                                    obj.flag = "SA";
                                                    obj.EntityId = _objstatutory_auditor.EntityId;
                                                    entities.BM_StatutoryAuditor_Mapping.Add(obj);
                                                    entities.SaveChanges();
                                                }
                                            }
                                        }
                                    }

                                    _objstatutory_auditor.successMessage = true;
                                    _objstatutory_auditor.showMessage = "Statutory auditor update successfully";

                                }
                                else
                                {
                                    _objstatutory_auditor.errorMessage = true;
                                    _objstatutory_auditor.showMessage = "Statutory auditor already exist";

                                }
                            }
                        }
                        else
                        {
                            _objstatutory_auditor.errorMessage = true;
                            _objstatutory_auditor.showMessage = "Duplicate value Found";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objstatutory_auditor.errorMessage = true;
                _objstatutory_auditor.showMessage = "Server error occurred";

            }
            return _objstatutory_auditor;
        }

        public VMStatutory_Auditor GetStatutorybyId(int id, int customer_Id)
        {
            List<VMAuditorDetails> objlist = new List<VMAuditorDetails>();
            try
            {
                if (id > 0)
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        var getstatutary_Details = (from row in entities.BM_StatutoryAuditor
                                                    where row.Id == id && row.CustomerId == customer_Id
                                                    //&& row.IsActive == false 
                                                    && row.Flag == "SA"
                                                    select new VMStatutory_Auditor
                                                    {
                                                        Id = row.Id,
                                                        Class_of_Company_sec139 = row.Class_of_Company_sec139,
                                                        Nature_of_Appointment = row.Nature_of_Appointment,
                                                        IsJoin_Auditor_Appointed = row.IsJoin_Auditor_Appointed,
                                                        Auditor_Number = row.Auditor_Number,
                                                        Auditor_AGM = (bool)row.Auditor_AGM,
                                                        //AuditorId = entities.BM_StatutoryAuditor_Mapping.Where(x => x.Statutory_AuditorId == id && x.IsActive == true).Select(x => x.Auditor_ID).ToList(),
                                                        Date_AGM = row.Date_AGM,
                                                        Date_Appointment = row.Date_Appointment,
                                                        IsAuditor_Appointed_casualvacancy = (bool)row.IsAuditor_Appointed_casualvacancy,
                                                        SRN_relevent_form = row.SRN_relevent_form,
                                                        Person_vacated_office = row.Person_vacated_office,
                                                        Membership_Number_or_Registrationfirm = row.Membership_Number_or_Registrationfirm,
                                                        Date_vacancy = row.Date_vacancy,
                                                        Reasons_casual_vacancy = row.Reasons_casual_vacancy,
                                                        SRN=row.SRN,
                                                        AGMNo_TillAppointmentTerminate = row.AGMNo_TillAppointmentTerminate

                                                    }).FirstOrDefault();
                        if (getstatutary_Details != null)
                        {
                            getstatutary_Details.AuditDeatilsList = (from x in entities.BM_StatutoryAuditor_Mapping
                                                                     where x.Statutory_AuditorId == id && //x.IsActive == true && 
                                                                     x.flag == "SA"
                                                                     select new VMAuditorDetails

                                                                     {

                                                                         Auditor_CriteriaId = x.CategoryId,
                                                                         Auditor_Id = x.Auditor_ID,
                                                                         Id = x.Id,
                                                                         AppointedFromdate = x.Appointed_FromDate,
                                                                         dueDate = x.Appointed_DueDate,
                                                                         AppointedFirstFYDate = x.StartFY,
                                                                         AppointedLastFYDate = x.EndFY,
                                                                         Number_of_appointedFY = x.Appointed_No_of_FY,

                                                                     }).ToList();
                        }
                        else
                        {
                            getstatutary_Details = new VMStatutory_Auditor();
                            getstatutary_Details.errorMessage = true;
                            getstatutary_Details.showMessage = "Something went wrong";
                        }
                        return getstatutary_Details;
                    }
                }
                else
                {
                    VMStatutory_Auditor obj = new VMStatutory_Auditor();
                    obj.errorMessage = true;
                    obj.showMessage = "Something went wrong";
                    return obj;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                VMStatutory_Auditor obj = new VMStatutory_Auditor();
                obj.errorMessage = true;
                obj.showMessage = "Server error occurred";
                return obj;
            }
        }

        public IEnumerable<BM_AuditorType> GetDropDownAuditors_Type()
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getListofAuditorType = (from row in entities.BM_AuditorType
                                                where row.IsActive == true
                                                select row).ToList();
                    return getListofAuditorType;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion

        #region Internal Auditor

        public VMInternalAuditor AddInternalAuditor(VMInternalAuditor _objinternal_auditor)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {

                    BM_StatutoryAuditor objstatutory = new BM_StatutoryAuditor();
                    BM_StatutoryAuditor_Mapping _objstatutorymapping = new BM_StatutoryAuditor_Mapping();
                    var checkInternalAuditorExist = (from row in entities.BM_StatutoryAuditor
                                                     join rows in entities.BM_StatutoryAuditor_Mapping
                                                     on row.Id equals rows.Statutory_AuditorId
                                                     where row.EntityId == _objinternal_auditor.EntityId
                                                     && rows.Auditor_ID == _objinternal_auditor.Auditor_Id
                                                      && row.Flag == "IA" && rows.flag == "IA"
                                                      && rows.IsActive == true
                                                     select new { row, rows }).FirstOrDefault();

                    if (checkInternalAuditorExist == null)
                    {
                        objstatutory.EntityId = _objinternal_auditor.EntityId;
                        objstatutory.Date_Appointment = _objinternal_auditor.Date_of_appointment;
                        objstatutory.Createdby = _objinternal_auditor.UserId;
                        objstatutory.Createdon = DateTime.Now;
                        objstatutory.Flag = "IA";
                        objstatutory.IsActive = true;
                        objstatutory.IsDeleted = false;
                        objstatutory.CustomerId = _objinternal_auditor.CustomerId;
                        entities.BM_StatutoryAuditor.Add(objstatutory);
                        entities.SaveChanges();
                        if (objstatutory.Id > 0)
                        {
                            _objstatutorymapping.EntityId = _objinternal_auditor.EntityId;
                            _objstatutorymapping.Auditor_ID = _objinternal_auditor.Auditor_Id;
                            _objstatutorymapping.Statutory_AuditorId = (int)objstatutory.Id;
                            _objstatutorymapping.IsEmployee = _objinternal_auditor.Isemployee;
                            if (_objinternal_auditor.Isemployee == "Y")
                                _objstatutorymapping.EmployeeNo = _objinternal_auditor.EmployeeNo;
                            _objstatutorymapping.CategoryId = _objinternal_auditor.InternalAuditor_CategoryId;
                            _objstatutorymapping.flag = "IA";
                            _objstatutorymapping.IsDeleted = false;
                            if(_objinternal_auditor.MeetingAgengaMappingID > 0)
                            {

                            }
                            else
                            {
                                _objstatutorymapping.IsActive = true;
                            }
                            

                            entities.BM_StatutoryAuditor_Mapping.Add(_objstatutorymapping);
                            entities.SaveChanges();
                        }
                        if (objstatutory.Id > 0 && _objstatutorymapping.Id > 0)
                        {
                            _objinternal_auditor.successMessage = true;
                            _objinternal_auditor.successerrorMessage = "Internal Auditor save successfully";
                            _objinternal_auditor.Id = objstatutory.Id;
                            return _objinternal_auditor;
                        }
                        else
                        {
                            _objinternal_auditor.errorMessage = true;
                            _objinternal_auditor.successerrorMessage = "Something went wrong";
                            return _objinternal_auditor;
                        }
                    }
                    else
                    {
                        _objinternal_auditor.errorMessage = true;
                        _objinternal_auditor.successerrorMessage = "Internal Auditor already exist";
                        return _objinternal_auditor;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objinternal_auditor.errorMessage = true;
                _objinternal_auditor.successerrorMessage = "Server error occurred";
                return _objinternal_auditor;
            }
        }

        public VMInternalAuditor UpdateInternalAuditor(VMInternalAuditor _objinternal_auditor)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var checkAuditor = (from A in entities.BM_StatutoryAuditor_Mapping where A.Auditor_ID == _objinternal_auditor.Auditor_Id && A.EntityId == _objinternal_auditor.EntityId && A.flag == "IA" && A.Id != _objinternal_auditor.AuditorMappingId && A.IsDeleted == false && A.IsActive == true select A).ToList();
                    if (checkAuditor.Count == 0)
                    {
                        var checkInternalAuditorExist = (from row in entities.BM_StatutoryAuditor
                                                         join rows in entities.BM_StatutoryAuditor_Mapping
                                                         on row.Id equals rows.Statutory_AuditorId
                                                         where row.EntityId == _objinternal_auditor.EntityId
                                                         && rows.Auditor_ID == _objinternal_auditor.Auditor_Id
                                                         && row.Flag == "IA" && rows.flag == "IA"
                                                         && row.Id == _objinternal_auditor.Id
                                                         && row.IsDeleted==false && row.IsDeleted==false
                                                         && rows.IsActive==true && row.IsActive==true
                                                         select new { row, rows }).FirstOrDefault();
                        if (checkInternalAuditorExist != null)
                        {
                            checkInternalAuditorExist.row.EntityId = _objinternal_auditor.EntityId;
                            checkInternalAuditorExist.row.Date_Appointment = _objinternal_auditor.Date_of_appointment;
                            checkInternalAuditorExist.row.Createdby = _objinternal_auditor.UserId;
                            checkInternalAuditorExist.row.Createdon = DateTime.Now;
                            checkInternalAuditorExist.row.Flag = "IA";
                        checkInternalAuditorExist.row.IsActive = true;
                        checkInternalAuditorExist.row.IsDeleted = false;

                            entities.SaveChanges();
                            if (checkInternalAuditorExist.row.Id > 0)
                            {
                                checkInternalAuditorExist.rows.EntityId = _objinternal_auditor.EntityId;
                                checkInternalAuditorExist.rows.Auditor_ID = _objinternal_auditor.Auditor_Id;
                                //checkInternalAuditorExist.rows.Statutory_AuditorId = (int)objstatutory.Id;
                                checkInternalAuditorExist.rows.IsEmployee = _objinternal_auditor.Isemployee;
                                if (_objinternal_auditor.Isemployee == "Y")
                                    checkInternalAuditorExist.rows.EmployeeNo = _objinternal_auditor.EmployeeNo;
                                checkInternalAuditorExist.rows.CategoryId = _objinternal_auditor.InternalAuditor_CategoryId;
                                checkInternalAuditorExist.rows.flag = "IA";
                            checkInternalAuditorExist.rows.IsActive = true;
                            checkInternalAuditorExist.rows.IsDeleted = false;

                            entities.SaveChanges();
                            }
                            if (checkInternalAuditorExist.rows.Id > 0 && checkInternalAuditorExist.row.Id > 0)
                            {
                                _objinternal_auditor.successMessage = true;
                                _objinternal_auditor.successerrorMessage = "Internal Auditor update successfully";
                                return _objinternal_auditor;
                            }
                            else
                            {
                                _objinternal_auditor.errorMessage = true;
                                _objinternal_auditor.successerrorMessage = "Something went wrong";
                                return _objinternal_auditor;
                            }
                        }
                        else
                        {
                            _objinternal_auditor.errorMessage = true;
                            _objinternal_auditor.successerrorMessage = "Internal Auditor Data not found";
                            return _objinternal_auditor;
                        }
                }
                    else
                    {
                    _objinternal_auditor.errorMessage = true;
                    _objinternal_auditor.successerrorMessage = "Internal Auditor already exist";
                    return _objinternal_auditor;
                }
            }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objinternal_auditor.errorMessage = true;
                _objinternal_auditor.successerrorMessage = "Server error occurred";
                return _objinternal_auditor;
            }
        }

        public string getAuditorName(int auditorId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getAuditorName = (from row in entities.BM_Auditor_Master where row.Id == auditorId && row.IsActive==true select row.FullName_FirmName).FirstOrDefault();
                    return getAuditorName;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public VMInternalAuditor GetInternalAuditorbyId(long id, int entityId)
        {

            try
            {
                VMInternalAuditor CheckAuditobyId = new VMInternalAuditor();
                if (id > 0)
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        CheckAuditobyId = (from row in entities.BM_StatutoryAuditor
                                           join rows in entities.BM_StatutoryAuditor_Mapping
                                           on row.Id equals rows.Statutory_AuditorId
                                           where row.Id == id && row.EntityId == entityId && row.IsDeleted == false
                                           select new VMInternalAuditor
                                           {
                                               Id = row.Id,
                                               AuditorMappingId = rows.Id,
                                               Auditor_Id = rows.Auditor_ID,
                                               EmployeeNo = rows.EmployeeNo,
                                               EntityId = row.EntityId,
                                               Isemployee = rows.IsEmployee,
                                               Date_of_appointment = row.Date_Appointment,
                                               InternalAuditor_CategoryId = rows.CategoryId
                                           }).FirstOrDefault();

                    }

                }
                return CheckAuditobyId;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                VMInternalAuditor obj = new VMInternalAuditor();
                obj.errorMessage = true;
                obj.successerrorMessage = "Server error occured";
                return obj;
            }
        }

        public List<VMInternalAuditor> GetInternalAuditor(int customerId, long EntityId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getInternalAuditor = (from x in entities.BM_StatutoryAuditor
                                              join y in entities.BM_StatutoryAuditor_Mapping
                                              on x.Id equals y.Statutory_AuditorId
                                              where x.IsActive == true && x.IsDeleted == false && x.Flag == "IA" && x.EntityId == EntityId
                                              select new VMInternalAuditor
                                              {
                                                  Id = x.Id,
                                                  Auditor_Id = y.Auditor_ID,
                                                  AuditorMappingId = y.Id,
                                                  EntityId = x.EntityId,
                                                  AuditorName = (from x in entities.BM_Auditor_Master where x.Id == y.Auditor_ID && x.IsActive==true && x.Audit_Type == 2 select x.FullName_FirmName).FirstOrDefault(),
                                                  InternalAuditor_Category = y.CategoryId == 1 ? "Individual" : "Firm",
                                                  Date_of_appointment = x.Date_Appointment,
                                                  EmployeeNo = y.EmployeeNo,
                                                  Isemployee = y.IsEmployee == "Y" ? "YES" : "NO",
                                                  IsMappingActive = y.IsActive
                                              }).ToList();
                    return getInternalAuditor;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        #endregion

        #region Secreterial Auditor
        public VM_SecreterialAuditor AddSecreterialDate(VM_SecreterialAuditor _objsecreterial)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var objcheckData = (from row in entities.BM_StatutoryAuditor
                                        join y in entities.BM_StatutoryAuditor_Mapping
                                        on row.Id equals y.Statutory_AuditorId
                                        where row.IsDeleted == false && row.IsActive == true
                                        && y.IsActive==true && y.IsDeleted==false
                                         && row.CustomerId == _objsecreterial.CustomerId && row.Flag == "SCA" && y.flag == "SCA"
                                         && y.Auditor_ID == _objsecreterial.AuditorID
                                         && row.EntityId == _objsecreterial.EntityId
                                        select row).FirstOrDefault();
                    if (_objsecreterial.Period_of_AppointmentFromDate < _objsecreterial.Period_of_AppointmentToDate)
                    {
                        if (objcheckData == null)
                        {
                            BM_StatutoryAuditor _objsecretial = new BM_StatutoryAuditor();
                            _objsecretial.EntityId = _objsecreterial.EntityId;
                            _objsecretial.CustomerId = _objsecreterial.CustomerId;
                            _objsecretial.Flag = "SCA";
                            _objsecretial.Certificate_of_Practice = _objsecreterial.Certificate_of_Practice;
                            _objsecretial.Date_Appointment = _objsecreterial.Date_of_appointment;
                            _objsecretial.IsActive = true;
                            _objsecretial.IsDeleted = false;
                            _objsecretial.Createdon = DateTime.Now;
                            _objsecretial.Auditor_AGM = false;
                            _objsecretial.IsAuditor_Appointed_casualvacancy = false;
                            entities.BM_StatutoryAuditor.Add(_objsecretial);
                            entities.SaveChanges();
                            if (_objsecretial.Id > 0)
                            {
                                BM_StatutoryAuditor_Mapping obj = new BM_StatutoryAuditor_Mapping();
                                obj.EntityId = _objsecreterial.EntityId;
                                obj.Statutory_AuditorId = (int)_objsecretial.Id;
                                obj.Auditor_ID = _objsecreterial.AuditorID;
                                obj.CategoryId = _objsecreterial.Category_of_SecreterialAuditor;
                                obj.Appointed_FromDate = _objsecreterial.Period_of_AppointmentFromDate;
                                obj.Appointed_DueDate = _objsecreterial.Period_of_AppointmentToDate;
                                obj.flag = "SCA";
                                if(_objsecreterial.MeetingAgengaMappingID > 0)
                                {

                                }
                                else
                                {
                                    obj.IsActive = true;
                                }
                                
                                obj.IsDeleted = false;
                                entities.BM_StatutoryAuditor_Mapping.Add(obj);
                                entities.SaveChanges();
                            }
                            _objsecreterial.successMessage = true;
                            _objsecreterial.successerrorMessage = "Saved Successfully.";
                            _objsecreterial.Id = _objsecretial.Id;
                            return _objsecreterial;
                        }
                        else
                        {
                            _objsecreterial.errorMessage = true;
                            _objsecreterial.successerrorMessage = "Secretarial Auditor/Auditor Firm already exist";
                            return _objsecreterial;
                        }
                    }
                    else
                    {
                        _objsecreterial.errorMessage = true;
                        _objsecreterial.successerrorMessage = "Secretarial Auditor/Auditor Firm appointment Date is not greater then appointment to Date";
                        return _objsecreterial;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objsecreterial.errorMessage = true;
                _objsecreterial.successerrorMessage = "Server error occured";
                return _objsecreterial;
            }
        }

        public VM_SecreterialAuditor updateSecreterialDate(VM_SecreterialAuditor _objsecreterial)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var CheckAuditor = (from row in entities.BM_StatutoryAuditor_Mapping where row.Auditor_ID == _objsecreterial.AuditorID && row.flag == "SCA" && row.EntityId == _objsecreterial.EntityId && row.Id != _objsecreterial.AuditorMappingId && row.IsActive == true && row.IsDeleted == false select row).ToList();
                    if (CheckAuditor.Count == 0)
                    {
                        var objcheckData = (from row in entities.BM_StatutoryAuditor
                                            join y in entities.BM_StatutoryAuditor_Mapping
                                            on row.Id equals y.Statutory_AuditorId
                                            where row.IsDeleted == false
                                            && row.IsActive==true && y.IsActive==true
                                           // && row.CustomerId== _objsecreterial.CustomerId
                                            && y.IsDeleted==false
                                            && y.Auditor_ID== _objsecreterial.AuditorID
                                            //&& row.Id == _objsecreterial.Id && row.IsActive == true
                                            && row.EntityId == _objsecreterial.EntityId && row.CustomerId == _objsecreterial.CustomerId && row.Flag == "SCA" && y.flag == "SCA"
                                            select new { row, y }).FirstOrDefault();
                        if (objcheckData != null)
                        {

                            objcheckData.row.EntityId = _objsecreterial.EntityId;
                            objcheckData.row.CustomerId = _objsecreterial.CustomerId;
                            objcheckData.row.Flag = "SCA";
                            objcheckData.row.Certificate_of_Practice = _objsecreterial.Certificate_of_Practice;
                            objcheckData.row.Date_Appointment = _objsecreterial.Date_of_appointment;
                            objcheckData.row.IsActive = true;
                            objcheckData.row.IsDeleted = false;
                            objcheckData.row.Createdon = DateTime.Now;

                            entities.SaveChanges();
                            if (objcheckData.row.Id > 0)
                            {
                                objcheckData.y.EntityId = _objsecreterial.EntityId;
                                objcheckData.y.Statutory_AuditorId = (int)objcheckData.row.Id;
                                objcheckData.y.Auditor_ID = _objsecreterial.AuditorID;
                                objcheckData.y.CategoryId = _objsecreterial.Category_of_SecreterialAuditor;
                                objcheckData.y.Appointed_FromDate = _objsecreterial.Period_of_AppointmentFromDate;
                                objcheckData.y.Appointed_DueDate = _objsecreterial.Period_of_AppointmentToDate;
                                objcheckData.y.flag = "SCA";
                                objcheckData.y.IsActive = true;
                                objcheckData.y.IsDeleted = false;
                                entities.SaveChanges();
                            }
                            _objsecreterial.successMessage = true;
                            _objsecreterial.successerrorMessage = "Updated Successfully.";
                            return _objsecreterial;

                        }
                        else
                        {
                            _objsecreterial.errorMessage = true;
                            _objsecreterial.successerrorMessage = "something went wrong";
                            return _objsecreterial;
                        }
                    }
                    else
                    {
                        _objsecreterial.errorMessage = true;
                        _objsecreterial.successerrorMessage = "Secreterial Auditor/Auditor Firm already exist";
                        return _objsecreterial;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objsecreterial.errorMessage = true;
                _objsecreterial.successerrorMessage = "Server error occured";
                return _objsecreterial;
            }
        }

        public VM_SecreterialAuditor getSecreterialAuditorbyId(long id, int entityId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var objcheckData = (from row in entities.BM_StatutoryAuditor
                                        join y in entities.BM_StatutoryAuditor_Mapping
                                       on row.Id equals y.Statutory_AuditorId
                                        where row.Id == id //&& row.IsActive == true  -- Commented due to appointment of auditor in board
                                        && row.IsDeleted == false
                                        && row.EntityId == entityId && row.Flag == "SCA"
                                        select new VM_SecreterialAuditor
                                        {
                                            Id = row.Id,
                                            AuditorMappingId = y.Id,
                                            EntityId = row.EntityId,
                                            AuditorID = y.Auditor_ID,
                                            Category_of_SecreterialAuditor = y.CategoryId,
                                            Period_of_AppointmentFromDate = y.Appointed_FromDate,
                                            Period_of_AppointmentToDate = y.Appointed_DueDate,
                                            Certificate_of_Practice = row.Certificate_of_Practice,
                                            Date_of_appointment = row.Date_Appointment,
                                        }).FirstOrDefault();
                    return objcheckData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<VM_SecreterialAuditor> getSecreterialAuditor(int customerId, long EntityId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var objcheckData = (from row in entities.BM_StatutoryAuditor
                                        join y in entities.BM_StatutoryAuditor_Mapping
                                        on row.Id equals y.Statutory_AuditorId
                                        where row.IsActive == true && row.EntityId == EntityId && row.IsDeleted == false && row.CustomerId == customerId && row.Flag == "SCA"

                                        select new VM_SecreterialAuditor
                                        {
                                            Id = row.Id,
                                            AuditorID = y.Auditor_ID,
                                            AuditorMappingId = y.Id,
                                            EntityId = row.EntityId,
                                            AuditorName = (from row in entities.BM_Auditor_Master where row.Id == y.Auditor_ID && row.IsActive == true select row.FullName_FirmName).FirstOrDefault(),
                                            CategoryName = y.CategoryId == 1 ? "Individual" : "Firm",
                                            Period_of_AppointmentFromDate = y.Appointed_FromDate,
                                            Period_of_AppointmentToDate = y.Appointed_DueDate,
                                            Certificate_of_Practice = row.Certificate_of_Practice,
                                            Date_of_appointment = row.Date_Appointment,
                                            IsMappingActive = y.IsActive
                                        }).ToList();
                    return objcheckData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion

        #region CostAuditor
        public VM_CostAuditor getCostAuditorbyId(long id, int entityId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var objcheckData = (from row in entities.BM_StatutoryAuditor
                                        join y in entities.BM_StatutoryAuditor_Mapping
                                       on row.Id equals y.Statutory_AuditorId
                                        where row.Id == id && row.IsActive == true && row.IsDeleted == false
                                        && row.EntityId == entityId && row.Flag == "CA"
                                        select new VM_CostAuditor
                                        {
                                            AuditorMappingId = y.Id,
                                            Id = row.Id,
                                            EntityId = row.EntityId,
                                            AuditorId = y.Auditor_ID,
                                            categoryId = y.CategoryId,
                                            Nature_of_intimation_cost = row.Nature_of_Intimatecost,
                                            Cost_auditor_firm_Original = row.CoustAuditor_FirmOrigine,
                                            dateofBordMeeting = row.BoardMeeting_date,
                                            ResulationNumber = row.Resulation_Number,
                                        }).FirstOrDefault();
                    return objcheckData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VM_CostAuditor updateCostDate(VM_CostAuditor _objcost)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var CheckAuditor = (from row in entities.BM_StatutoryAuditor_Mapping where row.Auditor_ID == _objcost.AuditorId && row.EntityId == _objcost.EntityId && row.flag == "CA" && row.Id != _objcost.AuditorMappingId && row.IsActive == true && row.IsDeleted == false select row).ToList();
                    if (CheckAuditor.Count == 0)
                    {
                        var objcheckData = (from row in entities.BM_StatutoryAuditor
                                            join y in entities.BM_StatutoryAuditor_Mapping
                                            on row.Id equals y.Statutory_AuditorId
                                            where row.Id == _objcost.Id && row.IsActive == true && row.IsDeleted == false && row.Id == _objcost.Id
                                            && row.EntityId == _objcost.EntityId && row.CustomerId == _objcost.CustomerId && row.Flag == "CA" && y.flag == "CA"
                                            select new { row, y }).FirstOrDefault();
                        if (objcheckData != null)
                        {
                            objcheckData.row.Nature_of_Intimatecost = _objcost.Nature_of_intimation_cost;
                            objcheckData.y.EntityId = _objcost.EntityId;
                            objcheckData.row.CoustAuditor_FirmOrigine = _objcost.Cost_auditor_firm_Original;
                            objcheckData.row.BoardMeeting_date = _objcost.dateofBordMeeting;
                            objcheckData.row.Resulation_Number = _objcost.ResulationNumber;
                            objcheckData.row.Flag = "CA";
                            objcheckData.row.IsActive = true;
                            objcheckData.row.IsDeleted = false;
                            objcheckData.row.Createdon = DateTime.Now;

                            entities.SaveChanges();
                            if (objcheckData.row.Id > 0)
                            {
                                var data = (from row in entities.BM_StatutoryAuditor_Mapping where row.Statutory_AuditorId == objcheckData.row.Id && row.IsDeleted == false && row.IsActive == true && row.CategoryId == _objcost.categoryId select row).FirstOrDefault();

                                if (data != null)
                                {
                                    data.EntityId = _objcost.EntityId;
                                    data.Auditor_ID = _objcost.AuditorId;
                                    data.CategoryId = _objcost.categoryId;
                                    data.Statutory_AuditorId = (int)objcheckData.row.Id;
                                    data.flag = "CA";
                                    data.IsActive = true;
                                    data.IsDeleted = false;
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    BM_StatutoryAuditor_Mapping objdata = new BM_StatutoryAuditor_Mapping();
                                    objdata.EntityId = _objcost.EntityId;
                                    objdata.Auditor_ID = _objcost.AuditorId;
                                    objdata.CategoryId = _objcost.categoryId;
                                    objdata.Statutory_AuditorId = (int)objcheckData.row.Id;
                                    objdata.flag = "CA";
                                    objdata.IsActive = true;
                                    objdata.IsDeleted = false;
                                    entities.BM_StatutoryAuditor_Mapping.Add(objdata);
                                    entities.SaveChanges();
                                }
                            }
                            _objcost.successMessage = true;
                            _objcost.SuccesserrorMessage = "Cost Auditor/Auditor Firm saved successfully.";
                            return _objcost;

                        }
                        else
                        {
                            _objcost.errorMessage = true;
                            _objcost.SuccesserrorMessage = "Data already exist";
                            return _objcost;
                        }
                    }
                    else
                    {
                        _objcost.errorMessage = true;
                        _objcost.SuccesserrorMessage = "Cost Auditor/Auditor Firm already exist";
                        return _objcost;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objcost.errorMessage = true;
                _objcost.SuccesserrorMessage = "Server error occured";
                return _objcost;
            }
        }

        public VM_CostAuditor AddCostDate(VM_CostAuditor _objcost)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var objcheckData = (from row in entities.BM_StatutoryAuditor
                                        join y in entities.BM_StatutoryAuditor_Mapping
                                        on row.Id equals y.Statutory_AuditorId
                                        where row.IsActive == true && row.IsDeleted == false && y.Auditor_ID == _objcost.AuditorId
                                        && y.IsDeleted==false
                                        && y.IsActive==true
                                        && row.EntityId == _objcost.EntityId && row.CustomerId == _objcost.CustomerId && row.Flag == "CA" && y.flag == "CA"
                                        select new { row, y }).FirstOrDefault();
                    if (objcheckData == null)
                    {
                        BM_StatutoryAuditor obj = new BM_StatutoryAuditor();
                        obj.CustomerId = _objcost.CustomerId;
                        obj.EntityId = _objcost.EntityId;
                        obj.Nature_of_Intimatecost = _objcost.Nature_of_intimation_cost;
                        obj.CoustAuditor_FirmOrigine = _objcost.Cost_auditor_firm_Original;
                        obj.BoardMeeting_date = _objcost.dateofBordMeeting;
                        obj.Resulation_Number = _objcost.ResulationNumber;
                        obj.Flag = "CA";
                        obj.IsActive = true;
                        obj.IsDeleted = false;
                        obj.Createdon = DateTime.Now;
                        obj.Auditor_AGM = false;
                        obj.IsAuditor_Appointed_casualvacancy = false;
                        entities.BM_StatutoryAuditor.Add(obj);
                        entities.SaveChanges();

                        _objcost.Id = obj.Id;
                        if (obj.Id > 0)
                        {
                            var data = (from row in entities.BM_StatutoryAuditor_Mapping where row.Statutory_AuditorId == obj.Id && row.Auditor_ID == _objcost.AuditorId && row.IsDeleted == false && row.IsActive == true && row.CategoryId == _objcost.categoryId select row).FirstOrDefault();

                            if (data == null)
                            {
                                BM_StatutoryAuditor_Mapping objmapping = new BM_StatutoryAuditor_Mapping();
                                objmapping.EntityId = _objcost.EntityId;
                                objmapping.Auditor_ID = _objcost.AuditorId;
                                objmapping.CategoryId = _objcost.categoryId;
                                objmapping.Statutory_AuditorId = (int)obj.Id;
                                objmapping.flag = "CA";
                                objmapping.IsActive = true;
                                objmapping.IsDeleted = false;

                                entities.BM_StatutoryAuditor_Mapping.Add(objmapping);
                                entities.SaveChanges();
                            }
                            else
                            {
                                _objcost.errorMessage = true;
                                _objcost.SuccesserrorMessage = "Data already exist";
                                return _objcost;
                            }
                        }
                        _objcost.successMessage = true;
                        _objcost.SuccesserrorMessage = "Saved Successfully.";
                        return _objcost;

                    }
                    else
                    {
                        _objcost.errorMessage = true;
                        _objcost.SuccesserrorMessage = "Data already exist";
                        return _objcost;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objcost.errorMessage = true;
                _objcost.SuccesserrorMessage = "Server error occurred";
                return _objcost;
            }
        }

        public List<VM_CostAuditor> getCostAuditor(int customerId, long EntityId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var objcheckData = (from row in entities.BM_StatutoryAuditor
                                        join y in entities.BM_StatutoryAuditor_Mapping
                                        on row.Id equals y.Statutory_AuditorId
                                        where row.IsActive == true && row.EntityId == EntityId && row.IsDeleted == false && row.CustomerId == customerId && row.Flag == "CA" && y.flag == "CA"

                                        select new VM_CostAuditor
                                        {
                                            Id = row.Id,
                                            AuditorMappingId = y.Id,
                                            AuditorId = y.Auditor_ID,
                                            EntityId = row.EntityId,
                                            Auditor_Name = (from row in entities.BM_Auditor_Master where row.Id == y.Auditor_ID && row.IsActive == true select row.FullName_FirmName).FirstOrDefault(),
                                            CategoryName = y.CategoryId == 1 ? "Individual" : "Firm",
                                            Nature_of_intimation_cost = row.Nature_of_Intimatecost,
                                            Cost_auditor_firm_Original = row.CoustAuditor_FirmOrigine,
                                            dateofBordMeeting = row.BoardMeeting_date,
                                            ResulationNumber = row.Resulation_Number,
                                            IsMappingActive = y.IsActive
                                        }).ToList();
                    return objcheckData;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion

        #region Auditor Master Exceel Upload

        public string getEntityName(int entity_ID)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    string Entity = (from row in entities.BM_EntityMaster where row.Id == entity_ID select row.CompanyName).FirstOrDefault();
                    return Entity;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public int Getcountry(string country)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getcountryId = (from row in entities.Countries where row.Name.ToUpper() == country.ToUpper() && row.IsDeleted == false select row.ID).FirstOrDefault();
                    return getcountryId;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public int getStateId(string state)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getstateId = (from row in entities.States where row.Name.ToUpper() == state.ToUpper() && row.IsDeleted == false select row.ID).FirstOrDefault();
                    return getstateId;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public int getCityId(int stateId, string city)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getcityId = (from row in entities.Cities where row.Name.ToUpper() == city.ToUpper() && row.StateId == stateId && row.IsDeleted == false select row.ID).FirstOrDefault();
                    return getcityId;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public bool ChecksaveUpdateForAuditorMaster(BM_Auditor_Master objauditor)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var checkData = (from row in entities.BM_Auditor_Master
                                     where row.Email_Id == objauditor.Email_Id
                                     && row.CustomerId == objauditor.CustomerId
                                     && row.IsActive==true
                                     select row).FirstOrDefault();
                    if (checkData != null)
                    {
                        checkData.Audit_Type = objauditor.Audit_Type;
                        checkData.FullName_FirmName = objauditor.FullName_FirmName;
                        checkData.Category_Id = objauditor.Category_Id;
                        checkData.Email_Id = objauditor.Email_Id;
                        checkData.MobileNo = objauditor.MobileNo;
                        checkData.PAN = objauditor.PAN;
                        checkData.Reg_MemberNumber = objauditor.Reg_MemberNumber;
                        checkData.AddressLine1 = objauditor.AddressLine1;
                        checkData.AddressLine2 = objauditor.AddressLine2;
                        checkData.Country_Id = objauditor.Country_Id;
                        checkData.state_Id = objauditor.state_Id;
                        checkData.city_Id = objauditor.city_Id;
                        checkData.CustomerId = objauditor.CustomerId;
                        checkData.IsActive = true;
                        checkData.UpdatedOn = DateTime.Now;
                        checkData.Updatedby = objauditor.Createdby;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        BM_Auditor_Master objauditors = new BM_Auditor_Master();
                        objauditors.Audit_Type = objauditor.Audit_Type;
                        objauditors.FullName_FirmName = objauditor.FullName_FirmName;
                        objauditors.Category_Id = objauditor.Category_Id;
                        objauditors.Email_Id = objauditor.Email_Id;
                        objauditors.MobileNo = objauditor.MobileNo;
                        objauditors.PAN = objauditor.PAN;
                        objauditors.Reg_MemberNumber = objauditor.Reg_MemberNumber;
                        objauditors.AddressLine1 = objauditor.AddressLine1;
                        objauditors.AddressLine2 = objauditor.AddressLine2;
                        objauditors.Country_Id = objauditor.Country_Id;
                        objauditors.state_Id = objauditor.state_Id;
                        objauditors.city_Id = objauditor.city_Id;
                        objauditors.CustomerId = objauditor.CustomerId;
                        objauditors.IsActive = true;
                        objauditors.CreatedOn = DateTime.Now;
                        objauditors.Createdby = objauditor.Createdby;
                       
                        entities.BM_Auditor_Master.Add(objauditors);
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Template field Generation
        public IEnumerable<VMAuditorforDropdown> GetAuditorsByType(int type, int customerID)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _objgetall_AduditorDetails = (from row in entities.BM_Auditor_Master
                                                      where row.Audit_Type == type && row.CustomerId == customerID && row.IsActive == true
                                                      select new VMAuditorforDropdown
                                                      {
                                                          Id = row.Id,
                                                          Name = row.FullName_FirmName + (row.Last_Name == null ? "" : " " + row.Last_Name)
                                                      }).ToList();
                    return _objgetall_AduditorDetails;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }


        #endregion
        public IEnumerable<BM_Auditor_Master> GetAuditorwhoresigned(long entityId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                var getReginAuditor = (from row in entities.BM_Auditor_Master
                                       join rows in entities.BM_StatutoryAuditor_Mapping on row.Id equals rows.Auditor_ID
                                       join rowss in entities.BM_StatutoryAuditor on rows.Statutory_AuditorId equals rowss.Id
                                       where rows.IsLeave == true && rows.flag == "SA" && rowss.EntityId == entityId && rows.EntityId == entityId
                                       select row).ToList();
                return getReginAuditor;
            }
        }

        public List<statutoryAuditor> GetAuditorForEntity(int customerID, int entityId, int v1, int v2)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {

                    var getStatutorAuditor = (from row in entities.BM_SP_GetAuditorForAGM(entityId, customerID)
                                              select new statutoryAuditor
                                              {
                                                  //AuditorName=row.FullName_FirmName,
                                                  AuditorId = (int)row.AuditorId,
                                                  EmailID = row.Email_Id,
                                                  IsAuditor = true,
                                                  Designation = row.Flag.Trim() == "SA" ? "Statutory Auditor" : row.Flag.Trim() == "SCA" ? "Secretarial Auditor" : "Cost Auditor"
                                              }).ToList();
                    return getStatutorAuditor;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        #region Resignation
        public Auditor_ResignationVM UpdateAuditorResignation(Auditor_ResignationVM obj)
        {
            throw new NotImplementedException();
        }

        public Auditor_ResignationVM SaveAuditorResignation(Auditor_ResignationVM obj, int customerId, int userId)
        {
            try
            {
                #region Upload Doc
                if (obj.Resignation_Doc != null)
                {
                    string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + obj.EntityId + "/Auditor/" + obj._AuditorId;
                        if (obj.Resignation_Doc.ContentLength > 0)
                        {
                            MemoryStream target = new MemoryStream();
                            obj.Resignation_Doc.InputStream.CopyTo(target);

                            int fileVersion = 0;

                            var objFileData = new FileDataVM()
                            {
                                FileData = target.ToArray(),
                                FileName = obj.Resignation_Doc.FileName,
                                FilePath = path
                            };

                            fileVersion = 1;

                            objFileData.Version = Convert.ToString(fileVersion);
                            objFileData = objIFileData_Service.Save(objFileData, userId);

                            if(objFileData.FileID > 0)
                            {
                                obj.FileDataId = objFileData.FileID;
                                obj.Resignation_DocName = objFileData.FileName;
                            }
                        }
                }
                #endregion
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_StatutoryAuditor_Mapping
                                where row.Statutory_AuditorId == obj.statutory_Id &&
                                row.Auditor_ID == obj._AuditorId
                                select row).FirstOrDefault();

                    if (_obj != null)
                    {
                        _obj.IsLeave = true;
                        _obj.Leavecase = obj.LeaveType;
                        _obj.Leave_Date = obj.DateOfResignation;
                        _obj.DateOfResignationSubmit = obj.DateOfResignationSubmit;
                        _obj.ResignforLeave = obj.textareaResonForLeave;
                        _obj.IsActive = false;
                        _obj.Iscasualvacc = true;

                        if (obj.FileDataId > 0)
                        {
                            _obj.FileDataId = obj.FileDataId;
                            _obj.ResignationDoc = obj.Resignation_DocName;
                        }

                        entities.SaveChanges();
                        obj.Message = "Saved Successfully.";
                        obj.Success = true;
                        obj.AuditorMappingId = _obj.Id;

                        if (obj.LeaveType == "2") //Resignation
                        {
                            #region Create Open Agenda (Auditor)
                            long agendaId = 0;
                            long uiFormId = 0;
                            long agendaIdCasualVacancy = 0;
                            long uiFormIdCasualVacancy = 0;
                            var entityType = (from entity in entities.BM_EntityMaster
                                              where entity.Id == obj.EntityId
                                              select entity.Entity_Type).FirstOrDefault();
                            switch (obj.AuditorType)
                            {
                                case "IA":
                                    uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_INTERNAL_AUDITOR;
                                    break;
                                case "SCA":
                                    uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_SECRETARIAL_AUDITOR;
                                    break;
                                case "SA":
                                    uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_STATUTORY_AUDITOR;

                                    //if (obj.AgendaFlow == "1")
                                    //{
                                    //    uiFormIdCasualVacancy = SecretarialConst.UIForms.AUDITOR_RECOMMANDATION_IN_CASE_OF_CASUAL_VACANCY_IN_BOARD;
                                    //}
                                    //else if (obj.AgendaFlow == "2")
                                    //{
                                    //    uiFormIdCasualVacancy = SecretarialConst.UIForms.AUDITOR_APPOINTED_IN_CASE_OF_CASUAL_VACANCY_IN_BOARD;
                                    //}

                                    if (obj.AgendaFlow == "1")
                                    {
                                        uiFormIdCasualVacancy = SecretarialConst.UIForms.AUDITOR_RECOMMANDATION_IN_CASE_OF_CASUAL_VACANCY_IN_BOARD;
                                    }
                                    else if (obj.AgendaFlow == "2")
                                    {
                                        uiFormIdCasualVacancy = SecretarialConst.UIForms.AUDITOR_APPOINTED_IN_CASE_OF_CASUAL_VACANCY_IN_BOARD;
                                    }

                                    break;
                                case "CA":
                                    uiFormId = SecretarialConst.UIForms.RESIGNATION_OF_COST_AUDITOR;
                                    break;
                                default:
                                    break;
                            }

                            agendaId = (from agenda in entities.BM_AgendaMaster
                                        where agenda.UIFormID == uiFormId && agenda.EntityType == entityType && agenda.IsDeleted == false && agenda.MeetingTypeId == 10
                                        select agenda.BM_AgendaMasterId
                                       ).FirstOrDefault();
                            if (uiFormId > 0 && agendaId > 0)
                            {
                                var _objOpenAgenda = new BM_MeetingOpenAgendaOnCompliance();
                                _objOpenAgenda.EntityId = obj.EntityId;
                                _objOpenAgenda.RefMasterName = "Auditor Resignation";
                                _objOpenAgenda.RefMasterID = obj.AuditorMappingId;
                                _objOpenAgenda.ScheduleOnID = 0;
                                _objOpenAgenda.AgendaId = agendaId;
                                _objOpenAgenda.MeetingTypeId = 10;
                                _objOpenAgenda.IsNoted = false;
                                _objOpenAgenda.IsDeleted = false;
                                _objOpenAgenda.CreatedBy = userId;
                                _objOpenAgenda.CreatedOn = DateTime.Now;
                                _objOpenAgenda.RefMaster = "A";
                                entities.BM_MeetingOpenAgendaOnCompliance.Add(_objOpenAgenda);
                                entities.SaveChanges();
                            }

                            #region Casual vacancy Agenda
                            if(uiFormIdCasualVacancy > 0)
                            {
                                agendaIdCasualVacancy = (from agenda in entities.BM_AgendaMaster
                                            where agenda.UIFormID == uiFormIdCasualVacancy && agenda.EntityType == entityType && agenda.IsDeleted == false && agenda.MeetingTypeId == 10
                                            select agenda.BM_AgendaMasterId
                                       ).FirstOrDefault();

                                var _objOpenAgendaCasualVacancy = new BM_MeetingOpenAgendaOnCompliance();
                                _objOpenAgendaCasualVacancy.EntityId = obj.EntityId;
                                _objOpenAgendaCasualVacancy.RefMasterName = "Auditor Casual Vacancy";
                                _objOpenAgendaCasualVacancy.RefMasterIDOriginal = obj.AuditorMappingId;
                                _objOpenAgendaCasualVacancy.ScheduleOnID = 0;
                                _objOpenAgendaCasualVacancy.AgendaId = agendaIdCasualVacancy;
                                _objOpenAgendaCasualVacancy.MeetingTypeId = 10;
                                _objOpenAgendaCasualVacancy.IsNoted = false;
                                _objOpenAgendaCasualVacancy.IsDeleted = false;
                                _objOpenAgendaCasualVacancy.CreatedBy = userId;
                                _objOpenAgendaCasualVacancy.CreatedOn = DateTime.Now;
                                _objOpenAgendaCasualVacancy.RefMaster = "A";
                                entities.BM_MeetingOpenAgendaOnCompliance.Add(_objOpenAgendaCasualVacancy);
                                entities.SaveChanges();
                            }
                            #endregion

                            #endregion
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                obj.Message = "Server error occured";
                obj.Error = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public Message RevertAuditorResignation(int statutory_AuditorId, int auditorId, int userId)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_StatutoryAuditor_Mapping
                                where row.Statutory_AuditorId == statutory_AuditorId &&
                                row.Auditor_ID == auditorId
                                select row).FirstOrDefault();

                    if (_obj != null)
                    {

                        #region check resignation agenda noted or not
                        var openAgenda = (from row in entities.BM_MeetingOpenAgendaOnCompliance
                                          where row.RefMasterID == _obj.Id && row.RefMaster == "A" && row.IsDeleted == false
                                          select new
                                          {
                                              row.Id,
                                              row.IsNoted
                                          }).FirstOrDefault();
                        if (openAgenda != null)
                        {
                            if (openAgenda.IsNoted == true)
                            {
                                var meetingDetails = (from row in entities.BM_MeetingAgendaMapping
                                                      join meeting in entities.BM_Meetings on row.MeetingID equals meeting.MeetingID
                                                      where row.RefOpenAgendaId == openAgenda.Id && row.IsDeleted == false && meeting.IsDeleted == false
                                                      select new
                                                      {
                                                          row.MeetingAgendaMappingID,
                                                          meeting.MeetingTitle,
                                                      }).FirstOrDefault();
                                if (meetingDetails != null)
                                {
                                    result.Error = true;
                                    result.Message = "Agenda is used in " + meetingDetails.MeetingTitle;
                                    return result;
                                }
                            }
                        }
                        #endregion

                        if(_obj.flag == "SA")
                        {
                            #region check casual vacancy agenda noted or not
                            var openAgendaCasualVacancy = (from row in entities.BM_MeetingOpenAgendaOnCompliance
                                              where row.RefMasterIDOriginal == _obj.Id && row.RefMaster == "A" && row.IsDeleted == false
                                              select row).FirstOrDefault();
                            if (openAgendaCasualVacancy != null)
                            {
                                if (openAgendaCasualVacancy.IsNoted == true)
                                {
                                    var meetingDetails = (from row in entities.BM_MeetingAgendaMapping
                                                          join meeting in entities.BM_Meetings on row.MeetingID equals meeting.MeetingID
                                                          where row.RefOpenAgendaId == openAgendaCasualVacancy.Id && row.IsDeleted == false && meeting.IsDeleted == false
                                                          select new
                                                          {
                                                              row.MeetingAgendaMappingID,
                                                              meeting.MeetingTitle,
                                                          }).FirstOrDefault();
                                    if (meetingDetails != null)
                                    {
                                        result.Error = true;
                                        result.Message = "Agenda is used in " + meetingDetails.MeetingTitle;
                                        return result;
                                    }
                                }

                                openAgendaCasualVacancy.IsDeleted = true;
                                openAgendaCasualVacancy.UpdatedBy = userId;
                                openAgendaCasualVacancy.UpdatedOn = DateTime.Now;
                                entities.SaveChanges();
                            }
                            #endregion
                        }

                        #region Delete open Agenda
                        if (openAgenda != null)
                        {
                            var openAgendaDetails = (from row in entities.BM_MeetingOpenAgendaOnCompliance
                                                     where row.Id == openAgenda.Id && row.IsDeleted == false
                                                     select row
                                            ).FirstOrDefault();
                            if (openAgendaDetails != null)
                            {
                                openAgendaDetails.IsDeleted = true;
                                openAgendaDetails.UpdatedBy = userId;
                                openAgendaDetails.UpdatedOn = DateTime.Now;
                                entities.SaveChanges();
                            }
                        }
                        #endregion

                        _obj.IsLeave = null;
                        _obj.Leavecase = null;
                        _obj.Leave_Date = null;
                        _obj.DateOfResignationSubmit = null;
                        _obj.ResignforLeave = null;
                        _obj.IsActive = true;
                        _obj.Iscasualvacc = null;
                        _obj.FileDataId = null;
                        _obj.ResignationDoc = null;

                        entities.SaveChanges();
                        result.Message = "Resign revert Successfully.";
                        result.Success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = "Server error occured";
                result.Error = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public Auditor_ResignationVM GetAuditorResignationDetails(int Statutory_AuditorId, int AuditorId, int EntityId, string AuditorCategory)
        {
            var obj = new Auditor_ResignationVM();
            try
            {
                obj.EntityId = EntityId;
                obj._AuditorId = AuditorId;
                obj.statutory_Id = Statutory_AuditorId;
                obj.AuditorName = getAuditorName(AuditorId);
                obj.AuditorType = AuditorCategory;
                obj.AgendaFlow = "1";

                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_StatutoryAuditor_Mapping
                                where row.Statutory_AuditorId == Statutory_AuditorId &&
                                row.Auditor_ID == AuditorId && row.Iscasualvacc == true
                                select row).FirstOrDefault();
                    if (_obj != null)
                    {
                        obj.LeaveType = _obj.Leavecase;
                        obj.DateOfResignation = _obj.Leave_Date;
                        obj.DateOfResignationSubmit = _obj.DateOfResignationSubmit;
                        obj.textareaResonForLeave = _obj.ResignforLeave;
                        obj.AuditorMappingId = _obj.Id;
                        obj.Resignation_DocName = _obj.ResignationDoc;
                        obj.FileDataId = _obj.FileDataId;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public Auditor_ResignationReviewVM ResignationLetter(long AuditorMappingId)
        {
            var obj = new Auditor_ResignationReviewVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_SP_FormGenerateOnEvent(AuditorMappingId, "Auditor", "Resignation")
                                select row).FirstOrDefault();
                    if (_obj != null)
                    {
                        obj.ResignationFormat = _obj.FormFormat;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public IEnumerable<VMAuditorforDropdown> GetAuditorListForApptCess(int entityId, string auditorType, long? statutoryMappingId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var obj = (from row in entities.BM_SP_AuditorListForApptCess(entityId, auditorType, statutoryMappingId, "Cessation")
                                select new VMAuditorforDropdown
                                {
                                    Id = row.Id,
                                    Name = row.AuditorName
                                }).ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        public string DeleteAuditor(int id, int customerId, int UserId)
        {
            string Message = String.Empty;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var checkedIsAudtiorMappedwithEntity = (from y in entities.BM_StatutoryAuditor_Mapping
                                                            join x in entities.BM_StatutoryAuditor
                                                            on y.Statutory_AuditorId equals x.Id
                                                            where y.Auditor_ID == id
                                                            && y.IsActive==true
                                                            && x.IsActive==true
                                                            && x.CustomerId== customerId
                                                            select y).ToList();
                    if (checkedIsAudtiorMappedwithEntity.Count == 0)
                    {
                        var checkAuditor = (from x in entities.BM_Auditor_Master
                                            where x.Id == id && x.CustomerId == customerId && x.IsActive==true
                                            select x).FirstOrDefault();
                        if (checkAuditor != null)
                        {
                            checkAuditor.IsActive = false;
                            checkAuditor.UpdatedOn = DateTime.Now;
                            checkAuditor.Updatedby = UserId;
                            entities.SaveChanges();
                            Message = "Auditor/Auditor Firm Deleted successfully";
                        }
                        else
                        {
                            Message = "Something went wrong";
                        }
                    }

                    else
                    {
                        Message = "You not able to delete this Auditor/Auditor firm,Because this Auditor/Auditor Firm Mapped with company";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                Message = "Server error occurred";
            }
            return Message;
        }
    }
}