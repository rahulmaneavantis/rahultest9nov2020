﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IHoliday
    {
        VMHolidayMaster CreateHoliday(VMHolidayMaster _objholiday);
        VMHolidayMaster UpdateHoliday(VMHolidayMaster _objholiday);
        List<VMHolidayMaster> GetHolidayList();
        VMHolidayMaster GetHolidaybyId(int id);
        int GetStockExchangevalue(string Stockexchange);
        int GetStatevalue(string GetStatevalue);
        bool CheckSaveorUpdate(BM_HolidayMaster objholiday);
        bool CheckSaveorUpdateforstockexchange(BM_HolidayMaster objholiday);
    }
}
