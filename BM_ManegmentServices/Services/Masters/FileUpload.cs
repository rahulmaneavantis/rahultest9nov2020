﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Hosting;

namespace BM_ManegmentServices.Services.Masters
{
    public class FileUpload
    {
        public static Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public static bool SaveTaskFile(IEnumerable<HttpPostedFileBase> files, int customerId, int userId,long TaskId)
        {

            //try
            //{
            string fileName = string.Empty;
            string directoryPath = string.Empty;
            if (files != null)
            {
                foreach (var file in files)
                {
                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                    if (file.ContentLength > 0)
                    {
                        IEnumerable<HttpPostedFileBase> uploadfile1 = files;

                        directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/Document/" + customerId + "/Task/");

                        if (!Directory.Exists(directoryPath))
                            Directory.CreateDirectory(directoryPath);

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(file.FileName));
                        Stream fs = file.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                        fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                        SaveDocFiles(fileList);

                        BM_FileData _objTaskDocument = new BM_FileData();
                        _objTaskDocument.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        _objTaskDocument.FileKey = fileKey1.ToString();
                        _objTaskDocument.FileName = file.FileName;
                        _objTaskDocument.TaskId = TaskId;
                        _objTaskDocument.UploadedBy = userId;
                        _objTaskDocument.UploadedOn = DateTime.Now;
                        if (file.ContentLength>0)
                        _objTaskDocument.FileSize = (file.ContentLength).ToString();
                        entities.BM_FileData.Add(_objTaskDocument);
                        entities.SaveChanges();
                       
                    }
                }
            }
            return true;
        }
        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(CryptographyHandler.Encrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }

                }
            }
        }
        public static byte[] ReadDocFiles(string filePath)
        {
            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int)fs.Length;
                byte[] buffer;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                return buffer;
            }
        }
        public static bool downloadTaskData(List<BM_FileData> checkData)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile TaskDocument = new ZipFile())
                {
                    foreach (var files in checkData)
                    {
                        string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.FileName));

                        if (files.FilePath != null && File.Exists(filePath))
                        {
                            documentSuccess = true;
                            int idx = files.FileName.LastIndexOf('.');
                            string str = files.FileName.Substring(0, idx) + "_" + "." + files.FileName.Substring(idx + 1);
                            string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                            if (!TaskDocument.ContainsEntry("Document" + "/" + str))
                                TaskDocument.AddEntry("Document" + "/" + str, CryptographyHandler.Decrypt(ReadDocFiles(filePath)));
                        }
                    }
                    if (documentSuccess)
                    {
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        TaskDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + "TaskDocuments.zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool downloadAgendaDocument(List<BM_FileData> checkData)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile TaskDocument = new ZipFile())
                {
                    foreach (var files in checkData)
                    {
                        string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.FileName));

                        if (files.FilePath != null && File.Exists(filePath))
                        {
                            documentSuccess = true;
                            int idx = files.FileName.LastIndexOf('.');
                            string str = files.FileName.Substring(0, idx) + "_" + "." + files.FileName.Substring(idx + 1);
                            string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                            if (!TaskDocument.ContainsEntry("Document" + "/" + str))
                                TaskDocument.AddEntry("Document" + "/" + str, CryptographyHandler.Decrypt(ReadDocFiles(filePath)));
                        }
                    }
                    if (documentSuccess)
                    {
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        TaskDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + "AgendaDocuments.zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public FileDataVM GetAgendaItemsFile(long fileID, int userId)
        {
            var obj = new FileDataVM();
            try
            {
                var result = (from row in entities.BM_FileData
                              where row.Id == fileID
                              select new FileDataVM
                              {
                                  FileName = row.FileName,
                                  FileKey = row.FileKey,
                                  FilePath = row.FilePath,
                                  Version = row.Version
                              }).FirstOrDefault();

                if (result != null)
                {
                    string path = System.Web.Hosting.HostingEnvironment.MapPath(result.FilePath);
                    string fileExtension = Path.GetExtension(result.FileName);
                    string fileName = result.FileKey + fileExtension;

                    string filePath = Path.Combine(path, fileName);

                    if (File.Exists(filePath))
                    {
                        obj.FileKey = result.FileKey;
                        obj.FileName = result.FileName;
                        //obj.FileData = ReadDocFiles(filePath);
                        obj.FileData = CryptographyHandler.Decrypt(ReadDocFiles(filePath));
                        obj.Success = true;
                    }
                    else
                    {
                        obj.Error = true;
                    }
                }
                else
                {
                    obj.Error = true;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public static bool SaveAgendaFile(VM_AgendaDocument _vmagendaDocument)
        {
            try
            {
                string fileName = string.Empty;
                string directoryPath = string.Empty;
                if (_vmagendaDocument.files != null)
                {
                    foreach (var file in _vmagendaDocument.files)
                    {
                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                        if (file.ContentLength > 0)
                        {

                            IEnumerable<HttpPostedFileBase> uploadfile1 = _vmagendaDocument.files;

                            directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/Document/" + _vmagendaDocument.CustomerId + "/Task/");

                            if (!Directory.Exists(directoryPath))
                                Directory.CreateDirectory(directoryPath);

                            Guid fileKey1 = Guid.NewGuid();
                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(file.FileName));
                            Stream fs = file.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                            SaveDocFiles(fileList);

                            BM_FileData _objTaskDocument = new BM_FileData();
                            _objTaskDocument.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            _objTaskDocument.FileKey = fileKey1.ToString();
                            _objTaskDocument.FileName = file.FileName;
                            _objTaskDocument.AgendaMappingId = _vmagendaDocument.MeetingAggendaMappingId;
                            _objTaskDocument.UploadedBy = _vmagendaDocument.UserId;
                            _objTaskDocument.UploadedOn = DateTime.Now;
                            if (file.ContentLength > 0)
                                _objTaskDocument.FileSize = (file.ContentLength).ToString();
                            entities.BM_FileData.Add(_objTaskDocument);
                            entities.SaveChanges();

                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}