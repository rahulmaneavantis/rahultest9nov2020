﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IAgendaCompliance
    {
        IEnumerable<AgendaComplianceVM> GetAll(long AgendaMasterId);
        AgendaComplianceEditorVM Get(long AgendaMasterId, long id);
        AgendaComplianceVM Create(AgendaComplianceVM template);
        AgendaComplianceVM Update(AgendaComplianceVM template);
        void AddAgendaItem(VMCompliences complianceItem, int userID, int? customerID);
        List<AgendaComplianceEditorVM> GetAgendaMappingDetails(long agendaMasterId,int? CustomerId);
        void AddAgendaDetails(AgendaComplianceEditorVM complianceItem, int userID, int? customerID);
        List<VMCompliences> CheckAgendaCompliance(long? agendaMasterId);
        bool IsOnComplianceCompletion(long? agendaMasterId);
        void SetHasCompliance(long? agendaMasterId);

        #region Binding Static DropDown For ComplianceMapping added by Ruchi
        IEnumerable<DayTypeViewModel> getDayType(string dayhoureId);
        IEnumerable<DaysOrHoursViewModel> GetHoureMinutesDay();

        void DeleteAgendaComplianceMapping(AgendaComplianceEditorVM complianceItem, int userID, int? customerID,string Role);
        #endregion
    }
}
