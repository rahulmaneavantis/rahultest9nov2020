﻿using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class AgendaCompliance : IAgendaCompliance
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public IEnumerable<AgendaComplianceVM> GetAll(long AgendaMasterId)
        {
            var result = from mapping in entities.BM_ComplianceMapping
                         join com in entities.Compliances on mapping.ComplianceID equals com.ID
                         where mapping.AgendaMasterID == AgendaMasterId && mapping.MappingType == SecretarialConst.ComplianceMappingType.AGENDA && mapping.IsDeleted == false
                         select new AgendaComplianceVM
                         {
                             Agenda_ComplianceId = mapping.MCM_ID,
                             AgendaMasterId = (long)mapping.AgendaMasterID,
                             ComplianceId = mapping.ComplianceID,

                             ComplianceShortDesc = com.ShortDescription,
                             DateType = mapping.DateType,
                             DayType = mapping.DayType.Trim(),
                             BeforeAfter = mapping.BeforeAfter.Trim(),
                             Numbers = mapping.Numbers,
                             DaysOrHours = mapping.DaysOrHours.Trim()
                         };
            return result;
        }
        public AgendaComplianceEditorVM Get(long agendaMasterId, long id)
        {
            var result = (from mapping in entities.BM_ComplianceMapping
                          join com in entities.Compliances on mapping.ComplianceID equals com.ID
                          where mapping.MCM_ID == id && mapping.AgendaMasterID == agendaMasterId && mapping.MappingType == SecretarialConst.ComplianceMappingType.AGENDA && mapping.IsDeleted == false
                          select new AgendaComplianceEditorVM
                          {
                              Agenda_ComplianceId = mapping.MCM_ID,
                              AgendaMasterId = (long)mapping.AgendaMasterID,
                              ComplianceId = mapping.ComplianceID,

                              DateType = mapping.DateType,
                              DayType = mapping.DayType.Trim(),
                              BeforeAfter = mapping.BeforeAfter.Trim(),
                              //var datetype = new
                              DayTypeNames = new DayTypeViewModel
                              {
                                  DayType = mapping.DayType,
                                  //DayTypeName= row.DayType == "W" ? "Working Day(s)" : "Calendar Day(s)",
                                  DayTypeName = mapping.DayType == null ? "" : mapping.DayType == "W" ? "Working Day(s)" : "Calendar Day(s)",
                              },
                              BeforeAfterNames = new BeforeAfterViewModel
                              {
                                  BeforeAfter = mapping.BeforeAfter,
                                  BeforeAfterName = mapping.BeforeAfter == null ? "" : mapping.BeforeAfter == "A" ? "After" : "Before",
                              },
                              DaysOrHoursNames = new DaysOrHoursViewModel
                              {
                                  DaysOrHours = mapping.DaysOrHours,
                                  DaysOrHoursName = mapping.DaysOrHours == null ? "" : mapping.DaysOrHours == "D" ? "Day(s)" : mapping.DaysOrHours == "H" ? "Hour(s)" : "Minute(s)",
                              },

                              Numbers = mapping.Numbers,
                              DaysOrHours = mapping.DaysOrHours.Trim()
                          }).FirstOrDefault();

            return result;
        }
        public AgendaComplianceVM Create(AgendaComplianceVM obj)
        {
            try
            {
                var isExists = entities.BM_ComplianceMapping.Where(k => k.AgendaMasterID == obj.AgendaMasterId && k.ComplianceID == obj.ComplianceId && k.IsDeleted == false).Any();

                if (isExists)
                {
                    obj.Error = true;
                    obj.Message = "Data Allready Exists.";
                    return obj;
                }
                var _obj = new BM_ComplianceMapping();

                _obj.MCM_ID = obj.Agenda_ComplianceId;
                _obj.AgendaMasterID = obj.AgendaMasterId;
                _obj.ComplianceID = obj.ComplianceId;

                _obj.MappingType = SecretarialConst.ComplianceMappingType.AGENDA;
                _obj.DateType = "M";
                _obj.DayType = obj.DayType;
                _obj.BeforeAfter = obj.BeforeAfter;
                _obj.Numbers = obj.Numbers;
                _obj.DaysOrHours = obj.DaysOrHours;

                _obj.IsDeleted = false;
                _obj.CreatedBy = obj.UserId;
                _obj.CreatedOn = DateTime.Now;

                entities.BM_ComplianceMapping.Add(_obj);
                entities.SaveChanges();
                obj.Agenda_ComplianceId = _obj.MCM_ID;

                var agenda = entities.BM_AgendaMaster.Where(k => k.BM_AgendaMasterId == obj.AgendaMasterId && k.IsDeleted == false).FirstOrDefault();
                if (agenda != null)
                {
                    agenda.HasCompliance = true;
                    entities.SaveChanges();
                }

                obj.Success = true;
                obj.Message = "Saved Successfully.";
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }
        public AgendaComplianceVM Update(AgendaComplianceVM obj)
        {
            try
            {
                var isExists = entities.BM_ComplianceMapping.Where(k => k.MCM_ID != obj.Agenda_ComplianceId && k.AgendaMasterID == obj.AgendaMasterId && k.ComplianceID == obj.ComplianceId && k.IsDeleted == false).Any();

                if (isExists)
                {
                    obj.Error = true;
                    obj.Message = "Data Allready Exists.";
                    return obj;
                }

                var _obj = entities.BM_ComplianceMapping.Where(k => k.MCM_ID == obj.Agenda_ComplianceId && k.AgendaMasterID == obj.AgendaMasterId && k.IsDeleted == false).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.ComplianceID = obj.ComplianceId;

                    _obj.DateType = obj.DateType;
                    _obj.DayType = obj.DayType;
                    _obj.BeforeAfter = obj.BeforeAfter;
                    _obj.Numbers = obj.Numbers;
                    _obj.DaysOrHours = obj.DaysOrHours;

                    _obj.IsDeleted = false;
                    _obj.UpdatedBy = obj.UserId;
                    _obj.UpdatedOn = DateTime.Now;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    obj.Success = true;
                    obj.Message = "Updated Successfully.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occur";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public void AddAgendaItem(VMCompliences complianceItem, int userID, int? customerID)
        {
            try
            {
                var checkCompliancemappingagendawise = (from row in entities.BM_ComplianceMapping
                                                        where row.AgendaMasterID == complianceItem.AgendaId

              && row.MappingType == "A"
              && row.ComplianceID == complianceItem.ID
                                                        select row).FirstOrDefault();
                if (checkCompliancemappingagendawise == null)
                {
                    BM_ComplianceMapping objcompliancemapping = new BM_ComplianceMapping();

                    objcompliancemapping.AgendaMasterID = complianceItem.AgendaId;
                    objcompliancemapping.MappingType = "A";
                    objcompliancemapping.DateType = "M";
                    objcompliancemapping.triggeron = "A";
                    objcompliancemapping.ComplianceID = complianceItem.ID;
                    objcompliancemapping.AutoComplete = false;
                    objcompliancemapping.IsDeleted = false;
                    objcompliancemapping.CreatedBy = userID;
                    objcompliancemapping.CreatedOn = DateTime.Now;
                    objcompliancemapping.CustomerID = customerID;
                    entities.BM_ComplianceMapping.Add(objcompliancemapping);
                    entities.SaveChanges();
                }
                else
                {
                   checkCompliancemappingagendawise.AgendaMasterID = complianceItem.AgendaId;
                   checkCompliancemappingagendawise.MappingType = "A";
                   checkCompliancemappingagendawise.DateType = "M";
                   checkCompliancemappingagendawise.triggeron = "A";
                   checkCompliancemappingagendawise.ComplianceID = complianceItem.ID;
                   checkCompliancemappingagendawise.AutoComplete = false;
                   checkCompliancemappingagendawise.IsDeleted = false;
                 
                    checkCompliancemappingagendawise.CustomerID = customerID;
                    checkCompliancemappingagendawise.IsDeleted = false;
                    checkCompliancemappingagendawise.UpdatedBy = userID;
                    checkCompliancemappingagendawise.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Get List of  Mapping Details
        public List<AgendaComplianceEditorVM> GetAgendaMappingDetails(long agendaMasterId,int? CustomerId)
        {
            try
            {
                var getAgendaMappingDetails = (from row in entities.BM_ComplianceMapping
                                               where row.AgendaMasterID == agendaMasterId
                                               && row.IsDeleted == false 
                                               //row.CustomerID== CustomerId
                                               select new AgendaComplianceEditorVM
                                               {
                                                   Agenda_ComplianceId = row.MCM_ID,
                                                   ComplianceId = row.ComplianceID,
                                                   ComplianceShortDesc = (from com in entities.Compliances where com.ID == row.ComplianceID select com.ShortDescription).FirstOrDefault(),

                                                   AgendaMasterId = row.AgendaMasterID,
                                                   Numbers = row.Numbers,
                                                  
                                                   Compliancesrno = row.Srno,
                                                   DayTypeNames = new DayTypeViewModel
                                                   {
                                                       DayType = row.DayType,
                                                       DayTypeName = row.DayType == "" ? "" : row.DateType == null ? "" : row.DayType == "W" ? "Clear Working Day(s)" : "Normal Day(s)",
                                                   },
                                                   BeforeAfterNames = new BeforeAfterViewModel
                                                   {
                                                       BeforeAfter = row.BeforeAfter,
                                                       BeforeAfterName = row.BeforeAfter == "" ? "" : row.BeforeAfter == null ? "" : row.BeforeAfter == "A" ? "After" : "Before",
                                                   },
                                                   DaysOrHoursNames = new DaysOrHoursViewModel
                                                   {
                                                       DaysOrHours = row.DaysOrHours,
                                                       DaysOrHoursName = row.DaysOrHours == "" ? "" : row.DaysOrHours == null ? "" : row.DaysOrHours == "D" ? "Day(s)" : row.DaysOrHours == "H" ? "Hour(s)" : "Minute(s)",
                                                   },
                                                   TriggerOn=new TriggeronViewdata
                                                   {
                                                       Triggeron=row.triggeron,
                                                       TriggerOnName=row.triggeron==""?"":row.triggeron==null?"":row.triggeron=="A"?"OnApproved":row.triggeron=="D"?"OnDisapproved":row.triggeron=="B"?"Both":"",
                                                   },
                                                   Type=new TypeViewModel
                                                   {
                                                       Type=row.Type,
                                                       TypeName=row.Type==""?"":row.Type==null?"":row.Type=="S"?"Single":row.Type=="C"?"Combined":"",
                                                   },
                                                   Autoclose = row.AutoComplete,
                                                   startDate = row.StartDate,
                                                   endDate = row.EndDate

                                               }).ToList();

                if (getAgendaMappingDetails.Count > 0)
                {
                  
                    getAgendaMappingDetails = (from row in getAgendaMappingDetails orderby row.Compliancesrno, row.afterbefor select row).OrderByDescending(m=>m.BeforeAfterNames.BeforeAfter).ToList();

                }
                return getAgendaMappingDetails;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion

        public void AddAgendaDetails(AgendaComplianceEditorVM complianceItem, int userID, int? customerID)
        {
            try
            {
                var checkCompliancedetails = (from row in entities.BM_ComplianceMapping
                                              where row.AgendaMasterID == complianceItem.AgendaMasterId
                                              //&& row.CustomerID== customerID
                                              //&& row.EntityType== complianceItem.EntityTypeId
                                              && row.MappingType == "A"
                                              && row.ComplianceID == complianceItem.ComplianceId
                                              select row).FirstOrDefault();
                if (checkCompliancedetails != null)
                {
                    checkCompliancedetails.MeetingTypeID = complianceItem.MeetingType;
                    checkCompliancedetails.MappingType = SecretarialConst.ComplianceMappingType.AGENDA;
                    checkCompliancedetails.DateType = SecretarialConst.ComplianceDateType.MEETINGDATE;
                    checkCompliancedetails.triggeron = complianceItem.TriggerOn.Triggeron;
                    checkCompliancedetails.Type = complianceItem.Type.Type;
                    checkCompliancedetails.ComplianceID = complianceItem.ComplianceId;
                    checkCompliancedetails.AutoComplete = complianceItem.Autoclose;
                    checkCompliancedetails.BeforeAfter = complianceItem.BeforeAfterNames.BeforeAfter;
                    //

                    checkCompliancedetails.Numbers = complianceItem.Numbers;
                    checkCompliancedetails.DaysOrHours = complianceItem.DaysOrHoursNames.DaysOrHours;
                    if (complianceItem.DaysOrHoursNames.DaysOrHours == "D")
                    {
                        checkCompliancedetails.DayType = complianceItem.DayTypeNames.DayType;
                    }
                    else
                    {
                        checkCompliancedetails.DayType = "";
                    }
                    checkCompliancedetails.UpdatedOn = DateTime.Now;
                    checkCompliancedetails.UpdatedBy = userID;
                    checkCompliancedetails.CustomerID = customerID;
                    if (complianceItem.BeforeAfterNames.BeforeAfter == "A")
                    {
                        checkCompliancedetails.DuesNoIn_dsending = (int)complianceItem.Numbers;
                    }
                    else if (complianceItem.BeforeAfterNames.BeforeAfter == "B")
                    {
                        checkCompliancedetails.DuesNoIn_dsending = (int)-complianceItem.Numbers;
                    }
                    if (complianceItem.MeetingLevel != null)
                    {
                        checkCompliancedetails.AutoCompleteOn = complianceItem.MeetingLevel.MeetingLevelId;
                    }
                    if (complianceItem.Compliancesrno > 0)
                    {
                        checkCompliancedetails.Srno = complianceItem.Compliancesrno;
                    }
                    if (complianceItem.startDate != null)
                    {
                        checkCompliancedetails.StartDate = complianceItem.startDate;
                    }
                    if (complianceItem.endDate != null)
                    {
                        checkCompliancedetails.EndDate = complianceItem.endDate;
                    }
                    checkCompliancedetails.IsDeleted = false;
                    entities.SaveChanges();
                }
                else
                {
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public List<VMCompliences> CheckAgendaCompliance(long? agendaMasterId)
        {
            try
            {
                var checkAgedacompliancemapping = (from row in entities.BM_ComplianceMapping
                                                   where row.AgendaMasterID == agendaMasterId
                                                   where row.IsDeleted == false
                                                   select new VMCompliences
                                                   {
                                                       ID = row.MCM_ID,
                                                       AgendaId = row.AgendaMasterID,

                                                   }).ToList();
                return checkAgedacompliancemapping;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public bool IsOnComplianceCompletion(long? agendaMasterId)
        {
            try
            {
                var result = (from row in entities.BM_AgendaMaster
                              where row.BM_AgendaMasterId == agendaMasterId && row.FrequencyId == 15
                              select row.BM_AgendaMasterId).Any();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public void SetHasCompliance(long? agendaMasterId)
        {
            try
            {
                if(agendaMasterId > 0)
                {
                    var hasCompliance = entities.BM_ComplianceMapping.Where(k => k.AgendaMasterID == agendaMasterId && k.IsDeleted == false).Any();
                    var agenda = entities.BM_AgendaMaster.Where(k => k.BM_AgendaMasterId == agendaMasterId && k.IsDeleted == false).FirstOrDefault();
                    if (agenda != null)
                    {
                        agenda.HasCompliance = hasCompliance;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                //obj.Error = true;
                //obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Binding Static DropDown For ComplianceMapping added by Ruchi
        public IEnumerable<DayTypeViewModel> getDayType(string dayhoureId)
        {
            List<DayTypeViewModel> listDayType = new List<DayTypeViewModel>();
            if (dayhoureId == "D")
            {
                listDayType = new List<DayTypeViewModel>
            {
       
          new DayTypeViewModel {DayTypeName="Clear Working Day(s)",DayType="W" },
          new DayTypeViewModel {DayTypeName="Normal Day(s)",DayType="C" },
            };
            }
            else
            {
                listDayType = new List<DayTypeViewModel>
                {
                new DayTypeViewModel { DayTypeName = "Select", DayType = "" }
                };
            }
            return listDayType;
        }

        public IEnumerable<DaysOrHoursViewModel> GetHoureMinutesDay()
        {
            var listDayorHoure = new List<DaysOrHoursViewModel>
         {
      
          new DaysOrHoursViewModel {DaysOrHoursName="Day(s)",DaysOrHours="D" },
          new DaysOrHoursViewModel {DaysOrHoursName="Hour(s)",DaysOrHours="H" },
          new DaysOrHoursViewModel {DaysOrHoursName="Minute(s)",DaysOrHours="M" }
         };
            return listDayorHoure;
        }
        #region Delete Compliance Mapping Updated on 7th july 2020 by Ruchi
        public void DeleteAgendaComplianceMapping(AgendaComplianceEditorVM complianceItem, int userID, int? customerID,string Role)
        {
            try
            {
                var CheckAgendaComplianceMapping = (from row in entities.BM_ComplianceMapping
                                                    where row.MCM_ID == complianceItem.Agenda_ComplianceId 
                                                    && row.IsDeleted == false 
                                                    //&& row.CustomerID== customerID
                                                    select row).FirstOrDefault();
                if (CheckAgendaComplianceMapping != null)
                {
                    CheckAgendaComplianceMapping.IsDeleted = true;
                    CheckAgendaComplianceMapping.UpdatedBy = userID;
                    CheckAgendaComplianceMapping.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        #endregion

        #endregion
    }
}