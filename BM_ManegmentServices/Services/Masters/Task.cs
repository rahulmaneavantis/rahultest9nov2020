﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using System.Reflection;
using System.IO;
using System.Web.Hosting;
using BM_ManegmentServices.Data;
using System.Data.Entity.SqlServer;

namespace BM_ManegmentServices.Services.Masters
{
    public class Task : ITask
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public static string DocPath = "";
        public IEnumerable<TaskMeeting> GetMeetingList(long EntityId)
        {
            try
            {
                var _objGetMeetingList = (from row in entities.BM_Meetings where row.EntityId== EntityId
                                          select new TaskMeeting
                                          {
                                              MeetingId=row.MeetingID,
                                              MeetingTitle=row.MeetingTitle
                                          }
                                          ).ToList();


                return _objGetMeetingList;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<BM_MeetingAgendaMapping> GetAgendaList(int MeetingId)
        {
            try
            {
                var _objGetMeetingList = (from row in entities.BM_MeetingAgendaMapping
                                          where row.IsDeleted == false && row.MeetingID == MeetingId
                                          select row).ToList();

                return _objGetMeetingList;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMTask UpdateTask(VMTask vmtask)
        {
            try
            {
                var CheckExist = (from row in entities.BM_Task
                                  where row.EntityId == vmtask.EntityId && row.TaskType == row.TaskType
                                  select row).FirstOrDefault();
                if (CheckExist != null)
                {
                    if (vmtask.EntityId > 0)
                    {
                        CheckExist.EntityId = (long)vmtask.EntityId;
                    }
                    CheckExist.CustomerId = vmtask.CustomerId;
                    CheckExist.TaskType = vmtask.TaskType;
                    CheckExist.TaskTitle = vmtask.TaskTitle;
                    CheckExist.TaskDesc = vmtask.Description;

                    CheckExist.AssignOn = DateTime.Now;
                    CheckExist.DueDate = Convert.ToDateTime(vmtask.DueDate);
                    CheckExist.MeetingId = vmtask.MeetingType;
                    CheckExist.AgendaId = vmtask.AgendaId;
                    CheckExist.IsActive = true;
                    CheckExist.UpdatedOn = DateTime.Now;
                    CheckExist.Updatedby = vmtask.UserId;
                    if (vmtask.files != null)
                    {
                        bool TaskUpload = FileUpload.SaveTaskFile(vmtask.files, vmtask.CustomerId, vmtask.UserId, vmtask.Id);
                    }
                    entities.SaveChanges();

                    vmtask.successMessage = true;
                    vmtask.successErrorMsg = "Updated Successfully.";
                }
                return vmtask;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                vmtask.errorMessage = true;
                vmtask.successErrorMsg = "Server error occured!Please try again later";
                return vmtask;
            }
        }

        public VMTask CreateTask(VMTask vmtask)
        {
            try
            {
                var CheckExist = (from row in entities.BM_Task
                                  where row.EntityId == vmtask.EntityId && row.TaskTitle == vmtask.TaskTitle
                                  select row).FirstOrDefault();
                if (CheckExist == null && vmtask.DueDate != null)
                {
                    BM_Task bm_task = new BM_Task();
                    if (vmtask.EntityId > 0)
                    {
                        bm_task.EntityId = (long)vmtask.EntityId;
                    }
                    bm_task.CustomerId = vmtask.CustomerId;
                    bm_task.TaskType = vmtask.TaskType;
                    bm_task.TaskTitle = vmtask.TaskTitle;
                    if (vmtask.Description == null)
                    {
                        bm_task.TaskDesc = "";
                    }
                    else
                    {
                        bm_task.TaskDesc = vmtask.Description;
                    }
                    bm_task.AssignOn = DateTime.Now;
                    bm_task.DueDate = DateTime.ParseExact(vmtask.DueDate, "dd/MM/yyyy", null);
                    if (vmtask.TaskType == 1)
                    {
                        bm_task.MeetingId = vmtask.MeetingType;
                    }
                    else if (vmtask.TaskType == 2)
                    {
                        bm_task.MeetingId = vmtask.MeetingType;
                        bm_task.AgendaId = vmtask.AgendaId;
                    }
                    if (vmtask.Tasklevel == "M" && vmtask.RoleName == SecretarialConst.Roles.DRCTR)
                    {
                        bm_task.IsATR = true;
                    }
                    else
                    {
                        bm_task.IsATR = false;
                    }

                    bm_task.IsActive = true;
                    bm_task.CreatedOn = DateTime.Now;
                    bm_task.Createdby = vmtask.Createby;
                    bm_task.status = "Pending";
                    entities.BM_Task.Add(bm_task);
                    entities.SaveChanges();
                    vmtask.Id = bm_task.ID;
                    if (vmtask.files != null)
                    {
                        bool TaskUpload = FileUpload.SaveTaskFile(vmtask.files, vmtask.CustomerId, vmtask.UserId, vmtask.Id);
                    }
                    if (vmtask.Id > 0)
                    {
                        BM_TaskAssignment objtaskAssignment = new BM_TaskAssignment();
                        objtaskAssignment.UserID = (long)vmtask.Createby;
                        objtaskAssignment.RoleId = 4;
                        objtaskAssignment.TaskInstanceID = vmtask.Id;
                        objtaskAssignment.CreatedOn = DateTime.Now;
                        objtaskAssignment.Createdby = vmtask.Createby;
                        objtaskAssignment.IsActive = true;
                        entities.BM_TaskAssignment.Add(objtaskAssignment);
                        entities.SaveChanges();
                        if (objtaskAssignment.ID > 0)
                        {
                            BM_TaskAssignment objtaskAssignmentper = new BM_TaskAssignment();
                            objtaskAssignmentper.UserID = vmtask.UserId;
                            objtaskAssignmentper.RoleId = 3;
                            objtaskAssignmentper.TaskInstanceID = vmtask.Id;
                            objtaskAssignmentper.CreatedOn = DateTime.Now;
                            objtaskAssignmentper.Createdby = vmtask.Createby;
                            objtaskAssignmentper.IsActive = true;
                            entities.BM_TaskAssignment.Add(objtaskAssignmentper);
                            entities.SaveChanges();
                        }
                    }

                    vmtask.successMessage = true;
                    vmtask.successErrorMsg = "Saved Successfully.";
                    return vmtask;
                }
                else
                {
                    vmtask.errorMessage = true;
                    vmtask.successErrorMsg = "Data allready exist";
                    return vmtask;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                vmtask.errorMessage = true;
                vmtask.successErrorMsg = "Server error occured!Please try again later";
                return vmtask;
            }
        }

        public VMTask CreateTaskAssinment(VMTask vmtask)
        {
            try
            {

                var checkTaskAssignment = (from row in entities.BM_TaskAssignment
                                           where row.TaskInstanceID == vmtask.TaskId
                                           && row.IsActive == true && row.UserID == vmtask.UserId && row.RoleId == vmtask.Role
                                           select row).ToList();
                if (checkTaskAssignment.Count > 2)
                {
                    vmtask.errorMessage = true;
                    vmtask.successErrorMsg = "Performer and Reviewer is not greater then three";
                    return vmtask;
                }
                else
                {
                    var entity = new BM_TaskAssignment();
                    entity.RoleId = vmtask.Role;
                    entity.UserID = vmtask.AssignTo;
                    entity.TaskInstanceID = vmtask.TaskId;
                    entity.CreatedOn = DateTime.Now;
                    entity.Createdby = vmtask.UserId;
                    entity.IsActive = true;
                    // entity.Status = "Pending";
                    entities.BM_TaskAssignment.Add(entity);
                    entities.SaveChanges();
                    vmtask.TaskAssinmentId = entity.ID;
                    vmtask.AssignTo = (int)entity.UserID;
                    vmtask.Role = entity.RoleId;
                    if (entity.ID > 0)
                    {
                        BM_TaskTransaction bm_taskTransaction = new BM_TaskTransaction();
                        bm_taskTransaction.TaskID = vmtask.TaskId;
                        bm_taskTransaction.IsActive = true;
                        bm_taskTransaction.Status = "Open";
                        bm_taskTransaction.StatusChangeOn = DateTime.Now;
                        bm_taskTransaction.UserID = vmtask.AssignTo;
                        bm_taskTransaction.RoleID = vmtask.Role;
                        bm_taskTransaction.CreatedOn = DateTime.Now;
                        bm_taskTransaction.CreatedBy = vmtask.UserId;
                        entities.BM_TaskTransaction.Add(bm_taskTransaction);
                        entities.SaveChanges();
                        vmtask.TaskTransactionId = bm_taskTransaction.ID;
                    }
                    GetAll(Convert.ToInt32(vmtask.TaskId)).Insert(0, vmtask);
                    return vmtask;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return vmtask;
            }
        }

        public List<VMTask> GetAll(int TaskId)
        {
            var result = entities.BM_TaskAssignment.Where(x => x.TaskInstanceID == TaskId).Select(assinment => new VMTask
            {
                AssignTo = (int)assinment.UserID,
                Role = assinment.RoleId,
                TaskAssinmentId = assinment.ID,
                TaskId = assinment.TaskInstanceID,
                UserName = (from x in entities.Users where x.ID == assinment.UserID select x.FirstName + "  " + x.LastName).FirstOrDefault(),
                RoleName = assinment.RoleId == 4 ? "Reviwer" : "Performer"
            }).ToList();

            return result;
        }

        public List<VMTask> ReadData(int TaskId)
        {
            try
            {
                return GetAll(TaskId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<VMTask> GetTaskDetails(int customerId, int userId, string Role)
        {
            try
            {

                List<VMTask> _obj = new List<VMTask>();
                var getTaskDetails = (from row in entities.BM_SP_Task(userId) orderby row.ID descending select row).ToList();
                var getAssignedUser = (from a in getTaskDetails
                                       orderby a.ID descending where a.RoleName.Trim()=="Performer"
                                       select new VMTask
                                       {
                                           UserId = (int)a.UserID,
                                           Createby = a.Createdby,
                                           Id = a.ID,
                                           TaskTitle = a.TaskTitle,
                                           status = a.Status,
                                           DueDate = a.DueDate.ToString("dd-MMM-yyyy"),
                                           TaskTypes = a.TaskType,
                                           RoleName = a.RoleName,
                                           Taskcreatedby = (from u in entities.Users where u.ID == a.Createdby select u.FirstName + "  " + u.LastName).FirstOrDefault(),
                                           TaskAssignTo = (from u in entities.Users where u.ID == a.UserID select u.FirstName + "  " + u.LastName).FirstOrDefault(),
                                           CompanyName = a.CompanyName,
                                       }).ToList();

                if (getAssignedUser.Count > 0)
                {
                    getAssignedUser = (from g in getAssignedUser
                                       group g by new
                                       {
                                           g.TaskId,
                                           g.UserId,
                                           g.Createby,
                                           g.Id,
                                           g.TaskTitle,
                                           g.Description,
                                           g.EntityId,
                                           g.CompanyName,
                                           g.DueDate,
                                           g.AssignOnDate,
                                           g.status,
                                           g.RoleName,
                                           g.TaskTypes,
                                           g.TaskAssignTo,
                                           g.Taskcreatedby,

                                       } into GCS
                                       select new VMTask()
                                       {
                                           Id = GCS.Key.Id,
                                           UserId = GCS.Key.UserId,
                                           Createby = GCS.Key.Createby,
                                           TaskTitle = GCS.Key.TaskTitle,
                                           Description = GCS.Key.Description,
                                           EntityId = GCS.Key.EntityId,
                                           CompanyName = GCS.Key.CompanyName,
                                           DueDate = GCS.Key.DueDate,
                                           AssignOnDate = GCS.Key.AssignOnDate,
                                           status = GCS.Key.status,
                                           RoleName = GCS.Key.RoleName,
                                           TaskTypes = GCS.Key.TaskTypes,
                                           TaskAssignTo = GCS.Key.TaskAssignTo,
                                           Taskcreatedby = GCS.Key.Taskcreatedby,
                                       }).ToList();
                   
                }

                return getAssignedUser;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMTask gettaskbyId(int id, int userId, int customerId, string Role)
        {
            try
            {
                VMTask obj = new VMTask();
                DateTime duedate;
                DateTime assigndate;
                //= DateTime.Parse(something);
                int per = 4;
                int rev = 3;
                //DateTime now = DateTime.Now;
                //string asString = now.ToString("dd MMMM yyyy hh:mm:ss tt");
                var gettaskdata = (from row in entities.BM_Task
                                   where row.ID == id
                                   select row).FirstOrDefault();
                if (gettaskdata != null)
                {
                    obj.TaskId = gettaskdata.ID;
                    obj.Id = gettaskdata.ID;
                    // assigndate = DateTime.Parse(gettaskdata.AssignOn);
                    // row.AssignOn == null ? " " : ((DateTime)row.AssignOn).ToString("dd/MM/yyyy"),
                    //DueDate = row.DueDate.ToString("dd/MM/yyyy"),
                    //date= DateTime.Parse(row.DueDate).ToString()
                    duedate = gettaskdata.DueDate;
                    obj.MeetingType = gettaskdata.MeetingId;
                    obj.AgendaId = gettaskdata.AgendaId;
                    obj.DueDate = duedate.ToString("dd/MM/yyyy");
                    obj.TaskType = gettaskdata.TaskType;
                    obj.EntityId = (int)gettaskdata.EntityId;
                    obj.TaskTitle = gettaskdata.TaskTitle;
                    obj.Description = gettaskdata.TaskDesc;
                    obj.Createby = gettaskdata.Createdby;
                    obj.Role = (from x in entities.BM_TaskAssignment where x.UserID == userId && x.TaskInstanceID == id select x.RoleId).FirstOrDefault();
                    obj.Remark = (from y in entities.BM_TaskAssignment where y.UserID == userId && y.TaskInstanceID == id select y.Remark).FirstOrDefault();

                }

                if ((obj.Createby == userId) && ((obj.Role != per) && (obj.Role != rev)))
                {


                    obj.View = "_AddEditTasknew";
                }
                else if (Role == "CS" && obj.Role != per && obj.Role != rev)
                {

                    obj.View = "_AddEditTasknew";
                }
                else
                {
                    if (obj.Role == 4)
                    {
                        obj.View = "_EditAssignTask";
                    }
                    else if (obj.Role == 3)
                    {
                        obj.View = "_EditTaskAssignmentPerformer";
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public VMTask UpdateAssigTask(VMTask _objvmtask)
        {
            try
            {
                var checktaskAssign = (from row in entities.BM_TaskAssignment
                                       where
 row.ID == _objvmtask.TaskAssinmentId && row.IsActive == true &&
 row.TaskInstanceID == _objvmtask.TaskId
                                       select row).FirstOrDefault();
                if (checktaskAssign != null)
                {
                    checktaskAssign.RoleId = _objvmtask.Role;
                    checktaskAssign.UserID = _objvmtask.AssignTo;
                    checktaskAssign.TaskInstanceID = _objvmtask.TaskId;
                    entities.SaveChanges();
                    _objvmtask.successMessage = true;
                    _objvmtask.successErrorMsg = "Updated Successfully.";
                    //return _objvmtask;
                }
                return _objvmtask;
            }
            catch (Exception ex)
            {
                return _objvmtask;
            }
        }

        public bool DownloadTaskFile(int id)
        {
            try
            {
                var CheckData = (from row in entities.BM_FileData where row.TaskId == id select row).ToList();
                if (CheckData.Count > 0)
                {
                    bool IsDocumentDownload = FileUpload.downloadTaskData(CheckData);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DownloadTaskFile1(int id)
        {
            try
            {
                var CheckData = (from row in entities.BM_FileData where row.TaskId == id select row).ToList();
                if (CheckData.Count > 0)
                {
                    bool IsDocumentDownload = FileUpload.downloadTaskData(CheckData);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool ViewTaskFile(int id)
        {
            try
            {
                string file = string.Empty;
                var getTaskDocument = (from row in entities.BM_FileData where row.TaskId == id select row).ToList();
                if (getTaskDocument.Count > 0)
                {
                    foreach (var files in getTaskDocument)
                    {
                        string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.FileName));

                        if (files.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + File;

                            string extension = System.IO.Path.GetExtension(filePath);
                            if (extension != ".zip")
                            {
                                //Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(HostingEnvironment.MapPath(DateFolder));
                                }

                                //string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = 6244 + "" + 5 + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(HostingEnvironment.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                bw.Write(CryptographyHandler.Decrypt(FileUpload.ReadDocFiles(filePath)));
                                bw.Close();

                                DocPath = FileName;

                                var documentViewer = new DocumentViewer
                                {
                                    Width = 800,
                                    Height = 600,
                                    Resizable = true,
                                    Document = DocPath
                                };


                            }
                            //else
                            //{
                            //    //lblDocuments.Text = "Zip file cannot be viewed here. Please use the download option.";
                            //    //DocumentViewer1.Visible = false;
                            //}
                        }

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<TaskRemarks> GetPerformerDetails(int customerId, int userId, int taskId)
        {
            try
            {
                var getfiledata = (from row in entities.BM_TaskAssignment
                                   where row.TaskInstanceID == taskId
                                   select new TaskRemarks
                                   {
                                       Id = row.ID,
                                       TaskId = row.TaskInstanceID,
                                       User = (from x in entities.Users where x.ID == row.UserID select x.FirstName + " " + x.LastName).FirstOrDefault(),
                                       Remark = row.Remark,
                                       Role = row.RoleId == 3 ? "Performer" : "Reviewer",
                                       //UploadedBy = (from F in entities.Users where F.ID == row.FileUploadedBy select F.FirstName + " " + F.LastName).FirstOrDefault(),
                                       //UploadedOn = (from U in entities.BM_FileData where U.Id == row.fileId select U.UploadedOn).FirstOrDefault()

                                   }).ToList();

                return getfiledata;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMTask UpdateTaskPerformer(VMTask vmtask)
        {
            throw new NotImplementedException();
        }

        public VMTask CreateTaskPerformer(VMTask vmtask)
        {
            try
            {
                var getTasassignkData = (from row in entities.BM_TaskAssignment where row.TaskInstanceID == vmtask.Id && row.UserID == vmtask.UserId select row).FirstOrDefault();
                var getTaskData = (from row in entities.BM_Task where row.ID == vmtask.Id && row.IsActive == true select row).FirstOrDefault();
                //   var getTaskremark = (from x in entities.BM_TaskAssignment where x.TaskId == getTaskData.ID  select x).FirstOrDefault();
                if (getTaskData != null)
                {
                    getTasassignkData.Remark = vmtask.Remark;
                    entities.SaveChanges();

                    if (getTaskData != null)
                    {
                        getTaskData.status = vmtask.status;
                        entities.SaveChanges();
                    }
                    if (vmtask.status.ToLower() == "Approved")
                    {
                        getTaskData.Isclose = true;
                        entities.SaveChanges();
                    }
                    BM_TaskTransaction bm_taskTransaction = new BM_TaskTransaction();
                    bm_taskTransaction.TaskID = vmtask.Id;
                    bm_taskTransaction.IsActive = true;
                    bm_taskTransaction.Status = vmtask.Remark;
                    bm_taskTransaction.StatusChangeOn = DateTime.Now;
                    bm_taskTransaction.UserID = vmtask.UserId;
                    bm_taskTransaction.RoleID = vmtask.Role;
                    bm_taskTransaction.CreatedOn = DateTime.Now;
                    bm_taskTransaction.CreatedBy = vmtask.UserId;
                    entities.BM_TaskTransaction.Add(bm_taskTransaction);
                   
                    entities.SaveChanges();
                    if (vmtask.files != null)
                    {
                        bool savechange = FileUpload.SaveTaskFile(vmtask.files, vmtask.CustomerId, vmtask.UserId, vmtask.Id);
                    }
                    vmtask.successMessage = true;
                    vmtask.successErrorMsg = "Saved successfully";
                }
                return vmtask;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                vmtask.errorMessage = true;
                vmtask.successErrorMsg = "Server error occurred";
                return vmtask;
            }
        }

        public bool ExportExcel(byte[] fileBytes)
        {
            HttpResponse response = HttpContext.Current.Response;
            response.Buffer = true;
            response.ClearContent();
            response.ClearHeaders();
            response.Clear();
            response.AddHeader("content-disposition", "attachment;filename=TaskReport.xlsx");
            response.Charset = "";
            response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            response.BinaryWrite(fileBytes);
            //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "TaskReport.xlsx");
            response.Flush(); // Sends all currently buffered output to the client.
            response.End();
            return true;
        }

        public List<BM_Task_SP_Status_Result> GetPerformerstatus(int customerId, int userId, int taskId)
        {
            try
            {
                List<BM_Task_SP_Status_Result> obj = new List<BM_Task_SP_Status_Result>();


                obj = (from row in entities.BM_Task_SP_Status(taskId) select row).ToList();
                return obj;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMTask GetTask(int taskId)
        {
            try
            {
                var getTask = (from row in entities.BM_Task
                               where row.ID == taskId
                               where row.IsActive == true
                               select new VMTask

                               {
                                   Id = row.ID,
                                   TaskTitle = row.TaskTitle,
                                   //DueDate = row.DueDate.ToString("dd/MM/yyyy"),
                                   DueDate = SqlFunctions.DateName("day", row.DueDate) + "/" + SqlFunctions.DateName("month", row.DueDate) + "/" + SqlFunctions.DateName("year", row.DueDate),
                                   Description = row.TaskDesc
                               }
                               ).FirstOrDefault();

                return getTask;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<VMTask> GetTaskType(int taskId, int userId)
        {
            try
            {

                var getTaskType = (from row in entities.BM_SP_Task(userId)
                                   where row.ID == taskId
                                   select new VMTask
                                   {

                                       TaskTypes = row.TaskType,
                                       TaskTypeDetails = (from x in entities.BM_Meetings where x.MeetingID == row.MeetingId select x.MeetingTitle).FirstOrDefault(),

                                   }).ToList();
                return getTaskType;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public TaskTypeVM getTaskTypeDetails(int taskId)
        {
            try
            {
                TaskTypeVM _tasktype = new TaskTypeVM();
                var objTask = (from row in entities.BM_Task where row.ID == taskId select row).FirstOrDefault();
                if (objTask != null)
                {
                    _tasktype.TaskId = objTask.ID;
                    if (objTask.TaskType == 1 && objTask.MeetingId > 0)
                    {
                        var getMeetingDetails = (from x in entities.BM_Meetings join y in entities.BM_CommitteeComp on x.MeetingTypeId equals y.Id where x.MeetingID == objTask.MeetingId select new { x, y }).FirstOrDefault();
                        if (getMeetingDetails != null)
                        {
                            _tasktype.TaskTypeNameforMeeting = "Meeting";
                            _tasktype.MeetingType = getMeetingDetails.y.MeetingTypeName;
                            _tasktype.MettingDate = getMeetingDetails.x.MeetingDate;
                            _tasktype.MeetingPurpose = getMeetingDetails.x.MeetingTitle;
                        }
                    }
                    else if (objTask.TaskType == 2 && objTask.MeetingId > 0 && objTask.AgendaId > 0)
                    {
                        _tasktype.TaskTypeNameforAgenda = "Agenda";
                        _tasktype.TaskTypeNameforMeeting = "Meeting";
                        var getMeetingDetails = (from x in entities.BM_Meetings join y in entities.BM_CommitteeComp on x.MeetingTypeId equals y.Id where x.MeetingID == objTask.MeetingId select new { x, y }).FirstOrDefault();
                        if (getMeetingDetails != null)
                        {
                            _tasktype.TaskTypeNameforMeeting = "Meeting";
                            _tasktype.MeetingType = getMeetingDetails.y.MeetingTypeName;
                            _tasktype.MettingDate = getMeetingDetails.x.MeetingDate;
                            _tasktype.MeetingPurpose = getMeetingDetails.x.MeetingTitle;
                        }
                        _tasktype.AgendaName = (from x in entities.BM_MeetingAgendaMapping where x.MeetingAgendaMappingID == objTask.AgendaId && x.MeetingID == objTask.MeetingId select x.AgendaItemText).FirstOrDefault();
                    }
                    else if (objTask.TaskType == 4)
                    {
                        _tasktype.TaskTypeName = "Other";
                    }

                }

                return _tasktype;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<string> GetTaskFile(long id)
        {
            try
            {
                var getFileName = (from row in entities.BM_FileData where row.TaskId == id && row.IsDeleted == false select row.FileName).ToList();
                return getFileName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<TaskFile> GetTaskFileDetails(long taskId, int userId, int customerId)
        {
            try
            {
                var gettaskFiledata = (from row in entities.BM_FileData
                                       where row.TaskId == taskId && row.IsDeleted == false
                                       select new TaskFile

                                       {
                                           FileId = row.Id,
                                           TaskId = row.TaskId,
                                           FileName = row.FileName
                                       }).ToList();
                return gettaskFiledata;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public bool DeleteTask(long fileId)
        {
            try
            {
                var deletefiledata = (from row in entities.BM_FileData where row.Id == fileId && row.IsDeleted == false select row).FirstOrDefault();

                if (deletefiledata != null)
                {
                    deletefiledata.IsDeleted = true;
                    entities.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string getAgendatitle(int? agendaId)
        {
            try
            {
                var getagendatitle = (from row in entities.BM_AgendaMaster where row.BM_AgendaMasterId == agendaId select row.Agenda).FirstOrDefault();
                return getagendatitle;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
    }
}