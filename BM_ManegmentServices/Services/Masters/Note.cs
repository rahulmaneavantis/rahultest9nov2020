﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{

    public class Note : INote
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public string AddNewNotes(VMNotes _objnotes, int customerId)
        {
            throw new NotImplementedException();
        }

        public VMNotes CreateNotes(VMNotes _objnotes)
        {
            try
            {
                var _objchecknotes = (from row in entities.BM_Notes
                                      where row.IsDeleted == false && row.Title == _objnotes.Title
                                      && row.Customer_Id == _objnotes.CustomerId

                                      select row).FirstOrDefault();

                if (_objchecknotes == null)
                {
                    BM_Notes _objnote = new BM_Notes();
                    _objnote.Customer_Id = _objnotes.CustomerId;
                    _objnote.UserID = _objnotes.UserId;
                    _objnote.Notes_details = _objnotes.NotesDetails;

                    _objnote.MeetingId = _objnotes.MeetingId;
                    _objnote.AgendaId = _objnotes.AgendaId;
                    _objnote.Title = _objnotes.Title;
                    _objnote.IsDeleted = false;
                    //_objnote.Entity_Id = 1;
                    _objnote.Created_by = _objnotes.UserId;
                    _objnote.Created_on = DateTime.Now;
                    entities.BM_Notes.Add(_objnote);
                    entities.SaveChanges();

                  if (_objnotes.DirectorId!=null && _objnote.Id > 0)
                    {
                        var checkNotesDetails = (from row in entities.BM_NotesUserMapping where row.NotesId == _objnote.Id && row.IsActive == true select row).FirstOrDefault();
                        if (checkNotesDetails != null)
                        {
                            checkNotesDetails.IsActive = false;
                            entities.SaveChanges();
                        }

                        foreach (var items in _objnotes.DirectorId)
                        {
                            var checkNotesDetailss = (from row in entities.BM_NotesUserMapping where row.NotesId == _objnote.Id && row.UserId == items select row).FirstOrDefault();
                            if (checkNotesDetailss == null)
                            {
                                BM_NotesUserMapping _objnotesusermapping = new BM_NotesUserMapping();
                                _objnotesusermapping.UserId = items;
                                _objnotesusermapping.NotesId = _objnote.Id;
                                _objnotesusermapping.IsActive = true;
                                _objnotesusermapping.Createdon = DateTime.Now;
                                _objnotesusermapping.Createdby = _objnotes.UserId;
                                entities.BM_NotesUserMapping.Add(_objnotesusermapping);
                                entities.SaveChanges();
                            }
                            else
                            {
                                checkNotesDetailss.UserId = items;
                                checkNotesDetailss.NotesId = _objnote.Id;
                                checkNotesDetailss.IsActive = true;
                                checkNotesDetailss.UpdatedOn = DateTime.Now;
                                checkNotesDetailss.Updatedby = _objnotes.UserId;
                                entities.SaveChanges();
                            }
                        }

                    }
                    _objnotes.Success = true;
                    _objnotes.Message = "Saved Successfully.";
                    return _objnotes;
                }
                else
                {
                    _objnotes.Error = true;
                    _objnotes.Message = "Data alrady exist";
                    return _objnotes;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objnotes.Error = true;
                _objnotes.Message = "Server error occured";
                return _objnotes;
            }
        }



        public string DeleteNotes(VMNotes _objnotes, int customerId)
        {
            try
            {

                if (_objnotes != null && _objnotes.Id > 0)
                {
                    var getnotesdata = (from row in entities.BM_Notes
                                        where row.Id == _objnotes.Id
                                        && row.IsDeleted == false
                                        select row).FirstOrDefault();
                    if (getnotesdata != null)
                    {
                        getnotesdata.IsDeleted = true;
                        entities.SaveChanges();
                        return "Deleted Successfully";

                    }
                    else
                    {
                        return "No data found";
                    }
                }
                else
                {
                    return "Somthing wents wrong";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "server error occured";
            }
        }

        public List<VMNotes> GetAllNotes(int customerId)
        {
            try
            {
                var _objgetnotes = (from row in entities.BM_Notes.Where(c => c.Customer_Id == customerId)
                                    join rows in entities.BM_NotesTypes on
                                    row.Notes_Type equals rows.Id
                                    where row.IsDeleted == false
                                    select new VMNotes
                                    {
                                        Id = row.Id,
                                        NotesDetails = row.Notes_details,
                                        Title = row.Title,
                                        type = rows.Name
                                    }).ToList();
                return _objgetnotes;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<BM_NotesTypes> GetAllNotesType(int customerId)
        {
            try
            {
                var getNotestype = (from row in entities.BM_NotesTypes
                                    select row).ToList();
                return getNotestype;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMNotes GetNotesbyId(long id, long meetingId, long agendaId)
        {
            try
            {
                var getNotes = (from row in entities.BM_Notes
                                where row.Id == id && row.MeetingId == meetingId && row.AgendaId == agendaId
                                select new VMNotes

                                {
                                    Id = row.Id,
                                    Title = row.Title,
                                    NotesDetails = row.Notes_details,
                                    MeetingId = meetingId,
                                    AgendaId = agendaId,
                                    DirectorId = (from x in entities.BM_NotesUserMapping where x.NotesId == row.Id && x.IsActive == true select x.UserId).ToList()

                                }).FirstOrDefault();
                return getNotes;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<VMNotes> GetsNotes(int userId, int customerId, long meetingId, long agendaId)
        {
            try
            {
                var getnotes = (from row in entities.BM_Notes
                                where row.Customer_Id == customerId && row.MeetingId == meetingId && row.UserID == userId && row.AgendaId == agendaId

                                select new VMNotes
                                {
                                    MeetingId = (long)row.MeetingId,
                                    AgendaId = (long)row.AgendaId,
                                    Title = row.Title,
                                    NotesDetails = row.Notes_details,
                                    Id = row.Id
                                }).ToList();

                return getnotes;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<VMNotes> GetsSharedNotes(int userId, int customerId, long meetingId)
        {
            try
            {
                var getsharedNotes = (from row in entities.BM_Notes
                                      join rows in entities.BM_NotesUserMapping
                                       on row.Id equals rows.NotesId
                                      where row.MeetingId == meetingId && rows.UserId == userId
                                      select new VMNotes
                                      {
                                          AgendaName = (from a in entities.BM_MeetingAgendaMapping where a.MeetingID == row.MeetingId && a.AgendaID == row.AgendaId select a.AgendaItemText).FirstOrDefault(),
                                          Title = row.Title,
                                          NotesDetails = row.Notes_details,
                                          userName = (from U in entities.Users where U.ID == rows.Createdby select U.FirstName +" "+U.LastName).FirstOrDefault()
                                      }
                                    ).ToList();
                return getsharedNotes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public VMNotes UpdateNotes(VMNotes _objnotes)
        {
            try
            {
                if (_objnotes != null && _objnotes.Id > 0)
                {
                    var _objgetnotes = (from row in entities.BM_Notes
                                        where row.Id == _objnotes.Id
                                        && row.IsDeleted == false
                                        select row
                                      ).FirstOrDefault();

                    if (_objgetnotes != null)
                    {
                        _objgetnotes.Notes_details = _objnotes.NotesDetails;
                        //_objgetnotes.Notes_Type = _objnotes.Notestype;
                        _objgetnotes.Title = _objnotes.Title;
                        _objgetnotes.Updated_on = DateTime.Now;
                        _objgetnotes.Updated_by = _objnotes.UserId;
                        entities.SaveChanges();
                        if (_objnotes.DirectorId.Count > 0 && _objgetnotes.Id > 0)
                        {
                            var checkNotesDetails = (from row in entities.BM_NotesUserMapping where row.NotesId == _objgetnotes.Id && row.IsActive == true select row).ToList();
                            if (checkNotesDetails.Count > 0)
                            {
                                foreach (var items in checkNotesDetails)
                                {

                                    items.IsActive = false;
                                    entities.SaveChanges();
                                }
                            }
                            foreach (var items in _objnotes.DirectorId)
                            {
                                var checkNotesDetailss = (from row in entities.BM_NotesUserMapping where row.NotesId == _objgetnotes.Id && row.UserId == items select row).FirstOrDefault();
                                if (checkNotesDetailss == null)
                                {
                                    BM_NotesUserMapping _objnotesusermapping = new BM_NotesUserMapping();
                                    _objnotesusermapping.UserId = items;
                                    _objnotesusermapping.NotesId = _objgetnotes.Id;
                                    _objnotesusermapping.IsActive = true;
                                    _objnotesusermapping.Createdon = DateTime.Now;
                                    _objnotesusermapping.Createdby = _objnotes.UserId;
                                    entities.BM_NotesUserMapping.Add(_objnotesusermapping);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkNotesDetailss.UserId = items;
                                    checkNotesDetailss.NotesId = _objgetnotes.Id;
                                    checkNotesDetailss.IsActive = true;
                                    checkNotesDetailss.UpdatedOn = DateTime.Now;
                                    checkNotesDetailss.Updatedby = _objnotes.UserId;
                                    entities.SaveChanges();
                                }
                            }
                        }
                        _objnotes.Success = true;
                        _objnotes.Message = "Updated Successfully.";
                        return _objnotes;
                    }
                    else
                    {

                        _objnotes.Error = true;
                        _objnotes.Message = "No Data found";
                        return _objnotes;
                    }
                }
                else
                {
                    _objnotes.Error = true;
                    _objnotes.Message = "Somthing wents wrong";
                    return _objnotes;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objnotes.Error = true;
                _objnotes.Message = "Server error occured";
                return _objnotes;
            }
        }
    }
}