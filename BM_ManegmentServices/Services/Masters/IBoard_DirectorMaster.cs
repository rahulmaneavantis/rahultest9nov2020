﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;
using static BM_ManegmentServices.VM.VMBoard_Director;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IBoard_DirectorMaster
    {
        List<Director_Age> GetAllDirectorAge(int customerId, int entityType);
        List<Director_Rule> GetAllDirectorDiscription(int customerId, int entityType);
        VMBoard_Director Update_DirectorsRules(VMBoard_Director _objBoardDirector);
      
        VMBoard_Director GetDirector(int id, int entity_Type, int designationId);
        VMBoard_Director Create_DirectorsRules(VMBoard_Director _objBoardDirector, int customerId);
        VMBoard_Director Update_DirectorAge(VMBoard_Director _objBoardDirector, int customerId);
        VMBoard_Director Create_DirectorAge(VMBoard_Director _objBoardDirector, int customerId);
        VMBoard_Director GetDirectorAge(int id, int entity_Type);
        int GetEntityType(int entityId,int CustomerId);
    }
}
