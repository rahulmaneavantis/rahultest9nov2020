﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.MyMeetings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.MyMeetings
{
    public interface IMyMeetingsService
    {
        #region Agenda Comments
        MeetingAgendaCommentVM SaveAgendaComments(MeetingAgendaCommentVM obj, int userId);
        MeetingAgendaCommentVM GetAgendaComments(long draftCirculationID);
        List<MeetingAgendaCommentListVM> GetParticipantAgendaComments(long meetingId);
        #endregion

        #region Draft Comments
        MeetingMinutesCommentVM SaveComments(MeetingMinutesCommentVM obj, int userId);
        MeetingMinutesCommentVM GetComments(long draftCirculationID);
        List<MeetingMinutesCommentListVM> GetParticipantComments(long meetingId);
        #endregion

        #region Agenda List of Concluded Meetings
        List<ConcludedMeetingAgendaItemVM> ConcludedMeetingAgenda(int customerId, int userId);
        List<ConcludedMeetingAgendaItemVM> ConcludedMeetingAgenda_CS(int customerId, int userId, string role);
        #endregion
    }
}
