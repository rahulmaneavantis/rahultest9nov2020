﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using BM_ManegmentServices.VM.MyMeetings;
using BM_ManegmentServices.Data;

namespace BM_ManegmentServices.Services.MyMeetings
{
    public class MyMeetingsService : IMyMeetingsService
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        #region Agenda Comments
        public MeetingAgendaCommentVM SaveAgendaComments(MeetingAgendaCommentVM obj, int userId)
        {
            try
            {
                var _obj = (from row in entities.BM_MeetingParticipant
                            where row.MeetingParticipantId == obj.ParticipantId
                            select row).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.AgendaComments = obj.CommentData;
                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                    obj.Success = true;
                    obj.Message = "Saved Successfully.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingAgendaCommentVM GetAgendaComments(long participantId)
        {
            var obj = new MeetingAgendaCommentVM();
            try
            {
                var _obj = (from row in entities.BM_MeetingParticipant
                            where row.MeetingParticipantId == participantId
                            select row).FirstOrDefault();

                if (_obj != null)
                {
                    obj.CommentData = _obj.AgendaComments;
                    obj.Success = true;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public List<MeetingAgendaCommentListVM> GetParticipantAgendaComments(long meetingId)
        {
            try
            {
                return (from participant in entities.BM_MeetingParticipant
                        join director in entities.BM_DirectorMaster on participant.Director_Id equals director.Id
                        where participant.Meeting_ID == meetingId && participant.AgendaComments != null
                        select new MeetingAgendaCommentListVM
                        {
                            Salutaion = director.Salutation,
                            FirstName = director.FirstName,
                            LastName = director.LastName,

                            MeetingId = participant.Meeting_ID,
                            ParticipantId = participant.MeetingParticipantId,

                            CommentData = participant.AgendaComments
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Draft Comments
        public MeetingMinutesCommentVM SaveComments(MeetingMinutesCommentVM obj, int userId)
        {
            try
            {
                var _obj = (from row in entities.BM_MeetingMinutesDetailsTransaction
                            where row.ID == obj.DraftCirculationID
                            select row).FirstOrDefault();

                if (_obj != null)
                {
                    _obj.DraftComments = obj.CommentData;
                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                    obj.Success = true;
                    obj.Message = "Saved Successfully.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingMinutesCommentVM GetComments(long draftCirculationID)
        {
            var obj = new MeetingMinutesCommentVM();
            try
            {
                var _obj = (from row in entities.BM_MeetingMinutesDetailsTransaction
                            where row.ID == draftCirculationID
                            select row).FirstOrDefault();

                if (_obj != null)
                {
                    obj.CommentData = _obj.DraftComments;
                    obj.Success = true;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public List<MeetingMinutesCommentListVM> GetParticipantComments(long meetingId)
        {
            try
            {
                return (from minutesDetails in entities.BM_MeetingMinutesDetails
                        join comment in entities.BM_MeetingMinutesDetailsTransaction on minutesDetails.Id equals comment.MinutesDetailsID
                        join participant in entities.BM_MeetingParticipant on comment.MeetingParticipantId equals participant.MeetingParticipantId
                        join director in entities.BM_DirectorMaster on participant.Director_Id equals director.Id
                        where minutesDetails.MeetingId == meetingId && comment.DraftComments != null
                        select new MeetingMinutesCommentListVM
                        {
                            Salutaion = director.Salutation,
                            FirstName = director.FirstName,
                            LastName = director.LastName,

                            MeetingId = minutesDetails.MeetingId,
                            DraftCirculationID = comment.ID,

                            CommentData = comment.DraftComments
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Agenda List of Concluded Meetings
        public List<ConcludedMeetingAgendaItemVM> ConcludedMeetingAgenda(int customerId, int userId)
        {
            try
            {
                return (from row in entities.BM_SP_MeetingConcludeAgendaList(customerId, userId)
                        select new ConcludedMeetingAgendaItemVM
                        {
                            //MeetingTypeId_ =
                            EntityId = row.EntityId,
                            CompanyName = row.CompanyName,

                            MeetingTypeId_ = row.MeetingTypeId,
                            MeetingTypeName = row.MeetingTypeName,

                            FY = row.FY,
                            FYText = row.FYText,

                            Meeting_Id = row.MeetingID,
                            MeetingID = row.MeetingID,
                            MeetingTitle = row.Meeting,
                            MeetingDate = row.MeetingDate,

                            MeetingAgendaMappingID = row.MeetingAgendaMappingID,
                            AgendaID = row.AgendaId,
                            AgendaItemText = row.AgendaItem,
                            ResultRemark = row.ResultRemark,
                            HasCompliance = row.HasCompliance,
                            HasInfo = row.HasInfo

                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<ConcludedMeetingAgendaItemVM> ConcludedMeetingAgenda_CS(int customerId, int userId, string role)
        {
            try
            {
                return (from row in entities.BM_SP_MeetingConcludeAgendaList_CS(customerId, userId, role)
                        select new ConcludedMeetingAgendaItemVM
                        {
                            //MeetingTypeId_ =
                            EntityId = row.EntityId,
                            CompanyName = row.CompanyName,

                            MeetingTypeId_ = row.MeetingTypeId,
                            MeetingTypeName = row.MeetingTypeName,

                            FY = row.FY,
                            FYText = row.FYText,

                            Meeting_Id = row.MeetingID,
                            MeetingID = row.MeetingID,
                            MeetingTitle = row.MeetingTitle,
                            MeetingDate = row.MeetingDate,

                            MeetingAgendaMappingID = row.MeetingAgendaMappingID,
                            AgendaID = (long) row.AgendaId,
                            AgendaItemText = row.AgendaItem,
                            ResultRemark = row.ResultRemark,
                            HasCompliance = row.HasCompliance,
                            HasInfo = row.HasInfo

                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

    }
}