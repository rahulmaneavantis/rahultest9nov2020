﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.DocumentManagenemt
{
    public interface IFileData_Service
    {
        int GetFileVersion(long scheduleOnID, string fileName);
        FileDataVM Save(FileDataVM obj, int createdBy);

        Message CreateFileDataMapping(long transactionID, long scheduleOnID, long fileID, Int16 fileType);

        FileDataVM GetFile(long fileID, int userId);
    }
}
