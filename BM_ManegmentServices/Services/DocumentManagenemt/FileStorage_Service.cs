﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.DocumentManagenemt
{
    public class FileStorage_Service : IFileStorage_Service
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public FileStorageDataVM UploadFile(FileStorageDataVM obj, long meetingId, int customerId)
        {
            try
            {
                string fileKey = Convert.ToString(Guid.NewGuid());
                string fileExtension = Path.GetExtension(obj.Files.FileName);
                string fileName = obj.Files.FileName;
                string fileNameNew = fileKey + fileExtension;

                Stream fileStream = obj.Files.InputStream;
                using (fileStream)
                {

                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    var directoryPath = "testsecretarial/" + customerId + "/"+ meetingId;
                    CloudBlobContainer container = blobClient.GetContainerReference(directoryPath);
                    container.CreateIfNotExistsAsync();

                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileNameNew);
                    blockBlob.UploadFromStream(fileStream);

                    BM_FileData _obj = new BM_FileData()
                    {
                        FilePath = directoryPath,
                        FileKey = fileKey,
                        FileName = fileName,
                        Version = "1",
                        IsDeleted = false,
                        EnType = "",
                        OnFileStorage = true,
                        MeetingId = obj.MeetingId,
                        VersionDate = DateTime.Now,
                        VersionComment = "",
                        UploadedBy = 1,
                        UploadedOn = DateTime.Now
                    };

                    _obj.FileSize = Convert.ToString(obj.Files.ContentLength);
                    entities.BM_FileData.Add(_obj);
                    entities.SaveChanges();

                    obj.Success = true;
                    obj.Message = "Uploaded successfully.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public FileDataVM GetFileDetails(long fileId)
        {
            var obj = new FileDataVM();
            try
            {
                obj = (from row in entities.BM_FileData
                       where row.Id == fileId && row.IsDeleted == false
                       select new FileDataVM
                       {
                           FileID = row.Id,
                           FileName = row.FileName,
                           FilePath = row.FilePath,
                           FileKey = row.FileKey,
                           Version = row.Version
                       }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public List<FileDataVM> GetUploadedFilesByMeetingId(long meetingId)
        {
            var result = new List<FileDataVM>();
            try
            {
                result = (from row in entities.BM_FileData
                           where row.MeetingId == meetingId && row.OnFileStorage == true && row.IsDeleted == false
                           select new FileDataVM
                           {
                               FileID = row.Id,
                               FileName = row.FileName,
                               FilePath = row.FilePath,
                               Version = row.Version
                           }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
    }
}