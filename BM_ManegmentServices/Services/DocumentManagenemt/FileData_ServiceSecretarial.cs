﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.DocumentManagenemt
{
    public class FileData_ServiceSecretarial : IFileData_Service
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public int GetFileVersion(long scheduleOnID, string fileName)
        {
            int version = 0;
            try
            {
                var result = (from row in entities.BM_FileDataMapping
                              join fileData in entities.BM_FileData on row.FileID equals fileData.Id
                              orderby row.FileDataMappingID descending
                              where row.ScheduledOnID == scheduleOnID && fileData.FileName == fileName
                              select fileData.Version).FirstOrDefault();

                int.TryParse(result, out version);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return version;
        }

        //Generate Key for FileData if Key is Empty
        public FileDataVM Save(FileDataVM obj, int userId)
        {
            try
            {
                if (string.IsNullOrEmpty(obj.FileKey))
                {
                    obj.FileKey = Convert.ToString(Guid.NewGuid());
                }

                string path = System.Web.Hosting.HostingEnvironment.MapPath(obj.FilePath);
                string fileExtension = Path.GetExtension(obj.FileName);
                string fileName = obj.FileKey + fileExtension;
                string filePath = Path.Combine(path, fileName);

                if (!Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);

                SaveDocFile(filePath, obj.FileData);

                if (File.Exists(filePath))
                {
                    BM_FileData _obj = new BM_FileData()
                    {
                        FilePath = obj.FilePath,
                        FileKey = obj.FileKey,
                        FileName = obj.FileName,
                        FileSize = Convert.ToString(obj.FileData.LongLength),
                        Version = obj.Version,
                        IsDeleted = false,
                        UploadedBy = userId,
                        UploadedOn = DateTime.Now
                    };

                    entities.BM_FileData.Add(_obj);
                    entities.SaveChanges();
                    obj.FileID = _obj.Id;
                }
                obj.Success = true;
                obj.Message = "Saved successfully";
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public Message CreateFileDataMapping(long transactionID, long scheduleOnID, long fileID, Int16 fileType)
        {
            var obj = new Message();
            try
            {
                var _obj = new BM_FileDataMapping()
                {
                    TransactionID = transactionID,
                    ScheduledOnID = scheduleOnID,
                    FileID = fileID,
                    FileType = fileType
                };
                entities.BM_FileDataMapping.Add(_obj);
                entities.SaveChanges();
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public FileDataVM GetFile(long fileID, int userId)
        {
            var obj = new FileDataVM();
            try
            {
                var result = (from row in entities.BM_FileData
                              where row.Id == fileID
                              select new FileDataVM
                              {
                                  FileName = row.FileName,
                                  FileKey = row.FileKey,
                                  FilePath = row.FilePath,
                                  Version = row.Version
                              }).FirstOrDefault();

                if (result != null)
                {
                    string path = System.Web.Hosting.HostingEnvironment.MapPath(result.FilePath);
                    string fileExtension = Path.GetExtension(result.FileName);
                    string fileName = result.FileKey + fileExtension;

                    string filePath = Path.Combine(path, fileName);

                    if (File.Exists(filePath))
                    {
                        obj.FileKey = result.FileKey;
                        obj.FileName = result.FileName;
                        //obj.FileData = ReadDocFiles(filePath);
                        obj.FileData = CryptographyHandler.AESDecrypt(ReadDocFiles(filePath));
                        obj.Success = true;
                    }
                    else
                    {
                        obj.Error = true;
                    }
                }
                else
                {
                    obj.Error = true;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        void SaveDocFile(string fileName, Byte[] data)
        {
            using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(fileName)))
            {
                // Writer raw data                
                Writer.Write(CryptographyHandler.AESEncrypt(data));
                //Writer.Write(data);
                Writer.Flush();
                Writer.Close();
            }
        }
        void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    SaveDocFile(file.Key, file.Value);
                }
            }
        }
        public static byte[] ReadDocFiles(string filePath)
        {
            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int)fs.Length;
                byte[] buffer;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                return buffer;
            }
        }

    }
}