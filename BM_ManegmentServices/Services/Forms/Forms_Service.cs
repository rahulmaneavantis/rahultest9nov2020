﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Forms
{
    public class Forms_Service : IForms_Service
    {
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public List<FormsVM> GetMeetingFormList(long meetingId, long meetingAgendaMappingId, long complianceId)
        {
            try
            {
                var result = (from mapping in entities.BM_MeetingFormMapping
                              join forms in entities.BM_FormMaster on mapping.FormID equals forms.FormID
                              join agendaMapping in entities.BM_MeetingAgendaMapping on mapping.MeetingAgendaMappingId equals agendaMapping.MeetingAgendaMappingID
                              where mapping.MeetingId == meetingId && mapping.IsDeleted == false && agendaMapping.IsDeleted == false && agendaMapping.IsActive == true
                              select new FormsVM
                              {
                                  MeetingFormMappingId = mapping.MeetingFormMappingID,
                                  MeetingId = meetingId,
                                  MeetingAgendaMappingId = mapping.MeetingAgendaMappingId,
                                  ComplianceId = (long)mapping.ComplianceId,

                                  FormId = (long)mapping.FormID,
                                  EForm = forms.EForm,
                                  FormName = mapping.FormName,
                                  IsEForm = forms.IsEForm,
                                  CanGenerate = agendaMapping.Result == SecretarialConst.AgendaResult.APPROVED ? true : false,
                                  ForMultipleEvents = forms.ForMultipleEvents,
                                  EventCount = 1
                              });

                if (meetingAgendaMappingId > 0)
                {
                    result = (from row in result
                              where row.MeetingAgendaMappingId == meetingAgendaMappingId
                              select row);
                }

                if (complianceId > 0)
                {
                    result = (from row in result
                              where row.ComplianceId == complianceId
                              select row);
                }

                var lst = result.ToList();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.ForMultipleEvents == true)
                        {
                            item.EventCount = (from row in entities.BM_ComplianceScheduleOnNew
                                               where row.MeetingID == item.MeetingId && row.ComplianceID == item.ComplianceId && row.IsActive == true && row.IsDeleted == false
                                               select row.ComplianceID
                                               ).Count();

                            if (item.EForm == "MGT-14" && item.EventCount > 1)
                            {
                                //item.EventMessage = "The form will be generated for " + item.EventCount + " resolution(s).";
                                item.EventMessage = "After generating the form type " + item.EventCount + " instead of 10 in point Number 6.";
                            }
                            else if (item.EForm == "DIR-12New" && item.EventCount > 1)
                            {
                                var directorCount = 0; var kmpCount = 0;
                                var lstUIForms = (from row in entities.BM_ComplianceScheduleOnNew
                                                  join agenda in entities.BM_AgendaMaster on row.AgendaMasterID equals agenda.BM_AgendaMasterId
                                                  join forms in entities.BM_UIForm on agenda.UIFormID equals forms.UIFormID
                                                  where row.MeetingID == item.MeetingId && row.ComplianceID == item.ComplianceId && row.IsActive == true && row.IsDeleted == false
                                                  select new
                                                  {
                                                      row.ComplianceID,
                                                      agenda.UIFormID
                                                  }
                                               ).ToList();
                                if (lstUIForms.Count > 0)
                                {
                                    directorCount = lstUIForms.Where(k => k.UIFormID > 10).Select(k => k.UIFormID).Count();
                                    kmpCount = lstUIForms.Where(k => k.UIFormID < 10 && k.UIFormID > 0).Select(k => k.UIFormID).Count();

                                    item.EventMessage = "The form will be generated for";
                                    if (directorCount > 0)
                                    {
                                        item.EventMessage += " " + directorCount + " MD/Director(s)";
                                    }

                                    if (kmpCount > 0)
                                    {
                                        if (directorCount > 0)
                                        {
                                            item.EventMessage += " &";
                                        }

                                        item.EventMessage += " " + kmpCount + "  Manager/CFO/CEO/CS.";
                                    }
                                }
                            }
                        }
                    }
                }
                return lst;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public string GenerateMeetingFormData(long meetingAgendaMappingId, long formMappingId)
        {
            try
            {
                return (from row in entities.BM_SP_MeetingFormGenerate(meetingAgendaMappingId, formMappingId) select row.FormFormat).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #region Generate E-Forms
        public EFormVM GenerateForm(long meetingFormMappingId, long meetingId)
        {
            var result = new EFormVM() { MeetingFormMappingId = meetingFormMappingId };
            var lstFields = new List<EFormFieldVM>();
            try
            {
                var obj = (from formMapping in entities.BM_MeetingFormMapping
                           join forms in entities.BM_FormMaster on formMapping.FormID equals forms.FormID
                           join agendaMapping in entities.BM_MeetingAgendaMapping on formMapping.MeetingAgendaMappingId equals agendaMapping.MeetingAgendaMappingID
                           join agenda in entities.BM_AgendaMaster on agendaMapping.AgendaID equals agenda.BM_AgendaMasterId
                           join meeting in entities.BM_Meetings on agendaMapping.MeetingID equals meeting.MeetingID
                           join entity in entities.BM_EntityMaster on meeting.EntityId equals entity.Id
                           where formMapping.MeetingFormMappingID == meetingFormMappingId
                           select new
                           {
                               entity.CIN_LLPIN,
                               meeting.MeetingDate,
                               meeting.SendDateNotice,

                               forms.EForm,
                               forms.FormPath,

                               agenda.UIFormID,
                               agendaMapping.RefMasterID,

                               agendaMapping.MeetingAgendaMappingID
                           }).FirstOrDefault();


                //result.FullFileName = "~/Areas/BM_Management/Documents/Forms/Form_DIR-12.pdf";
                //result.FormName = "DIR-12.pdf";

                if (obj != null)
                {
                    if (obj.RefMasterID > 0)
                    {
                        result.FullFileName = obj.FormPath;
                        result.FormName = obj.EForm + ".pdf";
                        if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(result.FullFileName)))
                        {
                            switch (obj.UIFormID)
                            {
                                case SecretarialConst.UIForms.APPOINTMENT_OF_CFO:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_CS:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_CEO:
                                case SecretarialConst.UIForms.RESIGNATION_OF_CFO:
                                case SecretarialConst.UIForms.RESIGNATION_OF_CS:
                                case SecretarialConst.UIForms.RESIGNATION_OF_CEO:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER:
                                    #region E-forms
                                    if (obj.EForm == "DIR-12")
                                    {
                                        #region DIR-12
                                        var cfo = (from row in entities.BM_DirectorMaster
                                                   where row.Id == (long)obj.RefMasterID
                                                   select new
                                                   {
                                                       row.FirstName,
                                                       row.LastName,
                                                       row.MiddleName,

                                                       row.Father,
                                                       row.FatherMiddleName,
                                                       row.FatherLastName,

                                                       row.EmailId_Personal,
                                                       row.PAN,
                                                       row.Present_Address_Line1,
                                                       row.Present_Address_Line2,
                                                       row.Present_PINCode,
                                                       row.MobileNo,

                                                       row.DesignationId,
                                                       row.CS_MemberNo,
                                                       row.Kmp_appointment_Date,
                                                       row.cessation_Date,

                                                       row.Present_CityId

                                                   }).FirstOrDefault();

                                        if (cfo != null)
                                        {
                                            result.FormName = "DIR-12_" + (string.IsNullOrEmpty(cfo.FirstName) ? "" : cfo.FirstName) + (string.IsNullOrEmpty(cfo.LastName) ? "" : "_" + cfo.LastName) + ".pdf";

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DIR_12[0].SRN_2_1_OR_CIN[0]", Value = obj.CIN_LLPIN });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DIR_12[0].NUM_M_S_CFO_CEO[0]", Value = "4" });

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].PAN[0]", Value = cfo.PAN });
                                            if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CFO || obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CS || obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CEO || obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].RB_A_C[0]", Value = "APPN" });
                                                if (cfo.Kmp_appointment_Date != null)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                                }
                                            }
                                            else
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].RB_A_C[0]", Value = "RESG" });
                                                if (cfo.cessation_Date != null)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.cessation_Date).ToString("yyyy-MM-dd") });
                                                }
                                            }
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].FIRST_NAME[0]", Value = cfo.FirstName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].MIDDLE_NAME[0]", Value = cfo.MiddleName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].LAST_NAME[0]", Value = cfo.LastName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].FIRST_NAME1[0]", Value = cfo.Father });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].MIDDLE_NAME1[0]", Value = cfo.FatherMiddleName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].LAST_NAME1[0]", Value = cfo.FatherLastName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].ADDRESS_LINE1[0]", Value = cfo.Present_Address_Line1 });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].ADDRESS_LINE2[0]", Value = cfo.Present_Address_Line2 });
                                            if (cfo.Present_CityId > 0)
                                            {
                                                var cityName = (from city in entities.Cities
                                                                where city.ID == cfo.Present_CityId
                                                                select city.Name).FirstOrDefault();
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].CITY[0]", Value = cityName });
                                            }

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].STATE[0]", Value = "MH" });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].PIN_CODE[0]", Value = cfo.Present_PINCode });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].ISO_COUNTRY_CODE[0]", Value = "IN" });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].PHONE[0]", Value = cfo.MobileNo });
                                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DATE_OF_BIRTH[0]", Value = "" });

                                            switch (cfo.DesignationId)
                                            {
                                                case 2:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DESIGNATION2[0]", Value = "MNGR" });
                                                    break;
                                                case 3:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DESIGNATION2[0]", Value = "SCTY" });
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].MEMBERSHIP_NUM[0]", Value = cfo.CS_MemberNo });
                                                    break;
                                                case 4:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DESIGNATION2[0]", Value = "CEO" });
                                                    break;
                                                case 5:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DESIGNATION2[0]", Value = "CFO" });
                                                    break;
                                                default:
                                                    break;
                                            }


                                            //if (cfo.Kmp_appointment_Date != null)
                                            //{
                                            //    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                            //}
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].EMAIL_ID[0]", Value = cfo.EmailId_Personal });
                                            //lstFields.Add(new EFormFieldVM() { Name = "", Value = "" });
                                        }
                                        #endregion
                                    }
                                    else if (obj.EForm == "MGT-14")
                                    {
                                        #region MGT-14
                                        var agendaDetails = (from row in entities.BM_SP_GetAgendaTemplate(obj.MeetingAgendaMappingID, null)
                                                             select row).FirstOrDefault();

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].CIN[0]", Value = obj.CIN_LLPIN });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].CB1_REGISTRATION[0]", Value = "RESL" });
                                        if (obj.SendDateNotice != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].RESOLUTION[0]", Value = Convert.ToDateTime(obj.SendDateNotice).ToString("yyyy-MM-dd") });
                                        }
                                        if (obj.MeetingDate != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].RESOLUTION1[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                                        }

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].NO_OF_RESOLUTION[0]", Value = "1" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].SECTION_PASSED[0]", Value = "179(3)" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].SECTION_PASSED1[0]", Value = "" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].PASSING_RESLTN[0]", Value = "C1" });

                                        if (agendaDetails != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].SUBJECT_RES[0]", Value = System.Text.RegularExpressions.Regex.Replace(agendaDetails.MinutesFormatHeading, "<.*?>", String.Empty) });
                                        }

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].AUTHRTY_PASSING[0]", Value = "BDIR" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].RB_RESOLUTION[0]", Value = "WRQM" });

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].AUTHORIZED[0]", Value = "12" }); //Director
                                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DATE_DECLARATION[0]", Value = "" }); // Date of Declaration
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DESIGNATION[0]", Value = "DIRT" }); //Director
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DIN_PAN[0]", Value = "" }); //DIN
                                        #endregion
                                    }
                                    else if (obj.EForm == "MR-1")
                                    {
                                        #region MR-1
                                        var cfo = (from row in entities.BM_DirectorMaster
                                                   where row.Id == (long)obj.RefMasterID
                                                   select new
                                                   {
                                                       row.FirstName,
                                                       row.LastName,
                                                       row.MiddleName,

                                                       row.Father,
                                                       row.FatherMiddleName,
                                                       row.FatherLastName,

                                                       row.EmailId_Personal,
                                                       row.PAN,
                                                       row.Present_Address_Line1,
                                                       row.Present_Address_Line2,
                                                       row.Present_PINCode,
                                                       row.MobileNo,

                                                       row.DesignationId,
                                                       row.CS_MemberNo,
                                                       row.Kmp_appointment_Date,
                                                       row.cessation_Date,

                                                       row.Present_CityId

                                                   }).FirstOrDefault();
                                        if (cfo != null)
                                        {
                                            var fullName = (string.IsNullOrEmpty(cfo.FirstName) ? "" : cfo.FirstName) + (string.IsNullOrEmpty(cfo.MiddleName) ? "" : " " + cfo.MiddleName) + (string.IsNullOrEmpty(cfo.LastName) ? "" : " " + cfo.LastName);


                                            result.FormName = "MR-1_" + (string.IsNullOrEmpty(fullName) ? "" : fullName.Replace(" ", "_")) + ".pdf";

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].CIN[0]", Value = obj.CIN_LLPIN });

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DIN[0]", Value = cfo.PAN });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].NAME[0]", Value = fullName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DESIGNATION_OPT[0]", Value = "MANG" });

                                            if (obj.MeetingDate != null)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RESOLUTION_DATE[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                                            }

                                            if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER)
                                            {
                                                if (cfo.Kmp_appointment_Date != null)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].EFFECT_DATE_APP[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].FROM_DATE[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                                }

                                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].TO_DATE[0]", Value = "" });
                                            }


                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RB_AGE_APPOINTEE[0]", Value = "NO" });
                                        }
                                        #endregion
                                    }
                                    #endregion
                                    break;

                                default:
                                    break;
                            }
                            SetData(result, lstFields);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public EFormVM GenerateForms(long meetingFormMappingId, long meetingId)
        {
            var result = new EFormVM() { MeetingFormMappingId = meetingFormMappingId };
            var lstFields = new List<EFormFieldVM>();
            var lstFormsVM = new List<MultipleFormsVM>();

            try
            {
                var obj = (from formMapping in entities.BM_MeetingFormMapping
                           join forms in entities.BM_FormMaster on formMapping.FormID equals forms.FormID
                           join agendaMapping in entities.BM_MeetingAgendaMapping on formMapping.MeetingAgendaMappingId equals agendaMapping.MeetingAgendaMappingID
                           join agenda in entities.BM_AgendaMaster on agendaMapping.AgendaID equals agenda.BM_AgendaMasterId
                           join meeting in entities.BM_Meetings on agendaMapping.MeetingID equals meeting.MeetingID
                           join entity in entities.BM_EntityMaster on meeting.EntityId equals entity.Id
                           where formMapping.MeetingFormMappingID == meetingFormMappingId
                           select new
                           {
                               entity.CIN_LLPIN,
                               meeting.MeetingDate,
                               meeting.SendDateNotice,
                               meeting.MeetingTypeId,

                               forms.FormID,
                               forms.EForm,
                               forms.ForMultipleEvents,
                               forms.FormPath,
                               forms.MultipleFormPath,

                               agenda.UIFormID,
                               agendaMapping.RefMasterID,

                               agendaMapping.MeetingID,
                               agendaMapping.MeetingAgendaMappingID,

                               formMapping.MeetingFormMappingID,
                               formMapping.ComplianceId
                           }).FirstOrDefault();

                //result.FullFileName = "~/Areas/BM_Management/Documents/Forms/Form_DIR-12.pdf";
                //result.FormName = "DIR-12.pdf";

                if (obj != null)
                {
                    if (obj.ForMultipleEvents == true)
                    {
                        lstFormsVM = (from formMapping in entities.BM_MeetingFormMapping
                                      join forms in entities.BM_FormMaster on formMapping.FormID equals forms.FormID
                                      join agendaMapping in entities.BM_MeetingAgendaMapping on formMapping.MeetingAgendaMappingId equals agendaMapping.MeetingAgendaMappingID
                                      join agenda in entities.BM_AgendaMaster on agendaMapping.AgendaID equals agenda.BM_AgendaMasterId
                                      where formMapping.MeetingId == obj.MeetingID && formMapping.FormID == obj.FormID && formMapping.ComplianceId == obj.ComplianceId &&
                                      agendaMapping.IsActive == true && agendaMapping.IsDeleted == false
                                      select new MultipleFormsVM
                                      {
                                          //forms.EForm,
                                          //forms.ForMultipleEvents,
                                          //forms.FormPath,
                                          //forms.MultipleFormPath,
                                          UIFormID = agenda.UIFormID,
                                          RefMasterID = agendaMapping.RefMasterID,

                                          MeetingId = agendaMapping.MeetingID,
                                          MeetingAgendaMappingId = agendaMapping.MeetingAgendaMappingID,

                                          MeetingFormMappingId = formMapping.MeetingFormMappingID
                                      }).ToList();
                    }
                    else
                    {
                        lstFormsVM.Add(new MultipleFormsVM()
                        {
                            MeetingId = obj.MeetingID,
                            MeetingAgendaMappingId = obj.MeetingAgendaMappingID,
                            MeetingFormMappingId = obj.MeetingFormMappingID,
                            RefMasterID = obj.RefMasterID,
                            UIFormID = obj.UIFormID
                        });
                    }

                    result.FormName = obj.EForm + ".pdf";

                    var index = -1;
                    var indexDirector = -1;
                    if (obj.EForm == "DIR-12")
                    {
                        #region DIR-12
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DIR_12[0].SRN_2_1_OR_CIN[0]", Value = obj.CIN_LLPIN });
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DIR_12[0].NUM_M_S_CFO_CEO[0]", Value = "4" });

                        foreach (var item in lstFormsVM)
                        {
                            switch (item.UIFormID)
                            {
                                #region KMP
                                case SecretarialConst.UIForms.APPOINTMENT_OF_CFO:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_CS:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_CEO:
                                case SecretarialConst.UIForms.RESIGNATION_OF_CFO:
                                case SecretarialConst.UIForms.RESIGNATION_OF_CS:
                                case SecretarialConst.UIForms.RESIGNATION_OF_CEO:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER:
                                case SecretarialConst.UIForms.RESIGNATION_OF_MANAGER:

                                    var cfo = (from details in entities.BM_Directors_DetailsOfInterest
                                                    join row in entities.BM_DirectorMaster on details.Director_Id equals row.Id
                                                    where details.Id == (long)item.RefMasterID
                                                    select new
                                                    {
                                                        row.Id,
                                                        row.DIN,
                                                        row.FirstName,
                                                        row.LastName,
                                                        row.MiddleName,

                                                        row.Father,
                                                        row.FatherMiddleName,
                                                        row.FatherLastName,

                                                        row.EmailId_Personal,
                                                        row.PAN,
                                                        row.Present_Address_Line1,
                                                        row.Present_Address_Line2,
                                                        row.Present_PINCode,
                                                        row.MobileNo,
                                                        row.DOB,

                                                        row.CS_MemberNo,
                                                        Kmp_appointment_Date = details.DateOfAppointment,
                                                        cessation_Date = details.CessationEffectFrom,

                                                        row.Present_CityId,

                                                        details.TenureOfAppointment,
                                                        details.DirectorDesignationId,
                                                    }).FirstOrDefault();

                                    if (cfo != null)
                                    {
                                        index++;

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].PAN[0]", Value = cfo.PAN });
                                        if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CFO || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CS || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CEO || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].RB_A_C[0]", Value = "APPN" });
                                            if (cfo.Kmp_appointment_Date != null)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                            }
                                        }
                                        else
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].RB_A_C[0]", Value = "RESG" });
                                            if (cfo.cessation_Date != null)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.cessation_Date).ToString("yyyy-MM-dd") });
                                            }
                                        }
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].FIRST_NAME[0]", Value = cfo.FirstName });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].MIDDLE_NAME[0]", Value = cfo.MiddleName });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].LAST_NAME[0]", Value = cfo.LastName });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].FIRST_NAME1[0]", Value = cfo.Father });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].MIDDLE_NAME1[0]", Value = cfo.FatherMiddleName });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].LAST_NAME1[0]", Value = cfo.FatherLastName });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].ADDRESS_LINE1[0]", Value = cfo.Present_Address_Line1 });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].ADDRESS_LINE2[0]", Value = cfo.Present_Address_Line2 });
                                        if (cfo.Present_CityId > 0)
                                        {
                                            var cityName = (from city in entities.Cities
                                                            where city.ID == cfo.Present_CityId
                                                            select city.Name).FirstOrDefault();
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].CITY[0]", Value = cityName });
                                        }

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].STATE[0]", Value = "MH" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].PIN_CODE[0]", Value = cfo.Present_PINCode });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].ISO_COUNTRY_CODE[0]", Value = "IN" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].PHONE[0]", Value = cfo.MobileNo });
                                        if (cfo.DOB != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DATE_OF_BIRTH[0]", Value = Convert.ToDateTime(cfo.DOB).ToString("yyyy-MM-dd") });
                                        }

                                        switch (cfo.DirectorDesignationId)
                                        {
                                            case 2:
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DESIGNATION2[0]", Value = "MNGR" });
                                                break;
                                            case 3:
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DESIGNATION2[0]", Value = "SCTY" });
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].MEMBERSHIP_NUM[0]", Value = cfo.CS_MemberNo });
                                                break;
                                            case 4:
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DESIGNATION2[0]", Value = "CEO" });
                                                break;
                                            case 5:
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DESIGNATION2[0]", Value = "CFO" });
                                                break;
                                            default:
                                                break;
                                        }


                                        //if (cfo.Kmp_appointment_Date != null)
                                        //{
                                        //    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                        //}
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].EMAIL_ID[0]", Value = cfo.EmailId_Personal });
                                        //lstFields.Add(new EFormFieldVM() { Name = "", Value = "" });

                                    }

                                    break;
                                #endregion

                                #region Director
                                case SecretarialConst.UIForms.APPOINTMENT_OF_MD:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_WTD:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_ADDD:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_ADDD_INDEPENDANT:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_CAVD:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_NOMD:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_ALTD:
                                case SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR:
                                case SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE:
                                case SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE_INDEPENDANT:
                                case SecretarialConst.UIForms.RESIGNATION_OF_MD:
                                case SecretarialConst.UIForms.RESIGNATION_OF_WTD:
                                case SecretarialConst.UIForms.RESIGNATION_OF_ADDD:
                                case SecretarialConst.UIForms.RESIGNATION_OF_ADDD_INDEPENDANT:
                                case SecretarialConst.UIForms.RESIGNATION_OF_CAVD:
                                case SecretarialConst.UIForms.RESIGNATION_OF_NOMD:
                                case SecretarialConst.UIForms.RESIGNATION_OF_ALTD:
                                    var director = (from details in entities.BM_Directors_DetailsOfInterest
                                                    join row in entities.BM_DirectorMaster on details.Director_Id equals row.Id
                                                    where details.Id == (long)item.RefMasterID
                                                    select new
                                                    {
                                                        row.Id,
                                                        row.DIN,
                                                        row.FirstName,
                                                        row.LastName,
                                                        row.MiddleName,

                                                        row.Father,
                                                        row.FatherMiddleName,
                                                        row.FatherLastName,

                                                        row.EmailId_Personal,
                                                        row.PAN,
                                                        row.Present_Address_Line1,
                                                        row.Present_Address_Line2,
                                                        row.Present_PINCode,
                                                        row.MobileNo,

                                                        row.DesignationId,
                                                        row.CS_MemberNo,
                                                        row.Kmp_appointment_Date,
                                                        row.cessation_Date,

                                                        row.Present_CityId,

                                                        details.DateOfAppointment,
                                                        details.TenureOfAppointment,
                                                        details.DirectorDesignationId,
                                                        details.IfDirector,

                                                        details.CompanyOrInstitutionForNominee,
                                                        details.EmailIdOfDirectorForNominee,
                                                        details.CessationEffectFrom,

                                                        details.OriginalDirector_Id

                                                    }).FirstOrDefault();

                                    if (director != null)
                                    {
                                        indexDirector++;
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DIN[0]", Value = director.DIN });

                                        if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_WTD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ADDD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ADDD_INDEPENDANT || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CAVD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_NOMD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ALTD)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].RB_A_C_CD[0]", Value = "APPN" });

                                            if (director.DateOfAppointment != null)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DATE_APP_CD[0]", Value = Convert.ToDateTime(director.DateOfAppointment).ToString("yyyy-MM-dd") });
                                            }

                                            if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "MAND" }); //Managing Director
                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_WTD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "WHTD" }); //Whole Time Director
                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ADDD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ADDD_INDEPENDANT)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "ADDD" }); //Additional Director
                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CAVD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "CAVD" }); //Casual vacancy
                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_NOMD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "NOMD" }); //Appointment of Nominee director
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].NAME_COMPANY[0]", Value = director.CompanyOrInstitutionForNominee });
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].EMAIL_ID_DIR[0]", Value = director.EmailIdOfDirectorForNominee });
                                                //data[0].T_ZMCA_NCA_DIR12_M[0].DATA[0].NAME_COMPANY[0]
                                                //data[0].T_ZMCA_NCA_DIR12_M[0].DATA[0].EMAIL_ID_DIR[0]
                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ALTD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "ALTD" }); //Appointment of Alternate director
                                                if (director.OriginalDirector_Id > 0)
                                                {
                                                    var OriginalDirector_DIN = (from row in entities.BM_DirectorMaster
                                                                                where row.Id == director.OriginalDirector_Id
                                                                                select row.DIN
                                                                               ).FirstOrDefault();
                                                    if (OriginalDirector_DIN != null)
                                                    {
                                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DIN1[0]", Value = OriginalDirector_DIN });
                                                    }
                                                }
                                            }

                                            #region Directorship details
                                            string category = "PROF", chairman = "NONE", executive = "NONE", nonExecutive = "NONE";
                                            switch (director.IfDirector)
                                            {
                                                case 1:
                                                    chairman = "CHRM";
                                                    executive = "EXEC";
                                                    break;
                                                case 4:
                                                    chairman = "CHRM";
                                                    nonExecutive = "NEXE";
                                                    break;
                                                case 5:
                                                    executive = "EXEC";
                                                    break;
                                                case 6:
                                                    nonExecutive = "NEXE";
                                                    break;
                                                case 7:
                                                    nonExecutive = "NEXE";
                                                    category = "INDP";
                                                    break;
                                                case 9:
                                                    chairman = "CHRM";
                                                    nonExecutive = "NEXE";
                                                    category = "INDP";
                                                    break;
                                                default:
                                                    break;
                                            }
                                            #endregion

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CATEGORY[0]", Value = category });

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_CHAIRMAN[0]", Value = chairman });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_EXEC_DIRECTOR[0]", Value = executive });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_N_EXE_DIR[0]", Value = nonExecutive }); //NEXE

                                            //lstFields.Add(new EFormFieldVM() { Name = "", Value = director.DIN });

                                            var detailsOfInterest = (from details in entities.BM_Directors_DetailsOfInterest
                                                                     join entity in entities.BM_EntityMaster on details.EntityId equals entity.Id
                                                                     join designation in entities.BM_Directors_Designation on details.DirectorDesignationId equals designation.Id
                                                                     where details.Director_Id == director.Id && details.Id != (long)item.RefMasterID &&
                                                                         details.IsDeleted == false && details.IsActive == true
                                                                     select new
                                                                     {
                                                                         entity.CIN_LLPIN,
                                                                         designation.Name,
                                                                         details.PercentagesOfShareHolding,
                                                                         details.DirectorDesignationId
                                                                     }).ToList();

                                            if (detailsOfInterest.Count > 0)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].NUM_SUCH_ENTITIE[0]", Value = detailsOfInterest.Count.ToString() }); //NEXE
                                                var entitydetails = detailsOfInterest.FirstOrDefault();

                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CIN_LLPIN_FCRN_R[0]", Value = entitydetails.CIN_LLPIN });
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION1[0]", Value = entitydetails.Name });
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].PERCENTAGE_OF_SH[0]", Value = entitydetails.PercentagesOfShareHolding.ToString() });
                                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[0].AMOUNT[0]", Value = "" });
                                            }

                                        }
                                        else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE_INDEPENDANT || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_MD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_WTD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ADDD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ADDD_INDEPENDANT || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_CAVD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_NOMD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ALTD)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].RB_A_C_CD[0]", Value = "RESG" });
                                            lstFields.Add(new EFormFieldVM() { Name = " data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].REASON[0]", Value = "RESG" });

                                            if (director.CessationEffectFrom != null)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DATE_CONFIRMATN[0]", Value = Convert.ToDateTime(director.CessationEffectFrom).ToString("yyyy-MM-dd") });
                                            }

                                            if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE_INDEPENDANT)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "DIRT" }); //Director
                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_MD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "MAND" }); //Managing Director
                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_WTD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "WHTD" }); //Whole Time Director
                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ADDD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ADDD_INDEPENDANT)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "ADDD" }); //Additional Director
                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_CAVD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "CAVD" }); //Casual vacancy
                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_NOMD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "NOMD" }); //
                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ALTD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "ALTD" }); //
                                            }

                                            #region Directorship details
                                            string category = "PROF", chairman = "NONE", executive = "NONE", nonExecutive = "NONE";
                                            switch (director.IfDirector)
                                            {
                                                case 1:
                                                    chairman = "CHRM";
                                                    executive = "EXEC";
                                                    break;
                                                case 4:
                                                    chairman = "CHRM";
                                                    nonExecutive = "NEXE";
                                                    break;
                                                case 5:
                                                    executive = "EXEC";
                                                    break;
                                                case 6:
                                                    nonExecutive = "NEXE";
                                                    break;
                                                case 7:
                                                    nonExecutive = "NEXE";
                                                    category = "INDP";
                                                    break;
                                                case 9:
                                                    chairman = "CHRM";
                                                    nonExecutive = "NEXE";
                                                    category = "INDP";
                                                    break;
                                                default:
                                                    break;
                                            }
                                            #endregion

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_CHAIRMAN[0]", Value = chairman });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_EXEC_DIRECTOR[0]", Value = executive });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_N_EXE_DIR[0]", Value = nonExecutive }); //NEXE
                                        }
                                    }
                                    #endregion
                                    break;
                                default:
                                    break;
                            }
                        }
                        #endregion
                    }
                    else if (obj.EForm == "MGT-14")
                    {
                        #region MGT-14

                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].CIN[0]", Value = obj.CIN_LLPIN });
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].CB1_REGISTRATION[0]", Value = "RESL" });
                        if (obj.SendDateNotice != null)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].RESOLUTION[0]", Value = Convert.ToDateTime(obj.SendDateNotice).ToString("yyyy-MM-dd") });
                        }
                        if (obj.MeetingDate != null)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].RESOLUTION1[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                        }

                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].NO_OF_RESOLUTION[0]", Value = "10" });

                        foreach (var item in lstFormsVM)
                        {
                            index++;
                            var agendaDetails = (from row in entities.BM_SP_GetAgendaTemplate(item.MeetingAgendaMappingId, null)
                                                 select row).FirstOrDefault();


                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].SECTION_PASSED[0]", Value = "179(3)" });
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].SECTION_PASSED1[0]", Value = "" });
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].PASSING_RESLTN[0]", Value = "C1" });

                            if (agendaDetails != null)
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].SUBJECT_RES[0]", Value = System.Text.RegularExpressions.Regex.Replace(agendaDetails.MinutesFormatHeading, "<.*?>", String.Empty) });
                            }

                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].AUTHRTY_PASSING[0]", Value = "BDIR" });
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].RB_RESOLUTION[0]", Value = "WRQM" });
                        }

                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].AUTHORIZED[0]", Value = "12" }); //Director
                                                                                                                               //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DATE_DECLARATION[0]", Value = "" }); // Date of Declaration
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DESIGNATION[0]", Value = "DIRT" }); //Director
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DIN_PAN[0]", Value = "" }); //DIN
                        #endregion
                    }
                    else if (obj.EForm == "MR-1")
                    {
                        #region MR-1
                        if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER)
                        {
                            #region Manager
                            var cfo = (from row in entities.BM_DirectorMaster
                                       where row.Id == (long)obj.RefMasterID
                                       select new
                                       {
                                           row.FirstName,
                                           row.LastName,
                                           row.MiddleName,

                                           row.Father,
                                           row.FatherMiddleName,
                                           row.FatherLastName,

                                           row.EmailId_Personal,
                                           row.PAN,
                                           row.Present_Address_Line1,
                                           row.Present_Address_Line2,
                                           row.Present_PINCode,
                                           row.MobileNo,

                                           row.DesignationId,
                                           row.CS_MemberNo,
                                           row.Kmp_appointment_Date,
                                           row.cessation_Date,

                                           row.Present_CityId

                                       }).FirstOrDefault();
                            if (cfo != null)
                            {
                                var fullName = (string.IsNullOrEmpty(cfo.FirstName) ? "" : cfo.FirstName) + (string.IsNullOrEmpty(cfo.MiddleName) ? "" : " " + cfo.MiddleName) + (string.IsNullOrEmpty(cfo.LastName) ? "" : " " + cfo.LastName);


                                result.FormName = "MR-1_" + (string.IsNullOrEmpty(fullName) ? "" : fullName.Replace(" ", "_")) + ".pdf";

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].CIN[0]", Value = obj.CIN_LLPIN });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DIN[0]", Value = cfo.PAN });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].NAME[0]", Value = fullName });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DESIGNATION_OPT[0]", Value = "MANG" });

                                if (obj.MeetingDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RESOLUTION_DATE[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                                }

                                if (cfo.Kmp_appointment_Date != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].EFFECT_DATE_APP[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].FROM_DATE[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                }

                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].TO_DATE[0]", Value = "" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RB_AGE_APPOINTEE[0]", Value = "NO" });
                            }
                            #endregion
                        }
                        else if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MD || obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_WTD)
                        {
                            #region MD/WTD
                            var director = (from details in entities.BM_Directors_DetailsOfInterest
                                            join row in entities.BM_DirectorMaster on details.Director_Id equals row.Id
                                            where details.Id == (long)obj.RefMasterID
                                            select new
                                            {
                                                row.DIN,
                                                row.FirstName,
                                                row.LastName,
                                                row.MiddleName,

                                                row.Father,
                                                row.FatherMiddleName,
                                                row.FatherLastName,

                                                row.EmailId_Personal,
                                                row.PAN,
                                                row.Present_Address_Line1,
                                                row.Present_Address_Line2,
                                                row.Present_PINCode,
                                                row.MobileNo,

                                                row.DesignationId,
                                                row.CS_MemberNo,
                                                row.Kmp_appointment_Date,
                                                row.cessation_Date,

                                                row.Present_CityId,

                                                details.DateOfAppointment,
                                                details.TenureOfAppointment,
                                                details.DirectorDesignationId,

                                            }).FirstOrDefault();
                            if (director != null)
                            {
                                var fullName = (string.IsNullOrEmpty(director.FirstName) ? "" : director.FirstName) + (string.IsNullOrEmpty(director.MiddleName) ? "" : " " + director.MiddleName) + (string.IsNullOrEmpty(director.LastName) ? "" : " " + director.LastName);

                                result.FormName = "MR-1_" + (string.IsNullOrEmpty(fullName) ? "" : fullName.Replace(" ", "_")) + ".pdf";

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].CIN[0]", Value = obj.CIN_LLPIN });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DIN[0]", Value = director.PAN });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].NAME[0]", Value = fullName });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DESIGNATION_OPT[0]", Value = "MDIR" });

                                if (obj.MeetingDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RESOLUTION_DATE[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                                }

                                if (director.DateOfAppointment != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].EFFECT_DATE_APP[0]", Value = Convert.ToDateTime(director.DateOfAppointment).ToString("yyyy-MM-dd") });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].FROM_DATE[0]", Value = Convert.ToDateTime(director.DateOfAppointment).ToString("yyyy-MM-dd") });
                                }

                                if (director.TenureOfAppointment != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].TO_DATE[0]", Value = Convert.ToDateTime(director.TenureOfAppointment).ToString("yyyy-MM-dd") });
                                }
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RB_AGE_APPOINTEE[0]", Value = "NO" });
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else if (obj.EForm == "ADT-1")
                    {
                        #region ADT-1
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].CIN[0]", Value = obj.CIN_LLPIN });

                        if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OR_REAPPOINTMENT_OF_STATUTORY_AUDITOR_IN_AGM)
                        {
                            #region Auditor
                            #region Statutory Auditor Master Data
                            var auditor = (from row in entities.BM_StatutoryAuditor
                                           where row.Id == obj.RefMasterID
                                           && row.Flag == "SA"
                                           select new VMStatutory_Auditor
                                           {
                                               Id = row.Id,
                                               Class_of_Company_sec139 = row.Class_of_Company_sec139,
                                               Nature_of_Appointment = row.Nature_of_Appointment,
                                               IsJoin_Auditor_Appointed = row.IsJoin_Auditor_Appointed,
                                               Auditor_Number = row.Auditor_Number,
                                               Auditor_AGM = (bool)row.Auditor_AGM,
                                               Date_AGM = row.Date_AGM,
                                               Date_Appointment = row.Date_Appointment,
                                               IsAuditor_Appointed_casualvacancy = (bool)row.IsAuditor_Appointed_casualvacancy,
                                               SRN_relevent_form = row.SRN_relevent_form,
                                               Person_vacated_office = row.Person_vacated_office,
                                               Membership_Number_or_Registrationfirm = row.Membership_Number_or_Registrationfirm,
                                               Date_vacancy = row.Date_vacancy,
                                               Reasons_casual_vacancy = row.Reasons_casual_vacancy,

                                               AGMNo_TillAppointmentTerminate = row.AGMNo_TillAppointmentTerminate

                                           }).FirstOrDefault();
                            if (auditor != null)
                            {
                                auditor.AuditDeatilsList = (from x in entities.BM_StatutoryAuditor_Mapping
                                                            where x.Statutory_AuditorId == obj.RefMasterID && x.IsActive == true && x.flag == "SA"
                                                            select new VMAuditorDetails

                                                            {

                                                                Auditor_CriteriaId = x.CategoryId,
                                                                Auditor_Id = x.Auditor_ID,
                                                                Id = x.Id,
                                                                AppointedFromdate = x.Appointed_FromDate,
                                                                dueDate = x.Appointed_DueDate,
                                                                AppointedFirstFYDate = x.StartFY,
                                                                AppointedLastFYDate = x.EndFY,
                                                                Number_of_appointedFY = x.Appointed_No_of_FY,

                                                            }).ToList();
                            }
                            #endregion

                            if (auditor != null)
                            {
                                if (auditor.AuditDeatilsList != null)
                                {
                                    var auditor1 = auditor.AuditDeatilsList.FirstOrDefault();
                                    var auditorPersonalDetails = (from row in entities.BM_Auditor_Master
                                                                  from city in entities.Cities.Where(k => k.ID == row.city_Id).DefaultIfEmpty()
                                                                  from state in entities.States.Where(k => k.ID == row.state_Id).DefaultIfEmpty()
                                                                  where row.Id == auditor1.Auditor_Id
                                                                  select new
                                                                  {
                                                                      row.PAN,
                                                                      row.FullName_FirmName,
                                                                      row.Reg_MemberNumber,
                                                                      row.AddressLine1,
                                                                      row.AddressLine2,
                                                                      row.city_Id,
                                                                      cityName = city.Name,
                                                                      row.state_Id,
                                                                      row.Country_Id,
                                                                      row.Pin_Number,
                                                                      row.Email_Id
                                                                  }).FirstOrDefault();

                                    if (auditor.Class_of_Company_sec139 == true)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_COMPANY_139_2[0]", Value = "YES" });
                                    }
                                    else
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_COMPANY_139_2[0]", Value = "NO" });
                                    }

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].NATURE_APPOINT[0]", Value = "FABD" });

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_JOINT_AUDITOR[0]", Value = "NO" });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].NUM_AUDITORS[0]", Value = "1" });

                                    if (obj.MeetingTypeId == 11)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AGM[0]", Value = "YES" });
                                        if (auditor.Date_AGM != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].DATE_OF_AGM[0]", Value = Convert.ToDateTime(auditor.Date_AGM).ToString("yyyy-MM-dd") });
                                        }
                                    }

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AUDITOR_APPNT[0]", Value = "NO" });

                                    if (auditor1.Auditor_CriteriaId == 2)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_CATEGORY_A[0]", Value = "AFRM" }); //Auditor's Firm
                                    }
                                    else
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_CATEGORY_A[0]", Value = "INDI" }); //Individual Auditor
                                    }

                                    if (auditorPersonalDetails != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].PAN_AUDITOR[0]", Value = auditorPersonalDetails.PAN }); //PAN
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NAME_AUDITOR[0]", Value = auditorPersonalDetails.FullName_FirmName });//Name
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].MEMBERSHIP_NUM[0]", Value = auditorPersonalDetails.Reg_MemberNumber });//Membership No
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].ADDRESS_LINE_I[0]", Value = auditorPersonalDetails.AddressLine1 });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].ADDRESS_LINE_II[0]", Value = auditorPersonalDetails.AddressLine2 });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].CITY[0]", Value = auditorPersonalDetails.cityName });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].STATE[0]", Value = "" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].COUNTRY[0]", Value = "" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].PIN_CODE[0]", Value = Convert.ToString(auditorPersonalDetails.Pin_Number) });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].EMAIL_ID_A[0]", Value = auditorPersonalDetails.Email_Id });
                                    }

                                    if (auditor1.AppointedFromdate != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].FROM_DATE[0]", Value = Convert.ToDateTime(auditor1.AppointedFromdate).ToString("yyyy-MM-dd") });
                                    }
                                    if (auditor1.dueDate != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].TO_DATE[0]", Value = Convert.ToDateTime(auditor1.dueDate).ToString("yyyy-MM-dd") });
                                    }

                                    if (auditor1.Number_of_appointedFY != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NUM_F_YEARS[0]", Value = Convert.ToString(auditor1.Number_of_appointedFY) });
                                    }

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_APPOINTMENT[0]", Value = "NO" });
                                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NUM_F_YEARS1[0]", Value = "" });
                                }
                            }
                            #endregion
                        }
                        else if (obj.UIFormID == SecretarialConst.UIForms.AUDITOR_APPOINTED_IN_CASE_OF_CASUAL_VACANCY_RATIFICATION_IN_EGM)
                        {
                            #region Auditor
                            var auditorAppointedInCV = (from x in entities.BM_StatutoryAuditor_Mapping
                                                        where x.Id == obj.RefMasterID && //x.IsActive == true &&
                                                        x.flag == "SA"
                                                        select new VMAuditorDetails
                                                        {
                                                            Id = x.Id,
                                                            Statutory_AuditorId_ = x.Statutory_AuditorId,
                                                            Auditor_CriteriaId = x.CategoryId,
                                                            Auditor_Id = x.Auditor_ID,
                                                            AppointedFromdate = x.Appointed_FromDate,
                                                            dueDate = x.Appointed_DueDate,
                                                            AppointedFirstFYDate = x.StartFY,
                                                            AppointedLastFYDate = x.EndFY,
                                                            Number_of_appointedFY = x.Appointed_No_of_FY
                                                        }).FirstOrDefault();

                            if (auditorAppointedInCV != null)
                            {
                                

                                #region Statutory Auditor Master Data
                                var auditor = (from row in entities.BM_StatutoryAuditor
                                               where row.Id == auditorAppointedInCV.Statutory_AuditorId_ //obj.RefMasterID
                                               && row.Flag == "SA"
                                               select new VMStatutory_Auditor
                                               {
                                                   Id = row.Id,
                                                   Class_of_Company_sec139 = row.Class_of_Company_sec139,
                                                   Nature_of_Appointment = row.Nature_of_Appointment,
                                                   IsJoin_Auditor_Appointed = row.IsJoin_Auditor_Appointed,
                                                   Auditor_Number = row.Auditor_Number,
                                                   Auditor_AGM = (bool)row.Auditor_AGM,
                                                   Date_AGM = row.Date_AGM,
                                                   Date_Appointment = row.Date_Appointment,
                                                   IsAuditor_Appointed_casualvacancy = (bool)row.IsAuditor_Appointed_casualvacancy,
                                                   SRN_relevent_form = row.SRN_relevent_form,
                                                   Person_vacated_office = row.Person_vacated_office,
                                                   Membership_Number_or_Registrationfirm = row.Membership_Number_or_Registrationfirm,
                                                   Date_vacancy = row.Date_vacancy,
                                                   Reasons_casual_vacancy = row.Reasons_casual_vacancy,

                                                   AGMNo_TillAppointmentTerminate = row.AGMNo_TillAppointmentTerminate

                                               }).FirstOrDefault();
                                if (auditor != null)
                                {
                                    auditor.AuditDeatilsList = new List<VMAuditorDetails>() { auditorAppointedInCV };
                                }
                                #endregion

                                if (auditor != null)
                                {
                                    if (auditor.AuditDeatilsList != null)
                                    {
                                        var auditor1 = auditor.AuditDeatilsList.FirstOrDefault();
                                        var auditorPersonalDetails = (from row in entities.BM_Auditor_Master
                                                                      from city in entities.Cities.Where(k => k.ID == row.city_Id).DefaultIfEmpty()
                                                                      from state in entities.States.Where(k => k.ID == row.state_Id).DefaultIfEmpty()
                                                                      where row.Id == auditor1.Auditor_Id
                                                                      select new
                                                                      {
                                                                          row.PAN,
                                                                          row.FullName_FirmName,
                                                                          row.Reg_MemberNumber,
                                                                          row.AddressLine1,
                                                                          row.AddressLine2,
                                                                          row.city_Id,
                                                                          cityName = city.Name,
                                                                          row.state_Id,
                                                                          row.Country_Id,
                                                                          row.Pin_Number,
                                                                          row.Email_Id
                                                                      }).FirstOrDefault();

                                        if (auditor.Class_of_Company_sec139 == true)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_COMPANY_139_2[0]", Value = "YES" });
                                        }
                                        else
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_COMPANY_139_2[0]", Value = "NO" });
                                        }

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].NATURE_APPOINT[0]", Value = "AACV" });

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_JOINT_AUDITOR[0]", Value = "NO" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].NUM_AUDITORS[0]", Value = "1" });

                                        if (obj.MeetingTypeId == 11)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AGM[0]", Value = "NO" });
                                            //if (auditor.Date_AGM != null)
                                            //{
                                            //    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].DATE_OF_AGM[0]", Value = Convert.ToDateTime(auditor.Date_AGM).ToString("yyyy-MM-dd") });
                                            //}
                                        }

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AUDITOR_APPNT[0]", Value = "NO" });

                                        if (auditor1.Auditor_CriteriaId == 2)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_CATEGORY_A[0]", Value = "AFRM" }); //Auditor's Firm
                                        }
                                        else
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_CATEGORY_A[0]", Value = "INDI" }); //Individual Auditor
                                        }

                                        if (auditorPersonalDetails != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].PAN_AUDITOR[0]", Value = auditorPersonalDetails.PAN }); //PAN
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NAME_AUDITOR[0]", Value = auditorPersonalDetails.FullName_FirmName });//Name
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].MEMBERSHIP_NUM[0]", Value = auditorPersonalDetails.Reg_MemberNumber });//Membership No
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].ADDRESS_LINE_I[0]", Value = auditorPersonalDetails.AddressLine1 });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].ADDRESS_LINE_II[0]", Value = auditorPersonalDetails.AddressLine2 });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].CITY[0]", Value = auditorPersonalDetails.cityName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].STATE[0]", Value = "" });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].COUNTRY[0]", Value = "" });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].PIN_CODE[0]", Value = Convert.ToString(auditorPersonalDetails.Pin_Number) });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].EMAIL_ID_A[0]", Value = auditorPersonalDetails.Email_Id });
                                        }

                                        if (auditor1.AppointedFromdate != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].FROM_DATE[0]", Value = Convert.ToDateTime(auditor1.AppointedFromdate).ToString("yyyy-MM-dd") });
                                        }
                                        if (auditor1.dueDate != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].TO_DATE[0]", Value = Convert.ToDateTime(auditor1.dueDate).ToString("yyyy-MM-dd") });
                                        }

                                        if (auditor1.Number_of_appointedFY != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NUM_F_YEARS[0]", Value = Convert.ToString(auditor1.Number_of_appointedFY) });
                                        }

                                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_APPOINTMENT[0]", Value = "NO" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AUDITOR_APPNT[0]", Value = "YES" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].SRN_RELEVNT_FORM[0]", Value = auditor.SRN_relevent_form });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].PERSON_VACATED[0]", Value = "AFRM" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].MEM_OR_REG_NUM[0]", Value = auditor.Membership_Number_or_Registrationfirm });
                                        if(auditor.Date_vacancy != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].DATE_VACANCY[0]", Value = Convert.ToDateTime(auditor.Date_vacancy).ToString("yyyy-MM-dd") });
                                        }
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].REASONS_CASUAL_V[0]", Value = auditor.Reasons_casual_vacancy });
                                    }
                                }
                                
                            }

                            #endregion
                        }
                        #endregion
                    }
                    else if (obj.EForm == "CRA-2")
                    {
                        #region CRA-2
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].CIN_OR_FCRN[0]", Value = obj.CIN_LLPIN });
                        if(obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_COST_AUDITOR)
                        {
                            #region Cost Auditor Master Data
                            var auditor = (from row in entities.BM_StatutoryAuditor
                                           join mapping in entities.BM_StatutoryAuditor_Mapping on row.Id equals mapping.Statutory_AuditorId
                                           where row.Id == obj.RefMasterID
                                           && row.Flag == "CA"
                                           select new VM_CostAuditor
                                           {
                                               AuditorMappingId = mapping.Id,
                                               Id = row.Id,
                                               EntityId = row.EntityId,
                                               AuditorId = mapping.Auditor_ID,
                                               categoryId = mapping.CategoryId,
                                               Nature_of_intimation_cost = row.Nature_of_Intimatecost,
                                               Cost_auditor_firm_Original = row.CoustAuditor_FirmOrigine,
                                               dateofBordMeeting = row.BoardMeeting_date,
                                               ResulationNumber = row.Resulation_Number
                                           }).FirstOrDefault();

                            if (auditor != null)
                            {
                                var auditorPersonalDetails = (from row in entities.BM_Auditor_Master
                                                              from city in entities.Cities.Where(k => k.ID == row.city_Id).DefaultIfEmpty()
                                                              from state in entities.States.Where(k => k.ID == row.state_Id).DefaultIfEmpty()
                                                              where row.Id == auditor.AuditorId
                                                              select new
                                                              {
                                                                  row.PAN,
                                                                  row.FullName_FirmName,
                                                                  row.Reg_MemberNumber,
                                                                  row.AddressLine1,
                                                                  row.AddressLine2,
                                                                  row.city_Id,
                                                                  cityName = city.Name,
                                                                  row.state_Id,
                                                                  row.Country_Id,
                                                                  row.Pin_Number,
                                                                  row.Email_Id
                                                              }).FirstOrDefault();

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].NATURE_INTIMATN[0]", Value = "ORGN" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].NUM_COST_AUDITOR[0]", Value = "1" });

                                if (auditor.categoryId == 1)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].RB_CATEGORY_ADTR[0]", Value = "INDVDL" }); //Individual Auditor
                                }
                                else
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].RB_CATEGORY_ADTR[0]", Value = "PRTNR" }); //Partner Firm
                                }

                                if (auditorPersonalDetails != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].MEMBERSHP_NO_COA[0]", Value = auditorPersonalDetails.Reg_MemberNumber });//Membership No
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].NAME_COST_AUDITR[0]", Value = auditorPersonalDetails.FullName_FirmName });//Name
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].FIRM_REGSTRN_NUM[0]", Value = auditor.Cost_auditor_firm_Original }); // Cost_auditor_firm_Original
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].NAME_COSTA_FIRM[0]", Value = "" }); // Name of LLP firm
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].ADDRESS_LINE_1[0]", Value = auditorPersonalDetails.AddressLine1 });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].ADDRESS_LINE_2[0]", Value = auditorPersonalDetails.AddressLine2 });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].CITY[0]", Value = auditorPersonalDetails.cityName });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].STATE[0]", Value = "" });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].COUNTRY[0]", Value = "" });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].PIN_CODE[0]", Value = Convert.ToString(auditorPersonalDetails.Pin_Number) });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].EMAIL_ID[0]", Value = auditorPersonalDetails.Email_Id });
                                }

                                if (obj.MeetingDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].DATE_BOARD_MEETN[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                                }
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].TYPE_APPOINTMENT[0]", Value = "1" }); // Original

                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].F_YEAR_FROM_DATE[0]", Value = "1" });
                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].F_YEAR_END_DATE[0]", Value = "1" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].RB_CHANGE_COST_A[0]", Value = "NO" });
                            }
                            #endregion
                        }
                        #endregion
                    }

                    if (lstFormsVM.Count > 1)
                    {
                        result.FullFileName = (from row in entities.BM_SP_FormPath(obj.EForm, index, indexDirector)
                                               select row).FirstOrDefault();
                    }
                    else
                    {
                        result.FullFileName = obj.FormPath;
                    }

                    if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(result.FullFileName)))
                    {
                        SetData(result, lstFields);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        private void SetData(EFormVM obj, List<EFormFieldVM> lstFields)
        {
            try
            {
                obj.FormData = null;

                var form = System.Web.Hosting.HostingEnvironment.MapPath(obj.FullFileName);
                PdfReader pdfReader = new PdfReader(form);

                using (MemoryStream ms = new MemoryStream())
                {
                    PdfStamper stamper = new PdfStamper(pdfReader, ms, '\0', true);

                    AcroFields pdfFormFields = stamper.AcroFields;

                    foreach (var item in lstFields)
                    {
                        pdfFormFields.SetField(item.Name, item.Value);
                    }
                    stamper.Close();
                    obj.FormData = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SetDataFileStream(EFormVM obj, List<EFormFieldVM> lstFields)
        {
            try
            {
                obj.FormData = null;

                var form = System.Web.Hosting.HostingEnvironment.MapPath(obj.FullFileName);
                var formNew = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/BM_Management/Documents/Forms/Form_DIR-12_Amol_Dere.pdf");
                PdfReader pdfReader = new PdfReader(form);

                PdfStamper stamper = new PdfStamper(pdfReader, new FileStream(formNew, FileMode.Create), pdfReader.PdfVersion, true);
                AcroFields pdfFormFields = stamper.AcroFields;

                //foreach (var item in lstFields)
                //{
                //    pdfFormFields.SetField(item.Name, item.Value);
                //}
                stamper.AcroFields.SetField("data[0].ZMCA_NCA_DIR_12[0].SRN_2_1_OR_CIN[0]", "L74140KA2000PLC118395");
                stamper.AcroFields.SetField("data[0].ZMCA_NCA_DIR_12[0].NUM_M_S_CFO_CEO[0]", "1");
                stamper.AcroFields.SetField("data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].PAN[0]", "ASNPP3939M");
                stamper.AcroFields.SetField("data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].RB_A_C[0]", "APPN");
                stamper.FormFlattening = false;
                stamper.Close();


                //
                var formNew1 = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/BM_Management/Documents/Forms/Form_DIR-12_Amol_Dere1.pdf");
                pdfReader = new PdfReader(form);
                PdfStamper stamper1 = new PdfStamper(pdfReader, new FileStream(formNew1, FileMode.Create), pdfReader.PdfVersion, true);

                //foreach (var item in lstFields)
                //{
                //    pdfFormFields.SetField(item.Name, item.Value);
                //}
                stamper1.AcroFields.SetField("data[0].ZMCA_NCA_DIR_12[0].SRN_2_1_OR_CIN[0]", "L74140KA2000PLC118395");
                stamper1.Close();
                //


                //
                var formNew2 = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/BM_Management/Documents/Forms/Form_DIR-12_Amol_Dere2.pdf");
                pdfReader = new PdfReader(form);
                PdfStamper stamper2 = new PdfStamper(pdfReader, new FileStream(formNew2, FileMode.Create), pdfReader.PdfVersion, false);

                //foreach (var item in lstFields)
                //{
                //    pdfFormFields.SetField(item.Name, item.Value);
                //}
                stamper2.AcroFields.SetField("data[0].ZMCA_NCA_DIR_12[0].SRN_2_1_OR_CIN[0]", "L74140KA2000PLC118395");
                stamper2.Close();
                //

                using (FileStream fs = File.OpenRead(formNew))
                {
                    int length = (int)fs.Length;
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        obj.FormData = br.ReadBytes(length);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Generate E-Forms (Director)
        public EFormVM GenerateFormsforDirector(long DirectorID)
        {
            var result = new EFormVM() { DirectorID = DirectorID };
            var lstFields = new List<EFormFieldVM>();
            var lstFormsVM = new List<MultipleFormsVM>();

            try
            {
                var obj = (from dir in entities.BM_DirectorMaster
                           join rows in entities.BM_DirectorTypeOfChanges
                           on dir.Id equals rows.DirectorId
                           join row in entities.BM_DirectorTypeChangesMapping
                           on rows.DirectorId equals row.DirectorId
                           where dir.Id == DirectorID
                           select new
                           {
                               dir.Id,
                               dir.FirstName,
                               dir.MiddleName,
                               dir.LastName,
                               dir.DIN,
                               dir.PAN,
                               dir.Father,
                               dir.FatherMiddleName,
                               dir.FatherLastName,
                               dir.Nationality,
                               dir.DOB,
                               dir.Gender,
                               dir.EmailId_Official,
                               dir.EmailId_Personal,
                               dir.Permenant_Address_Line1,
                               dir.Permenant_Address_Line2,
                               dir.Present_Address_Line1,
                               dir.Present_Address_Line2,
                               rows.Name_of_Director,
                               rows.FatherName,
                               rows.Mobile,
                               rows.EmailID,
                               //row.Gender,
                               //rows.Nationality as N,
                               //rows.PAN,
                               rows.Passport,
                               rows.Parmanent_Address,
                               rows.Present_Address,
                               rows.AadharNumber,
                               rows.DateofBirth,

                           }).FirstOrDefault();

                //result.FullFileName = "~/Areas/BM_Management/Documents/Forms/Form_DIR-12.pdf";
                //result.FormName = "DIR-12.pdf";

                if (obj != null)
                {
                    #region Dir-6

                    result.FullFileName = "~/Areas/BM_Management/Documents/Forms/Form_DIR-6.pdf";
                    result.FormName = "DIR6.pdf";


                    if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(result.FullFileName)))
                    {
                        #region DIR-6


                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZMCA_NCA_DIR_6[0].DIN[0]", Value = obj.DIN });
                        if (obj.Name_of_Director)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB1_DIRECTOR[0]", Value = "APPN" });
                        }
                        if (obj.FatherName)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB2_FATHER_NAME[0]", Value = "FATH" });
                        }

                        if (obj.DateofBirth)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB4_DATE_OF_BIRT[0]", Value = "DDOB" });
                        }
                        if (obj.Name_of_Director)
                        {
                            if (obj.FirstName != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].FIRST_NAME[0]", Value = obj.FirstName });
                            }
                            if (obj.LastName != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].LAST_NAME[0]", Value = obj.LastName });
                            }
                            if (obj.MiddleName != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].MIDDLE_NAME[0]", Value = obj.MiddleName });
                            }
                        }
                        if (obj.FatherName)
                        {
                            if (obj.Father != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].FATH_FIRST_NAME[0]", Value = obj.Father });
                            }
                            if (obj.FatherLastName != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].FATH_LAST_NAME[0]", Value = obj.FatherLastName });
                            }
                            if (obj.FatherMiddleName != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].FATH_MIDDLE_NAME[0]", Value = obj.FatherMiddleName });
                            }
                        }
                        if (obj.DateofBirth)
                        {
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].DATE_OF_BIRTH[0]",
                                Value = obj.DOB.ToString("yyyy-MM-dd")

                            });
                        }

                        if (obj.Present_Address)
                        {
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PERM_ADD_LINE1[0]",
                                Value = obj.Present_Address_Line1

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PERM_ADD_LINE2[0]",
                                Value = obj.Present_Address_Line2

                            });
                        }

                        if (obj.Parmanent_Address)
                        {
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PRES_ADD_LINE1[0]",
                                Value = obj.Permenant_Address_Line1

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PRES_ADD_LINE2[0]",
                                Value = obj.Permenant_Address_Line2

                            });
                        }

                        #endregion DIR-6
                    }
                    #endregion

                    SetData(result, lstFields);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public EFormVM GenerateFormforDirectorDIR3(long DirectorID)
        {
            var result = new EFormVM() { DirectorID = DirectorID };
            var lstFields = new List<EFormFieldVM>();
            var lstFormsVM = new List<MultipleFormsVM>();
            string Gender = string.Empty;
            try
            {
                var obj = (from dir in entities.BM_DirectorMaster
                           join rows in entities.BM_DirectorTypeOfChanges
                           on dir.Id equals rows.DirectorId
                           join row in entities.BM_DirectorTypeChangesMapping
                           on rows.DirectorId equals row.DirectorId
                           where dir.Id == DirectorID
                           select new
                           {
                               dir.Id,
                               dir.FirstName,
                               dir.MiddleName,
                               dir.LastName,
                               dir.DIN,
                               dir.PAN,
                               dir.Father,
                               dir.FatherMiddleName,
                               dir.FatherLastName,
                               dir.Nationality,
                               dir.DOB,
                               dir.Gender,
                               dir.EmailId_Official,
                               dir.EmailId_Personal,
                               dir.Permenant_Address_Line1,
                               dir.Permenant_Address_Line2,
                               dir.Present_Address_Line1,
                               dir.Present_Address_Line2,
                               dir.Aadhaar,
                               dir.MobileNo,

                               rows.Name_of_Director,
                               rows.FatherName,
                               rows.Mobile,
                               rows.EmailID,

                               //rows.Nationality as N,
                               rows.Parmanent_Address,
                               rows.Present_Address,
                               rows.AadharNumber,
                               rows.DateofBirth,

                           }).FirstOrDefault();


                if (obj != null)
                {


                    result.FullFileName = "~/Areas/BM_Management/Documents/Forms/Form_DIR3-KYC.pdf";
                    result.FormName = "Form_DIR-3_KYC.pdf";


                    if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(result.FullFileName)))
                    {


                        #region DIR-3 KYC

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].DIN[0]", Value = obj.DIN });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].FIRST_NAME[0]", Value = obj.FirstName });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].LAST_NAME[0]", Value = obj.LastName });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].MIDDLE_NAME[0]", Value = obj.MiddleName });

                        if (obj.Father != null)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZNCA_DIR3_KYC[0].FATH_FIRST_NAME[0]", Value = obj.Father });
                        }
                        if (obj.FatherLastName != null)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZNCA_DIR3_KYC[0].FATH_LAST_NAME[0]", Value = obj.FatherLastName });
                        }
                        if (obj.FatherMiddleName != null)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZNCA_DIR3_KYC[0].FATH_MIDDLE_NAME[0]", Value = obj.FatherMiddleName });
                        }
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].RB_CITIZEN_INDIA[0]", Value = "YES" });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].NATIONALITY[0]", Value = "IN" });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].RB_RESIDENT_IND[0]", Value = "YES" });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].DATE_OF_BIRTH[0]", Value = obj.DOB.ToString("yyyy-MM-dd") });
                        if (obj.Gender == 1)
                        {
                            Gender = "MALE";
                        }
                        else if (obj.Gender == 2)
                        {
                            Gender = "FEMALE";
                        }
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].RB_GENDER[0]", Value = Gender });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].IT_PAN[0]", Value = obj.PAN });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].RB_AADHAR_NUMBER[0]", Value = "Yes" });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].AADHAR_NUMBER[0]", Value = obj.Aadhaar });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERSONAL_MOB_NUM[0]", Value = obj.MobileNo });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERSONAL_EMAILID[0]", Value = obj.EmailId_Personal });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERSONAL_EMAILID[0]", Value = obj.EmailId_Personal });
                    }
                    #endregion DIR-3 KYC

                    SetData(result, lstFields);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

    }
}