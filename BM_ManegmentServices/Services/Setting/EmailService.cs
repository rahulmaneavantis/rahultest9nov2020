﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using System.Reflection;
using System.Net.Mail;
using System.Net;

namespace BM_ManegmentServices.Services.Setting
{
    public class EmailService : IEmailService
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public List<MailSettingVM> GetAll(int customerId, int userId, string role)
        {
            try
            {
                if (role == SecretarialConst.Roles.CS)
                {
                    return (from row in entities.BM_MailSetting
                            join view in entities.BM_AssignedEntitiesView on row.EntityID equals view.EntityId
                            join entity in entities.BM_EntityMaster on row.EntityID equals entity.Id
                            where row.IsDeleted == false && row.IsActive == true && row.CustomerID == customerId &&
                            view.userId == userId
                            select new MailSettingVM
                            {
                                MailSettingId = row.Id,
                                Email_Entity = row.Email,
                                Password = row.Password_,
                                Port = row.Port_,
                                Host = row.Host,
                                EnableSsl = row.EnableSsl,
                                EntityID = row.EntityID,
                                EntityName = entity.CompanyName
                            }).ToList();
                }
                else
                {
                    return (from row in entities.BM_MailSetting
                            join entity in entities.BM_EntityMaster on row.EntityID equals entity.Id
                            where row.IsDeleted == false && row.IsActive == true && row.CustomerID == customerId
                            select new MailSettingVM
                            {
                                MailSettingId = row.Id,
                                Email_Entity = row.Email,
                                Password = row.Password_,
                                Port = row.Port_,
                                Host = row.Host,
                                EnableSsl = row.EnableSsl,
                                EntityID = row.EntityID,
                                EntityName = entity.CompanyName
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
            
        }
        public TestMailVM Get(long id, int customerId)
        {
            try
            {
                return (from row in entities.BM_MailSetting
                        where row.IsDeleted == false && row.Id == id && row.CustomerID == customerId
                        select new TestMailVM
                        {
                            MailSettingId = row.Id,
                            EntityID = row.EntityID,
                            FromMail = row.Email
                        }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public MailSettingVM Save(MailSettingVM obj, int userId)
        {
            try
            {
                var _obj = new BM_MailSetting();
                if(obj.MailSettingId > 0)
                {
                    _obj = entities.BM_MailSetting.Where(k => k.Id == obj.MailSettingId && k.EntityID == obj.EntityID && k.CustomerID == obj.CustomerID).FirstOrDefault();
                    if(_obj !=null)
                    {
                        _obj.Email = obj.Email_Entity;
                        _obj.Password_ = obj.Password;
                        _obj.Port_ = obj.Port;
                        _obj.Host = obj.Host;
                        _obj.EnableSsl = obj.EnableSsl;
                        
                        _obj.IsDeleted = false;
                        _obj.IsActive = true;

                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now.Date;

                        entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = "Update Successfully.";
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = "Something wents wrong.";
                    }
                }
                else
                {
                    _obj = entities.BM_MailSetting.Where(k => k.EntityID == obj.EntityID && k.CustomerID == obj.CustomerID).FirstOrDefault();

                    if (_obj == null)
                    {
                        _obj = new BM_MailSetting();
                        _obj.Email = obj.Email_Entity;
                        _obj.Password_ = obj.Password;
                        _obj.Port_ = obj.Port;
                        _obj.Host = obj.Host;
                        _obj.EnableSsl = obj.EnableSsl;

                        _obj.IsDeleted = false;
                        _obj.IsActive = true;
                        _obj.EntityID = obj.EntityID;
                        _obj.CustomerID = obj.CustomerID;

                        _obj.CreatedBy = userId;
                        _obj.CreatedOn = DateTime.Now.Date;

                        entities.BM_MailSetting.Add(_obj);
                        entities.SaveChanges();

                        obj.MailSettingId = _obj.Id;
                        obj.Success = true;
                        obj.Message = "Save Successfully.";
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = "Mail setting already exists for this entity.";
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public static string TestMail(string from, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Attachment> attachment, int customerId, int entityId)
        {
            dynamic mailSetting = null;
            string result = "";
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                mailSetting = (from row in entities.BM_MailSetting
                               where row.EntityID == entityId && row.CustomerID == customerId && row.IsDeleted == false && row.IsActive == true
                               select new
                               {
                                   Email = row.Email,
                                   Password = row.Password_,
                                   Port = row.Port_,
                                   Host = row.Host,
                                   EnableSsl = row.EnableSsl

                               }).FirstOrDefault();
            }

            try
            {
                if (mailSetting != null)
                {
                    using (SmtpClient email = new SmtpClient())
                    using (MailMessage mailMessage = new MailMessage())
                    {
                        email.DeliveryMethod = SmtpDeliveryMethod.Network;
                        email.UseDefaultCredentials = false;
                        NetworkCredential credential = new NetworkCredential(mailSetting.Email, mailSetting.Password);
                        email.Credentials = credential;
                        email.Port = Convert.ToInt32(mailSetting.Port);
                        email.Host = mailSetting.Host;
                        email.Timeout = 60000;
                        email.EnableSsl = Convert.ToBoolean(mailSetting.EnableSsl);
                        MailMessage oMessage = new MailMessage();
                        //string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                        string FromEmailId = mailSetting.Email;
                        mailMessage.From = new MailAddress(FromEmailId);
                        mailMessage.Subject = subject;
                        mailMessage.Body = message;
                        mailMessage.IsBodyHtml = true;
                        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                        //mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                        if (attachment.Count > 0)
                        {
                            foreach (var attach in attachment)
                            {
                                mailMessage.Attachments.Add(attach);
                            }
                        }
                        if (to != null)
                            to.ForEach(entry => mailMessage.To.Add(entry));
                        if (cc != null)
                            cc.ForEach(entry => mailMessage.CC.Add(entry));
                        if (bcc != null)
                            bcc.ForEach(entry => mailMessage.Bcc.Add(entry));

                        email.Send(mailMessage);
                        result = "Send mail successfully.";
                    }
                }
                else
                {
                    result = "Somthing wents wrong.";
                }
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        result = "Delivery failed.";
                    }
                    else
                    {
                        result = "Failed to deliver message to " + ex.InnerExceptions[i].FailedRecipient;
                    }
                }
            }
            catch (Exception ex)
            {
                result = "Server Error Occure.";
            }

            return result;
        }
    }
}