﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Setting
{
    public class SettingService : ISettingService
    {
        ComplianceDBEntities entities = new ComplianceDBEntities();
        Compliance_SecretarialEntities entities_Secretarial = new Compliance_SecretarialEntities();
        public bool CheckCreateUserOfDirectorId(int customerId)
        {
            var result = false;
            try
            {
                var customer = (from row in entities.Customers
                                where row.ID == customerId
                                select new
                                {
                                    row.ID,
                                    row.ParentID,
                                    row.IsDistributor
                                }).FirstOrDefault();

                if (customer != null)
                {
                    if (customer.IsDistributor == true)
                    {
                        result = false;
                    }
                    else if (customer.ParentID != null)
                    {
                        var parentCustomer = (from row in entities.Customers
                                              where row.ID == customer.ParentID
                                              select new
                                              {
                                                  row.ID,
                                                  row.ParentID,
                                                  row.IsDistributor
                                              }).FirstOrDefault();

                        if (parentCustomer != null)
                        {
                            if (parentCustomer.IsDistributor == true)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        result = true;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public bool GetAccessToDirector(int customerId)
        {
            var result = false;
            try
            {
                result = (from row in entities_Secretarial.BM_CustomerConfiguration
                          where row.CustomerID == customerId 
                          && row.IsActive == true 
                          && row.SentMailtoDirector == true
                          select row.SentMailtoDirector).Any();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public bool CheckDefaultVirtualMeeting(int customerId)
        {
            var result = false;
            try
            {
                result = (from row in entities_Secretarial.BM_CustomerConfiguration
                          where row.CustomerID == customerId
                          && row.IsActive == true
                          && row.DefaultVirtualMeeting == true
                          select row.DefaultVirtualMeeting).Any();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
    }
}