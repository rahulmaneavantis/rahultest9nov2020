﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Setting
{
    public interface ISettingService
    {
        bool CheckCreateUserOfDirectorId(int customerId);
        bool GetAccessToDirector(int customerId);

        bool CheckDefaultVirtualMeeting(int customerId); 
    }
}
