﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Profile
{
    public interface IMyProfile
    {
        Director_MasterVM GetDirectorDetails(int userId, int customerID);
        Director_MasterVM updateDirectorProfile(Director_MasterVM obj);
        OnEditingDirector getOnEditChanges(long iD);
        ResignationofDirector GetDetailsofIntrest(long? iD, long? entityId);
        bool Canceleditchange(long directorID, int userID);

        OnEditingDirector SaveDirectorTypeOfChange(OnEditingDirector obj);
    }
}
