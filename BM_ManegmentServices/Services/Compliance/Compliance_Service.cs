﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace BM_ManegmentServices.Services.Compliance
{
    public class Compliance_Service : ICompliance_Service
    {       
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();        

        public int GetCustomerBranchID(int entityId, int customerId)
        {
            var customerBranchID = entities.BM_EntityMaster.Where(k => k.Id == entityId && k.Customer_Id == customerId && k.Is_Deleted == false).Select(k => k.CustomerBranchId).FirstOrDefault();
            return customerBranchID.HasValue ? (int)customerBranchID : 0;
        }

        #region Get Instance Id
        public long? GetInstanceId(int customerBranchID, long complianceId)
        {
            try
            {
                var result = (from row in entities.ComplianceInstances
                              where row.CustomerBranchID == customerBranchID && row.ComplianceId == complianceId && row.IsDeleted == false
                              select row.ID).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Assigned Compliances
        public IEnumerable<ComplianceAssignedListVM> GetAssignedCompliances(int entityID, int customerID, string mappingType)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetAssignedCompliance(entityID, customerID, mappingType)
                              select new ComplianceAssignedListVM
                              {
                                  EntityID = (int)row.EntityID,
                                  ComplianceId = row.ComplianceID,
                                  Description = row.Description,
                                  Performer = new PerformerReviewerViewModel()
                                  {
                                      UserID = row.PerformerID,
                                      FullName = row.Performer
                                  },
                                  Reviewer = new PerformerReviewerViewModel()
                                  {
                                      UserID = row.ReviewerID,
                                      FullName = row.Reviewer
                                  }
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Get Assignment for Schedule on
        public List<ComplianceScheduleAssignmentVM> GetAssignment(int entityId, long complianceID)
        {
            List<ComplianceScheduleAssignmentVM> result = null;
            try
            {
                var complianceInstanceID = (from entity in entities.BM_EntityMaster
                                            join instance in entities.ComplianceInstances on entity.CustomerBranchId equals instance.CustomerBranchID
                                            where entity.Id == entityId && instance.ComplianceId == complianceID && instance.IsDeleted == false
                                            select instance.ID).FirstOrDefault();

                if (complianceInstanceID > 0)
                {
                    var complianceAssignment = (from assignment in entities.ComplianceAssignments
                                                where assignment.ComplianceInstanceID == complianceInstanceID
                                                select new { assignment.RoleID, assignment.UserID }).ToList();

                    if (complianceAssignment != null)
                    {
                        result = complianceAssignment.Select(r => new ComplianceScheduleAssignmentVM
                        {
                            RoleID = r.RoleID,
                            UserID = r.UserID,
                            IsDeleted = false,
                            CreatedOn = DateTime.Now,
                        }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Compliance Assign
        public IEnumerable<AgendaComplianceListVM> GetAgendaComplianceList(int entityID, int customerId)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetAgendaComplianceByEntityID(entityID, customerId)
                              select new AgendaComplianceListVM
                              {
                                  AgendaId = row.AgendaID,
                                  AgendaItem = row.AgendaItem,
                                  ComplianceId = row.ComplianceID,
                                  Description = row.ShortDescription,
                                  IsAgendaSelect = true,
                                  IsComplianceSelect = true,
                                  PerformerID = 0,
                                  ReviewerID = 0,
                                  EntityID = entityID,
                                  Performer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                                  Reviewer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = row.MeetingTypeName
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public AgendaComplianceAssingVM CreateComplianceInstance_Statutory(AgendaComplianceAssingVM obj)
        {
            try
            {
                int customerBranchID = GetCustomerBranchID(obj.EntityID, obj.CustomerID);
                int customerID = obj.CustomerID;
                int userID = obj.UserID;
                string userName = obj.UserName;

                if (customerBranchID > 0)
                {
                    if (obj.lst != null)
                    {
                        DateTime ScheduleOnDate = new DateTime().Date;

                        DateTime.TryParse(obj.ScheduleOnDate, out ScheduleOnDate);

                        List<Tuple<com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment>> assignments
                            = new List<Tuple<com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment>>();

                        foreach (var item in obj.lst)
                        {
                            var isExists = entities.ComplianceInstances
                                                   .Where(k => k.ComplianceId == item.ComplianceId
                                                            && k.CustomerBranchID == customerBranchID
                                                            && k.IsDeleted == false).Any();

                            if (isExists == false)
                            {
                                if (item.Performer.UserID > 0 && item.Reviewer.UserID > 0)
                                {
                                    #region Generate InstanceID

                                    DateTime dtStartDate = Convert.ToDateTime(ScheduleOnDate);
                                    string StartDate = Convert.ToDateTime(dtStartDate).ToString("dd-MM-yyyy");

                                    int branchID = customerBranchID;

                                    com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance instance = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance();
                                    instance.ComplianceId = item.ComplianceId;
                                    instance.CustomerBranchID = branchID;
                                    instance.DirectorId = 0;
                                    instance.ScheduledOn = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    instance.IsSecretarial = true;

                                    com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment assignment = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment();
                                    assignment.UserID = item.Performer.UserID;
                                    assignment.RoleID = SecretarialConst.RoleID.PERFORMER;
                                    assignments.Add(new Tuple<com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment>(instance, assignment));

                                    com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment assignment1 = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment();
                                    assignment1.UserID = item.Reviewer.UserID;
                                    assignment1.RoleID = SecretarialConst.RoleID.REVIEWER;
                                    assignments.Add(new Tuple<com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment>(instance, assignment1));

                                    #endregion
                                }
                            }
                        }

                        if (assignments.Count != 0)
                        {
                            bool instanceSuccess = ComplianceManagement.CreateInstances_Secretarial(assignments, userID, userName, customerID);

                            if(instanceSuccess)
                            {
                                AddSchedule_BMScheduleOnNew(customerID, customerBranchID, obj.EntityID, 0, userID, "S");
                            }
                        }

                        obj.Success = true;
                        obj.Message = "Save Successfully.";
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = "Please select compliance from list.";
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Customer Branch not found.";
                }

            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure.";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public bool AddSchedule_BMScheduleOnNew(int custID,  int customerBranchID,  int entityID, int directorID, int loggedInUserID, string mappingType) 
        {
            try
            {                
                var lstScheduleOns = (from row in entities.BM_SP_GetRemainingSchedules(customerBranchID, directorID, mappingType)
                                      select row).ToList();

                if (lstScheduleOns != null)
                {
                    if (lstScheduleOns.Count > 0)
                    {
                        lstScheduleOns.ForEach(eachScheduleOn =>
                        {
                            if (eachScheduleOn.ComplianceInstanceID != null)
                                entityID = GetEntityIDByInstanceID(Convert.ToInt64(eachScheduleOn.ComplianceInstanceID));

                            if (entityID != -1)
                            {
                                var complianceScheduleOn = new BM_ComplianceScheduleOnNew
                                {
                                    ComplianceInstanceID = eachScheduleOn.ComplianceInstanceID,
                                    ScheduleOnID = eachScheduleOn.ComplianceScheduleOnID,
                                    ComplianceID = eachScheduleOn.ComplianceId,
                                    //MeetingID = meetingId,
                                    MappingType = mappingType,
                                    //MCM_ID = item.MCM_ID,
                                    //AgendaMasterID = item.AgendaMasterId,
                                    StatusId = 1,
                                    ScheduleOn = eachScheduleOn.ScheduleOn,
                                    CreatedBy = loggedInUserID,
                                    CreatedOn = DateTime.Now,
                                    IsActive = true,
                                    IsDeleted = false,
                                    EntityId = entityID,
                                    Customer_Id = custID,
                                    MeetingAgendaMappingId = 0
                                };

                                entities.BM_ComplianceScheduleOnNew.Add(complianceScheduleOn);
                                entities.SaveChanges();
                            }
                        });
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public int GetEntityIDByInstanceID(long instanceID)
        {
            try
            {
                var result = (from CI in entities.ComplianceInstances
                              join EM in entities.BM_EntityMaster
                              on CI.CustomerBranchID equals EM.CustomerBranchId
                              where CI.ID == instanceID
                              select EM.Id).FirstOrDefault();

                if (result != null)
                    return result;
                else
                    return 0;
            }            
            catch (Exception ex)
            {               
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return -1;
            }
        }

        public AgendaComplianceAssingVM CreateComplianceInstance(AgendaComplianceAssingVM obj)
        {
            try
            {
                int customerBranchID = GetCustomerBranchID(obj.EntityID, obj.CustomerID);

                if (customerBranchID > 0)
                {
                    if (obj.lst != null)
                    {
                        DateTime ScheduleOnDate = new DateTime().Date;

                        DateTime.TryParse(obj.ScheduleOnDate, out ScheduleOnDate);
                        foreach (var item in obj.lst)
                        {
                            var isExists = false;
                            var query = (from row in entities.ComplianceInstances
                                         where row.ComplianceId == item.ComplianceId &&
                                         row.CustomerBranchID == customerBranchID &&
                                         row.IsDeleted == false && row.IsSecretarial == true
                                         select row
                                        );

                            if (obj.DirectorID > 0)
                            {
                                query = query.Where(k => k.DirectorId == obj.DirectorID);
                            }

                            isExists = query.Select(k => k.ID).Any();

                            if (isExists == false)
                            {
                                if (item.Performer.UserID > 0 && item.Reviewer.UserID > 0)
                                {
                                    #region Generate Instance ID

                                    var complianceInstance = new ComplianceInstance()
                                    {
                                        //MappingType = obj.MappingType,
                                        ComplianceId = item.ComplianceId,
                                        CreatedOn = DateTime.Now,
                                        IsDeleted = false,
                                        
                                        GenerateSchedule = true,
                                        ScheduledOn = ScheduleOnDate,

                                        CustomerBranchID = customerBranchID,
                                        DirectorId = obj.DirectorID,
                                        //EntityID = obj.EntityID,
                                        //CustomerID = obj.CustomerID

                                        IsAvantis = true,
                                        IsSecretarial = true
                                    };

                                    entities.ComplianceInstances.Add(complianceInstance);
                                    entities.SaveChanges();

                                    item.ComplianceInstanceIdNew = complianceInstance.ID;

                                    #region Save Performer Reviewer
                                    if (complianceInstance.ID > 0)
                                    {
                                        var performer = new ComplianceAssignment()
                                        {
                                            ComplianceInstanceID = complianceInstance.ID,
                                            RoleID = SecretarialConst.RoleID.PERFORMER,
                                            UserID = item.Performer.UserID,
                                            //IsDeleted = false,
                                            //CreatedBy = obj.UserID,
                                            //CreatedOn = DateTime.Now
                                        };

                                        var reviewer = new ComplianceAssignment()
                                        {
                                            ComplianceInstanceID = complianceInstance.ID,
                                            RoleID = SecretarialConst.RoleID.REVIEWER,
                                            UserID = item.Reviewer.UserID,
                                            //IsDeleted = false,
                                            //CreatedBy = obj.UserID,
                                            //CreatedOn = DateTime.Now
                                        };

                                        entities.ComplianceAssignments.Add(performer);
                                        entities.ComplianceAssignments.Add(reviewer);
                                        entities.SaveChanges();
                                    }
                                    #endregion

                                    #endregion
                                }
                            }
                        }

                        obj.Success = true;
                        obj.Message = "Save Successfully.";
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = "Please select compliance from list.";
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Customer Branch not found.";
                }
                
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure.";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        #endregion

        #region Meeting level Compliance
        public IEnumerable<AgendaComplianceListVM> GetMeetingComplianceList(int entityID, int customerId)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetMeetingComplianceByEntityID(entityID, customerId)
                              select new AgendaComplianceListVM
                              {
                                  ComplianceId = row.ComplianceID,
                                  Description = row.ShortDescription,
                                  IsComplianceSelect = true,
                                  PerformerID = 0,
                                  ReviewerID = 0,
                                  EntityID = entityID,
                                  Performer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                                  Reviewer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                                  MeetingTypeId = row.MeetingTypeId,
                                  MeetingTypeName = row.MeetingTypeName
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Statutory Compliance
        public IEnumerable<StatutoryComplianceListVM> GetStatutoryComplianceList(int entityID, int customerId)
        {
            try
            {
                var result = (from row in entities.BM_SP_ComplianceStatutoryByEntityID(entityID, customerId)
                              select new StatutoryComplianceListVM
                              {
                                  ActId = row.ActID,
                                  //ActName = row.ActName,

                                  ComplianceId = row.ID,
                                  ShortDescription = row.ShortDescription,
                                  //Description = row.Description,
                                  Description = row.ShortDescription,
                                  Section = row.Sections,
                                  Risk = row.Risk,
                                  Frequency = row.Frequency,
                                  TagID = row.TagID,
                                  
                                   //IsComplianceSelect = true,
                                  IsComplianceSelect = false,
                                  PerformerID = 0,
                                  ReviewerID = 0,
                                  EntityID = entityID,
                                  Performer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                                  Reviewer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<ActVM> GetStatutoryActList(int entityID, int customerId)
        {
            try
            {
                var result = (from row in entities.BM_SP_ComplianceStatutoryActsByEntityID(entityID, customerId)
                              select new ActVM
                              {
                                  ActId = row.ActID,
                                  ActName = row.ActName,
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Compliances Re-Assignment
        public IEnumerable<ComplianceReAssignmentListVM> GetAssignedCompliancesDetails(int entityID, int customerID, string mappingType, int userID)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetAssignedComplianceDetails(entityID, customerID, mappingType)
                              where row.EntityID == entityID && row.UserID == userID
                              select new ComplianceReAssignmentListVM
                              {
                                  ComplianceAssignmentId = row.ComplianceAssignmentID,
                                  ComplianceId = row.ComplianceID,
                                  Description = row.ShortDescription,
                                  User = new PerformerReviewerViewModel()
                                  {
                                      UserID = row.UserID,
                                      FullName = row.UserName
                                  },
                                  RoleID = row.RoleID,
                                  UserRole = row.UserRole,
                                  IsComplianceSelect = true
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ComplianceReAssingmentVM ComplianceReAssignment(ComplianceReAssingmentVM obj)
        {
            try
            {
                if (obj.lst != null)
                {
                    foreach (var item in obj.lst)
                    {
                        var objDetails = entities.ComplianceAssignments.Where(k => k.ID == item.ComplianceAssignmentId).FirstOrDefault();
                        if (objDetails != null)
                        {
                            objDetails.UserID = item.User.UserID;

                            //objDetails.UpdatedBy = obj.UpdatedBy;
                            //objDetails.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }
                    }
                    obj.Success = true;
                    obj.Message = "Saved Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Compliance Upload
        public UploadComplianceVM CreateInstance(int customerId, int entityId, long complianceId, string performerEmail, string reviewerEmail, string scheduleOn, int createdBy)
        {
            var obj = new UploadComplianceVM() { EntityID = entityId, ComplianceId = complianceId, PerformerMail = performerEmail, ReviewerMail = reviewerEmail, ScheduleOn = scheduleOn, IsValid = true };

            try
            {
                var SrNo = 0;
                int CustomerBranchId = 0;
                if (entityId == 0)
                {
                    obj.IsValid = false;
                    SrNo++;
                    obj.ValidationMessage += SrNo + ". Invalid Entity ID. <br/> ";
                }
                else
                {
                    #region check Entity
                    var entity = (from row in entities.BM_EntityMaster
                                  where row.Id == entityId && row.Customer_Id == customerId && row.Is_Deleted == false
                                  select new { row.CompanyName, row.CustomerBranchId }).FirstOrDefault();

                    if (entity != null)
                    {
                        obj.EntityName = entity.CompanyName;
                        CustomerBranchId = (int)entity.CustomerBranchId;
                    }
                    else
                    {
                        obj.IsValid = false;
                        SrNo++;
                        obj.ValidationMessage += SrNo + ". Entity name not found. <br/> ";
                    }

                    #endregion
                }

                if (complianceId == 0)
                {
                    obj.IsValid = false;
                    SrNo++;
                    obj.ValidationMessage += SrNo + ". Invalid Compliance ID. <br/> ";
                }
                else
                {
                    #region Check Compliance

                    var shortDescription = (from row in entities.Compliances
                                            where row.ID == complianceId && row.IsDeleted == false
                                            select row.ShortDescription).FirstOrDefault();

                    if (string.IsNullOrEmpty(shortDescription))
                    {
                        obj.IsValid = false;
                        SrNo++;
                        obj.ValidationMessage += SrNo + ". Compliance not found. <br/> ";
                    }
                    else
                    {
                        obj.Description = shortDescription;

                        #region check Complaince Assigned or not

                        var isInstanceCreated = (from row in entities.ComplianceInstances
                                                 where row.CustomerBranchID == CustomerBranchId && row.ComplianceId == complianceId
                                                 select row.CustomerBranchID).Any();

                        if (isInstanceCreated)
                        {
                            obj.IsValid = false;
                            SrNo++;
                            obj.ValidationMessage += SrNo + ". Compliance all ready assigned. <br/> ";
                        }

                        #endregion
                    }
                    #endregion
                }

                if (string.IsNullOrEmpty(performerEmail))
                {
                    obj.IsValid = false;
                    SrNo++;
                    obj.ValidationMessage += SrNo + ". Performer mail required. <br/> ";
                }
                else
                {
                    #region Check Performer Mail
                    var performer = (from row in entities.Users
                                     where row.CustomerID == customerId && row.Email == performerEmail && row.IsActive == true && row.IsDeleted == false
                                     select new PerformerReviewerViewModel
                                     {
                                         UserID = row.ID,
                                         FullName = row.FirstName + " " + row.LastName
                                     }).FirstOrDefault();

                    if (performer != null)
                    {
                        obj.Performer = performer;
                    }
                    else
                    {
                        obj.IsValid = false;
                        SrNo++;
                        obj.ValidationMessage += SrNo + ". Performer not found. <br/> ";
                    }
                    #endregion
                }

                if (string.IsNullOrEmpty(reviewerEmail))
                {
                    obj.IsValid = false;
                    SrNo++;
                    obj.ValidationMessage += SrNo + ". Reviewer mail required. <br/> ";
                }
                else
                {
                    #region Check Reviewer Mail
                    var reviewer = (from row in entities.Users
                                    where row.CustomerID == customerId && row.Email == reviewerEmail && row.IsActive == true && row.IsDeleted == false
                                    select new PerformerReviewerViewModel
                                    {
                                        UserID = row.ID,
                                        FullName = row.FirstName + " " + row.LastName
                                    }).FirstOrDefault();

                    if (reviewer != null)
                    {
                        obj.Reviewer = reviewer;
                    }
                    else
                    {
                        obj.IsValid = false;
                        SrNo++;
                        obj.ValidationMessage += SrNo + ". Reviewer not found. <br/> ";
                    }
                    #endregion
                }

                if (string.IsNullOrEmpty(scheduleOn))
                {
                    obj.IsValid = false;
                    SrNo++;
                    obj.ValidationMessage += SrNo + ". Start Date required.";
                }
                else
                {
                    DateTime scheduleDate;
                    if (DateTime.TryParseExact(scheduleOn, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out scheduleDate))
                    {
                        obj.ScheduleOnDate = scheduleDate;
                    }
                    else
                    {
                        obj.IsValid = false;
                        SrNo++;
                        obj.ValidationMessage += SrNo + ". " + scheduleOn + " is an invalid date format.";
                    }
                }
            }
            catch (Exception ex)
            {
                obj.IsValid = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        public bool CreateInstance(List<UploadComplianceVM> lst, int customerId, int createdBy)
        {
            foreach (var obj in lst)
            {
                if (obj.IsValid)
                {
                    #region Generate Instance ID

                    int customerBranchID = GetCustomerBranchID(obj.EntityID, customerId);

                    var complianceInstance = new ComplianceInstance()
                    {
                        ComplianceId = obj.ComplianceId,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false,
                        GenerateSchedule = true,
                        ScheduledOn = obj.ScheduleOnDate,

                        CustomerBranchID = customerBranchID,
                        //EntityID = obj.EntityID,
                        //CustomerID = customerId,

                        IsAvantis = true,
                        IsSecretarial = true
                    };

                    entities.ComplianceInstances.Add(complianceInstance);
                    entities.SaveChanges();

                    #region Save Performer Reviewer
                    if (complianceInstance.ID > 0)
                    {
                        var performer = new ComplianceAssignment()
                        {
                            ComplianceInstanceID = complianceInstance.ID,
                            RoleID = SecretarialConst.RoleID.PERFORMER,
                            UserID = obj.Performer.UserID
                            //IsDeleted = false,
                            //CreatedBy = createdBy,
                            //CreatedOn = DateTime.Now
                        };

                        var reviewer = new ComplianceAssignment()
                        {
                            ComplianceInstanceID = complianceInstance.ID,
                            RoleID = SecretarialConst.RoleID.REVIEWER,
                            UserID = obj.Reviewer.UserID
                            //IsDeleted = false,
                            //CreatedBy = createdBy,
                            //CreatedOn = DateTime.Now
                        };

                        entities.ComplianceAssignments.Add(performer);
                        entities.ComplianceAssignments.Add(reviewer);
                        entities.SaveChanges();
                    }
                    #endregion

                    #endregion
                }
            }
            return true;
        }
        #endregion

        #region Director Related Compliance
        public IEnumerable<StatutoryComplianceListVM> GetDirectorComplianceList(long directorId, int customerId)
        {
            try
            {
                var result = (from row in entities.BM_SP_ComplianceDirectorByID(directorId, customerId)
                              select new StatutoryComplianceListVM
                              {
                                  ActId = row.ActID,
                                  //ActName = row.ActName,

                                  ComplianceId = row.ID,
                                  ShortDescription = row.ShortDescription,
                                  //Description = row.Description,
                                  Description = row.ShortDescription,
                                  Section = row.Sections,
                                  Risk = row.Risk,
                                  Frequency = row.Frequency,

                                  IsComplianceSelect = true,
                                  PerformerID = 0,
                                  ReviewerID = 0,
                                  DirectorID = directorId,
                                  EntityID =(int) row.EntityId,
                                  CustomerBranchID = row.CustomerBranchId,
                                  EntityName = row.CompanyName,
                                  Performer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                                  Reviewer = new PerformerReviewerViewModel()
                                  {
                                      UserID = 0,
                                      FullName = ""
                                  },
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public AgendaComplianceAssingVM CreateComplianceInstanceForDirector(AgendaComplianceAssingVM obj)
        {
            try
            {
                if (obj.DirectorID > 0)
                {
                    if (obj.lst != null)
                    {
                        int customerID = obj.CustomerID;
                        int userID = obj.UserID;
                        string userName = obj.UserName;

                        int directorID = 0;

                        DateTime ScheduleOnDate = new DateTime().Date;

                        DateTime.TryParse(obj.ScheduleOnDate, out ScheduleOnDate);

                        List<Tuple<com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment>> assignments
                            = new List<Tuple<com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment>>();

                        foreach (var item in obj.lst)
                        {
                            if (obj.DirectorID != null)
                                directorID = Convert.ToInt32(obj.DirectorID);

                            var isExists = entities.ComplianceInstances
                                                   .Where(k => k.ComplianceId == item.ComplianceId 
                                                        && k.DirectorId == obj.DirectorID
                                                        //k.DirectorId == obj.DirectorID && k.CustomerBranchID == 0 &&
                                                        && k.CustomerBranchID == item.CustomerBranchID &&
                                                        k.IsDeleted == false).Any();

                            if (isExists == false)
                            {
                                if (item.Performer.UserID > 0 && item.Reviewer.UserID > 0)
                                {
                                    #region Generate InstanceID

                                    DateTime dtStartDate = Convert.ToDateTime(ScheduleOnDate);
                                    string StartDate = Convert.ToDateTime(dtStartDate).ToString("dd-MM-yyyy");

                                    com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance instance = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance();
                                    instance.ComplianceId = item.ComplianceId;
                                    instance.CustomerBranchID = (int)item.CustomerBranchID;
                                    instance.ScheduledOn = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    instance.DirectorId = obj.DirectorID;
                                    instance.IsDeleted = false;
                                    instance.IsSecretarial = true;
                                    instance.IsAvantis = true;
                                    instance.GenerateSchedule = true;

                                    com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment assignment = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment();
                                    assignment.UserID = item.Performer.UserID;
                                    assignment.RoleID = SecretarialConst.RoleID.PERFORMER;
                                    assignments.Add(new Tuple<com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment>(instance, assignment));

                                    com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment assignment1 = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment();
                                    assignment1.UserID = item.Reviewer.UserID;
                                    assignment1.RoleID = SecretarialConst.RoleID.REVIEWER;
                                    assignments.Add(new Tuple<com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceInstance, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceAssignment>(instance, assignment1));

                                    #endregion
                                }
                            }                           
                        }

                        if (assignments.Count != 0)
                        {
                            bool instanceSuccess = ComplianceManagement.CreateInstances_Secretarial(assignments, userID, userName, customerID);

                            if (instanceSuccess && directorID!= 0)
                            {
                                AddSchedule_BMScheduleOnNew(customerID, 0, obj.EntityID, directorID, userID, "D");
                            }
                        }

                        obj.Success = true;
                        obj.Message = "Save Successfully.";
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = "Please select compliance from list.";
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Customer Branch not found.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure.";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public IEnumerable<ComplianceAssignedListVM> GetAssignedCompliancesDirector(long directorId, int customerId)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetAssignedComplianceDirector(directorId, customerId)
                              select new ComplianceAssignedListVM
                              {
                                  EntityID = row.EntityId,
                                  CompanyName = row.CompanyName,
                                  DirectorID = row.DirectorId,
                                  ComplianceId = row.ComplianceID,
                                  Description = row.Description,
                                  Performer = new PerformerReviewerViewModel()
                                  {
                                      UserID = row.PerformerID,
                                      FullName = row.Performer
                                  },
                                  Reviewer = new PerformerReviewerViewModel()
                                  {
                                      UserID = row.ReviewerID,
                                      FullName = row.Reviewer
                                  }
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Compliances Re-Assignment (Director)
        public IEnumerable<ComplianceReAssignmentListVM> GetAssignedCompliancesDetailsForDirector(long directorId, int customerID, int userID)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetAssignedComplianceDetailsForDirector(directorId, customerID)
                              where row.UserID == userID
                              select new ComplianceReAssignmentListVM
                              {
                                  EntityId = row.EntityId,
                                  ComplianceAssignmentId = row.ComplianceAssignmentID,
                                  ComplianceId = row.ComplianceID,
                                  Description = row.ShortDescription,
                                  User = new PerformerReviewerViewModel()
                                  {
                                      UserID = row.UserID,
                                      FullName = row.UserName
                                  },
                                  RoleID = row.RoleID,
                                  UserRole = row.UserRole,
                                  IsComplianceSelect = true,
                                  CompanyName = row.CompanyName
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ComplianceReAssingmentVM ComplianceReAssignmentForDirector(ComplianceReAssingmentVM obj)
        {
            try
            {
                if (obj.lst != null)
                {
                    foreach (var item in obj.lst)
                    {
                        var objDetails = entities.ComplianceAssignments.Where(k => k.ID == item.ComplianceAssignmentId).FirstOrDefault();
                        if (objDetails != null)
                        {
                            objDetails.UserID = item.User.UserID;

                            //objDetails.UpdatedBy = obj.UpdatedBy;
                            //objDetails.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }
                    }
                    obj.Success = true;
                    obj.Message = "Saved Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Get Compliance
        public List<VMCompliences> BindCompliancesNew(long? AgendaId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {

                var compliancesQuery = (from row in entities.BM_ComplianceListView

                                        select new VMCompliences
                                        {
                                            ID = row.ID
                                                ,
                                            ActID = row.ActID
                                                ,
                                            ActName = row.Name
                                                ,
                                            Description = row.Description
                                                ,
                                            Sections = row.Sections != null ? row.Sections.ToUpper() : row.Sections
                                                ,
                                            ComplianceType = row.ComplianceType
                                                ,
                                            UploadDocument = row.UploadDocument
                                                ,
                                            NatureOfCompliance = row.NatureOfCompliance
                                                ,
                                            ShortDescription = row.ShortDescription,
                                            RiskType = row.Risk,
                                            Frequency = row.Frequency,

                                            AgendaId = AgendaId,
                                            HasForms = (from C in entities.BM_ComplianceFormMapping where C.ComplianceId == row.ID && C.IsDeleted == false select C).Any(),
                                        }).ToList();

                return compliancesQuery;
            }
        }
        public IEnumerable<ActListViewVM> GetActName()
        {
            var acts = (from row in entities.BM_ActListView
                            ////where row.IsDeleted == false
                        orderby row.Name ascending
                        select new ActListViewVM
                        {
                            ID = row.ID,
                            Name = row.Name
                        }).OrderBy(entry => entry.Name).ToList();
            return acts;
        }
        #endregion

        #region My Compliances
        public IEnumerable<MyComplianceListVM> GetMyCompliances(MyCompliancesVM obj, int userId, int customerId)
        {
            try
            {
                return (from row in entities.BM_SP_Compliance_MyCompliancesNew(obj.StatusId, userId, obj.RoleId, customerId)
                        //from row in entities.BM_SP_Compliance_MyCompliances(obj.StatusId, userId, obj.RoleId, customerId)
                              select new MyComplianceListVM
                              {
                                  EntityId = row.EntityId,
                                  DirectorId = row.DirectorId,
                                  ScheduleOnID = row.ScheduleOnID,
                                  RoleID = row.RoleID,
                                  MappingType = row.MappingType,
                                  CompanyName = row.CompanyName,
                                  MeetingID = row.MeetingID,
                                  MeetingTitle = row.MeetingTitle,
                                  ShortDescription = row.ShortDescription,
                                  Description = row.Description,
                                  ScheduleOn = row.ScheduleOn,
                                  ScheduleOnTime = row.ScheduleOnTime,
                                  StatusName = row.Status_
                              }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<MyComplianceListVM> GetMyCompliances(int userID, int customerID, int statusID, int roleID)
        {
            try
            {
                return (from row in entities.BM_SP_DirectorMyCompliance(userID, customerID, null)
                        select new MyComplianceListVM
                        {
                            MappingType = row.MappingType,
                            EntityId = row.EntityId,
                            CompanyName = row.CompanyName,
                            DirectorId = row.DirectorId,
                            DirectorName = row.DirectorName,
                            MeetingID = row.MeetingID,
                            MeetingTitle = row.MeetingTitle,
                            //ComplianceID=row.
                            ShortForm = row.ShortForm,
                            ShortDescription = row.ShortDescription,
                            Description = row.Description,
                            RoleID = row.RoleID,
                            ScheduleOnID = row.ScheduleOnID,
                            ScheduleOn = row.ScheduleOn,
                            ScheduleOnTime = row.ScheduleOnTime,
                            StatusName = row.ComplianceStatus,
                            RequiredForms = row.RequiredForms,
                            Frequency = row.Frequency,
                            Period = row.ForMonth,
                        }).ToList();

                //return (from row in entities.BM_SP_MyCompliance_Director(userID, customerID, statusID, roleID, 2, null)
                //        select new MyComplianceListVM
                //        {
                //            MappingType = row.MappingType,
                //            EntityId = row.EntityId,
                //            CompanyName = row.CompanyName,
                //            DirectorId = row.DirectorId,
                //            DirectorName = row.DirectorName,
                //            MeetingID = row.MeetingID,
                //            MeetingTitle = row.MeetingTitle,
                //            //ComplianceID=row.
                //            ShortForm = row.ShortForm,
                //            ShortDescription = row.ShortDescription,
                //            Description = row.Description,
                //            RoleID = row.RoleID,
                //            ScheduleOnID = row.ScheduleOnID,
                //            ScheduleOn = row.ScheduleOn,
                //            ScheduleOnTime = row.ScheduleOnTime,
                //            StatusName = row.ComplianceStatus,
                //            RequiredForms = row.RequiredForms,
                //            Frequency = row.Frequency,
                //            Period = row.ForMonth,
                //        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_MyComplianceDocuments_Result> GetMyDocuments(int userID, int customerID, int? statusID, int showAll, string userRole)
        {
            try
            {
                return entities.BM_SP_MyComplianceDocuments(userID, customerID, statusID, showAll, userRole).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_DirectorComplianceDocuments_Result> GetMyDocuments_Director(int userID, int customerID, int statusID, int roleID)
        {
            try
            {
                return entities.BM_SP_DirectorComplianceDocuments(userID, customerID, statusID, roleID, 2).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_DirectorComplianceStatusReport_Result> GetComplianceStatusReport_Director(int userID, int customerID, int? statusID, int showAll)
        {
            try
            {
                return entities.BM_SP_DirectorComplianceStatusReport(userID, customerID, statusID, showAll).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_ComplianceStatusReport_Result> GetComplianceStatusReport(int userID, int customerID, int? statusID, int showAll, string userRole)
        {
            try
            {
                return entities.BM_SP_ComplianceStatusReport(userID, customerID, statusID, showAll, userRole).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_DirectorAttendanceReport_Result> GetAttendanceReport_Director(int userID, int customerID)
        {
            try
            {
                return entities.BM_SP_DirectorAttendanceReport(userID, customerID).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_AttendanceReport_Result> GetAttendanceReport(int userID, int customerID, string userRole)
        {
            try
            {
                return entities.BM_SP_AttendanceReport(userID, customerID, userRole).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion

        #region ScheduleOnId for Meeting level Compliace
        public long GetScheduleOnIDForMeetingEvent(long meetingID, string autoCompleteOn)
        {
            long result = 0;
            try
            {
                result = (from scheduleOn in entities.BM_ComplianceScheduleOnNew
                          join mapping in entities.BM_ComplianceMapping on scheduleOn.MCM_ID equals mapping.MCM_ID
                          where scheduleOn.MeetingID == meetingID && scheduleOn.MappingType == SecretarialConst.ComplianceMappingType.MEETING
                                  && mapping.AutoCompleteOn == autoCompleteOn
                                  && scheduleOn.IsActive == true && scheduleOn.IsDeleted == false
                          select scheduleOn.ScheduleOnID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Get file data list by scheduleOnID
        public List<FileDataVM> FileDataListByScheduleOnID(long schdeuleOnId)
        {
            var result = new List<FileDataVM>();
            try
            {
                result = (from mapping in entities.FileDataMappings
                          join file in entities.FileDatas on mapping.FileID equals file.ID
                          where mapping.ScheduledOnID == schdeuleOnId && file.IsDeleted == false
                          select new FileDataVM
                          {
                              FileID = file.ID,
                              FileName = file.Name,
                              Version = file.Version
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

public List<SecretarialTag_VM> GetSecretarialTagList()
        {
            try
            {
                var result = (from row in entities.Compliance_SecretarialTagMaster
                              where row.IsActive == true
                              select new SecretarialTag_VM
                              {
                                  ID = row.ID,
                                  Tag = row.Tag,
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
    }
}