﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM.Compliance;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Compliance
{
    public class MeetingComplianceMapping : IMeetingComplianceMapping
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public List<VMCompliences> BindCompliancesNew(int meetingTypeId, int entityTypeId)
        {
            try
            {
                var compliancesQuery = (from row in entities.BM_ComplianceListView

                                        select new VMCompliences
                                        {
                                            ID = row.ID
                                                ,
                                            ActID = row.ActID
                                                ,
                                            ActName = row.Name
                                                ,
                                            Description = row.Description
                                                ,
                                            Sections = row.Sections
                                                ,
                                            ComplianceType = row.ComplianceType
                                                ,
                                            UploadDocument = row.UploadDocument
                                                ,
                                            NatureOfCompliance = row.NatureOfCompliance
                                                ,
                                            ShortDescription = row.ShortDescription,
                                            RiskType = row.Risk,
                                            Frequency = row.Frequency,

                                            EntityTypeId = entityTypeId,
                                            MeetingTypeId = meetingTypeId
                                        }).ToList();

                return compliancesQuery;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<VMCompliences> obj = new List<VMCompliences>();
                return obj;
            }
        }

        public List<AgendaComplianceEditorVM> GetComplianceMappingDetails(int entitytypeId, long meetingTypeID)
        {
            try
            {
                var getAgendaMappingDetails = (from row in entities.BM_ComplianceMapping
                                               where row.EntityType == entitytypeId && row.MeetingTypeID == meetingTypeID
                                               && row.MappingType=="M"

                                               select new AgendaComplianceEditorVM
                                               {
                                                   ComplianceId = row.ComplianceID,
                                                   ComplianceShortDesc = (from com in entities.Compliances where com.ID == row.ComplianceID select com.ShortDescription).FirstOrDefault(),
                                                   EntityTypeId= entitytypeId,
                                                   MeetingType= (int)meetingTypeID,
                                                   AgendaMasterId = row.AgendaMasterID,
                                                   Numbers = row.Numbers,
                                                   decendingNo = row.DuesNoIn_dsending,

                                                   DayTypeNames = new DayTypeViewModel
                                                   {
                                                       DayType = row.DayType,
                                                       DayTypeName = row.DayType == "" ? "" : row.DateType == null ? "" : row.DayType == "W" ? "Clear Working Day(s)" : "Normal Day(s)",
                                                   },
                                                   BeforeAfterNames = new BeforeAfterViewModel
                                                   {
                                                       BeforeAfter = row.BeforeAfter,
                                                       BeforeAfterName = row.BeforeAfter == "" ? "" : row.BeforeAfter == null ? "" : row.BeforeAfter == "A" ? "After" : "Before",
                                                   },
                                                   DaysOrHoursNames = new DaysOrHoursViewModel
                                                   {
                                                       DaysOrHours = row.DaysOrHours,
                                                       DaysOrHoursName = row.DaysOrHours == "" ? "" : row.DaysOrHours == null ? "" : row.DaysOrHours == "D" ? "Day(s)" : row.DaysOrHours == "H" ? "Hour(s)" : "Minute(s)",
                                                   },
                                                   MeetingLevel=new MeetingViewModel
                                                   {
                                                       MeetingLevelId=row.AutoCompleteOn,
                                                       MeetingLevelName=row.AutoCompleteOn == "" ? "" : row.AutoCompleteOn == null ? "" : row.AutoCompleteOn,
                                                   },
                                                   Autoclose = row.AutoComplete

                                               }).ToList();

                if (getAgendaMappingDetails.Count > 0)
                {
                    getAgendaMappingDetails = getAgendaMappingDetails.OrderBy(entry => entry.decendingNo)

                       .ToList();
                }
                return getAgendaMappingDetails;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<AgendaComplianceEditorVM> obj = new List<AgendaComplianceEditorVM>();
                return obj;
            }
        }

        public List<VM_MeetingCamplianceMapping> GetMeetingType()
        {
            try
            {
                var GetMeetingType = (from row in entities.BM_CommitteeComp
                                      where row.IsDeleted == false && row.Customer_Id == null
                                      select new VM_MeetingCamplianceMapping
                                      {
                                          Id = row.Id,
                                          MeetingTypeName = row.MeetingTypeName,
                                          // IsMeeting_Circular=row.IsMeeting
                                      }).ToList();
                return GetMeetingType;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<VM_MeetingCamplianceMapping> obj = new List<VM_MeetingCamplianceMapping>();
                return obj;
            }
        }

        public void AddMeetingItem(VMCompliences complianceItem, int userID, int customerID)
        {
            try
            {
                var checkCompliancemappingagendawise = (from row in entities.BM_ComplianceMapping
                                                        where row.MeetingTypeID == complianceItem.MeetingTypeId
                                                        && row.EntityType== complianceItem.EntityTypeId

              && row.MappingType == "M"
              && row.ComplianceID == complianceItem.ID
                                                        select row).FirstOrDefault();
                if (checkCompliancemappingagendawise == null)
                {
                    BM_ComplianceMapping objcompliancemapping = new BM_ComplianceMapping();

                    objcompliancemapping.MeetingTypeID = complianceItem.MeetingTypeId;
                    objcompliancemapping.EntityType = complianceItem.EntityTypeId;
                    objcompliancemapping.MappingType = "M";
                    objcompliancemapping.ComplianceID = complianceItem.ID;
                    objcompliancemapping.AutoComplete = false;
                    objcompliancemapping.IsDeleted = false;
                    objcompliancemapping.CreatedBy = userID;
                    objcompliancemapping.CreatedOn = DateTime.Now;
                    entities.BM_ComplianceMapping.Add(objcompliancemapping);
                    entities.SaveChanges();
                }
                else
                {
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Binding Static DropDown For ComplianceMapping added by Ruchi
        public IEnumerable<DayTypeViewModel> getDayType(string dayhoureId)
        {
            List<DayTypeViewModel> listDayType = new List<DayTypeViewModel>();
            if (dayhoureId == "D")
            {
                listDayType = new List<DayTypeViewModel>
            {
          new DayTypeViewModel {DayTypeName="Select",DayType="" },
          new DayTypeViewModel {DayTypeName="Clear Working Day(s)",DayType="W" },
          new DayTypeViewModel {DayTypeName="Normal Day(s)",DayType="C" },
            };
            }
            else
            {
                listDayType = new List<DayTypeViewModel>
                {
                new DayTypeViewModel { DayTypeName = "Select", DayType = "" }
                };
            }
            return listDayType;
        }

        public IEnumerable<DaysOrHoursViewModel> GetHoureMinutesDay()
        {
            var listDayorHoure = new List<DaysOrHoursViewModel>
         {
          new DaysOrHoursViewModel {DaysOrHoursName="Select",DaysOrHours="" },
          new DaysOrHoursViewModel {DaysOrHoursName="Day(s)",DaysOrHours="D" },
          new DaysOrHoursViewModel {DaysOrHoursName="Hour(s)",DaysOrHours="H" },
          new DaysOrHoursViewModel {DaysOrHoursName="Minute(s)",DaysOrHours="M" }
         };
            return listDayorHoure;
        }

        #endregion

        public void AddMeetingMappingDetails(AgendaComplianceEditorVM complianceItem, int userID, int customerID)
        {
            
            try
            {
                var checkCompliancedetails = (from row in entities.BM_ComplianceMapping
                                              where row.MeetingTypeID == complianceItem.MeetingType
                                              && row.EntityType== complianceItem.EntityTypeId
                                              && row.MappingType == "M"
                                              && row.ComplianceID == complianceItem.ComplianceId
                                              select row).FirstOrDefault();
                if (checkCompliancedetails != null)
                {
                    checkCompliancedetails.MeetingTypeID = complianceItem.MeetingType;
                    checkCompliancedetails.EntityType = complianceItem.EntityTypeId;
                    checkCompliancedetails.MappingType = SecretarialConst.ComplianceMappingType.MEETING;
                    checkCompliancedetails.DateType = SecretarialConst.ComplianceDateType.MEETINGDATE;

                    checkCompliancedetails.ComplianceID = complianceItem.ComplianceId;
                    checkCompliancedetails.AutoComplete = complianceItem.Autoclose;
                    checkCompliancedetails.BeforeAfter = complianceItem.BeforeAfterNames.BeforeAfter;
                    //

                    checkCompliancedetails.Numbers = complianceItem.Numbers;
                    checkCompliancedetails.DaysOrHours = complianceItem.DaysOrHoursNames.DaysOrHours;
                    if (complianceItem.DaysOrHoursNames.DaysOrHours == "D")
                    {
                        checkCompliancedetails.DayType = complianceItem.DayTypeNames.DayType;
                    }
                    else
                    {
                        checkCompliancedetails.DayType = "";
                    }
                    checkCompliancedetails.UpdatedOn = DateTime.Now;
                    checkCompliancedetails.UpdatedBy = userID;
                    checkCompliancedetails.CustomerID = customerID;
                    if (complianceItem.BeforeAfterNames.BeforeAfter == "A")
                    {
                        checkCompliancedetails.DuesNoIn_dsending = (int)complianceItem.Numbers;
                    }
                    else if (complianceItem.BeforeAfterNames.BeforeAfter == "B")
                    {
                        checkCompliancedetails.DuesNoIn_dsending = (int)-complianceItem.Numbers;
                    }
                    if (complianceItem.MeetingLevel != null)
                        checkCompliancedetails.AutoCompleteOn = complianceItem.MeetingLevel.MeetingLevelId;

                    checkCompliancedetails.IsDeleted = false;
                    entities.SaveChanges();
                }
                else
                {
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
    }
