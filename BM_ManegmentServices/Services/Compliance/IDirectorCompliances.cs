﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Compliance
{
    public interface IDirectorCompliances
    {
        long? GetInstanceIdForDirector(long directorId, long complianceId);
        #region Director Schedule on
        Message GenerateDirectorScheduleOn(long directorId, string generateScheduleFor, DateTime closedDate, int createdBy, string userName, int? fileId, int? assignTo);
        Message GenerateDirectorScheduleOn(long directorId, string generateScheduleFor, DateTime closedDate, int createdBy, string userName, int? fileId, int? assignTo, DateTime? eventDateTime);
        Message GenerateScheduleOnForEntityOnEvent(int customerId, int entityId, string generateScheduleFor, DateTime closedDate, int createdBy, string userName, int? fileId, int? performerId, int? reviewerId, DateTime? eventDateTime, long? directorId = 0, string refMaster = null, long? refMasterId = null);
        #endregion

        #region Director Compliances performer user
        long? GetPerformer(long directorId, string generateScheduleFor);
        #endregion
    }
}
