﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Compliance
{
    public class ComplianceTransaction_Service : IComplianceTransaction_Service
    {
        IFileData_Service objIFileData_Service;
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public ComplianceTransaction_Service(IFileData_Service objFileData_Service)
        {
            objIFileData_Service = objFileData_Service;
        }

        public ComplianceTransactionVM CreateTransaction(ComplianceTransactionVM obj)
        {
            try
            {
                long? tempMeetingId = null;
                DateTime closedDate = obj.Dated;
                if (!string.IsNullOrEmpty(obj.ClosedTime))
                {
                    if (!DateTime.TryParse(obj.Dated.ToString("dd/MMM/yyyy") + " " + obj.ClosedTime, out closedDate))
                    {
                        closedDate = obj.Dated;
                    }
                }

                var instaceId = (from row in entities.BM_ComplianceScheduleOnNew
                                 where row.ScheduleOnID == obj.ComplianceScheduleOnID
                                 select row.ComplianceInstanceID).FirstOrDefault();

                if(instaceId > 0)
                {
                    ComplianceTransaction _obj = new ComplianceTransaction()
                    {
                        ComplianceInstanceId = (long)instaceId,
                        ComplianceScheduleOnID = obj.ComplianceScheduleOnID,
                        StatusId = obj.StatusId,
                        Remarks =  string.IsNullOrEmpty(obj.Remarks) ? "" : obj.Remarks,
                        //Dated = obj.Dated,
                        Dated = closedDate,
                        CreatedBy = obj.CreatedBy,
                        CreatedByText = obj.CreatedByText,
                        StatusChangedOn = DateTime.Now
                    };

                    entities.ComplianceTransactions.Add(_obj);
                    entities.SaveChanges();

                    obj.TransactionID = _obj.ID;

                    if (obj.TransactionID > 0)
                    {
                        var schedule = entities.BM_ComplianceScheduleOnNew.Where(k => k.ScheduleOnID == obj.ComplianceScheduleOnID).FirstOrDefault();
                        schedule.StatusId = obj.StatusId;
                        if (obj.StatusId == 4 || obj.StatusId == 5)
                        {
                            #region Set Open Agenda On Compliance Completion
                            var OnComplianceCompletion = (from row in entities.ComplianceScheduleOns
                                                          join instance in entities.ComplianceInstances on row.ComplianceInstanceID equals instance.ID
                                                          join mapping in entities.BM_ComplianceMapping on instance.ComplianceId equals mapping.ComplianceID
                                                          join agenda in entities.BM_AgendaMaster on mapping.AgendaMasterID equals agenda.BM_AgendaMasterId
                                                          where row.ID == obj.ComplianceScheduleOnID && agenda.FrequencyId == 15
                                                          select new
                                                          {
                                                              //row.EntityId,
                                                              agenda.BM_AgendaMasterId,
                                                              agenda.EntityType,
                                                              agenda.MeetingTypeId
                                                          }).FirstOrDefault();

                            if (OnComplianceCompletion != null)
                            {
                                var openAgenda = new BM_MeetingOpenAgendaOnCompliance()
                                {
                                    Id = 0,
                                    ScheduleOnID = obj.ComplianceScheduleOnID,
                                    EntityId = (int)obj.EntityID_,
                                    AgendaId = OnComplianceCompletion.BM_AgendaMasterId,
                                    MeetingTypeId = (int)OnComplianceCompletion.MeetingTypeId,
                                    IsNoted = false,
                                    IsDeleted = false,
                                    CreatedBy = (int)obj.CreatedBy,
                                    CreatedOn = DateTime.Now
                                };

                                entities.BM_MeetingOpenAgendaOnCompliance.Add(openAgenda);
                                entities.SaveChanges();
                            }
                            #endregion
                            schedule.ClosedDate = closedDate;

                            tempMeetingId = schedule.MeetingID;
                        }
                        entities.SaveChanges();

                        if(tempMeetingId > 0)
                        {
                            entities.BM_SP_ComplianceTransactionOnClosed(tempMeetingId, (int)obj.CreatedBy);
                            entities.SaveChanges();
                        }
                    }
                    obj.Success = true;
                    obj.Message = "Save Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something went wrong";
                }
                
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public List<ComplianceTransactionVM> CreateTransactionOnMeetingEvent(long meetingID, string autoCompleteON)
        {
            return CreateTransactionOnMeetingEvent(meetingID, autoCompleteON, true);
        }

        public List<ComplianceTransactionVM> CreateTransactionOnMeetingEvent(long meetingID, string autoCompleteON, bool autoClose)
        {
            return CreateTransactionOnMeetingEvent(meetingID, autoCompleteON, autoClose, 0, DateTime.Now, null, 0);
        }

        public List<ComplianceTransactionVM> CreateTransactionOnMeetingEvent(long meetingID, string autoCompleteON, bool autoClose, int statusId, DateTime dated, FileDataVM objFileData, int userID)
        {
            var lstComplianceTransaction = new List<ComplianceTransactionVM>();
            long? tempMeetingId = null;
            long? complianceScheduleOnID = null, transactionID = null;
            try
            {
                //Get List of all schedule to Perform on Meeting Event such as (Notice Send, Agenda Send, Meeting Date)
                var result = (from scheduleOn in entities.BM_ComplianceScheduleOnNew
                              join mapping in entities.BM_ComplianceMapping on scheduleOn.MCM_ID equals mapping.MCM_ID
                              where scheduleOn.MeetingID == meetingID && scheduleOn.MappingType == SecretarialConst.ComplianceMappingType.MEETING
                                     //&& scheduleOn.StatusId == null 
                                     && mapping.AutoCompleteOn == autoCompleteON
                                     && scheduleOn.IsActive == true && scheduleOn.IsDeleted == false
                              select new
                              {
                                  ComplianceInstanceID = scheduleOn.ComplianceInstanceID,
                                  ScheduleOnID = scheduleOn.ScheduleOnID,
                                  ScheduleOn = scheduleOn.ScheduleOn,
                                  AutoComplete = mapping.AutoComplete
                              }).ToList();

                if (result != null)
                {
                    foreach (var item in result)
                    {
                        //Get All Assigned user & role for Compliance Schedule
                        var user = (from row in entities.ComplianceAssignments
                                    join u in entities.Users on row.UserID equals u.ID
                                    where row.ComplianceInstanceID == item.ComplianceInstanceID //&& row.IsDeleted == false
                                    select new
                                    {
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        FullName = u.FirstName + " " + u.LastName
                                    }).ToList();

                        if (user != null)
                        {
                            var statusID = 1;
                            var performer = user.Where(k => k.RoleID == SecretarialConst.RoleID.PERFORMER).FirstOrDefault();
                            if (performer != null)
                            {
                                //Create New compliance assigned Transaction
                                ComplianceTransaction _obj = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = (long)item.ComplianceInstanceID,
                                    ComplianceScheduleOnID = item.ScheduleOnID,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned.",
                                    Dated = dated, //DateTime.Now,
                                    CreatedBy = performer.UserID,
                                    CreatedByText = performer.FullName
                                };

                                entities.ComplianceTransactions.Add(_obj);
                                entities.SaveChanges();

                                //Added on 20 Aug 2020
                                lstComplianceTransaction.Add(new ComplianceTransactionVM()
                                {
                                    TransactionID = _obj.ID,
                                    ComplianceScheduleOnID = (long)_obj.ComplianceScheduleOnID,
                                });

                                //Check compliance is AutoCompleted then Close Transaction
                                if (item.AutoComplete && autoClose)
                                {
                                    if (Convert.ToDateTime(item.ScheduleOn).Date >= dated) //DateTime.Now.Date)
                                    {
                                        statusID = 4;
                                    }
                                    else
                                    {
                                        statusID = 5;
                                    }
                                }
                                else if(statusId > 1)
                                {
                                    statusID = statusId;
                                }

                                if(statusID > 1)
                                {
                                    var objTransactionClosed = new ComplianceTransaction()
                                    {
                                        ComplianceInstanceId = (long)item.ComplianceInstanceID,
                                        ComplianceScheduleOnID = item.ScheduleOnID,
                                        StatusId = statusID,
                                        Remarks = "",
                                        Dated = dated, //DateTime.Now,
                                        CreatedBy = performer.UserID,
                                        CreatedByText = performer.FullName
                                    };

                                    entities.ComplianceTransactions.Add(objTransactionClosed);
                                    entities.SaveChanges();

                                    lstComplianceTransaction.Add(new ComplianceTransactionVM()
                                    {
                                        TransactionID = objTransactionClosed.ID,
                                        ComplianceScheduleOnID = (long)objTransactionClosed.ComplianceScheduleOnID,
                                    });
                                }

                                //Update Transaction status at Schedule Level
                                var schedule = entities.BM_ComplianceScheduleOnNew.Where(k => k.ScheduleOnID == item.ScheduleOnID
                                //&& k.StatusId == null
                                ).FirstOrDefault();
                                schedule.StatusId = statusID;
                                if (statusID == 4 || statusID == 5)
                                {
                                    schedule.ClosedDate = dated; //DateTime.Now;

                                    tempMeetingId = schedule.MeetingID;
                                }
                                else if (statusID == 1)
                                {
                                    schedule.ClosedDate = null;
                                }
                                entities.SaveChanges();

                                if (tempMeetingId > 0)
                                {
                                    entities.BM_SP_ComplianceTransactionOnClosed(tempMeetingId, 1);
                                    entities.SaveChanges();
                                    tempMeetingId = null;
                                }
                            }
                        }
                    }

                    #region File data mapping
                    if (objFileData != null)
                    {
                        int fileVersion = 0;
                        if (lstComplianceTransaction.Count > 0)
                        {
                            complianceScheduleOnID = lstComplianceTransaction.FirstOrDefault().ComplianceScheduleOnID;
                            transactionID = lstComplianceTransaction.FirstOrDefault().TransactionID;

                            if(complianceScheduleOnID > 0)
                            {
                                fileVersion = objIFileData_Service.GetFileVersion((long)complianceScheduleOnID, objFileData.FileName);

                                fileVersion++;
                                objFileData.Version = Convert.ToString(fileVersion);
                                objFileData = objIFileData_Service.Save(objFileData, userID);

                                if (objFileData.FileID > 0)
                                {
                                    objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData.FileID, 1);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lstComplianceTransaction = null;
            }
            return lstComplianceTransaction;
        }

        public bool CheckMeetingComplianceClosed(long meetingID, string autoCompleteON)
        {
            var result = false;
            try
            {
               result = (from scheduleOn in entities.BM_ComplianceScheduleOnNew
                        join mapping in entities.BM_ComplianceMapping on scheduleOn.MCM_ID equals mapping.MCM_ID
                        where scheduleOn.MeetingID == meetingID && scheduleOn.MappingType == SecretarialConst.ComplianceMappingType.MEETING
                                && mapping.AutoCompleteOn == autoCompleteON
                                && scheduleOn.IsActive == true && scheduleOn.IsDeleted == false
                                && (scheduleOn.StatusId == 4 || scheduleOn.StatusId == 5)
                         select scheduleOn.Id).Any();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public bool CreateTransactionOnMeetingEnd(long meetingID, int userId)
        {
            try
            {
                entities.BM_SP_ComplianceTransactionOnMeetingEnd(meetingID, true, userId);
                entities.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public bool CreateTransactionOnNoticeSend(long meetingID, int userId)
        {
            try
            {
                entities.BM_SP_ComplianceTransactionOnMeetingEnd(meetingID, false, userId);
                entities.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
    }
}