﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Compliance
{
    public class ComplianceTransaction_ServiceSecretarial : IComplianceTransaction_Service
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public ComplianceTransactionVM CreateTransaction(ComplianceTransactionVM obj)
        {
            try
            {
                DateTime closedDate = obj.Dated;
                if (!string.IsNullOrEmpty(obj.ClosedTime))
                {
                    if (!DateTime.TryParse(obj.Dated.ToString("dd/MMM/yyyy") + " " + obj.ClosedTime, out closedDate))
                    {
                        closedDate = obj.Dated;
                    }
                }

                BM_ComplianceTransaction _obj = new BM_ComplianceTransaction()
                {
                    ComplianceScheduleOnID = obj.ComplianceScheduleOnID,
                    StatusId = obj.StatusId,
                    Remarks = obj.Remarks,
                    //Dated = obj.Dated,
                    Dated = closedDate,
                    CreatedBy = obj.CreatedBy,
                    CreatedByText = obj.CreatedByText,
                    StatusChangedOn = DateTime.Now
                };

                entities.BM_ComplianceTransaction.Add(_obj);
                entities.SaveChanges();

                obj.TransactionID = _obj.TransactionID;

                if(obj.TransactionID > 0)
                {
                    var schedule = entities.BM_ComplianceScheduleOn.Where(k => k.ScheduleOnID == obj.ComplianceScheduleOnID).FirstOrDefault();
                    schedule.StatusId = obj.StatusId;
                    if (obj.StatusId == 4 || obj.StatusId == 5)
                    {
                        #region Set Open Agenda On Compliance Completion
                        var OnComplianceCompletion = (from row in entities.BM_ComplianceScheduleOn
                                                            join agenda in entities.BM_AgendaMaster on row.AgendaMasterID equals agenda.BM_AgendaMasterId
                                                            where row.ScheduleOnID == obj.ComplianceScheduleOnID && agenda.FrequencyId == 15
                                                            select new
                                                            {
                                                                row.EntityId,
                                                                agenda.BM_AgendaMasterId,
                                                                agenda.EntityType,
                                                                agenda.MeetingTypeId
                                                            }).FirstOrDefault();

                        if(OnComplianceCompletion != null)
                        {
                            var openAgenda = new BM_MeetingOpenAgendaOnCompliance()
                            {
                                Id = 0,
                                ScheduleOnID = obj.ComplianceScheduleOnID,
                                EntityId = (int) OnComplianceCompletion.EntityId,
                                AgendaId = OnComplianceCompletion.BM_AgendaMasterId,
                                MeetingTypeId = (int) OnComplianceCompletion.MeetingTypeId,
                                IsNoted = false,
                                IsDeleted = false,
                                CreatedBy = (int) obj.CreatedBy,
                                CreatedOn = DateTime.Now
                            };

                            entities.BM_MeetingOpenAgendaOnCompliance.Add(openAgenda);
                            entities.SaveChanges();
                        }
                        #endregion
                        schedule.ClosedDate = closedDate;
                    }
                    entities.SaveChanges();
                }
                obj.Success = true;
                obj.Message = "Save Successfully.";
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public List<ComplianceTransactionVM> CreateTransactionOnMeetingEvent(long meetingID, string autoCompleteON)
        {
            var lstComplianceTransaction = new List<ComplianceTransactionVM>();
            try
            {
                //Get List of all schedule to Perform on Meeting Event such as (Notice Send, Agenda Send, Meeting Date)
                var result = (from scheduleOn in entities.BM_ComplianceScheduleOn
                             join mapping in entities.BM_ComplianceMapping on scheduleOn.MCM_ID equals mapping.MCM_ID
                             where scheduleOn.MeetingID == meetingID && scheduleOn.MappingType == SecretarialConst.ComplianceMappingType.MEETING
                                    //&& scheduleOn.StatusId == null 
                                    && mapping.AutoCompleteOn == autoCompleteON
                                    && scheduleOn.IsActive == true && scheduleOn.IsDeleted == false
                             select new
                             {
                                 ScheduleOnID = scheduleOn.ScheduleOnID,
                                 ScheduleOn = scheduleOn.ScheduleOn,
                                 AutoComplete = mapping.AutoComplete
                             }).ToList();

                if(result !=null)
                {
                    foreach (var item in result)
                    {
                        //Get All Assigned user & role for Compliance Schedule
                        var user = (from row in entities.BM_ComplianceScheduleAssignment
                                    join u in entities.Users on row.UserID equals u.ID
                                    where row.ScheduleOnID == item.ScheduleOnID && row.IsDeleted == false
                                    select new
                                    {
                                        RoleID = row.RoleID,
                                        UserID = row.UserID,
                                        FullName = u.FirstName + " " + u.LastName
                                    }).ToList();

                        if (user != null)
                        {
                            var statusID = 1;
                            var performer = user.Where(k => k.RoleID == SecretarialConst.RoleID.PERFORMER).FirstOrDefault();
                            if(performer !=null)
                            {
                                //Create New compliance assigned Transaction
                                BM_ComplianceTransaction _obj = new BM_ComplianceTransaction()
                                {
                                    ComplianceScheduleOnID = item.ScheduleOnID,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned.",
                                    Dated = DateTime.Now,
                                    CreatedBy = performer.UserID,
                                    CreatedByText = performer.FullName
                                };

                                entities.BM_ComplianceTransaction.Add(_obj);
                                entities.SaveChanges();

                                //Check compliance is AutoCompleted then Close Transaction
                                if (item.AutoComplete)
                                {
                                    if(Convert.ToDateTime(item.ScheduleOn).Date >= DateTime.Now.Date )
                                    {
                                        statusID = 4;
                                    }
                                    else
                                    {
                                        statusID = 5;
                                    }

                                    var objTransactionClosed = new BM_ComplianceTransaction()
                                    {
                                        ComplianceScheduleOnID = item.ScheduleOnID,
                                        StatusId = statusID,
                                        Remarks = "",
                                        Dated = DateTime.Now,
                                        CreatedBy = performer.UserID,
                                        CreatedByText = performer.FullName
                                    };

                                    entities.BM_ComplianceTransaction.Add(objTransactionClosed);
                                    entities.SaveChanges();

                                    lstComplianceTransaction.Add(new ComplianceTransactionVM()
                                    {
                                        TransactionID = objTransactionClosed.TransactionID,
                                        ComplianceScheduleOnID = (long)objTransactionClosed.ComplianceScheduleOnID,
                                    });
                                }

                                //Update Transaction status at Schedule Level
                                var schedule = entities.BM_ComplianceScheduleOn.Where(k => k.ScheduleOnID == item.ScheduleOnID 
                                //&& k.StatusId == null
                                ).FirstOrDefault();
                                schedule.StatusId = statusID;
                                if(statusID == 4 || statusID == 5)
                                {
                                    schedule.ClosedDate = DateTime.Now;
                                }
                                entities.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lstComplianceTransaction = null;
            }
            return lstComplianceTransaction;
        }
        public bool CreateTransactionOnMeetingEnd(long meetingID, int userId)
        {
            try
            {
                entities.BM_SP_ComplianceTransactionOnMeetingEndSecretarial(meetingID, userId);
                entities.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public bool CreateTransactionOnNoticeSend(long meetingID, int userId)
        {
            throw new NotImplementedException();
        }

        public List<ComplianceTransactionVM> CreateTransactionOnMeetingEvent(long meetingID, string autoCompleteON, bool autoClose)
        {
            throw new NotImplementedException();
        }

        public List<ComplianceTransactionVM> CreateTransactionOnMeetingEvent(long meetingID, string autoCompleteON, bool autoClose, int statusId, DateTime dated, FileDataVM objFileData, int userID)
        {
            throw new NotImplementedException();
        }

        public bool CheckMeetingComplianceClosed(long meetingID, string autoCompleteON)
        {
            throw new NotImplementedException();
        }
    }
}