﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Meetings
{
    public interface IMeetingAgendaInterestedParty
    {
        IEnumerable<MeetingAgendaInterestedPartyVM> GetAll(long AgendaMasterId);
        MeetingAgendaInterestedPartyVM Create(MeetingAgendaInterestedPartyVM template, int createdBy);
        MeetingAgendaInterestedPartyVM Update(MeetingAgendaInterestedPartyVM template, int updatedBy);
        MeetingAgendaInterestedPartyVM Delete(MeetingAgendaInterestedPartyVM template, int updatedBy);
    }
}
