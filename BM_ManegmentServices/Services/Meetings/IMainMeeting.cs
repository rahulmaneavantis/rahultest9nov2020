﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace BM_ManegmentServices.Services.Meetings
{
    public interface IMainMeeting
    {
        List<MainMeeting_VM> GetMeetingAgenda(long meetingId);
        MainMeeting_VM GetAgendabyIDforMainContent(long meetingAgendaMappingID);
        List<AgendaVoting> GetMeetingPerticipent(long meetingId,long agendaId, long mappingId);
        List<MeetingAttendance_VM> GetPerticipenforAttendence(long meetingID, int customerId, int? userId);
        MeetingAttendance_VM UpdateMeetingAttendence(MeetingAttendance_VM item, int userId);
        AgendaVoting UpdateMeetingAgendaResponse(AgendaVoting item, int userId);
        startstopeDate getMeetingstartstopDate(long meetingId);
        startstopeDate StartMeeting(startstopeDate _objstartstopDate);
        int? getRole(int userId);
        startstopeDate PauseMeeting(startstopeDate _objstartstopdate);
        IEnumerable<MeetingParticipants> GetParticipantforNotes(long meetingId);
        startstopeDate ResumeMeeting(startstopeDate _objstartstopdate);
        MeetingAttendance_VM checkforQuorum(long meetingId,int CustomerId,int EntityId);
        IEnumerable<ChairpersonElection> GetParticipant(long meetingId);
        string GetChareManForMeeting(long meetingId);
        MeetingAgendaData GetAllAgendaDetails(long parentID, long agendaID, long meetingAgendaMappingId);
        List<AvailabilityResponseChartData_ResultVM> GetAgendaResponseChartData(long meetingID, long agendaId, long mappingId, int userId);
        ChairpersonElection UpdateChairmanPerson(ChairpersonElection _objchairmanelection);
        List<MeetingAttendance_VM> GetPerticipenforAttendenceCS(long meetingID, int customerId, int? userId);
        List<AgendaVoting> GetMeetingPerticipentbyuser(long meetingId, long agendaId, long mappingId, int userId);
        List<MeetingAgendaSummary> GetAgendaMeetingwise(long meetingId,int userId);
        dynamic checkNoofAgendadisscussed(long meetingId);
        startstopeDate ConcludeMeeting(startstopeDate objstartstopedate, int customerId, int concludedBy);
        IEnumerable<MeetingParticipants> GetAuthorisedDirector(long meetingId);
        dynamic getTotalNotesUserwise(long meetingId, int userId);

        #region Meeting Resolution
        MeetingResolution GetMeetingAgendaResolution(long meetingId, long agendaId);
        MeetingResolution updateMeetingResolution(MeetingResolution _objmeetingResolution);
        long getuserparticipantId(long meetingId, int userId);
        #endregion

        #region Generate CTC
        MeetingCTC getCTCdetails(long meetingId, long agendaId);
        List<AgendaListforCTC> getListofApprovedAgenda(long meetingId);
        string GenerateCTCDocument(long meetingId, long agendaId);
        MeetingCTC saveCTCAuthorizedDirector(MeetingCTC obctc);
        #endregion

        List<AttendenceDetails> getAttendenceParticipantDetails(long meetingId);
        List<AGMVoting> GetAgendaForAgm(long meetingId);
        List<AGMVoting> UpdateVirtualMeetingsAgendaResponse(List<AGMVoting> obj, int userId);
        AGMVoting UpdateAGMAgendaResponse(AGMVoting agendaResponse);
        bool SaveTotalMemberPresentInAgm(MeetingAttendance_VM _objTotalMemberPresent);
        List<MeetingAttendance_VM> GetPerticipenforAuditorAttendenceCS(long meetingID, int customerId, int? userId);
        List<MeetingAttendance_VM> GetPerticipenforInviteeAttendenceCS(long meetingID, int customerId, int? userId);
        MeetingAttendance_VM UpdateMeetingAttendenceforAuditor(MeetingAttendance_VM item, int userId);
        MeetingAttendance_VM UpdateMeetingAttendenceforInvitee(MeetingAttendance_VM item, int userId);
        string MarkAttendenceofDirector(long meetingId, long participantId,int userId);
        void saveNotingVotingbyDir(MeetingAgendaSummary items);
        List<MeetingAgendaSummary> AddCircularVotting(MeetingAgendaSummary vottings);
        MeetingAgendaData GetMeetingResult(long meetingId, long agendaId, long mappingId);
    }
}
