﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Meetings
{
    public interface IMeeting_Compliances
    {
        IEnumerable<MeetingComplianceScheduleDetails_ResultVM> GetCompliances(long meetingAgendaMappingId, long meetingId);

        #region Get Compliance Id for generate Meeting Level schedule
        Message GenerateScheduleOn(long meetingId, string mappingType, int customerId, int createdBy);
        Message GenerateScheduleOn(long meetingId, string mappingType, int entityId, int customerId, int createdBy);
        #endregion

        #region Get Meeting -> Compliance Performer / Reviewer
        List<MeetingComplianceScheduleDetails_ResultVM> GetMeetingComplianceScheduleAssignment(long meetingId, string MappingType);
        #endregion

        #region Meeting -> Compliances ReAssignment
        List<MeetingComplianceScheduleDetails_ResultVM> MeetingComplianceScheduleReAssignment(List<MeetingComplianceScheduleDetails_ResultVM> lstUsers, int updatedBy);
        #endregion

        #region Meeting -> Compliance schedule
        List<MeetingComplianceScheduleDetails_ResultVM> GetMeetingComplianceSchedule(long meetingId);
        List<MeetingComplianceScheduleDetails_ResultVM> GetMeetingComplianceSchedule(long meetingId, string mappingType);
        ComplianceScheduleDetailsVM GetScheduleDetails(long scheduleOnID, int entityID, int customerID);
        ComplianceScheduleDetailsVM GetScheduleDetailsNew(long scheduleOnID);
        List<FileDataDocumentVM> GetComplianceDocuments(long scheduleOnID);
        #endregion

        #region Meeting Compliance Schedule Generate date
        Message CalculateScheduleDate(long meetingID, int updatedBy);
        #endregion
    }
}
