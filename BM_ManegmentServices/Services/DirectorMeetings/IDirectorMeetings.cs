﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.DirectorMeetings
{
    public interface IDirectorMeetings
    {
        #region Upcomming Meeting 8 may 2020
        List<MeetingListVM> GetMeetingsForUpcommingMeeting(int UserID, int CustomerId, string Role);
        MeetingListVM saveupdateupcomingMeetingdtlsbydir(MeetingListVM _objmeetingvm,int UserId);
        Aviability UpdateSeekaviabilityResponses(Aviability item, int userID);

        #endregion
        List<MeetingVM> GetPedingAvailability(long UserId);
        IEnumerable<CommitteeCompVM> GetMeeting();
        List<MeetingListVM> GetCircularResolution(int UserID, int CustomerId, string Role);
        MeetingAgendaSummary AddCircularVotting(MeetingAgendaSummary vottings);
        List<MeetingListVM> GetPastCircularResolution(int userID, int customerId, string role);
        bool MarkAttendence(long meetingId, long participantId,int userId);
    }
}
