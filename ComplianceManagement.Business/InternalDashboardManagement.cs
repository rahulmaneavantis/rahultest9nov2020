﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{


   
    public  class InternalDashboardManagement
    {

         public static string GetCheckListUserName(int compliancectatusid, long scheduledonid, long complianceinstanceid, int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         join cm in entities.Users on row.UserID equals cm.ID
                                         where
                                          //row.ComplianceTransactionID == compliancetransactionid &&
                                          row.InternalComplianceInstanceID == complianceinstanceid
                                         && row.RoleID == roleID  //&& row.ComplianceTransactionID == scheduledonid
                                         && row.InternalComplianceStatusID == compliancectatusid
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select cm.FirstName + " " + cm.LastName).FirstOrDefault();

                //string User = (from row in entities.Users
                //               where row.ID == userID && row.RoleID == roleID
                //                       select row.FirstName + " "+ row.LastName).Single();
                return transactionsQuery;
            }
        }
        public static List<InternalComplianceInstanceTransactionView> DashboardDataForPerformer(int userID, CannedReportFilterForPerformer filter)
       {           
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               List<InternalComplianceInstanceTransactionView> transactionsQuery=new List<InternalComplianceInstanceTransactionView>();
               int performerRoleID = (from row in entities.Roles
                                      where row.Code == "PERF"
                                      select row.ID).Single();
               DateTime now = DateTime.UtcNow.Date;
               DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);


               switch (filter)
               {
                   case CannedReportFilterForPerformer.Upcoming:
                       transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                            where row.UserID == userID && row.RoleID == performerRoleID
                                            && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                            && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 10)
                                            select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();                       
                       break;
                   case CannedReportFilterForPerformer.Overdue:
                       transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                            where row.UserID == userID && row.RoleID == performerRoleID
                                            && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 10)
                                            && row.InternalScheduledOn < now
                                            select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();                      
                       break;
                   case CannedReportFilterForPerformer.PendingForReview:
                       transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                            where row.UserID == userID && row.RoleID == performerRoleID
                                            && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
                                            select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                       break;
               }
               var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();
               List<InternalComplianceInstanceTransactionView> newList = new List<InternalComplianceInstanceTransactionView>();
               foreach (var loc in locations)
               {
                   InternalComplianceInstanceTransactionView BranchRow = new InternalComplianceInstanceTransactionView();
                   BranchRow.ShortDescription = loc.Branch;
                   BranchRow.InternalComplianceInstanceID = -1;
                   BranchRow.UserID = -1;
                   BranchRow.RoleID = -1;
                   BranchRow.InternalComplianceStatusID = -1;
                   newList.Add(BranchRow);
                   List<InternalComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderByDescending(entry => entry.InternalScheduledOn).ToList();
                   foreach (var item1 in temptransaction)
                   {
                       newList.Add(item1);
                   }
               }
               return newList.ToList();
           }
       }

        public static List<InternalComplianceInstanceTransactionView> DashboardDataForPerformerNew(int userID, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType, DateTime CalenderDate)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Status")
                {

                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10 || row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
                                              && row.InternalScheduledOn < nextOneMonth
                                               && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                    else
                    {
                        transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10 || row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
                                              && row.InternalScheduledOn == CalenderDate
                                               && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    }
                }
                else if (status == "Upcoming")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (status == "Overdue")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && row.InternalScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && (row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 14)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Date Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }
                //string through search
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        //public static List<InternalComplianceInstanceTransactionView> DashboardDataForPerformerNew(int userID, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType)
        //{

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        if (status == "Status")
        //        {

        //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                  && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10 || row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
        //                                  && row.InternalScheduledOn < nextOneMonth
        //                                   && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 // && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
        //                                 //&& (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 10)
        //                                 select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //        }
        //        else if (status == "Upcoming")
        //        {
        //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
        //                                 && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
        //                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //        }
        //        else if (status == "Overdue")
        //        {
        //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
        //                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 && row.InternalScheduledOn < now
        //                                 select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "PendingForReview")
        //        {
        //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && row.RoleID == performerRoleID
        //                                 && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
        //                                  && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                 select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }

        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
        //        }
        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
        //        }

        //        //Date Filter
        //        if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
        //        }

        //        //Risk Filter
        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }

        //        //Location
        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

        //        }
        //        //string through search
        //        if (StringType != "")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}
        public static List<InternalComplianceInstanceTransactionView> DashboardDataForPerformerNew(int userID, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate)
        {
    
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Status")
                {

                        transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10 || row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
                                              && row.InternalScheduledOn < nextOneMonth
                                             // && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                             //&& (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 10)
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (status == "Upcoming")
                { 
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                         && (row.InternalComplianceStatusID == 1  || row.InternalComplianceStatusID == 10)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (status == "Overdue")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && (row.InternalComplianceStatusID == 1  || row.InternalComplianceStatusID == 10)
                                         && row.InternalScheduledOn < now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Date Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                return transactionsQuery.ToList();
            }
        }

        public static List<InternalComplianceInstanceTransactionView> DashboardDataForPerformerDisplayCount(int userID,
            List<InternalComplianceInstanceTransactionView> MasterInternaltransactionsQuery, CannedReportFilterForPerformer filter)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                             && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                              && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                             && row.InternalScheduledOn < now
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> SPDashboardDataForPerformerDisplayCount(int userID,
                List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> MasterInternaltransactionsQuery, CannedReportFilterForPerformer filter)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> transactionsQuery = new List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result>();
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                             && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                              && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10)
                                             && row.InternalScheduledOn < now
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 16 || row.InternalComplianceStatusID == 18)
                                             && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case CannedReportFilterForPerformer.Rejected:

                        transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && (row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 14)
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }


        public static List<InternalComplianceInstanceTransactionView> DashboardDataForReviewer(int userID, CannedReportFilterForPerformer filter, bool pending = false)
        {
            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionView> transactionsQuery  = new List<InternalComplianceInstanceTransactionView>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.InternalComplianceStatusID == 1 && row.InternalScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.InternalScheduledOn <= nextOneMonth
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();
                List<InternalComplianceInstanceTransactionView> newList = new List<InternalComplianceInstanceTransactionView>();
                foreach (var loc in locations)
                {
                    InternalComplianceInstanceTransactionView BranchRow = new InternalComplianceInstanceTransactionView();
                    BranchRow.ShortDescription = loc.Branch;
                    BranchRow.InternalComplianceInstanceID = -1;
                    BranchRow.UserID = -1;
                    BranchRow.RoleID = -1;
                    BranchRow.InternalComplianceStatusID = -1;
                    newList.Add(BranchRow);
                    List<InternalComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderBy(entry => entry.InternalScheduledOn).ToList();
                    foreach (var item1 in temptransaction)
                    {
                        newList.Add(item1);
                    }
                }
                return newList.ToList();
            }
        }

        public static List<InternalComplianceInstanceTransactionView> DashboardDataForReviewerNew(int userID, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType, int PerformerID, DateTime CalenderDate)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Status")
                {
                    if (CalenderDate.ToString() == "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.InternalScheduledOn <= now
                                              && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.InternalScheduledOn == CalenderDate
                                              && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                }
                else if (status == "PendingForApproval")
                {

                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "DueButNotSubmitted")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.InternalComplianceStatusID == 1 && row.InternalScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {

                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 14)
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                //if (status == CannedReportFilterForReviewer.Status)
                //{
                //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                //                         //&& (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                //}
                //else if (status == CannedReportFilterForReviewer.PendingForApproval)
                //{
                //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                //                         && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                //}
                ////else if (pending)
                ////{
                ////    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                ////                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                ////                         && row.InternalComplianceStatusID == 1 && row.InternalScheduledOn <= now
                ////                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                ////}
                //else
                //{
                //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                //                         && row.InternalScheduledOn <= nextOneMonth
                //                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                //}

                //var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();
                //List<InternalComplianceInstanceTransactionView> newList = new List<InternalComplianceInstanceTransactionView>();
                //foreach (var loc in locations)
                //{
                //    InternalComplianceInstanceTransactionView BranchRow = new InternalComplianceInstanceTransactionView();
                //    BranchRow.ShortDescription = loc.Branch;
                //    BranchRow.InternalComplianceInstanceID = -1;
                //    BranchRow.UserID = -1;
                //    BranchRow.RoleID = -1;
                //    BranchRow.InternalComplianceStatusID = -1;
                //    newList.Add(BranchRow);
                //    List<InternalComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderBy(entry => entry.InternalScheduledOn).ToList();
                //    foreach (var item1 in temptransaction)
                //    {
                //        newList.Add(item1);
                //    }
                //}

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                //performer ID AdvanceSearch
                if (PerformerID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.PerformerID == PerformerID)).ToList();
                }

                return transactionsQuery.ToList();
            }
        }

        //public static List<InternalComplianceInstanceTransactionView> DashboardDataForReviewerNew(int userID, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType, int PerformerID)
        //{

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
        //        var reviewerRoleIDs = (from row in entities.Roles
        //                               where row.Code.StartsWith("RVW")
        //                               select row.ID).ToList();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        if (status == "Status")
        //        {
        //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && row.InternalScheduledOn <= now
        //                                  && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
        //                                 select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "PendingForApproval")
        //        {

        //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
        //                                 select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "DueButNotSubmitted")
        //        {
        //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && row.InternalComplianceStatusID == 1 && row.InternalScheduledOn <= now
        //                                 select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        else if (status == "PendingForReview")
        //        {

        //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //                                 where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
        //                                 && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
        //                                 select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }

        //        //if (status == CannedReportFilterForReviewer.Status)
        //        //{
        //        //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //        //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //        //                         //&& (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
        //        //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        //}
        //        //else if (status == CannedReportFilterForReviewer.PendingForApproval)
        //        //{
        //        //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //        //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //        //                         && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
        //        //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        //}
        //        ////else if (pending)
        //        ////{
        //        ////    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //        ////                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //        ////                         && row.InternalComplianceStatusID == 1 && row.InternalScheduledOn <= now
        //        ////                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        ////}
        //        //else
        //        //{
        //        //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //        //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
        //        //                         && row.InternalScheduledOn <= nextOneMonth
        //        //                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
        //        //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        //}

        //        //var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();
        //        //List<InternalComplianceInstanceTransactionView> newList = new List<InternalComplianceInstanceTransactionView>();
        //        //foreach (var loc in locations)
        //        //{
        //        //    InternalComplianceInstanceTransactionView BranchRow = new InternalComplianceInstanceTransactionView();
        //        //    BranchRow.ShortDescription = loc.Branch;
        //        //    BranchRow.InternalComplianceInstanceID = -1;
        //        //    BranchRow.UserID = -1;
        //        //    BranchRow.RoleID = -1;
        //        //    BranchRow.InternalComplianceStatusID = -1;
        //        //    newList.Add(BranchRow);
        //        //    List<InternalComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderBy(entry => entry.InternalScheduledOn).ToList();
        //        //    foreach (var item1 in temptransaction)
        //        //    {
        //        //        newList.Add(item1);
        //        //    }
        //        //}

        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
        //        }
        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
        //        }

        //        //Datr Filter
        //        if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
        //        }

        //        //Risk Filter
        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }

        //        //Location
        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

        //        }

        //        //performer ID AdvanceSearch
        //        if (PerformerID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.PerformerID == PerformerID)).ToList();
        //        }

        //        return transactionsQuery.ToList();
        //    }
        //}

        public static List<InternalComplianceInstanceTransactionView> DashboardDataForReviewerNew(int userID, int risk, string status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate)
       {
           
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
               var reviewerRoleIDs = (from row in entities.Roles
                                      where row.Code.StartsWith("RVW")
                                      select row.ID).ToList();
               DateTime now = DateTime.UtcNow.Date;
               DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

               if (status == "Status")
               {
                   transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                        && row.InternalScheduledOn <= now
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3  || row.InternalComplianceStatusID == 11)
                                        select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
               }
                else if (status == "PendingForApproval")
                {
                   
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "DueButNotSubmitted")
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.InternalComplianceStatusID == 1 && row.InternalScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if(status == "PendingForReview")
               {
                   
                   transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                        && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                        select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
               }

                //if (status == CannedReportFilterForReviewer.Status)
                //{
                //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                //                         //&& (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                //}
                //else if (status == CannedReportFilterForReviewer.PendingForApproval)
                //{
                //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                //                         && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                //}
                ////else if (pending)
                ////{
                ////    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                ////                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                ////                         && row.InternalComplianceStatusID == 1 && row.InternalScheduledOn <= now
                ////                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                ////}
                //else
                //{
                //    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                //                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                //                         && row.InternalScheduledOn <= nextOneMonth
                //                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                //                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                //}

                //var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();
                //List<InternalComplianceInstanceTransactionView> newList = new List<InternalComplianceInstanceTransactionView>();
                //foreach (var loc in locations)
                //{
                //    InternalComplianceInstanceTransactionView BranchRow = new InternalComplianceInstanceTransactionView();
                //    BranchRow.ShortDescription = loc.Branch;
                //    BranchRow.InternalComplianceInstanceID = -1;
                //    BranchRow.UserID = -1;
                //    BranchRow.RoleID = -1;
                //    BranchRow.InternalComplianceStatusID = -1;
                //    newList.Add(BranchRow);
                //    List<InternalComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderBy(entry => entry.InternalScheduledOn).ToList();
                //    foreach (var item1 in temptransaction)
                //    {
                //        newList.Add(item1);
                //    }
                //}

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }

                return transactionsQuery.ToList();
           }
       }
        public static List<InternalComplianceInstanceTransactionView> DashboardDataForReviewerDisplayCount(int userID,
        List<InternalComplianceInstanceTransactionView> MasterInternaltransactionsQuery, CannedReportFilterForPerformer filter, bool pending = false)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due But Not Submitted
                    transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10) && row.InternalScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && row.InternalScheduledOn <= nextOneMonth
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                return transactionsQuery.ToList();
            }
        }
        public static List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> SPDashboardDataForReviewerDisplayCount(int userID,
     List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> MasterInternaltransactionsQuery, CannedReportFilterForPerformer filter, bool pending = false)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> transactionsQuery = new List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11 || row.InternalComplianceStatusID == 16 || row.InternalComplianceStatusID == 18)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (filter == CannedReportFilterForPerformer.Rejected)
                {
                    transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 6 || row.InternalComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due But Not Submitted
                    transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10) && row.InternalScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in MasterInternaltransactionsQuery //entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         && row.InternalScheduledOn <= nextOneMonth
                                         && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<InternalComplianceInstanceCheckListTransactionView> DashboardDataForReviewerInternalChecklistDisplayCount(int userID, CannedReportFilterForPerformer filter, bool pending = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 4)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due But Not Submitted
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10) && row.InternalScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && row.InternalScheduledOn <= nextOneMonth
                                         // && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> SPDashboardDataForReviewerInternalChecklistDisplayCount(int userID,
            List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> MasterInternalTransactionQuery ,CannedReportFilterForPerformer filter, bool pending = false)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();
                List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> transactionsQuery = new List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result>();
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                if (filter == CannedReportFilterForPerformer.PendingForReview)
                {
                    transactionsQuery = (from row in MasterInternalTransactionQuery //entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 4)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (pending)
                {
                    //Due But Not Submitted
                    transactionsQuery = (from row in MasterInternalTransactionQuery //entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10) && row.InternalScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in MasterInternalTransactionQuery //entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int) row.RoleID)
                                         && row.InternalScheduledOn <= nextOneMonth
                                        // && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3 || row.InternalComplianceStatusID == 11)
                                         select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<InternalComplianceInstanceTransactionView> DashboardDataForApprover(int userID, CannedReportFilterForPerformer filter)
       {
           #region Previous Code
           //using (ComplianceDBEntities entities = new ComplianceDBEntities())
            //{
            //    int approverRoleID = (from row in entities.Roles
            //                          where row.Code == "APPR"
            //                          select row.ID).Single();


            //    DateTime now = DateTime.UtcNow.Date;
            //    DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

            //    var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
            //                             where row.UserID == userID && row.RoleID == approverRoleID
            //                             select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

            //    switch (filter)
            //    {
            //        case CannedReportFilterForPerformer.Upcoming:
            //            transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 6));
            //            break;
            //        case CannedReportFilterForPerformer.Overdue:
            //            transactionsQuery = transactionsQuery.Where(entry => entry.InternalScheduledOn < now && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 6));
            //            break;
            //        case CannedReportFilterForPerformer.PendingForApproval:
            //            transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5);
            //            break;
            //    }

            //    //Change by SACHIN
            //    //var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).Select(entry => entry.FirstOrDefault()).ToList();
            //    var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();

            //    List<InternalComplianceInstanceTransactionView> newList = new List<InternalComplianceInstanceTransactionView>();
            //    foreach (var loc in locations)
            //    {
            //        InternalComplianceInstanceTransactionView BranchRow = new InternalComplianceInstanceTransactionView();

            //        BranchRow.ShortDescription = loc.Branch;
            //        BranchRow.InternalComplianceInstanceID = -1;
            //        BranchRow.UserID = -1;
            //        BranchRow.RoleID = -1;
            //        BranchRow.InternalComplianceStatusID = -1;
            //        newList.Add(BranchRow);

            //        List<InternalComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderByDescending(entry => entry.InternalScheduledOn).ToList();
            //        foreach (var item1 in temptransaction)
            //        {
            //            newList.Add(item1);
            //        }
            //    }

            //    return newList.ToList();
           //}
           #endregion

           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
               int approverRoleID = (from row in entities.Roles
                                     where row.Code == "APPR"
                                     select row.ID).Single();
               DateTime now = DateTime.UtcNow.Date;
               DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);                
               switch (filter)
               {
                   case CannedReportFilterForPerformer.Upcoming:

                       transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                            where row.UserID == userID && row.RoleID == approverRoleID
                                            && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                            && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 6)
                                            select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();                       
                       break;
                   case CannedReportFilterForPerformer.Overdue:
                       transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                            where row.UserID == userID && row.RoleID == approverRoleID
                                            && row.InternalScheduledOn < now && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 6)
                                            select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();                      
                       break;
                   case CannedReportFilterForPerformer.PendingForReview:
                       transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                            where row.UserID == userID && row.RoleID == approverRoleID
                                            && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                                            select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();                       
                       break;
               }
               var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();
               List<InternalComplianceInstanceTransactionView> newList = new List<InternalComplianceInstanceTransactionView>();
               foreach (var loc in locations)
               {
                   InternalComplianceInstanceTransactionView BranchRow = new InternalComplianceInstanceTransactionView();
                   BranchRow.ShortDescription = loc.Branch;
                   BranchRow.InternalComplianceInstanceID = -1;
                   BranchRow.UserID = -1;
                   BranchRow.RoleID = -1;
                   BranchRow.InternalComplianceStatusID = -1;
                   newList.Add(BranchRow);
                   List<InternalComplianceInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).OrderByDescending(entry => entry.InternalScheduledOn).ToList();
                   foreach (var item1 in temptransaction)
                   {
                       newList.Add(item1);
                   }
               }
               return newList.ToList();
           }
       }


        public static List<InternalComplianceInstanceTransactionView> DashboardDataForApproverDisplayCount(int userID, CannedReportFilterForPerformer filter)
        {
            #region Previous Code
            //using (ComplianceDBEntities entities = new ComplianceDBEntities())
            //{
            //    int approverRoleID = (from row in entities.Roles
            //                          where row.Code == "APPR"
            //                          select row.ID).Single();
            //    DateTime now = DateTime.UtcNow.Date;
            //    DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
            //    var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
            //                             where row.UserID == userID && row.RoleID == approverRoleID
            //                             select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

            //    switch (filter)
            //    {
            //        case CannedReportFilterForPerformer.Upcoming:
            //            transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 6));
            //            break;
            //        case CannedReportFilterForPerformer.Overdue:
            //            transactionsQuery = transactionsQuery.Where(entry => entry.InternalScheduledOn < now && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 6));
            //            break;
            //        case CannedReportFilterForPerformer.PendingForApproval:
            //            transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5);
            //            break;
            //    }
            //    return transactionsQuery.ToList();
            //}
            #endregion

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                int approverRoleID = (from row in entities.Roles
                                      where row.Code == "APPR"
                                      select row.ID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);                
                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:

                        transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == approverRoleID
                                             && (row.InternalScheduledOn >= now && row.InternalScheduledOn <= nextOneMonth)
                                             && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 6)
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();                        
                        break;
                    case CannedReportFilterForPerformer.Overdue:

                        transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == approverRoleID
                                             && row.InternalScheduledOn < now
                                             && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 6)
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();                        
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:

                        transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == approverRoleID
                                             && (row.InternalComplianceStatusID == 4 || row.InternalComplianceStatusID == 5)
                                             select row).ToList().GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
                return transactionsQuery.ToList();
            }
        }


        public static List<SP_GetOverDueHighInternalCompliance_Result> GetComplianceDashboardOverdueInternal(int Customerid,string isrole, int Userid = -1, bool ISApprover = false,int RiskId = 0, int IsDashBoard = 1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var transactionsQuery = (from row in entities.SP_GetOverDueHighInternalCompliance(Userid,Customerid, RiskId, IsDashBoard, isrole)
                                         where row.InternalScheduledOn < DateTime.Now
                                         select row).ToList();
              
                return transactionsQuery;
            }
        }
        //public static List<SP_GetOverDueHighInternalComplianceApprover_Result> GetComplianceDashboardOverdueInternalApprover(int Customerid, int Userid = -1, bool ISApprover = false, int RiskId = 0, int IsDashBoard = 1)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        entities.Database.CommandTimeout = 180;
        //        var transactionsQuery = (from row in entities.SP_GetOverDueHighInternalComplianceApprover(Userid, Customerid, RiskId, IsDashBoard)
        //                                 where row.InternalScheduledOn < DateTime.Now
        //                                 select row).ToList();

        //        return transactionsQuery;
        //    }
        //}

        //Added by Rahul on 15 FEB 2017 For Internal Dashboard
        public static List<InternalComplianceDashboardSummaryView> GetComplianceDashboardSummaryInternal(int Customerid, int Userid = -1, bool ISApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceDashboardSummaryView> transactionsQuery = new List<InternalComplianceDashboardSummaryView>();
                if (ISApprover == true)
                {
                    transactionsQuery = (from row in entities.InternalComplianceDashboardSummaryViews
                                         where row.CustomerID == Customerid
                                         && row.UserID == Userid && row.RoleID == 6                                     
                                         select row).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceDashboardSummaryViews
                                         join row2 in entities.EntitiesAssignmentInternals
                                         on (long)row.CustomerBranchID equals row2.BranchID
                                         where row.CustomerID == Customerid
                                         && row2.UserID == Userid                    
                                         select row).ToList();
                }
            
                return transactionsQuery;
            }
        }

        //Added by Rahul on 14 Dec 2015 For Internal Dashboard
        public static List<InternalComplianceDashboardSummaryView> GetComplianceDashboardSummaryInternal()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var transactionsQuery = (from row in entities.InternalComplianceDashboardSummaryViews
                                         select row).ToList();

                return transactionsQuery;
            }
        }
        public static DataTable GetPerformanceSummaryRiskWise(IEnumerable<dynamic> transactionsQuery, string role)
        {
            long delayedCount;
            long Intime;
            long pendingcount;
            //DateTime now = DateTime.UtcNow.Date;

            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("RiskCatagory", typeof(string));
            table.Columns.Add("Delayed", typeof(long));
            table.Columns.Add("InTime", typeof(long));
            table.Columns.Add("Pending", typeof(long));

            // for Heigh risk Compliances
            delayedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 0).Count();
            Intime = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 0).Count();
            if (role.Equals("PERF"))
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10 || entry.InternalComplianceStatusID == 6) && entry.Risk == 0).Count();
            }
            else
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3) && entry.Risk == 0).Count();
            }
            table.Rows.Add(0, "High", delayedCount, Intime, pendingcount);

            // for Medium risk Compliances
            delayedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 1).Count();
            Intime = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 1).Count();
            if (role.Equals("PERF"))
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10 || entry.InternalComplianceStatusID == 6) && entry.Risk == 1).Count();
            }
            else
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3) && entry.Risk == 1).Count();
            }

            table.Rows.Add(1, "Medium", delayedCount, Intime, pendingcount);

            // for Low risk Compliances
            delayedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 2).Count();
            Intime = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 2).Count();
            if (role.Equals("PERF"))
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10 || entry.InternalComplianceStatusID == 6) && entry.Risk == 2).Count();
            }
            else
            {
                pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3) && entry.Risk == 2).Count();
            }

            table.Rows.Add(2, "Low", delayedCount, Intime, pendingcount);

            return table;

        }

        public static DataTable GetPerformanceSummaryCatagoryWise(int customerid,IEnumerable<dynamic> transactionsQueryforCatagory, string role)
        {
            long delayedCount;
            long Intime;
            long pendingcount;
            //DateTime now = DateTime.UtcNow.Date;

            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("RiskCatagory", typeof(string));
            table.Columns.Add("Delayed", typeof(long));
            table.Columns.Add("InTime", typeof(long));
            table.Columns.Add("Pending", typeof(long));

            var CatagoryList = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);

            foreach (InternalCompliancesCategory cc in CatagoryList)
            {
                delayedCount = transactionsQueryforCatagory.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.IComplianceCategoryId == cc.ID).Count();
                Intime = transactionsQueryforCatagory.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.IComplianceCategoryId == cc.ID).Count();
                if (role.Equals("PERF"))
                {
                    pendingcount = transactionsQueryforCatagory.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10 || entry.InternalComplianceStatusID == 6) && entry.IComplianceCategoryID == cc.ID).Count();
                }
                else
                {
                    pendingcount = transactionsQueryforCatagory.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3) && entry.IComplianceCategoryID == cc.ID).Count();
                }

                if (delayedCount != 0 || Intime != 0 || pendingcount != 0)
                    table.Rows.Add(cc.ID, cc.Name, delayedCount, Intime, pendingcount);

            }

            return table;
        }

        public static int GetBranchCountforPerformer(int userID, DateTime fromdate, DateTime todate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID && row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate
                                         select row).GroupBy(entity => entity.CustomerBranchID).Select(entity => entity.FirstOrDefault());
                return transactionsQuery.Count();
            }
        }
        //performance summary for performer
        public static DataTable PerformanceSummaryforPerformer(int customerid,int userID, InternalPerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, int branchId = -1, InternalPerformanceSummaryForPerformer subFilter = InternalPerformanceSummaryForPerformer.Risk, bool isReportee = false, long ReviewerID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                DataTable table = new DataTable();

                switch (filter)
                {
                    case InternalPerformanceSummaryForPerformer.Risk:

                        IEnumerable<dynamic> transactionsQuery;

                        if (isReportee)
                        {

                            var instanceIds = InternalComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                            transactionsQuery = GetComplianceDashboardSummaryInternal()
                                                .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate && instanceIds.Contains(entry.InternalComplianceInstanceID))
                                                .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                        }
                        else
                        {
                            transactionsQuery = GetComplianceDashboardSummaryInternal()
                                               .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate)
                                               .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                        }

                        table = GetPerformanceSummaryRiskWise(transactionsQuery, "PERF");

                        break;

                    case InternalPerformanceSummaryForPerformer.Category:

                        IEnumerable<dynamic> transactionsQueryforCatagory;
                        if (isReportee)
                        {

                            var instanceIds = InternalComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                            transactionsQueryforCatagory = GetComplianceDashboardSummaryInternal()
                                                            .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate && instanceIds.Contains(entry.InternalComplianceInstanceID))
                                                            .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                        }
                        else
                        {
                            transactionsQueryforCatagory = GetComplianceDashboardSummaryInternal()
                                                            .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate)
                                                            .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                        }
                        table = GetPerformanceSummaryCatagoryWise(customerid,transactionsQueryforCatagory, "PERF");

                        break;
                    case InternalPerformanceSummaryForPerformer.Location:

                        if (subFilter == InternalPerformanceSummaryForPerformer.Risk)
                        {
                            if (branchId != -1)
                            {
                                //var transactionsQueryforLocation = GetComplianceDashboardSummary()
                                //                                   .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate && entry.CustomerBranchID == branchId)
                                //                                   .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                                IEnumerable<dynamic> transactionsQueryforLocation;
                                if (isReportee)
                                {

                                    var instanceIds = InternalComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                                    transactionsQueryforLocation = GetComplianceDashboardSummaryInternal()
                                                                   .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate && entry.CustomerBranchID == branchId
                                                                   && instanceIds.Contains(entry.InternalComplianceInstanceID))
                                                                   .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                                }
                                else
                                {
                                    transactionsQueryforLocation = GetComplianceDashboardSummaryInternal()
                                                                   .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate && entry.CustomerBranchID == branchId)
                                                                   .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                                }

                                table = GetPerformanceSummaryRiskWise(transactionsQueryforLocation, "PERF");
                            }
                            else
                            {

                                IEnumerable<dynamic> transactionsQueryforLocation;
                                if (isReportee)
                                {

                                    var instanceIds = InternalComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                                    transactionsQueryforLocation = GetComplianceDashboardSummaryInternal()
                                                                   .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate && instanceIds.Contains(entry.InternalComplianceInstanceID))
                                                                   .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                                }
                                else
                                {
                                    transactionsQueryforLocation = GetComplianceDashboardSummaryInternal()
                                                                   .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate)
                                                                   .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                                }

                                table = GetPerformanceSummaryRiskWise(transactionsQueryforLocation, "PERF");
                            }

                        }
                        else
                        {
                            if (branchId != -1)
                            {

                                IEnumerable<dynamic> QueryLocationCatagory;
                                if (isReportee)
                                {

                                    var instanceIds = InternalComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                                    QueryLocationCatagory = GetComplianceDashboardSummaryInternal()
                                                           .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate && entry.CustomerBranchID == branchId
                                                           && instanceIds.Contains(entry.InternalComplianceInstanceID))
                                                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                                }
                                else
                                {
                                    QueryLocationCatagory = GetComplianceDashboardSummaryInternal()
                                                                  .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate && entry.CustomerBranchID == branchId)
                                                                  .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                                }


                                table = GetPerformanceSummaryCatagoryWise(customerid, QueryLocationCatagory, "PERF");
                            }
                            else
                            {
                                //var QueryLocationCatagory = GetComplianceDashboardSummary()
                                //                                  .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                //                                  .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                                IEnumerable<dynamic> QueryLocationCatagory;
                                if (isReportee)
                                {

                                    var instanceIds = InternalComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                                    QueryLocationCatagory = GetComplianceDashboardSummaryInternal()
                                                           .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate && instanceIds.Contains(entry.InternalComplianceInstanceID))
                                                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                                }
                                else
                                {
                                    QueryLocationCatagory = GetComplianceDashboardSummaryInternal()
                                                           .Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate)
                                                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                                }

                                table = GetPerformanceSummaryCatagoryWise(customerid, QueryLocationCatagory, "PERF");
                            }

                        }

                        break;
                }

                return table;

            }
        }

        public static List<object> PerformanceSummaryforPerformerPending(int customerid,int userID, InternalPerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, out bool displayflag, int catagoryId = -1, int branchId = -1, InternalPerformanceSummaryForPerformer subFilter = InternalPerformanceSummaryForPerformer.Risk, bool isReportee = false, long ReviewerID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime today = DateTime.Now.Date;
                bool flag = false;
                List<object> summary = new List<object>();

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                IEnumerable<dynamic> transactionsQuery;
                if (isReportee)
                {
                    var instanceIds = InternalComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         //join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.UserID == userID && row.RoleID == performerRoleID && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10 || row.InternalComplianceStatusID == 6) && row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate
                                         && instanceIds.Contains(row.InternalComplianceInstanceID)
                                         select new
                                         {
                                             ComplianceInstanceID = row.InternalComplianceInstanceID,
                                             row.InternalComplianceID,
                                             //acte.ComplianceCategoryId,
                                             row.IComplianceCategoryID,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.InternalScheduledOnID
                                         }).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        // join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.UserID == userID && row.RoleID == performerRoleID && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10 || row.InternalComplianceStatusID == 6) && row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate
                                         select new
                                         {
                                             ComplianceInstanceID = row.InternalComplianceInstanceID,
                                             row.InternalComplianceID,
                                             //acte.ComplianceCategoryId,
                                             row.IComplianceCategoryID,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.InternalScheduledOnID
                                         }).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                }

                switch (filter)
                {
                    case InternalPerformanceSummaryForPerformer.Risk:
                        var CatagoryList = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);

                        int cnt = 0;
                        foreach (InternalCompliancesCategory cc in CatagoryList)
                        {
                            var Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0).Count();
                            if (Convert.ToInt32(Count) > 0)
                                summary.Add(new { Name = cc.Name, Quantity = Count });

                            cnt += Count;
                        }

                        flag = cnt != 0 ? true : false;

                        break;
                    case InternalPerformanceSummaryForPerformer.Category:
                        int HighRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 0).Count();
                        int MediumRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 1).Count();
                        int LowRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 2).Count();

                        summary.Add(new { Name = "Low", Quantity = LowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = MediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = HighRiskCount });

                        flag = (LowRiskCount + MediumRiskCount + HighRiskCount) != 0 ? true : false;
                        break;
                    case InternalPerformanceSummaryForPerformer.Location:

                        if (subFilter == InternalPerformanceSummaryForPerformer.Risk)
                        {
                            var CatagoryListforLocation = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);

                            int LocCnt = 0;
                            foreach (InternalCompliancesCategory cc in CatagoryListforLocation)
                            {
                                int Count = 0;
                                if (branchId != -1)
                                    Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0 && entry.CustomerBranchID == branchId).Count();
                                else
                                    Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0).Count();

                                if (Count > 0)
                                    summary.Add(new { Name = cc.Name, Quantity = Count });

                                LocCnt += Count;
                            }
                            flag = LocCnt != 0 ? true : false;
                        }
                        else
                        {
                            if (branchId != -1)
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == branchId);
                            }
                            int HighRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 0).Count();
                            int MediumRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 1).Count();
                            int LowRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 2).Count();
                            summary.Add(new { Name = "Low", Quantity = LowRiskCountForLoc });
                            summary.Add(new { Name = "Medium", Quantity = MediumRiskCountForLoc });
                            summary.Add(new { Name = "High", Quantity = HighRiskCountForLoc });

                            flag = (LowRiskCountForLoc + MediumRiskCountForLoc + HighRiskCountForLoc) != 0 ? true : false;
                        }

                        break;
                }

                displayflag = flag;

                return summary;
            }
        }

        public static List<object> PerformanceSummaryforPerformerDelayed(int customerid, int userID, InternalPerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, out bool displayflag, int catagoryId = -1, int branchId = -1, InternalPerformanceSummaryForPerformer subFilter = InternalPerformanceSummaryForPerformer.Risk, bool isReportee = false, long ReviewerID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime today = DateTime.Now.Date;
                bool flag = false;
                List<object> summary = new List<object>();

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                IEnumerable<dynamic> transactionsQuery;
                if (isReportee)
                {
                    var instanceIds = InternalComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         //join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.UserID == userID && row.RoleID == performerRoleID && row.InternalComplianceStatusID == 5 && row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate
                                         && instanceIds.Contains(row.InternalComplianceInstanceID)
                                         select new
                                         {
                                             ComplianceInstanceID = row.InternalComplianceInstanceID,
                                             row.InternalComplianceID,
                                             //acte.ComplianceCategoryId,
                                             row.IComplianceCategoryID,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.InternalScheduledOnID
                                         }).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         //join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.UserID == userID && row.RoleID == performerRoleID && row.InternalComplianceStatusID == 5 && row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate
                                         select new
                                         {
                                             ComplianceInstanceID = row.InternalComplianceInstanceID,
                                             row.InternalComplianceID,
                                             //acte.ComplianceCategoryId,
                                             row.IComplianceCategoryID,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.InternalScheduledOnID
                                         }).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                }

                switch (filter)
                {
                    case InternalPerformanceSummaryForPerformer.Risk:
                        var CatagoryList = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);

                        int cnt = 0;
                        foreach (InternalCompliancesCategory cc in CatagoryList)
                        {
                            var Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0).Count();

                            if (Convert.ToInt32(Count) > 0)
                                summary.Add(new { Name = cc.Name, Quantity = Count });

                            cnt += Count;
                        }
                        flag = cnt != 0 ? true : false;
                        break;

                    case InternalPerformanceSummaryForPerformer.Category:
                        int HighRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 0).Count();
                        int MediumRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 1).Count();
                        int LowRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 2).Count();
                        summary.Add(new { Name = "Low", Quantity = LowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = MediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = HighRiskCount });

                        flag = (LowRiskCount + MediumRiskCount + HighRiskCount) != 0 ? true : false;
                        break;

                    case InternalPerformanceSummaryForPerformer.Location:

                        if (subFilter == InternalPerformanceSummaryForPerformer.Risk)
                        {
                            var CatagoryListforLocation = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);

                            int LocCnt = 0;
                            foreach (InternalCompliancesCategory cc in CatagoryListforLocation)
                            {
                                int Count = 0;
                                if (branchId != -1)
                                    Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0 && entry.CustomerBranchID == branchId).Count();
                                else
                                    Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0).Count();

                                if (Count > 0)
                                    summary.Add(new { Name = cc.Name, Quantity = Count });

                                LocCnt += Count;
                            }
                            flag = LocCnt != 0 ? true : false;
                        }
                        else
                        {
                            if (branchId != -1)
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == branchId);
                            }
                            int HighRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 0).Count();
                            int MediumRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 1).Count();
                            int LowRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 2).Count();
                            summary.Add(new { Name = "Low", Quantity = LowRiskCountForLoc });
                            summary.Add(new { Name = "Medium", Quantity = MediumRiskCountForLoc });
                            summary.Add(new { Name = "High", Quantity = HighRiskCountForLoc });

                            flag = (LowRiskCountForLoc + MediumRiskCountForLoc + HighRiskCountForLoc) != 0 ? true : false;
                        }

                        break;
                }

                displayflag = flag;
                return summary;
            }
        }
        public static List<InternalComplianceDashboardSummaryView> GetSummaryDetails(int userID, DateTime fromDate, DateTime toDate, int role, List<int> status, int baranchId, string filter, string subfilter, int typeID, bool isReportee = false, long ReviewerID = -1)
        {

            List<InternalComplianceDashboardSummaryView> ComplianceTransaction = new List<InternalComplianceDashboardSummaryView>();
            if (!filter.ToString().Equals("Reportee"))
            {
                if (isReportee)
                {
                    var instanceIds = InternalComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                    ComplianceTransaction = InternalDashboardManagement.GetComplianceDashboardSummaryInternal()
                                            .Where(entry => entry.UserID == userID && entry.RoleID == role && entry.InternalScheduledOn > fromDate && entry.InternalScheduledOn < toDate && instanceIds.Contains(entry.InternalComplianceInstanceID))
                                            .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    ComplianceTransaction = InternalDashboardManagement.GetComplianceDashboardSummaryInternal()
                                           .Where(entry => entry.UserID == userID && entry.RoleID == role && entry.InternalScheduledOn > fromDate && entry.InternalScheduledOn < toDate)
                                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                if (status.Count > 0)
                {
                    ComplianceTransaction = ComplianceTransaction.Where(entry => status.Contains((int)entry.InternalComplianceStatusID)).ToList();
                }

                if (baranchId != -1)
                {
                    ComplianceTransaction = ComplianceTransaction.Where(entry => entry.CustomerBranchID == baranchId).ToList();
                }

                if (filter != null)
                {
                    if (filter.Equals("Category"))
                    {
                        ComplianceTransaction = ComplianceTransaction.Where(entry => entry.IComplianceCategoryID == typeID).ToList();
                    }
                    else if (filter.Equals("Risk"))
                    {
                        ComplianceTransaction = ComplianceTransaction.Where(entry => entry.Risk == typeID).ToList();
                    }
                    else
                    {
                        if (subfilter.Equals("Risk"))
                        {
                            ComplianceTransaction = ComplianceTransaction.Where(entry => entry.Risk == typeID).ToList();
                        }
                        else
                        {
                            ComplianceTransaction = ComplianceTransaction.Where(entry => entry.IComplianceCategoryID == typeID).ToList();
                        }
                    }
                }
            }
            else
            {
                if (isReportee)
                {
                    var instanceIds = InternalComplianceManagement.GetInstanceIDfromReviewer(ReviewerID);
                    ComplianceTransaction = InternalDashboardManagement.GetComplianceDashboardSummaryInternal()
                                           .Where(entry => entry.UserID == userID && entry.RoleID == 3 && entry.InternalScheduledOn > fromDate && entry.InternalScheduledOn < toDate && instanceIds.Contains(entry.InternalComplianceInstanceID)).ToList();
                }
                else
                {
                    ComplianceTransaction = InternalDashboardManagement.GetComplianceDashboardSummaryInternal()
                                           .Where(entry => entry.UserID == userID && entry.RoleID == 3 && entry.InternalScheduledOn > fromDate && entry.InternalScheduledOn < toDate).ToList();
                }

                if (status.Count > 0)
                {
                    ComplianceTransaction = ComplianceTransaction.Where(entry => status.Contains((int)entry.InternalComplianceStatusID))
                                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

            }

            return ComplianceTransaction;
        }
        public static string GetSumofPenaltyForPerformer(int userID, DateTime fromdate, DateTime todate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;

                var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         join cm in entities.InternalCompliances on row.InternalComplianceID equals cm.ID
                                         where row.UserID == userID && row.RoleID == performerRoleID && row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate
                                         //&& row.InternalComplianceStatusID == 6 && row.NonComplianceType == 0 commented by rahul on 22 DEC 2015
                                         && row.InternalComplianceStatusID == 6 
                                         select new
                                         {
                                             row.InternalComplianceInstanceID,
                                             row.InternalComplianceStatusID,
                                             row.InternalComplianceID,
                                             //cm.FixedMinimum,
                                             //cm.FixedMaximum,
                                             row.InternalScheduledOnID
                                         }).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                string penalty = "";
                //if (transactionsQuery.Count() != 0)
                //{
                //    double? fixedMinimumValue = transactionsQuery.Sum(entity => entity.FixedMinimum);
                //    double? fixedMaximumValue = transactionsQuery.Sum(entity => entity.FixedMaximum);

                //    penalty = "*Sum of Penalty minimum " + fixedMinimumValue + " and maximum " + fixedMaximumValue + " Rupees is applicable for " + transactionsQuery.Count() + " Non-Compliances.";
                //}

                return penalty;
            }
        }


        //Performence summary for reviewer.
        public static DataTable PerformanceSummaryforReviewer(int customerid,int userID, InternalPerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, int branchId = -1, InternalPerformanceSummaryForPerformer subFilter = InternalPerformanceSummaryForPerformer.Risk)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                DataTable table = new DataTable();

                switch (filter)
                {
                    case InternalPerformanceSummaryForPerformer.Risk:

                        var transactionsQuery = GetComplianceDashboardSummaryInternal()
                                                .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID) && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate)
                                                .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                        table = GetPerformanceSummaryRiskWise(transactionsQuery, "RVW");

                        break;

                    case InternalPerformanceSummaryForPerformer.Category:

                        var transactionsQueryforCatagory = GetComplianceDashboardSummaryInternal()
                                                           .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID) && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate)
                                                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                        table = GetPerformanceSummaryCatagoryWise(customerid,transactionsQueryforCatagory, "RVW");

                        break;
                    case InternalPerformanceSummaryForPerformer.Location:

                        if (subFilter == InternalPerformanceSummaryForPerformer.Risk)
                        {
                            if (branchId != -1)
                            {
                                var transactionsQueryforLocation = GetComplianceDashboardSummaryInternal()
                                                                   .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID) && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate)
                                                                   .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                                table = GetPerformanceSummaryRiskWise(transactionsQueryforLocation, "RVW");
                            }
                            else
                            {

                                var transactionsQueryforLocation = GetComplianceDashboardSummaryInternal()
                                                                  .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID) && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate)
                                                                  .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());
                                table = GetPerformanceSummaryRiskWise(transactionsQueryforLocation, "RVW");
                            }

                        }
                        else
                        {
                            if (branchId != -1)
                            {

                                var QueryLocationCatagory = GetComplianceDashboardSummaryInternal()
                                                            .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID) && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate)
                                                            .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                                table = GetPerformanceSummaryCatagoryWise(customerid,QueryLocationCatagory, "RVW");
                            }
                            else
                            {

                                var QueryLocationCatagory = GetComplianceDashboardSummaryInternal()
                                                         .Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID) && entry.InternalScheduledOn > fromdate && entry.InternalScheduledOn < todate)
                                                         .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                                table = GetPerformanceSummaryCatagoryWise(customerid,QueryLocationCatagory, "RVW");
                            }

                        }

                        break;

                    case InternalPerformanceSummaryForPerformer.Reportee:

                        table = GetPerformanceSummaryofReporty(userID, fromdate, todate, branchId);

                        break;


                }

                return table;

            }
        }
        public static DataTable GetPerformanceSummaryofReporty(int userID, DateTime fromdate, DateTime todate, int branchId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();


                List<long> reporteeIds = InternalComplianceManagement.GetPerformerListUsingReviewer(userID);
                List<long> MappedinstanceId = InternalComplianceManagement.GetInstanceIDfromReviewer(userID);

                long delayedCount;
                long Intime;
                long pendingcount;
                DateTime now = DateTime.UtcNow.Date;

                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(string));
                table.Columns.Add("RiskCatagory", typeof(string));
                table.Columns.Add("Delayed", typeof(long));
                table.Columns.Add("InTime", typeof(long));
                table.Columns.Add("Pending", typeof(long));

                var ReportyList = (from row in entities.Users
                                   where reporteeIds.Contains(row.ID) && row.IsDeleted != true
                                   select row).ToList();

                List<InternalComplianceInstanceTransactionView> transactionQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                                                            where row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate && row.RoleID == performerRoleID
                                                                            && MappedinstanceId.Contains(row.InternalComplianceInstanceID)
                                                                            select row).ToList();

                if (branchId != -1)
                {
                    transactionQuery = transactionQuery.Where(entry => entry.CustomerBranchID == branchId).ToList();
                }

                foreach (User reporty in ReportyList)
                {
                    pendingcount = transactionQuery.Where(entry => entry.UserID == reporty.ID && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10 || entry.InternalComplianceStatusID == 6)).Select(entity => entity.InternalScheduledOnID).Distinct().Count();
                    delayedCount = transactionQuery.Where(entry => entry.UserID == reporty.ID && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9)).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).Count();
                    Intime = transactionQuery.Where(entry => entry.UserID == reporty.ID && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7)).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).Count();
                    table.Rows.Add(reporty.ID, reporty.FirstName + " " + reporty.LastName, delayedCount, Intime, pendingcount);
                }
                return table;
            }

        }
        public static List<object> PerformanceSummaryforReviewerPending(int customerid,int userID, InternalPerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, out bool displayflag, int catagoryId = -1, int branchId = -1, InternalPerformanceSummaryForPerformer subFilter = InternalPerformanceSummaryForPerformer.Risk)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime today = DateTime.Now.Date;
                bool flag = false;
                List<object> summary = new List<object>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         //join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) && (row.InternalComplianceStatusID == 2 || row.InternalComplianceStatusID == 3) && row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate
                                         select new
                                         {
                                             ComplianceInstanceID = row.InternalComplianceInstanceID,
                                             row.InternalComplianceID,
                                             //acte.ComplianceCategoryId,
                                             row.IComplianceCategoryID,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.InternalScheduledOnID
                                         }).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                switch (filter)
                {
                    case InternalPerformanceSummaryForPerformer.Risk:
                        var CatagoryList = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);

                        int cnt = 0;
                        foreach (InternalCompliancesCategory cc in CatagoryList)
                        {
                            var Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0).Count();
                            summary.Add(new { Name = cc.Name, Quantity = Count });
                            cnt += Count;
                        }

                        flag = cnt != 0 ? true : false;

                        break;
                    case InternalPerformanceSummaryForPerformer.Category:
                        int HighRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 0).Count();
                        int MediumRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 1).Count();
                        int LowRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 2).Count();

                        summary.Add(new { Name = "Low", Quantity = LowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = MediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = HighRiskCount });

                        flag = (LowRiskCount + MediumRiskCount + HighRiskCount) != 0 ? true : false;
                        break;
                    case InternalPerformanceSummaryForPerformer.Location:

                        if (subFilter == InternalPerformanceSummaryForPerformer.Risk)
                        {
                            var CatagoryListforLocation = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);

                            int LocCnt = 0;
                            foreach (InternalCompliancesCategory cc in CatagoryListforLocation)
                            {
                                int Count = 0;
                                if (branchId != -1)
                                    Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0 && entry.CustomerBranchID == branchId).Count();
                                else
                                    Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0).Count();

                                summary.Add(new { Name = cc.Name, Quantity = Count });
                                LocCnt += Count;
                            }
                            flag = LocCnt != 0 ? true : false;
                        }
                        else
                        {
                            if (branchId != -1)
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == branchId);
                            }
                            int HighRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 0).Count();
                            int MediumRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 1).Count();
                            int LowRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 2).Count();
                            summary.Add(new { Name = "Low", Quantity = LowRiskCountForLoc });
                            summary.Add(new { Name = "Medium", Quantity = MediumRiskCountForLoc });
                            summary.Add(new { Name = "High", Quantity = HighRiskCountForLoc });

                            flag = (LowRiskCountForLoc + MediumRiskCountForLoc + HighRiskCountForLoc) != 0 ? true : false;
                        }

                        break;
                    case InternalPerformanceSummaryForPerformer.Reportee:

                        var ReportyQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                            where row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate && row.UserID == catagoryId && row.RoleID == 3
                                            && (row.InternalComplianceStatusID == 1 || row.InternalComplianceStatusID == 10 || row.InternalComplianceStatusID == 6)
                                            select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                        int ReportyHighRiskCount = ReportyQuery.Where(entry => entry.Risk == 0).Count();
                        int ReportyMediumRiskCount = ReportyQuery.Where(entry => entry.Risk == 1).Count();
                        int ReportyLowRiskCount = ReportyQuery.Where(entry => entry.Risk == 2).Count();

                        summary.Add(new { Name = "Low", Quantity = ReportyLowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = ReportyMediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = ReportyHighRiskCount });

                        flag = (ReportyLowRiskCount + ReportyMediumRiskCount + ReportyHighRiskCount) != 0 ? true : false;
                        break;

                }

                displayflag = flag;

                return summary;
            }
        }

        public static List<object> PerformanceSummaryforReviewerDelayed(int customerid,int userID, InternalPerformanceSummaryForPerformer filter, DateTime fromdate, DateTime todate, out bool displayflag, int catagoryId = -1, int branchId = -1, InternalPerformanceSummaryForPerformer subFilter = InternalPerformanceSummaryForPerformer.Risk)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime today = DateTime.Now.Date;
                bool flag = false;
                List<object> summary = new List<object>();

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();


                var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        // join acte in entities.Acts on row.ActID equals acte.ID
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) && row.InternalComplianceStatusID == 5 && row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate
                                         select new
                                         {
                                             ComplianceInstanceID = row.InternalComplianceInstanceID,
                                             row.InternalComplianceID,
                                             //acte.ComplianceCategoryId,
                                             row.IComplianceCategoryID,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.InternalScheduledOnID
                                         }).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                switch (filter)
                {
                    case InternalPerformanceSummaryForPerformer.Risk:
                        var CatagoryList = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);

                        int cnt = 0;
                        foreach (InternalCompliancesCategory cc in CatagoryList)
                        {
                            var Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0).Count();
                            summary.Add(new { Name = cc.Name, Quantity = Count });
                            cnt += Count;
                        }
                        flag = cnt != 0 ? true : false;
                        break;

                    case InternalPerformanceSummaryForPerformer.Category:
                        int HighRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 0).Count();
                        int MediumRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 1).Count();
                        int LowRiskCount = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 2).Count();
                        summary.Add(new { Name = "Low", Quantity = LowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = MediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = HighRiskCount });

                        flag = (LowRiskCount + MediumRiskCount + HighRiskCount) != 0 ? true : false;
                        break;

                    case InternalPerformanceSummaryForPerformer.Location:

                        if (subFilter == InternalPerformanceSummaryForPerformer.Risk)
                        {
                            var CatagoryListforLocation = ComplianceCategoryManagement.GetAll();

                            int LocCnt = 0;
                            foreach (ComplianceCategory cc in CatagoryListforLocation)
                            {
                                int Count = 0;
                                if (branchId != -1)
                                    Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0 && entry.CustomerBranchID == branchId).Count();
                                else
                                    Count = transactionsQuery.Where(entry => entry.IComplianceCategoryID == cc.ID && entry.Risk == 0).Count();

                                summary.Add(new { Name = cc.Name, Quantity = Count });
                                LocCnt += Count;
                            }
                            flag = LocCnt != 0 ? true : false;
                        }
                        else
                        {
                            if (branchId != -1)
                            {
                                transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == branchId);
                            }
                            int HighRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 0).Count();
                            int MediumRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 1).Count();
                            int LowRiskCountForLoc = transactionsQuery.Where(entry => entry.IComplianceCategoryID == catagoryId && entry.Risk == 2).Count();
                            summary.Add(new { Name = "Low", Quantity = LowRiskCountForLoc });
                            summary.Add(new { Name = "Medium", Quantity = MediumRiskCountForLoc });
                            summary.Add(new { Name = "High", Quantity = HighRiskCountForLoc });

                            flag = (LowRiskCountForLoc + MediumRiskCountForLoc + HighRiskCountForLoc) != 0 ? true : false;
                        }

                        break;
                    case InternalPerformanceSummaryForPerformer.Reportee:

                        var ReportyQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                            where row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate && row.UserID == catagoryId
                                            && row.InternalComplianceStatusID == 5 && row.RoleID == 3
                                            select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                        int ReportyHighRiskCount = ReportyQuery.Where(entry => entry.Risk == 0).Count();
                        int ReportyMediumRiskCount = ReportyQuery.Where(entry => entry.Risk == 1).Count();
                        int ReportyLowRiskCount = ReportyQuery.Where(entry => entry.Risk == 2).Count();

                        summary.Add(new { Name = "Low", Quantity = ReportyLowRiskCount });
                        summary.Add(new { Name = "Medium", Quantity = ReportyMediumRiskCount });
                        summary.Add(new { Name = "High", Quantity = ReportyHighRiskCount });

                        flag = (ReportyLowRiskCount + ReportyMediumRiskCount + ReportyHighRiskCount) != 0 ? true : false;
                        break;
                }

                displayflag = flag;
                return summary;
            }
        }

        public static string GetSumofPenaltyForReviewer(int userID, DateTime fromdate, DateTime todate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();


                DateTime now = DateTime.UtcNow.Date;

                var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         join cm in entities.InternalCompliances on row.InternalComplianceID equals cm.ID
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) && row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate
                                         //&& row.ComplianceStatusID == 6 && row.NonComplianceType == 0
                                         && row.InternalComplianceStatusID == 6                                          
                                         select new
                                         {
                                             row.InternalComplianceInstanceID,
                                             row.InternalComplianceStatusID,
                                             row.InternalComplianceID,
                                             //cm.FixedMinimum,
                                             //cm.FixedMaximum,
                                             row.InternalScheduledOnID
                                         }).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault());

                string penalty = "";
                //if (transactionsQuery.Count() != 0)
                //{
                //    double? fixedMinimumValue = transactionsQuery.Sum(entity => entity.FixedMinimum);
                //    double? fixedMaximumValue = transactionsQuery.Sum(entity => entity.FixedMaximum);

                //    penalty = "*Sum of Penalty minimum " + fixedMinimumValue + " and maximum " + fixedMaximumValue + " Rupees is applicable for " + transactionsQuery.Count() + " Non-Compliances.";
                //}

                return penalty;
            }
        }

        public static int GetBranchCountforReviewer(int userID, DateTime fromdate, DateTime todate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) && row.InternalScheduledOn > fromdate && row.InternalScheduledOn < todate
                                         select row).GroupBy(entity => entity.CustomerBranchID).Select(entity => entity.FirstOrDefault());

                return transactionsQuery.Count();
            }
        }
    }
}
