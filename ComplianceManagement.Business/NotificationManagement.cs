﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class NotificationManagement
    {

        public static void SendNotificationMobile(string[] arr1, string body, string title)
        {
            string MobileNotificationApplicationID = ConfigurationManager.AppSettings["MobileNotificationApplicationID"];
            string MobileNotificationSenderID = ConfigurationManager.AppSettings["MobileNotificationSenderID"];
            //var applicationID = "AAAAsnlG7x4:APA91bG_-BXHSF2mTaLAJHegEVfwg81VV41fI5KEZOAOAKuQoCpVpEHs9MVGM_aE_fpwJ5_7iyAtJfqQAZ2ss_6XUQZsW022WDj1eSqk7WFwmKO__3wWAnTQoYEGRudCc5pa03ApAGTY";

            //var senderId = "766538870558";

            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

            tRequest.Method = "post";

            tRequest.ContentType = "application/json";

            var data = new
            {
                //to = deviceId,//for single
                registration_ids = arr1,//for multiple
                notification = new
                {
                    body = body.ToString(),
                    title = title.ToString()
                    //icon = "E:/Andriod App Avantis/AvantisNew/ComplianceManagement.Portal/Images/avantis1.jpg"
                }
            };

            var serializer = new JavaScriptSerializer();
            // var serializer = new System.Web.Extensions.JavaScriptSerializer();

            var json = serializer.Serialize(data);

            Byte[] byteArray = Encoding.UTF8.GetBytes(json);

            tRequest.Headers.Add(string.Format("Authorization: key={0}", MobileNotificationApplicationID));

            tRequest.Headers.Add(string.Format("Sender: id={0}", MobileNotificationSenderID));

            tRequest.ContentLength = byteArray.Length;

            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = tRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();
                            string str = sResponseFromServer;
                        }
                    }
                }
            }
        }

        public static void SendNotificationDailyUpdateMobile(string[] arr1, string body, string title)
        {
            string MobileNotificationApplicationID = ConfigurationManager.AppSettings["MobileDailyUpdateNotificationApplicationID"];
            string MobileNotificationSenderID = ConfigurationManager.AppSettings["MobileDailyUpdateNotificationSenderID"];         
            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            var data = new
            {                
                registration_ids = arr1,//for multiple
                notification = new
                {
                    body = body.ToString(),
                    title = title.ToString()                    
                }
            };

            var serializer = new JavaScriptSerializer();            
            var json = serializer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.Headers.Add(string.Format("Authorization: key={0}", MobileNotificationApplicationID));
            tRequest.Headers.Add(string.Format("Sender: id={0}", MobileNotificationSenderID));
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = tRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            String sResponseFromServer = tReader.ReadToEnd();
                            string str = sResponseFromServer;
                        }
                    }
                }
            }
        }
        public static void CreateNotification(Notification newNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Notifications.Add(newNotification);
                entities.SaveChanges();
            }
        }

        public static void CreateUserNotification(UserNotification newNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.UserNotifications.Add(newNotification);
                entities.SaveChanges();
            }
        }

        public static long UpdateNotification(Notification NewNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationRecordToUpdate = (from row in entities.Notifications
                                                  where row.ActID == NewNotification.ActID
                                                  && row.ComplianceID == NewNotification.ComplianceID
                                                  select row).FirstOrDefault();

                if (NotificationRecordToUpdate != null)
                {
                    NotificationRecordToUpdate.Remark = NewNotification.Remark;
                    NotificationRecordToUpdate.UpdatedBy = NewNotification.UpdatedBy;
                    NotificationRecordToUpdate.UpdatedOn = NewNotification.UpdatedOn;
                    entities.SaveChanges();

                    return NotificationRecordToUpdate.ID;
                }
                else
                    return 0;
            }
        }

        public static void UpdateUserNotification(UserNotification NewNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationRecordToUpdate = (from row in entities.UserNotifications
                                                  where row.NotificationID == NewNotification.NotificationID
                                                  && row.UserID == NewNotification.UserID
                                                  select row).FirstOrDefault();

                if (NotificationRecordToUpdate != null)
                {
                    NotificationRecordToUpdate.IsRead = NewNotification.IsRead;
                    entities.SaveChanges();
                }
            }
        }

        public static List<Notification> GetAllUserNotification(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserNotifications = (from row in entities.Notifications
                                         join row1 in entities.UserNotifications
                                         on row.ID equals row1.NotificationID
                                         where row1.UserID == UserID
                                         select row).ToList();

                return UserNotifications;
            }
        }

        public static int GetUnreadUserNotification(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserNotifications = (from row in entities.UserNotifications
                                         where row.UserID == UserID
                                         && row.IsRead == false
                                         select row).ToList();

                if (UserNotifications != null)
                    return UserNotifications.Count;
                else
                    return 0;
            }
        }

        public static List<String> GetNewUserNotification(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<String> returnNotificationList = new List<string>();

                var UserNotifications = (from row in entities.Notifications
                                         join row1 in entities.UserNotifications
                                         on row.ID equals row1.NotificationID
                                         where row1.UserID == UserID
                                         && row1.IsRead == false
                                         select row).ToList();

                if (UserNotifications.Count > 0)
                {
                    var TypeList = UserNotifications.GroupBy(p => p.Type).Select(g => g.First()).Select(h => h.Type).ToList();

                    TypeList.ForEach(EachType =>
                    {
                        int Count = UserNotifications.Where(Entry => Entry.Type == EachType).Count();

                        if (Count > 0)
                        {
                            String MsgToAdd = String.Empty;

                            MsgToAdd = Count + " " + EachType + " " + "Details Updated";

                            returnNotificationList.Add(MsgToAdd);
                        }
                    });

                }

                return returnNotificationList;
            }
        }

        public static bool ExistsNotification(Notification newNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Notifications
                             where row.ActID == newNotification.ActID
                             && row.ComplianceID == newNotification.ComplianceID
                             select row);

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static bool ExistsUserNotification(UserNotification newNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.UserNotifications
                             where row.NotificationID == newNotification.NotificationID
                             && row.UserID == newNotification.UserID
                             select row);

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static void SetReadToNotification(List<long> Notifications, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var eachNotification in Notifications)
                {
                    var NotificationDetails = (from row in entities.UserNotifications
                                               where row.NotificationID == eachNotification
                                               && row.UserID == userid
                                               select row).FirstOrDefault();

                    if (NotificationDetails != null)
                    {
                        NotificationDetails.IsRead = true;
                    }
                }

                entities.SaveChanges();
            }
        }

        public static Notification GetNotificationRemarkByNotificationID(int NotificationID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationDetails = (from row in entities.Notifications
                                           where row.ID == NotificationID
                                           select row).FirstOrDefault();
               
                    return NotificationDetails;                             
            }
        }
    }
}
