﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class CannedReportManagement
    {
        public static List<CheckListInstanceTransactionReviewerView> GetEventCheckListCannedReportDataForReviewer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int actid, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<CheckListInstanceTransactionReviewerView> transactionsQuery = new List<CheckListInstanceTransactionReviewerView>();

                var GetApprover = (from row in entities.ComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8 || RoleID == 9)
                {
                    transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                         join row1 in entities.EntitiesAssignments
                                          on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.Eventflag == true
                                         select row).Distinct().ToList();

                    transactionsQuery = transactionsQuery.Where(entry => (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                         where row.CustomerID == Customerid && row.Eventflag == true
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID)
                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                }


                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToInt32(type))).ToList();
                }

                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToInt32(category))).ToList();
                }

                //actid Filter
                if (actid != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(actid))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                if (EventID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchudeOnID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }
                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }

        public static List<SP_GetCheckListCannedReportCompliancesSummary_Result> GetEventChecklistCannedReportDataForPerformer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                List<SP_GetCheckListCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetCheckListCannedReportCompliancesSummary_Result>();
                var GetApprover = (from row in entities.ComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8 || RoleID == 9)
                {
                    transactionsQuery = (from row in entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "MGMT")
                                         where row.EventFlag == true
                                         select row).ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "APPR")
                                         where row.EventFlag == true
                                         select row).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "PERF")
                                         where row.EventFlag == true
                                         select row).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToInt32(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToInt32(category))).ToList();
                }
                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(ActID))).ToList();
                }
                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }
                if (RoleID == 8)
                {
                    if (ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn <= ToDate).ToList();
                    }
                }
                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                if (EventID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchudeOnID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();


                //Status
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;
                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;
                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;
                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;
                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;
                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }
                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }

        #region
        //Comment By rahul on 30 Augest 2018
        //public static List<CheckListInstanceTransactionView> GetCheckListReportDataForPerformer(int Customerid, int userID, string Flag, CheckListReportFilterForPerformerCompletedNotCompleted status)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();


        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        if (Flag == "N")//call If Company Admin
        //        {
        //            var transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
        //                                     where row.CustomerID == Customerid && row.RoleID == performerRoleID && row.UserID == userID

        //                                     select row).ToList();

        //            ////Type Filter
        //            //if (type != -1)
        //            //{
        //            //    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
        //            //}
        //            ////category Filter
        //            //if (category != -1)
        //            //{
        //            //    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
        //            //}

        //            switch (status)
        //            {
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.NotCompleted:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
        //                    break;
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.Completed:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
        //                    break;
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.Overdue:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
        //                    break;
        //            }

        //            return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();

        //        }
        //        else
        //        {
        //            var transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID
        //                                       && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                     select row).ToList();
        //            switch (status)
        //            {
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.NotCompleted:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
        //                    break;
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.Completed:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
        //                    break;
        //                case CheckListReportFilterForPerformerCompletedNotCompleted.Overdue:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
        //                    break;
        //            }
        //            return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
        //        }
        //    }
        //}
        public static List<SP_GetCheckListCannedReportCompliancesSummary_Result> GetCheckListReportDataForPerformer(int Customerid, int userID, string Flag, CheckListReportFilterForPerformerCompletedNotCompleted status)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                DateTime now = DateTime.Today.Date;                
                if (Flag == "N")//call If Company Admin
                {
                    var transactionsQuery = (entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "PERF")).ToList();
                    switch (status)
                    {
                        case CheckListReportFilterForPerformerCompletedNotCompleted.NotCompleted:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                            break;
                        case CheckListReportFilterForPerformerCompletedNotCompleted.Completed:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListReportFilterForPerformerCompletedNotCompleted.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
                else
                {
                    var transactionsQuery = (entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "PERF")).ToList();
                    switch (status)
                    {
                        case CheckListReportFilterForPerformerCompletedNotCompleted.NotCompleted:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                            break;
                        case CheckListReportFilterForPerformerCompletedNotCompleted.Completed:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListReportFilterForPerformerCompletedNotCompleted.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
            }
        }
        //Comment By rahul on 30 Augest 2018
        //public static List<CheckListInstanceTransactionView> GetCheckListReportDataForPerformerNew(int Customerid, int userID, string Flag, int risk, CheckListCannedReportPerformer status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();


        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        if (Flag == "N")//call If Company Admin
        //        {
        //            var transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
        //                                     where row.CustomerID == Customerid && row.RoleID == performerRoleID && row.UserID == userID

        //                                     select row).ToList();

        //            //Type Location
        //            if (location != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
        //            }

        //            //Type Filter
        //            if (type != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
        //            }
        //            //category Filter
        //            if (category != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
        //            }

        //            //category Filter
        //            if (ActID != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
        //            }

        //            //Datr Filter
        //            if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
        //            }

        //            //Risk Filter
        //            if (risk != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //            }

        //            //Location
        //            if (location != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

        //            }

        //            switch (status)
        //            {
        //                //case CheckListCannedReportPerformer.NotCompleted:                            
        //                //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
        //                //    break;
        //                case CheckListCannedReportPerformer.Status:
        //                    //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4 && entry.ScheduledOn < nextOneMonth).ToList();

        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
        //                    break;
        //                case CheckListCannedReportPerformer.Upcoming:
        //                    //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now && entry.ScheduledOn < nextOneMonth).ToList();

        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
        //                    break;
        //                case CheckListCannedReportPerformer.ClosedTimely:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
        //                    break;
        //                case CheckListCannedReportPerformer.Overdue:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
        //                    break;
        //            }

        //            return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();

        //        }
        //        else
        //        {
        //            var transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
        //                                     where row.UserID == userID && row.RoleID == performerRoleID
        //                                       && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //                                     select row).ToList();

        //            //Type Filter
        //            if (type != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
        //            }
        //            //category Filter
        //            if (category != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
        //            }

        //            //category Filter
        //            if (ActID != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
        //            }

        //            //Datr Filter
        //            if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
        //            }

        //            //Risk Filter
        //            if (risk != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //            }

        //            //Location
        //            if (location != -1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

        //            }
        //            switch (status)
        //            {
        //                //case CheckListCannedReportPerformer.NotCompleted:                           
        //                //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
        //                //    break;
        //                case CheckListCannedReportPerformer.Status:
        //                    //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4 && entry.ScheduledOn < nextOneMonth).ToList();

        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
        //                    break;
        //                case CheckListCannedReportPerformer.Upcoming:
        //                    //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now && entry.ScheduledOn < nextOneMonth).ToList();

        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
        //                    break;
        //                case CheckListCannedReportPerformer.ClosedTimely:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
        //                    break;
        //                case CheckListCannedReportPerformer.Overdue:
        //                    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
        //                    break;
        //            }
        //            return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
        //        }
        //    }
        //}
        public static List<SP_GetCheckListCannedReportCompliancesSummary_Result> GetCheckListReportDataForPerformerNew(int Customerid, int userID, string Flag, int risk, CheckListCannedReportPerformer status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int ComplianceType, string queryStringFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime now = DateTime.Today.Date;
                DateTime nextOneMonth = DateTime.Today.Date.AddMonths(1);
                if (Flag == "N")//call If Company Admin
                {
                    entities.Database.CommandTimeout = 180;
                    var transactionsQuery = (entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "PERF")).ToList();
                    //Type Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
                    }
                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }
                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }
                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }
                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }
                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                    }
                    switch (status)
                    {
                        case CheckListCannedReportPerformer.Status:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 15).ToList();
                            break;
                        case CheckListCannedReportPerformer.Upcoming:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformer.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.NotApplicable:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID ==15).ToList();
                            break;
                        case CheckListCannedReportPerformer.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }

                    if (queryStringFlag != "A")
                    {
                        //Check Statutory Checklist
                        if (ComplianceType == -1)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == null)).ToList();
                        }
                        //Check EventBased Checklist
                        if (ComplianceType == 0)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                        }
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();

                }
                else
                {
                    entities.Database.CommandTimeout = 180;
                    var transactionsQuery = (entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "PERF")).ToList();
                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }
                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }
                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }
                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }
                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                    }
                    switch (status)
                    {
                        case CheckListCannedReportPerformer.Status:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.Upcoming:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformer.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.NotApplicable:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 15).ToList();
                            break;
                        case CheckListCannedReportPerformer.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }

                    if (queryStringFlag != "A")
                    {
                        //Check Statutory Checklist
                        if (ComplianceType == -1)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == null)).ToList();
                        }
                        //Check EventBased Checklist
                        if (ComplianceType == 0)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                        }
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
            }
        }

        public static List<SP_GetCheckListCannedReportCompliancesSummary_Result> GetCheckListReportDataForPerformerAddedNotCompliedStatus(int Customerid, int userID, string Flag, int risk, CheckListCannedReportPerformerAddedNotCompliedStatus status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int ComplianceType, string queryStringFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime now = DateTime.Today.Date;
                DateTime nextOneMonth = DateTime.Today.Date.AddMonths(1);
                if (Flag == "N")//call If Company Admin
                {
                    entities.Database.CommandTimeout = 180;
                    var transactionsQuery = (entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "PERF")).ToList();
                    //Type Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
                    }
                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }
                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }
                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }
                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }
                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                    }
                    switch (status)
                    {
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Status:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 15).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Upcoming:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.NotApplicable:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 15).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.NotComplied:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 17).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }

                    if (queryStringFlag != "A")
                    {
                        //Check Statutory Checklist
                        if (ComplianceType == -1)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == null)).ToList();
                        }
                        //Check EventBased Checklist
                        if (ComplianceType == 0)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                        }
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();

                }
                else
                {
                    entities.Database.CommandTimeout = 180;
                    var transactionsQuery = (entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "PERF")).ToList();
                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }
                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }
                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }
                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }
                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                    }
                    switch (status)
                    {
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Status:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Upcoming:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.NotApplicable:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 15).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.NotComplied:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 17).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }

                    if (queryStringFlag != "A")
                    {
                        //Check Statutory Checklist
                        if (ComplianceType == -1)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == null)).ToList();
                        }
                        //Check EventBased Checklist
                        if (ComplianceType == 0)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                        }
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
            }
        }

        public static List<SP_GetCheckListCannedReportCompliancesSummary_Result> GetChecklistCannedReportDataForPerformer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {               
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                List<SP_GetCheckListCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetCheckListCannedReportCompliancesSummary_Result>();
                var GetApprover = (from row in entities.ComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8 || RoleID == 9)
                {
                    //transactionsQuery = (entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "MGMT")).ToList();

                    transactionsQuery = (from row in entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "MGMT")
                                         where row.EventFlag == null
                                         select row).ToList();
                }
                else if (RoleID == 6)
                {
                    //transactionsQuery = (entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "APPR")).ToList();

                    transactionsQuery = (from row in entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "APPR")
                                         where row.EventFlag == null
                                         select row).ToList();
                }
                else
                {
                    //transactionsQuery = (entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "PERF")).ToList();
                    transactionsQuery = (from row in entities.SP_GetCheckListCannedReportCompliancesSummary(userID, Customerid, "PERF")
                                         where row.EventFlag == null
                                         select row).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToInt32(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToInt32(category))).ToList();
                }
                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(ActID))).ToList();
                }
                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }
                if (RoleID == 8)
                {
                    if (ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn <= ToDate).ToList();
                    }
                }
                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }
                //Status
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;
                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;
                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;
                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;
                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;
                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }
                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }
        public static List<CheckListInstanceTransactionReviewerView> GetCheckListReportDataForReviewerrNew(int Customerid, int userID, string Flag, int risk, CheckListCannedReportPerformer status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int ComplianceType, string queryStringFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int RevRoleID = (from row in entities.Roles
                                 where row.Code == "RVW1"
                                 select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (Flag == "N")//call If Company Admin
                {
                    entities.Database.CommandTimeout = 180;
                    var transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                             where row.CustomerID == Customerid && row.RoleID == RevRoleID && row.UserID == userID

                                             select row).ToList();

                    //Type Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
                    }

                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }

                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }

                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }

                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }

                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                    }

                    switch (status)
                    {
                        //case CheckListCannedReportPerformer.NotCompleted:                            
                        //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        //    break;
                        case CheckListCannedReportPerformer.Status:
                            //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4 && entry.ScheduledOn < nextOneMonth).ToList();

                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.Upcoming:
                            //transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now && entry.ScheduledOn < nextOneMonth).ToList();

                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformer.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.NotApplicable:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 15).ToList();
                            break;
                        case CheckListCannedReportPerformer.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }
                    if (queryStringFlag != "A")
                    {
                        //Check Statutory Checklist
                        if (ComplianceType == -1)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.Eventflag == null)).ToList();
                        }
                        //Check EventBased Checklist
                        if (ComplianceType == 0)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.Eventflag == true)).ToList();
                        }
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
                else
                {
                    entities.Database.CommandTimeout = 180;
                    var transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                             where row.UserID == userID && row.RoleID == RevRoleID
                                               && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             select row).ToList();

                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }

                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }

                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }

                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }

                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                    }
                    switch (status)
                    {
                        case CheckListCannedReportPerformer.Status:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.Upcoming:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformer.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformer.NotApplicable:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 15).ToList();
                            break;
                        case CheckListCannedReportPerformer.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }

                    if (queryStringFlag != "A")
                    {
                        //Check Statutory Checklist
                        if (ComplianceType == -1)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.Eventflag == null)).ToList();
                        }
                        //Check EventBased Checklist
                        if (ComplianceType == 0)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.Eventflag == true)).ToList();
                        }
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
            }
        }

        public static List<CheckListInstanceTransactionReviewerView> GetCheckListReportDataForReviewerAddedNotCompliedStatus(int Customerid, int userID, string Flag, int risk, CheckListCannedReportPerformerAddedNotCompliedStatus status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int ComplianceType, string queryStringFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int RevRoleID = (from row in entities.Roles
                                 where row.Code == "RVW1"
                                 select row.ID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (Flag == "N")//call If Company Admin
                {
                    entities.Database.CommandTimeout = 180;
                    var transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                             where row.CustomerID == Customerid && row.RoleID == RevRoleID && row.UserID == userID

                                             select row).ToList();

                    //Type Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == Convert.ToByte(location))).ToList();
                    }

                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }

                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }

                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }

                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }

                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                    }

                    switch (status)
                    {
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Status:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Upcoming:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.NotApplicable:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 15).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.NotComplied:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 17).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }
                    if (queryStringFlag != "A")
                    {
                        //Check Statutory Checklist
                        if (ComplianceType == -1)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.Eventflag == null)).ToList();
                        }
                        //Check EventBased Checklist
                        if (ComplianceType == 0)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.Eventflag == true)).ToList();
                        }
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
                else
                {
                    entities.Database.CommandTimeout = 180;
                    var transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                             where row.UserID == userID && row.RoleID == RevRoleID
                                               && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             select row).ToList();
                    //Type Filter
                    if (type != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                    }
                    //category Filter
                    if (category != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                    }

                    //category Filter
                    if (ActID != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                    }

                    //Datr Filter
                    if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                    }

                    //Risk Filter
                    if (risk != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                    }

                    //Location
                    if (location != -1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                    }
                    switch (status)
                    {
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Status:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Upcoming:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn > now).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.ClosedTimely:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.NotApplicable:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 15).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.NotComplied:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 17).ToList();
                            break;
                        case CheckListCannedReportPerformerAddedNotCompliedStatus.Overdue:
                            transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                            break;
                    }

                    if (queryStringFlag != "A")
                    {
                        //Check Statutory Checklist
                        if (ComplianceType == -1)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.Eventflag == null)).ToList();
                        }
                        //Check EventBased Checklist
                        if (ComplianceType == 0)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => (entry.Eventflag == true)).ToList();
                        }
                    }
                    return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
                }
            }
        }

        public static List<CheckListInstanceTransactionReviewerView> GetCheckListCannedReportDataForReviewer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int actid, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<CheckListInstanceTransactionReviewerView> transactionsQuery = new List<CheckListInstanceTransactionReviewerView>();

                var GetApprover = (from row in entities.ComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8 || RoleID == 9)
                {
                    transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                         join row1 in entities.EntitiesAssignments
                                          on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.Eventflag == null
                                         select row).Distinct().ToList();

                    transactionsQuery = transactionsQuery.Where(entry => (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.CheckListInstanceTransactionReviewerViews
                                         where row.CustomerID == Customerid && row.Eventflag == null
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID)
                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                }


                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToInt32(type))).ToList();
                }

                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToInt32(category))).ToList();
                }

                //actid Filter
                if (actid != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(actid))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }
                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }
        #endregion
        public static List<MatrixInformation> GetNonComplianceReportsForApprover(int Customerid, int userID, CannedReportFilterForApprover filter)
        {
            List<MatrixInformation> reportData = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {               
                var complianceInstanceIDs = (from row in entities.ComplianceInstanceAssignmentViews
                                             where row.CustomerID == Customerid && row.RoleID == 6 && row.UserID == userID
                                              && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             select row.ComplianceInstanceID).Distinct().ToList();

                var results = (from row in entities.NonCompliancesSummaryViews
                               where complianceInstanceIDs.Contains(row.ComplianceInstanceID)
                               select row).ToList();

                switch (filter)
                {
                    case CannedReportFilterForApprover.EntityByCategory:
                        reportData = results.GroupBy(entry => new { entry.CustomerBranchID, entry.ComplianceCategoryId }).Select(entry => new MatrixInformation() { Row = entry.Select(row => row.LocationName).FirstOrDefault(), Column = entry.Select(row => row.ComplianceCategoryName).FirstOrDefault(), Count = entry.Count() }).ToList();
                        break;
                    case CannedReportFilterForApprover.CategoryByEntity:
                        reportData = results.GroupBy(entry => new { entry.CustomerBranchID, entry.ComplianceCategoryId }).Select(entry => new MatrixInformation() { Row = entry.Select(row => row.ComplianceCategoryName).FirstOrDefault(), Column = entry.Select(row => row.LocationName).FirstOrDefault(), Count = entry.Count() }).ToList();
                        break;
                    case CannedReportFilterForApprover.RiskByEntity:
                        reportData = results.GroupBy(entry => new { entry.CustomerBranchID, entry.RiskType }).Select(entry => new MatrixInformation() { Row = ((RiskType) entry.Select(row => row.RiskType).FirstOrDefault()).ToString(), Column = entry.Select(row => row.LocationName).FirstOrDefault(), Count = entry.Count() }).ToList();
                        break;
                }
            }

            return reportData;
        }
        //Comment By rahul on 18 April 2018
        //public static List<ComplianceInstanceTransactionView> GetCannedReportDataForPerformer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID, string StringType)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        int performerRoleID = 3;
        //        long RoleID = (from row in entities.Users
        //                       where row.ID == userID
        //                       select row.RoleID).Single();
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
        //        List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();

        //        var GetApprover = (entities.Sp_GetApproverUsers(userID)).ToList();
        //        if (GetApprover.Count > 0)
        //        {
        //            RoleID = 6;
        //        }

        //        if (RoleID == 8 || RoleID == 9)
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 join row1 in entities.EntitiesAssignments
        //                                  on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
        //                                 where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
        //                                 && row1.UserID == userID && row.RoleID == 3
        //                                 select row).Distinct().ToList();

        //            if (ComplianceType == 1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true && entry.ComplinceVisible == true)).ToList();
        //            }

        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
        //        }
        //        else if (RoleID == 6)
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.CustomerID == Customerid
        //                                 select row).ToList();

        //            if (ComplianceType == 1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
        //            }

        //            transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6
        //                && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
        //        }
        //        else
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.CustomerID == Customerid
        //                                 select row).ToList();
        //            if (ComplianceType == 1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
        //            }

        //            transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID
        //                && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
        //        }


        //        if (EventID != -1)
        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

        //        if (EventSchudeOnID != -1)
        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();



        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
        //        }
        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
        //        }

        //        //category Filter
        //        if (ActID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
        //        }

        //        //Datr Filter
        //        if (Convert.ToDateTime(FromDate.ToString()).Date.Year != 1900 && Convert.ToDateTime(ToDate.ToString()).Date.Year != 1900)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
        //        }
        //        if (RoleID == 8)
        //        {
        //            if (Convert.ToDateTime(ToDate.ToString()).Date.Year != 1900)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn <= ToDate).ToList();
        //            }
        //        }

        //        //Risk Filter
        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }

        //        //Location
        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

        //        }
        //        //Status
        //        switch (status)
        //        {
        //            case CannedReportFilterNewStatus.Upcoming:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.Overdue:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.ClosedTimely:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.ClosedDelayed:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.PendingForReview:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.Rejected:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
        //                break;
        //        }

        //        //Type SubType
        //        if (SubTypeID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
        //        }
        //        // Find data through String contained in Description
        //        if (StringType != "")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
        //        }

        //        return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
        //    }
        //}
        public static List<SP_GetCannedReportCompliancesSummary_Result> GetCannedReportDataForPerformer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = 3;
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                List<SP_GetCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetCannedReportCompliancesSummary_Result>();
                var GetApprover = (entities.Sp_GetApproverUsers(userID)).ToList();
                if (GetApprover.Count > 0)
                {
                    RoleID = 6;
                }
                if (RoleID == 8 || RoleID == 9)
                {                    
                    transactionsQuery = (entities.SP_GetCannedReportCompliancesSummary(userID, Customerid, "MGMT")).ToList();
                    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();                    
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true && entry.ComplinceVisible == true)).ToList();
                    }
                }
                else if (RoleID == 6)
                {                                        
                     transactionsQuery = (entities.SP_GetCannedReportCompliancesSummary(userID, Customerid, "APPR")).ToList();
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }
                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (entities.SP_GetCannedReportCompliancesSummary(userID, Customerid, "PERF")).ToList();                    
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }
                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                }
                if (EventID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchudeOnID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId ==category)).ToList();
                }
                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
                }
                //Datr Filter
                if (Convert.ToDateTime(FromDate.ToString()).Date.Year != 1900 && Convert.ToDateTime(ToDate.ToString()).Date.Year != 1900)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }
                if (RoleID == 8)
                {
                    if (Convert.ToDateTime(ToDate.ToString()).Date.Year != 1900)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn <= ToDate).ToList();
                    }
                }
                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }
                //Status
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;
                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;
                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;
                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;
                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;
                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }
                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }
                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }
        //Comment By rahul on 18 April 2018
        //public static List<ComplianceInstanceTransactionView> GetCannedReportDataForReviewer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int actid, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {              
        //        long RoleID = (from row in entities.Users
        //                       where row.ID == userID
        //                       select row.RoleID).Single();

        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();


        //        var GetApprover = (from row in entities.ComplianceAssignments
        //                           where row.UserID == userID
        //                           select row)
        //                           .GroupBy(a => a.RoleID)
        //                           .Select(a => a.FirstOrDefault())
        //                           .Select(entry => entry.RoleID).ToList();

        //        if (GetApprover.Count == 1 && GetApprover.Contains(6))
        //            RoleID = 6;

        //        if (RoleID == 8 || RoleID == 9)
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 join row1 in entities.EntitiesAssignments
        //                                  on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
        //                                 where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
        //                                 && row1.UserID == userID
        //                                 select row).Distinct().ToList();
        //            if (ComplianceType == 1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true && entry.ComplinceVisible == true)).ToList();
        //            }

        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
        //        }
        //        else if (RoleID == 6)
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.CustomerID == Customerid
        //                                 select row).ToList();

        //            if (ComplianceType == 1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
        //            }

        //            transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6
        //                && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
        //        }
        //        else
        //        {
        //            transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
        //                                 where row.CustomerID == Customerid
        //                                 select row).ToList();

        //            if (ComplianceType == 1)
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
        //            }

        //            transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID==4
        //                && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
        //        }



        //        if (EventID != -1)
        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

        //        if (EventSchudeOnID != -1)
        //            transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
        //        }

        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
        //        }

        //        //actid Filter
        //        if (actid != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(actid))).ToList();
        //        }

        //        //Datr Filter
        //        if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
        //        }

        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }

        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
        //        }

        //        switch (status)
        //        {
        //            case CannedReportFilterNewStatus.Upcoming:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.Overdue:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.ClosedTimely:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.ClosedDelayed:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.PendingForReview:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.Rejected:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
        //                break;
        //        }

        //        //Type SubType
        //        if (SubTypeID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
        //        }
        //        return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
        //    }
        //}
        public static List<SP_GetCannedReportCompliancesSummary_Result> GetCannedReportDataForReviewer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int actid, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID)

        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                List<SP_GetCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetCannedReportCompliancesSummary_Result>();
                var GetApprover = (entities.Sp_GetApproverUsers(userID)).ToList();
                if (GetApprover.Count > 0)
                {
                    RoleID = 6;
                }

                if (RoleID == 8 || RoleID == 9)
                {
                    transactionsQuery = (entities.SP_GetCannedReportCompliancesSummary(userID, Customerid, "MGMT")).ToList();
                    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();                   
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true && entry.ComplinceVisible == true)).ToList();
                    }                    
                }
                else if (RoleID == 6)
                {                   
                    transactionsQuery = (entities.SP_GetCannedReportCompliancesSummary(userID, Customerid, "APPR")).ToList();
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }
                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {                   
                    transactionsQuery = (entities.SP_GetCannedReportCompliancesSummary(userID, Customerid, "RVW")).ToList();                
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }
                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 4).ToList();
                }
                if (EventID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchudeOnID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();
                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToInt32(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == Convert.ToInt32(category))).ToList();
                }
                //actid Filter
                if (actid != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(actid))).ToList();
                }
                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }
                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }
        public static List<ComplianceInstanceTransactionView> GetCannedReportDataForApprover(int Customerid, int userID, CannedReportFilterForApprover filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {              
                DateTime now = DateTime.UtcNow.Date;               
                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();
                transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6
                                          && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();

                switch (filter)
                {
                    case CannedReportFilterForApprover.ForFinalApproval:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5).ToList();
                        break;
                }
                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }
        public static List<object> GetDefaultersForApprover(int userID, CannedReportFilterForApprover filter)
        {
            List<object> defaulters = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                var complianceInstanceIDs = (from row in entities.ComplianceInstanceAssignmentViews
                                             where row.UserID == userID && row.RoleID == 6
                                              && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             select row.ComplianceInstanceID).Distinct().ToList();

                var results = (from row in entities.ComplianceTransactions
                               where complianceInstanceIDs.Contains(row.ComplianceInstanceId)
                               select new { UserID = row.CreatedBy, Status = row.StatusId, Name = row.CreatedByText }).ToList();


                var userIDs = results.Select(entry => entry.UserID).Distinct().ToList();
                var defaulterIDs = new List<long>();
                foreach (var item in userIDs)
                {
                    var transactions = results.Where(entry => entry.UserID == item).ToList();
                    for (int index = 0; index < (transactions.Count - 2); index++)
                    {
                        if (transactions[index].Status == 6 && transactions[index + 1].Status == 6 && transactions[index + 2].Status == 6)
                        {
                            if (!defaulterIDs.Contains(transactions[index].UserID))
                            {
                                defaulterIDs.Add(transactions[index].UserID);
                            }
                        }
                    }
                }
                var users = (from row in entities.Users
                             where defaulterIDs.Contains(row.ID)
                             select new { row.ID, row.CustomerBranchID, Name = row.FirstName + " " + row.LastName }).ToList();
                var branchIDs = users.Select(entry => entry.CustomerBranchID).Distinct().ToList();
                var branches = (from row in entities.CustomerBranches
                                where branchIDs.Contains(row.ID)
                                select new { row.ID, row.Name }).ToList();

                defaulters = (from userRow in users
                              join branchRow in branches on userRow.CustomerBranchID equals branchRow.ID into summary
                              from summaryRow in summary.DefaultIfEmpty()
                              select new { Name = userRow.Name, Location = (summaryRow != null ? summaryRow.Name : "--") }).ToList<object>();
            }
            return defaulters;
        }
    }
}
