﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using paytm;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class PayTM_TxnStatus_Response
    {
        public string TXNID { get; set; }
        public string BANKTXNID { get; set; }
        public string ORDERID { get; set; }
        public string TXNAMOUNT { get; set; }
        public string STATUS { get; set; }
        public string TXNTYPE { get; set; }
        public string GATEWAYNAME { get; set; }
        public string RESPCODE { get; set; }
        public string RESPMSG { get; set; }
        public string BANKNAME { get; set; }
        public string MID { get; set; }
        public string PAYMENTMODE { get; set; }
        public string REFUNDAMT { get; set; }
        public string TXNDATE { get; set; }
    }

    public class PaymentManagement
    {
        public static string Get_UniqueOrderID(int çustID, string orderType)
        {
            string orderID = string.Empty;

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstTxnIDs = (from row in entities.PaymentLogs
                                     select row.OrderID).Distinct();
                    do
                    {
                        Random random = new Random();
                        string combination = "0123456789"; //ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz

                        StringBuilder customOrderID = new StringBuilder();
                        string sampleTxnID = string.Empty;

                        if (orderType.Trim().ToUpper().Equals("ORDER"))
                        {
                            customOrderID.Append("AV" + çustID + DateTime.Now.Date.ToString("ddMMyyyy"));
                        }
                        else if (orderType.Trim().ToUpper().Equals("REFUND"))
                        {
                            customOrderID.Append("REF" + çustID + DateTime.Now.Date.ToString("ddMMyyyy"));
                        }

                        for (int i = 0; i < 10; i++)
                            customOrderID.Append(combination[random.Next(combination.Length)]);

                        sampleTxnID = customOrderID.ToString().Substring(0, 20);

                        if (!lstTxnIDs.Contains(sampleTxnID))
                            orderID = sampleTxnID;
                    } while (String.IsNullOrEmpty(orderID));

                    lstTxnIDs = null;

                    return orderID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return orderID;
            }
        }

        public static bool CreatePaymentLog(int custID, int PGID, int TxnType, string orderID, string amount, int statusID, int loggedInUserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    PaymentLog newPaymentLog = new PaymentLog();
                    newPaymentLog.CustomerID = custID;
                    newPaymentLog.PGID = PGID;
                    newPaymentLog.TxnType = TxnType;
                    newPaymentLog.TxnDate = DateTime.Now;
                    newPaymentLog.OrderID = orderID;
                    newPaymentLog.StatusID = statusID;
                    newPaymentLog.Amount = Convert.ToDecimal(amount);
                    newPaymentLog.CreatedOn = DateTime.Now;
                    newPaymentLog.CreatedBy = loggedInUserID;

                    entities.PaymentLogs.Add(newPaymentLog);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateInitial_PaymentTransactionLog(int custID, int mode, int PGID, int TxnType, string orderID, string amount, int statusID, string mail, string phone, int loggedInUserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    PaymentTransactionLog newPaymentLog = new PaymentTransactionLog();
                    newPaymentLog.PGID = PGID;
                    newPaymentLog.OrderID = orderID;
                    newPaymentLog.CustomerID = custID;
                    newPaymentLog.Phone = phone;
                    newPaymentLog.Email = mail;
                    newPaymentLog.StatusID = statusID;
                    newPaymentLog.Amount = Convert.ToDecimal(amount);
                    newPaymentLog.TxnDate = DateTime.Now;

                    newPaymentLog.IsDeleted = false;
                    newPaymentLog.CreatedOn = DateTime.Now;
                    newPaymentLog.CreatedBy = loggedInUserID;

                    newPaymentLog.MODE = mode;

                    var saveSuccess = CreateUpdate_PaymentTransactionLog(newPaymentLog);

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdate_PaymentTransactionLog(PaymentTransactionLog record)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var recordExists = (from row in entities.PaymentTransactionLogs
                                        where row.CustomerID == record.CustomerID
                                        && row.OrderID == record.OrderID
                                        && row.IsDeleted == false
                                        select row).FirstOrDefault();

                    if (recordExists != null)
                    {
                        recordExists.UpdatedBy = record.CreatedBy;
                        recordExists.UpdatedOn = DateTime.Now;
                        recordExists.IsDeleted = false;

                        if (record.StatusID != null)
                            recordExists.StatusID = record.StatusID;

                        if (record.Phone != null)
                            recordExists.Phone = record.Phone;

                        if (record.Email != null)
                            recordExists.Email = record.Email;

                        if (record.Amount != null)
                            recordExists.Amount = record.Amount;

                        if (record.TxnID != null)
                            recordExists.TxnID = record.TxnID;

                        if (record.BankTxnID != null)
                            recordExists.BankTxnID = record.BankTxnID;

                        if (record.Payment_Mode != null)
                            recordExists.Payment_Mode = record.Payment_Mode;

                        if (record.Resp_Code != null)
                            recordExists.Resp_Code = record.Resp_Code;

                        if (record.Resp_MSG != null)
                            recordExists.Resp_MSG = record.Resp_MSG;

                        if (record.Status != null)
                            recordExists.Status = record.Status;

                        if (record.TxnDate != null)
                            recordExists.TxnDate = record.TxnDate;

                        if (record.PG_Type != null)
                            recordExists.PG_Type = record.PG_Type;

                        if (record.payUMoneyID != null)
                            recordExists.payUMoneyID = record.payUMoneyID;

                        if (record.productInfo != null)
                            recordExists.productInfo = record.productInfo;

                        if (record.errorCode != null)
                            recordExists.errorCode = record.errorCode;

                        if (record.MODE != null)
                            recordExists.MODE = record.MODE;

                        entities.SaveChanges();
                    }
                    else
                    {
                        entities.PaymentTransactionLogs.Add(record);
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static string getJsonFormattedString(Dictionary<string, string> parameters)
        {
            string requiredString = string.Empty;

            requiredString = "{";

            foreach (string key in parameters.Keys)
            {
                requiredString += "\"" + key + "\":\"" + parameters[key] + "\",";
            }

            requiredString = requiredString.Trim(',') + "}";

            return requiredString;
        }

        public static PaymentTransactionLog GetPaymentOrderDetailsByOrderID(string orderID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    return entities.PaymentTransactionLogs.Where(row => row.OrderID == orderID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                PaymentTransactionLog newObj = null;
                return newObj;
            }
        }

        public static PaymentTransactionLog GetPaymentOrderDetailsByPaymentID(int paymentID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    return entities.PaymentTransactionLogs.Where(row => row.PaymentID == paymentID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                PaymentTransactionLog newObj = null;
                return newObj;
            }
        }

        #region Paytm

        public static void PaywithPayTM(int customerID, int mode, string orderID, string amount, string phone, string email, int LoginUserID)
        {
            try
            {
                string merchantKey = ConfigurationManager.AppSettings["PayTM_merchantKey"];
                string MID = ConfigurationManager.AppSettings["PayTM_merchantID"];
                string parameter_WebSite = ConfigurationManager.AppSettings["PayTM_WEBSITE_Paramater"];

                string responseURL = ConfigurationManager.AppSettings["Payment_Response_URL"];

                /* initialize a TreeMap object */
                Dictionary<String, String> paytmParams = new Dictionary<String, String>();

                /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                paytmParams.Add("MID", MID);

                /* Find your WEBSITE in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                paytmParams.Add("WEBSITE", parameter_WebSite);

                /* Find your INDUSTRY_TYPE_ID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                paytmParams.Add("INDUSTRY_TYPE_ID", "Retail");

                /* WEB for website and WAP for Mobile-websites or App */
                paytmParams.Add("CHANNEL_ID", "WEB");

                /* Enter your unique order id */
                paytmParams.Add("ORDER_ID", orderID);

                /* unique id that belongs to your customer */
                paytmParams.Add("CUST_ID", customerID.ToString());

                /* customer's mobile number */
                paytmParams.Add("MOBILE_NO", phone);

                /* customer's email */
                paytmParams.Add("EMAIL", email);

                /**
                * Amount in INR that is payble by customer
                * this should be numeric with optionally having two decimal points */
                paytmParams.Add("TXN_AMOUNT", amount);

                /* on completion of transaction, we will send you the response on this URL */
                paytmParams.Add("CALLBACK_URL", responseURL);

                /**
                * Generate checksum for parameters we have
                * You can get Checksum DLL from https://developer.paytm.com/docs/checksum/
                * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                String checksum = paytm.CheckSum.generateCheckSum(merchantKey, paytmParams);

                /* for Staging */
                String url = "https://securegw-stage.paytm.in/order/process";

                /* for Production */
                // String url = "https://securegw.paytm.in/order/process";


                //Dictionary<string, string> parameters = new Dictionary<string, string>();
                //parameters.Add("MID", MID);
                //parameters.Add("CHANNEL_ID", "WEB");
                //parameters.Add("INDUSTRY_TYPE_ID", "Retail109");
                //parameters.Add("WEBSITE", parameter_WebSite);
                //parameters.Add("EMAIL", email);
                //parameters.Add("MOBILE_NO", phone);
                //parameters.Add("CUST_ID", customerID.ToString());
                //parameters.Add("ORDER_ID", orderID);
                //parameters.Add("TXN_AMOUNT", amount);
                //parameters.Add("CALLBACK_URL", responseURL); //This parameter is not mandatory. Use this to pass the callback url dynamically.

                //string checksum = CheckSum.generateCheckSum(merchantKey, parameters);

                PaymentManagement.CreateInitial_PaymentTransactionLog(customerID, mode, 2, 1, orderID, amount, 0, email, phone, LoginUserID); //PGID--2--PayTM, TxnType--1--PaymentRequest, StatusID--0--Initiated
                PaymentManagement.CreatePaymentLog(customerID, 2, 1, orderID, amount, 0, LoginUserID); //PGID--2--PayTM, TxnType--1--PaymentRequest, StatusID--0--Initiated

                PostToPayTM(paytmParams, checksum, orderID);
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void PostToPayTM(Dictionary<string, string> parameters, string checksum, string orderID)
        {
            try
            {
                string payTMUrl = ConfigurationManager.AppSettings["PayTM_BASE_URL"] + "?orderid=" + orderID;

                string outputHTML = "<html>";
                outputHTML += "<head>";
                outputHTML += "<title>Proceed to Payment</title>";
                outputHTML += "</head>";
                outputHTML += "<body>";
                outputHTML += "<center><h1>Please do not refresh this page...</h1></center>";
                outputHTML += "<form method='post' action='" + payTMUrl + "' name='f1'>";
                outputHTML += "<table border='1'>";
                outputHTML += "<tbody>";

                foreach (string key in parameters.Keys)
                {
                    outputHTML += "<input type='hidden' name='" + key + "' value='" + parameters[key] + "'>";
                }

                outputHTML += "<input type='hidden' name='CHECKSUMHASH' value='" + checksum + "'>";
                outputHTML += "</tbody>";
                outputHTML += "</table>";
                outputHTML += "<script type='text/javascript'>";
                outputHTML += "document.f1.submit();";
                outputHTML += "</script>";
                outputHTML += "</form>";
                outputHTML += "</body>";
                outputHTML += "</html>";

                System.Web.HttpContext.Current.Response.Write(outputHTML);
            }
            catch (paytm.exception.CryptoException ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static bool GetTxnStatus_Paytm(int customerID, string orderID, int loggedInUserID)
        {
            try
            {
                bool success = false;
                string contentString = string.Empty;

                string merchantKey = ConfigurationManager.AppSettings["PayTM_merchantKey"];
                string MID = ConfigurationManager.AppSettings["PayTM_merchantID"];
                string TxnStatusURL = ConfigurationManager.AppSettings["PayTM_TxnStatus_URL"];

                /* initialize a Dictionary object */
                Dictionary<String, String> paytmParams = new Dictionary<String, String>();

                /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                paytmParams.Add("MID", MID);

                /* Enter your order id which needs to be check status for */
                paytmParams.Add("ORDERID", orderID);

                /**
                * Generate checksum by parameters we have in body
                * You can get Checksum DLL from https://developer.paytm.com/docs/checksum/
                * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                String checksum = paytm.CheckSum.generateCheckSum(merchantKey, paytmParams);

                /* put generated checksum value here */
                paytmParams.Add("CHECKSUMHASH", checksum);

                #region TxnStatus Request-Response Method-1
                ///* prepare JSON string for request */
                //String post_data = new JavaScriptSerializer().Serialize(paytmParams);

                //try
                //{
                //    HttpWebRequest connection = (HttpWebRequest)WebRequest.Create(TxnStatusURL);
                //    connection.Headers.Add("ContentType", "application/json");
                //    connection.Method = "POST";

                //    using (StreamWriter requestWriter = new StreamWriter(connection.GetRequestStream()))
                //    {
                //        requestWriter.Write(post_data);
                //    }

                //    string responseData = string.Empty;
                //    using (StreamReader responseReader = new StreamReader(connection.GetResponse().GetResponseStream()))
                //    {
                //        responseData = responseReader.ReadToEnd();
                //        var txnStatusResponse = JsonConvert.DeserializeObject<PayTM_TxnStatus_Response>(responseData);
                //        processTxnStatusResponse_PayTM(customerID, orderID, loggedInUserID, txnStatusResponse);
                //    }
                //}
                //catch (Exception ex)
                //{

                //} 
                #endregion


                #region TxnStaus Request-Response Method-2
                //string JsonDataString = getJsonFormattedString(parameters);
                string JsonDataString = new JavaScriptSerializer().Serialize(paytmParams);

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response;

                //Execute the REST API call.
                response = client.GetAsync(TxnStatusURL + "?JsonData=" + JsonDataString).Result;

                if (response.IsSuccessStatusCode)
                {
                    //Get the JSON response.
                    contentString = response.Content.ReadAsStringAsync().Result;

                    var txnStatusResponse = JsonConvert.DeserializeObject<PayTM_TxnStatus_Response>(contentString);

                    processTxnStatusResponse_PayTM(customerID, orderID, loggedInUserID, txnStatusResponse);

                    success = true;
                }
                else
                    success = false;
                #endregion

                return success;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static void processTxnStatusResponse_PayTM(int customerID, string orderID, int loggedInUserID, PayTM_TxnStatus_Response txnStatusResponse)
        {
            try
            {
                if (txnStatusResponse != null)
                {
                    int statusID = 0;

                    if (txnStatusResponse.STATUS != null)
                    {
                        if (txnStatusResponse.STATUS.ToUpper().Contains("SUCCESS"))
                            statusID = 1;
                        else if (txnStatusResponse.STATUS.ToUpper().Contains("FAILURE"))
                            statusID = 2;
                        else if (txnStatusResponse.STATUS.ToUpper().Contains("PENDING"))
                            statusID = 3;
                        else if (txnStatusResponse.STATUS.ToUpper().Contains("OPEN"))
                            statusID = 4;
                    }

                    PaymentTransactionLog newPaymentTxn = new PaymentTransactionLog();

                    string amount = "0";

                    if (!string.IsNullOrEmpty(txnStatusResponse.TXNAMOUNT))
                    {
                        newPaymentTxn.Amount = Convert.ToDecimal(txnStatusResponse.TXNAMOUNT);
                        amount = txnStatusResponse.TXNAMOUNT;
                    }

                    if (!string.IsNullOrEmpty(txnStatusResponse.ORDERID))
                        newPaymentTxn.OrderID = Convert.ToString(txnStatusResponse.ORDERID);

                    if (!string.IsNullOrEmpty(txnStatusResponse.TXNID))
                        newPaymentTxn.TxnID = Convert.ToString(txnStatusResponse.TXNID);

                    if (!string.IsNullOrEmpty(txnStatusResponse.BANKTXNID))
                        newPaymentTxn.BankTxnID = Convert.ToString(txnStatusResponse.BANKTXNID);

                    if (!string.IsNullOrEmpty(txnStatusResponse.RESPCODE))
                        newPaymentTxn.Resp_Code = Convert.ToString(txnStatusResponse.RESPCODE);

                    if (!string.IsNullOrEmpty(txnStatusResponse.RESPMSG))
                        newPaymentTxn.Resp_MSG = Convert.ToString(txnStatusResponse.RESPMSG);

                    if (!string.IsNullOrEmpty(txnStatusResponse.STATUS))
                        newPaymentTxn.Status = Convert.ToString(txnStatusResponse.STATUS);

                    if (!string.IsNullOrEmpty(txnStatusResponse.TXNDATE))
                        newPaymentTxn.TxnDate = Convert.ToDateTime(txnStatusResponse.TXNDATE);

                    if (!string.IsNullOrEmpty(txnStatusResponse.GATEWAYNAME))
                        newPaymentTxn.PG_Type = Convert.ToString(txnStatusResponse.GATEWAYNAME);

                    if (!string.IsNullOrEmpty(txnStatusResponse.PAYMENTMODE))
                        newPaymentTxn.Payment_Mode = Convert.ToString(txnStatusResponse.PAYMENTMODE);

                    newPaymentTxn.StatusID = statusID;
                    newPaymentTxn.CustomerID = customerID;
                    newPaymentTxn.PGID = 2; //2--PayTM

                    newPaymentTxn.IsDeleted = false;

                    PaymentManagement.CreatePaymentLog(customerID, 2, 3, orderID, amount, statusID, loggedInUserID); //PGID--2--PayTM, TxnType--3--TxnStatus Check,
                    PaymentManagement.CreateUpdate_PaymentTransactionLog(newPaymentTxn);
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        public static bool ProcessPaymentDeduction(int custID, int fileID)
        {
            bool processSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var recordToProcess = entities.SP_RLCS_GetUploadedInputRecords(fileID, custID).ToList();
                    if (recordToProcess != null)
                    {
                        if (recordToProcess.Count > 0)
                        {
                            var lstCustomerWiseEmployees = entities.SP_RLCS_GetAllCustomerWiseEmployees(custID).ToList();

                            if (lstCustomerWiseEmployees != null)
                            {
                                if (lstCustomerWiseEmployees.Count > 0)
                                {
                                    var masterPricingPlan = entities.RLCS_PricingPlan.ToList();

                                    List<RLCS_CustomerBranchWisePaymentDeduction> lstRecords = new List<RLCS_CustomerBranchWisePaymentDeduction>();

                                    recordToProcess.ForEach(eachRecord =>
                                    {
                                        int? employeeCount = lstCustomerWiseEmployees.Where(row => row.AVACOM_BranchID == eachRecord.BranchID).Select(row => row.EmployeeCount).FirstOrDefault();

                                        if (employeeCount == null)
                                            employeeCount = 0;

                                        decimal pricing = GetBranchEmployeeWisePricing(masterPricingPlan, Convert.ToInt32(employeeCount));

                                        RLCS_CustomerBranchWisePaymentDeduction newRecord = new RLCS_CustomerBranchWisePaymentDeduction()
                                        {
                                            CustomerID = custID,
                                            BranchID = Convert.ToInt32(eachRecord.BranchID),
                                            Period = eachRecord.Period,
                                            Year = Convert.ToString(eachRecord.Year),
                                            EmployeeCount = Convert.ToInt32(employeeCount),
                                            AmountDeducted = pricing,
                                            IsActive = true
                                        };

                                        lstRecords.Add(newRecord);
                                    });

                                    if (lstRecords.Count > 0)
                                        processSuccess = CreateUpdate_PaymentDeductionRecord(lstRecords);
                                }
                            }
                        }
                    }

                }

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ProcessPaymentDeduction(int custID, int branchID, string period, string year)
        {
            bool processSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstCustomerWiseEmployees = entities.SP_RLCS_GetAllCustomerWiseEmployees(custID).ToList();

                    if (lstCustomerWiseEmployees != null)
                    {
                        if (lstCustomerWiseEmployees.Count > 0)
                        {
                            var masterPricingPlan = entities.RLCS_PricingPlan.ToList();

                            int? employeeCount = lstCustomerWiseEmployees.Where(row => row.AVACOM_BranchID == branchID).Select(row => row.EmployeeCount).FirstOrDefault();

                            if (employeeCount == null)
                                employeeCount = 0;

                            decimal pricing = GetBranchEmployeeWisePricing(masterPricingPlan, Convert.ToInt32(employeeCount));

                            RLCS_CustomerBranchWisePaymentDeduction newRecord = new RLCS_CustomerBranchWisePaymentDeduction()
                            {
                                CustomerID = custID,
                                BranchID = Convert.ToInt32(branchID),
                                Period = period,
                                Year = Convert.ToString(year),
                                EmployeeCount = Convert.ToInt32(employeeCount),
                                AmountDeducted = pricing,
                                IsActive = true
                            };

                            processSuccess = CreateUpdate_PaymentDeductionRecord(newRecord);
                        }
                    }
                }

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ProcessPaymentDeduction_New(int custID, int branchID, string period, string year)
        {
            bool processSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstCustomerWiseEmployees = entities.SP_RLCS_GetAllCustomerWiseEmployees(custID).ToList();

                    if (lstCustomerWiseEmployees != null)
                    {
                        if (lstCustomerWiseEmployees.Count > 0)
                        {
                            var masterPricingPlan = entities.RLCS_PricingPlan.ToList();

                            int? employeeCount = lstCustomerWiseEmployees.Where(row => row.AVACOM_BranchID == branchID).Select(row => row.EmployeeCount).FirstOrDefault();

                            if (employeeCount == null)
                                employeeCount = 0;

                            RLCS_CustomerBranchWisePaymentDeduction newRecord = new RLCS_CustomerBranchWisePaymentDeduction()
                            {
                                CustomerID = custID,
                                BranchID = Convert.ToInt32(branchID),
                                Period = period,
                                Year = Convert.ToString(year),
                                EmployeeCount = Convert.ToInt32(employeeCount),
                                IsActive = true
                            };

                            processSuccess = CreateUpdate_PaymentDeductionRecord(newRecord);

                            if (processSuccess)
                            {
                                try
                                {
                                    entities.SP_RLCS_ProcessCustomerWiseDeductions(custID, period, year, DateTime.Now);
                                }
                                catch (Exception ex)
                                {
                                    ContractManagement.InsertErrorMsg_DBLog("", "ProcessPaymentDeduction_New-CustID-" + custID + "-Period-" + period + "-Year-" + year,
                                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                            }
                        }
                    }
                }

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static decimal GetBranchEmployeeWisePricing(List<RLCS_PricingPlan> masterPricingPlan, int employeeCount)
        {
            try
            {
                var pricing = masterPricingPlan.Where(row => (employeeCount >= row.MinEmp && employeeCount <= row.MaxEmp)
                                             && (DateTime.Now >= row.ValidFrom && (DateTime.Now <= row.ValidUntil || row.ValidUntil == null)))
                                            .Select(row => row.Price).FirstOrDefault();

                return pricing;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool CreateUpdate_PaymentDeductionRecord(List<RLCS_CustomerBranchWisePaymentDeduction> lstRecords)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstRecords.ForEach(_objRecord =>
                    {
                        var prevRecord = (from row in entities.RLCS_CustomerBranchWisePaymentDeduction
                                          where row.BranchID == _objRecord.BranchID
                                          && row.Period == _objRecord.Period
                                          && row.Year == _objRecord.Year
                                          && row.IsActive == _objRecord.IsActive
                                          select row).FirstOrDefault();

                        if (prevRecord != null)
                        {
                            prevRecord.EmployeeCount = _objRecord.EmployeeCount;
                            prevRecord.AmountDeducted = _objRecord.AmountDeducted;
                            prevRecord.UpdatedOn = DateTime.Now;

                            saveSuccess = true;
                        }
                        else
                        {
                            _objRecord.CreatedOn = DateTime.Now;

                            entities.RLCS_CustomerBranchWisePaymentDeduction.Add(_objRecord);
                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdate_PaymentDeductionRecord(RLCS_CustomerBranchWisePaymentDeduction _objRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_CustomerBranchWisePaymentDeduction
                                      where row.BranchID == _objRecord.BranchID
                                      && row.Period == _objRecord.Period
                                      && row.Year == _objRecord.Year
                                      && row.IsActive == _objRecord.IsActive
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.EmployeeCount = _objRecord.EmployeeCount;
                        prevRecord.AmountDeducted = _objRecord.AmountDeducted;
                        prevRecord.UpdatedOn = DateTime.Now;

                        saveSuccess = true;
                    }
                    else
                    {
                        _objRecord.CreatedOn = DateTime.Now;

                        entities.RLCS_CustomerBranchWisePaymentDeduction.Add(_objRecord);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        /// <summary>
        /// Check the Balance of Provided CustomerID's Distributor
        /// </summary>
        /// <param name="custID">For Which Document Generating</param>
        /// <returns>
        /// Code - (0-NonPaymentCustomer, 1-Payment Customer and Balance Available, 2-Payment Customer but Balance not available)
        /// </returns>
        public static int CheckAvailableBalance(int custID)
        {
            int resultBalance = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var balanceRecord = entities.SP_RLCS_CheckBalance(custID).FirstOrDefault();

                    if (balanceRecord != null)
                    {
                        if (balanceRecord.IsPayment != null)
                        {
                            if (Convert.ToBoolean(balanceRecord.IsPayment))
                            {
                                if (balanceRecord.Balance != null)
                                {
                                    if (balanceRecord.Balance > 0)
                                        resultBalance = 2;
                                }
                            }
                        }
                        else
                            resultBalance = 1;
                    }
                }

                return resultBalance;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
    }
}
