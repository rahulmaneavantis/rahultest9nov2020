﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using Nelibur.ObjectMapper;
using System.Data.Entity;
using Newtonsoft.Json;
using System.Globalization; 

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class RLCS_ClientsManagement
    {
        public static string GetClientSPOC(string clientID, string requestUrl)
        {
            string SPOCname = string.Empty;
            try
            {
                requestUrl += "GetClientMaster";

                string responseData = WebAPIUtility.Invoke("GET", requestUrl, "");

                if (!string.IsNullOrEmpty(responseData))
                {
                    var res_ClientsMaster = JsonConvert.DeserializeObject<Response_Clients_Master>(responseData);

                    if (res_ClientsMaster != null)
                    {
                        var lstClients_Master = res_ClientsMaster.Clients;

                        if (lstClients_Master.Count > 0)
                        {
                            var clientRecord = lstClients_Master.Where(row => row.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())).FirstOrDefault();

                            if (clientRecord != null)
                            {
                                SPOCname = clientRecord.CB_SPOCSanitation + " " + clientRecord.CB_SPOC_Name + " " + clientRecord.CB_SPOC_LastName;
                            }
                        }
                    }
                }

                return SPOCname;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return SPOCname;
            }
        }

        public static List<ComplianceAssignmentEntitiesView> GetAssignedEntityBranches(int customerID = -1, int userID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstAllAssignedEntityBranches = (from row in entities.ComplianceAssignmentEntitiesViews
                                                    select row).ToList();

                if (lstAllAssignedEntityBranches.Count > 0)
                {
                    lstAllAssignedEntityBranches = (from g in lstAllAssignedEntityBranches
                                                    group g by new
                                                    {
                                                        g.userID,
                                                        g.UserName,
                                                        g.BranchID,
                                                        g.Branch,
                                                    } into GCS
                                                    select new ComplianceAssignmentEntitiesView()
                                                    {
                                                        userID = GCS.Key.userID,
                                                        UserName = GCS.Key.UserName,
                                                        BranchID = GCS.Key.BranchID,
                                                        Branch = GCS.Key.Branch,
                                                    }).ToList();


                    var branchIds = (from row in entities.CustomerBranches
                                     where row.IsDeleted == false
                                     && row.CustomerID == customerID
                                     select row.ID).ToList();

                    if (branchIds != null)
                    {
                        lstAllAssignedEntityBranches = lstAllAssignedEntityBranches.Where(entry => branchIds.Contains((int)entry.BranchID)).ToList();
                    }

                    if (userID != -1)
                    {
                        lstAllAssignedEntityBranches = lstAllAssignedEntityBranches.Where(entry => entry.userID == userID).ToList();
                    }
                }

                return lstAllAssignedEntityBranches;
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetAll_EntityClient(int customerID, List<int> lstAssignedEntityBranch)
        {
            List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstEntities = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstEntities = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                   where row.AVACOM_CustomerID == customerID
                                   && row.BranchType == "E"
                                   && row.CM_Status == "A"
                                   && row.IsProcessed == true
                                   && row.AVACOM_BranchID != null
                                   && lstAssignedEntityBranch.Contains((int)row.AVACOM_BranchID)
                                   select row).ToList();
                }

                return lstEntities;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstEntities;
            }
        }

        public static List<RLCS_CustomerBranchView> GetAll_EntityBranch_RLCS(int customerID, long parentID, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerBranches = (from row in entities.RLCS_CustomerBranchView
                                        where row.IsDeleted == false
                                        && row.CustomerID == customerID
                                        select row).ToList();

                if (parentID != -1)
                {
                    customerBranches = customerBranches.Where(entry => entry.ParentID == parentID).ToList();
                }
                else
                {
                    customerBranches = customerBranches.Where(entry => entry.ParentID == null).ToList();
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    customerBranches = customerBranches.Where(entry => entry.Name.Contains(filter) || entry.SM_Name.Contains(filter) || entry.LM_Name.Contains(filter) || entry.CM_City.Contains(filter) || entry.CM_State.Contains(filter)).ToList();
                }

                if (customerBranches.Count() > 0)
                {
                    customerBranches = (from g in customerBranches
                                        group g by new
                                        {
                                            g.ID,
                                            g.CustomerID,
                                            g.CM_ClientID,
                                            g.ParentID,
                                            g.Name,
                                            g.Type,
                                            g.StateID,
                                            g.CM_State,
                                            g.SM_Name,
                                            g.CityID,
                                            g.CM_City,
                                            g.LM_Name,
                                            g.TypeName,
                                            g.ComplianceProductType,
                                            g.CM_EstablishmentType,
                                            g.BranchType,
                                        } into GCS
                                        select new RLCS_CustomerBranchView()
                                        {
                                            ID = GCS.Key.ID, //ContractID
                                            CustomerID = GCS.Key.CustomerID,
                                            CM_ClientID = GCS.Key.CM_ClientID,
                                            ParentID = GCS.Key.ParentID,
                                            Name = GCS.Key.Name,
                                            Type = GCS.Key.Type,
                                            StateID = GCS.Key.StateID,
                                            CM_State = GCS.Key.CM_State,
                                            SM_Name = GCS.Key.SM_Name,
                                            CityID = GCS.Key.CityID,
                                            CM_City = GCS.Key.CM_City,
                                            LM_Name = GCS.Key.LM_Name,
                                            TypeName = GCS.Key.TypeName,
                                            ComplianceProductType = GCS.Key.ComplianceProductType,
                                            CM_EstablishmentType = GCS.Key.CM_EstablishmentType,
                                            BranchType = GCS.Key.BranchType,
                                        }).ToList();
                }

                return customerBranches.ToList();
            }
        }

        public static int GetCustomerIDByCustomerBranchID(int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var custID = (from row in entities.CustomerBranches
                              where row.ID == customerBranchID
                              select row.CustomerID).FirstOrDefault();

                return custID;
            }
        }

        public static List<NameValueHierarchy> GetAllHierarchy(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                IQueryable<CustomerBranch> query1 = (from row in entities.CustomerBranches
                                                     where row.IsDeleted == false
                                                     && row.Status == 1
                                                     && row.CustomerID == customerID
                                                     select row);

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities, query1);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, IQueryable<CustomerBranch> lstCustBranches)
        {
            var query = lstCustBranches;

            if (isClient)
            {
                query = lstCustBranches.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = lstCustBranches.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntities(item, false, entities, lstCustBranches);
            }
        }

        public static string GetClientID(string ClientID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string Present = "";
                    Present = (from c in entities.RLCS_CustomerBranch_ClientsLocation_Mapping where c.CM_ClientID == ClientID && c.BranchType == "E" select c.CM_ClientName).FirstOrDefault();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }

        public static int GetBranchIDByBranchName(string Name)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int BranchID = 0;
                    BranchID = (from c in entities.CustomerBranches where c.Name == Name select c.ID).FirstOrDefault();

                    return BranchID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool CheckBranchIDExist(string Name, string ClientID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool Present = false;
                    Present = (from c in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                               where c.AVACOM_BranchName == Name 
                               && c.CM_ClientID == ClientID
                               select c).Any();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CheckBranchCodeExist(string BranchCode)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool Present = false;
                    Present = (from c in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                               where c.CL_BranchCode == BranchCode
                               select c).Any();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static void Update_ProcessedStatus_ClientBasicDetail(int BranchID, string ClientID, bool status)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var ClientBasicRecord = (from row in entities.RLCS_Client_BasicDetails
                                             where row.AVACOM_BranchID == BranchID
                                             && row.CB_ClientID == ClientID
                                             select row).FirstOrDefault();

                    var ClientEntityRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                              where row.AVACOM_BranchID == BranchID
                                              && row.BranchType == "E"
                                             && row.CM_ClientID == ClientID
                                              select row).FirstOrDefault();
                    if (ClientBasicRecord != null && ClientEntityRecord != null)
                    {
                        ClientEntityRecord.IsProcessed = status;
                        ClientBasicRecord.IsProcessed = status;
                        entities.SaveChanges();
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }

        public static void Update_ProcessedStatus_ClientLocationDetail(int BranchID, string ClientID, bool status)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var rlcsCustomerBranchRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                where row.AVACOM_BranchID == BranchID
                                                && row.CM_ClientID == ClientID
                                                && row.BranchType == "B"
                                                select row).FirstOrDefault();

                if (rlcsCustomerBranchRecord != null)
                {
                    rlcsCustomerBranchRecord.IsProcessed = status;
                    entities.SaveChanges();
                }
            }
        }

        #region SetUp 
        public static List<RLCS_Country_Mapping> GetAllCountry()
        {
            List<RLCS_Country_Mapping> countries = new List<RLCS_Country_Mapping>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    countries = (from c in entities.RLCS_Country_Mapping select c).ToList();

                    return countries;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static string GetAllDesignation(string name)
        {
          //  List<RLCS_Designation_Master> designation = new List<RLCS_Designation_Master>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                 string designation = (from c in entities.RLCS_Designation_Master
                                   where c.Long_designation == name
                                   && c.Status.Trim().ToUpper().Equals("A")
                                   && c.Status != null
                                   select c.Long_designation).FirstOrDefault();

                    return designation;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public static string GetRLCSCountry(string country)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string RLCSCountry = (from row in entities.RLCS_Country_Mapping
                                          where row.CNT_Name == country && row.CNT_Status =="A"
                                          select row.CNT_ID).FirstOrDefault();

                    return RLCSCountry;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public static bool GetCountrybyname(string country)
        {
            bool present = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    present = (from c in entities.RLCS_Country_Mapping select c).Any(c => c.CNT_ID == country);

                    return present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return present;
            }
        }

        public static void deleteCustomerBranch(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var tet = entities.CustomerBranches.Where(x => x.ID == Userid).FirstOrDefault();
                    entities.CustomerBranches.Remove(tet);
                    entities.SaveChanges();
                    Userid--;
                    entities.SP_ResetIDCustomerBranch(Userid);
                }
                catch (Exception ex)
                {
                }
            }
        }

        public static bool UpdateClientInfo(RLCS_CustomerBranch_ClientsLocation_Mapping ClientBranch, int state, string city)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool update_complianceDB = false;
                bool Update_AuditDB = false;
                try
                {
                    if (!String.IsNullOrEmpty(ClientBranch.CM_ClientName))
                    {
                        CustomerBranch CustomerBranch = (from row in entities.CustomerBranches
                                                         where row.ID == ClientBranch.AVACOM_BranchID
                                                         && row.IsDeleted == false
                                                         select row).FirstOrDefault();
                        if (CustomerBranch != null)
                        {
                            //Already exist CustomerBranch - Update
                            if (state != 0)
                                CustomerBranch.StateID = state;

                            if (!String.IsNullOrEmpty(city))
                                CustomerBranch.CityID = (from s in entities.RLCS_Location_City_Mapping where s.LM_Code == city select (int)s.AVACOM_CityID).FirstOrDefault();

                            CustomerBranch.AddressLine1 = ClientBranch.CM_Address;
                            CustomerBranch.IsDeleted = false;

                            if (ClientBranch.CM_Status == "A")
                                CustomerBranch.Status = 1;
                            else
                                CustomerBranch.Status = 0;
                            //NEW ADD by GG
                            CustomerBranch.ContactPerson = ClientBranch.CL_HRContactPerson;
                            CustomerBranch.Landline = ClientBranch.CL_HR1stLevelPhNo;
                            CustomerBranch.EmailID= ClientBranch.CL_HRMailID;
                            entities.SaveChanges();

                            update_complianceDB = true;

                            if (update_complianceDB)
                            {
                                TinyMapper.Bind<CustomerBranch, mst_CustomerBranch>();
                                mst_CustomerBranch Custbranch1 = TinyMapper.Map<mst_CustomerBranch>(CustomerBranch);

                                entities.SaveChanges();
                                Update_AuditDB = true;

                                if (update_complianceDB && Update_AuditDB)
                                {
                                    RLCS_CustomerBranch_ClientsLocation_Mapping BranchUpdate = (from entry in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                                                                where entry.AVACOM_BranchID == CustomerBranch.ID
                                                                                                && entry.BranchType == "E"
                                                                                                select entry).FirstOrDefault();

                                    if (BranchUpdate != null)
                                    {
                                        BranchUpdate.CM_EstablishmentType = ClientBranch.CM_EstablishmentType;
                                        //BranchUpdate.CM_ServiceStartDate = ClientBranch.CM_ServiceStartDate;

                                        if (state != 0)
                                            BranchUpdate.CM_State = (from s in entities.RLCS_State_Mapping where s.AVACOM_StateID == state select s.SM_Code).FirstOrDefault();

                                        BranchUpdate.CM_City = city;
                                        BranchUpdate.CM_Pincode = city;
                                        BranchUpdate.CM_Status = ClientBranch.CM_Status;
                                        BranchUpdate.CM_Address = ClientBranch.CM_Address;
                                        BranchUpdate.CM_BonusPercentage = ClientBranch.CM_BonusPercentage;
                                        BranchUpdate.CM_Excemption = ClientBranch.CM_Excemption;
                                        BranchUpdate.CM_ActType = ClientBranch.CM_ActType;

                                        //BranchUpdate.CM_IsBonusExcempted = ClientBranch.CM_IsBonusExcempted;	
                                        BranchUpdate.CM_IsPOApplicable = ClientBranch.CM_IsPOApplicable;
                                        BranchUpdate.CL_PF_Code = ClientBranch.CL_PF_Code;
                                        BranchUpdate.IsProcessed = false;
                                        BranchUpdate.UpdatedOn = DateTime.Now;
                                        BranchUpdate.CM_IsAventisClientOrBranch = ClientBranch.CM_IsAventisClientOrBranch;
                                        entities.SaveChanges();

                                        update_complianceDB = true;
                                    }
                                    else
                                    {
                                        //Create New RLCS_CustomerBranch_ClientsLocation_Mapping
                                        RLCS_CustomerBranch_ClientsLocation_Mapping Branch = new RLCS_CustomerBranch_ClientsLocation_Mapping();

                                        Branch.AVACOM_BranchID = CustomerBranch.ID;
                                        Branch.AVACOM_BranchName = ClientBranch.CM_ClientName;
                                        Branch.BranchType = "E";
                                        Branch.CO_CorporateID = ClientBranch.CO_CorporateID;
                                        Branch.CM_ClientID = ClientBranch.CM_ClientID;
                                        Branch.CM_ClientName = ClientBranch.CM_ClientName;
                                        Branch.CM_EstablishmentType = ClientBranch.CM_EstablishmentType;
                                        //Branch.CM_ServiceStartDate = ClientBranch.CM_ServiceStartDate;

                                        if (state != 0)
                                            Branch.CM_State = (from s in entities.RLCS_State_Mapping where s.AVACOM_StateID == state select s.SM_Code).FirstOrDefault();

                                        Branch.CM_City = city;
                                        Branch.CM_Pincode = city;
                                        Branch.CM_Status = ClientBranch.CM_Status;

                                        Branch.AVACOM_CustomerID = CustomerBranch.CustomerID;
                                        Branch.CM_Address = ClientBranch.CM_Address;
                                        Branch.CM_BonusPercentage = ClientBranch.CM_BonusPercentage;
                                        Branch.CM_Excemption = ClientBranch.CM_Excemption;
                                        Branch.CM_ActType = ClientBranch.CM_ActType;
                                        
                                        Branch.CreatedOn = DateTime.Now;
                                        Branch.CM_IsAventisClientOrBranch = ClientBranch.CM_IsAventisClientOrBranch;

                                        //Branch.CM_IsBonusExcempted = ClientBranch.CM_IsBonusExcempted;//30april	
                                        Branch.CM_IsPOApplicable = ClientBranch.CM_IsPOApplicable;//30april
                                        Branch.CL_PF_Code = ClientBranch.CL_PF_Code;
                                        //BranchUpdate.CM_Excemption = ClientBranch.CM_Excemption;
                                        //Branch.CM_ActType = ClientBranch.CM_ActType;

                                        update_complianceDB = CreateUpdate_CustomerBranch_ClientsOrLocation_Mapping(Branch);
                                        ClientBranch.AVACOM_BranchID = Branch.AVACOM_BranchID;
                                    }
                                }
                            }
                        }
                        else
                        {
                            //Not Exists CustomerBranch - Create New
                            CustomerBranch CustBranch = new CustomerBranch();

                            CustBranch.Type = 1;

                            CustBranch.StateID = state;
                            if (!String.IsNullOrEmpty(city))
                                CustBranch.CityID = (from s in entities.RLCS_Location_City_Mapping where s.LM_Code == city select (int)s.AVACOM_CityID).FirstOrDefault();

                            CustBranch.AuditPR = false;

                            CustBranch.Name = ClientBranch.CM_ClientName;
                            if (ClientBranch.CM_Status == "A")
                                CustBranch.Status = 1;
                            else
                                CustBranch.Status = 0;
                            CustBranch.CreatedOn = DateTime.Now;
                            CustBranch.IsDeleted = false;


                            if (!String.IsNullOrEmpty(ClientBranch.CO_CorporateID))
                                CustBranch.CustomerID = (from c in entities.RLCS_Customer_Corporate_Mapping where c.CO_CorporateID == ClientBranch.CO_CorporateID select (int)c.AVACOM_CustomerID).FirstOrDefault();

                            CustBranch.AddressLine1 = ClientBranch.CM_Address;
                            //NEW ADD by GG
                            CustBranch.ContactPerson = ClientBranch.CL_HRContactPerson;
                            CustBranch.Landline = ClientBranch.CL_HR1stLevelPhNo;
                            CustBranch.EmailID = ClientBranch.CL_HRMailID;
                            entities.CustomerBranches.Add(CustBranch);
                            entities.SaveChanges();

                            update_complianceDB = true;
                            if (update_complianceDB)
                            {
                                mst_CustomerBranch Custbranch1 = new mst_CustomerBranch();
                                TinyMapper.Bind<CustomerBranch, mst_CustomerBranch>();
                                Custbranch1 = TinyMapper.Map<mst_CustomerBranch>(CustBranch);

                                Update_AuditDB = CustomerBranchManagement.CreateRLCS(Custbranch1);
                                entities.SaveChanges();
                                // Update1 = true;

                                if (update_complianceDB && Update_AuditDB)
                                {
                                    RLCS_CustomerBranch_ClientsLocation_Mapping RLCS_Branch = new RLCS_CustomerBranch_ClientsLocation_Mapping();

                                    if (state != 0)
                                        RLCS_Branch.CM_State = (from s in entities.RLCS_State_Mapping
                                                                where s.AVACOM_StateID == state
                                                                select s.SM_Code).FirstOrDefault();
                                    if (city != null)
                                        RLCS_Branch.CM_City = city;
                                    RLCS_Branch.CM_Pincode = city;
                                    RLCS_Branch.BranchType = "E";
                                    RLCS_Branch.CM_ClientID = ClientBranch.CM_ClientID;
                                    RLCS_Branch.CO_CorporateID = ClientBranch.CO_CorporateID;
                                    RLCS_Branch.CM_ClientName = ClientBranch.CM_ClientName;
                                    RLCS_Branch.AVACOM_BranchName = ClientBranch.CM_ClientName;
                                    RLCS_Branch.AVACOM_BranchID = CustBranch.ID;
                                    RLCS_Branch.CM_Address = ClientBranch.CM_Address;
                                    RLCS_Branch.CL_CompPhoneNo = ClientBranch.CL_CompPhoneNo;
                                    RLCS_Branch.CM_ActType = ClientBranch.CM_ActType;
                                    RLCS_Branch.CM_EstablishmentType = ClientBranch.CM_EstablishmentType;
                                    RLCS_Branch.CM_Excemption = ClientBranch.CM_Excemption;
                                    //RLCS_Branch.CM_ServiceStartDate = ClientBranch.CM_ServiceStartDate;
                                    RLCS_Branch.CM_BonusPercentage = ClientBranch.CM_BonusPercentage;
                                    RLCS_Branch.CM_Status = ClientBranch.CM_Status;
                                    RLCS_Branch.CM_ActType = ClientBranch.CM_ActType;
                                    RLCS_Branch.CM_Address = ClientBranch.CM_Address;
                                    RLCS_Branch.CreatedOn = DateTime.Now;
                                    RLCS_Branch.CM_IsAventisClientOrBranch = ClientBranch.CM_IsAventisClientOrBranch;
                                    RLCS_Branch.IsProcessed = false;
                                    RLCS_Branch.AVACOM_CustomerID = ClientBranch.AVACOM_CustomerID;

                                    //RLCS_Branch.CM_IsBonusExcempted = ClientBranch.CM_IsBonusExcempted;//30apr	
                                    RLCS_Branch.CM_IsPOApplicable = ClientBranch.CM_IsPOApplicable;//30apr
                                    RLCS_Branch.CL_PF_Code = ClientBranch.CL_PF_Code;
                                    entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Add(RLCS_Branch);
                                    entities.SaveChanges();
                                    update_complianceDB = true;
                                    ClientBranch.AVACOM_BranchID = RLCS_Branch.AVACOM_BranchID;
                                }
                                else
                                {
                                    RLCS_ClientsManagement.DeleteCustomerBranch_Compliance(CustBranch.ID);
                                    Update_AuditDB = false;
                                }
                            }
                        }
                        return update_complianceDB;
                    }
                    else
                    {
                        return Update_AuditDB;
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //entities.Connection.Close();
                    return false;
                }

            }
        }

        public static bool UpdateClientLocationInfo(RLCS_CustomerBranch_ClientsLocation_Mapping ClientBranch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //entities.Connection.Open();
                try
                {
                    if (ClientBranch.AVACOM_BranchID != 0)
                    {
                        bool update = false;
                        bool Update1 = false;

                        CustomerBranch CustomerBranch = (from row in entities.CustomerBranches
                                                         where row.ID == ClientBranch.AVACOM_BranchID
                                                         && row.IsDeleted == false
                                                         select row).FirstOrDefault();

                        RLCS_CustomerBranch_ClientsLocation_Mapping CBranch = (from entry in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                                               where entry.AVACOM_BranchName == ClientBranch.AVACOM_BranchName
                                                                               && entry.CO_CorporateID == ClientBranch.CO_CorporateID
                                                                               && entry.BranchType == "B"
                                                                               select entry).FirstOrDefault();
                        if (CustomerBranch != null && CBranch != null)
                        {
                            if (!String.IsNullOrEmpty(ClientBranch.CM_State))
                                CustomerBranch.StateID = (from s in entities.RLCS_State_Mapping
                                                          where (s.SM_Code.Trim().ToUpper().Equals(ClientBranch.CM_State.Trim().ToUpper())
                                                        || s.SM_Name.Trim().ToUpper().Equals(ClientBranch.CM_State.Trim().ToUpper()))
                                                          select s.AVACOM_StateID).FirstOrDefault();

                            if (!String.IsNullOrEmpty(ClientBranch.CM_City))
                                CustomerBranch.CityID = (from s in entities.RLCS_Location_City_Mapping
                                                         where (s.LM_Code.Trim().ToUpper().Equals(ClientBranch.CM_City.Trim().ToUpper())
                                                         || s.LM_Name.Trim().ToUpper().Equals(ClientBranch.CM_City.Trim().ToUpper()))
                                                         select (int)s.AVACOM_CityID).FirstOrDefault();

                            CustomerBranch.AddressLine1 = ClientBranch.CM_Address;

                            CustomerBranch.IsDeleted = false;
                            entities.SaveChanges();
                            update = true;

                            if (update)
                            {
                                TinyMapper.Bind<CustomerBranch, mst_CustomerBranch>();
                                mst_CustomerBranch Custbranch1 = TinyMapper.Map<mst_CustomerBranch>(CustomerBranch);
                                Update1 = CustomerBranchManagement.UpdateRLCS(Custbranch1);
                                entities.SaveChanges();
                                Update1 = true;

                                if (Update1 && update)
                                {
                                    RLCS_CustomerBranch_ClientsLocation_Mapping BranchUpdate = (from entry in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                                                                where entry.AVACOM_BranchID == CustomerBranch.ID
                                                                                                && entry.CO_CorporateID == ClientBranch.CO_CorporateID
                                                                                                && entry.BranchType == "B"
                                                                                                select entry).FirstOrDefault();

                                    if (BranchUpdate != null)
                                    {
                                        BranchUpdate.BranchType = "B";
                                        BranchUpdate.AVACOM_BranchName = ClientBranch.AVACOM_BranchName;
                                        BranchUpdate.CO_CorporateID = ClientBranch.CO_CorporateID;
                                        BranchUpdate.CM_ClientID = ClientBranch.CM_ClientID;
                                        BranchUpdate.CM_ClientName = ClientBranch.AVACOM_BranchName;
                                        BranchUpdate.CM_EstablishmentType = ClientBranch.CM_EstablishmentType;
                                        BranchUpdate.CL_OfficeType = ClientBranch.CL_OfficeType;

                                        BranchUpdate.CM_State = ClientBranch.CM_State;
                                        BranchUpdate.CM_City = ClientBranch.CM_City;

                                        BranchUpdate.CM_Pincode = ClientBranch.CM_Pincode;
                                        BranchUpdate.CL_Pincode = ClientBranch.CL_Pincode;

                                        BranchUpdate.CM_Status = ClientBranch.CM_Status;

                                        BranchUpdate.CL_PT_State = ClientBranch.CL_PT_State;
                                        BranchUpdate.CL_PF_Code = ClientBranch.CL_PF_Code;
                                        BranchUpdate.CL_ESIC_Code = ClientBranch.CL_ESIC_Code;

                                        BranchUpdate.CM_Address = ClientBranch.CM_Address;

                                        BranchUpdate.CL_EmployerName = ClientBranch.CL_EmployerName;
                                        BranchUpdate.CL_ManagerName = ClientBranch.CL_ManagerName;
                                        BranchUpdate.CL_EmployerAddress = ClientBranch.CL_EmployerAddress;
                                        BranchUpdate.CL_ManagerAddress = ClientBranch.CL_ManagerAddress;
                                        BranchUpdate.CL_CompPhoneNo = ClientBranch.CL_CompPhoneNo;
                                        BranchUpdate.CL_HRContactPerson = ClientBranch.CL_HRContactPerson;
                                        BranchUpdate.CL_HRPhNo = ClientBranch.CL_HRPhNo;
                                        BranchUpdate.CL_HRMailID = ClientBranch.CL_HRMailID;
                                        BranchUpdate.CL_HR1stLevelMail = ClientBranch.CL_HR1stLevelMail;
                                        BranchUpdate.CL_HR1stLevelPhNo = ClientBranch.CL_HR1stLevelPhNo;
                                        BranchUpdate.CL_RCNO = ClientBranch.CL_RCNO;
                                        BranchUpdate.CL_NatureofBusiness = ClientBranch.CL_NatureofBusiness;
                                        BranchUpdate.CL_BusinessType = ClientBranch.CL_BusinessType;
                                        BranchUpdate.CL_WorkHoursFrom = ClientBranch.CL_WorkHoursFrom;
                                        BranchUpdate.CL_WorkHoursTo = ClientBranch.CL_WorkHoursTo;
                                        BranchUpdate.CL_IntervalsTo = ClientBranch.CL_IntervalsTo;
                                        BranchUpdate.CL_IntervalsFrom = ClientBranch.CL_IntervalsFrom;
                                        BranchUpdate.CL_WorkTimings = ClientBranch.CL_WorkTimings;
                                        BranchUpdate.CL_weekoffDay = ClientBranch.CL_weekoffDay;
                                        BranchUpdate.CL_LIN = ClientBranch.CL_LIN;
                                        BranchUpdate.CL_LWF_State = ClientBranch.CL_LWF_State;
                                        BranchUpdate.CL_Municipality = ClientBranch.CL_Municipality;
                                        BranchUpdate.CL_PermissionMaintainingForms = ClientBranch.CL_PermissionMaintainingForms;
                                        BranchUpdate.CL_RequirePowerforFines = ClientBranch.CL_RequirePowerforFines;
                                        BranchUpdate.CL_CommencementDate = ClientBranch.CL_CommencementDate;
                                        BranchUpdate.CL_ClassificationofEstablishment = ClientBranch.CL_ClassificationofEstablishment;
                                        BranchUpdate.CL_LicenceNo = ClientBranch.CL_LicenceNo;
                                        BranchUpdate.CL_NICCode = ClientBranch.CL_NICCode;
                                        BranchUpdate.CL_SectionofAct = ClientBranch.CL_SectionofAct;
                                        BranchUpdate.CL_District = ClientBranch.CL_District;
                                        BranchUpdate.CM_Pincode = ClientBranch.CM_Pincode;
                                        BranchUpdate.CL_Juridiction = ClientBranch.CL_Juridiction;
                                        BranchUpdate.CL_RC_ValidFrom = ClientBranch.CL_RC_ValidFrom;
                                        BranchUpdate.CL_RC_ValidTo = ClientBranch.CL_RC_ValidTo;
                                        BranchUpdate.CL_BusinessType = ClientBranch.CL_BusinessType;
                                        BranchUpdate.IsProcessed = false;
                                        BranchUpdate.UpdatedOn = DateTime.Now;
                                        entities.SaveChanges();
                                        return true;
                                    }
                                    else
                                    {
                                        RLCS_CustomerBranch_ClientsLocation_Mapping Branch = new RLCS_CustomerBranch_ClientsLocation_Mapping();
                                      
                                        Branch.BranchType = "B";
                                        Branch.AVACOM_BranchID = CustomerBranch.ID;
                                      
                                        Branch.AVACOM_BranchName = ClientBranch.AVACOM_BranchName;
                                        Branch.CM_ClientName = ClientBranch.AVACOM_BranchName;
                                        Branch.CM_EstablishmentType = ClientBranch.CM_EstablishmentType;
                                        Branch.CM_State = ClientBranch.CM_State;
                                        Branch.CM_City = ClientBranch.CM_City;

                                        Branch.CL_Pincode = ClientBranch.CL_Pincode;
                                        Branch.CM_Pincode = ClientBranch.CM_Pincode;

                                        Branch.CL_OfficeType = ClientBranch.CL_OfficeType;
                                        Branch.CL_PT_State = ClientBranch.CL_PT_State;
                                        Branch.CL_PF_Code = ClientBranch.CL_PF_Code;
                                        Branch.CL_ESIC_Code = ClientBranch.CL_ESIC_Code;

                                        Branch.AVACOM_CustomerID = ClientBranch.AVACOM_CustomerID;

                                        Branch.CM_Address = ClientBranch.CM_Address;
                                        Branch.CM_Status = ClientBranch.CM_Status;

                                        Branch.CL_EmployerName = ClientBranch.CL_EmployerName;
                                        Branch.CL_ManagerName = ClientBranch.CL_ManagerName;
                                        Branch.CL_EmployerAddress = ClientBranch.CL_EmployerAddress;
                                        Branch.CL_ManagerAddress = ClientBranch.CL_ManagerAddress;

                                        Branch.CL_HRContactPerson = ClientBranch.CL_HRContactPerson;
                                        Branch.CL_HRPhNo = ClientBranch.CL_HRPhNo;
                                        Branch.CL_HRMailID = ClientBranch.CL_HRMailID;
                                        Branch.CL_HR1stLevelMail = ClientBranch.CL_HR1stLevelMail;
                                        Branch.CL_HR1stLevelPhNo = ClientBranch.CL_HR1stLevelPhNo;
                                        Branch.CL_RCNO = ClientBranch.CL_RCNO;
                                        Branch.CL_NatureofBusiness = ClientBranch.CL_NatureofBusiness;
                                        Branch.CL_WorkHoursFrom = ClientBranch.CL_WorkHoursFrom;
                                        Branch.CL_WorkHoursTo = ClientBranch.CL_WorkHoursTo;
                                        Branch.CL_IntervalsTo = ClientBranch.CL_IntervalsTo;
                                        Branch.CL_IntervalsFrom = ClientBranch.CL_IntervalsFrom;
                                        Branch.CL_weekoffDay = ClientBranch.CL_weekoffDay;
                                        Branch.CL_WorkTimings = ClientBranch.CL_WorkTimings;
                                        Branch.CL_LIN = ClientBranch.CL_LIN;
                                        Branch.CL_LWF_State = ClientBranch.CL_LWF_State;
                                        Branch.CL_Municipality = ClientBranch.CL_Municipality;
                                        Branch.CL_PermissionMaintainingForms = ClientBranch.CL_PermissionMaintainingForms;
                                        Branch.CL_RequirePowerforFines = ClientBranch.CL_RequirePowerforFines;
                                        Branch.CL_CommencementDate = ClientBranch.CL_CommencementDate;
                                        Branch.CL_ClassificationofEstablishment = ClientBranch.CL_ClassificationofEstablishment;
                                        Branch.CL_LicenceNo = ClientBranch.CL_LicenceNo;
                                        Branch.CL_NICCode = ClientBranch.CL_NICCode;
                                        Branch.CO_CorporateID = ClientBranch.CO_CorporateID;
                                        Branch.CL_SectionofAct = ClientBranch.CL_SectionofAct;
                                        Branch.CL_District = ClientBranch.CL_District;
                                        Branch.CL_Pincode = ClientBranch.CL_Pincode;
                                        Branch.CL_Juridiction = ClientBranch.CL_Juridiction;
                                        Branch.CL_RC_ValidFrom = ClientBranch.CL_RC_ValidFrom;
                                        Branch.CL_RC_ValidTo = ClientBranch.CL_RC_ValidTo;
                                        Branch.CL_BusinessType = ClientBranch.CL_BusinessType;
                                        Branch.CreatedOn = DateTime.Now;
                                        Branch.IsProcessed = false;
                                        entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Add(Branch);
                                        entities.SaveChanges();
                                    }
                                }
                            }
                        }
                        else
                        {                            
                            CustomerBranch CustBranch = new CustomerBranch();
                            CustBranch.Type = 2;

                            if (!String.IsNullOrEmpty(ClientBranch.CM_State))
                                CustomerBranch.StateID = (from s in entities.RLCS_State_Mapping
                                                          where (s.SM_Code.Trim().ToUpper().Equals(ClientBranch.CM_State.Trim().ToUpper())
                                                        || s.SM_Name.Trim().ToUpper().Equals(ClientBranch.CM_State.Trim().ToUpper()))
                                                          select s.AVACOM_StateID).FirstOrDefault();

                            if (!String.IsNullOrEmpty(ClientBranch.CM_City))
                                CustomerBranch.CityID = (from s in entities.RLCS_Location_City_Mapping
                                                         where (s.LM_Code.Trim().ToUpper().Equals(ClientBranch.CM_City.Trim().ToUpper())
                                                         || s.LM_Name.Trim().ToUpper().Equals(ClientBranch.CM_City.Trim().ToUpper()))
                                                         select (int)s.AVACOM_CityID).FirstOrDefault();

                            CustBranch.ParentID = (from b in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                   where b.BranchType == "B"
                                                   && b.CM_ClientID == ClientBranch.CM_ClientID
                                                   select b.AVACOM_BranchID).FirstOrDefault();

                            CustBranch.AuditPR = false;
                            CustBranch.Name = ClientBranch.AVACOM_BranchName;
                            CustBranch.CustomerID = (from c in entities.RLCS_CustomerBranch_ClientsLocation_Mapping join CB in entities.CustomerBranches on c.AVACOM_BranchID equals CB.ID where c.CM_ClientID == ClientBranch.CM_ClientID select CB.CustomerID).FirstOrDefault();
                            CustBranch.CreatedOn = DateTime.Now;
                            CustBranch.IsDeleted = false;
                            CustBranch.AddressLine1 = ClientBranch.CM_Address;

                            if (ClientBranch.CM_Status == "A")
                                CustBranch.Status = 1;
                            else
                                CustBranch.Status = 0;

                            entities.CustomerBranches.Add(CustBranch);
                            entities.SaveChanges();

                            update = true;

                            if (update)
                            {
                                mst_CustomerBranch Custbranch1 = new mst_CustomerBranch();
                                TinyMapper.Bind<CustomerBranch, mst_CustomerBranch>();
                                Custbranch1 = TinyMapper.Map<mst_CustomerBranch>(CustBranch);
                                Update1 = CustomerBranchManagement.CreateRLCS(Custbranch1);

                                if (Update1 && update)
                                {
                                    RLCS_CustomerBranch_ClientsLocation_Mapping BranchUpdate = new RLCS_CustomerBranch_ClientsLocation_Mapping();
                                    BranchUpdate.BranchType = "B";
                                    BranchUpdate.AVACOM_BranchName = ClientBranch.AVACOM_BranchName;
                                    BranchUpdate.CO_CorporateID = ClientBranch.CO_CorporateID;
                                    BranchUpdate.CM_ClientID = ClientBranch.CM_ClientID;
                                    BranchUpdate.CM_ClientName = ClientBranch.AVACOM_BranchName;
                                    BranchUpdate.CM_EstablishmentType = ClientBranch.CM_EstablishmentType;
                                    BranchUpdate.CL_OfficeType = ClientBranch.CL_OfficeType;
                                    BranchUpdate.CM_State = ClientBranch.CM_State;
                                    BranchUpdate.CM_City = ClientBranch.CM_City;
                                    BranchUpdate.CM_Pincode = ClientBranch.CM_Pincode;
                                    BranchUpdate.CL_Pincode = ClientBranch.CL_Pincode;
                                    BranchUpdate.CM_Status = ClientBranch.CM_Status;
                                    BranchUpdate.CL_PT_State = ClientBranch.CL_PT_State;
                                    BranchUpdate.CL_PF_Code = ClientBranch.CL_PF_Code;
                                    BranchUpdate.CL_ESIC_Code = ClientBranch.CL_ESIC_Code;
                                    BranchUpdate.CM_Address = ClientBranch.CM_Address;
                                    BranchUpdate.CL_EmployerName = ClientBranch.CL_EmployerName;
                                    BranchUpdate.CL_ManagerName = ClientBranch.CL_ManagerName;
                                    BranchUpdate.CL_EmployerAddress = ClientBranch.CL_EmployerAddress;
                                    BranchUpdate.CL_ManagerAddress = ClientBranch.CL_ManagerAddress;
                                    BranchUpdate.CL_CompPhoneNo = ClientBranch.CL_CompPhoneNo;
                                    BranchUpdate.CL_HRContactPerson = ClientBranch.CL_HRContactPerson;
                                    BranchUpdate.CL_HRPhNo = ClientBranch.CL_HRPhNo;
                                    BranchUpdate.CL_HRMailID = ClientBranch.CL_HRMailID;
                                    BranchUpdate.CL_HR1stLevelMail = ClientBranch.CL_HR1stLevelMail;
                                    BranchUpdate.CL_HR1stLevelPhNo = ClientBranch.CL_HR1stLevelPhNo;
                                    BranchUpdate.CL_RCNO = ClientBranch.CL_RCNO;
                                    BranchUpdate.CL_NatureofBusiness = ClientBranch.CL_NatureofBusiness;
                                    BranchUpdate.CL_BusinessType = ClientBranch.CL_BusinessType;
                                    BranchUpdate.CL_WorkHoursFrom = ClientBranch.CL_WorkHoursFrom;
                                    BranchUpdate.CL_WorkHoursTo = ClientBranch.CL_WorkHoursTo;
                                    BranchUpdate.CL_IntervalsTo = ClientBranch.CL_IntervalsTo;
                                    BranchUpdate.CL_IntervalsFrom = ClientBranch.CL_IntervalsFrom;
                                    BranchUpdate.CL_WorkTimings = ClientBranch.CL_WorkTimings;
                                    BranchUpdate.CL_weekoffDay = ClientBranch.CL_weekoffDay;
                                    BranchUpdate.CL_LIN = ClientBranch.CL_LIN;
                                    BranchUpdate.CL_LWF_State = ClientBranch.CL_LWF_State;
                                    BranchUpdate.CL_Municipality = ClientBranch.CL_Municipality;
                                    BranchUpdate.CL_PermissionMaintainingForms = ClientBranch.CL_PermissionMaintainingForms;
                                    BranchUpdate.CL_RequirePowerforFines = ClientBranch.CL_RequirePowerforFines;
                                    BranchUpdate.CL_CommencementDate = ClientBranch.CL_CommencementDate;
                                    BranchUpdate.CL_ClassificationofEstablishment = ClientBranch.CL_ClassificationofEstablishment;
                                    BranchUpdate.CL_LicenceNo = ClientBranch.CL_LicenceNo;
                                    BranchUpdate.CL_NICCode = ClientBranch.CL_NICCode;
                                    BranchUpdate.CL_SectionofAct = ClientBranch.CL_SectionofAct;
                                    BranchUpdate.CL_District = ClientBranch.CL_District;                                    
                                    BranchUpdate.CL_Juridiction = ClientBranch.CL_Juridiction;
                                    BranchUpdate.CL_RC_ValidFrom = ClientBranch.CL_RC_ValidFrom;
                                    BranchUpdate.CL_RC_ValidTo = ClientBranch.CL_RC_ValidTo;
                                    BranchUpdate.CL_BusinessType = ClientBranch.CL_BusinessType;

                                    BranchUpdate.IsProcessed = false;
                                    BranchUpdate.UpdatedOn = DateTime.Now;

                                    entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Add(BranchUpdate);
                                    entities.SaveChanges();
                                    //update1 = true;
                                }
                                else
                                {
                                    RLCS_ClientsManagement.DeleteCustomerBranch_Compliance(CustBranch.ID);
                                    Update1 = false;
                                }
                            }
                        }

                        return update;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //entities.Connection.Close();
                    return false;
                }
            }
        }

        public static bool CheckDuplicateBranch(string BranchName, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool check = false;
                try
                {
                        CustomerBranch CustomerBranch = (from row in entities.CustomerBranches
                                                         where row.Name == BranchName
                                                         && row.CustomerID == CustomerID
                                                         && row.IsDeleted == false
                                                         select row).FirstOrDefault();
                        if (CustomerBranch != null)
                            check = true;
                        else
                            check = false;
                    
                    return check;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //entities.Connection.Close();
                    return false;
                }
            }

        }
        public static bool CreateUpdate_RLCS_ClientLocationDetails(RLCS_CustomerBranch_ClientsLocation_Mapping ClientBranch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    if (!string.IsNullOrEmpty(ClientBranch.AVACOM_BranchName))
                    {
                        bool createUpdateSuccess_ComplianceDB = false;
                        bool createUpdateSuccess_AuditDB = false;

                        CustomerBranch CustomerBranch = (from row in entities.CustomerBranches
                                                         where row.Name == ClientBranch.AVACOM_BranchName
                                                         && row.CustomerID == ClientBranch.AVACOM_CustomerID
                                                         && row.IsDeleted == false
                                                         select row).FirstOrDefault();

                        RLCS_CustomerBranch_ClientsLocation_Mapping rlcsCustomerBranch = (from entry in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                                                          where entry.AVACOM_BranchName == ClientBranch.AVACOM_BranchName
                                                                                          && entry.CO_CorporateID == ClientBranch.CO_CorporateID && entry.CM_ClientID==ClientBranch.CM_ClientID
                                                                                          && entry.BranchType == "B"
                                                                                          select entry).FirstOrDefault();

                        if (CustomerBranch != null && rlcsCustomerBranch != null)
                        {
                            if (!String.IsNullOrEmpty(ClientBranch.CM_State))
                                CustomerBranch.StateID = (from s in entities.RLCS_State_Mapping
                                                          where (s.SM_Code.Trim().ToUpper().Equals(ClientBranch.CM_State.Trim().ToUpper())
                                                        || s.SM_Name.Trim().ToUpper().Equals(ClientBranch.CM_State.Trim().ToUpper()))
                                                          select s.AVACOM_StateID).FirstOrDefault();

                            if (!String.IsNullOrEmpty(ClientBranch.CM_City))
                                CustomerBranch.CityID = (from s in entities.RLCS_Location_City_Mapping
                                                         where (s.LM_Code.Trim().ToUpper().Equals(ClientBranch.CM_City.Trim().ToUpper())
                                                         || s.LM_Name.Trim().ToUpper().Equals(ClientBranch.CM_City.Trim().ToUpper()))
                                                         select (int)s.AVACOM_CityID).FirstOrDefault();

                            CustomerBranch.AddressLine1 = ClientBranch.CM_Address;
                            CustomerBranch.EmailID = ClientBranch.CL_HRMailID;
                            CustomerBranch.ContactPerson = ClientBranch.CL_HRContactPerson;
                            if (ClientBranch.CM_Status == "A")
                                CustomerBranch.Status = 1;
                            else
                                CustomerBranch.Status = 0;

                            CustomerBranch.IsDeleted = false;
                            entities.SaveChanges();

                            createUpdateSuccess_ComplianceDB = true;

                            if (createUpdateSuccess_ComplianceDB)
                            {
                                TinyMapper.Bind<CustomerBranch, mst_CustomerBranch>();
                                mst_CustomerBranch custBranch_Audit = TinyMapper.Map<mst_CustomerBranch>(CustomerBranch);
                                createUpdateSuccess_AuditDB = CustomerBranchManagement.UpdateRLCS(custBranch_Audit);
                                entities.SaveChanges();
                                createUpdateSuccess_AuditDB = true;

                                if (createUpdateSuccess_ComplianceDB && createUpdateSuccess_AuditDB)
                                {
                                    RLCS_CustomerBranch_ClientsLocation_Mapping PrevBranchRecord = (from entry in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                                                                    where entry.AVACOM_BranchID == CustomerBranch.ID
                                                                                                    && entry.CO_CorporateID == ClientBranch.CO_CorporateID
                                                                                                    && entry.BranchType == "B"
                                                                                                    select entry).FirstOrDefault();

                                    if (PrevBranchRecord != null)
                                    {
                                        PrevBranchRecord.AVACOM_BranchName = ClientBranch.AVACOM_BranchName;
                                        PrevBranchRecord.BranchType = "B";
                                        PrevBranchRecord.CO_CorporateID = ClientBranch.CO_CorporateID;
                                        PrevBranchRecord.CM_ClientID = ClientBranch.CM_ClientID;
                                        PrevBranchRecord.CM_ClientName = ClientBranch.AVACOM_BranchName;

                                        PrevBranchRecord.CM_EstablishmentType = ClientBranch.CM_EstablishmentType;
                                        PrevBranchRecord.CL_OfficeType = ClientBranch.CL_OfficeType;

                                        PrevBranchRecord.CM_State = ClientBranch.CM_State;
                                        PrevBranchRecord.CM_City = ClientBranch.CM_City;

                                        PrevBranchRecord.CM_Pincode = ClientBranch.CM_Pincode;
                                        PrevBranchRecord.CL_Pincode = ClientBranch.CL_Pincode;

                                        PrevBranchRecord.CM_Status = ClientBranch.CM_Status;

                                        PrevBranchRecord.CL_PT_State = ClientBranch.CL_PT_State;
                                        PrevBranchRecord.CL_PF_Code = ClientBranch.CL_PF_Code;
                                        PrevBranchRecord.CL_ESIC_Code = ClientBranch.CL_ESIC_Code;

                                        PrevBranchRecord.AVACOM_CustomerID = ClientBranch.AVACOM_CustomerID;

                                        PrevBranchRecord.CM_Address = ClientBranch.CM_Address;
                                        PrevBranchRecord.CL_EmployerName = ClientBranch.CL_EmployerName;
                                        PrevBranchRecord.CL_EmployerAddress = ClientBranch.CL_EmployerAddress;
                                        PrevBranchRecord.CL_ManagerName = ClientBranch.CL_ManagerName;
                                        PrevBranchRecord.CL_ManagerAddress = ClientBranch.CL_ManagerAddress;
                                        PrevBranchRecord.CL_CompPhoneNo = ClientBranch.CL_CompPhoneNo;
                                        PrevBranchRecord.CL_HRContactPerson = ClientBranch.CL_HRContactPerson;
                                        PrevBranchRecord.CL_HRPhNo = ClientBranch.CL_HRPhNo;
                                        PrevBranchRecord.CL_HRMailID = ClientBranch.CL_HRMailID;
                                        PrevBranchRecord.CL_HR1stLevelMail = ClientBranch.CL_HR1stLevelMail;
                                        PrevBranchRecord.CL_HR1stLevelPhNo = ClientBranch.CL_HR1stLevelPhNo;

                                        PrevBranchRecord.CL_RCNO = ClientBranch.CL_RCNO;
                                        PrevBranchRecord.CL_RC_ValidFrom = ClientBranch.CL_RC_ValidFrom;
                                        PrevBranchRecord.CL_RC_ValidTo = ClientBranch.CL_RC_ValidTo;

                                        PrevBranchRecord.CL_NatureofBusiness = ClientBranch.CL_NatureofBusiness;
                                        PrevBranchRecord.CL_WorkHoursFrom = ClientBranch.CL_WorkHoursFrom;
                                        PrevBranchRecord.CL_WorkHoursTo = ClientBranch.CL_WorkHoursTo;
                                        PrevBranchRecord.CL_IntervalsFrom = ClientBranch.CL_IntervalsFrom;
                                        PrevBranchRecord.CL_IntervalsTo = ClientBranch.CL_IntervalsTo;
                                        PrevBranchRecord.CL_weekoffDay = ClientBranch.CL_weekoffDay;
                                        PrevBranchRecord.CL_WorkTimings = ClientBranch.CL_WorkTimings;
                                        PrevBranchRecord.CL_LIN = ClientBranch.CL_LIN;
                                        PrevBranchRecord.CL_LWF_State = ClientBranch.CL_LWF_State;
                                        PrevBranchRecord.CL_Municipality = ClientBranch.CL_Municipality;
                                        PrevBranchRecord.CL_PermissionMaintainingForms = ClientBranch.CL_PermissionMaintainingForms;
                                        PrevBranchRecord.CL_RequirePowerforFines = ClientBranch.CL_RequirePowerforFines;
                                        PrevBranchRecord.CL_CommencementDate = ClientBranch.CL_CommencementDate;
                                        PrevBranchRecord.CL_ClassificationofEstablishment = ClientBranch.CL_ClassificationofEstablishment;
                                        PrevBranchRecord.CL_LicenceNo = ClientBranch.CL_LicenceNo;
                                        PrevBranchRecord.CL_NICCode = ClientBranch.CL_NICCode;
                                        PrevBranchRecord.CL_SectionofAct = ClientBranch.CL_SectionofAct;
                                        PrevBranchRecord.CL_District = ClientBranch.CL_District;
                                        PrevBranchRecord.CL_Juridiction = ClientBranch.CL_Juridiction;

                                        PrevBranchRecord.CL_BusinessType = ClientBranch.CL_BusinessType;
                                        PrevBranchRecord.CM_ActType = ClientBranch.CM_ActType;

                                        PrevBranchRecord.IsProcessed = false;
                                        PrevBranchRecord.UpdatedOn = DateTime.Now;

                                        if (string.IsNullOrEmpty(Convert.ToString(ClientBranch.NoofEmployees)))
                                        {
                                            ClientBranch.NoofEmployees = 0;
                                        }
                                        PrevBranchRecord.NoofEmployees = (int)ClientBranch.NoofEmployees;
                                        PrevBranchRecord.CM_IsAventisClientOrBranch = ClientBranch.CM_IsAventisClientOrBranch;
                                        entities.SaveChanges();
                                        return true;
                                    }
                                    else
                                    {
                                        RLCS_CustomerBranch_ClientsLocation_Mapping newRLCS_Branch = new RLCS_CustomerBranch_ClientsLocation_Mapping();

                                        newRLCS_Branch.AVACOM_BranchID = CustomerBranch.ID;
                                        newRLCS_Branch.AVACOM_BranchName = ClientBranch.AVACOM_BranchName;
                                        newRLCS_Branch.BranchType = "B";
                                        newRLCS_Branch.CO_CorporateID = ClientBranch.CO_CorporateID;
                                        newRLCS_Branch.CM_ClientID = ClientBranch.CM_ClientID;
                                        newRLCS_Branch.CM_ClientName = ClientBranch.AVACOM_BranchName;
                                        newRLCS_Branch.CM_EstablishmentType = ClientBranch.CM_EstablishmentType;
                                        newRLCS_Branch.CL_OfficeType = ClientBranch.CL_OfficeType;
                                       
                                        newRLCS_Branch.CM_State = ClientBranch.CM_State;
                                        newRLCS_Branch.CM_City = ClientBranch.CM_City;
                                   
                                        newRLCS_Branch.CM_Pincode = ClientBranch.CM_Pincode;
                                        newRLCS_Branch.CL_Pincode = ClientBranch.CL_Pincode;
                                   
                                        newRLCS_Branch.CM_Status = ClientBranch.CM_Status;
                                       
                                        newRLCS_Branch.CL_PT_State = ClientBranch.CL_PT_State;
                                        newRLCS_Branch.CL_PF_Code = ClientBranch.CL_PF_Code;
                                        newRLCS_Branch.CL_ESIC_Code = ClientBranch.CL_ESIC_Code;
                                   
                                        newRLCS_Branch.AVACOM_CustomerID = ClientBranch.AVACOM_CustomerID;
                                       
                                        newRLCS_Branch.CM_Address = ClientBranch.CM_Address;
                                        newRLCS_Branch.CL_EmployerName = ClientBranch.CL_EmployerName;
                                        newRLCS_Branch.CL_ManagerName = ClientBranch.CL_ManagerName;
                                        newRLCS_Branch.CL_EmployerAddress = ClientBranch.CL_EmployerAddress;
                                        newRLCS_Branch.CL_ManagerAddress = ClientBranch.CL_ManagerAddress;
                                        newRLCS_Branch.CL_CompPhoneNo = ClientBranch.CL_CompPhoneNo;
                                        newRLCS_Branch.CL_HRContactPerson = ClientBranch.CL_HRContactPerson;
                                        newRLCS_Branch.CL_HRPhNo = ClientBranch.CL_HRPhNo;
                                        newRLCS_Branch.CL_HRMailID = ClientBranch.CL_HRMailID;
                                        newRLCS_Branch.CL_HR1stLevelMail = ClientBranch.CL_HR1stLevelMail;
                                        newRLCS_Branch.CL_HR1stLevelPhNo = ClientBranch.CL_HR1stLevelPhNo;
                               
                                        newRLCS_Branch.CL_RCNO = ClientBranch.CL_RCNO;
                                        newRLCS_Branch.CL_RC_ValidFrom = ClientBranch.CL_RC_ValidFrom;
                                        newRLCS_Branch.CL_RC_ValidTo = ClientBranch.CL_RC_ValidTo;
                                    
                                        newRLCS_Branch.CL_NatureofBusiness = ClientBranch.CL_NatureofBusiness;
                                       
                                        newRLCS_Branch.CL_WorkHoursFrom = ClientBranch.CL_WorkHoursFrom;
                                        newRLCS_Branch.CL_WorkHoursTo = ClientBranch.CL_WorkHoursTo;
                                        newRLCS_Branch.CL_IntervalsFrom = ClientBranch.CL_IntervalsFrom;
                                        newRLCS_Branch.CL_IntervalsTo = ClientBranch.CL_IntervalsTo;
                                        newRLCS_Branch.CL_weekoffDay = ClientBranch.CL_weekoffDay;
                                        newRLCS_Branch.CL_WorkTimings = ClientBranch.CL_WorkTimings;
                                        newRLCS_Branch.CL_LIN = ClientBranch.CL_LIN;
                                        newRLCS_Branch.CL_LWF_State = ClientBranch.CL_LWF_State;
                                        newRLCS_Branch.CL_Municipality = ClientBranch.CL_Municipality;
                                        newRLCS_Branch.CL_PermissionMaintainingForms = ClientBranch.CL_PermissionMaintainingForms;
                                        newRLCS_Branch.CL_RequirePowerforFines = ClientBranch.CL_RequirePowerforFines;
                                        newRLCS_Branch.CL_CommencementDate = ClientBranch.CL_CommencementDate;
                                        newRLCS_Branch.CL_ClassificationofEstablishment = ClientBranch.CL_ClassificationofEstablishment;
                                        newRLCS_Branch.CL_LicenceNo = ClientBranch.CL_LicenceNo;
                                        newRLCS_Branch.CL_NICCode = ClientBranch.CL_NICCode;
                                        newRLCS_Branch.CL_SectionofAct = ClientBranch.CL_SectionofAct;
                                        newRLCS_Branch.CL_District = ClientBranch.CL_District;
                                        newRLCS_Branch.CL_Juridiction = ClientBranch.CL_Juridiction;
                                        newRLCS_Branch.CM_BonusPercentage = ClientBranch.CM_BonusPercentage;
                                        newRLCS_Branch.CM_Excemption = ClientBranch.CM_Excemption;
                                        newRLCS_Branch.CL_BusinessType = ClientBranch.CL_BusinessType;
                                        newRLCS_Branch.CM_ActType = ClientBranch.CM_ActType;
                                        
                                        newRLCS_Branch.CreatedOn = DateTime.Now;
                                        newRLCS_Branch.IsProcessed = false;

                                        if (string.IsNullOrEmpty(Convert.ToString(ClientBranch.NoofEmployees)))
                                        {
                                            ClientBranch.NoofEmployees = 0;
                                        }
                                        PrevBranchRecord.NoofEmployees = (int)ClientBranch.NoofEmployees;
                                        PrevBranchRecord.CM_IsAventisClientOrBranch = ClientBranch.CM_IsAventisClientOrBranch;
                                        entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Add(newRLCS_Branch);
                                        entities.SaveChanges();
                                    }
                                }
                            }
                        }
                        else
                        {                            
                            CustomerBranch CustBranch = new CustomerBranch();
                            CustBranch.Type = 2;

                            if (!String.IsNullOrEmpty(ClientBranch.CM_State))
                                CustBranch.StateID = (from s in entities.RLCS_State_Mapping
                                                      where (s.SM_Code.Trim().ToUpper().Equals(ClientBranch.CM_State.Trim().ToUpper())
                                                    || s.SM_Name.Trim().ToUpper().Equals(ClientBranch.CM_State.Trim().ToUpper()))
                                                      select s.AVACOM_StateID).FirstOrDefault();

                            if (!String.IsNullOrEmpty(ClientBranch.CM_City))
                                CustBranch.CityID = (from s in entities.RLCS_Location_City_Mapping
                                                     where (s.LM_Code.Trim().ToUpper().Equals(ClientBranch.CM_City.Trim().ToUpper())
                                                     || s.LM_Name.Trim().ToUpper().Equals(ClientBranch.CM_City.Trim().ToUpper()))
                                                     select (int)s.AVACOM_CityID).FirstOrDefault();

                            var Parent = (from b in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                          where b.BranchType == "E"
                                          && b.CM_ClientID == ClientBranch.CM_ClientID
                                          && b.AVACOM_CustomerID == ClientBranch.AVACOM_CustomerID
                                          select b).FirstOrDefault();
                            if (Parent != null)
                            {
                                CustBranch.ParentID = Parent.AVACOM_BranchID;
                                //CustBranch.CustomerID = (int)Parent.AVACOM_CustomerID;
                            }
                            CustBranch.AuditPR = false;
                            CustBranch.Name = ClientBranch.AVACOM_BranchName;
                            CustBranch.CustomerID = (int)ClientBranch.AVACOM_CustomerID;
                            CustBranch.AddressLine1 = ClientBranch.CM_Address;
                            CustBranch.EmailID = ClientBranch.CL_HRMailID;
                            CustBranch.ContactPerson = ClientBranch.CL_HRContactPerson;
                            CustBranch.CreatedOn = DateTime.Now;
                            CustBranch.IsDeleted = false;                           

                            if (ClientBranch.CM_Status == "A")
                                CustBranch.Status = 1;
                            else
                                CustBranch.Status = 0;

                            entities.CustomerBranches.Add(CustBranch);
                            entities.SaveChanges();

                            createUpdateSuccess_ComplianceDB = true;

                            if (createUpdateSuccess_ComplianceDB)
                            {
                                mst_CustomerBranch Custbranch1 = new mst_CustomerBranch();
                                TinyMapper.Bind<CustomerBranch, mst_CustomerBranch>();
                                Custbranch1 = TinyMapper.Map<mst_CustomerBranch>(CustBranch);

                                createUpdateSuccess_AuditDB = CustomerBranchManagement.CreateRLCS(Custbranch1);

                                if (createUpdateSuccess_ComplianceDB && createUpdateSuccess_AuditDB)
                                {
                                    RLCS_CustomerBranch_ClientsLocation_Mapping BranchUpdate = new RLCS_CustomerBranch_ClientsLocation_Mapping();

                                    BranchUpdate.AVACOM_CustomerID = ClientBranch.AVACOM_CustomerID;
                                    BranchUpdate.AVACOM_BranchID = CustBranch.ID;
                                    BranchUpdate.BranchType = "B";
                                    BranchUpdate.AVACOM_BranchName = ClientBranch.AVACOM_BranchName;
                                    BranchUpdate.CO_CorporateID = ClientBranch.CO_CorporateID;
                                    BranchUpdate.CM_ClientID = ClientBranch.CM_ClientID;
                                    BranchUpdate.CM_ClientName = ClientBranch.AVACOM_BranchName;
                                    BranchUpdate.CM_EstablishmentType = ClientBranch.CM_EstablishmentType;
                                    BranchUpdate.CL_OfficeType = ClientBranch.CL_OfficeType;
                                    BranchUpdate.CM_State = ClientBranch.CM_State;
                                    BranchUpdate.CM_City = ClientBranch.CM_City;

                                    BranchUpdate.CL_Pincode = ClientBranch.CL_Pincode;
                                    BranchUpdate.CM_Pincode = ClientBranch.CM_Pincode;

                                    BranchUpdate.CM_Status = ClientBranch.CM_Status;
                                    
                                    BranchUpdate.CL_PT_State = ClientBranch.CL_PT_State;
                                    BranchUpdate.CL_PF_Code = ClientBranch.CL_PF_Code;
                                    BranchUpdate.CL_ESIC_Code = ClientBranch.CL_ESIC_Code;
                                    BranchUpdate.CM_Address = ClientBranch.CM_Address;
                                    BranchUpdate.CL_EmployerName = ClientBranch.CL_EmployerName;
                                    BranchUpdate.CL_ManagerName = ClientBranch.CL_ManagerName;
                                    BranchUpdate.CL_EmployerAddress = ClientBranch.CL_EmployerAddress;
                                    BranchUpdate.CL_ManagerAddress = ClientBranch.CL_ManagerAddress;
                                    BranchUpdate.CL_CompPhoneNo = ClientBranch.CL_CompPhoneNo;
                                    BranchUpdate.CL_HRContactPerson = ClientBranch.CL_HRContactPerson;
                                    BranchUpdate.CL_HRPhNo = ClientBranch.CL_HRPhNo;
                                    BranchUpdate.CL_HRMailID = ClientBranch.CL_HRMailID;
                                    BranchUpdate.CL_HR1stLevelMail = ClientBranch.CL_HR1stLevelMail;
                                    BranchUpdate.CL_HR1stLevelPhNo = ClientBranch.CL_HR1stLevelPhNo;
                                    BranchUpdate.CL_NatureofBusiness = ClientBranch.CL_NatureofBusiness;
                                    BranchUpdate.CL_RCNO = ClientBranch.CL_RCNO;
                                    BranchUpdate.CL_RC_ValidFrom = ClientBranch.CL_RC_ValidFrom;
                                    BranchUpdate.CL_RC_ValidTo = ClientBranch.CL_RC_ValidTo;
                                    BranchUpdate.CL_WorkHoursFrom = ClientBranch.CL_WorkHoursFrom;
                                    BranchUpdate.CL_WorkHoursTo = ClientBranch.CL_WorkHoursTo;
                                    BranchUpdate.CL_IntervalsTo = ClientBranch.CL_IntervalsTo;
                                    BranchUpdate.CL_IntervalsFrom = ClientBranch.CL_IntervalsFrom;
                                    BranchUpdate.CL_weekoffDay = ClientBranch.CL_weekoffDay;
                                    BranchUpdate.CL_WorkTimings = ClientBranch.CL_WorkTimings;
                                    BranchUpdate.CL_BusinessType = ClientBranch.CL_BusinessType;
                                    BranchUpdate.CL_LIN = ClientBranch.CL_LIN;
                                    BranchUpdate.CL_LWF_State = ClientBranch.CL_LWF_State;
                                    BranchUpdate.CL_Municipality = ClientBranch.CL_Municipality;
                                    BranchUpdate.CL_PermissionMaintainingForms = ClientBranch.CL_PermissionMaintainingForms;
                                    BranchUpdate.CL_RequirePowerforFines = ClientBranch.CL_RequirePowerforFines;
                                    BranchUpdate.CL_CommencementDate = ClientBranch.CL_CommencementDate;
                                    BranchUpdate.CL_ClassificationofEstablishment = ClientBranch.CL_ClassificationofEstablishment;
                                    BranchUpdate.CL_LicenceNo = ClientBranch.CL_LicenceNo;
                                    BranchUpdate.CL_NICCode = ClientBranch.CL_NICCode;
                                    BranchUpdate.CL_SectionofAct = ClientBranch.CL_SectionofAct;
                                    BranchUpdate.CL_District = ClientBranch.CL_District;
                                    BranchUpdate.CL_BusinessType = ClientBranch.CL_BusinessType;
                                    BranchUpdate.CL_Juridiction = ClientBranch.CL_Juridiction;
                                    BranchUpdate.CM_BonusPercentage = ClientBranch.CM_BonusPercentage;
                                    BranchUpdate.CM_ActType = ClientBranch.CM_ActType;

                                    BranchUpdate.CreatedOn = DateTime.Now;
                                    BranchUpdate.IsProcessed = false;

                                    if (string.IsNullOrEmpty(Convert.ToString(ClientBranch.NoofEmployees)))
                                    {
                                        ClientBranch.NoofEmployees = 0;
                                    }
                                    BranchUpdate.NoofEmployees = (int)ClientBranch.NoofEmployees;
                                    BranchUpdate.CM_IsAventisClientOrBranch = ClientBranch.CM_IsAventisClientOrBranch;
                                    BranchUpdate.CL_BranchCode = ClientBranch.CL_BranchCode;
                                    entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Add(BranchUpdate);
                                    entities.SaveChanges();                      
                                }
                                else
                                {
                                    RLCS_ClientsManagement.DeleteCustomerBranch_Compliance(CustBranch.ID);
                                    createUpdateSuccess_ComplianceDB = false;
                                }
                            }
                        }

                        return createUpdateSuccess_ComplianceDB;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //entities.Connection.Close();
                    return false;
                }
            }
        }

        public static int GetBranchCount()
        {
            int branchCnt = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {            
                    branchCnt = entities.CustomerBranches.Where(t => t.Type == 2).Count(); 
                    return branchCnt;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return branchCnt;
            }
        }

        public static int GetBranchCountByClient(string ClientID, string EstablishmentType)
        {
            int branchCnt = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    branchCnt = entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Where(t => t.BranchType == "B" && t.CM_ClientID == ClientID && t.CM_EstablishmentType == EstablishmentType).Count();
                    return branchCnt;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return branchCnt;
            }
        }

        public static bool UpdateClientBasicInfo(RLCS_Client_BasicDetails RLCS_Client_BasicDetails)
        {
            bool saveSuccess = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    RLCS_Client_BasicDetails Client = (from row in entities.RLCS_Client_BasicDetails
                                                       where row.AVACOM_BranchID == RLCS_Client_BasicDetails.AVACOM_BranchID
                                                       && row.CB_ClientID == RLCS_Client_BasicDetails.CB_ClientID
                                                       select row).FirstOrDefault();
                    if (Client != null)
                    {
                        Client.AVACOM_BranchID = RLCS_Client_BasicDetails.AVACOM_BranchID;

                        Client.CB_ClientID = RLCS_Client_BasicDetails.CB_ClientID;
                        Client.CB_PaymentDate = RLCS_Client_BasicDetails.CB_PaymentDate;

                        Client.CB_WagePeriodFrom = RLCS_Client_BasicDetails.CB_WagePeriodFrom;
                        Client.CB_WagePeriodTo = RLCS_Client_BasicDetails.CB_WagePeriodTo;
                        Client.CB_DateOfCommencement = RLCS_Client_BasicDetails.CB_DateOfCommencement;
                        Client.CB_ServiceTaxExmpted = RLCS_Client_BasicDetails.CB_ServiceTaxExmpted;                       
                        //Client.CB_EDLIExemption = RLCS_Client_BasicDetails.CB_EDLIExemption;                       
                        Client.CB_ActType = RLCS_Client_BasicDetails.CB_ActType;
                        Client.CB_PF_Code = RLCS_Client_BasicDetails.CB_PF_Code;
                        Client.CM_PFCode = RLCS_Client_BasicDetails.CM_PFCode;

                        Client.IsProcessed = false;

                        Client.CB_ModifiedBy = "Avantis";
                        Client.CB_ModifiedDate = DateTime.Now;

                        entities.SaveChanges();
                        saveSuccess = true;
                    }
                    else
                    {
                        if (RLCS_Client_BasicDetails.AVACOM_BranchID == 0)
                        {
                            RLCS_Client_BasicDetails.AVACOM_BranchID = (from s in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                                        where s.CM_ClientID == RLCS_Client_BasicDetails.CB_ClientID
                                                                        && s.BranchType == "E"
                                                                        select (int)s.AVACOM_BranchID).FirstOrDefault();
                        }

                        if (RLCS_Client_BasicDetails.AVACOM_BranchID != 0)
                        {
                            RLCS_Client_BasicDetails.CB_PaymentDate = RLCS_Client_BasicDetails.CB_PaymentDate;

                            RLCS_Client_BasicDetails.CB_WagePeriodFrom = RLCS_Client_BasicDetails.CB_WagePeriodFrom;
                            RLCS_Client_BasicDetails.CB_WagePeriodTo = RLCS_Client_BasicDetails.CB_WagePeriodTo;
                            RLCS_Client_BasicDetails.CB_ServiceTaxExmpted = RLCS_Client_BasicDetails.CB_ServiceTaxExmpted;
                            
                            //RLCS_Client_BasicDetails.CB_EDLIExemption = RLCS_Client_BasicDetails.CB_EDLIExemption;
                            RLCS_Client_BasicDetails.CB_DateOfCommencement = RLCS_Client_BasicDetails.CB_DateOfCommencement;

                            RLCS_Client_BasicDetails.CB_CreatedBy = "Avantis";
                            RLCS_Client_BasicDetails.CB_CreatedDate = DateTime.Now;

                            RLCS_Client_BasicDetails.CB_PF_Code = RLCS_Client_BasicDetails.CB_PF_Code;
                            RLCS_Client_BasicDetails.CM_PFCode = RLCS_Client_BasicDetails.CM_PFCode;
                            RLCS_Client_BasicDetails.CB_ActType = RLCS_Client_BasicDetails.CB_ActType;

                            RLCS_Client_BasicDetails.IsProcessed = false;

                            entities.RLCS_Client_BasicDetails.Add(RLCS_Client_BasicDetails);
                            entities.SaveChanges();

                            saveSuccess = true;
                        }
                    }

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //entities.Connection.Close();
                    return saveSuccess;
                }
            }
        }
        
        public static String GetCityIDByCode(string city)
        {
            string City = "";
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    City = Convert.ToString((from s in entities.RLCS_Location_City_Mapping where s.LM_Code == city select s.AVACOM_CityID).FirstOrDefault());
                    //   Client.CM_City = 
                    return City;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static RLCS_CustomerBranch_ClientsLocation_Mapping GetClientInfoByID(int branchID, string branchType)
        {
            RLCS_CustomerBranch_ClientsLocation_Mapping Client = new RLCS_CustomerBranch_ClientsLocation_Mapping();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Client = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                              where row.AVACOM_BranchID == branchID
                              && row.BranchType.Equals(branchType)
                              select row).FirstOrDefault();
                    //   Client.CM_City = Convert.ToString((from s in entities.RLCS_Location_City_Mapping where s.LM_Code == Client.CM_City select s.AVACOM_CityID).FirstOrDefault());
                    return Client;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static RLCS_Client_BasicDetails GetClientBasicByID(int ID, string clientID)
        {
            RLCS_Client_BasicDetails Client = new RLCS_Client_BasicDetails();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Client = (from row in entities.RLCS_Client_BasicDetails
                              where row.AVACOM_BranchID == ID && row.CB_ClientID == clientID
                              select row).FirstOrDefault();

                    return Client;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<City> GetCities(string stateid)
        {
            int stateID = Convert.ToInt32(stateid);
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var CityList = (from c in entities.Cities
                                    where (c.StateId == stateID)
                                    select c).Distinct().ToList();

                    return CityList.Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        // get all locations using state code
        public static List<RLCS_Location_City_Mapping> GetLocationCitiesByStateCode(string stateCode)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var LocationsList = (from RLCM in entities.RLCS_Location_City_Mapping
                                         join RSM in entities.RLCS_State_Mapping
                                         on RLCM.SM_Code equals RSM.SM_Code
                                         where (RLCM.SM_Code == stateCode)
                                         && RSM.SM_Status == "A"
                                         && RLCM.LM_Status == "A"
                                         select RLCM).Distinct().ToList();

                    return LocationsList.Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<RLCS_Location_City_Mapping> GetLocationCitiesByStateID(int stateID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var LocationsList = (from RLCM in entities.RLCS_Location_City_Mapping
                                         join RSM in entities.RLCS_State_Mapping
                                         on RLCM.SM_Code equals RSM.SM_Code
                                         where (RSM.AVACOM_StateID == stateID)
                                         && RSM.SM_Status == "A"
                                         && RLCM.LM_Status == "A"
                                         select RLCM).Distinct().ToList();

                    return LocationsList.Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        
        public static List<RLCS_Location_City_Mapping> GetAllLocations()
        {

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var LocationsList = (from c in entities.RLCS_Location_City_Mapping select c).Distinct().ToList();

                    return LocationsList.Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<RLCS_State_Mapping> GetAllStates()
        {
            List<RLCS_State_Mapping> States = new List<RLCS_State_Mapping>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var StateList = (from c in entities.RLCS_State_Mapping select c).Distinct().ToList();

                    return StateList;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<RLCS_State_Mapping> GetAllPTStates()
        {
            List<RLCS_State_Mapping> States = new List<RLCS_State_Mapping>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var StateList = (from c in entities.RLCS_State_Mapping where c.PT_Applicability == "Y" select c).Distinct().ToList();

                    return StateList;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static bool GetStatebyCode(string stateCodeORName)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool Present = false;
                    Present = (from c in entities.RLCS_State_Mapping select c).Any(x => x.SM_Code == stateCodeORName || x.SM_Name == stateCodeORName);

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }

        public static RLCS_State_Mapping GetStateRecordbyCode(string stateCodeORName)
        {
            RLCS_State_Mapping RLCS_State = null;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    return (from c in entities.RLCS_State_Mapping
                            where c.SM_Code.Trim().ToUpper() == stateCodeORName || c.SM_Name.Trim().ToUpper() == stateCodeORName
                            && c.SM_Status == "A"
                            select c).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return RLCS_State;
            }
        }

        public static RLCS_Location_City_Mapping GetCity(string stateCode,string CityCodeORName)
        {
            RLCS_Location_City_Mapping RLCS_City = null;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    return (from c in entities.RLCS_Location_City_Mapping
                            where (c.LM_Code.Trim().ToUpper() == CityCodeORName || c.LM_Name.Trim().ToUpper() == CityCodeORName)
                            && c.SM_Code.Trim().ToUpper() == stateCode
                            && c.LM_Status == "A"
                            select c).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return RLCS_City;
            }
        }

        public static RLCS_State_Mapping GetPTStateRecordbyCode(string stateCodeORName)
        {
            //bool Ptstate = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var state = (from c in entities.RLCS_State_Mapping select c)
                          .Where(x => x.SM_Code.Trim().ToUpper() == stateCodeORName.Trim().ToUpper()
                          || x.SM_Name.Trim().ToUpper() == stateCodeORName.Trim().ToUpper()
                           && x.SM_Status == "A" && x.PT_Applicability != null
                           && x.PT_Applicability == "Y").FirstOrDefault();
                    if (state != null && state.PT_Applicability == null)
                    {
                        RLCS_State_Mapping PTstate = null;
                        return PTstate;
                    }


                    return state;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static bool GetCitybyCode(string state, string City)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = (from c in entities.RLCS_Location_City_Mapping select c).Any(x => x.SM_Code == state &&
                    (x.LM_Name.Trim().ToUpper() == City || x.LM_Code.Trim().ToUpper() == City));

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }

        public static bool CheckESICCode(string ESIC)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = entities.RLCS_Employee_Master.Any(x => x.EM_ESICNO == ESIC);

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }
        public static string GetCommencementDate(string ClientID)
        {
            string CommencementDate = "";
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.RLCS_Client_BasicDetails
                                 where row.CB_ClientID == ClientID
                                 select row
                          ).FirstOrDefault();
                    if (query != null)
                    {
                        DateTime dt = new DateTime();
                        dt = Convert.ToDateTime(query.CB_DateOfCommencement);
                        CommencementDate = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                    }

                    return CommencementDate;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return CommencementDate;
            }

        }
        public static string GetClientIDByBranchID(string BranchID)
        {
            string ClientID = "";
            int branchid = Convert.ToInt32(BranchID);
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.RLCS_Client_BasicDetails
                                 where row.AVACOM_BranchID == branchid
                                 select row
                          ).FirstOrDefault();
                    if (query != null)
                       ClientID = query.CB_ClientID.ToString();
                       
                    return ClientID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ClientID;
            }

        }
        public static string GetCommDateforNewEmployee(string BranchID)
        {
            string CommencementDate = "";
            string ClientID = "";
            int branchid = Convert.ToInt32(BranchID);
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.CustomerBranches
                                 where row.ID == branchid
                                 select row
                          ).FirstOrDefault();
                    if (query != null)
                    {
                        string ParentID = query.ParentID.ToString();
                        if (ParentID != "")
                            ClientID = RLCS_ClientsManagement.GetClientIDByBranchID(ParentID);
                        else
                            ClientID = RLCS_ClientsManagement.GetClientIDByBranchID(BranchID);

                        if (ClientID != "")
                            CommencementDate = RLCS_ClientsManagement.GetCommencementDate(ClientID);
                        
                    }

                    return CommencementDate;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return CommencementDate;
            }

        }
        public static bool CheckUANCode(string UAN)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = entities.RLCS_Employee_Master.Any(x => x.EM_UAN == UAN);

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }

        public static bool CheckPFCode(string PF)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = entities.RLCS_Employee_Master.Any(x => x.EM_PFNO == PF);

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }
        public static string GetCityCode(string City)
        {
            try
            {
                string Present = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = (from c in entities.RLCS_Location_City_Mapping
                               where c.LM_Name == City
                               select c.LM_Code).FirstOrDefault();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public static string GetCityCodeByID(int City)
        {
            try
            {
                string Present = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = (from c in entities.RLCS_Location_City_Mapping
                               where c.AVACOM_CityID == City
                               select c.LM_Code).FirstOrDefault();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        //public static string GetCustomerID(string CorporateID)
        //{
        //    try
        //    {
        //        string Present = "";
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            Present = (from c in entities.Customers
        //                       where c.LM_Name == City
        //                       select c.LM_Code).FirstOrDefault();

        //            return Present;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return null;
        //    }

        //}
        public static bool CheckExistsCorporate(string corpID)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = (from row in entities.RLCS_Customer_Corporate_Mapping
                               select row).Any(x => x.CO_CorporateID == corpID);

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static int? GetAVACOMCustIDByCorpID(string corpID)
        {
            try
            {
                int? customerID = 0;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    customerID = (from row in entities.RLCS_Customer_Corporate_Mapping
                                  where row.CO_CorporateID == corpID
                                  select row.AVACOM_CustomerID).FirstOrDefault();

                    return customerID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static int GetCustomerIDByClientID(string ClientID)
        {
            try
            {
                int customerID = 0;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var BranchID = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                    where row.CM_ClientID == ClientID
                                    select row.AVACOM_BranchID).FirstOrDefault();

                    customerID = (from c in entities.CustomerBranches
                                  where c.ID == BranchID
                                  select c.CustomerID).FirstOrDefault();

                    return customerID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool ExistsClientID(string clientID)
        {
            try
            {
                bool Present = false;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                               select row).Any(x => x.CM_ClientID == clientID);

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool GetClientByid(string ClientID)
        {
            try
            {
                bool Present = false;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //Present = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                    //           select row).Any(x => x.CM_ClientID == ClientID);

                    Present = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                               select row).Any(x => x.CM_ClientID == ClientID && x.CM_Status == "A");

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool GetClientName(string ClientName)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                               select row).Any(x => x.CM_ClientName == ClientName);

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CheckBranch(string clientID, string CorporateID, string Branch, string BranchType)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                               select row).Any(x => x.AVACOM_BranchName.Trim().ToUpper().Equals(Branch.Trim().ToUpper())
                              // && x.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                               && x.CM_Status=="A"
                               && x.CO_CorporateID.Trim().ToUpper().Equals(CorporateID.Trim().ToUpper())
                               && x.BranchType == BranchType);

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion Setup

        public static RLCS_CustomerBranch_ClientsLocation_Mapping GetStateLocationByBranchName(string branchnm, int CustomerID)
        {
            RLCS_CustomerBranch_ClientsLocation_Mapping Branch = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                try
                {
                    RLCS_CustomerBranch_ClientsLocation_Mapping BranchNew = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                                             where row.AVACOM_BranchName == branchnm && row.AVACOM_CustomerID == CustomerID
                                                                             select row).FirstOrDefault();


                    if (BranchNew != null)
                    {
                        return BranchNew;
                    }
                    else
                    {
                        return Branch;
                    }

                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //entities.Connection.Close();
                    return Branch;
                }
            }
        }

        #region Inputs
        public static bool CreateUpdateEmployeeMaster(RLCS_Employee_Master Employee)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool success;
                //entities.Connection.Open();
                try
                {
                    RLCS_Employee_Master employeeMaster = (from entry in entities.RLCS_Employee_Master
                                                           where entry.EM_EmpID == Employee.EM_EmpID 
                                                           && entry.AVACOM_CustomerID == Employee.AVACOM_CustomerID
                                                           select entry).FirstOrDefault();

                    //string RLCSCountry = "";
                    //if (!string.IsNullOrEmpty(Employee.EM_Nationality) && Employee.EM_International_workers.Trim().ToUpper() == "Y")
                    //{
                    //    var Country = (from row in entities.Countries
                    //                   where row.Name.Trim().ToUpper() == Employee.EM_Nationality.Trim().ToUpper()
                    //                   select row.ID).SingleOrDefault();
                    //    if (Country > 0)
                    //    {
                    //        RLCSCountry = (from row in entities.RLCS_Country_Mapping
                    //                       where row.AVACOM_CountryID == Country
                    //                       select row.CNT_ID).SingleOrDefault();
                    //    }
                    //}


                    if (employeeMaster != null)
                    {
                        employeeMaster.AVACOM_CustomerID = Employee.AVACOM_CustomerID;
                        employeeMaster.AVACOM_BranchID = Employee.AVACOM_BranchID;

                        employeeMaster.EM_EmpID = Employee.EM_EmpID;
                        employeeMaster.EM_ClientID = Employee.EM_ClientID;
                        employeeMaster.EM_EmpName = Employee.EM_EmpName;
                        employeeMaster.EM_State = Employee.EM_State;                       
                        employeeMaster.EM_Location = Employee.EM_Location;
                        employeeMaster.EM_Branch = Employee.EM_Branch;
                        employeeMaster.EM_PT_State = Employee.EM_PT_State;
                        employeeMaster.EM_Gender = Employee.EM_Gender;
                        employeeMaster.EM_FatherName = Employee.EM_FatherName;
                        employeeMaster.EM_Relationship = Employee.EM_Relationship;
                        employeeMaster.EM_DOB = Employee.EM_DOB;
                        employeeMaster.EM_MaritalStatus = Employee.EM_MaritalStatus;
                        employeeMaster.EM_DOJ = Employee.EM_DOJ;
                        employeeMaster.EM_DOL = Employee.EM_DOL;
                        employeeMaster.EM_PFNO = Employee.EM_PFNO;
                        employeeMaster.EM_ESICNO = Employee.EM_ESICNO;
                        employeeMaster.EM_UAN = Employee.EM_UAN;
                        employeeMaster.EM_Emailid = Employee.EM_Emailid;
                        employeeMaster.EM_MobileNo = Employee.EM_MobileNo;
                        employeeMaster.EM_PAN = Employee.EM_PAN;
                        employeeMaster.EM_Aadhar = Employee.EM_Aadhar;
                        employeeMaster.EM_BankName = Employee.EM_BankName;
                        employeeMaster.EM_Department = Employee.EM_Department;
                        employeeMaster.EM_Designation = Employee.EM_Designation;
                        employeeMaster.EM_SkillCategory = Employee.EM_SkillCategory;
                        employeeMaster.EM_Bankaccountnumber = Employee.EM_Bankaccountnumber;
                        employeeMaster.EM_IFSC = Employee.EM_IFSC;
                        employeeMaster.EM_Address = Employee.EM_Address;
                        employeeMaster.EM_PassportNo = Employee.EM_PassportNo;
                        employeeMaster.EM_PhysicallyChallenged = Employee.EM_PhysicallyChallenged;
                        employeeMaster.EM_Firsttime_secondtime = Employee.EM_Firsttime_secondtime;
                        employeeMaster.EM_International_workers = Employee.EM_International_workers;
                        employeeMaster.EM_PF_Capping_Applicability = Employee.EM_PF_Capping_Applicability;
                        employeeMaster.EM_Employmenttype = Employee.EM_Employmenttype;
                        employeeMaster.EM_ChangeEffective_from = Employee.EM_ChangeEffective_from;
                        employeeMaster.EM_Status = Employee.EM_Status;
                        employeeMaster.EM_Nationality = Employee.EM_Nationality;
                        employeeMaster.EM_PayrollMonth = Employee.EM_PayrollMonth;
                        employeeMaster.EM_PayrollYear = Employee.EM_PayrollYear;
                        employeeMaster.EM_PF_Applicability = Employee.EM_PF_Applicability;
                        employeeMaster.EM_ESI_Applicability = Employee.EM_ESI_Applicability;
                        employeeMaster.EM_ESI_Out_of_Courage_Month = Employee.EM_ESI_Out_of_Courage_Month;
                        employeeMaster.EM_ESI_Out_of_Courage_Year = Employee.EM_ESI_Out_of_Courage_Year;
                        employeeMaster.EM_PassportIssued_Country = Employee.EM_PassportIssued_Country;
                        employeeMaster.EM_Passport_Valid_From = Employee.EM_Passport_Valid_From;
                        employeeMaster.EM_Passport_Valid_Upto = Employee.EM_Passport_Valid_Upto;
                        employeeMaster.EM_EPFO_Aadhar_Upload = Employee.EM_EPFO_Aadhar_Upload;
                        employeeMaster.EM_EPFO_Bank_Ac_Upload = Employee.EM_EPFO_Bank_Ac_Upload;
                        employeeMaster.EM_EPFO_PAN_Upload = Employee.EM_EPFO_PAN_Upload;
                        employeeMaster.EM_PMRPY = Employee.EM_PMRPY;
                        employeeMaster.EM_PT_Applicability = Employee.EM_PT_Applicability;
                        employeeMaster.EM_NoOf_Certificate = Employee.EM_NoOf_Certificate;
                        employeeMaster.EM_NoOf_Certificate_Date = Employee.EM_NoOf_Certificate_Date;
                        employeeMaster.EM_TokenNo = Employee.EM_TokenNo;
                        employeeMaster.EM_Relay_Assigned = Employee.EM_Relay_Assigned;
                        employeeMaster.EM_Letter_Of_Group = Employee.EM_Letter_Of_Group;
                        employeeMaster.EM_WomenWorkingNightshift = Employee.EM_WomenWorkingNightshift;
                        employeeMaster.EM_ModeofTransport = Employee.EM_ModeofTransport;
                        employeeMaster.EM_SecurityProvided = Employee.EM_SecurityProvided;

                        employeeMaster.EM_ExistReasonCode = Employee.EM_ExistReasonCode;

                        employeeMaster.EM_YearsOfExperience = Employee.EM_YearsOfExperience;
                        employeeMaster.EM_DateWhenClothesGiven = Employee.EM_DateWhenClothesGiven;
                        employeeMaster.EM_NumberandDateOfExemptingOrder = Employee.EM_NumberandDateOfExemptingOrder;
                        employeeMaster.EM_ParticularsOfTransferFromOneGroupToAnother = Employee.EM_ParticularsOfTransferFromOneGroupToAnother;
                        employeeMaster.EM_SalesPromotion = Employee.EM_SalesPromotion;
                        employeeMaster.EM_PaymentMode = Employee.EM_PaymentMode;
                        employeeMaster.EM_PermanentAddress = Employee.EM_PermanentAddress;
                        employeeMaster.EM_Markof_Identification = Employee.EM_Markof_Identification;

                        employeeMaster.EM_Placeof_work = Employee.EM_Placeof_work;

                        employeeMaster.EM_Remarks = Employee.EM_Remarks;
                        employeeMaster.EM_EductionLevel = Employee.EM_EductionLevel;
                        employeeMaster.EM_Place_of_Employment = Employee.EM_Place_of_Employment;
                        employeeMaster.EM_Training_Number = Employee.EM_Training_Number;
                        employeeMaster.EM_Training_Date = Employee.EM_Training_Date;
                        employeeMaster.EM_Emergency_contact_Address = Employee.EM_Emergency_contact_Address;
                        employeeMaster.EM_Emergency_contact_mobile_no = Employee.EM_Emergency_contact_mobile_no;
                        employeeMaster.EM_EPS_Applicabilty = Employee.EM_EPS_Applicabilty;

                        employeeMaster.EM_Client_ESI_Number = Employee.EM_Client_ESI_Number;
                        employeeMaster.EM_Client_PT_State = Employee.EM_Client_PT_State; 

                        employeeMaster.EM_ModifiedDate = DateTime.Now;

                        employeeMaster.IsProcessed = false;
						employeeMaster.EM_IsLwf_Exempted = Employee.EM_IsLwf_Exempted;
                        entities.SaveChanges();

                        success = true;
                        return success;
                    }
                    else
                    {
                        //Employee.EM_Status = "A";
                        Employee.EM_CreateDate = DateTime.Now;

                        entities.RLCS_Employee_Master.Add(Employee);
                        entities.SaveChanges();

                        success = true;
                        return success;
                    }

                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //entities.Connection.Close();
                    return false;
                }
            }
        }

        public static RLCS_Employee_Master GetEmployeeDetails(string EmpID, int CustID, string ClientID)
        {
            RLCS_Employee_Master Employee = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    RLCS_Employee_Master employeeMaster = (from entry in entities.RLCS_Employee_Master
                                                           where entry.EM_EmpID == EmpID 
                                                           && entry.AVACOM_CustomerID == CustID 
                                                           && entry.EM_ClientID == ClientID
                                                           select entry).FirstOrDefault();

                    if (employeeMaster != null)
                    {
                        return employeeMaster;
                    }
                    else
                    {
                        return Employee;
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //entities.Connection.Close();
                    return Employee;
                }
            }
        }

        //public static Dictionary<string, bool> MatchAllClientHeaders(Dictionary<string, bool> ClientHeaders, string TableName)
        //{
        //    Dictionary<string, bool> MatchedHeaders = new Dictionary<string, bool>();
        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            var HeadersExist = (from c in entities.RLCS_ClientHeader_Mapping where c.TableName == TableName select c).Distinct().ToList();

        //            if (HeadersExist != null && HeadersExist.Count > 0)
        //            {
        //                //HeadersExist.Any(x =>x.ClientHeaderName == ClientHeaders.)
        //                foreach (var a in HeadersExist)
        //                {
        //                    if (ClientHeaders.Any(x => x.Key.Equals(a.ClientHeaderName)))
        //                    {
        //                        MatchedHeaders.Add(a.ColName, true);
        //                    }
        //                }
        //                return MatchedHeaders;
        //            }
        //            else
        //            {
        //                return ClientHeaders;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return null;
        //    }
        //}

        public static List<RLCS_TableColumn_Mapping> GetAllTableColumn(string TableName)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var TableColumns = (from c in entities.RLCS_TableColumn_Mapping where c.TableName == TableName select c).Distinct().ToList();
                    return TableColumns;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public static List<RLCS_ClientHeader_Mapping> GetMappedClientHeadersColumn(string TableName, int CustID)
        {
            List<RLCS_ClientHeader_Mapping> ClientHeaders = new List<RLCS_ClientHeader_Mapping>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ClientHeaders = (from c in entities.RLCS_ClientHeader_Mapping where c.TableName == TableName && c.CustomerID == CustID select c).Distinct().ToList();
                    return ClientHeaders;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public static void Update_ProcessedStatus_EmployeeMaster(string EMPID, string ClientID, bool status)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var EmployeeRecord = (from row in entities.RLCS_Employee_Master
                                      where row.EM_EmpID == EMPID
                                      && row.EM_ClientID == ClientID
                                      select row).FirstOrDefault();

                if (EmployeeRecord != null)
                {
                    EmployeeRecord.IsProcessed = status;
                    entities.SaveChanges();
                }
            }
        }

        public static bool SaveclientHeaderMapping(List<RLCS_ClientHeader_Mapping> RLCS_ClientHeader_MappingModellist, string TblName, int CustID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool success = false;
                try
                {
                    var ClientHeaders = (from Header in entities.RLCS_ClientHeader_Mapping
                                         where Header.TableName == TblName && Header.CustomerID == CustID
                                         select Header).ToList();

                    if (ClientHeaders != null && ClientHeaders.Count > 0)
                    {
                       foreach(var r in RLCS_ClientHeader_MappingModellist)
                        {
                            var header = (from row in entities.RLCS_ClientHeader_Mapping
                                                 where row.ColName == r.ColName && row.ClientHeaderName == r.ClientHeaderName && row.CustomerID == CustID
                                          select row).FirstOrDefault();
                            if(header!=null)
                            {
                                header.ColName = r.ColName;
                                header.ClientHeaderName = r.ClientHeaderName;
                                entities.Entry(header).State = EntityState.Modified;
                            }
                            else
                            {
                                var data = (from row in entities.RLCS_ClientHeader_Mapping
                                              where row.CustomerID == CustID && row.ClientHeaderName == r.ClientHeaderName
                                            select row).SingleOrDefault();
                                data.ColName = r.ColName;
                                entities.Entry(data).State = EntityState.Modified;
                                //entities.RLCS_ClientHeader_Mapping.Remove(data);
                                //entities.RLCS_ClientHeader_Mapping.Add(r);
                            }
                            
                        }
                        entities.SaveChanges();
                        //  var remaining = RLCS_ClientHeader_MappingModellist.Where(x => !(ClientHeaders.Any(y => y.ClientHeaderName == x.ClientHeaderName))).ToList();
                        //      foreach (var Header in remaining)
                        //{
                        //    // ClientHeaders.RemoveAll(x => ClientHeaders.Any(y => y.ClientHeaderName == Header.ClientHeaderName));
                        //    var result = (from d in entities.RLCS_ClientHeader_Mapping where d.ColName == Header.ColName select d).FirstOrDefault();
                        //    if (result != null)
                        //        entities.RLCS_ClientHeader_Mapping.Remove(result);

                        //}
                        //entities.RLCS_ClientHeader_Mapping.AddRange(remaining);
                        //entities.SaveChanges();
                        success = true;
                    }
                    else
                    {
                        if (RLCS_ClientHeader_MappingModellist != null && RLCS_ClientHeader_MappingModellist.Count > 0)
                        {
                            foreach (var Header in RLCS_ClientHeader_MappingModellist)
                            {
                                if (!string.IsNullOrEmpty(Header.ColName) && !string.IsNullOrEmpty(Header.ClientHeaderName))
                                {

                                    ClientHeaders.Add(new RLCS_ClientHeader_Mapping { ID = 0, CustomerID = Header.CustomerID, TableName = "RLCS_Employee_Master", ColName = Header.ColName, ClientHeaderName = Header.ClientHeaderName });
                                }
                            }
                        }
                        entities.RLCS_ClientHeader_Mapping.AddRange(ClientHeaders);
                        entities.SaveChanges();
                        success = true;
                    }
                    return success;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //entities.Connection.Close();
                    return false;
                }

            }
        }

        public static bool CreateUploadEmployeeMaster(List<RLCS_Employee_Master> Employee, int custid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool success = false;
                //entities.Connection.Open();
                try
                {
                    //  var present = entities.Employee_Master.Where(a => !Employee.Any(x => x.EM_EmpID == a.EM_EmpID && x.AVACOM_CustomerID == custid)).Any();
                    //(from s in entities.Employee_Master select s).Where()
                    //  where s.EM_ClientID == custid && (s.EM_EmpID != Employee.Any(x =>))
                    if (Employee != null && Employee.Count > 0)
                    {
                        Employee.ForEach(x => x.EM_ClientID = "TREG");
                        Employee.ForEach(x => x.AVACOM_CustomerID = custid);
                        Employee.ForEach(x => x.EM_Passport_Valid_From = null);
                        Employee.ForEach(x => x.EM_Passport_Valid_Upto = null);
                        entities.RLCS_Employee_Master.AddRange(Employee);
                        entities.SaveChanges();
                        success = true;

                    }
                    return success;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //entities.Connection.Close();
                    return false;
                }
            }
        }

        //public static bool UpdateUploadEmployeeMaster(List<RLCS_Employee_Master> Employee, int CustID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        bool success = false;
        //        //entities.Connection.Open();
        //        try
        //        {
        //            if (Employee != null && Employee.Count > 0)
        //            {
        //                foreach (var Emp in Employee)
        //                {

        //                    RLCS_Employee_Master employeeMaster = (from entry in entities.RLCS_Employee_Master
        //                                                           where entry.EM_EmpID == Emp.EM_EmpID && entry.AVACOM_CustomerID == CustID && entry.EM_ClientID == Emp.EM_ClientID
        //                                                           select entry).FirstOrDefault();

        //                    if (employeeMaster != null)
        //                    {
        //                        RLCS_Employee_Master empl = new RLCS_Employee_Master();
        //                        employeeMaster.AVACOM_CustomerID = CustID;
        //                        employeeMaster.EM_EmpID = Emp.EM_EmpID;
        //                        employeeMaster.EM_EmpName = Emp.EM_EmpName;
        //                        employeeMaster.EM_State = Emp.EM_State;
        //                        employeeMaster.EM_Location = Emp.EM_Location;
        //                        employeeMaster.EM_Branch = Emp.EM_Branch;
        //                        employeeMaster.EM_Gender = Emp.EM_Gender;
        //                        employeeMaster.EM_FatherName = Emp.EM_FatherName;
        //                        employeeMaster.EM_DOB = Emp.EM_DOB;
        //                        employeeMaster.EM_DOJ = Emp.EM_DOJ;
        //                        employeeMaster.EM_SkillCategory = Emp.EM_SkillCategory;
        //                        employeeMaster.EM_ESICNO = Emp.EM_ESICNO;
        //                        employeeMaster.EM_UAN = Emp.EM_UAN;
        //                        employeeMaster.EM_PAN = Emp.EM_PAN;
        //                        employeeMaster.EM_Aadhar = Emp.EM_Aadhar;
        //                        employeeMaster.EM_Bankaccountnumber = Emp.EM_Bankaccountnumber;
        //                        employeeMaster.EM_IFSC = Emp.EM_IFSC;
        //                        employeeMaster.EM_Firsttime_secondtime = Emp.EM_Firsttime_secondtime;
        //                        employeeMaster.EM_International_workers = Emp.EM_International_workers;
        //                        employeeMaster.EM_PF_Capping_Applicability = Emp.EM_PF_Capping_Applicability;
        //                        employeeMaster.EM_Nationality = Emp.EM_Nationality;
        //                        employeeMaster.EM_PayrollMonth = Emp.EM_PayrollMonth;
        //                        employeeMaster.EM_PayrollYear = Emp.EM_PayrollYear;
        //                        employeeMaster.EM_PF_Applicability = Emp.EM_PF_Applicability;
        //                        employeeMaster.EM_ESI_Applicability = Emp.EM_ESI_Applicability;
        //                        employeeMaster.EM_PFNO = Emp.EM_PFNO;
        //                        entities.SaveChanges();
        //                        success = true;
        //                    }
        //                }

        //            }
        //            return success;
        //        }


        //        catch (Exception ex)
        //        {
        //            ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //            //entities.Connection.Close();
        //            return false;
        //        }
        //    }
        //}
        public static bool CheckEmployeeID(string EMPID, int? CustID)
        {
            try
            {
                bool Present = false;
                if (CustID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Present = (from row in entities.RLCS_Employee_Master
                                   where row.EM_EmpID == EMPID
                                   && row.EM_Status == "A"
                                   && row.AVACOM_CustomerID == CustID
                                   select row).Any();

                        return Present;
                    }
                }
                else
                    return Present;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CheckBranchExist(string Branch, int? CustID)
        {
            try
            {
                bool Present = false;
                if (CustID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Present = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                   where row.AVACOM_BranchName == Branch
                                   && row.BranchType == "B"
                                   && row.AVACOM_CustomerID == CustID
                                   select row).Any();

                        return Present;
                    }
                }
                else
                    return Present;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool GetEmployeeByid(string EMPID, int CustID, string ClientID)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = (from row in entities.RLCS_Employee_Master
                               where row.EM_EmpID == EMPID 
                               && row.AVACOM_CustomerID == CustID 
                               && row.EM_ClientID == ClientID
                               select row).Any();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static int GetStateidBycode(string Statecode)
        {
            try
            {
                int Stateid = 0;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Stateid = (from row in entities.RLCS_State_Mapping
                               where row.SM_Code == Statecode
                               select row.AVACOM_StateID).FirstOrDefault();

                    return Stateid;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static string GetCorporateID(string ClientID)
        {
            try
            {
                string Stateid = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Stateid = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                               where row.CM_ClientID == ClientID 
                               && row.BranchType == "E"
                               select row.CO_CorporateID).FirstOrDefault();

                    return Stateid;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static RLCS_CustomerBranch_ClientsLocation_Mapping GetStateLocationByBranch(int BranchID)
        {
            RLCS_CustomerBranch_ClientsLocation_Mapping Branch = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    RLCS_CustomerBranch_ClientsLocation_Mapping BranchNew = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                                             where row.AVACOM_BranchID == BranchID
                                                                             select row).FirstOrDefault();

                    if (BranchNew != null)
                    {
                        return BranchNew;
                    }
                    else
                    {
                        return Branch;
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //entities.Connection.Close();
                    return Branch;
                }
            }
        }

        #endregion

        public static bool CreateUpdate_CustomerBranch_ClientsOrLocation_Mapping(RLCS_CustomerBranch_ClientsLocation_Mapping _objRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                      where row.CM_ClientID.Trim().ToUpper().Equals(_objRecord.CM_ClientID.Trim().ToUpper())
                                      && row.AVACOM_BranchName.Trim().ToUpper().Equals(_objRecord.AVACOM_BranchName.Trim().ToUpper())
                                      && row.BranchType.Trim().ToUpper().Equals(_objRecord.BranchType.Trim().ToUpper())
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.AVACOM_BranchID = _objRecord.AVACOM_BranchID;
                        prevRecord.AVACOM_BranchName = _objRecord.AVACOM_BranchName;
                        prevRecord.BranchType = _objRecord.BranchType;

                        prevRecord.CO_CorporateID = _objRecord.CO_CorporateID;

                        prevRecord.CM_ClientID = _objRecord.CM_ClientID;
                        prevRecord.CM_ClientName = _objRecord.CM_ClientName;
                        prevRecord.CM_EstablishmentType = _objRecord.CM_EstablishmentType;
                        prevRecord.CM_ServiceStartDate = _objRecord.CM_ServiceStartDate;
                        prevRecord.CM_Country = _objRecord.CM_Country;
                        prevRecord.CM_State = _objRecord.CM_State;
                        prevRecord.CM_City = _objRecord.CM_City;
                        prevRecord.CM_Pincode = _objRecord.CM_Pincode;
                        prevRecord.CM_Status = _objRecord.CM_Status;
                        prevRecord.CM_RLCSAnchor = _objRecord.CM_RLCSAnchor;
                        prevRecord.CM_BDAnchor = _objRecord.CM_BDAnchor;
                        prevRecord.CM_ProcessAnchor = _objRecord.CM_ProcessAnchor;
                        prevRecord.CL_LocationAnchor = _objRecord.CL_LocationAnchor;

                        prevRecord.CM_Excemption = _objRecord.CM_Excemption;
                        prevRecord.CM_ActType = _objRecord.CM_ActType;
                        prevRecord.CM_BonusPercentage = _objRecord.CM_BonusPercentage;
                        prevRecord.CM_Address = _objRecord.CM_Address;
                        prevRecord.CM_IsPOApplicable = _objRecord.CM_IsPOApplicable;
                        prevRecord.CL_PF_Code = _objRecord.CL_PF_Code;

                        prevRecord.IsProcessed = false;
                        prevRecord.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }
                    else
                    {
                        _objRecord.CreatedOn = DateTime.Now;

                        entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Add(_objRecord);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static void Update_ProcessedStatus_Corporate(int customerID, string corporateID, bool status)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var corporateRecord = (from row in entities.RLCS_Customer_Corporate_Mapping
                                           where row.CO_CorporateID == corporateID
                                           && row.AVACOM_CustomerID == customerID
                                           select row).FirstOrDefault();

                    if (corporateRecord != null)
                    {
                        corporateRecord.IsProcessed = status;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void DeleteCustomerBranch_Compliance(int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var recordCustBranch = entities.CustomerBranches.Where(x => x.ID == customerBranchID).FirstOrDefault();
                    if (recordCustBranch != null)
                    {
                        entities.CustomerBranches.Remove(recordCustBranch);
                        entities.SaveChanges();
                    }

                    if (customerBranchID > 0)
                    {
                        customerBranchID--;
                        entities.SP_ResetIDCustomerBranch(customerBranchID);
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        public static RLCS_CustomerBranch_ClientsLocation_Mapping GetClientLocationDetails(string clientID, string stateCode, string city, string branchName, string branchType)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                      where row.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                      && (row.CM_State != null && row.CM_State.Trim().ToUpper().Equals(stateCode.Trim().ToUpper()))
                                      && (row.CM_City != null && row.CM_City.Trim().ToUpper().Equals(city.Trim().ToUpper()))
                                      && (row.CM_ClientName != null && row.CM_ClientName.Trim().ToUpper().Equals(branchName.Trim().ToUpper()))
                                      && row.BranchType.Trim().ToUpper().Equals(branchType)
                                      select row).FirstOrDefault();

                    return prevRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static RLCS_CustomerBranch_ClientsLocation_Mapping GetClientLocationDetails(int branchID, string branchType)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                      where row.AVACOM_BranchID == branchID
                                      && row.BranchType.Trim().ToUpper().Equals(branchType)
                                      select row).FirstOrDefault();

                    return prevRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<State> GetStates()
        {
            List<State> States = new List<State>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var StateList = (from c in entities.States select c).Distinct().ToList();

                    return StateList;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<ActVModel> GetAllActs(int State)
        {
            List<ActVModel> ActVModel = new List<ActVModel>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (State != -1)
                    {
                        ActVModel = (from row in entities.Acts.AsParallel()
                                     where row.IsDeleted == false && row.StateID == State
                                     orderby row.Name ascending
                                     select new ActVModel { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                    }
                    else
                    {
                        ActVModel = (from row in entities.Acts.AsParallel()
                                     where row.IsDeleted == false
                                     orderby row.Name ascending
                                     select new ActVModel { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();
                    }
                    // ActVModel.AddRange(acts);
                    return ActVModel;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static string GetLocationName(string LM_Code)
        {
            try
            {
                string Present = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = (from c in entities.RLCS_Location_City_Mapping
                               where c.LM_Code == LM_Code
                               select c.LM_Name).FirstOrDefault();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static string GetStateName(string SM_Code)
        {
            try
            {
                string Present = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = (from c in entities.RLCS_State_Mapping
                               where c.SM_Code == SM_Code
                               select c.SM_Name).FirstOrDefault();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static string GetClientNameByParentID(int ParentID)
        {
            try
            {
                string Present = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Parent = (from c in entities.CustomerBranches
                                  where c.ID == ParentID
                                  select c).FirstOrDefault();
                    if (Parent.ParentID == null)
                    {
                        Present = Parent.Name;
                    }
                    else
                    {
                        Present = (from s in entities.CustomerBranches
                                   where s.ID == Parent.ParentID
                                   select s.Name).FirstOrDefault();
                    }
                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public static string GetCorporateIDByClientName(string Client, int BranchID)
        {
            try
            {
                string client = "";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    client = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                              where row.BranchType == "E"
                              && row.AVACOM_BranchID == BranchID
                              select row.CO_CorporateID).FirstOrDefault();

                    return client;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static string GetClientIDByName(string Client)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string Present = "";
                    Present = (from c in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                               where c.CM_ClientName.Trim().ToUpper() == Client.Trim().ToUpper()
                               select c.CM_ClientID).FirstOrDefault();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }
        public static string GetClientIDByName_New(string Client, int BranchID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string Present = "";
                    Present = (from c in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                               where c.AVACOM_BranchID == BranchID
                               select c.CM_ClientID).FirstOrDefault();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }
        #region Paycode


        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetAll_client()
        {
            List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstEntities = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstEntities = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                   where row.BranchType == "E"
                                   && row.CM_Status == "A"
                                   && row.IsProcessed == true
                                   select row).Distinct().ToList();
                }

                return lstEntities;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstEntities;
            }
        }

        public static List<object> GetAll_StandardColumnsForPaycode(string client)
        {
            List<object> lstEntities = new List<object>();


            try
            {
                lstEntities.Add("Select");
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstEntities = (from row in entities.RLCS_Client_Paycode_Mapping_Details
                                   where !string.IsNullOrEmpty(row.CPMD_Standard_Column)
                                   && row.CPMD_Status == "A"
                                   select row.CPMD_Standard_Column).Distinct().ToList<object>();

                    //&& row.CPMD_ClientID.Trim().ToUpper() == client.Trim().ToUpper()
                    // lstEntities =  entities.RLCS_Client_Paycode_Mapping_Details.Where(row=> !string.IsNullOrEmpty(row.CPMD_Standard_Column) && row.CPMD_Status == "A").Distinct().ToList();
                }

                return lstEntities;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstEntities;
            }
        }
        public static bool CreateUpdate_ClientPaycodeMapping(List<RLCS_Client_Paycode_Mapping> _objRecord, List<RLCS_Client_Paycode_Mapping_Details> lst_objRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (DbContextTransaction transaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        bool saveSuccess = false;

                        _objRecord.ForEach(result =>
                        {
                            var prevRecord = (from row in entities.RLCS_Client_Paycode_Mapping
                                                  //where row.CPM_ClientID == row.CPM_ClientID
                                              where row.CPM_ClientID == result.CPM_ClientID
                                              select row).FirstOrDefault();

                            if (prevRecord != null)
                            {
                                prevRecord.AVACOM_CustomerID = result.AVACOM_CustomerID;
                                prevRecord.CPM_ClientID = result.CPM_ClientID;
                                prevRecord.CPM_PayGroup = result.CPM_PayGroup;
                                prevRecord.CPM_CreatedBy = result.CPM_CreatedBy;

                                prevRecord.CPM_ModifiedBy = result.CPM_CreatedBy;
                                prevRecord.CPM_ModifiedDate = DateTime.Now; ;
                                saveSuccess = true;
                            }
                            else
                            {
                                result.CPM_CreatedDate = DateTime.Now;
                                entities.RLCS_Client_Paycode_Mapping.Add(result);
                                saveSuccess = true;
                            }
                            entities.SaveChanges();
                        });
                        if (saveSuccess)
                        {
                            lst_objRecord.ForEach(eachRecord =>
                            {
                                var record = (from row in entities.RLCS_Client_Paycode_Mapping_Details
                                              where row.CPMD_ClientID == eachRecord.CPMD_ClientID
                                              && row.AVACOM_CustomerID == eachRecord.AVACOM_CustomerID
                                              && (row.CPMD_PayCode == eachRecord.CPMD_PayCode && row.CPMD_Standard_Column == eachRecord.CPMD_Standard_Column)
                                              
                                              select row).FirstOrDefault();

                                if (record != null)
                                {
                                    record.AVACOM_CustomerID = eachRecord.AVACOM_CustomerID;
                                    record.CPMD_ClientID = eachRecord.CPMD_ClientID;
                                    record.CPMD_PayCode = eachRecord.CPMD_PayCode;
                                    record.CPMD_PayGroup = eachRecord.CPMD_PayGroup;

                                    record.CPMD_Standard_Column = eachRecord.CPMD_Standard_Column;
                                    record.CPMD_Header = eachRecord.CPMD_Header;
                                    
                                    record.CPMD_Sequence_Order = eachRecord.CPMD_Sequence_Order;
                                    record.CPMD_Deduction_Type = eachRecord.CPMD_Deduction_Type;
                                    record.CPMD_Status = eachRecord.CPMD_Status ?? "A";
                                    record.CPMD_appl_ESI = eachRecord.CPMD_appl_ESI;
                                    record.CPMD_Appl_PT = eachRecord.CPMD_Appl_PT;
                                    record.CPMD_Appl_LWF = eachRecord.CPMD_Appl_LWF;
                                    record.CPMD_appl_PF = eachRecord.CPMD_appl_PF;
                                    record.Column_DataType = "float";

                                    record.UpdatedOn = DateTime.Now;

                                    saveSuccess = true;
                                }
                                else
                                {
                                    if (RLCS_ClientsManagement.CheckSequence(eachRecord.CPMD_Sequence_Order, eachRecord.CPMD_ClientID,0))
                                        eachRecord.CPMD_Sequence_Order = RLCS_ClientsManagement.GetSequence(eachRecord.CPMD_ClientID); //.CPMD_Sequence_Order;
                                    else
                                        eachRecord.CPMD_Sequence_Order = eachRecord.CPMD_Sequence_Order;

                                    eachRecord.CreatedOn = DateTime.Now;
                                    eachRecord.CPMD_Status = "A";
                                    if (string.IsNullOrEmpty(eachRecord.CPMD_PayCode))
                                        eachRecord.CPMD_PayCode = " ";
                                    eachRecord.Column_DataType = "float";
                                    entities.RLCS_Client_Paycode_Mapping_Details.Add(eachRecord);
                                    saveSuccess = true;
                                }
                            });
                        }
                        entities.SaveChanges();

                        transaction.Commit();
                        return saveSuccess;
                    }

                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        // Logger.InsertException_DBLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        return false;
                    }
                }
            }
        }

        //To Validate file
        //public static void Update_ProcessedStatus_PayCodesMapping(int customerID, string clientID, bool status)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var objRecord = (from row in entities.RLCS_Client_Paycode_Mapping
        //                         where row.CPM_ClientID == clientID
        //                         && row.AVACOM_CustomerID == customerID
        //                         select row).FirstOrDefault();

        //        if (objRecord != null)
        //        {
        //            objRecord.IsProcessed = status;
        //            objRecord.CPM_ModifiedDate = DateTime.Now;
        //            entities.SaveChanges();
        //        }
        //    }
        //}

        //public bool CreateUpdate_ClientPaycodeMappingDetails(List<RLCS_Client_Paycode_Mapping_Details> lst_objRecord)
        //{
        //    try
        //    {
        //        bool saveSuccess = false;
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            lst_objRecord.ForEach(eachRecord =>
        //            {
        //                var prevRecord = (from row in entities.RLCS_Client_Paycode_Mapping_Details
        //                                  where row.CPMD_ClientID == eachRecord.CPMD_ClientID
        //                                  && row.AVACOM_CustomerID == eachRecord.AVACOM_CustomerID
        //                                  && (row.CPMD_PayCode == eachRecord.CPMD_PayCode && row.CPMD_Standard_Column == eachRecord.CPMD_Standard_Column)
        //                                  //&& row.CPMD_Status == _objRecord.CPMD_Status
        //                                  select row).FirstOrDefault();

        //                if (prevRecord != null)
        //                {
        //                    prevRecord.AVACOM_CustomerID = eachRecord.AVACOM_CustomerID;
        //                    prevRecord.CPMD_ClientID = eachRecord.CPMD_ClientID;
        //                    prevRecord.CPMD_PayCode = eachRecord.CPMD_PayCode;
        //                    prevRecord.CPMD_PayGroup = eachRecord.CPMD_PayGroup;

        //                    prevRecord.CPMD_Standard_Column = eachRecord.CPMD_Standard_Column;
        //                    prevRecord.CPMD_Header = eachRecord.CPMD_Header;
        //                    prevRecord.CPMD_Sequence_Order = eachRecord.CPMD_Sequence_Order;

        //                    prevRecord.CPMD_Deduction_Type = eachRecord.CPMD_Deduction_Type;
        //                    prevRecord.CPMD_Status = eachRecord.CPMD_Status;
        //                    prevRecord.CPMD_appl_ESI = eachRecord.CPMD_appl_ESI;
        //                    prevRecord.CPMD_Appl_PT = eachRecord.CPMD_Appl_PT;
        //                    prevRecord.CPMD_Appl_LWF = eachRecord.CPMD_Appl_LWF;
        //                    prevRecord.CPMD_appl_PF = eachRecord.CPMD_appl_PF;

        //                    prevRecord.Column_DataType = eachRecord.Column_DataType;

        //                    prevRecord.UpdatedOn = DateTime.Now;

        //                    saveSuccess = true;
        //                }
        //                else
        //                {
        //                    eachRecord.CreatedOn = DateTime.Now;

        //                    entities.RLCS_Client_Paycode_Mapping_Details.Add(eachRecord);
        //                    saveSuccess = true;
        //                }
        //            });

        //            entities.SaveChanges();
        //            return saveSuccess;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return false;
        //    }
        //}

        public static List<RLCS_PayCode_Master> GetPaycodeByType(string Paycode)
        {
            List<RLCS_PayCode_Master> Paycodes = new List<RLCS_PayCode_Master>();
            RLCS_PayCode_Master PaycodeMaster = new RLCS_PayCode_Master();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Paycodelist = (from row in entities.RLCS_PayCode_Master
                                       where row.PEM_Deduction_Type.ToUpper().Equals(Paycode.ToUpper())
                                       select new
                                       {
                                           PEM_Pay_Code = row.PEM_Pay_Code,
                                           PEM_Pay_Code_Description = row.PEM_Pay_Code + " / " + row.PEM_Pay_Code_Description,
                                       }).Distinct().ToList();
                    if (Paycodelist != null)
                    {
                        Paycodelist.ForEach(
                            eachrow =>
                            {
                                PaycodeMaster = new RLCS_PayCode_Master();
                                PaycodeMaster.PEM_Pay_Code = eachrow.PEM_Pay_Code;
                                PaycodeMaster.PEM_Pay_Code_Description = eachrow.PEM_Pay_Code_Description;

                                Paycodes.Add(PaycodeMaster);
                            }
                        );

                    }
                    return Paycodes;
                }


            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Paycodes;
            }
        }


        public static bool CheckSequence(int Sequence, string Client, int Id)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = entities.RLCS_Client_Paycode_Mapping_Details.Where(x => x.CPMD_ClientID.Equals(Client) && x.CPMD_Sequence_Order == Sequence && x.CPMD_Status == "A" && x.ID != Id).Any();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }

        public static bool CheckHeader(string Header, string Client, string Id)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int recid = Convert.ToInt32(Id);
                    Present = entities.RLCS_Client_Paycode_Mapping_Details.Where(x => x.CPMD_ClientID.Equals(Client) && x.CPMD_Header.Equals(Header) && x.CPMD_Status == "A" && x.ID != recid).Any();
                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }

        #region CHECK_PAY_STAND_TYPE 29APRIL2020
         public static bool CheckTypeOFStandardExist(string Type, string Client)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = entities.RLCS_Client_Paycode_Mapping_Details.Where(x => x.CPMD_ClientID.Equals(Client) && x.CPMD_Standard_Column.Equals(Type) && x.CPMD_Status=="A").Any();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }
        public static bool CheckTypeOFPaycodeExist(string Type, string Client)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Present = entities.RLCS_Client_Paycode_Mapping_Details.Where(x => x.CPMD_ClientID.Equals(Client) && x.CPMD_PayCode.Equals(Type) && x.CPMD_Status == "A").Any();

                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }
        #endregion

        public static string GetPaycodeDetails(string Paycode, string Client)
        {

            RLCS_Client_Paycode_Mapping_Details PaycodeDetails = new RLCS_Client_Paycode_Mapping_Details();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //PaycodeDetails = (from row in entities.RLCS_Client_Paycode_Mapping_Details
                    //                   where (row.CPMD_PayCode.ToUpper() == Paycode.ToUpper() || row.CPMD_Standard_Column.ToUpper() == Paycode.ToUpper()) && row.CPMD_ClientID.ToUpper()==Client 
                    //                   select row).Distinct().SingleOrDefault();

                    var Maxrecord = entities.RLCS_Client_Paycode_Mapping_Details.Where(p => p.CPMD_ClientID == Client && p.CPMD_Status == "A").Max(x => x.CPMD_Sequence_Order).ToString();
                    if (Maxrecord == null)
                    {
                        Maxrecord = "0";
                    }
                    string Maxrecord1 = Convert.ToString(Convert.ToInt32(Maxrecord) + 1);
                    return Maxrecord1;
                }


            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "1";
            }
        }

        //UpdateIsProcessedPaycodeMaster
        public static bool UpdateIsProcessedPaycodeMaster(string Client)
        {
            bool delete = false;
            RLCS_Client_Paycode_Mapping PaycodeMaster = new RLCS_Client_Paycode_Mapping();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    PaycodeMaster = (from row in entities.RLCS_Client_Paycode_Mapping
                                     where row.CPM_ClientID.ToUpper() == Client.ToUpper() 
                                      select row).SingleOrDefault();

                    if (PaycodeMaster != null)
                    {
                        PaycodeMaster.IsProcessed = false;
                        entities.SaveChanges();
                        delete = true;
                    }
                    return delete;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return delete;
            }
        }
        //ADD NEW FUNCTION 
        public static bool ActivePaycodeCheck(string Client,string Headercode)
        {
            bool delete = false;
            RLCS_Client_Paycode_Mapping_Details PaycodeDetails = new RLCS_Client_Paycode_Mapping_Details();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    PaycodeDetails = (from row in entities.RLCS_Client_Paycode_Mapping_Details
                                      where row.CPMD_ClientID.ToUpper() == Client.ToUpper() && (row.CPMD_PayCode == Headercode|| row.CPMD_Standard_Column == Headercode) && row.CPMD_Status=="A"
                                      select row).SingleOrDefault();
                    if (string.IsNullOrEmpty(PaycodeDetails.CPMD_ClientID))
                    {
                        delete= false;
                    }
                    else
                    {
                        delete = true;
                    }
                   
                }

                return delete;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return  delete ;
            }
        }
        //29APRIL2020 CHANGE BY GG
        public static bool DeletePaycodeDetails(long ID, string Client)
        {
            bool delete = false;
            RLCS_Client_Paycode_Mapping_Details PaycodeDetails = new RLCS_Client_Paycode_Mapping_Details();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //PaycodeDetails = (from row in entities.RLCS_Client_Paycode_Mapping_Details
                    //                  where row.CPMD_ClientID.ToUpper() == Client.ToUpper() && (row.CPMD_PayCode.ToUpper() == Paycode || row.CPMD_Header.ToUpper() == Paycode)
                    //                  select row).SingleOrDefault();
                    PaycodeDetails = (from row in entities.RLCS_Client_Paycode_Mapping_Details
                                      where row.CPMD_ClientID.ToUpper() == Client.ToUpper() &&  row.ID == ID
                                      select row).SingleOrDefault();

                    if (PaycodeDetails != null)
                    {

                        PaycodeDetails.CPMD_Status = (PaycodeDetails.CPMD_Status == "A") ? "I" : "A";
                        entities.SaveChanges();
                        delete = true;
                    }
                    return delete;
                }


            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return delete;
            }
        }


        //public static List<RLCS_Client_Paycode_Mapping_Details> GetAllPaycodeDetailsList(string Client, string filter = null)
        //{
        //    List<RLCS_Client_Paycode_Mapping_Details> PaycodeList = new List<RLCS_Client_Paycode_Mapping_Details>();
        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            PaycodeList = (from row in entities.RLCS_Client_Paycode_Mapping_Details
        //                           where row.CPMD_ClientID.ToUpper() == Client.ToUpper()
        //                           select row).ToList();

        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                PaycodeList = PaycodeList.Where(x => x.CPMD_PayCode.ToUpper().Contains(filter.ToUpper()) || x.CPMD_Standard_Column.ToUpper().Contains(filter.ToUpper()) || x.CPMD_Header.ToUpper().Contains(filter.ToUpper())).ToList();
        //            }
        //            return PaycodeList;
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return PaycodeList;
        //    }
        //}

        public static List<SP_Get_ClientPaycodeMappingDetails_Result> GetAllPaycodeDetailsList(string Client, string filter = null)
        {
            List<SP_Get_ClientPaycodeMappingDetails_Result> PaycodeList = new List<SP_Get_ClientPaycodeMappingDetails_Result>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    PaycodeList = (from row in entities.SP_Get_ClientPaycodeMappingDetails(Client)
                                   select row).ToList();

                    if (!string.IsNullOrEmpty(filter))
                    {
                        PaycodeList = PaycodeList.Where(x => x.CPMD_PayCode.ToUpper().Contains(filter.ToUpper()) || x.CPMD_Standard_Column.ToUpper().Contains(filter.ToUpper()) || x.CPMD_Header.ToUpper().Contains(filter.ToUpper()) || x.CPMD_PayGroup.ToUpper().Contains(filter.ToUpper()) || x.CPMD_Deduction_Type.ToUpper().Contains(filter.ToUpper()) || x.PS_Status.ToUpper().Equals(filter.ToUpper())).ToList();
                    }
                    return PaycodeList;
                }


            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PaycodeList;
            }
        }


        public static List<object> GetAll_Entities(int CustomerID)
        {
            List<object> EntityList = new List<object>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //EntityList = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                    //              where row.BranchType == "E"
                    //              && row.CM_Status == "A"
                    //              && row.IsProcessed == true
                    //              && row.AVACOM_CustomerID == CustomerID
                    //              orderby row.CM_ClientName ascending
                    //              select new
                    //              {
                    //                  ID = row.CM_ClientID,
                    //                  Name = row.CM_ClientName
                    //              }).Distinct().ToList<object>();

                     EntityList = (
                               from RCRM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                               join CB in entities.CustomerBranches on RCRM.AVACOM_BranchID  equals CB.ID
                               where RCRM.BranchType == "E" 
                               && RCRM.CM_Status == "A" 
                               && RCRM.IsProcessed == true 
                               && RCRM.AVACOM_CustomerID == CustomerID 
                               && CB.IsDeleted==false
                               orderby RCRM.CM_ClientName ascending
                               select new
                               {
                                   ID = RCRM.CM_ClientID, Name = RCRM.CM_ClientName
                               }).Distinct().ToList<object>();

                    return EntityList;
                }


            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return EntityList;
            }
        }

        public static bool CheckPaycodeClient(string Client)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    // Present = entities.RLCS_Client_Paycode_Mapping.Where(x => x.CPM_ClientID.Equals(Client) && x.CPM_Status.ToUpper() == "A").Any();
                    Present = entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Where(x => x.CM_ClientID.Equals(Client) && x.CM_Status.ToUpper() == "A" && x.BranchType == "E").Any();
                    return Present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }


        public static bool CheckStandardType(string StandardColumn)
        {
            try
            {
                bool present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    present = entities.RLCS_Client_Paycode_Mapping_Details.Where(x => x.CPMD_PayGroup.Trim().ToUpper() == "STANDARD" && x.CPMD_Standard_Column.ToUpper() == StandardColumn.Trim().ToUpper()).Any();

                    return present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }


        public static bool CheckPaycodes(string Paycode, string Type)
        {
            try
            {
                bool present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    
                    present = entities.RLCS_PayCode_Master.Where(x => x.PEM_Pay_Code.ToUpper() == Paycode.Trim().ToUpper() && x.PEM_Deduction_Type.ToUpper() == Type.Trim().ToUpper()).Any();

                    return present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }
        public static bool CheckPaycodesExist(string Paycode,string ClientID)
        {
            try
            {
                bool present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    present = entities.RLCS_Client_Paycode_Mapping_Details.Where(x => x.CPMD_Standard_Column.ToUpper() == Paycode.Trim().ToUpper()).Any();

                    return present;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }
        public static int GetSequence(string Client)
        {
            try
            {
                bool Present = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var sequence = entities.RLCS_Client_Paycode_Mapping_Details.Where(x => x.CPMD_ClientID.Equals(Client) && x.CPMD_Status == "A").Max(y => (int?)y.CPMD_Sequence_Order);
                    sequence = string.IsNullOrEmpty(Convert.ToString(sequence)) ? 0 : Convert.ToInt32(sequence);
                    return Convert.ToInt32(sequence) + 1;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }

        }
        public static int GetComplianceAssignment(string branchID)
        {
            int cnt = 0;
            try
            {
                List<Compliance> compliances = new List<Compliance>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstComplianceRegister = (from x in entities.RLCS_Register_Compliance_Mapping
                                                 where x.IsActive == true && x.Register_Status == "A"
                                                 select x.AVACOM_ComplianceID).ToList();
                    var lstComplianceReturns = (from x in entities.RLCS_Returns_Compliance_Mapping
                                                where x.IsActive == true && x.RM_Status == "A"
                                                select x.AVACOM_ComplianceID).ToList();
                    var lstComplianceChallan = (from x in entities.RLCS_Challan_Compliance_Mapping
                                                where x.IsActive == true
                                                select x.AVACOM_ComplianceID).ToList();
                    var lstComplianceID = lstComplianceRegister.Union(lstComplianceReturns).Union(lstComplianceChallan).Distinct().ToList();
                    foreach (var item in lstComplianceID)
                    {
                        Compliance objcompliance = new Compliance();
                        objcompliance.ID = Convert.ToInt64(item);
                        compliances.Add(objcompliance);
                    }

                    int customerBranchId = Convert.ToInt32(branchID);
                    cnt = (from c in compliances
                           join x in entities.ComplianceInstances on c.ID equals x.ComplianceId
                           join y in entities.ComplianceAssignments
                           on x.ID equals y.ComplianceInstanceID
                           where x.CustomerBranchID == customerBranchId
                           select x).Count();

                    return cnt;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return cnt;
            }
        }
        //public static int GetComplianceAssignment(string branchID)
        //{
        //    int cnt = 0;
        //    try
        //    {

        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            int customerBranchId = Convert.ToInt32(branchID);
        //             cnt = (from x in entities.ComplianceInstances
        //                       join y in entities.ComplianceAssignments
        //                       on x.ID equals y.ComplianceInstanceID
        //                       where x.CustomerBranchID == customerBranchId
        //                       select x).Count();

        //            return cnt;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return cnt;
        //    }
        //}


        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetClientsByCustomerId(int customerId)
        {
            List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstEntities = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstEntities = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                   where row.AVACOM_CustomerID == customerId
                                   && row.BranchType == "E"
                                   && row.CM_Status == "A"                                 
                                   select row).Distinct().ToList();

                   
                }

                return lstEntities;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstEntities;
            }
        }

        public static RLCS_Client_Paycode_Mapping_Details GetStandardHeaderData(int customerID, string clientid,string CPMD_Standard_Column,string mode,string headers)
        {
            RLCS_Client_Paycode_Mapping_Details standardHeaders = null;

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    if (headers != "")
                    {
                        if (mode == "S")//standard
                            standardHeaders = entities.RLCS_Client_Paycode_Mapping_Details.Where(t => t.AVACOM_CustomerID == customerID && t.CPMD_ClientID == clientid  && t.CPMD_PayGroup.ToLower() == "standard" && t.CPMD_Header.ToLower() == headers.ToLower()).FirstOrDefault();
                        else
                            standardHeaders = entities.RLCS_Client_Paycode_Mapping_Details.Where(t => t.AVACOM_CustomerID == customerID && t.CPMD_ClientID == clientid  && t.CPMD_PayGroup.ToLower() == "paycodes" && t.CPMD_Header.ToLower() == headers.ToLower()).FirstOrDefault();
                    }
                    else
                    {
                        if (mode == "S")//standard
                            standardHeaders = entities.RLCS_Client_Paycode_Mapping_Details.Where(t => t.AVACOM_CustomerID == customerID && t.CPMD_ClientID == clientid && t.CPMD_PayGroup.ToLower() == "standard" && t.CPMD_Standard_Column.ToLower() == CPMD_Standard_Column).FirstOrDefault();                  
                         else
                            standardHeaders = entities.RLCS_Client_Paycode_Mapping_Details.Where(t => t.AVACOM_CustomerID == customerID && t.CPMD_ClientID == clientid && t.CPMD_PayGroup.ToLower() == "paycodes" && t.CPMD_PayCode.ToLower() == CPMD_Standard_Column).FirstOrDefault();

                    }
                }

                return standardHeaders;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return standardHeaders;
            }
        }

        public static void Update_ProcessedStatus_Ctc(List<long> lstIDs, List<string> lstEMSDIDs, bool status)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstRecords = (from row in entities.Employee_CTC_Master
                                      where lstIDs.Contains(row.ECS_Id)
                                      select row).ToList();

                    var lstDetailsRecords = (from row in entities.Employee_CTC_Details
                                             where lstEMSDIDs.Contains(row.ECSD_ECS_Id)
                                             select row).ToList();


                    if (lstRecords != null && lstDetailsRecords != null)
                    {
                        lstRecords.ForEach(row => row.ISProcessed = status);
                        lstDetailsRecords.ForEach(row => row.ISProcessed = status);
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        public static List<RLCS_Client_Paycode_Mapping_Details> GetMappedPaycodesForClientsAndCustomers(int customerID,string clientid)
        {
            List<RLCS_Client_Paycode_Mapping_Details> lstCols = new List<RLCS_Client_Paycode_Mapping_Details>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                     lstCols = (from t in entities.RLCS_Client_Paycode_Mapping_Details
                                   where t.AVACOM_CustomerID == customerID && t.CPMD_ClientID == clientid && t.CPMD_Status == "A"
                                   select t).OrderBy(t => t.CPMD_Sequence_Order).ToList();

                    return lstCols;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstCols;
            }

        }
        public static List<RLCS_Employee_Master> GetEmployeeByCustomerAndClient(int customerID,string clientid)
        {
            List<RLCS_Employee_Master> lstRows = new List<RLCS_Employee_Master>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstRows = (from t in entities.RLCS_Employee_Master
                               where t.EM_Status == "A" && t.AVACOM_CustomerID == customerID && t.EM_ClientID == clientid
                               select t).ToList();

                    return lstRows;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstRows;
            }

        }

        public static RLCS_Client_Paycode_Mapping_Details GetMappedPaycodeDetails(int ID)
        {
            RLCS_Client_Paycode_Mapping_Details paycodeData = new RLCS_Client_Paycode_Mapping_Details();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    paycodeData = (from t in entities.RLCS_Client_Paycode_Mapping_Details
                                   where t.ID == ID
                                   select t).FirstOrDefault();

                    return paycodeData;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return paycodeData;
            }

        }

        //public static List<unprocessedList> GetUnprocessedMonthlyCtcDetailsRecords()
        //{
        //    List<unprocessedList> EntityList = new List<unprocessedList>();

        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {

        //            EntityList= entities.Database.SqlQuery<unprocessedList>("Exec dbo.SP_GetUnprocessedMonthlyCtcDetailsRecords").ToList<unprocessedList>();
        //        }
        //        return EntityList;
        //    }
        //    catch (Exception ex)
        //    {
        //        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return EntityList;
        //    }
        //}


        #endregion
    }

}
