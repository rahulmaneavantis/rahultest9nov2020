﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class UserCustomerMappingManagement
    {
        public static bool CreateUpdate_UserCustomerMapping(UserCustomerMapping _objRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.UserCustomerMappings
                                      where row.UserID == _objRecord.UserID
                                      && row.CustomerID == _objRecord.CustomerID
                                      && row.ProductID==_objRecord.ProductID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.MgrID = _objRecord.MgrID;
                        prevRecord.ProductID = _objRecord.ProductID;
                        prevRecord.IsActive = _objRecord.IsActive;
                        prevRecord.UpdatedOn = DateTime.Now;

                        saveSuccess = true;
                    }
                    else
                    {
                        _objRecord.CreatedOn = DateTime.Now;
                        entities.UserCustomerMappings.Add(_objRecord);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdate_UserCustomerMapping_Multiple(List<UserCustomerMapping> objUserCustMapping)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objUserCustMapping.ForEach(entry =>
                    {
                        var prevRecord = (from row in entities.UserCustomerMappings
                                          where row.UserID == entry.UserID
                                          && row.CustomerID == entry.CustomerID
                                          select row).FirstOrDefault();

                        if (prevRecord != null)
                        {
                            prevRecord.IsActive = true;
                            prevRecord.UpdatedOn = DateTime.Now;
                        }
                        else
                        {
                            entry.CreatedOn = DateTime.Now;
                            entities.UserCustomerMappings.Add(entry);
                        }
                    });

                    if (Convert.ToInt32(entities.SaveChanges()) > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<SP_GETUserCustomerMapping_ALL_Result> GetUserCustomerMappingList(int userID, int customerID, int serviceProviderID, int distributorID, int mgrID)
        {
            List<SP_GETUserCustomerMapping_ALL_Result> UserCustomerMappingList = new List<SP_GETUserCustomerMapping_ALL_Result>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    UserCustomerMappingList = (entities.SP_GETUserCustomerMapping_ALL(Convert.ToInt32(userID), Convert.ToInt32(customerID), serviceProviderID, distributorID, mgrID)).OrderBy(row => row.CustomerName).ToList();

                    //if (UserCustomerMappingList.Count > 0)
                    //{
                    //    if (mgrID != -1)
                    //        UserCustomerMappingList = UserCustomerMappingList.Where(row => row.MgrID == mgrID).ToList();
                    //}
                }

                return UserCustomerMappingList;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }


        //public static List<object> GetAll_Users(int customerID, int serviceProviderID, int distributorID, string roleCode)
        //{
        //    List<object> query = new List<object>();

        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        if (serviceProviderID != -1)
        //        {
        //            query = (from U in entities.Users
        //                     join CUST in entities.Customers on U.CustomerID equals CUST.ID
        //                     join rowRole in entities.Roles on U.RoleID equals rowRole.ID
        //                     where U.IsDeleted == false
        //                     && U.IsActive == true
        //                     && (CUST.ServiceProviderID == serviceProviderID || CUST.ID == serviceProviderID)
        //                     select new
        //                     {
        //                         ID = U.ID,
        //                         ParentID = CUST.ParentID,
        //                         CustomerID = U.CustomerID,
        //                         roleCode = rowRole.ID
        //                     }).ToList<object>();
        //        }
        //        else if (distributorID != -1)
        //        {
        //            query = (from U in entities.Users
        //                     join CUST in entities.Customers on U.CustomerID equals CUST.ID
        //                     join rowRole in entities.Roles on U.RoleID equals rowRole.ID
        //                     where U.IsDeleted == false
        //                     && U.IsActive == true
        //                     && (CUST.ParentID == distributorID || CUST.ID == distributorID)
        //                     select new
        //                     {
        //                         ID = U.ID,
        //                         ParentID = CUST.ParentID,
        //                         CustomerID = U.CustomerID,
        //                         roleCode = rowRole.ID
        //                     }).ToList<object>();
        //        }
        //        else
        //        {
        //            query = (from U in entities.Users
        //                     join CUST in entities.Customers on U.CustomerID equals CUST.ID
        //                     join rowRole in entities.Roles on U.RoleID equals rowRole.ID
        //                     where U.IsDeleted == false
        //                     && U.IsActive == true
        //                     select new
        //                     {
        //                         ID = U.ID,
        //                         ParentID = CUST.ParentID,
        //                         CustomerID = U.CustomerID,
        //                         roleCode = rowRole.ID
        //                     }).ToList<object>();
        //        }

        //        if (customerID != -1)
        //            query = query.Where(row => row.CustomerID == customerID).ToList<object>();

        //        if (!string.IsNullOrEmpty(roleCode))
        //            query = query.Where(row => row.roleCode == customerID).ToList<object>();

        //        var users = (from row in query
        //                     select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).Distinct().ToList<object>();

        //        return users;
        //    }
        //}

        public static List<SP_RLCS_GetAllUser_ServiceProviderDistributor_Result> GetAll_Users(int customerID, int serviceProviderID, int distributorID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstRecords = (entities.SP_RLCS_GetAllUser_ServiceProviderDistributor(serviceProviderID, distributorID, customerID)).ToList();

                return lstRecords;
            }
        }

        public static List<object> GetAllUser_ServiceProviderDistributorCustomer(int customerID, int serviceProviderID, int distributorID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstRecords = (entities.SP_RLCS_GetAllUser_ServiceProviderDistributor(serviceProviderID, distributorID, customerID)).ToList();

                var userList = (from row in lstRecords
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).Distinct().ToList<object>();

                return userList;
            }
        }

        public static bool Delete_UserCustomerMapping(long recordID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //User userToDelete = new User() { ID = userID };
                    //entities.Users.Attach(userToDelete);
                    UserCustomerMapping userToDelete = (from row in entities.UserCustomerMappings
                                                        where row.ID == recordID
                                                        select row).FirstOrDefault();

                    if (userToDelete != null)
                    {
                        // entities.UserCustomerMappings.Remove(userToDelete);
                        userToDelete.IsActive = false;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static UserCustomerMapping GetRecord_UserCustomerMapping(long recordID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    UserCustomerMapping userToDelete = (from row in entities.UserCustomerMappings
                                                        where row.ID == recordID
                                                        select row).FirstOrDefault();

                    return userToDelete;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<Customer> Get_UserCustomerMapping(int userID)
        {
            try
            {
                List<Customer> lstAssignedCustomers = new List<Customer>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    lstAssignedCustomers = (from UCM in entities.UserCustomerMappings
                                            join CUST in entities.Customers
                                            on UCM.CustomerID equals CUST.ID
                                            where UCM.IsActive == true
                                            && CUST.IsDeleted == false
                                            && CUST.Status == 1
                                            && (UCM.UserID == userID || UCM.MgrID == userID)
                                            //&& UCM.CustomerID == customerID
                                            select CUST).ToList();


                    return lstAssignedCustomers.Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<USP_RLCS_GetUserCustomerMapping_Result> Get_UserCustomerMapping(int userID, string roleCode)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstAssignedCustomers = entities.USP_RLCS_GetUserCustomerMapping(userID, roleCode).ToList();

                    return lstAssignedCustomers.Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<SP_RLCS_GetAssignedSPOC_Result> Get_AssignedSPOC(int userID, int distID, string roleCode)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstAssignedSPOCs = (from row in entities.SP_RLCS_GetAssignedSPOC(userID, roleCode, distID)
                                            select row).ToList();

                    return lstAssignedSPOCs;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<SP_RLCS_GetAssignedCustomers_Result> Get_AssignedCustomers(int userID, int distID, string roleCode)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstAssignedSPOCs = (from row in entities.SP_RLCS_GetAssignedCustomers(userID, roleCode, distID)
                                            select row).ToList();

                    return lstAssignedSPOCs;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static bool DeActivate_UserCustomerMapping_Multiple(int customerID, int productID)
        {
            bool saveSuccess = true;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevMappingRecords = (from row in entities.UserCustomerMappings
                                              where row.CustomerID == customerID
                                              && row.ProductID == productID
                                              select row).ToList();

                    if (prevMappingRecords != null)
                    {
                        if (prevMappingRecords.Count > 0)
                        {
                            prevMappingRecords.ForEach(row => row.IsActive = false);
                            prevMappingRecords.ForEach(row => row.UpdatedOn = DateTime.Now);

                            if (Convert.ToInt32(entities.SaveChanges()) > 0)
                            {
                                saveSuccess = true;
                            }
                            else
                            {
                                saveSuccess = false;
                            }
                        }
                    }

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<int> GetRecord_Users(int custID, int prodID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var assignedUsers = (from row in entities.UserCustomerMappings
                                         where row.CustomerID == custID
                                         && row.ProductID == prodID
                                         && row.IsActive == true
                                         select row.UserID).ToList();

                    return assignedUsers;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
    }
}
