﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class RLCS_ApplicabilityCheck
    {
        public static bool Exists_Customer_TempCustomer(TempCustomer customer)
        {
            bool sameNameExists = true;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (customer.ID != 0)
                    {
                        sameNameExists = (from row in entities.TempCustomers
                                          where row.IsDeleted == false
                                          && row.ID != customer.ID
                                          && row.Name.Trim().ToUpper().Equals(customer.Name.Trim().ToUpper())
                                          select row).Count() > 0;
                    }
                    else
                    {
                        sameNameExists = (from row in entities.TempCustomers
                                          where row.IsDeleted == false                                          
                                          && row.Name.Trim().ToUpper().Equals(customer.Name.Trim().ToUpper())
                                          select row).Count() > 0;
                    }
                    if (!sameNameExists)
                    {
                        if (customer.AVACOM_CustomerID != null)
                        {
                            sameNameExists = (from row in entities.Customers
                                              where row.IsDeleted == false
                                               && row.ID != customer.AVACOM_CustomerID
                                              && row.Name.Trim().ToUpper().Equals(customer.Name.Trim().ToUpper())
                                              select row).Count() > 0;
                        }
                        else
                        {
                            sameNameExists = (from row in entities.Customers
                                              where row.IsDeleted == false
                                             
                                              && row.Name.Trim().ToUpper().Equals(customer.Name.Trim().ToUpper())
                                              select row).Count() > 0;
                        }
                    }
                    return sameNameExists;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return sameNameExists;
            }
        }

        public static bool CreateUpdate_TempCustomer(TempCustomer customerRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (customerRecord.ID != null && customerRecord.ID != 0)
                    {
                        var prevRecord = (from row in entities.TempCustomers
                                          where row.ID == customerRecord.ID
                                          select row).FirstOrDefault();

                        if (prevRecord != null)
                        {
                            prevRecord.Name = customerRecord.Name;
                            prevRecord.BuyerName = customerRecord.BuyerName;
                            prevRecord.BuyerEmail = customerRecord.BuyerEmail;
                            prevRecord.BuyerContactNumber = customerRecord.BuyerContactNumber;
                            prevRecord.Address = customerRecord.Address;

                            prevRecord.UpdatedBy = customerRecord.UpdatedBy;
                            prevRecord.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }
                    }
                    else
                    {
                        customerRecord.IsDeleted = false;
                        customerRecord.CreatedOn = DateTime.Now;

                        entities.TempCustomers.Add(customerRecord);
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool Update_TempCustomer_SetAvacomCustomerID(int recordID, int avacomCustID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (recordID != 0)
                    {
                        var record = (from row in entities.TempCustomers
                                      where row.ID == recordID
                                      select row).FirstOrDefault();

                        if (record != null)
                        {
                            record.AVACOM_CustomerID = avacomCustID;
                            entities.SaveChanges();
                        }
                    }
                    
                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static TempCustomer GetDetails(int recordID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var record = (from row in entities.TempCustomers
                                      where row.ID == recordID                                     
                                      select row).FirstOrDefault();
                   
                    return record;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

    }
}
