﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class RLCS_Assesment
    {

        public static List<RLCS_AssesmentVM> FillAssessmentDetails(RLCS_AssesmentVM Input)
        {
            List<RLCS_AssesmentVM> customerBranches = new List<RLCS_AssesmentVM>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                customerBranches = (from records in entities.RLCS_CustomerAssessmentDetails
                                    where records.CustomerID == Input.CustomerID && records.BranchID == Input.BranchID
                                    select new RLCS_AssesmentVM
                                    {
                                        Type = records.BranchType,
                                        Location = records.Location,
                                        State = records.State,
                                        MinBasic = records.MinimumBasicSalary,
                                        EmpCount = records.EmployeeCount,
                                        MinGross = records.MinimumGrossSalary,
                                        MaxGross = records.MaximumGrossSalary,
                                        ParentBranchID = records.ParentBranchID,
                                        BranchID = records.BranchID
                                    }).ToList();

            }
            return customerBranches;


        }

        public static List<RLCS_AssesmentVM> RLCSAssesmentGridBind(int CustomerId, int ParentBranchId, int BranchID)
        {

            IQueryable<RLCS_AssesmentVM> branches; // = new IQueryable<RLCS_AssesmentVM>;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (ParentBranchId > 0)
                {
                    branches = (from records in entities.RLCS_CustomerAssessmentDetails
                                join records2 in entities.RLCS_CustomerAssessmentDetails
                                on records.ParentBranchID equals records2.BranchID into g
                                from tps in g.DefaultIfEmpty()
                                where records.CustomerID == CustomerId && records.BranchID == BranchID && records.IsActive == true  //&&records.ParentBranchID== ParentBranchId
                                select new RLCS_AssesmentVM
                                {
                                    Type = records.LocationType,
                                    Location = records.Location,
                                    State = records.State,
                                    MinBasic = records.MinimumBasicSalary,
                                    EmpCount = records.EmployeeCount,
                                    MinGross = records.MinimumGrossSalary,
                                    MaxGross = records.MaximumGrossSalary,
                                    ParentBranchID = records.ParentBranchID,
                                    BranchID = records.BranchID,
                                    parentLocation = tps.Location,
                                    BranchType = records.BranchType

                                });

                }
                else
                {
                    branches = (from records in entities.RLCS_CustomerAssessmentDetails
                                join records2 in entities.RLCS_CustomerAssessmentDetails
                                on records.ParentBranchID equals records2.BranchID
                                where records.CustomerID == CustomerId && records.ParentBranchID == BranchID && records.IsActive == true
                                select new RLCS_AssesmentVM
                                {
                                    Type = records.LocationType,
                                    Location = records.Location,
                                    State = records.State,
                                    MinBasic = records.MinimumBasicSalary,
                                    EmpCount = records.EmployeeCount,
                                    MinGross = records.MinimumGrossSalary,
                                    MaxGross = records.MaximumGrossSalary,
                                    ParentBranchID = records.ParentBranchID,
                                    BranchID = records.BranchID,
                                    parentLocation = records2.Location,
                                    BranchType = records.BranchType

                                });

                }

                return branches.ToList();
            }
        }

        public static string RLCSAssesmentParentName(int CustomerId, int ParentBranchId, int BranchID)
        {
            string customerBranches = "";

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                customerBranches = (from records in entities.RLCS_CustomerAssessmentDetails
                                    where records.CustomerID == CustomerId && records.BranchID == BranchID
                                    select records.Location
                                          ).SingleOrDefault();

            }
            return customerBranches;


        }

    }



    public partial class RLCS_AssesmentVM
    {
        public int? CustomerID { get; set; }
        public int BranchID { get; set; }
        public Nullable<int> ParentBranchID { get; set; }
        public string Type { get; set; }
        public Nullable<int> AVACOM_BranchID { get; set; }
        public string State { get; set; }
        public string Location { get; set; }
        public string LocationType { get; set; }
        public Nullable<int> EmpCount { get; set; }
        public Nullable<decimal> MinBasic { get; set; }
        public Nullable<decimal> MinGross { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<decimal> MaxGross { get; set; }
        public string parentLocation { get; set; }
        public string BranchType { get; set; }
    }
}
