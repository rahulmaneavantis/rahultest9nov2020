﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Net.Mime;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class EmailManager
    {
        public static void SendMail(string from, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<Stream, string>> attachment)
        {
            string PortalURL = ConfigurationManager.AppSettings["PortalURL"];
            if (!string.IsNullOrEmpty(PortalURL) && !string.IsNullOrEmpty(message))
            {
                if (message.Contains(PortalURL))
                    message = UpdatePortalURL(to, PortalURL, message);
            }

            using (SmtpClient email = new SmtpClient())
            using (MailMessage mailMessage = new MailMessage())
            {
                email.DeliveryMethod = SmtpDeliveryMethod.Network;
                email.UseDefaultCredentials = false;
                NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                email.Credentials = credential;
                email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                email.Timeout = 60000;
                email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                MailMessage oMessage = new MailMessage();
                string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                mailMessage.From = new MailAddress(FromEmailId, "Avantis Regtech");
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                if (attachment.Count > 0)
                {
                    foreach (var attach in attachment)
                    {
                        Attachment data = new Attachment(attach.Item1, attach.Item2);
                        mailMessage.Attachments.Add(data);
                    }
                }
                if (to != null)
                    to.ForEach(entry => mailMessage.To.Add(entry));
                if (cc != null)
                    cc.ForEach(entry => mailMessage.CC.Add(entry));
                if (bcc != null)
                    bcc.ForEach(entry => mailMessage.Bcc.Add(entry));

                //email.Send(mailMessage);
            }
        }
        public static void SendMail(string from, List<string> to, List<string> cc, List<string> bcc, string subject, string message)
        {
            string PortalURL = ConfigurationManager.AppSettings["PortalURL"];

            if (!string.IsNullOrEmpty(PortalURL) && !string.IsNullOrEmpty(message))
            {
                if (message.Contains(PortalURL))
                    message = UpdatePortalURL(to, PortalURL, message);
            }

            using (SmtpClient email = new SmtpClient())
            using (MailMessage mailMessage = new MailMessage())
            {
                email.DeliveryMethod = SmtpDeliveryMethod.Network;
                email.UseDefaultCredentials = false;
                NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                email.Credentials = credential;
                email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                email.Timeout = 60000;
                email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                MailMessage oMessage = new MailMessage();
                string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                mailMessage.From = new MailAddress(FromEmailId, "Avantis Regtech");
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                {
                    if (to != null)
                        to.ForEach(entry => mailMessage.To.Add(entry));
                    if (cc != null)
                        cc.ForEach(entry => mailMessage.CC.Add(entry));
                    if (bcc != null)
                        bcc.ForEach(entry => mailMessage.Bcc.Add(entry));
                }

                //email.Send(mailMessage);
            }
        }
        public static void SendMail(string from, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Attachment> attachment)
        {
            string PortalURL = ConfigurationManager.AppSettings["PortalURL"];

            if (!string.IsNullOrEmpty(PortalURL) && !string.IsNullOrEmpty(message))
            {
                if (message.Contains(PortalURL))
                    message = UpdatePortalURL(to, PortalURL, message);
            }

            using (SmtpClient email = new SmtpClient())
            using (MailMessage mailMessage = new MailMessage())
            {
                email.DeliveryMethod = SmtpDeliveryMethod.Network;
                email.UseDefaultCredentials = false;
                NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                email.Credentials = credential;
                email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                email.Timeout = 60000;
                email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                MailMessage oMessage = new MailMessage();
                string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                mailMessage.From = new MailAddress(FromEmailId, "Avantis Regtech");
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                if (attachment.Count > 0)
                {
                    foreach (var attach in attachment)
                    {
                        mailMessage.Attachments.Add(attach);
                    }
                }
                if (to != null)
                    to.ForEach(entry => mailMessage.To.Add(entry));
                if (cc != null)
                    cc.ForEach(entry => mailMessage.CC.Add(entry));
                if (bcc != null)
                    bcc.ForEach(entry => mailMessage.Bcc.Add(entry));

                //email.Send(mailMessage);
            }
        }

        public static string UpdatePortalURL(List<string> to, string oldPortalURL, string messageBody)
        {
            string userEmail = string.Empty;
            string newPortalURL = string.Empty;

            if (to.Count > 0)
                userEmail = to[0];

            if (!string.IsNullOrEmpty(userEmail))
            {
                var customerRecord = UserManagement.GetCustomerRecordByUserEmail(userEmail);

                if (customerRecord != null)
                {
                    int registrationBy = -1;

                    if (customerRecord.RegistrationBy != null)
                    {
                        registrationBy = Convert.ToInt32(customerRecord.RegistrationBy);

                        if (registrationBy != -1)
                        {
                            if (registrationBy == 1)
                                newPortalURL = "https://icai.avantis.co.in";
                            else if (registrationBy == 2)
                                newPortalURL = "https://icsi.avantis.co.in";
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(newPortalURL))
            {
                messageBody = messageBody.Replace(oldPortalURL, newPortalURL);
            }

            return messageBody;
        }
    }
}
