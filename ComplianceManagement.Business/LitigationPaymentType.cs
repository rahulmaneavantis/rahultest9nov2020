﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class LitigationPaymentType
    {
        public static tbl_PaymentType PaymentMasterGetByID(int paymentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MstDepartmentMaster = (from row in entities.tbl_PaymentType
                                           where row.ID == paymentID
                                           select row).SingleOrDefault();

                return MstDepartmentMaster;
            }
        }

        public static void DeletePaymentMaster(int paymentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                tbl_PaymentType ObjPay = (from row in entities.tbl_PaymentType
                                          where row.ID == paymentID
                                          && row.IsDeleted == false
                                          select row).FirstOrDefault();

                ObjPay.IsDeleted = true;
                entities.SaveChanges();
            }
        }

        public static bool PaymentExists(tbl_PaymentType paymentmaster)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.tbl_PaymentType
                             where row.TypeName.ToUpper().Equals(paymentmaster.TypeName.ToUpper()) && row.IsDeleted == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreatePaymentMaster(tbl_PaymentType paymentmaster)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.tbl_PaymentType.Add(paymentmaster);
                entities.SaveChanges();
            }
        }

        public static List<State> getParticularLists(int CustomerId)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var stateList = (from row in entities.States
                                     select row).ToList();
                    return stateList;
                }
                }
            catch(Exception ex)
            {
                return null;
            }
        }

        public static void UpdatePaymentMaster(tbl_PaymentType paymentmaster)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                tbl_PaymentType objPayementUpdate = (from row in entities.tbl_PaymentType
                                                     where row.ID == paymentmaster.ID
                                                        && row.IsDeleted == false
                                                 select row).FirstOrDefault();

                objPayementUpdate.TypeName = paymentmaster.TypeName;
                entities.SaveChanges();
            }
        }

        public static List<tbl_PaymentType> GetAllPaymentMasterList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjPayment = (from row in entities.tbl_PaymentType
                                  where row.IsDeleted == false
                                        select row).OrderBy(entry => entry.TypeName);

                return ObjPayment.ToList();
            }
        }

        //Ruchi Code Start
        
        public static List<SP_tbl_CasePay_HearingDtls_Result> GetAllHearingIDList(int CaseInstantID, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var result = entities.SP_tbl_CasePay_HearingDtls(CaseInstantID, CustomerID).ToList();
                return result;
            }
        }
        
        public static List<SP_Litiation_BindHearingRefNo_Result> GetAllHearingIDList(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var result = entities.SP_Litiation_BindHearingRefNo(userID).ToList();
                return result;
            }
        }

        public static List<object> getLawyerList(int customerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var result = (from row in entities.Users
                                  where row.CustomerID == customerID
                                  && row.LitigationRoleID != 0 
                                  && row.LawyerFirmID != null
                                  && row.IsActive == true
                                  && row.IsDeleted == false
                                  select row).ToList();
                    var lstUsers = (from row in result
                                    select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();
                    return lstUsers;
                }
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public static string getLawyerList(int customerID,string email)
        {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var result = (from row in entities.Users
                                  where row.CustomerID == customerID
                                  where row.Email.ToUpper().Trim() == email.ToUpper().Trim()
                                 && row.LitigationRoleID != 0
                                  && row.LawyerFirmID != null
                                  && row.IsActive == true
                                  && row.IsDeleted == false
                                  select row.FirstName + " " + row.LastName).FirstOrDefault();
                    return result;
                }
        }


        //End Code
    }
}
