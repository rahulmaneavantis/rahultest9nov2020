//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataPract
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalesExpensesTransactionDocument
    {
        public int DocumentTransactionID { get; set; }
        public int TransactionDetailID { get; set; }
        public string FileName { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
    
        public virtual SalesExpensesTransactionDetail SalesExpensesTransactionDetail { get; set; }
    }
}
