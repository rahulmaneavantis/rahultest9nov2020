//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataPract
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalesExpensesTransactionDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SalesExpensesTransactionDetail()
        {
            this.SalesExpensesTransactionDocuments = new HashSet<SalesExpensesTransactionDocument>();
            this.SaleBillApprovalTransactoins = new HashSet<SaleBillApprovalTransactoin>();
        }
    
        public int TransactionDetailID { get; set; }
        public int TransactionID { get; set; }
        public int SalesExpID { get; set; }
        public Nullable<System.DateTime> TranscationDetailDate { get; set; }
        public Nullable<int> Amount { get; set; }
        public Nullable<bool> IsProofDoc { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> ActionTypeID { get; set; }
        public bool IsDeleted { get; set; }
        public string Remark { get; set; }
    
        public virtual SalesExpens SalesExpens { get; set; }
        public virtual SalesExpensesTransaction SalesExpensesTransaction { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalesExpensesTransactionDocument> SalesExpensesTransactionDocuments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaleBillApprovalTransactoin> SaleBillApprovalTransactoins { get; set; }
    }
}
