﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.DataPract
{
    public class PTCustomerManagement
    {
        public static List<TM_tbl_ServiceDetails> GetAllServiceDetails()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var clist = (from row in entities.TM_tbl_ServiceDetails
                             select row);

                return clist.ToList();
            }
        }
        public static List<TM_tbl_Frequency> GetAllFrequency()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var clist = (from row in entities.TM_tbl_Frequency                             
                             select row);
                             
                return clist.ToList();
            }
        }
        public static TM_TaxDetails GetByID(int tid)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var user = (from row in entities.TM_TaxDetails
                            where row.ID == tid
                            select row).FirstOrDefault();

                return user;
            }
        }
        public static List<TM_Sp_TaxDetails_Result> GetTaxDetailsAll(int sid, string filter = null)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var users = (from row in entities.TM_Sp_TaxDetails(sid)
                             select row);
                
                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.RegNo.Contains(filter) || entry.TaxType.Contains(filter) || entry.TaxType.Contains(filter) || entry.CustomerName.Contains(filter));
                }

                return users.ToList();


            }
        }
        public static void CreateUpdate_TAXDetailsData(TM_TaxDetails lstTD)
        {
            try
            {                
                using (PracticeEntities entities = new PracticeEntities())
                {                    
                        var prevRecord = (from row in entities.TM_TaxDetails
                                          where row.CustomerId == lstTD.CustomerId
                                          && row.RegNo.Trim().ToUpper().Equals(lstTD.RegNo.Trim().ToUpper())
                                          && row.TaxType.Trim().ToUpper().Equals(lstTD.TaxType.Trim().ToUpper())
                                          select row).FirstOrDefault();
                    if (prevRecord != null)
                    {                        
                        prevRecord.RegNo = lstTD.RegNo;                      
                        prevRecord.IsActive = false;
                        prevRecord.Updatedby = lstTD.Updatedby;
                        prevRecord.UpdatedOn = DateTime.Now;                      
                    }
                    else
                    {
                        prevRecord.IsActive = false;
                        lstTD.CreatedOn = DateTime.Now;
                        lstTD.CreatedBy = lstTD.CreatedBy;
                        entities.TM_TaxDetails.Add(lstTD);                     
                    }                   
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "CreateUpdate_TAXDetailsData";
                msg.FunctionName = "CreateUpdate_TAXDetailsData";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);
            }
        }
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();
                });
            }
        }

        public static List<Customer> GetAllGroupCustomers( int distributorid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var clist = (from row in entities.Customers
                             where row.IsDeleted == false
                              && row.ComplianceProductType != 1
                             select row);
                clist = clist.Where(entry => entry.ParentID == distributorid);
                return clist.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<Customer> GetAllCustomers(int customerID, string rolecode, int distributorid, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var clist = (from row in entities.Customers
                             where row.IsDeleted == false
                              && row.ComplianceProductType != 1
                             select row);

                if (rolecode == "DADMN")
                {
                    clist = clist.Where(entry => entry.ParentID == distributorid);
                }
                else
                {
                    if (customerID != -1)
                    {
                        clist = clist.Where(entry => entry.ID == customerID);
                    }
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    clist = clist.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter) || entry.BuyerContactNumber.Contains(filter));
                }

                return clist.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static object FillServiceProvider(int serviceProviderID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                             && row.IsServiceProvider == true
                             select row);

                if (serviceProviderID != -1)
                    query = query.Where(row => row.ID == serviceProviderID);

                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object Fill_ServiceProviders_Distributors(int IsSPDist, int serviceProviderID, int distributorID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                IQueryable<Customer> query = null;

                if (IsSPDist == 0)
                {
                    query = (from row in entities.Customers
                             where row.IsDeleted == false
                             && row.IsServiceProvider == true
                             select row);
                }
                else if (IsSPDist == 1)
                {
                    query = (from row in entities.Customers
                             where row.IsDeleted == false
                             && row.IsDistributor == true
                             select row);
                }
                else if (IsSPDist == 2)
                {
                    query = (from row in entities.Customers
                             where row.IsDeleted == false
                             && (row.IsServiceProvider == true || row.IsDistributor == true)
                             select row);
                }

                if (serviceProviderID != -1)
                    query = query.Where(row => row.ID == serviceProviderID);

                if (distributorID != -1)
                    query = query.Where(row => row.ID == distributorID);

                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static void AuditDelete(int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Customer customerToDelete = (from row in entities.mst_Customer
                                                 where row.ID == customerID
                                                 select row).FirstOrDefault();

                customerToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }
        public static void Delete(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Customer customerToDelete = (from row in entities.Customers
                                             where row.ID == customerID
                                             select row).FirstOrDefault();

                customerToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }

        public static bool CustomerDelete(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var exists = (from row in entities.ComplianceInstances
                              join row1 in entities.CustomerBranches
                              on row.CustomerBranchID equals row1.ID
                              where row1.CustomerID == CustomerID
                              select true).FirstOrDefault();

                return !exists;
            }
        }
     
        public static int? GetServiceProviderID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var serviceProviderID = (from row in entities.Customers
                                         where row.ID == customerID
                                         select row.ServiceProviderID).FirstOrDefault();

                return serviceProviderID;
            }
        }

        public static List<Customer> GetAll_CustomersByServiceProviderOrDistributor(int customerID, int serviceProviderID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers    
                                 where row.IsDeleted==false                            
                                 select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    customers = customers.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter));
                }

                if (serviceProviderID != -1)
                    customers = customers.Where(entry => entry.ServiceProviderID == serviceProviderID);


                if (customerID != -1)
                    customers = customers.Where(entry => entry.ID == customerID);

                if (customers.Count() > 0)
                    customers = customers.OrderBy(row => row.Name);

                return customers.ToList();
            }
        }

        public static List<Customer> GetAll(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             where row.IsDeleted == false
                              && row.Status == 1
                             select row);

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.ID == customerID);
                }


                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter) || entry.BuyerContactNumber.Contains(filter));
                }

                return users.OrderBy(entry => entry.Name).ToList();
            }
        }
    }
}
