//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataPract
{
    using System;
    
    public partial class TM_GetActivityDetails_Result
    {
        public int ID { get; set; }
        public string ActivityName { get; set; }
        public string Name { get; set; }
        public int ProductID { get; set; }
    }
}
