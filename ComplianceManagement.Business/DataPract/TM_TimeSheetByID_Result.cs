//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataPract
{
    using System;
    
    public partial class TM_TimeSheetByID_Result
    {
        public int TimeSheetID { get; set; }
        public System.DateTime Date { get; set; }
        public Nullable<int> Duration { get; set; }
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public string ProjectName { get; set; }
        public string ActivityName { get; set; }
        public string Description { get; set; }
        public int ProjectID { get; set; }
        public int ActivityID { get; set; }
        public Nullable<long> legalentityid { get; set; }
    }
}
