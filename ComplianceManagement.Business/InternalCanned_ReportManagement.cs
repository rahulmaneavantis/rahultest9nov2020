﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class InternalCanned_ReportManagement
    {
        public static List<InternalComplianceInstanceCheckListTransactionView> GetChecklistCannedReportDataForPerformer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();


                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8 || RoleID == 9)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(ActID))).ToList();
                }

                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;


                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
            }

        }

        public static List<InternalComplianceInstanceCheckListTransactionView> GetCheckListCannedReportDataForReviewer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();



                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8 || RoleID == 9)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                         on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID)).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToInt32(type))).ToList();
                }

                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToInt32(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;


                }

                return transactionsQuery.OrderBy(entry => entry.InternalScheduledOn).ToList();
            }
        }

        public static List<InternalComplianceInstanceCheckListTransactionView> GetCheckListCannedReportDataForReviewer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int actid, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();



                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8 || RoleID == 9)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                         on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID)).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }

                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                if (actid != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(actid))).ToList();
                }
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;


                }

                return transactionsQuery.OrderBy(entry => entry.InternalScheduledOn).ToList();
            }
        }

        //public static List<InternalComplianceInstanceCheckListTransactionView> GetChecklistCannedReportDataForPerformer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        int performerRoleID = (from row in entities.Roles
        //                               where row.Code == "PERF"
        //                               select row.ID).Single();

        //        long RoleID = (from row in entities.Users
        //                       where row.ID == userID
        //                       select row.RoleID).Single();

        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();


        //        var GetApprover = (from row in entities.InternalComplianceAssignments
        //                           where row.UserID == userID
        //                           select row)
        //                           .GroupBy(a => a.RoleID)
        //                           .Select(a => a.FirstOrDefault())
        //                           .Select(entry => entry.RoleID).ToList();

        //        if (GetApprover.Count == 1 && GetApprover.Contains(6))
        //            RoleID = 6;

        //        if (RoleID == 8 || RoleID == 9)
        //        {
        //            transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
        //                                 join row1 in entities.EntitiesAssignmentInternals
        //                                  on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
        //                                 where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
        //                                 && row1.UserID == userID && row.RoleID == 3
        //                                 select row).Distinct().ToList();
        //        }
        //        else
        //        {
        //            transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
        //                                 where row.CustomerID == Customerid
        //                                 select row).ToList();

        //            transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
        //        }

        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
        //        }
        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
        //        }

        //        //Datr Filter
        //        if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
        //        }

        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }

        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
        //        }

        //        if (ActID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(ActID))).ToList();
        //        }

        //        switch (status)
        //        {
        //            case CannedReportFilterNewStatus.Upcoming:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.Overdue:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.ClosedTimely:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.ClosedDelayed:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.PendingForReview:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.Rejected:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
        //                break;


        //        }
        //        // Find data through String contained in Description
        //        if (StringType != "")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
        //        }

        //        return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
        //    }

        //}

        public static List<InternalComplianceInstanceCheckListTransactionView> GetCannedReportInternalDataForPerformer(int Customerid, int userID, int risk, CheckListCannedReportPerformer status, int location, int type, int category, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();

                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long) row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CheckListCannedReportPerformer.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CheckListCannedReportPerformer.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;
                    case CheckListCannedReportPerformer.NotApplicable:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 15).ToList();
                        break;
                    case CheckListCannedReportPerformer.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                
                }
               

                return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
            }

        }


        public static List<InternalComplianceInstanceCheckListTransactionView> GetCannedReportInternalDataForPerformerAddedNotCompliedStatus(int Customerid, int userID, int risk, CheckListCannedReportPerformerAddedNotCompliedStatus status, int location, int type, int category, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();

                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CheckListCannedReportPerformerAddedNotCompliedStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CheckListCannedReportPerformerAddedNotCompliedStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;
                    case CheckListCannedReportPerformerAddedNotCompliedStatus.NotApplicable:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 15).ToList();
                        break;
                    case CheckListCannedReportPerformerAddedNotCompliedStatus.NotComplied:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 17).ToList();
                        break;
                    case CheckListCannedReportPerformerAddedNotCompliedStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;
                }
                return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
            }

        }

        public static List<InternalComplianceInstanceCheckListTransactionView> GetCannedReportInternalDataForReviewer(int Customerid, int userID, int risk, CheckListCannedReportPerformer status, int location, int type, int category, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "RVW1"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();

                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long) row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 4
                                         select row).Distinct().ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CheckListCannedReportPerformer.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CheckListCannedReportPerformer.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;
                    case CheckListCannedReportPerformer.NotApplicable:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 15)).ToList();
                        break;
                    case CheckListCannedReportPerformer.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                   
                }
                

                return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
            }

        }

        public static List<InternalComplianceInstanceCheckListTransactionView> GetCannedReportInternalDataForReviewerAddedNotCompliedStatus(int Customerid, int userID, int risk, CheckListCannedReportPerformerAddedNotCompliedStatus status, int location, int type, int category, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "RVW1"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceCheckListTransactionView> transactionsQuery = new List<InternalComplianceInstanceCheckListTransactionView>();

                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 4
                                         select row).Distinct().ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CheckListCannedReportPerformerAddedNotCompliedStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CheckListCannedReportPerformerAddedNotCompliedStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;
                    case CheckListCannedReportPerformerAddedNotCompliedStatus.NotApplicable:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 15)).ToList();
                        break;
                    case CheckListCannedReportPerformerAddedNotCompliedStatus.NotComplied:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 17)).ToList();
                        break;
                    case CheckListCannedReportPerformerAddedNotCompliedStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;
                }
                return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
            }

        }

        public static List<InternalComplianceInstanceTransactionView> GetCannedReportDataForPerformer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();


                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8 || RoleID == 9)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                //ActID Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(ActID))).ToList();
                }
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;


                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
            }

        }

        public static List<InternalComplianceInstanceTransactionView> GetCannedReportDataForPerformer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();


                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a=>a.FirstOrDefault())
                                   .Select(entry=>entry.RoleID).ToList();

                if (GetApprover.Count==1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                          on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();                    
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID).ToList();
                }                

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;

                    
                }

                return transactionsQuery.Distinct().OrderBy(entry => entry.InternalScheduledOn).ToList();
            }

        }

        public static List<InternalComplianceInstanceTransactionView> GetCannedReportDataForReviewer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int actid, DateTime FromDate, DateTime ToDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();

                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();



                var GetApprover = (from row in entities.InternalComplianceAssignments
                                   where row.UserID == userID
                                   select row)
                                   .GroupBy(a => a.RoleID)
                                   .Select(a => a.FirstOrDefault())
                                   .Select(entry => entry.RoleID).ToList();

                if (GetApprover.Count == 1 && GetApprover.Contains(6))
                    RoleID = 6;

                if (RoleID == 8 || RoleID == 9)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         join row1 in entities.EntitiesAssignmentInternals
                                         on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                         where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                         && row1.UserID == userID && row.RoleID == 3
                                         select row).Distinct().ToList();
                }
                else if (RoleID == 6)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID)).ToList();
                }

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToByte(type))).ToList();
                }

                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToByte(category))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                if (actid != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToInt32(actid))).ToList();
                }
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;


                }

                return transactionsQuery.OrderBy(entry => entry.InternalScheduledOn).ToList();
            }
        }

        //public static List<InternalComplianceInstanceTransactionView> GetCannedReportDataForReviewer(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, DateTime FromDate, DateTime ToDate)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var reviewerRoleIDs = (from row in entities.Roles
        //                               where row.Code.StartsWith("RVW")
        //                               select row.ID).ToList();

        //        long RoleID = (from row in entities.Users
        //                       where row.ID == userID
        //                       select row.RoleID).Single();

        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

        //        List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();



        //        var GetApprover = (from row in entities.InternalComplianceAssignments
        //                           where row.UserID == userID
        //                           select row)
        //                           .GroupBy(a => a.RoleID)
        //                           .Select(a => a.FirstOrDefault())
        //                           .Select(entry => entry.RoleID).ToList();

        //        if (GetApprover.Count == 1 && GetApprover.Contains(6))
        //            RoleID = 6;

        //        if (RoleID == 8 || RoleID == 9)
        //        {
        //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //                                 join row1 in entities.EntitiesAssignmentInternals
        //                                 on (long)row.IComplianceCategoryID equals row1.ComplianceCatagoryID
        //                                 where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
        //                                 && row1.UserID == userID && row.RoleID == 3
        //                                 select row).Distinct().ToList();
        //        }
        //        else if (RoleID == 6)
        //        {
        //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //                                 where row.CustomerID == Customerid
        //                                 select row).ToList();

        //            transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
        //        }
        //        else
        //        {
        //            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
        //                                 where row.CustomerID == Customerid
        //                                 select row).ToList();

        //            transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID)).ToList();
        //        }                

        //        //Type Filter
        //        if (type != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceTypeID == Convert.ToInt32(type))).ToList();
        //        }

        //        //category Filter
        //        if (category != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.IComplianceCategoryID == Convert.ToInt32(category))).ToList();
        //        }

        //        //Datr Filter
        //        if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= ToDate)).ToList();
        //        }

        //        if (risk != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
        //        }

        //        if (location != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
        //        }

        //        switch (status)
        //        {
        //            case CannedReportFilterNewStatus.Upcoming:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.InternalScheduledOn >= now && entry.InternalScheduledOn <= nextOneMonth) && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10)).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.Overdue:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 10) && entry.InternalScheduledOn < now).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.ClosedTimely:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.ClosedDelayed:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.PendingForReview:
        //                transactionsQuery = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
        //                break;

        //            case CannedReportFilterNewStatus.Rejected:
        //                transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
        //                break;


        //        }

        //        return transactionsQuery.OrderBy(entry => entry.InternalScheduledOn).ToList();
        //    }
        //}

        public static List<MatrixInformation> GetNonComplianceReportsForApprover(int userID, CannedReportFilterForApprover filter)
        {
            List<MatrixInformation> reportData = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int approverRoleID = (from row in entities.Roles
                                      where row.Code == "APPR"
                                      select row.ID).Single();
               
                var complianceInstanceIDs = (from row in entities.InternalComplianceInstanceAssignmentViews
                                             where row.RoleID == approverRoleID && row.UserID == userID
                                             select row.ComplianceInstanceID).Distinct().ToList();

                var values = (from row in entities.NonInternalCompliancesSummaryViews
                              select row.InternalComplianceInstanceID).ToList();


                var results = (from row in entities.NonInternalCompliancesSummaryViews
                               where complianceInstanceIDs.Contains(row.InternalComplianceInstanceID)
                               select row).ToList();

                switch (filter)
                {
                    case CannedReportFilterForApprover.EntityByCategory:
                        //reportData = results.GroupBy(entry => new { entry.CustomerBranchID, entry.ComplianceCategoryId }).Select(entry => new MatrixInformation() { Row = entry.Select(row => row.LocationName).FirstOrDefault(), Column = entry.Select(row => row.ComplianceCategoryName).FirstOrDefault(), Count = entry.Count() }).ToList();
                        reportData = results.GroupBy(entry => new { entry.CustomerBranchID, entry.IComplianceCategoryID }).Select(entry => new MatrixInformation() { Row = entry.Select(row => row.LocationName).FirstOrDefault(), Column = entry.Select(row => row.IComplianceCategoryName).FirstOrDefault(), Count = entry.Count() }).ToList();
                        break;
                    case CannedReportFilterForApprover.CategoryByEntity:
                        reportData = results.GroupBy(entry => new { entry.CustomerBranchID, entry.IComplianceCategoryID }).Select(entry => new MatrixInformation() { Row = entry.Select(row => row.IComplianceCategoryName).FirstOrDefault(), Column = entry.Select(row => row.LocationName).FirstOrDefault(), Count = entry.Count() }).ToList();
                        break;
                    case CannedReportFilterForApprover.RiskByEntity:
                        reportData = results.GroupBy(entry => new { entry.CustomerBranchID, entry.IRiskType }).Select(entry => new MatrixInformation() { Row = ((RiskType)entry.Select(row => row.IRiskType).FirstOrDefault()).ToString(), Column = entry.Select(row => row.LocationName).FirstOrDefault(), Count = entry.Count() }).ToList();
                        break;
                }
            }

            return reportData;
        }
       
        public static List<InternalComplianceInstanceTransactionView> GetCannedReportDataForApprover(int userID, CannedReportFilterForApprover filter)
        {            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int approverRoleID = (from row in entities.Roles
                                      where row.Code == "APPR"
                                      select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;             

                var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == approverRoleID
                                         select row);

                switch (filter)
                {
                    case CannedReportFilterForApprover.ForFinalApproval:                       
                        transactionsQuery = transactionsQuery.Where(entry => entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5);
                        break;
                }                
                return transactionsQuery.OrderBy(entry => entry.InternalScheduledOn).ToList();
            }
        }

        public static List<object> GetDefaultersForApprover(int userID, CannedReportFilterForApprover filter)
        {
            List<object> defaulters = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int approverRoleID = (from row in entities.Roles
                                      where row.Code == "APPR"
                                      select row.ID).Single();

               
                var complianceInstanceIDs = (from row in entities.InternalComplianceInstanceAssignmentViews
                                             where row.RoleID == approverRoleID && row.UserID == userID
                                             select row.ComplianceInstanceID).Distinct().ToList();

                var results = (from row in entities.InternalComplianceTransactions
                               where complianceInstanceIDs.Contains(row.InternalComplianceInstanceID)
                               select new { UserID = row.CreatedBy, Status = row.StatusId, Name = row.CreatedByText }).ToList();



                var userIDs = results.Select(entry => entry.UserID).Distinct().ToList();

                var defaulterIDs = new List<long>();

                foreach (var item in userIDs)
                {
                    var transactions = results.Where(entry => entry.UserID == item).ToList();

                    for (int index = 0; index < (transactions.Count - 2); index++)
                    {
                        if (transactions[index].Status == 6 && transactions[index + 1].Status == 6 && transactions[index + 2].Status == 6)
                        {
                            if (!defaulterIDs.Contains(transactions[index].UserID))
                            {
                                defaulterIDs.Add(transactions[index].UserID);
                            }
                        }
                    }
                }

                var users = (from row in entities.Users
                             where defaulterIDs.Contains(row.ID)
                             select new { row.ID, row.CustomerBranchID, Name = row.FirstName + " " + row.LastName }).ToList();

                var branchIDs = users.Select(entry => entry.CustomerBranchID).Distinct().ToList();
                var branches = (from row in entities.CustomerBranches
                                where branchIDs.Contains(row.ID)
                                select new { row.ID, row.Name }).ToList();


                defaulters = (from userRow in users
                              join branchRow in branches on userRow.CustomerBranchID equals branchRow.ID into summary
                              from summaryRow in summary.DefaultIfEmpty()
                              select new { Name = userRow.Name, Location = (summaryRow != null ? summaryRow.Name : "--") }).ToList<object>();
            }

            return defaulters;
        }

        public static List<InternalComplianceCannedReportNew> GetCannedReportDataInternalCompliance(int CustID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var result = entities.InternalComplianceInstanceTransactionViews.AsEnumerable()
                                .Where(entry => entry.CustomerID == CustID)
                                .Select(r => new {
                                    r.User,
                                    r.UserID,
                                    r.RoleID,
                                    r.Role,
                                    r.Risk,
                                    r.InternalComplianceInstanceID,
                                    r.InternalComplianceID,
                                    r.IComplianceTypeID,
                                    r.IComplianceCategoryID,
                                    r.InternalComplianceStatusID,
                                    r.Status,
                                    r.InternalScheduledOn,
                                    r.CustomerBranchID,
                                    r.CustomerID,
                                    r.Branch,
                                    r.ShortDescription,
                                    r.ForMonth
                                })
                                 .GroupBy(x => new { x.InternalComplianceInstanceID, x.InternalComplianceID })
                                .Select(g => new InternalComplianceCannedReportNew()
                                {
                                    InternalComplianceID = g.First().InternalComplianceID,
                                    InternalComplianceTypeID = g.First().IComplianceTypeID,
                                    InternalComplianceCategoryID = g.First().IComplianceCategoryID,
                                    InternalComplianceInstanceID = g.First().InternalComplianceInstanceID,
                                    InternalComplianceStatusID = g.First().InternalComplianceStatusID,
                                    Status = g.First().Status,
                                    CustomerBranchID = g.First().CustomerBranchID,
                                    CustomerID = g.First().CustomerID,
                                    ShortDescription = g.First().ShortDescription,
                                    CustomerBranchName = g.First().Branch,
                                    InternalScheduledOn = g.First().InternalScheduledOn,
                                    ForMonth = g.First().ForMonth,
                                    IPerformer = g.Where(n => n.RoleID == 3).Select(x => x.User).FirstOrDefault(),
                                    IReviewer = g.Where(n => n.RoleID == 4).Select(x => x.User).FirstOrDefault(),
                                    IApprover = g.Where(n => n.RoleID == 6).Select(x => x.User).FirstOrDefault(),
                                    RiskID = g.First().Risk,
                                    Risk = g.First().Risk == 0 ? "High" : g.First().Risk == 1 ? "Medium" : g.First().Risk == 2 ? "Low" : "",

                                    IPerformerRoleID = g.Where(n => n.RoleID == 3).Select(x => x.RoleID).FirstOrDefault(),
                                    IReviewerRoleID = g.Where(n => n.RoleID == 4).Select(x => x.RoleID).FirstOrDefault(),

                                    RoleID = g.First().RoleID,
                                    UserID = g.First().UserID,

                                    IPerformerID = g.Where(n => n.RoleID == 3).Select(x => x.UserID).FirstOrDefault(),
                                    IReviewerID = g.Where(n => n.RoleID == 4).Select(x => x.UserID).FirstOrDefault(),
                                    IApproverID = g.Where(n => n.RoleID == 6).Select(x => x.UserID).FirstOrDefault(),

                                }).ToList();

                return result;
            }
        }
    }
}
