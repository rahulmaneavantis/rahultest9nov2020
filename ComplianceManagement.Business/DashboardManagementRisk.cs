﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class DashboardManagementRisk
    {
        public static List<sp_PerformerReviewView_Result> sp_GetPerFormerReviewer()
        {
            List<sp_PerformerReviewView_Result> obj = new List<sp_PerformerReviewView_Result>();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                obj = (from row in entities.sp_PerformerReviewView()
                       select row).ToList();
            }
            return obj;
        }
        public static int GetPersonResponsibleAuditCountOpenSubmit(List<SP_PersonResponsible_AuditStatusSummary_Result> MasterPersonResponsibleAuditCountView, int statusid, string status)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                List<SP_PersonResponsible_AuditStatusSummary_Result> record = new List<SP_PersonResponsible_AuditStatusSummary_Result>();
                if (statusid == 1)
                {
                    if (status == "RS")
                    {
                        record = (from row in MasterPersonResponsibleAuditCountView
                                  where row.AuditeeResponse == status &&
                                  row.AuditStatusID == 6
                                  select row).ToList().OrderByDescending(entry => entry.AuditID).ToList();

                        count = record.Count;
                    }
                    else if (status == "AS")
                    {
                        record = (from row in MasterPersonResponsibleAuditCountView
                                  where row.AuditeeResponse == status
                                  && row.AuditStatusID == 6
                                  select row).ToList();

                        count = record.Count;
                    }
                    else if (status == "RA")
                    {
                        record = (from row in MasterPersonResponsibleAuditCountView
                                  where row.AuditeeResponse == status
                                  && row.AuditStatusID == 6
                                  select row).ToList();

                        count = record.Count;
                    }
                }
                else if (statusid == 3)
                {
                    if (status == "HC")
                    {
                        record = (from row in MasterPersonResponsibleAuditCountView
                                  where row.AuditeeResponse == status
                                  && row.AuditStatusID == 3
                                  select row).ToList();

                        count = record.Count();
                    }
                }
                return count;
            }
        }
        public static int GetPersonResponsibleAuditOpenSubmitCountIMP(List<Sp_ImplementationAuditSummaryCountView_Result> MasterData, int statusid, List<string> Status, string type)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                if (statusid == 1)
                {
                    if (type == "Due")
                    {
                        var OpenCount = (from row in MasterData
                                         where row.AuditeeResponse == null
                                         && row.RoleID == 3
                                         && row.TimeLine > DateTime.Now
                                         select row).ToList();

                        count = OpenCount.Count();
                    }
                    else if (type == "OverDue")
                    {
                        var OpenCount = (from row in MasterData
                                         where row.AuditeeResponse == null
                                         && row.RoleID == 3
                                         && row.TimeLine <= DateTime.Now
                                         select row).ToList();
                        count = OpenCount.Count();
                    }
                    else if (Status != null)
                    {
                        var OpenCount = (from row in MasterData
                                         where Status.Contains(row.AuditeeResponse)
                                         && row.RoleID == 3
                                         select row).ToList();
                        count = OpenCount.Count();
                    }
                }
                else if (statusid == 3)
                {
                    var closeCount = (from row in MasterData
                                      where Status.Contains(row.AuditeeResponse)
                                      && row.RoleID == 3
                                      select row).ToList();

                    count = closeCount.Count();
                }
                return count;
            }
        }
        public static List<FeedbackFormUpload> GetFileDataFeedBackFormDocument(long auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.FeedbackFormUploads
                                where row.AuditId == auditID
                                select row).ToList();
                return fileData;
            }
        }

        public static bool DeleteFileAnnxeture(string path, int file)
        {
            //if (System.IO.File.Exists(path))
            //{
            //    System.IO.File.Delete(path);
            //}

            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Tran_AnnxetureUpload fileToUpdate = entities.Tran_AnnxetureUpload.Where(x => x.Id == file).FirstOrDefault();
                fileToUpdate.IsDeleted = true;
                entities.SaveChanges();

                //Tran_AnnxetureUpload fileToUpdate = new Tran_AnnxetureUpload() { Id = file };
                //entities.Tran_AnnxetureUpload.Attach(fileToUpdate);
                //fileToUpdate.IsDeleted = true;
            }
            return true;
        }

        public static List<MyDocumentAuditView_ReviewNew> GetFileDataReviewDocument(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.MyDocumentAuditView_ReviewNew
                                where row.AuditID == AuditID && row.Name != null
                                select row).ToList();
                return fileData;
            }
        }

        public static List<MyReportFinalUnionAuditCommite> GetFileDataAllReportDocument(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.MyReportFinalUnionAuditCommites
                                where row.AuditID == AuditID
                                select row).ToList();

                return fileData;
            }
        }
        public static List<FeedbackFormUpload> GetFileDataFeedbackFormUploadDocument(long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.FeedbackFormUploads
                                where row.AuditId == AuditID
                                select row).ToList();
                return fileData;
            }
        }
        public static List<Tran_FinalDeliverableUpload> GetFileDataFinalDelivrablesDocument(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.Tran_FinalDeliverableUpload
                                where row.AuditID == AuditID
                                select row).ToList();
                return fileData;
            }
        }

        public static List<Tran_AuditCommitePresantionUpload> GetFileDataAuditCommiteDocument(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.Tran_AuditCommitePresantionUpload
                                where row.AuditID == AuditID
                                select row).ToList();
                return fileData;
            }
        }
        public static List<MyDocumentAuditNewView_Working> GetFileDataWorkingDocument(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.MyDocumentAuditNewView_Working
                                where row.AuditID == AuditID
                                && row.IsDeleted == false
                                select row).ToList();
                return fileData;
            }
        }
        public static List<Tran_AnnxetureUpload> GetFileDataGetTran_AnnxetureUploadDocument(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.Tran_AnnxetureUpload
                                where row.AuditID == AuditID
                                 && row.IsDeleted == false && row.FileType == "Annxeture"
                                select row).ToList();
                return fileData;
            }
        }

        public static List<MyDocumentAuditNewView_Deleted> GetFileDataDeletedDocument(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.MyDocumentAuditNewView_Deleted
                                where row.AuditID == AuditID
                                && row.IsDeleted == true
                                select row).ToList();
                return fileData;
            }
        }

        public static string quartername;




        #region Person Responsible


        public static int GetPersonResponsibleAuditCount(List<sp_PersonResponsibleAuditCountView_Result> MasterPersonResponsibleAuditCountView, int statusid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();
                if (statusid == 1)
                {

                    record = (from row in MasterPersonResponsibleAuditCountView
                              where (row.ACCStatus == null)
                              select new OpenCloseAuditDetailsClass()
                              {
                                  AuditID = row.AuditID,
                                  CustomerID = (int)row.CustomerID,
                                  CustomerBranchID = (int)row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  FinancialYear = row.FinancialYear,
                                  VerticalId = row.VerticalId,
                                  VerticalName = row.VerticalName,
                              }).Distinct().ToList();

                    //record = (from row in entities.PersonResponsibleAuditStatusDisplayViews
                    //          where row.CustomerID == customerID
                    //          && row.PersonResponsible == userid
                    //          && (row.ACCStatus == null)
                    //          select new OpenCloseAuditDetailsClass()
                    //          {
                    //              CustomerID = row.CustomerID,
                    //              CustomerBranchID = row.CustomerBranchID,
                    //              Branch = row.Branch,
                    //              ForMonth = row.ForMonth,
                    //              FinancialYear = row.FinancialYear,
                    //              VerticalId = (long) row.VerticalID,
                    //              VerticalName = row.VerticalName,
                    //          }).Distinct().ToList();

                    count = record.Count();

                }
                else if (statusid == 3)
                {
                    record = (from row in MasterPersonResponsibleAuditCountView
                              where row.ACCStatus == 1
                              select new OpenCloseAuditDetailsClass()
                              {
                                  AuditID = row.AuditID,
                                  CustomerID = (int)row.CustomerID,
                                  CustomerBranchID = (int)row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  FinancialYear = row.FinancialYear,
                                  VerticalId = row.VerticalId,
                                  VerticalName = row.VerticalName,
                              }).Distinct().ToList();

                    //record = (from row in entities.PersonResponsibleAuditStatusDisplayViews
                    //          where row.CustomerID == customerID
                    //          && row.PersonResponsible == userid
                    //          && (row.AuditStatusID == 3) && row.ACCStatus == 1                           
                    //          select new OpenCloseAuditDetailsClass()
                    //          {
                    //              CustomerID = row.CustomerID,
                    //              CustomerBranchID = row.CustomerBranchID,
                    //              Branch = row.Branch,
                    //              ForMonth = row.ForMonth,
                    //              FinancialYear = row.FinancialYear,
                    //              VerticalId = (long) row.VerticalID,
                    //              VerticalName = row.VerticalName,
                    //          }).Distinct().ToList();

                    count = record.Count();
                }

                return count;
            }
        }
        public static int GetPersonResponsibleAuditCountIMP(List<ImplimentInternalPersonResponsibleAuditClosedCountView> MasterImplimentInternalPersonResponsibleAuditClosedCountView, int statusid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;

                if (statusid == 1)
                {

                    int flag = 0;
                    var OpenCount = (from row in MasterImplimentInternalPersonResponsibleAuditClosedCountView
                                     where row.AllClosed != flag
                                     select new OpenCloseAuditDetailsClass()
                                     {
                                         AuditID = row.AuditID,
                                         CustomerID = row.CustomerID,
                                         CustomerBranchID = (int)row.CustomerBranchID,
                                         ForMonth = row.ForMonth,
                                     }).Distinct().ToList();

                    count = OpenCount.Count();

                    //record = (from row in entities.PersonResponsibleImplementationAuditStatusDisplayViews
                    //          where row.CustomerID == customerID
                    //          && row.PersonResponsible == userid
                    //          && (row.AuditStatusID != 3 || row.AuditStatusID == null)
                    //          select new OpenCloseAuditDetailsClass()
                    //          {
                    //              CustomerID = row.CustomerID,
                    //              CustomerBranchID = row.CustomerBranchID,
                    //              Branch = row.Branch,
                    //              ForMonth = row.ForMonth,
                    //              FinancialYear = row.FinancialYear,
                    //              VerticalId = (long) row.VerticalID,
                    //              VerticalName = row.VerticalName,
                    //          }).Distinct().ToList();

                    //count = record.Count();

                }
                else if (statusid == 3)
                {

                    int flag = 0;
                    var closeCount = (from row in MasterImplimentInternalPersonResponsibleAuditClosedCountView
                                      where row.AllClosed == flag
                                      select new OpenCloseAuditDetailsClass()
                                      {
                                          AuditID = row.AuditID,
                                          CustomerID = row.CustomerID,
                                          CustomerBranchID = (int)row.CustomerBranchID,
                                          ForMonth = row.ForMonth,
                                      }).Distinct().ToList();

                    count = closeCount.Count();


                    //record = (from row in entities.PersonResponsibleImplementationAuditStatusDisplayViews
                    //          where row.CustomerID == customerID
                    //          && row.PersonResponsible == userid
                    //          && (row.AuditStatusID == 3)
                    //          select new OpenCloseAuditDetailsClass()
                    //          {
                    //              CustomerID = row.CustomerID,
                    //              CustomerBranchID = row.CustomerBranchID,
                    //              Branch = row.Branch,
                    //              ForMonth = row.ForMonth,
                    //              FinancialYear = row.FinancialYear,
                    //              VerticalId = (long) row.VerticalID,
                    //              VerticalName = row.VerticalName,
                    //          }).Distinct().ToList();

                    //count = record.Count();
                }

                return count;
            }
        }
        public static List<OpenCloseAuditDetailsClass> GetPersonResponsibleAudits(int Customerid, List<int?> BranchList, String FinYear, String Period, int userid, int statusid, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();

                if (statusid == 1)
                {
                    record = (from row in entities.sp_PersonResponsibleAuditCountView(userid)
                              where row.ACCStatus == null
                              select new OpenCloseAuditDetailsClass()
                              {
                                  AuditID = row.AuditID,
                                  CustomerID = (int)row.CustomerID,
                                  CustomerBranchID = (int)row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  FinancialYear = row.FinancialYear,
                                  VerticalId = row.VerticalId,
                                  VerticalName = row.VerticalName,
                                  UserName = row.UserName,
                                  CProcessName = row.CProcessName
                              }).Distinct().ToList();

                    //record = (from row in entities.PersonResponsibleAuditCountViews
                    //          where row.CustomerID == Customerid
                    //          && row.PersonResponsible == userid
                    //          && (row.ACCStatus == null)
                    //          select new OpenCloseAuditDetailsClass()
                    //          {
                    //              AuditID = row.AuditID,
                    //              CustomerID = row.CustomerID,
                    //              CustomerBranchID = (int)row.CustomerBranchID,
                    //              Branch = row.Branch,
                    //              ForMonth = row.ForMonth,
                    //              FinancialYear = row.FinancialYear,
                    //              VerticalId = (int)row.VerticalsId,
                    //              VerticalName = row.VerticalName,
                    //              UserName = row.UserName,
                    //              CProcessName = row.CProcessName
                    //          }).Distinct().ToList();
                }

                else if (statusid == 3)
                {
                    record = (from row in entities.sp_PersonResponsibleAuditCountView(userid)
                              where row.ACCStatus == 1
                              select new OpenCloseAuditDetailsClass()
                              {
                                  AuditID = row.AuditID,
                                  CustomerID = (int)row.CustomerID,
                                  CustomerBranchID = (int)row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  FinancialYear = row.FinancialYear,
                                  VerticalId = row.VerticalId,
                                  VerticalName = row.VerticalName,
                                  UserName = row.UserName,
                                  CProcessName = row.CProcessName
                              }).Distinct().ToList();

                    //record = (from row in entities.PersonResponsibleAuditCountViews
                    //          where row.CustomerID == Customerid
                    //          && row.PersonResponsible == userid
                    //          && (row.ACCStatus == 1)
                    //          select new OpenCloseAuditDetailsClass()
                    //          {
                    //              AuditID = row.AuditID,
                    //              CustomerID = row.CustomerID,
                    //              CustomerBranchID = (int)row.CustomerBranchID,
                    //              Branch = row.Branch,
                    //              ForMonth = row.ForMonth,
                    //              FinancialYear = row.FinancialYear,
                    //              VerticalId = (int)row.VerticalsId,
                    //              VerticalName = row.VerticalName,
                    //              UserName = row.UserName,
                    //              CProcessName = row.CProcessName
                    //          }).Distinct().ToList();
                }

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (CustBranchid != -1)
                //    record = record.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();

                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                return record;
            }
        }





        #endregion

        #region Audit Manager
        public static List<OpenCloseAuditDetailsClass> GetAuditManagerDashboardAudits(String FinYear, String Period, int Customerid, List<int?> BranchList, int VerticalID, List<int?> BranchAssigned)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();

                var Masterrecord = (from row in entities.AuditSummaryCountViews
                                    select row).ToList();

                record = (from row in Masterrecord
                          where row.CustomerID == Customerid
                          && BranchAssigned.Contains(row.CustomerBranchID)
                          select new OpenCloseAuditDetailsClass()
                          {
                              CustomerID = row.CustomerID,
                              CustomerBranchID = row.CustomerBranchID,
                              Branch = row.Branch,
                              ForMonth = row.ForMonth,
                              FinancialYear = row.FinancialYear,
                              VerticalId = row.VerticalId,
                              VerticalName = row.VerticalName,
                          }).Distinct().ToList();

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (CustBranchid != -1)
                //    record = record.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();

                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                return record;
            }
        }
        public static List<OpenCloseAuditDetailsClass> GetManagementDashboardAudits(String FinYear, String Period, int Customerid, List<int?> BranchList, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();

                var Masterrecord = (from row in entities.AuditSummaryCountViews
                                    select row).ToList();

                record = (from row in Masterrecord
                          where row.CustomerID == Customerid
                          select new OpenCloseAuditDetailsClass()
                          {
                              CustomerID = row.CustomerID,
                              CustomerBranchID = row.CustomerBranchID,
                              Branch = row.Branch,
                              ForMonth = row.ForMonth,
                              FinancialYear = row.FinancialYear,
                              VerticalId = row.VerticalId,
                              VerticalName = row.VerticalName,
                          }).Distinct().ToList();

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (CustBranchid != -1)
                //    record = record.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();

                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                return record;
            }
        }
        public static List<int?> GetDistinctAuditsStatusManagementDashboard(String FinYear, String Period, int Customerid, int CustBranchid, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int?> AuditStatusList = new List<int?>();

                var Masterrecord = (from row in entities.AuditSummaryCountViews
                                    select row).ToList();

                var record = (from row in Masterrecord
                              where row.CustomerID == Customerid
                              && row.CustomerBranchID == CustBranchid
                              && row.VerticalId == VerticalID
                              && row.FinancialYear == FinYear
                              && row.ForMonth == Period
                              select row).ToList();

                if (record.Count > 0)
                    AuditStatusList = record.Select(Entry => Entry.AuditStatusID).Distinct().ToList();

                return AuditStatusList;
            }
        }
        public static int CheckAuditStatusReviewWorkingManagementDashboard(String FinYear, String Period, int Customerid, int CustBranchid, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int Result = 0;

                List<int?> ReviewStatusList = new List<int?> { 3, 4, 5, 6 };



                var Records = (from row in entities.AuditSummaryCountViews
                               where row.CustomerID == Customerid
                               && row.CustomerBranchID == CustBranchid
                               && row.VerticalId == VerticalID
                               && row.FinancialYear == FinYear
                               && row.ForMonth == Period
                               && row.RoleID == 4
                               select row).ToList();

                if (Records.Count > 0)
                {
                    var TotalATBD = Records.Select(Entry => Entry.ATBDID).Distinct().Count();

                    var ReviewedATBD = Records.Select(Entry => ReviewStatusList.Contains(Entry.AuditStatusID)).Distinct().Count();

                    if (TotalATBD != 0)
                        Result = (ReviewedATBD / TotalATBD) * 100;

                    if (Result >= 75)
                        Result = 3;
                    else
                        Result = 2;
                }

                return Result;
            }
        }
        public static List<OpenCloseAuditDetailsClass> GetAuditManagerAudits(String FinYear, String Period, int Customerid, List<long> BranchList, int userid, int statusid, int VerticalID, int RoleID, string departmentheadFlag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();
                String isAMAH = CustomerManagementRisk.GetAuditHeadOrManagerid(userid);
                if (CustomerManagementRisk.CheckIsManagement(userid) == 8)
                {
                    #region Management   
                    if (statusid == 1)
                    {
                        var Masterrecord = (from C in entities.AuditCountView_Dashboard
                                            join EAAMR in entities.EntitiesAssignmentManagementRisks
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            where C.ProcessId == EAAMR.ProcessId
                                            && C.CustomerID == Customerid
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where C.CustomerID == Customerid
                                  && (C.ACCStatus == null)
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    else if (statusid == 3)
                    {
                        var Masterrecord = (from C in entities.AuditCountViews
                                            join EAAMR in entities.EntitiesAssignmentManagementRisks
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            where C.ProcessId == EAAMR.ProcessId
                                            && C.CustomerID == Customerid
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where C.CustomerID == Customerid
                                   && C.ACCStatus == 1
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    #endregion
                }
                else if (!String.IsNullOrEmpty(isAMAH) && (isAMAH == "AM" || isAMAH == "AH"))
                {
                    #region  Audit Manager             
                    if (statusid == 1)
                    {
                        var Masterrecord = (from C in entities.AuditCountView_Dashboard
                                            join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            where C.ProcessId == EAAMR.ProcessId
                                            && C.CustomerID == Customerid
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where C.CustomerID == Customerid
                                  && (C.ACCStatus == null)
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    else if (statusid == 3)
                    {
                        var Masterrecord = (from C in entities.AuditCountViews
                                            join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            where C.ProcessId == EAAMR.ProcessId
                                            && C.CustomerID == Customerid
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where C.CustomerID == Customerid
                                   && C.ACCStatus == 1
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    #endregion
                }
                else if (departmentheadFlag == "DH")
                {
                    #region Department Head

                    if (statusid == 1)
                    {
                        var Masterrecord = (from C in entities.AuditCountView_Dashboard
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.AuditID equals ICAA.AuditID
                                            join EAAMR in entities.EntitiesAssignmentDepartmentHeads
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            join MSP in entities.Mst_Process
                                            on ICAA.ProcessId equals MSP.Id
                                            where EAAMR.DepartmentID == MSP.DepartmentID
                                            && C.CustomerID == Customerid
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where C.CustomerID == Customerid
                                  && (C.ACCStatus == null)
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    else if (statusid == 3)
                    {
                        var Masterrecord = (from C in entities.AuditCountViews
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.AuditID equals ICAA.AuditID
                                            join EAAMR in entities.EntitiesAssignmentDepartmentHeads
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            join MSP in entities.Mst_Process
                                            on ICAA.ProcessId equals MSP.Id
                                            where EAAMR.DepartmentID == MSP.DepartmentID
                                            && C.CustomerID == Customerid
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where C.CustomerID == Customerid
                                   && C.ACCStatus == 1
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    #endregion
                }
                else
                {
                    #region Performer and Reviewer
                    if (statusid == 1)
                    {
                        var Masterrecord = (from C in entities.AuditCountViews
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.CustomerBranchID equals (int)ICAA.CustomerBranchID
                                            where C.ProcessId == ICAA.ProcessId
                                            && C.AuditID == ICAA.AuditID
                                            && C.CustomerID == Customerid
                                            && ICAA.UserID == userid
                                            && ICAA.RoleID == RoleID
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where C.CustomerID == Customerid
                                  && (C.ACCStatus == null)
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    else if (statusid == 3)
                    {
                        var Masterrecord = (from C in entities.AuditCountViews
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.CustomerBranchID equals (int)ICAA.CustomerBranchID
                                            where C.ProcessId == ICAA.ProcessId
                                            && C.AuditID == ICAA.AuditID
                                            && C.CustomerID == Customerid
                                            && ICAA.UserID == userid
                                            && ICAA.RoleID == RoleID
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where C.CustomerID == Customerid
                                   && C.ACCStatus == 1
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    #endregion
                }

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();
                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains((int)Entry.CustomerBranchID)).ToList();
                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();
                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                if (record.Count > 0)
                    record = record.OrderBy(Entry => Entry.Branch).ThenBy(entry => entry.VerticalName).ThenBy(entry => entry.FinancialYear).ToList();

                return record;
            }
        }
        public static List<OpenCloseAuditDetailsClass> GetAuditManagerAuditsIMP(String FinYear, String Period, int Customerid, int CustBranchid, int userid, int statusid, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();
                string isAMAH = CustomerManagementRisk.GetAuditHeadOrManagerid(userid);
                if (!String.IsNullOrEmpty(isAMAH) && (isAMAH == "AM" || isAMAH == "AH"))
                {
                    List<long> branchids = AssignEntityManagementRisk.CheckAuditManagerLocation(userid);

                    List<int?> BranchAssigned = branchids.Select(x => (int?)x).ToList();

                    if (statusid == 1)
                    {
                        List<int> chkOpenBranchcount = new List<int>();
                        int flag = 0;
                        if (BranchAssigned.Count > 0)
                        {
                            //Added by rahul on 9 FEB 2018
                            chkOpenBranchcount = (from C in entities.ImplimentInternalAuditClosedCountViews
                                                  join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                                  on C.CustomerBranchID equals EAAMR.BranchID
                                                  where C.ProcessID == EAAMR.ProcessId
                                                  && C.CustomerID == Customerid
                                                  && EAAMR.UserID == userid
                                                  && EAAMR.ISACTIVE == true
                                                  && C.RoleID == 3 && C.AllClosed != flag
                                                  select C.CustomerBranchID).ToList();

                            //Comment by rahul on 9 FEB 2018
                            //chkOpenBranchcount = (from row in entities.ImplimentInternalAuditClosedCountViews
                            //                      where row.CustomerID == Customerid && BranchAssigned.Contains(row.CustomerBranchID)
                            //                     && row.RoleID == 3 && row.AllClosed != flag
                            //                      select row.CustomerBranchID).ToList();

                            record = (from row in entities.ImplementationAuditSummaryCountViews
                                      where row.CustomerID == Customerid
                                     && chkOpenBranchcount.Contains(row.CustomerBranchID)
                                     && (row.AuditStatusID != 3 || (row.ImplementationAuditStatusID == 3 || row.ImplementationAuditStatusID == null))
                                      select new OpenCloseAuditDetailsClass()
                                      {
                                          CustomerID = row.CustomerID,
                                          CustomerBranchID = row.CustomerBranchID,
                                          Branch = row.Branch,
                                          ForMonth = row.ForMonth,
                                          FinancialYear = row.FinancialYear,
                                          VerticalId = (long)row.VerticalID,
                                          VerticalName = row.VerticalName,
                                          UserName = row.UserName,
                                          CProcessName = row.CProcessName,
                                          AuditID = row.AuditID,
                                          AuditName = row.Branch + "/" + row.FinancialYear + "/" + row.ForMonth,
                                      }).Distinct().ToList();
                        }
                    }

                    else if (statusid == 3)
                    {
                        List<int> chkclosedBranchcount = new List<int>();
                        int flag = 0;
                        if (BranchAssigned.Count > 0)
                        {

                            //Added by rahul on 9 FEB 2018
                            chkclosedBranchcount = (from C in entities.ImplimentInternalAuditClosedCountViews
                                                    join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                                    on C.CustomerBranchID equals EAAMR.BranchID
                                                    where C.ProcessID == EAAMR.ProcessId
                                                    && C.CustomerID == Customerid
                                                    && EAAMR.UserID == userid
                                                    && EAAMR.ISACTIVE == true
                                                    && C.RoleID == 3 && C.AllClosed == flag
                                                    select C.CustomerBranchID).ToList();

                            //Comment by rahul on 9 FEB 2018
                            //chkclosedBranchcount = (from row in entities.ImplimentInternalAuditClosedCountViews
                            //                        where row.CustomerID == Customerid && BranchAssigned.Contains(row.CustomerBranchID)
                            //                       && row.RoleID == 3 && row.AllClosed == flag
                            //                        select row.CustomerBranchID).ToList();

                            record = (from row in entities.ImplementationAuditSummaryCountViews
                                      where row.CustomerID == Customerid && chkclosedBranchcount.Contains(row.CustomerBranchID)
                                        && row.AuditStatusID == 3
                                        && row.ImplementationAuditStatusID != 3
                                      select new OpenCloseAuditDetailsClass()
                                      {
                                          CustomerID = row.CustomerID,
                                          CustomerBranchID = row.CustomerBranchID,
                                          Branch = row.Branch,
                                          ForMonth = row.ForMonth,
                                          FinancialYear = row.FinancialYear,
                                          VerticalId = (long)row.VerticalID,
                                          VerticalName = row.VerticalName,
                                          UserName = row.UserName,
                                          CProcessName = row.CProcessName,
                                          AuditID = row.AuditID,
                                          AuditName = row.Branch + "/" + row.FinancialYear + "/" + row.ForMonth,
                                      }).Distinct().ToList();
                        }
                    }
                }

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (CustBranchid != -1)
                    record = record.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();
                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();
                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                return record;
            }
        }

        public static List<OpenCloseAuditDetailsClass> GetAuditManagerDashboardAudits(String FinYear, String Period, int Customerid, int CustBranchid, int VerticalID, List<int?> BranchAssigned)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();

                record = (from row in entities.AuditSummaryCountViews
                          where row.CustomerID == Customerid
                          && BranchAssigned.Contains(row.CustomerBranchID)
                          select new OpenCloseAuditDetailsClass()
                          {
                              CustomerID = row.CustomerID,
                              CustomerBranchID = row.CustomerBranchID,
                              Branch = row.Branch,
                              ForMonth = row.ForMonth,
                              FinancialYear = row.FinancialYear,
                              VerticalId = row.VerticalId,
                              VerticalName = row.VerticalName,
                          }).Distinct().ToList();

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (CustBranchid != -1)
                    record = record.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();

                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                return record;
            }
        }
        public static List<OpenCloseAuditDetailsClass> GetManagementDashboardAudits(String FinYear, String Period, int Customerid, int CustBranchid, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();

                record = (from row in entities.AuditSummaryCountViews
                          where row.CustomerID == Customerid
                          select new OpenCloseAuditDetailsClass()
                          {
                              CustomerID = row.CustomerID,
                              CustomerBranchID = row.CustomerBranchID,
                              Branch = row.Branch,
                              ForMonth = row.ForMonth,
                              FinancialYear = row.FinancialYear,
                              VerticalId = row.VerticalId,
                              VerticalName = row.VerticalName,
                          }).Distinct().ToList();

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (CustBranchid != -1)
                    record = record.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();

                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                return record;
            }
        }
        #endregion

        #region Internal Audit
        public static int GetInternalAuditCount(List<ICFRAuditCountView> ICFRCountRecords, int userid, int statusid, long customerid, int roleID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                List<OpenCloseInternalAuditDetailsCountClass> record = new List<OpenCloseInternalAuditDetailsCountClass>();

                if (statusid == 1)
                {

                    record = (from C in ICFRCountRecords
                              where C.UserID == userid
                               && C.RoleID == roleID
                              && (C.AuditStatusID != 3 || C.AuditStatusID == null)
                              group C by new
                              {
                                  C.CustomerID,
                                  C.CustomerBranchID,
                                  C.ForMonth,
                                  C.UserID,
                                  C.FinancialYear,

                              } into GCS
                              select new OpenCloseInternalAuditDetailsCountClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  ForMonth = GCS.Key.ForMonth,
                                  UserID = GCS.Key.UserID,
                                  FinancialYear = GCS.Key.FinancialYear,

                              }).Distinct().ToList();

                    //record = (from row in entities.AuditInstanceTransactionViews
                    //          where row.UserID == userid
                    //          && row.RoleID == roleID
                    //          && (row.AuditStatusID != 3 || row.AuditStatusID == null)
                    //          select new OpenCloseInternalAuditDetailsClass()
                    //          {
                    //              CustomerID = row.CustomerID,
                    //              CustomerBranchID = row.CustomerBranchID,                                  
                    //              ForMonth = row.ForMonth,
                    //              UserID = row.UserID,
                    //              FinancialYear = row.FinancialYear,
                    //              AuditStatusID=row.AuditStatusID
                    //          }).Distinct().ToList();
                    count = record.Count();
                }

                else if (statusid == 3)
                {
                    int flag = 0;

                    List<string> forMonth = new List<string>();
                    forMonth.Add("Apr - Jun");
                    forMonth.Add("Jul - Sep");
                    forMonth.Add("Oct - Dec");
                    forMonth.Add("Jan - Mar");

                    var countBarnch = (from row in entities.AuditAssignments
                                       where row.UserID == userid && row.RoleID == roleID
                                       select row.CustomerBranchID).Distinct().ToList();

                    if (countBarnch.Count > 0)
                    {
                        foreach (var item in countBarnch)
                        {
                            foreach (var itemmonth in forMonth)
                            {
                                var test = (from row in ICFRCountRecords
                                            where row.CustomerBranchID == item && row.UserID == userid
                                            && row.ForMonth.Equals(itemmonth) && row.CustomerID == customerid
                                            && row.RoleID == roleID
                                            select new OpenCloseInternalAuditDetailsCountClass()
                                            {
                                                CustomerID = row.CustomerID,
                                                CustomerBranchID = row.CustomerBranchID,
                                                ForMonth = row.ForMonth,
                                                UserID = row.UserID,
                                                FinancialYear = row.FinancialYear,
                                                AuditStatusID = row.AuditStatusID,
                                            }).Distinct().ToList().Count;

                                //var test1 = (from row in ICFRCountRecords
                                //             where row.CustomerBranchID == item && row.UserID == userid 
                                //             && row.ForMonth.Equals(itemmonth) && row.CustomerID == customerid
                                //             && row.RoleID == roleID && row.AuditStatusID == 3                                             
                                //             select new OpenCloseInternalAuditDetailsCountClass()
                                //             {
                                //                 CustomerID = row.CustomerID,
                                //                 CustomerBranchID = row.CustomerBranchID,
                                //                 ForMonth = row.ForMonth,
                                //                 UserID = row.UserID,
                                //                 FinancialYear = row.FinancialYear,
                                //                 AuditStatusID= row.AuditStatusID,
                                //             }).Distinct().ToList().Count;

                                var test1 = (from C in ICFRCountRecords
                                             where C.CustomerBranchID == item && C.UserID == userid
                                             && C.ForMonth.Equals(itemmonth) && C.CustomerID == customerid
                                             && C.RoleID == roleID && C.AuditStatusID == 3
                                             group C by new
                                             {
                                                 C.CustomerID,
                                                 C.CustomerBranchID,
                                                 C.ForMonth,
                                                 C.UserID,
                                                 C.FinancialYear,
                                                 AuditStatusID = C.AuditStatusID,

                                             } into GCS
                                             select new OpenCloseInternalAuditDetailsCountClass()
                                             {
                                                 CustomerID = GCS.Key.CustomerID,
                                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                                 ForMonth = GCS.Key.ForMonth,
                                                 UserID = GCS.Key.UserID,
                                                 FinancialYear = GCS.Key.FinancialYear,
                                                 AuditStatusID = GCS.Key.AuditStatusID,
                                             }).Distinct().ToList().Count;


                                if (test == test1)
                                {
                                    if (test > 0)
                                    {
                                        flag = flag + 1;
                                    }
                                }
                            }
                        }
                    }
                    count = flag;
                }
                return count;
            }
        }

        //Created by Rahul on 16 FEB 2018
        public static int GetInternalAuditOpenAuditCount(int userid, int roleID)
        {
            int count = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ICFROpenAuditCountDetails_Result> record = new List<ICFROpenAuditCountDetails_Result>();
                entities.Database.CommandTimeout = 300;
                record = (from C in entities.ICFROpenAuditCountDetails(userid, roleID)
                          select C).ToList();

                count = record.Count();

            }
            return count;
        }
        public static List<ICFROpenAuditCountDetails_Result> GetInternalOpenAudits(List<long> CustBranchid, String FinYear, String Period, int userid, int roleID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ICFROpenAuditCountDetails_Result> record = new List<ICFROpenAuditCountDetails_Result>();

                entities.Database.CommandTimeout = 300;
                record = (from C in entities.ICFROpenAuditCountDetails(userid, roleID)
                          select C).ToList();
                if (record.Count > 0)
                {
                    if (CustBranchid.Count > 0)
                        record = record.Where(Entry => CustBranchid.Contains((long)Entry.CustomerBranchID)).ToList();

                    if (FinYear != "")
                        record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                    if (Period != "")
                        record = record.Where(Entry => Entry.ForMonth == Period).ToList();
                }
                return record;
            }
        }

        //Created by Rahul on 16 FEB 2018
        public static int GetInternalAuditClosedAuditCount(int userid, int roleID)
        {
            int count = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<ICFRClosedAuditCountDetails_Result> record = new List<ICFRClosedAuditCountDetails_Result>();
                record = (from C in entities.ICFRClosedAuditCountDetails(userid, roleID)
                          select C).ToList();

                count = record.Count();

            }
            return count;
        }
        public static List<ICFRClosedAuditCountDetails_Result> GetInternalClosedAudits(List<long> CustBranchid, String FinYear, String Period, int userid, int roleID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ICFRClosedAuditCountDetails_Result> record = new List<ICFRClosedAuditCountDetails_Result>();

                entities.Database.CommandTimeout = 300;
                record = (from C in entities.ICFRClosedAuditCountDetails(userid, roleID)
                          select C).ToList();
                if (record.Count > 0)
                {
                    if (CustBranchid.Count > 0)
                        record = record.Where(Entry => CustBranchid.Contains((long)Entry.CustomerBranchID)).ToList();

                    if (FinYear != "")
                        record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                    if (Period != "")
                        record = record.Where(Entry => Entry.ForMonth == Period).ToList();
                }
                return record;
            }
        }
        public static List<OpenCloseInternalAuditDetailsClass> GetInternalAudits(int Customerid, List<long> CustBranchid, String FinYear, String Period, int userid, int statusid, int roleID = 0)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseInternalAuditDetailsClass> record = new List<OpenCloseInternalAuditDetailsClass>();

                if (statusid == 1)
                {
                    record = (from row in entities.AuditInstanceTransactionViews
                              where row.UserID == userid
                              && row.RoleID == roleID
                              && (row.AuditStatusID != 3 || row.AuditStatusID == null)
                              select new OpenCloseInternalAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  UserID = row.UserID,
                                  RoleID = row.RoleID,
                                  Role = row.Role,
                                  FinancialYear = row.FinancialYear
                              }).Distinct().ToList();

                }

                else if (statusid == 3)
                {
                    record = (from row in entities.AuditInstanceTransactionViews
                              where row.UserID == userid
                              && row.RoleID == roleID
                              && row.AuditStatusID == 3
                              select new OpenCloseInternalAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  UserID = row.UserID,
                                  RoleID = row.RoleID,
                                  Role = row.Role,
                                  FinancialYear = row.FinancialYear
                              }).Distinct().ToList();
                }

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (CustBranchid.Count > 0)
                    record = record.Where(Entry => CustBranchid.Contains((long)Entry.CustomerBranchID)).ToList();

                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                //if (roleID != -1)
                //    record = record.Where(Entry => Entry.RoleID == roleID).ToList();

                return record;
            }
        }
        public static List<OpenCloseInternalAuditDetailsClass> GetInternalAuditsNew(int Customerid, List<long> CustBranchid, String FinYear, String Period, int userid, int statusid, int roleID = 0)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseInternalAuditDetailsClass> record = new List<OpenCloseInternalAuditDetailsClass>();

                if (statusid == 1)
                {

                    entities.Database.CommandTimeout = 300;
                    var Masterrecord = (from row in entities.ICFRAuditCountViews
                                        select row).ToList();
                    record = (from C in Masterrecord
                              where C.UserID == userid
                               && C.RoleID == roleID
                              && (C.AuditStatusID != 3 || C.AuditStatusID == null)
                              group C by new
                              {
                                  C.CustomerID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.UserID,
                                  C.RoleID,
                                  C.FinancialYear,
                                  C.UserName,
                                  C.CProcessName

                              } into GCS
                              select new OpenCloseInternalAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  UserID = GCS.Key.UserID,
                                  RoleID = GCS.Key.RoleID,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  UserName = GCS.Key.UserName,
                                  CProcessName = GCS.Key.CProcessName,
                              }).Distinct().ToList();
                }

                else if (statusid == 3)
                {

                    entities.Database.CommandTimeout = 300;
                    var Masterrecord = (from row in entities.ICFRAuditCountViews
                                        select row).ToList();

                    List<string> forMonth = new List<string>();
                    forMonth.Add("Apr - Jun");
                    forMonth.Add("Jul - Sep");
                    forMonth.Add("Oct - Dec");
                    forMonth.Add("Jan - Mar");

                    var countBarnch = (from row in entities.AuditAssignments
                                       where row.UserID == userid && row.RoleID == roleID
                                       select row.CustomerBranchID).Distinct().ToList();

                    if (countBarnch.Count > 0)
                    {
                        foreach (var item in countBarnch)
                        {
                            foreach (var itemmonth in forMonth)
                            {
                                var test = (from row in Masterrecord
                                            where row.CustomerBranchID == item && row.UserID == userid
                                            && row.ForMonth.Equals(itemmonth) && row.CustomerID == Customerid
                                            && row.RoleID == roleID
                                            select new OpenCloseInternalAuditDetailsCountClass()
                                            {
                                                CustomerID = row.CustomerID,
                                                CustomerBranchID = row.CustomerBranchID,
                                                ForMonth = row.ForMonth,
                                                UserID = row.UserID,
                                                FinancialYear = row.FinancialYear
                                            }).Distinct().ToList().Count;

                                var test1 = (from row in Masterrecord
                                             where row.CustomerBranchID == item && row.UserID == userid
                                             && row.ForMonth.Equals(itemmonth) && row.CustomerID == Customerid
                                             && row.RoleID == roleID && row.AuditStatusID == 3
                                             select new OpenCloseInternalAuditDetailsCountClass()
                                             {
                                                 CustomerID = row.CustomerID,
                                                 CustomerBranchID = row.CustomerBranchID,
                                                 ForMonth = row.ForMonth,
                                                 UserID = row.UserID,
                                                 FinancialYear = row.FinancialYear
                                             }).Distinct().ToList().Count;

                                if (test == test1)
                                {
                                    if (test > 0)
                                    {

                                        record = (from C in Masterrecord
                                                  where C.UserID == userid
                                                   && C.RoleID == roleID
                                                   && C.AuditStatusID == 3
                                                  group C by new
                                                  {
                                                      C.CustomerID,
                                                      C.CustomerBranchID,
                                                      C.Branch,
                                                      C.ForMonth,
                                                      C.UserID,
                                                      C.RoleID,
                                                      C.FinancialYear,
                                                      C.UserName,
                                                      C.CProcessName

                                                  } into GCS
                                                  select new OpenCloseInternalAuditDetailsClass()
                                                  {
                                                      CustomerID = GCS.Key.CustomerID,
                                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                                      Branch = GCS.Key.Branch,
                                                      ForMonth = GCS.Key.ForMonth,
                                                      UserID = GCS.Key.UserID,
                                                      RoleID = GCS.Key.RoleID,
                                                      FinancialYear = GCS.Key.FinancialYear,
                                                      UserName = GCS.Key.UserName,
                                                      CProcessName = GCS.Key.CProcessName,
                                                  }).Distinct().ToList();

                                    }
                                }
                            }
                        }
                    }
                }

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (CustBranchid.Count > 0)
                    record = record.Where(Entry => CustBranchid.Contains((long)Entry.CustomerBranchID)).ToList();

                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                //if (roleID != -1)
                //    record = record.Where(Entry => Entry.RoleID == roleID).ToList();

                return record;
            }
        }


        #endregion

        public static int getDataCountFailds(List<FailedControlQuarterWiseDisplayView> FailedControlQuarterWiseDisplayViewRecords, int userID, long customerID, string flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                List<OpenCloseInternalAuditDetailsClass> record = new List<OpenCloseInternalAuditDetailsClass>();
                List<int> PerformerstatusIds = new List<int>();
                PerformerstatusIds.Clear();
                PerformerstatusIds.Add(3);
                PerformerstatusIds.Add(4);
                if (flag == "D")
                {
                    record = (from row in FailedControlQuarterWiseDisplayViewRecords
                              where row.PersonResponsible == userID && row.RoleID == 3
                              && PerformerstatusIds.Contains((int)row.AuditStatusID) && row.TimeLine <= DateTime.Today.Date
                              select new OpenCloseInternalAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = (int)row.CustomerBranchID,
                                  Branch = row.Branch,
                                  UserID = row.UserID,
                                  FinancialYear = row.FinancialYear,
                                  ScheduledOnID = row.ScheduledOnID
                              }).Distinct().ToList();
                    count = record.Count();
                }

                else if (flag == "ND")
                {
                    record = (from row in FailedControlQuarterWiseDisplayViewRecords
                              where row.PersonResponsible == userID && row.RoleID == 3 && PerformerstatusIds.Contains((int)row.AuditStatusID)
                             && row.TimeLine > DateTime.Today.Date
                              select new OpenCloseInternalAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = (int)row.CustomerBranchID,
                                  Branch = row.Branch,
                                  UserID = row.UserID,
                                  FinancialYear = row.FinancialYear,
                                  ScheduledOnID = row.ScheduledOnID
                              }).Distinct().ToList();
                    count = record.Count();
                }


                return count;
            }
        }
        public static int GetAuditCount(List<FailedControlQuarterWiseDisplayView> FailedControlQuarterWiseDisplayViewRecords, int userid, string flag, long customerid, int roleID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                List<OpenCloseInternalAuditDetailsClass> record = new List<OpenCloseInternalAuditDetailsClass>();
                List<int> PerformerstatusIds = new List<int>();
                PerformerstatusIds.Clear();
                PerformerstatusIds.Add(3);
                PerformerstatusIds.Add(4);
                if (flag == "D")
                {
                    record = (from row in FailedControlQuarterWiseDisplayViewRecords
                              where row.UserID == userid && PerformerstatusIds.Contains((int)row.AuditStatusID)
                              && row.RoleID == roleID && row.TimeLine <= DateTime.Today.Date
                              select new OpenCloseInternalAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = (int)row.CustomerBranchID,
                                  Branch = row.Branch,
                                  UserID = row.UserID,
                                  FinancialYear = row.FinancialYear,
                                  ScheduledOnID = row.ScheduledOnID
                              }).Distinct().ToList();
                    count = record.Count();
                }

                else if (flag == "ND")
                {
                    record = (from row in FailedControlQuarterWiseDisplayViewRecords
                              where row.UserID == userid
                              && row.RoleID == roleID && PerformerstatusIds.Contains((int)row.AuditStatusID)
                             && row.TimeLine > DateTime.Today.Date
                              select new OpenCloseInternalAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = (int)row.CustomerBranchID,
                                  Branch = row.Branch,
                                  UserID = row.UserID,
                                  FinancialYear = row.FinancialYear,
                                  ScheduledOnID = row.ScheduledOnID
                              }).Distinct().ToList();
                    count = record.Count();
                }
                return count;
            }
        }
        public static List<OpenCloseAuditDetailsClass> GetAudits(String FinYear, String Period, int Customerid, int CustBranchid, int userid, int statusid, int VerticalID, int roleID = 0)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();
                if (statusid == 1)
                {

                    entities.Database.CommandTimeout = 300;

                    var Masterrecord = (from row in entities.AuditCountViews
                                        select row).ToList();

                    record = (from C in Masterrecord
                              where C.UserID == userid
                              && C.ACCStatus == null
                              group C by new
                              {
                                  C.CustomerID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.Role,
                                  C.User,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  Role = GCS.Key.Role,
                                  User = GCS.Key.User,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                              }).ToList();

                    //record = (from row in entities.AuditSummaryCountViews
                    //          where row.UserID == userid
                    //          && row.ACCStatus == null
                    //          select new OpenCloseAuditDetailsClass()
                    //          {
                    //              CustomerID = row.CustomerID,
                    //              CustomerBranchID = row.CustomerBranchID,
                    //              Branch = row.Branch,
                    //              ForMonth = row.ForMonth,
                    //              RoleID = row.RoleID,
                    //              UserID = row.UserID,
                    //              Role = row.Role,
                    //              User = row.User,
                    //              FinancialYear = row.FinancialYear,
                    //              ExpectedStartDate = row.ExpectedStartDate,
                    //              ExpectedEndDate = row.ExpectedEndDate,
                    //              VerticalId = row.VerticalId,
                    //              VerticalName = row.VerticalName,
                    //          }).Distinct().ToList();

                }
                else if (statusid == 3)
                {
                    entities.Database.CommandTimeout = 300;
                    //var Masterrecord = (from row in entities.AuditSummaryCountViews
                    //                    select row).ToList();
                    var Masterrecord = (from row in entities.AuditCountViews
                                        select row).ToList();

                    record = (from C in Masterrecord
                              where C.UserID == userid
                              //&& C.AuditStatusID==3
                              && C.ACCStatus == 1
                              group C by new
                              {
                                  C.CustomerID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.Role,
                                  C.User,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  Role = GCS.Key.Role,
                                  User = GCS.Key.User,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                              }).ToList();

                    //record = (from row in entities.AuditSummaryCountViews
                    //          where row.UserID == userid && row.AuditStatusID == 3
                    //          && row.ACCStatus == 1
                    //          select new OpenCloseAuditDetailsClass()
                    //          {
                    //              CustomerID = row.CustomerID,
                    //              CustomerBranchID = row.CustomerBranchID,
                    //              Branch = row.Branch,
                    //              ForMonth = row.ForMonth,
                    //              RoleID = row.RoleID,
                    //              UserID = row.UserID,
                    //              Role = row.Role,
                    //              User = row.User,
                    //              FinancialYear = row.FinancialYear,
                    //              ExpectedStartDate = row.ExpectedStartDate,
                    //              ExpectedEndDate = row.ExpectedEndDate,
                    //              VerticalId = row.VerticalId,
                    //              VerticalName = row.VerticalName,
                    //          }).Distinct().ToList();

                }
                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();
                if (CustBranchid != -1)
                    record = record.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();
                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();
                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();
                if (roleID != -1)
                    record = record.Where(Entry => Entry.RoleID == roleID).ToList();

                if (record.Count > 0)
                    record = record.OrderBy(Entry => Entry.Branch).ThenBy(entry => entry.VerticalName).ThenBy(entry => entry.FinancialYear).ToList();

                return record;
            }
        }

        public static List<OpenCloseAuditDetailsClass> GetAudits(String FinYear, String Period, int Customerid, List<long> BranchList, int userid, int statusid, int VerticalID, int roleID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();
                if (statusid == 1)
                {
                    entities.Database.CommandTimeout = 300;
                    //var masterrecord = (from row in entities.auditsummarycountviews
                    //                    select row).tolist();

                    var Masterrecord = (from row in entities.AuditCountViews
                                        select row).ToList();

                    record = (from C in Masterrecord
                              where C.UserID == userid
                              && C.ACCStatus == null
                              group C by new
                              {
                                  C.CustomerID,
                                  C.AuditID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.Role,
                                  C.User,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName,
                                  C.UserName,
                                  C.CProcessName

                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  AuditID = GCS.Key.AuditID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  Role = GCS.Key.Role,
                                  User = GCS.Key.User,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                                  UserName = GCS.Key.UserName,
                                  CProcessName = GCS.Key.CProcessName
                              }).ToList();

                    Masterrecord.Clear();
                    Masterrecord = null;
                }
                else if (statusid == 3)
                {
                    entities.Database.CommandTimeout = 300;

                    //var Masterrecord = (from row in entities.AuditSummaryCountViews
                    //                    select row).ToList();

                    var Masterrecord = (from row in entities.AuditCountViews
                                        select row).ToList();

                    record = (from C in Masterrecord
                              where C.UserID == userid
                              && C.ACCStatus == 1
                              //&& C.AuditStatusID==3
                              group C by new
                              {
                                  C.CustomerID,
                                  C.AuditID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.Role,
                                  C.User,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName,
                                  C.UserName,
                                  C.CProcessName
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  AuditID = GCS.Key.AuditID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  Role = GCS.Key.Role,
                                  User = GCS.Key.User,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                                  UserName = GCS.Key.UserName,
                                  CProcessName = GCS.Key.CProcessName
                              }).ToList();

                    Masterrecord.Clear();
                    Masterrecord = null;
                }

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains((int)Entry.CustomerBranchID)).ToList();

                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();

                if (AuditID != -1)
                    record = record.Where(Entry => Entry.AuditID == AuditID).ToList();

                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                if (roleID != -1)
                    record = record.Where(Entry => Entry.RoleID == roleID).ToList();

                if (record.Count > 0)
                    record = record.OrderBy(Entry => Entry.Branch).ThenBy(entry => entry.VerticalName).ThenBy(entry => entry.FinancialYear).ToList();

                return record;
            }
        }
        public static List<OpenCloseAuditDetailsClass> GetAudits(String FinYear, String Period, int Customerid, List<long> BranchList, int userid, int statusid, int VerticalID, int roleID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();
                if (statusid == 1)
                {
                    entities.Database.CommandTimeout = 300;
                    //var masterrecord = (from row in entities.auditsummarycountviews
                    //                    select row).tolist();

                    var Masterrecord = (from row in entities.AuditCountViews
                                        select row).ToList();

                    record = (from C in Masterrecord
                              where C.UserID == userid
                              && C.ACCStatus == null
                              group C by new
                              {
                                  C.CustomerID,
                                  C.AuditID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.Role,
                                  C.User,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName,
                                  C.UserName,
                                  C.CProcessName,
                                  C.SubProcess,
                                  AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,

                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  AuditID = GCS.Key.AuditID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  Role = GCS.Key.Role,
                                  User = GCS.Key.User,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                                  UserName = GCS.Key.UserName,
                                  CProcessName = GCS.Key.CProcessName,
                                  SubProcess = GCS.Key.SubProcess,
                                  AuditName = GCS.Key.AuditName,
                              }).Distinct().ToList();

                    Masterrecord.Clear();
                    Masterrecord = null;
                }
                else if (statusid == 3)
                {
                    entities.Database.CommandTimeout = 300;

                    //var Masterrecord = (from row in entities.AuditSummaryCountViews
                    //                    select row).ToList();

                    var Masterrecord = (from row in entities.AuditCountViews
                                        select row).ToList();

                    record = (from C in Masterrecord
                              where C.UserID == userid
                              && C.ACCStatus == 1
                              //&& C.AuditStatusID==3
                              group C by new
                              {
                                  C.CustomerID,
                                  C.AuditID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.Role,
                                  C.User,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName,
                                  C.UserName,
                                  C.CProcessName,
                                  C.SubProcess,
                                  AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  AuditID = GCS.Key.AuditID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  Role = GCS.Key.Role,
                                  User = GCS.Key.User,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                                  UserName = GCS.Key.UserName,
                                  CProcessName = GCS.Key.CProcessName,
                                  SubProcess = GCS.Key.SubProcess,
                                  AuditName = GCS.Key.AuditName,
                              }).Distinct().ToList();

                    Masterrecord.Clear();
                    Masterrecord = null;
                }

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains((int)Entry.CustomerBranchID)).ToList();

                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();


                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                //if (roleID != -1)
                //    record = record.Where(Entry => Entry.RoleID == roleID).ToList();

                if (roleID != -1)
                {
                    if (roleID == 5)
                    {
                        record = record.Where(Entry => Entry.RoleID == 5).ToList();
                    }
                    if (roleID == 4)
                    {
                        record = record.Where(Entry => Entry.RoleID == 4).ToList();
                    }
                    else
                    {
                        record = record.Where(Entry => Entry.RoleID == roleID).ToList();
                    }
                }
                if (record.Count > 0)
                    record = record.OrderBy(Entry => Entry.Branch).ThenBy(entry => entry.VerticalName).ThenBy(entry => entry.FinancialYear).ToList();

                return record;
            }
        }

        public static List<AssignedUserwiseAuditList> GetAuditsAssignedUser(String FinYear, String Period, int Customerid, List<long> BranchList, int userid, int statusid, int VerticalID, int roleID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AssignedUserwiseAuditList> record = new List<AssignedUserwiseAuditList>();
                entities.Database.CommandTimeout = 300;
                var Masterrecord = (from row in entities.Sp_UserwiseAssignedAudit(userid)
                                    select row).ToList();

                record = (from C in Masterrecord
                          group C by new
                          {
                              C.AuditID,
                              C.CustomerBranchID,
                              C.Branch,
                              C.ForMonth,
                              C.RoleID,
                              C.UserID,
                              C.Role,
                              C.FinancialYear,
                              C.VerticalId,
                              C.VerticalName,
                              C.UserName,
                              C.ProcessId,
                              C.ProcessName,
                              C.SubProcessId,
                              C.SubProcessName,
                              //C.ATBDID
                          } into GCS
                          select new AssignedUserwiseAuditList()
                          {
                              AuditID = GCS.Key.AuditID,
                              CustomerBranchID = GCS.Key.CustomerBranchID,
                              Branch = GCS.Key.Branch,
                              ForMonth = GCS.Key.ForMonth,
                              RoleID = GCS.Key.RoleID,
                              UserID = GCS.Key.UserID,
                              Role = GCS.Key.Role,
                              User = GCS.Key.UserName,
                              FinancialYear = GCS.Key.FinancialYear,
                              VerticalId = (int)GCS.Key.VerticalId,
                              VerticalName = GCS.Key.VerticalName,
                              UserName = GCS.Key.UserName,
                              ProcessName = GCS.Key.ProcessName,
                              ProcessId = (int)GCS.Key.ProcessId,
                              SubProcessID = (int)GCS.Key.SubProcessId,
                              SubProcessName = GCS.Key.SubProcessName,
                              // ATBDID = GCS.Key.ATBDID
                          }).Distinct().ToList();

                Masterrecord.Clear();
                Masterrecord = null;


                //if (Customerid != -1)
                //    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains((int)Entry.CustomerBranchID)).ToList();

                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();


                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                //if (roleID != -1)
                //    record = record.Where(Entry => Entry.RoleID == roleID).ToList();

                if (roleID != -1)
                {
                    if (roleID == 4 || roleID == 5)
                    {
                        record = record.Where(Entry => Entry.RoleID == 5 || Entry.RoleID == 4).ToList();
                    }
                    else
                    {
                        record = record.Where(Entry => Entry.RoleID == roleID).ToList();
                    }
                }
                if (record.Count > 0)
                    record = record.OrderBy(Entry => Entry.Branch).ThenBy(entry => entry.VerticalName).ThenBy(entry => entry.FinancialYear).ToList();

                return record;
            }
        }




        public static bool DeleteFileIMP(string path, int file)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                ImplementationFileData_Risk fileToUpdate = new ImplementationFileData_Risk() { ID = file };
                entities.ImplementationFileData_Risk.Attach(fileToUpdate);
                fileToUpdate.IsDeleted = true;
                entities.SaveChanges();
            }
            return true;
        }
        public static bool DeleteFile(string path, int file)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var filedata = (from row in entities.InternalFileData_Risk
                                where row.ID == file
                                select row).FirstOrDefault();

                if (filedata != null)
                {
                    filedata.IsDeleted = true;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static List<DeptDashboardSummaryview> GetPRDashboardSummary(int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var transactionsQuery = (from row in entities.DeptDashboardSummaryviews
                                         where (row.PersonResponsible == userid)
                                         select row).ToList();
                return transactionsQuery;
            }
        }
        public static List<DeptDashboardSummaryview> GetDeptDashboardSummary(int deptid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var transactionsQuery = (from row in entities.DeptDashboardSummaryviews
                                         where (row.DepartmentID == deptid)
                                         select row).ToList();
                return transactionsQuery;
            }
        }
        public static List<AuditDashboardSummaryview> GetAuditDashboardSummary()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var transactionsQuery = (from row in entities.AuditDashboardSummaryviews
                                             //  where (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select row).ToList();
                return transactionsQuery;
            }
        }
        public static List<ReviewHistory> GetAllRemarks(int RiskCreationId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.ReviewHistories
                                  where row.RiskCreationId == RiskCreationId
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<ReviewHistory> GetAllRemarks(int RiskCreationId, string FinancialYear, string Period)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.ReviewHistories
                                  where row.RiskCreationId == RiskCreationId
                                  && row.FinancialYear == FinancialYear && row.ForMonth == Period
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<ImplementationReviewHistory> GetImplementationReviewAllRemarks(string ForPeriod, string FinancialYear, int ScheduledOnID, int ImplementationInstance, int ResultID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.ImplementationReviewHistories
                                  where row.ForMonth == ForPeriod && row.FinancialYear == FinancialYear
                                  && row.ImplementationScheduleOnID == ScheduledOnID
                                  && row.ImplementationInstance == ImplementationInstance
                                  && row.ResultID == ResultID
                                  select row).ToList();

                return statusList;
            }
        }
        public static List<InternalReviewHistory> GetInternalAllRemarks(int ProcessId, int ScheduledOnID, int ATBDId, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.InternalReviewHistories
                                  where row.ProcessId == ProcessId
                                  && row.AuditScheduleOnID == ScheduledOnID
                                  && row.ATBDId == ATBDId
                                  && row.AuditID == AuditID
                                  select row).ToList();

                return statusList;
            }
        }
        public static AuditInstanceTransactionView GetForMonth(int ScheduledOnID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                AuditInstanceTransactionView ScheduleOnData = (from row in entities.AuditInstanceTransactionViews
                                                               where row.ScheduledOnID == ScheduledOnID
                                                               select row).FirstOrDefault();

                if (string.IsNullOrEmpty(ScheduleOnData.ForMonth))
                {
                    //var complianceSchedule = ComplianceManagement.GetScheduleByComplianceID(ScheduleOnData.ComplianceID);

                    //if (complianceSchedule.Count > 0)// added by rahul on 11 Jan 2016 for download one time document error
                    //{
                    //var objCompliance = ComplianceManagement.GetByID(ScheduleOnData.ComplianceID);

                    //string spacialDate = ScheduleOnData.ScheduledOn.Day.ToString("00") + ScheduleOnData.ScheduledOn.Month.ToString("00");
                    //var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                    // string forMonth = string.Empty;
                    //forMonth = ComplianceManagement.GetForMonth(ScheduleOnData.ScheduledOn, ForMonthSchedule.ForMonth, objCompliance.Frequency);
                    ScheduleOnData.ForMonth = ScheduleOnData.ForMonth + " " + ScheduleOnData.FinancialYear;
                    //}
                }

                return ScheduleOnData;
            }
        }

        public static InternalAuditInstanceTransactionView GetForMonthInternalAuditInstanceTransaction(int ScheduledOnID, int ATBDID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                InternalAuditInstanceTransactionView ScheduleOnData = (from row in entities.InternalAuditInstanceTransactionViews
                                                                       where row.ScheduledOnID == ScheduledOnID && row.ATBDId == ATBDID
                                                                       && row.AuditID == AuditID
                                                                       select row).FirstOrDefault();

                if (string.IsNullOrEmpty(ScheduleOnData.ForMonth))
                {
                    //var complianceSchedule = ComplianceManagement.GetScheduleByComplianceID(ScheduleOnData.ComplianceID);

                    //if (complianceSchedule.Count > 0)// added by rahul on 11 Jan 2016 for download one time document error
                    //{
                    //var objCompliance = ComplianceManagement.GetByID(ScheduleOnData.ComplianceID);

                    //string spacialDate = ScheduleOnData.ScheduledOn.Day.ToString("00") + ScheduleOnData.ScheduledOn.Month.ToString("00");
                    //var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                    // string forMonth = string.Empty;
                    //forMonth = ComplianceManagement.GetForMonth(ScheduleOnData.ScheduledOn, ForMonthSchedule.ForMonth, objCompliance.Frequency);
                    ScheduleOnData.ForMonth = ScheduleOnData.ForMonth + " " + ScheduleOnData.FinancialYear;
                    //}
                }

                return ScheduleOnData;
            }
        }


        public static List<ImplementationAuditTransactionView> GetAllImplementationAuditTransactionViews(int ScheduledOnID, int ResultID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.ImplementationAuditTransactionViews
                                  where row.AuditID == AuditID
                                  && row.ImplementationScheduleOnID == ScheduledOnID
                                  && row.AuditStatusID != null && (row.ResultID == ResultID || row.ResultID == null)
                                  && row.StatusChangedOn != null
                                  orderby row.AuditTransactionID descending
                                  select row).Distinct().ToList();

                return statusList;
            }
        }
        public static List<InternalAuditTransactionView> GetAllInternalAuditTransactions(int ScheduledOnID, int ATBDID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.InternalAuditTransactionViews
                                  where row.AuditScheduleOnID == ScheduledOnID
                                  && row.AuditID == AuditID
                                  && row.AuditStatusID != null && (row.ATBDId == ATBDID || row.ATBDId == null)
                                  orderby row.AuditTransactionID descending
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<AuditTransactionView> GetAllTransactions(int ScheduledOnID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.AuditTransactionViews
                                  where row.AuditScheduleOnID == ScheduledOnID
                                  orderby row.AuditTransactionID descending
                                  select row).ToList();

                return statusList;
            }
        }
        public static List<GetAuditDocumentsView> GetFileData1(int ScheduledOnID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.GetAuditDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID
                                select row).ToList();

                return fileData;
            }
        }
        //public static List<SP_GetPersonResponsible_Result> GetFileDataPersonResponsibleObservationStatus(int UserID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        List<SP_GetPersonResponsible_Result> fileData=new List<SP_GetPersonResponsible_Result>();
        //         fileData = entities.SP_GetPersonResponsible(UserID).ToList();
        //         fileData= fileData.Where(row => row.PersonResponsible == UserID && row.AuditStatusID == 6
        //                        && row.UserID != UserID).Distinct().ToList();
        //        return fileData;
        //    }
        //}
        public static List<SP_GetPersonResponsibleIMPelementation_Result> GetFileDataPersonResponsibleObservationIMPelementationStatus(int UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SP_GetPersonResponsibleIMPelementation_Result> fileData = new List<SP_GetPersonResponsibleIMPelementation_Result>();
                fileData = entities.SP_GetPersonResponsibleIMPelementation(UserID).ToList();
                fileData = fileData.Where(row => row.PersonResponsible == UserID && row.AuditStatusID == 6
                               && row.UserID != UserID).Distinct().ToList();
                return fileData;
            }
        }
        //public static List<PersonResponsibleObservationStatu> GetFileDataGetPersonResponsibleObservationStatus(int UserID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var fileData = (from row in entities.PersonResponsibleObservationStatus
        //                        where row.PersonResponsible == UserID && row.AuditStatusID == 6
        //                        && row.UserID != UserID
        //                        select row).Distinct().ToList();
        //        return fileData;
        //    }
        //}
        public static List<Tran_FinalDeliverableUpload> GetFileDataGetTran_FinalDeliverableUpload(string FinancialYear, string Period, int CustomerBranchId, int VerticalId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.Tran_FinalDeliverableUpload
                                where row.CustomerBranchId == CustomerBranchId && row.FinancialYear == FinancialYear
                                && row.Period == Period && row.VerticalID == VerticalId
                                select row).ToList();
                return fileData;
            }
        }
        public static List<Tran_AuditCommitePresantionUpload> GetFileDataGetTran_AuditCommitePresantionUpload(string FinancialYear, string Period, int CustomerBranchId, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.Tran_AuditCommitePresantionUpload
                                where row.CustomerBranchId == CustomerBranchId && row.FinancialYear == FinancialYear
                                && row.Period == Period && row.VerticalID == VerticalID
                                select row).ToList();
                return fileData;
            }
        }
        public static List<Tran_AnnxetureUpload> GetFileDataGetTran_AnnxetureUpload(int ATBDID, string FinancialYear, string Period, int ProcesId, int Customerbranchid, int VerticalID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.Tran_AnnxetureUpload
                                where row.ATBDId == ATBDID && row.FinancialYear == FinancialYear
                                && row.Period == Period && row.ProcessId == ProcesId && row.IsDeleted == false
                                && row.CustomerBranchId == Customerbranchid && row.VerticalID == VerticalID
                                && row.AuditID == AuditID
                                select row).ToList();
                return fileData;
            }
        }

        public static List<GetImplementationAuditDocumentsView> GetFileDataGetImplementationAuditDocumentsView(int ScheduledOnID, int ResultId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.GetImplementationAuditDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID && row.ResultId == ResultId
                                select row).ToList();
                return fileData;
            }
        }
        public static List<GetInternalAuditDocumentsView> GetFileDataGetInternalAuditDocumentsView(int ScheduledOnID, int ATBDID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.GetInternalAuditDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID && row.ATBDId == ATBDID
                                && row.AuditID == AuditID
                                select row).ToList();
                return fileData;
            }
        }
        public static List<GetInternalAuditDocumentsView> GetFileDataGetInternalAuditDocumentsView1(int ScheduledOnID, int ATBDID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.GetInternalAuditDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID && row.ATBDId == ATBDID
                                select row).ToList();
                return fileData;
            }
        }


        //add new in DashboardManagementRisk
        // comment on 16-06-2020
        //public static List<FailedControlQuarterWiseDisplayView> DashboardDataForFailedControlTestingPR(int userID, string filter, long BranchID, long ProcessId, long SubprocessId, string FinacialYear, string Key, long quarter, long AuditStatusId, int customerID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        List<FailedControlQuarterWiseDisplayView> transactionsQuery = new List<FailedControlQuarterWiseDisplayView>();
        //        List<int> PerformerstatusIds = new List<int>();
        //        PerformerstatusIds.Clear();
        //        PerformerstatusIds.Add(3);
        //        PerformerstatusIds.Add(4);
        //        if (filter == "D")
        //        {
        //            transactionsQuery = (from row in entities.FailedControlQuarterWiseDisplayViews
        //                                 where PerformerstatusIds.Contains((int)row.AuditStatusID) && row.PersonResponsible == userID
        //                                 && row.CustomerID == customerID && row.TimeLine <= DateTime.Today.Date
        //                                 select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //        }
        //        else if (filter == "ND")
        //        {
        //            transactionsQuery = (from row in entities.FailedControlQuarterWiseDisplayViews
        //                                 where PerformerstatusIds.Contains((int)row.AuditStatusID) && row.PersonResponsible == userID
        //                                 && row.CustomerID == customerID && row.TimeLine > DateTime.Today.Date
        //                                 select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //        }
        //        if (BranchID != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == BranchID).ToList();
        //        }
        //        if (!string.IsNullOrEmpty(FinacialYear))
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.FinancialYear == FinacialYear).ToList();
        //        }
        //        if (quarter != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.QuaterCount == quarter).ToList();
        //        }
        //        if (ProcessId != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.ProcessId == ProcessId).ToList();
        //        }
        //        if (SubprocessId != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.SubProcessId == SubprocessId).ToList();
        //        }
        //        if (!string.IsNullOrEmpty(Key))
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.KeyName == Key).ToList();
        //        }
        //        if (AuditStatusId != -1)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.AuditStatusID == AuditStatusId).ToList();
        //        }
        //        return transactionsQuery.ToList();
        //    }
        //}

        // create duplicate method and change code
        // change on 16-06-2020
        public static List<FailedControlQuarterWiseDisplayView> DashboardDataForFailedControlTestingPR(int userID, string filter, List<long> BranchIDList, long ProcessId, long SubprocessId, string FinacialYear, string Key, long quarter, long AuditStatusId, int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<FailedControlQuarterWiseDisplayView> transactionsQuery = new List<FailedControlQuarterWiseDisplayView>();
                List<int> PerformerstatusIds = new List<int>();
                PerformerstatusIds.Clear();
                PerformerstatusIds.Add(3);
                PerformerstatusIds.Add(4);
                if (filter == "D")
                {
                    transactionsQuery = (from row in entities.FailedControlQuarterWiseDisplayViews
                                         where PerformerstatusIds.Contains((int)row.AuditStatusID) && row.PersonResponsible == userID
                                         && row.CustomerID == customerID && row.TimeLine <= DateTime.Today.Date
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (filter == "ND")
                {
                    transactionsQuery = (from row in entities.FailedControlQuarterWiseDisplayViews
                                         where PerformerstatusIds.Contains((int)row.AuditStatusID) && row.PersonResponsible == userID
                                         && row.CustomerID == customerID && row.TimeLine > DateTime.Today.Date
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (BranchIDList.Count > 0)
                {
                    transactionsQuery = transactionsQuery.Where(entry => BranchIDList.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!string.IsNullOrEmpty(FinacialYear))
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.FinancialYear == FinacialYear).ToList();
                }
                if (quarter != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.QuaterCount == quarter).ToList();
                }
                if (ProcessId != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.ProcessId == ProcessId).ToList();
                }
                if (SubprocessId != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                }
                if (!string.IsNullOrEmpty(Key))
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.KeyName == Key).ToList();
                }
                if (AuditStatusId != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.AuditStatusID == AuditStatusId).ToList();
                }
                return transactionsQuery.ToList();
            }
        }
        public static List<FailedControlQuarterWiseDisplayView> DashboardDataForFailedControlTesting(int userID, string filter, long BranchID, long ProcessId, long SubprocessId, string FinacialYear, string Key, long quarter, long AuditStatusId, int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> PerformerstatusIds = new List<int>();
                PerformerstatusIds.Clear();
                PerformerstatusIds.Add(3);
                PerformerstatusIds.Add(4);
                List<FailedControlQuarterWiseDisplayView> transactionsQuery = new List<FailedControlQuarterWiseDisplayView>();

                if (filter == "D")
                {
                    transactionsQuery = (from row in entities.FailedControlQuarterWiseDisplayViews
                                         where row.UserID == userID && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                         && row.CustomerID == customerID && row.TimeLine <= DateTime.Today.Date
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (filter == "ND")
                {
                    transactionsQuery = (from row in entities.FailedControlQuarterWiseDisplayViews
                                         where row.UserID == userID && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                         && row.CustomerID == customerID && row.TimeLine > DateTime.Today.Date
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (BranchID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == BranchID).ToList();
                }
                if (!string.IsNullOrEmpty(FinacialYear))
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.FinancialYear == FinacialYear).ToList();
                }
                if (quarter != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.QuaterCount == quarter).ToList();
                }
                if (ProcessId != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.ProcessId == ProcessId).ToList();
                }
                if (SubprocessId != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.SubProcessId == SubprocessId).ToList();
                }
                if (!string.IsNullOrEmpty(Key))
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.KeyName == Key).ToList();
                }
                if (AuditStatusId != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.AuditStatusID == AuditStatusId).ToList();
                }
                return transactionsQuery.ToList();
            }
        }

        public static List<AuditInstanceTransactionView> DashboardData(int userID, int roleID, string filter, long BranchID, long ProcessId, long SubprocessId, string FinacialYear, string Key, string Period, long AuditStatusId, int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditInstanceTransactionView> transactionsQuery = new List<AuditInstanceTransactionView>();
                entities.Database.CommandTimeout = 300;
                transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                     where row.ForMonth == Period && row.UserID == userID
                                     && row.RoleID == roleID
                                     && row.CustomerID == customerID
                                     select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                //if (customerID != -1)
                //    transactionsQuery = transactionsQuery.Where(Entry => Entry.CustomerID == customerID).ToList();

                //if (roleID != -1)
                //    transactionsQuery = transactionsQuery.Where(Entry => Entry.RoleID == roleID).ToList();

                //if (userID != -1)
                //    transactionsQuery = transactionsQuery.Where(Entry => Entry.UserID == userID).ToList();

                if (AuditStatusId != 0 || AuditStatusId != -1)
                    transactionsQuery = transactionsQuery.Where(Entry => Entry.AuditStatusID == AuditStatusId).ToList();

                if (BranchID != -1)
                    transactionsQuery = transactionsQuery.Where(Entry => Entry.CustomerBranchID == BranchID).ToList();

                if (ProcessId != -1)
                    transactionsQuery = transactionsQuery.Where(Entry => Entry.ProcessId == ProcessId).ToList();

                if (SubprocessId != -1)
                    transactionsQuery = transactionsQuery.Where(Entry => Entry.SubProcessId == SubprocessId).ToList();

                //if (BranchID != -1)
                //    transactionsQuery = transactionsQuery.Where(Entry => Entry.CustomerBranchID == BranchID).ToList();

                if (!string.IsNullOrEmpty(FinacialYear))
                    transactionsQuery = transactionsQuery.Where(Entry => Entry.FinancialYear == FinacialYear).ToList();

                //if (!string.IsNullOrEmpty(Period))
                //    transactionsQuery = transactionsQuery.Where(Entry => Entry.ForMonth == Period && FrequencyIds.Contains((int) Entry.Frequency)).ToList();

                return transactionsQuery;
            }
        }
        //Satuatory
        public static List<AuditInstanceTransactionView> DashboardDataForPerformer(int userID, string filter, long BranchID, long ProcessId, long SubprocessId, string FinacialYear, string Key, string Period, long AuditStatusId, int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditInstanceTransactionView> transactionsQuery = new List<AuditInstanceTransactionView>();
                int performerRoleID = (from row in entities.mst_Role
                                       where row.Code == "PERF"
                                       select row.ID).Single();
                List<int> PerformerstatusIds = new List<int>();
                PerformerstatusIds.Clear();
                PerformerstatusIds.Add(1);
                PerformerstatusIds.Add(4);

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);


                List<int> FrequencyIds = new List<int>();
                if (Period == "Apr - Jun")
                {
                    FrequencyIds.Clear();
                    FrequencyIds.Add(1);
                    FrequencyIds.Add(2);
                    FrequencyIds.Add(3);
                    FrequencyIds.Add(4);
                    FrequencyIds.Add(5);

                }
                else if (Period == "Jul - Sep")
                {
                    FrequencyIds.Clear();
                    FrequencyIds.Add(1);
                    FrequencyIds.Add(2);
                    FrequencyIds.Add(3);
                    FrequencyIds.Add(4);
                    FrequencyIds.Add(5);
                    FrequencyIds.Add(6);
                }
                else if (Period == "Oct - Dec")
                {
                    FrequencyIds.Clear();
                    FrequencyIds.Add(1);
                    FrequencyIds.Add(2);
                    FrequencyIds.Add(3);
                    FrequencyIds.Add(4);
                    FrequencyIds.Add(5);
                    FrequencyIds.Add(6);
                }
                else if (Period == "Jan - Mar")
                {
                    FrequencyIds.Clear();
                    FrequencyIds.Add(1);
                    FrequencyIds.Add(2);
                    FrequencyIds.Add(3);
                    FrequencyIds.Add(4);
                    FrequencyIds.Add(5);
                    FrequencyIds.Add(6);
                    FrequencyIds.Add(7);
                    FrequencyIds.Add(9);
                }
                else
                {
                    FrequencyIds.Clear();
                    FrequencyIds.Add(1);
                    FrequencyIds.Add(2);
                    FrequencyIds.Add(3);
                    FrequencyIds.Add(4);
                    FrequencyIds.Add(5);
                    FrequencyIds.Add(6);
                    FrequencyIds.Add(7);
                }
                if (filter == "All")
                {
                    #region All
                    transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                          && row.CustomerID == customerID
                                          && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                          && FrequencyIds.Contains((int)row.Frequency)
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    #endregion
                }
                else if (filter == "Location")
                {
                    #region Customer Location Wise
                    if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                    #endregion
                }
                else if (filter == "FinacialYear")
                {
                    #region Financial Year
                    if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.FinancialYear == FinacialYear && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                    #endregion
                }
                else if (filter == "Period")
                {
                    #region Period
                    if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.CustomerID == customerID
                                               && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.CustomerID == customerID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    #endregion
                }
                else if (filter == "Process")
                {
                    #region Process
                    if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.CustomerID == customerID
                                               && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.CustomerID == customerID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    #endregion
                }
                else if (filter == "SubProcess")
                {
                    #region Sub Process
                    if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.CustomerID == customerID
                                               && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.CustomerID == customerID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    #endregion
                }
                else if (filter == "KeyNonKey")
                {
                    #region KeyNonkey
                    if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(Key))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period && row.KeyName == Key
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Key))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.KeyName == Key
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.CustomerID == customerID
                                               && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.CustomerID == customerID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    #endregion
                }
                else if (filter == "Status")
                {
                    #region Status
                    if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(Key) && AuditStatusId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period && row.KeyName == Key
                                              && row.AuditStatusID == AuditStatusId
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(Key))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period && row.KeyName == Key
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period) && AuditStatusId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && row.AuditStatusID == AuditStatusId
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Key) && AuditStatusId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.KeyName == Key
                                              && row.AuditStatusID == AuditStatusId
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Key) && !string.IsNullOrEmpty(Period) && AuditStatusId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                               && row.KeyName == Key && row.ForMonth == Period
                                              && row.AuditStatusID == AuditStatusId
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Key))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.KeyName == Key
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (AuditStatusId != -1 && !string.IsNullOrEmpty(Period) && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && row.AuditStatusID == AuditStatusId && row.ForMonth == Period
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                             && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                              && row.ProcessId == ProcessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.CustomerID == customerID
                                               && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID && row.RoleID == performerRoleID
                                               && row.CustomerID == customerID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    #endregion
                }

                var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();

                List<AuditInstanceTransactionView> newList = new List<AuditInstanceTransactionView>();
                foreach (var loc in locations)
                {
                    AuditInstanceTransactionView BranchRow = new AuditInstanceTransactionView();

                    BranchRow.ActivityDescription = loc.Branch;
                    BranchRow.ControlObjective = "";
                    BranchRow.RiskCreationId = -1;
                    BranchRow.UserID = -1;
                    BranchRow.RoleID = -1;
                    BranchRow.AuditStatusID = -1;
                    newList.Add(BranchRow);

                    List<AuditInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).ToList();
                    foreach (var item1 in temptransaction)
                    {
                        newList.Add(item1);
                    }
                }

                return newList.ToList();
            }
        }
        public static List<AuditInstanceTransactionView> DashboardDataForPerformerDisplayCount(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                int performerRoleID = (from row in entities.mst_Role
                                       where row.Code == "PERF"
                                       select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == performerRoleID
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                transactionsQuery = transactionsQuery.Where(entry => (entry.AuditStatusID == 1));
                return transactionsQuery.ToList();
            }
        }
        public static List<AuditInstanceTransactionView> DashboardDataForReviewerDisplayCount(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var reviewerRoleIDs = (from row in entities.mst_Role
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();


                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         where row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID)
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                transactionsQuery = transactionsQuery.Where(entry => entry.AuditStatusID == 2);
                //if (filter == CannedReportFilterForPerformer.PendingForApproval)
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5);
                //}
                //else if (pending)
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn <= now);
                //}
                //else
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn <= nextOneMonth && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11));
                //}
                return transactionsQuery.ToList();
            }
        }
        //
        public static List<AuditInstanceTransactionView> DashboardDataForReviewer(int userID, string filter, long BranchID, long ProcessId, long SubprocessId, string FinacialYear, string Key, string Period, long AuditStatusId, int customerID, int ReporteeId)
        {


            using (AuditControlEntities entities = new AuditControlEntities())
            {

                //List<AuditInstanceTransactionView> transactionsQuery = new List<AuditInstanceTransactionView>();
                //    var reviewerRoleIDs = (from row in entities.mst_Role
                //                           where row.Code.StartsWith("RVW")
                //                           select row.ID).ToList();

                List<AuditInstanceTransactionView> transactionsQuery = new List<AuditInstanceTransactionView>();
                var reviewerRoleIDs = (from row in entities.mst_Role
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();
                List<int> PerformerstatusIds = new List<int>();
                PerformerstatusIds.Clear();
                PerformerstatusIds.Add(2);
                PerformerstatusIds.Add(4);


                //    List<int> reviewerstatusIds = new List<int>();
                //    reviewerstatusIds.Clear();
                //    reviewerstatusIds.Add(2);
                //    reviewerstatusIds.Add(4);

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);


                List<int> FrequencyIds = new List<int>();
                if (Period == "Apr - Jun")
                {
                    FrequencyIds.Clear();
                    FrequencyIds.Add(1);
                    FrequencyIds.Add(2);
                    FrequencyIds.Add(3);
                    FrequencyIds.Add(4);
                    FrequencyIds.Add(5);

                }
                else if (Period == "Jul - Sep")
                {
                    FrequencyIds.Clear();
                    FrequencyIds.Add(1);
                    FrequencyIds.Add(2);
                    FrequencyIds.Add(3);
                    FrequencyIds.Add(4);
                    FrequencyIds.Add(5);
                    FrequencyIds.Add(6);
                }
                else if (Period == "Oct - Dec")
                {
                    FrequencyIds.Clear();
                    FrequencyIds.Add(1);
                    FrequencyIds.Add(2);
                    FrequencyIds.Add(3);
                    FrequencyIds.Add(4);
                    FrequencyIds.Add(5);
                    FrequencyIds.Add(6);
                }
                else if (Period == "Jan - Mar")
                {
                    FrequencyIds.Clear();
                    FrequencyIds.Add(1);
                    FrequencyIds.Add(2);
                    FrequencyIds.Add(3);
                    FrequencyIds.Add(4);
                    FrequencyIds.Add(5);
                    FrequencyIds.Add(6);
                    FrequencyIds.Add(7);
                }
                else
                {
                    FrequencyIds.Clear();
                    FrequencyIds.Add(1);
                    FrequencyIds.Add(2);
                    FrequencyIds.Add(3);
                    FrequencyIds.Add(4);
                    FrequencyIds.Add(5);
                    FrequencyIds.Add(6);
                    FrequencyIds.Add(7);
                }
                if (filter == "All")
                {
                    #region All
                    transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                          && row.CustomerID == customerID
                                          && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                          && FrequencyIds.Contains((int)row.Frequency)
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    #endregion
                }
                else if (filter == "Location")
                {
                    #region Customer Location Wise
                    if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                    #endregion
                }
                else if (filter == "FinacialYear")
                {
                    #region Financial Year
                    if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.FinancialYear == FinacialYear && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                    #endregion
                }
                else if (filter == "Reportee")
                {
                    #region Reportee
                    if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.FinancialYear == FinacialYear && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                    #endregion
                }
                else if (filter == "Period")
                {
                    #region Period
                    if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.CustomerID == customerID
                                               && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.CustomerID == customerID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    #endregion
                }
                else if (filter == "Process")
                {
                    #region Process
                    if (ProcessId != -1 && BranchID != -1 && ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.CustomerID == customerID
                                               && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.CustomerID == customerID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    #endregion
                }
                else if (filter == "SubProcess")
                {
                    #region Sub Process
                    if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.CustomerID == customerID
                                               && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.CustomerID == customerID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    #endregion
                }
                else if (filter == "KeyNonKey")
                {
                    #region KeyNonkey
                    if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(Key))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period && row.KeyName == Key
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Key))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.KeyName == Key
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.CustomerID == customerID
                                               && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.CustomerID == customerID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    #endregion
                }
                else if (filter == "Status")
                {
                    #region Status
                    if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(Key) && AuditStatusId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period && row.KeyName == Key
                                              && row.AuditStatusID == AuditStatusId
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(Key))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period && row.KeyName == Key
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(Key))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period && row.KeyName == Key
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period) && AuditStatusId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && row.AuditStatusID == AuditStatusId
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Key) && AuditStatusId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.KeyName == Key
                                              && row.AuditStatusID == AuditStatusId
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Key) && !string.IsNullOrEmpty(Period) && AuditStatusId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                               && row.KeyName == Key && row.ForMonth == Period
                                              && row.AuditStatusID == AuditStatusId
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Key))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID ////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.KeyName == Key
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID ////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId ////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId ////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID ////&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (AuditStatusId != -1 && !string.IsNullOrEmpty(Period) && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && row.AuditStatusID == AuditStatusId && row.ForMonth == Period
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear) && !string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && ReporteeId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ReporteeId != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == ReporteeId //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1 && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerBranchID == BranchID
                                              && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && SubprocessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (SubprocessId != -1 && BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.SubProcessId == SubprocessId && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period) && !string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.CustomerID == customerID
                                              && row.FinancialYear == FinacialYear && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (BranchID != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                             && row.CustomerBranchID == BranchID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                              && row.ProcessId == ProcessId && row.CustomerID == customerID
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(FinacialYear))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.CustomerID == customerID
                                               && row.FinancialYear == FinacialYear
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (!string.IsNullOrEmpty(Period))
                    {
                        transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.UserID == userID //&& reviewerRoleIDs.Contains((int)row.RoleID)
                                               && row.CustomerID == customerID
                                               && row.ForMonth == Period
                                              && PerformerstatusIds.Contains((int)row.AuditStatusID)
                                              && FrequencyIds.Contains((int)row.Frequency)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    #endregion
                }

                var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();

                List<AuditInstanceTransactionView> newList = new List<AuditInstanceTransactionView>();
                foreach (var loc in locations)
                {
                    AuditInstanceTransactionView BranchRow = new AuditInstanceTransactionView();

                    BranchRow.ActivityDescription = loc.Branch;
                    BranchRow.ControlObjective = "";
                    BranchRow.RiskCreationId = -1;
                    BranchRow.UserID = -1;
                    BranchRow.RoleID = -1;
                    BranchRow.AuditStatusID = -1;
                    newList.Add(BranchRow);

                    List<AuditInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).ToList();
                    foreach (var item1 in temptransaction)
                    {
                        newList.Add(item1);
                    }
                }

                return newList.ToList();
            }
            //using (AuditControlEntities entities = new AuditControlEntities())
            //{
            //    List<AuditInstanceTransactionView> transactionsQuery = new List<AuditInstanceTransactionView>();
            //    var reviewerRoleIDs = (from row in entities.mst_Role
            //                           where row.Code.StartsWith("RVW")
            //                           select row.ID).ToList();

            //    List<int> reviewerstatusIds = new List<int>();
            //    reviewerstatusIds.Clear();
            //    reviewerstatusIds.Add(2);
            //    reviewerstatusIds.Add(4);
            //    DateTime now = DateTime.UtcNow.Date;
            //    DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
            //    switch (filter)
            //    {
            //        case CannedReportFilterForReviewerAudit.All:
            //transactionsQuery = (from row in entities.AuditInstanceTransactionViews
            //                     where // row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&

            //                      row.CustomerBranchID == BranchID
            //                      && reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
            //                     select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
            //            break;
            //        case CannedReportFilterForReviewerAudit.Location:
            //            transactionsQuery = (from row in entities.AuditInstanceTransactionViews
            //                                 where //row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&    
            //                                   row.CustomerBranchID == BranchID
            //                                   && reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
            //                                 select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
            //            break;
            //        case CannedReportFilterForReviewerAudit.Reportee:
            //transactionsQuery = (from row in entities.AuditInstanceTransactionViews
            //                     where // row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
            //                       reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
            //                       && row.CustomerBranchID == BranchID && row.UserID == Reportee
            //                     select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
            //            break;
            //        case CannedReportFilterForReviewerAudit.Process:
            //            transactionsQuery = (from row in entities.AuditInstanceTransactionViews
            //                                 where //row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
            //                                  reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
            //                                 && row.CustomerBranchID == BranchID && row.UserID == Reportee && row.ProcessId == ProcessId
            //                                 select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
            //            break;
            //        case CannedReportFilterForReviewerAudit.SubProcess:
            //            transactionsQuery = (from row in entities.AuditInstanceTransactionViews
            //                                 where // row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
            //                                   reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
            //                                 && row.CustomerBranchID == BranchID && row.UserID == Reportee && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId
            //                                 select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
            //            break;
            //        case CannedReportFilterForReviewerAudit.KeyNonKey:
            //            transactionsQuery = (from row in entities.AuditInstanceTransactionViews
            //                                 where // row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
            //                                   reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
            //                                 && row.CustomerBranchID == BranchID && row.UserID == Reportee && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.KeyName == Key
            //                                 select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
            //            break;
            //        case CannedReportFilterForReviewerAudit.FinacialYear:
            //            transactionsQuery = (from row in entities.AuditInstanceTransactionViews
            //                                 where //row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
            //                                  reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
            //                                  && row.CustomerBranchID == BranchID && row.UserID == Reportee && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.KeyName == Key && row.FinancialYear == FinacialYear
            //                                 select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

            //            break;
            //        case CannedReportFilterForReviewerAudit.Status:
            //            transactionsQuery = (from row in entities.AuditInstanceTransactionViews
            //                                 where //row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&      
            //                                   row.AuditStatusID == AuditStatusId
            //                                  && row.CustomerBranchID == BranchID && row.UserID == Reportee && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.KeyName == Key && row.FinancialYear == FinacialYear
            //                                 select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
            //            break;
            //    }


            //    var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();

            //    List<AuditInstanceTransactionView> newList = new List<AuditInstanceTransactionView>();
            //    foreach (var loc in locations)
            //    {
            //        AuditInstanceTransactionView BranchRow = new AuditInstanceTransactionView();

            //        BranchRow.ActivityDescription = loc.Branch;
            //        BranchRow.ControlObjective = loc.Branch;
            //        BranchRow.RiskCreationId = -1;
            //        BranchRow.UserID = -1;
            //        BranchRow.RoleID = -1;
            //        BranchRow.AuditStatusID = -1;
            //        newList.Add(BranchRow);

            //        List<AuditInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).ToList();
            //        foreach (var item1 in temptransaction)
            //        {
            //            newList.Add(item1);
            //        }
            //    }
            //    return newList.ToList();
            //}
        }
        //public static List<AuditInstanceTransactionView> DashboardDataForReviewer(int userID, CannedReportFilterForReviewerAudit filter, long BranchID, long ProcessId, long SubprocessId, string FinacialYear, string Key, long Reportee, long AuditStatusId)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        List<AuditInstanceTransactionView> transactionsQuery = new List<AuditInstanceTransactionView>();
        //        var reviewerRoleIDs = (from row in entities.mst_Role
        //                               where row.Code.StartsWith("RVW")
        //                               select row.ID).ToList();

        //        List<int> reviewerstatusIds = new List<int>();
        //        reviewerstatusIds.Clear();
        //        reviewerstatusIds.Add(2);
        //        reviewerstatusIds.Add(4);              
        //        DateTime now = DateTime.UtcNow.Date;
        //        DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);             
        //        switch (filter)
        //        {
        //            case CannedReportFilterForReviewerAudit.All:
        //                transactionsQuery = (from row in entities.AuditInstanceTransactionViews
        //                                     where // row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&

        //                                      row.CustomerBranchID == BranchID
        //                                      && reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
        //                                     select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForReviewerAudit.Location:
        //                transactionsQuery = (from row in entities.AuditInstanceTransactionViews
        //                                     where //row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&    
        //                                       row.CustomerBranchID == BranchID
        //                                       && reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
        //                                     select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForReviewerAudit.Reportee:
        //                transactionsQuery = (from row in entities.AuditInstanceTransactionViews
        //                                     where // row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
        //                                       reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
        //                                       && row.CustomerBranchID == BranchID && row.UserID == Reportee
        //                                     select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForReviewerAudit.Process:
        //                transactionsQuery = (from row in entities.AuditInstanceTransactionViews
        //                                     where //row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
        //                                      reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
        //                                     && row.CustomerBranchID == BranchID && row.UserID == Reportee && row.ProcessId == ProcessId
        //                                     select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForReviewerAudit.SubProcess:
        //                transactionsQuery = (from row in entities.AuditInstanceTransactionViews
        //                                     where // row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
        //                                       reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
        //                                     && row.CustomerBranchID == BranchID && row.UserID == Reportee && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId
        //                                     select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForReviewerAudit.KeyNonKey:
        //                transactionsQuery = (from row in entities.AuditInstanceTransactionViews
        //                                     where // row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
        //                                       reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
        //                                     && row.CustomerBranchID == BranchID && row.UserID == Reportee && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.KeyName == Key
        //                                     select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //            case CannedReportFilterForReviewerAudit.FinacialYear:
        //                transactionsQuery = (from row in entities.AuditInstanceTransactionViews
        //                                     where //row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&
        //                                      reviewerstatusIds.Contains((int)row.AuditStatusID) //&& row.AuditStatusID == 2
        //                                      && row.CustomerBranchID == BranchID && row.UserID == Reportee && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.KeyName == Key && row.FinancialYear == FinacialYear
        //                                     select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //                break;
        //            case CannedReportFilterForReviewerAudit.Status:
        //                transactionsQuery = (from row in entities.AuditInstanceTransactionViews
        //                                     where //row.UserID == userID && reviewerRoleIDs.Contains((int)row.RoleID) &&      
        //                                       row.AuditStatusID == AuditStatusId
        //                                      && row.CustomerBranchID == BranchID && row.UserID == Reportee && row.ProcessId == ProcessId && row.SubProcessId == SubprocessId && row.KeyName == Key && row.FinancialYear == FinacialYear
        //                                     select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
        //                break;
        //        }

        //        //if (filter == CannedReportFilterForPerformer.PendingForApproval)
        //        //{
        //        //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5);
        //        //}
        //        //else if (pending)
        //        //{
        //        //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn <= now);
        //        //}
        //        //else
        //        //{
        //        //    transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn <= nextOneMonth && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11));
        //        //}



        //        var locations = transactionsQuery.GroupBy(entry => entry.CustomerBranchID).ToList().Select(entry => entry.FirstOrDefault()).ToList();

        //        List<AuditInstanceTransactionView> newList = new List<AuditInstanceTransactionView>();
        //        foreach (var loc in locations)
        //        {
        //            AuditInstanceTransactionView BranchRow = new AuditInstanceTransactionView();

        //            BranchRow.ActivityDescription = loc.Branch;
        //            BranchRow.ControlObjective = loc.Branch;
        //            BranchRow.RiskCreationId = -1;
        //            BranchRow.UserID = -1;
        //            BranchRow.RoleID = -1;
        //            BranchRow.AuditStatusID = -1;
        //            newList.Add(BranchRow);

        //            List<AuditInstanceTransactionView> temptransaction = transactionsQuery.Where(entry => entry.CustomerBranchID == loc.CustomerBranchID).ToList();
        //            foreach (var item1 in temptransaction)
        //            {
        //                newList.Add(item1);
        //            }
        //        }
        //        return newList.ToList();
        //    }
        //}

        public static string GetUserName(long RiskCreationID, int? Branchid, int roleID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.AuditAssignments
                                         join cm in entities.mst_User on row.UserID equals cm.ID
                                         where
                                          row.RiskCreationId == RiskCreationID
                                         && row.RoleID == roleID
                                         && row.CustomerBranchID == Branchid && row.IsActive == true
                                         select cm.FirstName + " " + cm.LastName).FirstOrDefault();

                return transactionsQuery;
            }
        }
        public static string GetUserName(int compliancectatusid, long? scheduledonid, long RiskCreationId, int roleID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         join cm in entities.mst_User on row.UserID equals cm.ID
                                         where
                                          row.RiskCreationId == RiskCreationId
                                         && row.RoleID == roleID
                                         && row.AuditStatusID == compliancectatusid
                                         select cm.FirstName + " " + cm.LastName).FirstOrDefault();

                return transactionsQuery;
            }
        }

        //29 Jan
        public static List<IMPOpenCloseAuditDetailsClass> IMPGetAudits(String FinYear, String Period, int Customerid, List<long> BranchList, int userid, int statusid, int VerticalID, int roleID = 0)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<IMPOpenCloseAuditDetailsClass> record = new List<IMPOpenCloseAuditDetailsClass>();

                if (statusid == 1)
                {
                    record = (from row in entities.ImplementationAuditSummaryCountViews
                              where row.CustomerID == Customerid && row.UserID == userid
                              && (row.AuditStatusID != 3 || (row.ImplementationAuditStatusID == 3 || row.ImplementationAuditStatusID == null))
                              select new IMPOpenCloseAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  RoleID = row.RoleID,
                                  UserID = row.UserID,
                                  Role = row.Role,
                                  User = row.User,
                                  FinancialYear = row.FinancialYear,
                                  VerticalId = (long)row.VerticalID,
                                  VerticalName = row.VerticalName,
                                  AuditID = row.AuditID,
                                  ProcessId = row.ProcessID,
                                  SubProcessId = row.SubProcessId,
                                  ATBDID = row.ATBDId,
                                  ProcessName = row.ProcessName,
                                  SubProcessName = row.SubProcess,
                              }).Distinct().ToList();
                }
                else if (statusid == 3)
                {

                    int count = 0, flag = 0;
                    List<int> aa = new List<int>();

                    var closedCount = (from row in entities.ImplimentInternalAuditClosedCountViews
                                       where row.CustomerID == Customerid && row.UserID == userid && row.CustomerID == Customerid
                                       && row.AllClosed == flag
                                       select row.CustomerBranchID).ToList();
                    count = closedCount.Count;
                    if (count > 0)
                    {
                        foreach (var item in closedCount)
                        {
                            aa.Add(item);
                        }
                    }

                    record = (from row in entities.ImplementationAuditSummaryCountViews
                              where row.CustomerID == Customerid && row.UserID == userid
                              && row.AuditStatusID == 3
                              && row.ImplementationAuditStatusID != 3
                              && aa.Contains(row.CustomerBranchID)
                              select new IMPOpenCloseAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  RoleID = row.RoleID,
                                  UserID = row.UserID,
                                  Role = row.Role,
                                  User = row.User,
                                  FinancialYear = row.FinancialYear,
                                  VerticalId = (long)row.VerticalID,
                                  VerticalName = row.VerticalName,
                                  AuditID = row.AuditID,
                                  ProcessId = row.ProcessID,
                                  SubProcessId = row.SubProcessId,
                                  ATBDID = row.ATBDId,
                                  ProcessName = row.ProcessName,
                                  SubProcessName = row.SubProcess,
                              }).Distinct().ToList();
                }

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains((int)Entry.CustomerBranchID)).ToList();

                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();

                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                if (roleID != -1)
                    record = record.Where(Entry => Entry.RoleID == roleID).ToList();

                return record;
            }
        }

        public static List<IMPOpenCloseAuditDetailsClass> IMPGetAudits(String FinYear, String Period, int Customerid, int CustBranchid, int userid, int statusid, int VerticalID, int roleID = 0)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<IMPOpenCloseAuditDetailsClass> record = new List<IMPOpenCloseAuditDetailsClass>();

                if (statusid == 1)
                {
                    record = (from row in entities.ImplementationAuditSummaryCountViews
                              where row.CustomerID == Customerid && row.UserID == userid
                                && (row.AuditStatusID != 3 || (row.ImplementationAuditStatusID == 3 || row.ImplementationAuditStatusID == null))
                              select new IMPOpenCloseAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  RoleID = row.RoleID,
                                  UserID = row.UserID,
                                  Role = row.Role,
                                  User = row.User,
                                  FinancialYear = row.FinancialYear,
                                  VerticalId = (long)row.VerticalID,
                                  VerticalName = row.VerticalName,
                                  UserName = row.UserName,
                                  CProcessName = row.CProcessName,
                                  AuditID = row.AuditID,
                              }).Distinct().ToList();

                    //record = (from row in entities.ImplementationAuditSummaryCountViews
                    //          where row.CustomerID == Customerid && row.UserID == userid
                    //          //  && (row.AuditStatusID == null)
                    //            && (row.ImplementationAuditStatusID == 3 || row.ImplementationAuditStatusID == null)                              
                    //          // && (row.AStatusId != 3  || row.ImplementationAuditStatusID != 3)
                    //          select new IMPOpenCloseAuditDetailsClass()
                    //          {
                    //              CustomerID = row.CustomerID,
                    //              CustomerBranchID = row.CustomerBranchID,
                    //              Branch = row.Branch,
                    //              ForMonth = row.ForMonth,
                    //              RoleID = row.RoleID,
                    //              UserID = row.UserID,
                    //              Role = row.Role,
                    //              User = row.User,
                    //              FinancialYear = row.FinancialYear,
                    //              VerticalId = (long) row.VerticalID,
                    //              VerticalName = row.VerticalName,
                    //          }).Distinct().ToList();

                }
                else if (statusid == 3)
                {

                    int count = 0, flag = 0;
                    List<int> aa = new List<int>();

                    var closedCount = (from row in entities.ImplimentInternalAuditClosedCountViews
                                       where row.CustomerID == Customerid && row.UserID == userid && row.CustomerID == Customerid
                                       && row.AllClosed == flag
                                       select row.CustomerBranchID).ToList();
                    count = closedCount.Count;
                    if (count > 0)
                    {
                        foreach (var item in closedCount)
                        {
                            aa.Add(item);
                        }
                    }

                    record = (from row in entities.ImplementationAuditSummaryCountViews
                              where row.CustomerID == Customerid && row.UserID == userid
                              && row.AuditStatusID == 3
                              && row.ImplementationAuditStatusID != 3
                              && aa.Contains(row.CustomerBranchID)
                              select new IMPOpenCloseAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  RoleID = row.RoleID,
                                  UserID = row.UserID,
                                  Role = row.Role,
                                  User = row.User,
                                  FinancialYear = row.FinancialYear,
                                  VerticalId = (long)row.VerticalID,
                                  VerticalName = row.VerticalName,
                                  UserName = row.UserName,
                                  CProcessName = row.CProcessName,
                                  AuditID = row.AuditID,
                              }).Distinct().ToList();
                }

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();


                if (CustBranchid != -1)
                    record = record.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();
                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();
                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                if (roleID != -1)
                    record = record.Where(Entry => Entry.RoleID == roleID).ToList();

                return record;
            }
        }

        //30 Jan
        public static List<ImplementationReviewHistory> GetImplementationReviewAllRemarks(string ForPeriod, string FinancialYear, int ScheduledOnID, int ResultID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.ImplementationReviewHistories
                                  where row.ForMonth == ForPeriod && row.FinancialYear == FinancialYear
                                  && row.ImplementationScheduleOnID == ScheduledOnID
                                  && row.ResultID == ResultID && row.AuditID == AuditID
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<GetImplementationAuditDocumentsView> GetFileDataGetImplementationAuditDocumentsView(int ScheduledOnID, int ResultId, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.GetImplementationAuditDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID && row.ResultId == ResultId
                                && row.AuditID == AuditID
                                select row).ToList();
                return fileData;
            }
        }

        public static List<OpenCloseAuditDetailsClass> GetPersonResponsibleAuditsIMP(int Customerid, List<int?> BranchList, String FinYear, String Period, int userid, int statusid, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();

                if (statusid == 1)
                {
                    List<int> chkOpenBranchcount = new List<int>();
                    int flag = 0;

                    chkOpenBranchcount = (from row in entities.ImplimentInternalPersonResponsibleAuditClosedCountViews
                                          where row.CustomerID == Customerid && row.PersonResponsible == userid
                                          && row.AllClosed != flag
                                          select row.CustomerBranchID).ToList();


                    record = (from row in entities.PersonResponsibleImplementationAuditStatusDisplayViews
                              where row.PersonResponsible == userid
                              && row.CustomerID == Customerid
                              && (row.AuditStatusID != 3 || (row.ImplementationAuditStatusID == 3 || row.ImplementationAuditStatusID == null))
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  FinancialYear = row.FinancialYear,
                                  VerticalId = (long)row.VerticalID,
                                  VerticalName = row.VerticalName,
                                  UserName = row.UserName,
                                  CProcessName = row.CProcessName,
                                  AuditID = row.AuditID,
                              }).Distinct().ToList();

                    //record = (from row in entities.PersonResponsibleImplementationAuditStatusDisplayViews
                    //          where row.PersonResponsible == userid
                    //          && row.CustomerID == Customerid && chkOpenBranchcount.Contains(row.CustomerBranchID)
                    //           && row.AuditStatusID != 3
                    //           && (row.ImplementationAuditStatusID == 3 || row.ImplementationAuditStatusID == null)
                    //          select new OpenCloseAuditDetailsClass()
                    //          {
                    //              CustomerID = row.CustomerID,
                    //              CustomerBranchID = row.CustomerBranchID,
                    //              Branch = row.Branch,
                    //              ForMonth = row.ForMonth,
                    //              FinancialYear = row.FinancialYear,
                    //              VerticalId = (long) row.VerticalID,
                    //              VerticalName = row.VerticalName,
                    //          }).Distinct().ToList();
                }
                else if (statusid == 3)
                {
                    List<int> chkCloseBranchcount = new List<int>();
                    int flag = 0;

                    chkCloseBranchcount = (from row in entities.ImplimentInternalPersonResponsibleAuditClosedCountViews
                                           where row.CustomerID == Customerid && row.PersonResponsible == userid
                                           && row.AllClosed == flag
                                           select row.CustomerBranchID).ToList();


                    record = (from row in entities.PersonResponsibleImplementationAuditStatusDisplayViews
                              where row.PersonResponsible == userid
                              && row.CustomerID == Customerid
                               && row.AuditStatusID == 3
                               && row.ImplementationAuditStatusID != 3
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = row.CustomerID,
                                  CustomerBranchID = row.CustomerBranchID,
                                  Branch = row.Branch,
                                  ForMonth = row.ForMonth,
                                  FinancialYear = row.FinancialYear,
                                  VerticalId = (long)row.VerticalID,
                                  VerticalName = row.VerticalName,
                                  UserName = row.UserName,
                                  CProcessName = row.CProcessName,
                                  AuditID = row.AuditID,
                              }).Distinct().ToList();
                }

                if (Customerid != -1)
                    record = record.Where(Entry => Entry.CustomerID == Customerid).ToList();

                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (CustBranchid != -1)
                //    record = record.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();

                if (FinYear != "")
                    record = record.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                if (Period != "")
                    record = record.Where(Entry => Entry.ForMonth == Period).ToList();

                return record;
            }
        }

        //19-02-2020
        public static List<Tran_AnnxetureUpload> GetFileDataGetTran_ClientDataUploadDocument(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.Tran_AnnxetureUpload
                                where row.AuditID == AuditID
                                 && row.IsDeleted == false && row.FileType == "ClientData"
                                select row).ToList();
                return fileData;
            }
        }

        #region added by sagar on 21-09-2020
        public static List<OpenCloseAuditDetailsClass> GetAuditManagerAuditsForDashbaord(List<String> FinYearList, List<String> PeriodList, List<int?> CustomerIdList, List<long> BranchList, int userid, int statusid, int VerticalID, int RoleID, string departmentheadFlag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();
                String isAMAH = CustomerManagementRisk.GetAuditHeadOrManagerid(userid);
                if (CustomerManagementRisk.CheckIsManagement(userid) == 8)
                {
                    #region Management   
                    if (statusid == 1)
                    {
                        var Masterrecord = (from C in entities.AuditCountView_Dashboard
                                            join EAAMR in entities.EntitiesAssignmentManagementRisks
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            where C.ProcessId == EAAMR.ProcessId
                                            && CustomerIdList.Contains(C.CustomerID)
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where CustomerIdList.Contains(C.CustomerID)
                                  && (C.ACCStatus == null)
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    else if (statusid == 3)
                    {
                        var Masterrecord = (from C in entities.AuditCountViews
                                            join EAAMR in entities.EntitiesAssignmentManagementRisks
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            where C.ProcessId == EAAMR.ProcessId
                                            && CustomerIdList.Contains(C.CustomerID)
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where CustomerIdList.Contains(C.CustomerID)
                                   && C.ACCStatus == 1
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    #endregion
                }
                else if (!String.IsNullOrEmpty(isAMAH) && (isAMAH == "AM" || isAMAH == "AH"))
                {
                    #region  Audit Manager             
                    if (statusid == 1)
                    {
                        var Masterrecord = (from C in entities.AuditCountView_Dashboard
                                            join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            where C.ProcessId == EAAMR.ProcessId
                                            && CustomerIdList.Contains(C.CustomerID)
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where CustomerIdList.Contains(C.CustomerID)
                                  && (C.ACCStatus == null)
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    else if (statusid == 3)
                    {
                        var Masterrecord = (from C in entities.AuditCountViews
                                            join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            where C.ProcessId == EAAMR.ProcessId
                                            && CustomerIdList.Contains(C.CustomerID)
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where CustomerIdList.Contains(C.CustomerID)
                                   && C.ACCStatus == 1
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    #endregion
                }
                else if (departmentheadFlag == "DH")
                {
                    #region Department Head

                    if (statusid == 1)
                    {
                        var Masterrecord = (from C in entities.AuditCountView_Dashboard
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.AuditID equals ICAA.AuditID
                                            join EAAMR in entities.EntitiesAssignmentDepartmentHeads
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            join MSP in entities.Mst_Process
                                            on ICAA.ProcessId equals MSP.Id
                                            where EAAMR.DepartmentID == MSP.DepartmentID
                                            && CustomerIdList.Contains(C.CustomerID)
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where CustomerIdList.Contains(C.CustomerID)
                                  && (C.ACCStatus == null)
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    else if (statusid == 3)
                    {
                        var Masterrecord = (from C in entities.AuditCountViews
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.AuditID equals ICAA.AuditID
                                            join EAAMR in entities.EntitiesAssignmentDepartmentHeads
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            join MSP in entities.Mst_Process
                                            on ICAA.ProcessId equals MSP.Id
                                            where EAAMR.DepartmentID == MSP.DepartmentID
                                            && CustomerIdList.Contains(C.CustomerID)
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == userid
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where CustomerIdList.Contains(C.CustomerID)
                                   && C.ACCStatus == 1
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    #endregion
                }
                else
                {
                    #region Performer and Reviewer
                    if (statusid == 1)
                    {
                        var Masterrecord = (from C in entities.AuditCountViews
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.CustomerBranchID equals (int)ICAA.CustomerBranchID
                                            where C.ProcessId == ICAA.ProcessId
                                            && C.AuditID == ICAA.AuditID
                                            && CustomerIdList.Contains(C.CustomerID)
                                            && ICAA.UserID == userid
                                            && ICAA.RoleID == RoleID
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where CustomerIdList.Contains(C.CustomerID)
                                  && (C.ACCStatus == null)
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    else if (statusid == 3)
                    {
                        var Masterrecord = (from C in entities.AuditCountViews
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.CustomerBranchID equals (int)ICAA.CustomerBranchID
                                            where C.ProcessId == ICAA.ProcessId
                                            && C.AuditID == ICAA.AuditID
                                            && CustomerIdList.Contains(C.CustomerID)
                                            && ICAA.UserID == userid
                                            && ICAA.RoleID == RoleID
                                            select C).ToList();

                        record = (from C in Masterrecord
                                  where CustomerIdList.Contains(C.CustomerID)
                                   && C.ACCStatus == 1
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName,
                                      C.UserName,
                                      C.CProcessName,
                                      C.SubProcess,
                                      AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                      UserName = GCS.Key.UserName,
                                      CProcessName = GCS.Key.CProcessName,
                                      AuditName = GCS.Key.AuditName,
                                      SubProcess = GCS.Key.SubProcess
                                  }).ToList();

                        Masterrecord.Clear();
                        Masterrecord = null;
                    }
                    #endregion
                }

                if (CustomerIdList.Count > 0)
                    record = record.Where(Entry => CustomerIdList.Contains(Entry.CustomerID)).ToList();
                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains((int)Entry.CustomerBranchID)).ToList();
                if (VerticalID != -1)
                    record = record.Where(Entry => Entry.VerticalId == VerticalID).ToList();
                if (FinYearList.Count > 0)
                    record = record.Where(Entry => FinYearList.Contains(Entry.FinancialYear)).ToList();
                if (PeriodList.Count > 0)
                    record = record.Where(Entry => PeriodList.Contains(Entry.ForMonth)).ToList();

                if (record.Count > 0)
                    record = record.OrderBy(Entry => Entry.Branch).ThenBy(entry => entry.VerticalName).ThenBy(entry => entry.FinancialYear).ToList();

                return record;
            }
        }

        public static List<OpenCloseAuditDetailsClass> GetAuditManagerAuditsIMPForDashBoard(List<String> FinYearList, List<String> PeriodList, List<int?> CustomerIdList, List<int?> CustBranchIdList, int userid, int statusid, List<int> VerticalListID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();
                string isAMAH = CustomerManagementRisk.GetAuditHeadOrManagerid(userid);
                if (!String.IsNullOrEmpty(isAMAH) && (isAMAH == "AM" || isAMAH == "AH"))
                {
                    List<long> branchids = AssignEntityManagementRisk.CheckAuditManagerLocation(userid);

                    List<int?> BranchAssigned = branchids.Select(x => (int?)x).ToList();

                    if (statusid == 1)
                    {
                        List<int> chkOpenBranchcount = new List<int>();
                        int flag = 0;
                        if (BranchAssigned.Count > 0)
                        {
                            chkOpenBranchcount = (from C in entities.ImplimentInternalAuditClosedCountViews
                                                  join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                                  on C.CustomerBranchID equals EAAMR.BranchID
                                                  where C.ProcessID == EAAMR.ProcessId
                                                  && CustomerIdList.Contains(C.CustomerID)
                                                  && EAAMR.UserID == userid
                                                  && EAAMR.ISACTIVE == true
                                                  && C.RoleID == 3 && C.AllClosed != flag
                                                  select C.CustomerBranchID).ToList();

                            record = (from row in entities.ImplementationAuditSummaryCountViews
                                      where CustomerIdList.Contains(row.CustomerID)
                                     && chkOpenBranchcount.Contains(row.CustomerBranchID)
                                     && (row.AuditStatusID != 3 || (row.ImplementationAuditStatusID == 3 || row.ImplementationAuditStatusID == null))
                                      select new OpenCloseAuditDetailsClass()
                                      {
                                          CustomerID = row.CustomerID,
                                          CustomerBranchID = row.CustomerBranchID,
                                          Branch = row.Branch,
                                          ForMonth = row.ForMonth,
                                          FinancialYear = row.FinancialYear,
                                          VerticalId = (long)row.VerticalID,
                                          VerticalName = row.VerticalName,
                                          UserName = row.UserName,
                                          CProcessName = row.CProcessName,
                                          AuditID = row.AuditID,
                                          AuditName = row.Branch + "/" + row.FinancialYear + "/" + row.ForMonth,
                                      }).Distinct().ToList();
                        }
                    }

                    else if (statusid == 3)
                    {
                        List<int> chkclosedBranchcount = new List<int>();
                        int flag = 0;
                        if (BranchAssigned.Count > 0)
                        {

                            chkclosedBranchcount = (from C in entities.ImplimentInternalAuditClosedCountViews
                                                    join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                                    on C.CustomerBranchID equals EAAMR.BranchID
                                                    where C.ProcessID == EAAMR.ProcessId
                                                    && CustomerIdList.Contains(C.CustomerID)
                                                    && EAAMR.UserID == userid
                                                    && EAAMR.ISACTIVE == true
                                                    && C.RoleID == 3 && C.AllClosed == flag
                                                    select C.CustomerBranchID).ToList();


                            record = (from row in entities.ImplementationAuditSummaryCountViews
                                      where CustomerIdList.Contains(row.CustomerID) && chkclosedBranchcount.Contains(row.CustomerBranchID)
                                        && row.AuditStatusID == 3
                                        && row.ImplementationAuditStatusID != 3
                                      select new OpenCloseAuditDetailsClass()
                                      {
                                          CustomerID = row.CustomerID,
                                          CustomerBranchID = row.CustomerBranchID,
                                          Branch = row.Branch,
                                          ForMonth = row.ForMonth,
                                          FinancialYear = row.FinancialYear,
                                          VerticalId = (long)row.VerticalID,
                                          VerticalName = row.VerticalName,
                                          UserName = row.UserName,
                                          CProcessName = row.CProcessName,
                                          AuditID = row.AuditID,
                                          AuditName = row.Branch + "/" + row.FinancialYear + "/" + row.ForMonth,
                                      }).Distinct().ToList();
                        }
                    }
                }

                if (CustomerIdList.Count > 0)
                    record = record.Where(Entry => CustomerIdList.Contains(Entry.CustomerID)).ToList();

                if (CustBranchIdList.Count > 0)
                    record = record.Where(Entry => CustBranchIdList.Contains(Entry.CustomerBranchID)).ToList();

                if (VerticalListID.Count >0)
                    record = record.Where(Entry => VerticalListID.Contains((int)Entry.VerticalId)).ToList();

                if (FinYearList.Count > 0)
                    record = record.Where(Entry => FinYearList.Contains(Entry.FinancialYear)).ToList();

                if (PeriodList.Count > 0)
                    record = record.Where(Entry => PeriodList.Contains(Entry.ForMonth)).ToList();

                return record;
            }
        }

        public static List<OpenCloseAuditDetailsClass> GetAuditsForMultiSelect(List<String> FinYearList, List<String> PeriodList, List<int?> CustomerIdsList, List<int?> CustBranchIdList, int userid, int statusid, List<long> VerticalIdList, int roleID = 0)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();
                if (statusid == 1)
                {

                    entities.Database.CommandTimeout = 300;

                    var Masterrecord = (from row in entities.AuditCountViews
                                        select row).ToList();

                    record = (from C in Masterrecord
                              where C.UserID == userid
                              && C.ACCStatus == null
                              group C by new
                              {
                                  C.CustomerID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.Role,
                                  C.User,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName,
                                  C.UserName,
                                  C.AuditID,
                                  C.SubProcess,
                                  AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  Role = GCS.Key.Role,
                                  User = GCS.Key.User,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                                  UserName  = GCS.Key.UserName,
                                  AuditID = GCS.Key.AuditID,
                                  SubProcess = GCS.Key.SubProcess,
                                  AuditName = GCS.Key.AuditName
                                  
                              }).ToList();
                }
                else if (statusid == 3)
                {
                    entities.Database.CommandTimeout = 300;
                    var Masterrecord = (from row in entities.AuditCountViews
                                        select row).ToList();

                    record = (from C in Masterrecord
                              where C.UserID == userid
                              && C.ACCStatus == 1
                              group C by new
                              {
                                  C.CustomerID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  C.RoleID,
                                  C.UserID,
                                  C.Role,
                                  C.User,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName,
                                  C.UserName,
                                  C.AuditID,
                                  C.SubProcess,
                                  AuditName = C.Branch + "/" + C.FinancialYear + "/" + C.ForMonth,
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  Role = GCS.Key.Role,
                                  User = GCS.Key.User,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                                  UserName = GCS.Key.UserName,
                                  AuditID = GCS.Key.AuditID,
                                  SubProcess = GCS.Key.SubProcess,
                                  AuditName = GCS.Key.AuditName
                              }).ToList();

                }
                if (CustomerIdsList.Count > 0)
                    record = record.Where(Entry => CustomerIdsList.Contains(Entry.CustomerID)).ToList();

                if (CustBranchIdList.Count > 0)
                    record = record.Where(Entry => CustBranchIdList.Contains(Entry.CustomerBranchID)).ToList();

                if (VerticalIdList.Count > 0)
                    record = record.Where(Entry => VerticalIdList.Contains(Entry.VerticalId)).ToList();

                if (FinYearList.Count > 0)
                    record = record.Where(Entry => FinYearList.Contains(Entry.FinancialYear)).ToList();

                if (PeriodList.Count > 0)
                    record = record.Where(Entry => PeriodList.Contains(Entry.ForMonth)).ToList();

                if (roleID != -1)
                    record = record.Where(Entry => Entry.RoleID == roleID).ToList();

                if (record.Count > 0)
                    record = record.OrderBy(Entry => Entry.Branch).ThenBy(entry => entry.VerticalName).ThenBy(entry => entry.FinancialYear).ToList();

                return record;
            }
        }
        #endregion
    }
}
