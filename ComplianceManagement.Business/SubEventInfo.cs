﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class SubEventInfo
    {
        public Nullable<long> EventID { get; set; }
        public string Name { get; set; }
        public Nullable<long> ParentID { get; set; }
        public long SubEventID { get; set; }   
    }
}
