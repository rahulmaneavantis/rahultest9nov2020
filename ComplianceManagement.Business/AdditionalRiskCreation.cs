﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class AdditionalRiskCreation
    {
        public static List<object> FillAuditStepList(int Customerid, List<long> BranchList, int ProcessId, int SubProcessId, List<int> verticalList, bool status)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Database.CommandTimeout = 300;

                //var ddlAuditStepList = new List<object>();

                //if (BranchList.Count > 0 && ProcessId != -1 && SubProcessId != -1 && verticalList.Count > 0)
                //{
                //    ddlAuditStepList = (from row in entities.AuditStepMasterViews
                //                        where BranchList.Contains((long)row.CustomerBranchID)
                //                        && verticalList.Contains((int)row.VerticalID)
                //                        && row.ProcessId == ProcessId
                //                        && row.SubProcessId == SubProcessId
                //                        select new
                //                        {
                //                            ID = row.ID,
                //                            AuditStep = row.AuditStep,
                //                        }).Distinct().ToList<object>();
                //}
                //else if (BranchList.Count > 0 && ProcessId != -1 && SubProcessId != -1)
                //{
                //    ddlAuditStepList = (from row in entities.AuditStepMasterViews
                //                        where BranchList.Contains((long)row.CustomerBranchID)
                //                        && row.ProcessId == ProcessId
                //                        && row.SubProcessId == SubProcessId
                //                        select new
                //                        {
                //                            ID = row.ID,
                //                            AuditStep = row.AuditStep,
                //                        }).Distinct().ToList<object>();
                //}
                //else if (BranchList.Count > 0 && ProcessId != -1)
                //{
                //    ddlAuditStepList = (from row in entities.AuditStepMasterViews
                //                        where BranchList.Contains((long)row.CustomerBranchID)
                //                        && row.ProcessId == ProcessId
                //                        select new
                //                        {
                //                            ID = row.ID,
                //                            AuditStep = row.AuditStep,
                //                        }).Distinct().ToList<object>();
                //}
                //else if (verticalList.Count > 0 && ProcessId != -1)
                //{
                //    ddlAuditStepList = (from row in entities.AuditStepMasterViews
                //                        where verticalList.Contains((int)row.VerticalID)
                //                        && row.ProcessId == ProcessId
                //                        select new
                //                        {
                //                            ID = row.ID,
                //                            AuditStep = row.AuditStep,
                //                        }).Distinct().ToList<object>();
                //}
                //else if (ProcessId != -1 && SubProcessId != -1)
                //{
                //    ddlAuditStepList = (from row in entities.AuditStepMasterViews
                //                        where row.ProcessId == ProcessId
                //                        && row.SubProcessId == SubProcessId
                //                        select new
                //                        {
                //                            ID = row.ID,
                //                            AuditStep = row.AuditStep,
                //                        }).Distinct().ToList<object>();
                //}
                //else if (BranchList.Count > 0)
                //{
                //    ddlAuditStepList = (from row in entities.AuditStepMasterViews
                //                        where BranchList.Contains((long)row.CustomerBranchID)
                //                        select new
                //                        {
                //                            ID = row.ID,
                //                            AuditStep = row.AuditStep,
                //                        }).Distinct().ToList<object>();
                //}
                //else if (verticalList.Count > 0)
                //{
                //    ddlAuditStepList = (from row in entities.AuditStepMasterViews
                //                        where verticalList.Contains((int)row.VerticalID)
                //                        select new
                //                        {
                //                            ID = row.ID,
                //                            AuditStep = row.AuditStep,
                //                        }).Distinct().ToList<object>();
                //}
                //else if (ProcessId != -1)
                //{
                //    ddlAuditStepList = (from row in entities.AuditStepMasterViews
                //                        where row.ProcessId == ProcessId
                //                        select new
                //                        {
                //                            ID = row.ID,
                //                            AuditStep = row.AuditStep,
                //                        }).Distinct().ToList<object>();
                //}
                //else
                //{
                //    ddlAuditStepList = (from row in entities.AuditStepMasterViews
                //                        select new
                //                        {
                //                            ID = row.ID,
                //                            AuditStep = row.AuditStep,
                //                        }).Distinct().ToList<object>();
                //}

                var AuditStepMasterList = (from row in entities.AuditStepMasterViews.AsNoTracking()
                                           where row.IsActive == status
                                           select row).ToList();

                if (BranchList.Count > 0)
                    AuditStepMasterList = AuditStepMasterList.Where(row => BranchList.Contains((long)row.CustomerBranchID)).ToList();

                if (ProcessId != -1)
                    AuditStepMasterList = AuditStepMasterList.Where(entry => entry.ProcessId == ProcessId).ToList();

                if (SubProcessId != -1)
                    AuditStepMasterList = AuditStepMasterList.Where(entry => entry.SubProcessId == SubProcessId).ToList();

                if (verticalList.Count > 0)
                    AuditStepMasterList = AuditStepMasterList.Where(entry => verticalList.Contains((int)entry.VerticalID)).ToList();

                var ddlAuditStepList = new List<object>();

                if (AuditStepMasterList.Count > 0)
                {
                    ddlAuditStepList = (from row in AuditStepMasterList
                                        select new
                                        {
                                            ID = row.ID,
                                            AuditStep = row.AuditStep,
                                        }).Distinct().ToList<object>();

                    AuditStepMasterList.Clear();
                }

                return ddlAuditStepList;
            }
        }
        public static List<DeActivatedAuditStepView> GetAuditStepExportForActivate(int Customerid, List<long> BranchList, int ProcessId, List<int> verticalList, List<long> auditStepMasterIDList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Database.CommandTimeout = 300;
                List<DeActivatedAuditStepView> riskcategorycreations = new List<DeActivatedAuditStepView>();


                #region Process and Sub Process

                if (BranchList.Count > 0 && ProcessId != -1)
                {
                    riskcategorycreations = (from row in entities.DeActivatedAuditStepViews.AsNoTracking()
                                             where row.CustomerID == Customerid
                                              && BranchList.Contains((long)row.BranchId)
                                             && row.ProcessId == ProcessId
                                             orderby row.RiskCreationId
                                             select row).ToList();
                }
                else if (BranchList.Count > 0)
                {
                    riskcategorycreations = (from row in entities.DeActivatedAuditStepViews.AsNoTracking()
                                             where row.CustomerID == Customerid
                                              && BranchList.Contains((long)row.BranchId)
                                             orderby row.RiskCreationId
                                             select row).ToList();
                }
                else if (ProcessId != -1)
                {
                    riskcategorycreations = (from row in entities.DeActivatedAuditStepViews.AsNoTracking()
                                             where row.CustomerID == Customerid
                                             && row.ProcessId == ProcessId
                                             orderby row.RiskCreationId
                                             select row).ToList();
                }
                else
                {
                    riskcategorycreations = (from row in entities.DeActivatedAuditStepViews.AsNoTracking()
                                             where row.CustomerID == Customerid
                                             orderby row.RiskCreationId
                                             select row).ToList();
                }
                #endregion


                //if (verticalid != -1)
                //    riskcategorycreations = riskcategorycreations.Where(entry => entry.VerticalId == verticalid).ToList();

                if (verticalList.Count > 0)
                    riskcategorycreations = riskcategorycreations.Where(entry => verticalList.Contains(entry.VerticalId)).ToList();

                if (auditStepMasterIDList.Count > 0)
                    riskcategorycreations = riskcategorycreations.Where(entry => auditStepMasterIDList.Contains((long)entry.AuditStepMasterID)).ToList();

                if (riskcategorycreations.Count > 0)
                {
                    riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ActivityTobeDone)
                                                               //.ThenBy(entry => entry.VerticalName)
                                                               .ThenBy(entry => entry.ProcessName)
                                                               .ThenBy(entry => entry.SubProcessName)
                                                               .ToList();

                }

                return riskcategorycreations;
            }
        }
        //public static List<DeActivatedAuditStepView> GetAuditStepExportForActivate(int Customerid, List<long> BranchList, int ProcessId, int verticalid, string Flag)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        entities.Database.CommandTimeout = 300;
        //        List<DeActivatedAuditStepView> riskcategorycreations = new List<DeActivatedAuditStepView>();

        //        if (Flag == "P")
        //        {
        //            #region Process and Sub Process

        //            if (BranchList.Count > 0 && ProcessId != -1)
        //            {
        //                riskcategorycreations = (from row in entities.DeActivatedAuditStepViews
        //                                         where row.CustomerID == Customerid
        //                                          && BranchList.Contains((long) row.BranchId)
        //                                         && row.ProcessId == ProcessId
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else if (BranchList.Count > 0)
        //            {
        //                riskcategorycreations = (from row in entities.DeActivatedAuditStepViews
        //                                         where row.CustomerID == Customerid
        //                                          && BranchList.Contains((long) row.BranchId)
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else if (ProcessId != -1)
        //            {
        //                riskcategorycreations = (from row in entities.DeActivatedAuditStepViews
        //                                         where row.CustomerID == Customerid
        //                                         && row.ProcessId == ProcessId
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else
        //            {
        //                riskcategorycreations = (from row in entities.DeActivatedAuditStepViews
        //                                         where row.CustomerID == Customerid
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            #endregion
        //        }
        //        else
        //        {
        //            #region Non Process and sub Process
        //            if (BranchList.Count > 0 && ProcessId != -1)
        //            {
        //                riskcategorycreations = (from row in entities.DeActivatedAuditStepViews
        //                                         where row.CustomerID == Customerid
        //                                          && BranchList.Contains((long) row.BranchId)
        //                                         && row.ProcessId == ProcessId
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else if (BranchList.Count > 0)
        //            {
        //                riskcategorycreations = (from row in entities.DeActivatedAuditStepViews
        //                                         where row.CustomerID == Customerid
        //                                          && BranchList.Contains((long) row.BranchId)
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else if (ProcessId != -1)
        //            {
        //                riskcategorycreations = (from row in entities.DeActivatedAuditStepViews
        //                                         where row.CustomerID == Customerid
        //                                         && row.ProcessId == ProcessId
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else
        //            {
        //                riskcategorycreations = (from row in entities.DeActivatedAuditStepViews
        //                                         where row.CustomerID == Customerid
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            #endregion
        //        }

        //        if (verticalid != -1)
        //            riskcategorycreations = riskcategorycreations.Where(entry => entry.VerticalId == verticalid).ToList();

        //        if (riskcategorycreations.Count > 0)
        //        {
        //            riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ActivityTobeDone)
        //                                                       //.ThenBy(entry => entry.VerticalName)
        //                                                       .ThenBy(entry => entry.ProcessName)
        //                                                       .ThenBy(entry => entry.SubProcessName)
        //                                                       .ToList();

        //        }

        //        return riskcategorycreations;
        //    }
        //}
        public static List<RCMExportCheckListView> GetAllRiskCategoryCreationExport(int Customerid, List<long> BranchList, int ProcessId, int verticalid, string Flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Database.CommandTimeout = 300;
                List<RCMExportCheckListView> riskcategorycreations = new List<RCMExportCheckListView>();

                if (Flag == "P")
                {
                    #region Process and Sub Process

                    if (BranchList.Count > 0 && ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.RCMExportCheckListViews
                                                 where row.Customerid == Customerid
                                                 && BranchList.Contains((long)row.BranchId)
                                                 && row.ProcessId == ProcessId
                                                 && row.IsProcessNonProcess == "P"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (BranchList.Count > 0)
                    {
                        riskcategorycreations = (from row in entities.RCMExportCheckListViews
                                                 where row.Customerid == Customerid
                                                 && BranchList.Contains((long)row.BranchId)
                                                 && row.IsProcessNonProcess == "P"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.RCMExportCheckListViews
                                                 where row.Customerid == Customerid
                                                  && row.ProcessId == ProcessId
                                                  && row.IsProcessNonProcess == "P"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else
                    {
                        riskcategorycreations = (from row in entities.RCMExportCheckListViews
                                                 where row.Customerid == Customerid
                                                 && row.IsProcessNonProcess == "P"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    #endregion
                }
                else
                {
                    #region Non Process and sub Process
                    if (BranchList.Count > 0 && ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.RCMExportCheckListViews
                                                 where row.Customerid == Customerid
                                                 && BranchList.Contains((long)row.BranchId)
                                                 && row.ProcessId == ProcessId
                                                 && row.IsProcessNonProcess == "N"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (BranchList.Count > 0)
                    {
                        riskcategorycreations = (from row in entities.RCMExportCheckListViews
                                                 where row.Customerid == Customerid
                                                 && BranchList.Contains((long)row.BranchId)
                                                 && row.IsProcessNonProcess == "N"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.RCMExportCheckListViews
                                                 where row.Customerid == Customerid
                                                  && row.ProcessId == ProcessId
                                                  && row.IsProcessNonProcess == "N"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else
                    {
                        riskcategorycreations = (from row in entities.RCMExportCheckListViews
                                                 where row.Customerid == Customerid
                                                 && row.IsProcessNonProcess == "N"
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    #endregion
                }

                if (verticalid != -1)
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.VerticalId == verticalid).ToList();

                if (riskcategorycreations.Count > 0)
                {
                    //riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.BranchName)                                                               
                    //                                           .ThenBy(entry => entry.ProcessName).ToList();

                    riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ProcessName)
                                                              .ThenBy(entry => entry.SubProcessName)
                                                              .ThenBy(entry => entry.BranchName).ToList();
                }

                return riskcategorycreations;
            }
        }
        public static List<sp_AuditStepExport_Result> GetAuditStepExportForUpdate(long Customerid, List<long> BranchList, int ProcessId, int subProcessID, int verticalid, string filter, string Flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Database.CommandTimeout = 300;
                List<sp_AuditStepExport_Result> riskcategorycreations = new List<sp_AuditStepExport_Result>();

                if (Flag == "P")
                {
                    #region Process and Sub Process

                    if (BranchList.Count > 0 && ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                  && BranchList.Contains((long)row.BranchId)
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (BranchList.Count > 0)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where BranchList.Contains((long)row.BranchId)
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    #endregion
                }
                else
                {
                    #region Non Process and sub Process
                    if (BranchList.Count > 0 && ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                  && BranchList.Contains((long)row.BranchId)
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (BranchList.Count > 0)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                  && BranchList.Contains((long)row.BranchId)
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    #endregion
                }

                if (verticalid != -1)
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.VerticalId == verticalid).ToList();

                if (subProcessID != -1)
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == subProcessID).ToList();

                if (filter != "")
                    riskcategorycreations = riskcategorycreations
                                             .Where(entry => entry.ControlNo.Contains(filter)
                                             || entry.BranchName.Contains(filter)
                                             || entry.VerticalName.Contains(filter)
                                             || entry.ActivityTobeDone.Contains(filter)).ToList();

                if (riskcategorycreations.Count > 0)
                {
                    riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ActivityTobeDone)
                                                               //.ThenBy(entry => entry.VerticalName)
                                                               .ThenBy(entry => entry.ProcessName)
                                                               .ThenBy(entry => entry.SubProcessName)
                                                               .ToList();

                }

                return riskcategorycreations;
            }
        }

        public static List<sp_AuditStepExport_New_Result> GetAuditStepExportForUpdateAdditional(long Customerid, List<long> BranchList, int ProcessId, int subProcessID, int verticalid, string filter, string Flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Database.CommandTimeout = 300;
                List<sp_AuditStepExport_New_Result> riskcategorycreations = new List<sp_AuditStepExport_New_Result>();

                if (Flag == "P")
                {
                    #region Process and Sub Process

                    if (BranchList.Count > 0 && ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport_New((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                  && BranchList.Contains((long)row.BranchId)
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (BranchList.Count > 0)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport_New((int)Customerid)
                                                 where BranchList.Contains((long)row.BranchId)
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport_New((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport_New((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    #endregion
                }
                else
                {
                    #region Non Process and sub Process
                    if (BranchList.Count > 0 && ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport_New((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                  && BranchList.Contains((long)row.BranchId)
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (BranchList.Count > 0)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport_New((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                  && BranchList.Contains((long)row.BranchId)
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else if (ProcessId != -1)
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport_New((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 && row.ProcessId == ProcessId
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    else
                    {
                        riskcategorycreations = (from row in entities.sp_AuditStepExport_New((int)Customerid)
                                                 where row.CustomerID == Customerid
                                                 orderby row.RiskCreationId
                                                 select row).ToList();
                    }
                    #endregion
                }

                if (verticalid != -1)
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.VerticalId == verticalid).ToList();

                if (subProcessID != -1)
                    riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == subProcessID).ToList();

                if (filter != "")
                    riskcategorycreations = riskcategorycreations
                                             .Where(entry => entry.ControlNo.Contains(filter)
                                             || entry.BranchName.Contains(filter)
                                             || entry.VerticalName.Contains(filter)
                                             || entry.ActivityTobeDone.Contains(filter)).ToList();

                if (riskcategorycreations.Count > 0)
                {
                    riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ActivityTobeDone)
                                                               //.ThenBy(entry => entry.VerticalName)
                                                               .ThenBy(entry => entry.ProcessName)
                                                               .ThenBy(entry => entry.SubProcessName)
                                                               .ToList();

                }

                return riskcategorycreations;
            }
        }
        //public static List<AuditStepExportFillView> GetAuditStepExportForUpdate(long Customerid, List<long> BranchList, int ProcessId, int subProcessID, int verticalid, string filter, string Flag)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        entities.Database.CommandTimeout = 300;
        //        List<AuditStepExportFillView> riskcategorycreations = new List<AuditStepExportFillView>();

        //        if (Flag == "P")
        //        {
        //            #region Process and Sub Process

        //            if (BranchList.Count > 0 && ProcessId != -1)
        //            {
        //                riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
        //                                         where row.CustomerID == Customerid
        //                                          && BranchList.Contains((long) row.BranchId)
        //                                         && row.ProcessId == ProcessId
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else if (BranchList.Count > 0)
        //            {
        //                riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
        //                                         where row.CustomerID == Customerid
        //                                          && BranchList.Contains((long) row.BranchId)
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else if (ProcessId != -1)
        //            {
        //                riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
        //                                         where row.CustomerID == Customerid
        //                                         && row.ProcessId == ProcessId
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else
        //            {
        //                riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
        //                                         where row.CustomerID == Customerid
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            #endregion
        //        }
        //        else
        //        {
        //            #region Non Process and sub Process
        //            if (BranchList.Count > 0 && ProcessId != -1)
        //            {
        //                riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
        //                                         where row.CustomerID == Customerid
        //                                          && BranchList.Contains((long) row.BranchId)
        //                                         && row.ProcessId == ProcessId
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else if (BranchList.Count > 0)
        //            {
        //                riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
        //                                         where row.CustomerID == Customerid
        //                                          && BranchList.Contains((long) row.BranchId)
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else if (ProcessId != -1)
        //            {
        //                riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
        //                                         where row.CustomerID == Customerid
        //                                         && row.ProcessId == ProcessId
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            else
        //            {
        //                riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
        //                                         where row.CustomerID == Customerid
        //                                         orderby row.RiskCreationId
        //                                         select row).ToList();
        //            }
        //            #endregion
        //        }

        //        if (verticalid != -1)
        //            riskcategorycreations = riskcategorycreations.Where(entry => entry.VerticalId == verticalid).ToList();

        //        if (subProcessID != -1)
        //            riskcategorycreations = riskcategorycreations.Where(entry => entry.SubProcessId == subProcessID).ToList();

        //        if (filter != "")
        //            riskcategorycreations = riskcategorycreations
        //                                     .Where(entry => entry.ControlNo.Contains(filter)
        //                                     || entry.BranchName.Contains(filter)
        //                                     || entry.VerticalName.Contains(filter)
        //                                     || entry.ActivityTobeDone.Contains(filter)).ToList();

        //        if (riskcategorycreations.Count > 0)
        //        {
        //            riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ActivityTobeDone)
        //                                                       //.ThenBy(entry => entry.VerticalName)
        //                                                       .ThenBy(entry => entry.ProcessName)
        //                                                       .ThenBy(entry => entry.SubProcessName)
        //                                                       .ToList();

        //        }

        //        return riskcategorycreations;
        //    }
        //}


        public static List<AuditStepExportFillView> GetAuditStepExportForDeActivate(int Customerid, List<long> BranchList, int ProcessId, List<int> verticalList, List<long> auditStepMasterIDList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Database.CommandTimeout = 300;
                List<AuditStepExportFillView> riskcategorycreations = new List<AuditStepExportFillView>();

                #region Process and Sub Process

                if (BranchList.Count > 0 && ProcessId != -1)
                {
                    riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
                                             where row.CustomerID == Customerid
                                              && BranchList.Contains((long)row.BranchId)
                                             && row.ProcessId == ProcessId
                                             orderby row.RiskCreationId
                                             select row).ToList();
                }
                else if (BranchList.Count > 0)
                {
                    riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
                                             where row.CustomerID == Customerid
                                             && BranchList.Contains((long)row.BranchId)
                                             orderby row.RiskCreationId
                                             select row).ToList();
                }
                else if (ProcessId != -1)
                {
                    riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
                                             where row.CustomerID == Customerid
                                             && row.ProcessId == ProcessId
                                             orderby row.RiskCreationId
                                             select row).ToList();
                }
                else
                {
                    riskcategorycreations = (from row in entities.AuditStepExportFillViews.AsNoTracking()
                                             where row.CustomerID == Customerid
                                             orderby row.RiskCreationId
                                             select row).ToList();
                }
                #endregion


                if (verticalList.Count > 0)
                    riskcategorycreations = riskcategorycreations.Where(entry => verticalList.Contains(entry.VerticalId)).ToList();

                if (auditStepMasterIDList.Count > 0)
                    riskcategorycreations = riskcategorycreations.Where(entry => auditStepMasterIDList.Contains((long)entry.AuditStepMasterID)).ToList();

                //if (verticalid != -1)
                //    riskcategorycreations = riskcategorycreations.Where(entry => entry.VerticalId == verticalid).ToList();

                if (riskcategorycreations.Count > 0)
                {
                    riskcategorycreations = riskcategorycreations.OrderBy(entry => entry.ActivityTobeDone)
                                                               //.ThenBy(entry => entry.VerticalName)
                                                               .ThenBy(entry => entry.ProcessName)
                                                               .ThenBy(entry => entry.SubProcessName)
                                                               .ToList();

                }

                return riskcategorycreations;
            }
        }

        public static bool ActiveDeactiveAuditSteps(List<int> AuditStepIDList, bool flag)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (AuditStepIDList.Count > 0)
                    {
                        //Update Assignment Details
                        AuditStepIDList.ForEach(EachAuditStep =>
                        {
                            var assignmentToUpdate = (from RATBDM in entities.RiskActivityToBeDoneMappings
                                                      where RATBDM.ID == EachAuditStep
                                                      select RATBDM).FirstOrDefault();
                            if (assignmentToUpdate != null)
                                assignmentToUpdate.IsActive = flag;
                        });

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
