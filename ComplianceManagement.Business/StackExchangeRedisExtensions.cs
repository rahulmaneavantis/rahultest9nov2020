﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{      
    public static class StackExchangeRedisExtensions
    {
        private static readonly Encoding encoding = Encoding.UTF8;
        private static readonly JsonSerializerSettings settings;
        private static readonly IDatabase _cache;
        private static ConnectionMultiplexer _connectionMultiplexer;
        static StackExchangeRedisExtensions()
        {
            var connection = ConfigurationManager.AppSettings["RedisConnectionsring"];
            if (!string.IsNullOrEmpty(connection))
            {
                _connectionMultiplexer = ConnectionMultiplexer.Connect(connection);
                _cache = _connectionMultiplexer.GetDatabase();
            }
          
        }
        public static bool KeyExists(string key)
        {
            //var connect = ConnectionMultiplexer.Connect("avacomnewredis.redis.cache.windows.net:6380,password=QxzWs46k5vaIFcEewcCfiFu+HVAf7S7BiVIDrND7GFc=,ssl=True,abortConnect=False,connectTimeout=3000,synctimeout=3000,connectretry=2");
            //IDatabase cache = connect.GetDatabase();

            return _cache.KeyExists(key);
        }
        public static void Remove(string key)
        {
            //var connect = ConnectionMultiplexer.Connect("avacomnewredis.redis.cache.windows.net:6380,password=QxzWs46k5vaIFcEewcCfiFu+HVAf7S7BiVIDrND7GFc=,ssl=True,abortConnect=False,connectTimeout=3000,synctimeout=3000,connectretry=2");
            //IDatabase cache = connect.GetDatabase();
            _cache.KeyDelete(key);
        }
        public static T Get<T>(string key)
        {
            //var connect = ConnectionMultiplexer.Connect("avacomnewredis.redis.cache.windows.net:6380,password=QxzWs46k5vaIFcEewcCfiFu+HVAf7S7BiVIDrND7GFc=,ssl=True,abortConnect=False,connectTimeout=3000,synctimeout=3000,connectretry=2");
            //IDatabase cache = connect.GetDatabase();
            var r = _cache.StringGet(key);
            return Deserialize<T>(r);
        }
        public static void Clear()
        {
            var endpoints = _connectionMultiplexer.GetEndPoints(true);
            foreach (var endpoint in endpoints)
            {
                var server = _connectionMultiplexer.GetServer(endpoint);
                server.FlushAllDatabases();
            }
        }
        public static List<T> GetList<T>(string key)
        {
            return (List<T>)Get(key);
        }

        public static void SetList<T>(string key, List<T> list)
        {
            Set(key, list);
        }

        public static object Get(string key)
        {
            //var connect = ConnectionMultiplexer.Connect("avacomnewredis.redis.cache.windows.net:6380,password=QxzWs46k5vaIFcEewcCfiFu+HVAf7S7BiVIDrND7GFc=,ssl=True,abortConnect=False,connectTimeout=3000,synctimeout=3000,connectretry=2");
            //IDatabase cache = connect.GetDatabase();
           
            return Deserialize<object>(_cache.StringGet(key));
        }

        public static void Set(string key, object value)
        {
            //var connect = ConnectionMultiplexer.Connect("avacomnewredis.redis.cache.windows.net:6380,password=QxzWs46k5vaIFcEewcCfiFu+HVAf7S7BiVIDrND7GFc=,ssl=True,abortConnect=False,connectTimeout=3000,synctimeout=3000,connectretry=2");
            //IDatabase cache = connect.GetDatabase();
            _cache.StringSet(key, Serialize(value));
        }

        static byte[] Serialize(object o)
        {
            if (o == null)
            {
                return null;
            }
            var type = o?.GetType();
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, o);
                byte[] objectDataAsStream = memoryStream.ToArray();
                return objectDataAsStream;
            }
            //BinaryFormatter binaryFormatter = new BinaryFormatter();
            //binaryFormatter.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            //using (MemoryStream memoryStream = new MemoryStream())
            //{
            //    binaryFormatter.Serialize(memoryStream, o);
            //    byte[] objectDataAsStream = memoryStream.ToArray();
            //    return objectDataAsStream;
            //}
        }

        static T Deserialize<T>(byte[] stream)
        {
            if (stream == null)
            {
                return default(T);
            }
            //BinaryFormatter binaryFormatter = new BinaryFormatter();
            //using (MemoryStream memoryStream = new MemoryStream(stream))
            //{
            //    T result = (T)binaryFormatter.Deserialize(memoryStream);
            //    return result;
            //}
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Binder = new InsideCOMBinder();
            using (MemoryStream memoryStream = new MemoryStream(stream))
            {
                T result = (T)binaryFormatter.Deserialize(memoryStream);
                return result;
            }
        }
    }
    public class InsideCOMBinder : System.Runtime.Serialization.SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type tyType = null;
            string sShortAssemblyName = assemblyName.Split(',')[0];

            Assembly[] ayAssemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly ayAssembly in ayAssemblies)
            {
                if (sShortAssemblyName == ayAssembly.FullName.Split(',')[0])
                {
                    tyType = ayAssembly.GetType(typeName);
                    break;
                }
            }
            return tyType;
            //return Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));
        }
    }
}
