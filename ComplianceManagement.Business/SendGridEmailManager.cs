﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Web;
using SendGrid;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
   public class SendGridEmailManager
    {
        public static void SendGridMail(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message)
        {
            var username = ConfigurationManager.AppSettings["SendGridUserName"];
            var pswd = ConfigurationManager.AppSettings["SendGridPassword"];

            //Create the email object first, then add the properties.
            var myMessage = new SendGridMessage();

            //Add the message properties.
            myMessage.From = new MailAddress(from, fromName);
            myMessage.Subject = subject;

            // Add the HTML and Text bodies
            myMessage.Html = message;
            //myMessage.Text = "Hello World plain text!";

            if (to.Count > 0)
            {
                to.ForEach(EachTo =>
                {
                    myMessage.AddTo(EachTo);
                });
            }

            if (cc != null)
            {
                if (cc.Count > 0)
                {
                    cc.ForEach(eachCc =>
                    {
                        myMessage.AddCc(eachCc);
                    });
                }
            }

            if (bcc != null)
            {
                if (bcc.Count > 0)
                {
                    bcc.ForEach(eachBcc =>
                    {
                        myMessage.AddBcc(eachBcc);
                    });
                }
            }

            var credentials = new NetworkCredential(username, pswd);

            // Create an Web transport for sending email.
            var transportWeb = new Web(credentials);

            //Send the email.
            transportWeb.Deliver(myMessage);                  
        }

        public static void SendGridMail(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {
            var username = ConfigurationManager.AppSettings["SendGridUserName"];
            var pswd = ConfigurationManager.AppSettings["SendGridPassword"];

            //Create the email object first, then add the properties.
            var myMessage = new SendGridMessage();

            //Add the message properties.
            myMessage.From = new MailAddress(from, fromName);
            myMessage.Subject = subject;

            // Add the HTML and Text bodies
            myMessage.Html = message;
            //myMessage.Text = "Hello World plain text!";

            if (to.Count > 0)
            {
                to.ForEach(EachTo =>
                {
                    myMessage.AddTo(EachTo);
                });
            }

            if (cc.Count > 0)
            {
                cc.ForEach(eachCc =>
                {
                    myMessage.AddCc(eachCc);
                });
            }

            if (bcc.Count > 0)
            {
                bcc.ForEach(eachBcc =>
                {
                    myMessage.AddBcc(eachBcc);
                });
            }

            if (attachment.Count > 0)
            {
                foreach (var attach in attachment)
                {
                    using (var attachmentFileStream = new FileStream(attach.Item1, FileMode.Open))
                    {
                        myMessage.AddAttachment(attachmentFileStream, attach.Item2);
                        //Attachment data = new Attachment(attach.Item1, attach.Item2);
                        //myMessage.AddAttachment(attach.Item1, attach.Item2);
                    }
                }
            }

            var credentials = new NetworkCredential(username, pswd);

            // Create an Web transport for sending email.
            var transportWeb = new Web(credentials);

            //Send the email.
            transportWeb.Deliver(myMessage);
        }

        public static void SendGridNewsLetterMail(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {
            if (bcc.Count > 0)
            {
                var bccList = ListExtensions.ChunkBy(bcc, 500);

                if (bccList.Count > 0)
                {
                    bccList.ForEach(eachBccList =>
                    {
                        var username = ConfigurationManager.AppSettings["SendGridUserName"];
                        var pswd = ConfigurationManager.AppSettings["SendGridPassword"];

                        //Create the email object first, then add the properties.
                        var myMessage = new SendGridMessage();

                        //Add the message properties.
                        myMessage.From = new MailAddress(from, fromName);
                        myMessage.Subject = subject;

                        // Add the HTML and Text bodies
                        myMessage.Html = message;
                        //myMessage.Text = "Hello World plain text!";

                        if (to.Count > 0)
                        {
                            to.ForEach(EachTo =>
                            {
                                myMessage.AddTo(EachTo);
                            });
                        }

                        if (cc.Count > 0)
                        {
                            cc.ForEach(eachCc =>
                            {
                                myMessage.AddCc(eachCc);
                            });
                        }

                        if (eachBccList.Count > 0)
                        {
                            eachBccList.ForEach(eachBcc =>
                            {
                                myMessage.AddBcc(eachBcc);
                            });
                        }

                        if (attachment.Count > 0)
                        {
                            foreach (var attach in attachment)
                            {
                                using (var attachmentFileStream = new FileStream(attach.Item1, FileMode.Open))
                                {
                                    myMessage.AddAttachment(attachmentFileStream, attach.Item2);
                                    //Attachment data = new Attachment(attach.Item1, attach.Item2);
                                    //myMessage.AddAttachment(attach.Item1, attach.Item2);
                                }
                            }
                        }

                        var credentials = new NetworkCredential(username, pswd);

                        // Create an Web transport for sending email.
                        var transportWeb = new Web(credentials);

                        //Send the email.
                        transportWeb.Deliver(myMessage);
                    });
                }
            }

            #region to Multiple
            //if (bcc.Count > 0)
            //{
            //    bcc.ForEach(EachTo =>
            //    {
            //        //List<Email> TosList = new List<Email>();
            //        //if (to != null)
            //        //    to.ForEach(entry => TosList.Add(new Email(entry, entry)));

            //        //List<Email> BccsList = new List<Email>();
            //        //if (bcc != null)
            //        //    bcc.ForEach(entry => BccsList.Add(new Email(entry, entry)));

            //        //List<Email> CcsList = new List<Email>();
            //        //if (cc != null)
            //        //    cc.ForEach(entry => CcsList.Add(new Email(entry, entry)));

            //        //var personalization = new Personalization()
            //        //{
            //        //    Subject = subject,
            //        //    Tos = TosList,
            //        //    Bccs = BccsList,
            //        //    Ccs = CcsList
            //        //};                   

            //        var username = "narendrasonone";
            //        var pswd = "Narendra16!!";

            //        // Create the email object first, then add the properties.
            //        var myMessage = new SendGridMessage();   

            //        // Add the message properties.
            //        myMessage.From = new MailAddress(from);

            //        myMessage.AddTo(EachTo);
            //        myMessage.Subject = subject;

            //        // Add the HTML and Text bodies
            //        myMessage.Html = message;
            //        myMessage.Text = "Hello World plain text!";

            //        if (attachment.Count > 0)
            //        {
            //            foreach (var attach in attachment)
            //            {
            //                //Attachment data = new Attachment(attach.Item1, attach.Item2);
            //                myMessage.AddAttachment(attach.Item1, attach.Item2);
            //            }
            //        }

            //        //using (var attachmentFileStream = new FileStream(@"C:\file.txt", FileMode.Open))
            //        //{
            //        //    myMessage.AddAttachment(attachmentFileStream, "My Cool File.txt");
            //        //}

            //        var credentials = new NetworkCredential(username, pswd);

            //        // Create an Web transport for sending email.
            //        var transportWeb = new Web(credentials);

            //        //Send the email.
            //        transportWeb.Deliver(myMessage);                    
            //    });
            //}

            #endregion
        }

        public static void SendGridMailwithAttachment(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {

            var username = ConfigurationManager.AppSettings["SendGridUserName"];
            var pswd = ConfigurationManager.AppSettings["SendGridPassword"];

            //Create the email object first, then add the properties.
            var myMessage = new SendGridMessage();

            //Add the message properties.
            myMessage.From = new MailAddress(from, fromName);
            myMessage.Subject = subject;

            // Add the HTML and Text bodies
            myMessage.Html = message;
            //myMessage.Text = "Hello World plain text!";

            if (to.Count > 0)
            {
                to.ForEach(EachTo =>
                {
                    myMessage.AddTo(EachTo);
                });
            }

            if (cc.Count > 0)
            {
                cc.ForEach(eachCc =>
                {
                    myMessage.AddCc(eachCc);
                });
            }

            if (attachment.Count > 0)
            {
                foreach (var attach in attachment)
                {
                    var aa = HttpContext.Current.Server.MapPath(attach.Item2);
                    using (var attachmentFileStream = new FileStream(aa, FileMode.Open))
                    {
                        myMessage.AddAttachment(attachmentFileStream, attach.Item2);
                    }
                }
            }

            var credentials = new NetworkCredential(username, pswd);

            // Create an Web transport for sending email.
            var transportWeb = new Web(credentials);

            //Send the email.
            //transportWeb.Deliver(myMessage);
        }
        public static void SendGridNewsLetterMail1(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {

            var username = ConfigurationManager.AppSettings["SendGridUserName"];
            var pswd = ConfigurationManager.AppSettings["SendGridPassword"];

            //Create the email object first, then add the properties.
            var myMessage = new SendGridMessage();

            //Add the message properties.
            myMessage.From = new MailAddress(from, fromName);
            myMessage.Subject = subject;

            // Add the HTML and Text bodies
            myMessage.Html = message;
            //myMessage.Text = "Hello World plain text!";

            if (to.Count > 0)
            {
                to.ForEach(EachTo =>
                {
                    myMessage.AddTo(EachTo);
                });
            }

            if (cc.Count > 0)
            {
                cc.ForEach(eachCc =>
                {
                    myMessage.AddCc(eachCc);
                });
            }

            if (attachment.Count > 0)
            {
                foreach (var attach in attachment)
                {
                    var aa = HttpContext.Current.Server.MapPath(attach.Item2);
                    using (var attachmentFileStream = new FileStream(aa, FileMode.Open))
                    {
                        myMessage.AddAttachment(attachmentFileStream, attach.Item2);
                    }
                }
            }

            var credentials = new NetworkCredential(username, pswd);

            // Create an Web transport for sending email.
            var transportWeb = new Web(credentials);

            //Send the email.
            transportWeb.Deliver(myMessage);
        }
    }
}
