﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Business.License
{
    public class LicenseMgmt
    {
        #region Statutory       
        public static List<SP_LIC_GetUserLicenseTypeMappingData_Result> GetAllCategoryMappingData(long customerID, long UserID,string isstatutoryinternal)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.SP_LIC_GetUserLicenseTypeMappingData(customerID, UserID, isstatutoryinternal)
                             select row).ToList();

                return Query;
            }
        }
        public static List<int> GetAssignedroleid(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> roles = new List<int>();
                var rolesfromsp = (entities.LIC_Sp_GetAllAssignedRoles(Userid)).ToList();
                roles = rolesfromsp.Where(x => x != null).Cast<int>().ToList();

                return roles;
            }
        }
        public static List<int> GetAssignedLocationList(int UserID, int custID, String Role, string statutoryInternal)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (statutoryInternal == "S")
                {
                    if (Role == "MGMT" || Role == "AUDT" || Role == "HRMGR")
                    {
                        var query = (from row in entities.LIC_EntitiesAssignment
                                     where row.UserID == UserID
                                     select (int)row.BranchID).Distinct().ToList();

                        if (query != null)
                            LocationList = query.ToList();

                    }
                    else
                    {
                        var query = (from row in entities.ComplianceAssignedInstancesViews
                                     where row.CustomerID == custID
                                     select row.CustomerBranchID).Distinct().ToList();

                        if (query != null)
                            LocationList = query.ToList();
                    }
                }
                else if (statutoryInternal == "I")
                {
                    if (Role == "MGMT" || Role == "AUDT" || Role == "HRMGR")
                    {
                        var query = (from row in entities.LIC_InternalEntitiesAssignment
                                     where row.UserID == UserID
                                     select (int)row.BranchID).Distinct().ToList();

                        if (query != null)
                            LocationList = query.ToList();

                    }
                    else
                    {
                        var query = (from row in entities.InternalComplianceAssignedInstancesViews
                                     where row.CustomerID == custID
                                     select (int)row.CustomerBranchID).Distinct().ToList();

                        if (query != null)
                            LocationList = query.ToList();
                    }
                }
                return LocationList;
            }
        }
        public static List<long> GetLicenseScheduleonIdList(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_LicenseComplianceInstanceScheduleOnMapping
                             join row1 in entities.Lic_tbl_LicenseInstance
                             on row.LicenseID equals row1.ID
                             select row.ComplianceScheduleOnID).ToList();

                return query.ToList();
            }
        }
        public static List<Lic_tbl_StatusMaster> GetStatusList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> statuslist = new List<long>();
                statuslist.Add(6);
                statuslist.Add(7);
                var statusList = (from row in entities.Lic_tbl_StatusMaster
                                  where statuslist.Contains(row.ID)
                                  select row).ToList();

                return statusList;
            }
        }        
        public static void DeactiveAllCategoryOfUser(int UserID, long LogedInUserID, long CustomerID,string isstatutoryinternal)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.Lic_tbl_UserAndLicenseTypeMapping
                             where row.IsActive == true &&
                             row.UserID == UserID &&
                             row.CustomerID == CustomerID
                             && row.Is_Statutory_NoStatutory== isstatutoryinternal
                             select row).ToList();

                if (Query.Count > 0)
                {
                    Query.ForEach(EachUser =>
                    {
                        EachUser.IsActive = false;
                        EachUser.UpdateBy = LogedInUserID;
                        EachUser.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                    });
                }
            }
        }
        public static bool SaveUserCategoryManagement(Lic_tbl_UserAndLicenseTypeMapping newAssignment)
        {
            bool result = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.Lic_tbl_UserAndLicenseTypeMapping
                             where row.UserID == newAssignment.UserID &&
                             row.LicenseTypeID == newAssignment.LicenseTypeID &&
                             row.CustomerID == newAssignment.CustomerID
                             && row.Is_Statutory_NoStatutory == newAssignment.Is_Statutory_NoStatutory
                             select row).FirstOrDefault();

                if (Query != null)
                {
                    Query.IsActive = true;
                    Query.UpdateBy = newAssignment.UpdateBy;
                    Query.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                    result = true;
                }
                else
                {
                    newAssignment.CreatedOn = DateTime.Now;
                    entities.Lic_tbl_UserAndLicenseTypeMapping.Add(newAssignment);
                    entities.SaveChanges();
                    result = true;
                }
                return result;
            }
        }            
        public static List<SP_GetAuditLogLicenseTransaction_Result> GetAllTransactionLog(long licenseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.SP_GetAuditLogLicenseTransaction(licenseID)                                                                   
                                  select row).ToList();

                return statusList;
            }
        }
        public static LIC_PreviouslyLicenseDetails_Result GetPreviousLicenseDetail(long licenseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var masterquery = (from row in entities.LIC_PreviouslyLicenseDetails(licenseID)
                                   select row).FirstOrDefault();
                return masterquery;
            }
        }        
        public static bool UploadLicenseCreateDocuments(long complianceTransactionId, long complianceScheduleOnId, List<FileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, byte[]>> filelist)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Statutory_SaveDocFiles(filelist);

                            if (files != null)
                            {
                                foreach (FileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.FileDatas.Add(fl);
                                    entities.SaveChanges();

                                    FileDataMapping fileMapping = new FileDataMapping();
                                    fileMapping.TransactionID = complianceTransactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = complianceScheduleOnId;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.FileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filelist != null)
                            {
                                foreach (var dfile in filelist)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool ExistsActivationDate(long LicenseID ,DateTime enddate)
        {
            bool result = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.SP_LIC_CheckDateInBetween(LicenseID, enddate)                                
                                 select row);

                    result = query.Select(entry => true).SingleOrDefault();

                }
                return result;
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static bool ExistsComplianceScheduleOn(long licenseID,DateTime scheduleon)
        {
            bool result = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Lic_tbl_LicenseComplianceInstanceScheduleOnMapping
                                 join row1 in entities.ComplianceScheduleOns 
                                 on row.ComplianceInstanceID equals row1.ComplianceInstanceID
                                 where row.LicenseID== licenseID  && row1.ScheduleOn == scheduleon
                                 select row);
                   
                        result = query.Select(entry => true).SingleOrDefault();
                   
                }
                return result;
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static Lic_tbl_LicenseComplianceInstanceScheduleOnMapping GetLicenseComplianceInstanceScheduleOnByID(long licenseID)
        {
            Lic_tbl_LicenseComplianceInstanceScheduleOnMapping record = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    record = (from row in entities.Lic_tbl_LicenseComplianceInstanceScheduleOnMapping
                              where row.LicenseID == licenseID
                              select row).OrderByDescending(x => x.ID).FirstOrDefault();
                    return record;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return record;
            }
        }        
        public static List<Lic_SP_MasterWorkspaceDetail_Result> GetAllMasterLicenseDetials(int customerID, List<int> branchList, string licenseStatus, long licenseTypeID, string Is_StatutoryInternal)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_SP_MasterWorkspaceDetail(customerID, Is_StatutoryInternal)
                             select row).ToList();

                if (query.Count > 0)
                {                                          
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();
                                       
                    if (!string.IsNullOrEmpty(licenseStatus))
                        query = query.Where(entry => entry.Status == licenseStatus).ToList();

                    if (licenseTypeID != -1 && licenseTypeID != 0)
                        query = query.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();

                }
                                            
                return query.ToList();
            }
        }
        public static List<Lic_SP_MyWorkspaceDetail_Result> GetAllLicenseDetials(int customerID, int UserID, List<int> branchList, int deptID, string licenseStatus, long licenseTypeID, string IsPERMGMTCA,string Is_StatutoryInternal)
        {
            List<int> statusId = new List<int>();
            statusId.Add(4);
            statusId.Add(5);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_SP_MyWorkspaceDetail(UserID, customerID, IsPERMGMTCA, Is_StatutoryInternal)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (IsPERMGMTCA == "MGMT" || IsPERMGMTCA == "CADMN")
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList();                        
                    }                   
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (IsPERMGMTCA == "MGMT" || IsPERMGMTCA == "CADMN")
                    {
                        List<int> cstatusId = new List<int>();
                        cstatusId.Add(1);
                        cstatusId.Add(2);
                        cstatusId.Add(3);
                        cstatusId.Add(6);
                        cstatusId.Add(8);
                        cstatusId.Add(10);

                        if (!string.IsNullOrEmpty(licenseStatus))
                        {
                            if (licenseStatus == "Applied")
                            {
                                query = query.Where(row => row.MGRStatus == licenseStatus 
                                && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                                 
                            }
                            else if (licenseStatus == "Active")
                            {
                                query = query.Where(row => row.MGRStatus == licenseStatus
                                //&& !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                                && cstatusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Expiring")
                            {
                                query = query.Where(row => row.MGRStatus == licenseStatus 
                                //&& !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                                && cstatusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Expired")
                            {
                                query = query.Where(row => row.MGRStatus == licenseStatus
                                //&& !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                                && cstatusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Rejected")
                            {
                                query = query.Where(row => row.MGRStatus == licenseStatus).ToList();
                            }
                            else if (licenseStatus == "PR")
                            {
                                query = query.Where(row => row.StatusID == 6 || row.StatusID == 7).ToList();
                            }
                            else
                            {
                                query = query.Where(entry => entry.MGRStatus == licenseStatus).ToList();
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(licenseStatus))
                        {
                            if (licenseStatus == "Applied")
                            {
                                query = query.Where(row => row.Status == licenseStatus && statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Active")
                            {
                                query = query.Where(row => row.Status == licenseStatus && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Expiring")
                            {
                                query = query.Where(row => row.Status == licenseStatus && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Expired")
                            {
                                query = query.Where(row => row.Status == licenseStatus && !statusId.Contains((int)row.ComplianceStatusID)).ToList();
                            }
                            else if (licenseStatus == "Rejected")
                            {
                                query = query.Where(row => row.Status == licenseStatus).ToList();
                            }
                            else if (licenseStatus == "PR")
                            {
                                query = query.Where(row => row.StatusID == 6 || row.StatusID == 7).ToList();
                            }
                            else
                            {
                                query = query.Where(entry => entry.Status == licenseStatus).ToList();
                            }
                        }
                    }
                    if (licenseStatus == "" || licenseStatus == "-1")
                    {
                        query = query.Where(row => (row.Status == "Applied" || !statusId.Contains((int)row.ComplianceStatusID))).ToList();
                    }

                    if (licenseTypeID != -1 && licenseTypeID != 0)
                        query = query.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
                }
                return query.ToList();
            }
        }        
                            
        public static void InsertLog(Exception ex, string ClassName, string FunctionName)
        {
            try
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> lst_LogMessages = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = ClassName;
                msg.FunctionName = FunctionName;
                msg.CreatedOn = DateTime.Now;
                if (ex != null)
                {
                    msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                    msg.StackTrace = ex.StackTrace;
                }
                else
                {
                    msg.Message = ClassName;
                    msg.StackTrace = ClassName;
                }
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;

                if (ex != null)
                {
                    lst_LogMessages.Add(new com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage() { LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ex.Message + "----\r\n" + ex.InnerException, StackTrace = ex.StackTrace, CreatedOn = DateTime.Now });
                }
                else
                {
                    lst_LogMessages.Add(new com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage() { LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ClassName, StackTrace = ClassName, CreatedOn = DateTime.Now });
                }
                Business.ComplianceManagement.InsertLogToDatabase(lst_LogMessages);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        #region License        
        public static Lic_tbl_LicenseInstance GetLicenseByID(long licenseID)
        {
            Lic_tbl_LicenseInstance record = new Lic_tbl_LicenseInstance();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    record = (from row in entities.Lic_tbl_LicenseInstance
                              where row.ID == licenseID
                              select row).FirstOrDefault();
                    return record;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return record;
            }
        }       
        public static bool ExistsLicenseNo(int customerID, string licenseNo)
        {
         
            try
            {
                bool result = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Lic_tbl_LicenseInstance
                                 where row.LicenseNo.Trim().ToUpper().Equals(licenseNo.Trim().ToUpper())
                                 && row.CustomerID == customerID
                                 && row.IsDeleted == false
                                 select row);
                    result = query.Select(entry => true).SingleOrDefault();                  
                    return result;
                }               
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }       
        public static long CreateLicenseLog(Lic_tbl_LicenseInstance_Log licenselogRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    licenselogRecord.CreatedOn = DateTime.Now;
                    entities.Lic_tbl_LicenseInstance_Log.Add(licenselogRecord);
                    entities.SaveChanges();

                    return licenselogRecord.ID;
                }
                catch (Exception ex)
                {
                    LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }
        public static long CreateLicense(Lic_tbl_LicenseInstance licenseRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    licenseRecord.CreatedOn = DateTime.Now;                    
                    entities.Lic_tbl_LicenseInstance.Add(licenseRecord);
                    entities.SaveChanges();

                    return licenseRecord.ID;
                }
                catch (Exception ex)
                {
                    LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }
        public static bool UpdateLicenseNew(Lic_tbl_LicenseInstance licenseRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var recordToUpdate = (from row in entities.Lic_tbl_LicenseInstance
                                          where row.ID == licenseRecord.ID
                                          select row).FirstOrDefault();

                    if (recordToUpdate != null)
                    {
                        recordToUpdate.LicenseNo = licenseRecord.LicenseNo;
                        recordToUpdate.LicenseTitle = licenseRecord.LicenseTitle;                        
                        recordToUpdate.StartDate = licenseRecord.StartDate;
                        recordToUpdate.EndDate = licenseRecord.EndDate;                        
                        recordToUpdate.UpdatedBy = licenseRecord.UpdatedBy;
                        recordToUpdate.UpdatedOn = licenseRecord.UpdatedOn;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        public static bool UpdateLicense(Lic_tbl_LicenseInstance licenseRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var recordToUpdate = (from row in entities.Lic_tbl_LicenseInstance
                                          where row.ID == licenseRecord.ID
                                          select row).FirstOrDefault();

                    if (recordToUpdate != null)
                    {
                        recordToUpdate.LicenseNo = licenseRecord.LicenseNo;
                        recordToUpdate.LicenseTitle = licenseRecord.LicenseTitle;
                        recordToUpdate.LicenseDetailDesc = licenseRecord.LicenseDetailDesc;
                        recordToUpdate.StartDate = licenseRecord.StartDate;
                        recordToUpdate.EndDate = licenseRecord.EndDate;                       
                        recordToUpdate.RemindBeforeNoOfDays = licenseRecord.RemindBeforeNoOfDays;
                        recordToUpdate.UpdatedBy = licenseRecord.UpdatedBy;
                        recordToUpdate.UpdatedOn = licenseRecord.UpdatedOn;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        #endregion
                
        #region License Status-Transaction
        public static bool CreateLicenseStatusTransaction(Lic_tbl_LicenseStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objNewStatusRecord.CreatedOn = DateTime.Now;
                    objNewStatusRecord.UpdatedOn = DateTime.Now;

                    entities.Lic_tbl_LicenseStatusTransaction.Add(objNewStatusRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        #endregion

        #region License Audit Log
        public static bool CreateLicenseAuditLog(Lic_tbl_LicenseAudit_Log objNewStatusRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objNewStatusRecord.CreatedOn = DateTime.Now;                                    
                    entities.Lic_tbl_LicenseAudit_Log.Add(objNewStatusRecord);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        #endregion

        #region License Status
        public static List<Lic_tbl_StatusMaster> GetStatusList_All()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.Lic_tbl_StatusMaster
                                  where row.IsDeleted == false
                                   && row.IsVisibleToUser == true
                                  select row).ToList();                
                return statusList;
            }
        }        
        #endregion
               
        #region License Master       
        public static List<long> GetLicenseToComplianceInstanceMappingList(long licenseId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> complianceIdsList = (from row in entities.Lic_tbl_LicenseComplianceInstanceMapping
                                                where row.LicenseID == licenseId
                                                select row.ComplianceInstanceID).ToList();

                return complianceIdsList;
            }
        }        
        public static long CreateComplianceInstance(ComplianceInstance objNewComplianceInstanceRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.ComplianceInstances.Add(objNewComplianceInstanceRecord);
                    entities.SaveChanges();

                    return objNewComplianceInstanceRecord.ID;


                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }        
        public static long CreateComplianceAssignment(ComplianceAssignment objNewComplianceAssignmentsRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.ComplianceAssignments.Add(objNewComplianceAssignmentsRecord);
                    entities.SaveChanges();

                    return objNewComplianceAssignmentsRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }        
        public static long CreateComplianceScheduleOn(ComplianceScheduleOn objNewComplianceScheduleOnRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.ComplianceScheduleOns.Add(objNewComplianceScheduleOnRecord);
                    entities.SaveChanges();

                    return objNewComplianceScheduleOnRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }      
        public static long CreateComplianceTransaction(ComplianceTransaction objNewComplianceTransactionRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.ComplianceTransactions.Add(objNewComplianceTransactionRecord);
                    entities.SaveChanges();

                    return objNewComplianceTransactionRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }                
        public static long CreateLicenseLicenseComplianceInstanceScheduleOnMapping(Lic_tbl_LicenseComplianceInstanceScheduleOnMapping objNewLicComplianceInstMappingRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Lic_tbl_LicenseComplianceInstanceScheduleOnMapping.Add(objNewLicComplianceInstMappingRecord);
                    entities.SaveChanges();

                    return objNewLicComplianceInstMappingRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public static long CreateLicenseComplianceInstanceMapping(Lic_tbl_LicenseComplianceInstanceMapping objNewLicComplianceInstMappingRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Lic_tbl_LicenseComplianceInstanceMapping.Add(objNewLicComplianceInstMappingRecord);
                    entities.SaveChanges();

                    return objNewLicComplianceInstMappingRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }       
        #endregion

        #region License Creation
        public static List<Lic_SP_GetAssignedCompliances_All_Result> GetAssignedCompliancesList(long customerID, int loggedInUserID, int roleID, List<int> branchList, int deptID, string isStatutory, long licenseTypeID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Lic_SP_GetAssignedCompliances_All(customerID)
                                 select row).ToList();

                    if (query.Count > 0)
                    {
                        if (isStatutory != null)
                            query = query.Where(entry => entry.IsStatutory == isStatutory).ToList();

                        if (licenseTypeID != -1 && licenseTypeID != 0)
                            query = query.Where(entry => entry.LicensetypeID == licenseTypeID).ToList();

                        if (branchList.Count > 0)
                            query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();                       
                    }
                    return query.ToList();
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<Lic_SP_GetTempActivatedCompliances_All_Result> GetTempActivatedCompliancesList(long customerID, int loggedInUserID, int roleID, List<int> branchList, int deptID, string isStatutory, long licenseTypeID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Lic_SP_GetTempActivatedCompliances_All(customerID)
                                 select row).ToList();

                    if (query.Count > 0)
                    {
                        if (isStatutory != null)
                            query = query.Where(entry => entry.IsStatutory == isStatutory).ToList();

                        if (licenseTypeID != -1 && licenseTypeID != 0)
                            query = query.Where(entry => entry.LicensetypeID == licenseTypeID).ToList();

                        if (branchList.Count > 0)
                            query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();
                    }
                    return query.ToList();
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static bool UpdateTemplateAssignment(long complianceId, int customerbranchId)
        {
            bool result = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var recordToUpdate = (from row in entities.TempAssignmentTables
                                      where row.ComplianceId == complianceId
                                      && row.CustomerBranchID == customerbranchId
                                      && row.IsActive == true
                                      select row).ToList();

                if (recordToUpdate != null && recordToUpdate.Count>0)
                {
                    if (recordToUpdate.Count  == 2 || recordToUpdate.Count == 3)
                    {
                        recordToUpdate.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                        result= true;
                        
                    }
                    else 
                    {
                        List<long> roles = new List<long>();
                        roles.Add(3);
                        roles.Add(4);
                        roles.Add(6);
                        foreach (var item in roles)
                        {
                            var recorddetails = (from row in entities.TempAssignmentTables
                                                  where row.ComplianceId == complianceId
                                                  && row.CustomerBranchID == customerbranchId
                                                  && row.IsActive == true
                                                  && row.RoleID == item
                                                 select row).FirstOrDefault();
                            if (recorddetails != null)
                            {
                                recorddetails.IsActive = false;                             
                                entities.SaveChanges();
                                result= true;
                            }                           
                        }                                             
                    }                
                }
                return result;
            }           
        }
        #endregion       
        public static List<SP_LicenseMyReport_Result> GetAllReportData(int customerID, int loggedInUserID, string loggedInUserRole, List<int> branchList, int deptID, long licenseStatusID, long licenseTypeID, string isstatutoryinternal) /*int branchID,*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_LicenseMyReport(loggedInUserID, customerID, loggedInUserRole, isstatutoryinternal)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (loggedInUserRole == "MGMT" || loggedInUserRole == "CADMN")
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList();
                    }
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (licenseStatusID != -1 && licenseStatusID != 0)
                        query = query.Where(entry => entry.StatusID == licenseStatusID).ToList();

                    if (licenseTypeID != -1 && licenseTypeID != 0)
                        query = query.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
                }

                return query;
            }
        }
        #endregion

        
    }
}
