﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.License
{
   public class LIC_Assign_Entity_Management
    {
        #region Statutory
        public static LIC_EntitiesAssignment SelectEntity(int branchId = -1, int userID = -1, int LictypeId = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var EntitiesAssignmentData = (from row in entities.LIC_EntitiesAssignment
                                              where row.BranchID == branchId 
                                              && row.UserID == userID && row.LicenseTypeID == LictypeId
                                              select row).FirstOrDefault();
                return EntitiesAssignmentData;
            }
        }
        public static void Create(LIC_EntitiesAssignment objEntitiesAssignment)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.LIC_EntitiesAssignment.Add(objEntitiesAssignment);
                entities.SaveChanges();
            }
        }
        public static void Create(List<LIC_EntitiesAssignment> objEntitiesAssignment)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objEntitiesAssignment.ForEach(entry =>
                {
                    entities.LIC_EntitiesAssignment.Add(entry);
                });
                entities.SaveChanges();
            }

        }
        public static List<SP_LIC_ComplianceAssignmentEntitiesView_Result> SelectAllEntities(int customerID, int userID = -1,int branchId = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceTransactionEntity = (from row in entities.SP_LIC_ComplianceAssignmentEntitiesView(customerID)
                                                   select row).ToList();

                var branchIds = (from row in entities.CustomerBranches
                                 where row.IsDeleted == false && row.CustomerID == customerID
                                 select row.ID).ToList();

                if (branchIds != null)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => branchIds.Contains((int)entry.BranchID)).ToList();
                }
                if (branchId != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.BranchID == branchId).ToList();
                }
                if (userID != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
                }

                return ComplianceTransactionEntity;
            }
        }
       // sandesh
        public static List<SP_LIC_ComplianceAssignmentEntitiesView_Result> SelectAllEntitiesList(int CustomerID, int userID, List<long> CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceTransactionEntity = (from row in entities.SP_LIC_ComplianceAssignmentEntitiesView(CustomerID)
                                                   where CustomerBranchID.Contains(row.BranchID)
                                                   select row).ToList();

                if (userID != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
                }
                return ComplianceTransactionEntity;
            }
        }
        #endregion

        #region Internal
        public static LIC_InternalEntitiesAssignment SelectEntityInternal(int branchId = -1, int userID = -1, int LictypeId = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var EntitiesAssignmentData = (from row in entities.LIC_InternalEntitiesAssignment
                                              where row.BranchID == branchId
                                              && row.UserID == userID && row.LicenseTypeID == LictypeId
                                              select row).FirstOrDefault();
                return EntitiesAssignmentData;
            }
        }
        public static void CreateInternal(LIC_InternalEntitiesAssignment objEntitiesAssignment)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.LIC_InternalEntitiesAssignment.Add(objEntitiesAssignment);
                entities.SaveChanges();
            }
        }
        public static void CreateInternal(List<LIC_InternalEntitiesAssignment> objEntitiesAssignment)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objEntitiesAssignment.ForEach(entry =>
                {
                    entities.LIC_InternalEntitiesAssignment.Add(entry);
                });
                entities.SaveChanges();
            }

        }
        public static List<SP_LIC_InternalComplianceAssignmentEntitiesView_Result> SelectAllEntitiesInternalList(int customerID, int userID,  List<long> CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {               
                var ComplianceTransactionEntity = (from row in entities.SP_LIC_InternalComplianceAssignmentEntitiesView(customerID)
                                                   where CustomerBranchID.Contains(row.BranchID)
                                                   select row).ToList();
               
                if (userID != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
                }

                return ComplianceTransactionEntity;
            }
        }
        public static List<SP_LIC_InternalComplianceAssignmentEntitiesView_Result> SelectAllEntitiesInternal(int customerID, int userID = -1, int branchId = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceTransactionEntity = (from row in entities.SP_LIC_InternalComplianceAssignmentEntitiesView(customerID)
                                                   select row).ToList();

                var branchIds = (from row in entities.CustomerBranches
                                 where row.IsDeleted == false && row.CustomerID == customerID
                                 select row.ID).ToList();

                if (branchIds != null)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => branchIds.Contains((int)entry.BranchID)).ToList();
                }
                if (branchId != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.BranchID == branchId).ToList();
                }
                if (userID != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
                }

                return ComplianceTransactionEntity;
            }
        }
        #endregion
    }
}
