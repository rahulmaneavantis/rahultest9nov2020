﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.License
{
    public class LicenseDocumentManagement
    {
        public static Lic_tbl_LicenseInstance GetLicense(long LicenseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ScheduleOnData = (from row in entities.Lic_tbl_LicenseInstance
                                                          where row.ID == LicenseID
                                                          select row).FirstOrDefault();               
                return ScheduleOnData;
            }
        }
        public static List<SP_GetLicenseDocument_Result> GetFileData(long LicenseID,long fileid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.SP_GetLicenseDocument(LicenseID)                                
                                select row).ToList();

                if (fileid !=-1)
                {
                    fileData = fileData.Where(entry => entry.FileID == fileid).ToList();
                }

                return fileData;
            }
        }
        public static List<Lic_SP_MyDocuments_All_Result> GetAssigned_MyDocuments(int customerID, int loggedInUserID, string loggedInUserRole, List<int> branchList, long licenseStatusID, long licenseTypeID,string isstatutoryinternal)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_SP_MyDocuments_All(loggedInUserID, customerID, loggedInUserRole, isstatutoryinternal)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();
                                        
                    if (licenseStatusID != -1 && licenseStatusID != 0)
                        query = query.Where(entry => entry.StatusID == licenseStatusID).ToList();

                    if (licenseTypeID != -1 && licenseTypeID != 0)
                        query = query.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();

                    if (loggedInUserRole == "MGMT" || loggedInUserRole == "CADMN")
                        query = query.Where(entry => (entry.RoleID == 3)).ToList();

                    query = query.GroupBy(a => a.LicenseID).Select(a => a.FirstOrDefault()).ToList();                    
                }

                return query.ToList();
            }
        }
    }
}
