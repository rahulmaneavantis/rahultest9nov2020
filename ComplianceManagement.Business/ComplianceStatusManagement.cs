﻿using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ComplianceStatusManagement
    {
        public static List<ComplianceStatu> GetStatusList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.ComplianceStatus
                                  where row.ID != 16
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<ComplianceStatu> GetNotCompliedStatusList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.ComplianceStatus
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<ComplianceStatu> GetEscalationRevStatusList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.ComplianceStatus
                                  where row.ID == 13 || row.ID == 14
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<ComplianceStatu> GetEscalationStatusList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.ComplianceStatus
                                  where row.ID == 12
                                  select row).ToList();

                return statusList;
            }
        }
        public static List<ComplianceStatusTransition> GetStatusTransitionList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transitionList = (from row in entities.ComplianceStatusTransitions
                                  select row).ToList();

                return transitionList;
            }
        }
        public static List<ComplianceStatusTransition> GetStatusTransitionListByInitialId(int initialId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transitionList = (from row in entities.ComplianceStatusTransitions
                                      select row).ToList();

                if (initialId != 0)
                {
                    transitionList = transitionList.Where(row => row.InitialStateID == initialId).ToList();
                }

                return transitionList.ToList();
            }
        }
    }
}
