﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    [Serializable()]
    public class TempComplianceAsignmentProperties
    {
        private int complianceId;
        private string startDate;
        private string role;
        private int id;
        private int userID;
        private bool performer;
        private int customerBranchID;
        private long departmentID;
        private string sequenceID;


        public int CustomerBranchID
        {
            get { return customerBranchID; }
            set { customerBranchID = value; }
        }

        public int ComplianceId
        {
            get { return complianceId; }
            set { complianceId = value; }
        }

        public string StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        public string Role
        {
            get { return role; }
            set { role = value; }
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public int UserID
        {
            get { return userID; }
            set { userID = value; }
        }
        public bool Performer
        {
            get { return performer; }
            set { performer = value; }
        }
        public long DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }            
        }

        public string SequenceID
        {
            get { return sequenceID; }
            set { sequenceID = value; }
        }
    }



    [Serializable()]
    public class ComplianceAssignmentLeave
    {

        private int complianceId;
        private bool performer;
        private bool reviewer;
        private string shortdeription;


        public int ComplianceId
        {
            get { return complianceId; }
            set { complianceId = value; }
        }

        public string shortdescription
        {
            get { return shortdeription; }
            set { shortdeription = value; }
        }

        public bool Performer
        {
            get { return performer; }
            set { performer = value; }
        }

        public bool Reviewer
        {
            get { return reviewer; }
            set { reviewer = value; }
        }
    }

}
