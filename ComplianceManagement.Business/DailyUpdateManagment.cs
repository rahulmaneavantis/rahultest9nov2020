﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class DailyUpdateManagment
    {
        public static List<long> GetComplianceMappedID(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.DailyUpdates
                                      join row1 in entities.DailyUpdateComplianceMappings
                                      on row.ID equals row1.DailyUpdateID
                                      where row.ID == DailyUpdateID && row.IsDeleted == false
                                      select row1.ComplianceID).ToList();

                return eventMappedIDs;
            }

        }
        public static void RemoveDailyUpdateComplianceMapping(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.DailyUpdateComplianceMappings
                           where row.DailyUpdateID == DailyUpdateID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    DailyUpdateComplianceMapping prevmappedids = (from row in entities.DailyUpdateComplianceMappings
                                                                  where row.ID == entry
                                                                  select row).FirstOrDefault();
                    entities.DailyUpdateComplianceMappings.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }
        }
        public static void CreateDailyUpdateComplianceMapping(DailyUpdateComplianceMapping comMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.DailyUpdateComplianceMappings.Add(comMapping);
                entities.SaveChanges();
            }
        }
        public static DailyUpdateDocument GetDailyUpdateDocument(int DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.DailyUpdateDocuments
                            where row.DailyUpdateID == DailyUpdateID
                            select row).FirstOrDefault();
                return data;
            }
        }
        public static void CreateDailyUpdateDocument(DailyUpdateDocument doc)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (doc != null)
                {
                    entities.DailyUpdateDocuments.Add(doc);
                    entities.SaveChanges();
                }
            }
        }
        public static void RemoveDailyUpdateDocument(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.DailyUpdateDocuments
                           where row.DailyUpdateID == DailyUpdateID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    DailyUpdateDocument prevmappedids = (from row in entities.DailyUpdateDocuments
                                                         where row.ID == entry
                                                         select row).FirstOrDefault();
                    entities.DailyUpdateDocuments.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }
        }
        public static void UpdateEventInternalComplianceMapping(EventInternalComplianceMapping eventInternalComplianceMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                EventInternalComplianceMapping eventComplianceMapping = (from row in entities.EventInternalComplianceMappings
                                             where row.ParentEventID == eventInternalComplianceMapping.ParentEventID && row.IntermediateEventID == eventInternalComplianceMapping.IntermediateEventID
                                             && row.SubEventID == eventInternalComplianceMapping.SubEventID && row.InternalComplianceID == eventInternalComplianceMapping.InternalComplianceID
                                             select row).FirstOrDefault();

                eventComplianceMapping.Days = eventInternalComplianceMapping.Days;
                eventComplianceMapping.IsOneTime = eventInternalComplianceMapping.IsOneTime;
                entities.SaveChanges();
            }
        }
        public static void CreateDailyUpdateIndustryMapping(DailyUpdateIndustryMapping IndustryMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.DailyUpdateIndustryMappings.Add(IndustryMapping);
                entities.SaveChanges();
            }
        }
        public static void RemoveDailyUpdateIndustryMapping(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.DailyUpdateIndustryMappings
                           where row.DailyUpdateID == DailyUpdateID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    DailyUpdateIndustryMapping prevmappedids = (from row in entities.DailyUpdateIndustryMappings
                                                              where row.ID == entry
                                                              select row).FirstOrDefault();
                    entities.DailyUpdateIndustryMappings.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }
        }

        public static void RemoveDailyUpdateStateMapping(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.DailyUpdate_StateMapping
                           where row.DailyUpdateID == DailyUpdateID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    DailyUpdate_StateMapping prevmappedids = (from row in entities.DailyUpdate_StateMapping
                                                              where row.ID == entry
                                                              select row).FirstOrDefault();
                    entities.DailyUpdate_StateMapping.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }
        }

        public static void RemoveDailyUpdateCategoryMapping(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.DailyUpdate_CategoryMapping
                           where row.DailyUpdateID == DailyUpdateID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    DailyUpdate_CategoryMapping prevmappedids = (from row in entities.DailyUpdate_CategoryMapping
                                                                 where row.ID == entry
                                                                 select row).FirstOrDefault();
                    entities.DailyUpdate_CategoryMapping.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }
        }

        public static List<int> GetStateMappedID(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.DailyUpdates
                                      join row1 in entities.DailyUpdate_StateMapping
                                      on row.ID equals row1.DailyUpdateID
                                      where row.ID == DailyUpdateID && row.IsDeleted == false
                                      select row1.StateID).ToList();

                return eventMappedIDs;
            }

        }

        public static List<int> GetComplianceCategoryFromScheme(int SchemeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CategoryIDList = (from row in entities.Schemes
                                      join row1 in entities.SchemeMappings
                                      on row.ID equals row1.SchemeID
                                      join row2 in entities.Acts
                                      on row1.ActID equals row2.ID
                                      join row3 in entities.ComplianceCategories
                                      on row2.ComplianceCategoryId equals row3.ID
                                      where row2.IsDeleted == false
                                      && row.ID == SchemeID
                                      select row3.ID).Distinct().ToList();
                return CategoryIDList;
            }
        }
        public static List<int> GetComplianceCategoryFromAct(List<int> actList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CategoryIDList = (from row in entities.ComplianceCategories
                                      join row1 in entities.Acts
                                      on row.ID equals row1.ComplianceCategoryId
                                      where row1.IsDeleted == false
                                      && actList.Contains(row1.ID)
                                      select row.ID).Distinct().ToList();
                return CategoryIDList;
            }
        }


        public static List<int> GetIndustryMappedID(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IndustryMappedIDs = (from row in entities.DailyUpdates
                                         join row1 in entities.DailyUpdateIndustryMappings
                                         on row.ID equals row1.DailyUpdateID
                                         where row.ID == DailyUpdateID && row.IsDeleted == false
                                         select row1.IndustryID).ToList();

                return IndustryMappedIDs;
            }

        }
        public static List<int> GetCategoryMappedID(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.DailyUpdates
                                      join row1 in entities.DailyUpdate_CategoryMapping
                                      on row.ID equals row1.DailyUpdateID
                                      where row.ID == DailyUpdateID && row.IsDeleted == false
                                      select row1.CategoryID).ToList();

                return eventMappedIDs;
            }

        }
        public static void CreateDailyUpdateStateMapping(DailyUpdate_StateMapping stateMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.DailyUpdate_StateMapping.Add(stateMapping);
                entities.SaveChanges();
            }
        }
        public static void CreateDailyUpdateCategoryMapping(DailyUpdate_CategoryMapping categoryMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.DailyUpdate_CategoryMapping.Add(categoryMapping);
                entities.SaveChanges();
            }
        }

        public static bool CheckUnsubscribeEntry(long UserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.EmailUnsubscriptions
                                 where row.UserID == UserID
                                 select row).FirstOrDefault();

                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool EmailUnsubscribe(EmailUnsubscription unsub)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.EmailUnsubscriptions.Add(unsub);
                    entities.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// Created By Hardik Sapara For Grid Bind 
        /// Gets all daily update listforgrid.
        /// </summary>
        /// <returns>List&lt;DailyUpdate&gt;.</returns>
        /// 


        public static List<DailyUpdate> GetAllDailyUpdateListforgrid()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<DailyUpdate> eventList = new List<DailyUpdate>();

                eventList = (from row in entities.DailyUpdates
                             where row.IsDeleted == false
                             orderby row.ID descending
                             select row).ToList();
                return eventList;
            }
        }

        /// <summary>
        /// Created By Hardik Sapara For Grid Bind for news letter.
        /// Gets all news letter list.
        /// </summary>
        /// <returns>List&lt;NewsLetter&gt;.</returns>
        public static List<NewsLetter> GetAllNewsLetterList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<NewsLetter> eventList = new List<NewsLetter>();

                eventList = (from row in entities.NewsLetters
                             where row.IsDeleted == false || row.IsDeleted ==null
                             orderby row.ID descending
                             select row).ToList();
                return eventList;
            }
        }
        public static List<DailyUpdate> GetAllDailyUpdate()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<DailyUpdate> eventList = new List<DailyUpdate>();

                eventList = (from row in entities.DailyUpdates
                             where row.IsDeleted == false
                             select row).ToList();

                eventList = eventList.OrderByDescending(s => s.ID).ToList();
                return eventList;
            }
        }

        public static DailyUpdate GetByID(long eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.DailyUpdates
                            where row.ID == eventID
                            select row).FirstOrDefault();
                return data;
            }
        }


        public static void DeleteDailyUpdate(int dailyUpdatesID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DailyUpdate DailyUpdateDelete = (from row in entities.DailyUpdates
                                       where row.ID == dailyUpdatesID
                                       select row).FirstOrDefault();

                DailyUpdateDelete.IsDeleted = true;
                entities.SaveChanges();
            }
        }
        public static void CreateDailyUpdate(DailyUpdate dailyUpdates)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.DailyUpdates.Add(dailyUpdates);
                entities.SaveChanges();
            }
        }



        public static void UpdateDailyUpdate(DailyUpdate dailyUpdates)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DailyUpdate eventToUpdate = (from row in entities.DailyUpdates
                                             where row.ID == dailyUpdates.ID
                                             select row).FirstOrDefault();

                eventToUpdate.Title = dailyUpdates.Title;
                eventToUpdate.Description = dailyUpdates.Description;
                eventToUpdate.SchemeID = dailyUpdates.SchemeID;
                eventToUpdate.Link = dailyUpdates.Link;
                eventToUpdate.SelectAll = dailyUpdates.SelectAll; 
                eventToUpdate.IsDeleted = false;
                eventToUpdate.UpdatedBy = dailyUpdates.UpdatedBy;
                eventToUpdate.UpdatedDate = dailyUpdates.UpdatedDate;
                eventToUpdate.SubTitle = dailyUpdates.SubTitle;
                eventToUpdate.type = dailyUpdates.type;
                eventToUpdate.IsActive = dailyUpdates.IsActive;
                eventToUpdate.MainFlag = dailyUpdates.MainFlag;
                eventToUpdate.MetaKeyWord = dailyUpdates.MetaKeyWord;
                eventToUpdate.MetaDesc = dailyUpdates.MetaDesc;
                eventToUpdate.IsMiscellaneous = dailyUpdates.IsMiscellaneous;
                eventToUpdate.RegulatorID = dailyUpdates.RegulatorID;
                eventToUpdate.MinistryID = dailyUpdates.MinistryID;
                eventToUpdate.DeptID = dailyUpdates.DeptID;
                eventToUpdate.IsTranslate = dailyUpdates.IsTranslate;
                eventToUpdate.IsCovid = dailyUpdates.IsCovid;
                entities.SaveChanges();
            }
        }

        public static List<NewsLetter> GetAllNewsLetter()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<NewsLetter> eventList = new List<NewsLetter>();

                eventList = (from row in entities.NewsLetters
                             where row.IsDeleted == false
                             select row).ToList();

                return eventList;
            }
        }

        public static void CreateNewsLetter(NewsLetter eventData)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.NewsLetters.Add(eventData);
                entities.SaveChanges();
            }
        }
        public static void UpdateNewsLetter(NewsLetter eventData)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                NewsLetter eventToUpdate = (from row in entities.NewsLetters
                                            where row.ID == eventData.ID
                                            select row).FirstOrDefault();

                eventToUpdate.Title = eventData.Title;
                eventToUpdate.FileName = eventData.FileName;
                eventToUpdate.FilePath = eventData.FilePath;
                eventToUpdate.DocFileName = eventData.DocFileName;
                eventToUpdate.DocFilePath = eventData.DocFilePath;
                eventToUpdate.NewsDate = eventData.NewsDate;
                eventToUpdate.UpdatedDate = eventData.UpdatedDate;
                eventToUpdate.UpdatedBy = eventData.UpdatedBy;
                eventToUpdate.Description = eventData.Description;
                eventToUpdate.IsDeleted = false;
                entities.SaveChanges();
            }
        }
        public static NewsLetter GetByNewsLetterID(long eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.NewsLetters
                            where row.ID == eventID
                            select row).FirstOrDefault();
                return data;
            }
        }
        public static void DeleteNewsLetter(int newsLetterID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                NewsLetter newsLetterDelete = (from row in entities.NewsLetters
                                                 where row.ID == newsLetterID
                                                 select row).FirstOrDefault();

                newsLetterDelete.IsDeleted = true;
                entities.SaveChanges();
            }
        }

        public static NewsLetter GetNewsletter(int NewsLetterID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.NewsLetters
                                  where row.ID == NewsLetterID && row.IsDeleted == false
                                  select row).SingleOrDefault(); ;

                return statusList;
            }
        }

        //Added by Amita as on 9-jan-2019
        public static List<State> GetStatesByActID(int actID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<State> stateList = new List<State>();
                if(actID != -1)
                {
                    stateList = (from row in entities.States
                                 where row.IsDeleted == false
                                 join acts in entities.Acts
                                 on row.ID equals acts.StateID
                                 where acts.StateID != null && acts.IsDeleted == false && acts.ID == actID
                                 select row).Distinct().ToList();
                }
                else
                {

                    stateList = (from row in entities.States
                                 where row.IsDeleted == false
                                 select row).Distinct().ToList();                   
                }               

                return stateList;
            }
        }      

        //Added by amita as on 9-jan-2019
        public static List<ComplianceCategory> GetComplianceCategoriesByStateID(int actID = -1, int stateID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceCategory> categoryList = (from row in entities.ComplianceCategories
                                                         select row).ToList();

                List<ActView> actList = (from row in entities.ActViews
                                         select row).ToList();

                if (actID != -1)
                    actList = actList.Where(entry => entry.ID == actID).ToList();

                if (stateID != -1)
                    actList = actList.Where(entry => entry.StateID == stateID).ToList();


                if (actList != null)
                {
                    List<int> categoryIDs = new List<int>();

                    categoryIDs = (from row in actList
                                   select (int)row.ComplianceCategoryId
                                   ).Distinct().ToList();

                    categoryList = categoryList.Where(entry => categoryIDs.Contains(entry.ID)).Distinct().ToList();
                }

                return categoryList;
            }
        }

        //Added by amita as on 11-jan-2019
        public static List<ComplianceCategory> GetAllComplianceCategories()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceCategory> categoryList = (from row in entities.ComplianceCategories
                                                         join dailyupdates in entities.DailyUpdateViews
                                                         on row.ID equals dailyupdates.ComplianceCategoryId 
                                                         orderby row.ID                                                   
                                                         select row).Distinct().ToList();

                return categoryList;
            }
        }

        //Added By Amita as on 11 jan 2019
        public static List<State> GetAllStates(int categoryID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<State> stateList = new List<State>();                

                if (categoryID != -1)
                {
                    stateList = (from row in entities.States
                                 join dailyupdate in entities.DailyUpdateViews
                                 on row.ID equals dailyupdate.StateID                                 
                                 where row.IsDeleted == false   
                                 && dailyupdate.ComplianceCategoryId == categoryID 
                                 orderby row.ID                                                         
                                 select row).Distinct().ToList();                   
                }
                else
                {
                    stateList = (from row in entities.States
                                 join dailyupdate in entities.DailyUpdateViews
                                 on row.ID equals dailyupdate.StateID
                                 where row.IsDeleted == false    
                                 orderby row.ID                             
                                 select row).Distinct().ToList();
                }                

                return stateList;
            }
        }

        public static List<ActView> GetAllActs(int categoryID = -1,int stateID =-1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ActView> actList = new List<ActView>();

                actList = (from row in entities.ActViews
                           join dailyupdate in entities.DailyUpdateViews
                           on row.ID equals dailyupdate.ActID
                           orderby row.ID
                           select row).Distinct().ToList();

                if (categoryID != -1)
                    actList = actList.Where(entry => entry.ComplianceCategoryId == categoryID).Distinct().ToList();

                if (stateID != -1)
                {
                    //Compliance Type = Central 
                    if (stateID == -2)
                    {
                        actList = actList.Where(entry => entry.ComplianceTypeName.Trim().ToUpper() == "CENTRAL").Distinct().ToList();
                    }
                    else
                    {
                        actList = actList.Where(entry => entry.StateID == stateID && entry.ComplianceTypeName.Trim().ToUpper() == "STATE").Distinct().ToList();
                    }
                }

                actList.OrderBy(entry => entry.Name).ToList();

                return actList;
            }
        }

        //Added By Amita as on 11 jan 2019
        public static List<DailyUpdate> GetAllDailyUpdateList(int categoryID = -1,int stateID = -1 ,int actID =-1, string filterText = "")
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                IQueryable<DailyUpdateView> dailyUpdateList = (from row in entities.DailyUpdateViews
                                                               orderby row.ID
                                                               select row);

                if (categoryID != -1)
                    dailyUpdateList = dailyUpdateList.Where(entry => entry.ComplianceCategoryId == categoryID);

                if (stateID != -1)
                {
                    //Compliance Type = Central 
                    if (stateID == -2)
                    {
                        dailyUpdateList = (from row in dailyUpdateList
                                           where row.ComplianceTypeName.Trim().ToUpper().Equals("CENTRAL")
                                           select row);
                    }
                    else
                    {
                        dailyUpdateList = dailyUpdateList.Where(entry => entry.StateID == stateID && entry.ComplianceTypeName.Trim().ToUpper().Equals("STATE"));
                    }
                }

                if (actID != -1)
                    dailyUpdateList = dailyUpdateList.Where(entry => entry.ActID == actID);

                if (!string.IsNullOrEmpty(filterText))
                    dailyUpdateList = dailyUpdateList.Where(entry => entry.Title.Trim().ToUpper().Contains(filterText));

                var dailyUpdateFinalList = (from row in entities.DailyUpdates
                                            join view in dailyUpdateList
                                            on row.ID equals view.ID                                           
                                            select row).Distinct().OrderBy(entry => entry.ID).ToList();

                return dailyUpdateFinalList;
            }
        }
    }
}
