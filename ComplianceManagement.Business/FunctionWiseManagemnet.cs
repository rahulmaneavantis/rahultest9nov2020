﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class FunctionWiseManagemnet
    {
        #region Statutory                
        #region Without Branch
        
        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionPIEGetManagementDetailView(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                }

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {                   
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();                   
                }
                else if (!FromDate.ToString().Contains("1900"))
                {                   
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();                 
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {                       
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();                                       
                    }                   
                }
                else
                {
                    if (pointername == "Not completed")
                    {                        
                        detailView = detailView.Where(entry =>entry.ScheduledOn <= EndDate).ToList();                                        
                    }                    
                }            
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }

        

        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionBARGetManagementDetailView(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {                   
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();                   
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();                    
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry =>entry.ScheduledOn <= EndDateF).ToList();                      
                    }                    
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();                       
                    }                   
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.ComplianceCategoryId == CategoryID && entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        #endregion

        #region  With Branch        
        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionBARGetManagementDetailView(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }               
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();                                   
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();                  
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();                       
                    }                   
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry =>  entry.ScheduledOn <= EndDate).ToList();                        
                    }                   
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.ComplianceCategoryId == CategoryID && entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }        
        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionPIEGetManagementDetailView(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "MGMT")).ToList();
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {                    
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();                   
                }
                else if (!FromDate.ToString().Contains("1900"))
                {                  
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();                   
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {                        
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();                        
                    }                   
                }
                else
                {
                    if (pointername == "Not completed")
                    {                        
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();                        
                    }                  
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }

        #endregion

        #endregion

        #region Internal             
        public static List<InternalComplianceDashboardSummaryView> FunctionBARInternalGetManagementDetailView(int customerid, List<long> branchlist, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            List<InternalComplianceDashboardSummaryView> detailView = new List<InternalComplianceDashboardSummaryView>();
            DateTime EndDate = DateTime.Today.Date;
            if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
            {
                if (approver == true)
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                             .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                             && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDateF)
                             .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                           .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                           && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDateF)
                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
            }
            else if (!FromDate.ToString().Contains("1900"))
            {
                if (approver == true)
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                            .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                            && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDate)
                            .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                                                .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                                                && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDate)
                                                .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
            }
            else if (!EndDateF.ToString().Contains("1900"))
            {
                if (pointername == "Not completed")
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                            .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                            && entry.InternalScheduledOn <= EndDateF)
                            .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                          .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                          && entry.InternalScheduledOn <= EndDateF)
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                           .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID))
                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                          .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID))
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else
            {
                if (pointername == "Not completed")
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                            .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                            && entry.InternalScheduledOn <= EndDate)
                            .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                                                  .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                                                  && entry.InternalScheduledOn <= EndDate)
                                                  .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                           .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID))
                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                          .Where(entry => entry.CustomerID == customerid && statusIDs.Contains((int)entry.InternalComplianceStatusID))
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            if (branchlist.Count > 0)
            {
                detailView = detailView.Where(entry => branchlist.Contains((long)entry.CustomerBranchID)).ToList();
            }

            if (filter.Equals("Function"))
            {
                if (CategoryID != -1)
                {
                    detailView = detailView.Where(entry => entry.IComplianceCategoryID == CategoryID && entry.Risk == Risk).ToList();
                }
            }
            return detailView;
        }
        public static List<SP_GetCannedReportInternalCompliancesSummary_Result> FunctionBARInternalGetManagementDetailViewNew(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportInternalCompliancesSummary_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMT")).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.ComplianceCategoryId == CategoryID && entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        

        public static List<InternalComplianceDashboardSummaryView> FunctionBARInternalGetManagementDetailView(int customerid, List<long> branchlist, int barnchId, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {

            List<InternalComplianceDashboardSummaryView> detailView = new List<InternalComplianceDashboardSummaryView>();
            DateTime EndDate = DateTime.Today.Date;

            if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
            {
                if (approver == true)
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                           .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID)
                           //&& entry.CustomerBranchID == barnchId
                           && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                           && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                           && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDateF)
                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                                               .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                                               && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                                               && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                                               && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDateF)
                                               .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
            }
            else if (!FromDate.ToString().Contains("1900"))
            {
                if (approver == true)
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                          .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                          && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                          && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                          && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDate)
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                          .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                          && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                          && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                          && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDate)
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
            }
            else if (!EndDateF.ToString().Contains("1900"))
            {
                if (pointername == "Not completed")
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                          .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                          && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                           && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                          && entry.InternalScheduledOn <= EndDateF)
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                         .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                         && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                         && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                         && entry.InternalScheduledOn <= EndDateF)
                         .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                        .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                        && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false))
                        .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                        .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                        && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                         && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false))
                        .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                }
            }
            else
            {
                if (pointername == "Not completed")
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                          .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                          && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                          && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                          && entry.InternalScheduledOn <= EndDate)
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                          .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                          && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                          && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                          && entry.InternalScheduledOn <= EndDate)
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                }
                else
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                        .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                        && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false))
                        .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                          .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                          && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                          && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                          && entry.InternalScheduledOn <= EndDate)
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                }
            }
            if (filter.Equals("Function"))
            {
                if (CategoryID != -1)
                {
                    detailView = detailView.Where(entry => entry.IComplianceCategoryID == CategoryID && entry.Risk == Risk).ToList();
                }
            }
            return detailView;
        }
        public static List<SP_GetCannedReportInternalCompliancesSummary_Result> FunctionBARInternalGetManagementDetailViewNew(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportInternalCompliancesSummary_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMT")).ToList();
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.ComplianceCategoryId == CategoryID && entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }

        
        public static List<InternalComplianceDashboardSummaryView> FunctionPIEInternalGetManagementDetailView(int customerid, List<long> branchlist, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {

            List<InternalComplianceDashboardSummaryView> detailView = new List<InternalComplianceDashboardSummaryView>();
            DateTime EndDate = DateTime.Today.Date;
            if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
            {
                if (approver == true)
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                             .Where(entry => entry.CustomerID == customerid
                             && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                             && CategoryID.Contains(entry.IComplianceCategoryID)
                             && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDateF)
                             .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                                                 .Where(entry => entry.CustomerID == customerid
                                                 && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                                                 && CategoryID.Contains(entry.IComplianceCategoryID)
                                                 && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDateF)
                                                 .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }


            }
            else if (!FromDate.ToString().Contains("1900"))
            {
                if (approver == true)
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                              .Where(entry => entry.CustomerID == customerid
                              && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                               && CategoryID.Contains(entry.IComplianceCategoryID)
                              && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDate)
                              .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                             .Where(entry => entry.CustomerID == customerid
                             && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                              && CategoryID.Contains(entry.IComplianceCategoryID)
                             && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDate)
                             .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
            }
            else if (!EndDateF.ToString().Contains("1900"))
            {
                if (pointername == "Not completed")
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                              .Where(entry => entry.CustomerID == customerid
                              && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                               && CategoryID.Contains(entry.IComplianceCategoryID)
                              && entry.InternalScheduledOn <= EndDateF)
                              .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                              .Where(entry => entry.CustomerID == customerid
                              && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                               && CategoryID.Contains(entry.IComplianceCategoryID)
                              && entry.InternalScheduledOn <= EndDateF)
                              .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                             .Where(entry => entry.CustomerID == customerid
                             && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                             && CategoryID.Contains(entry.IComplianceCategoryID))
                             .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                             .Where(entry => entry.CustomerID == customerid
                             && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                             && CategoryID.Contains(entry.IComplianceCategoryID))
                             .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                }
            }
            else
            {
                if (pointername == "Not completed")
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                             .Where(entry => entry.CustomerID == customerid
                             && statusIDs.Contains((int)entry.InternalComplianceStatusID) && CategoryID.Contains(entry.IComplianceCategoryID)
                             && entry.InternalScheduledOn <= EndDate)
                             .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                                                     .Where(entry => entry.CustomerID == customerid
                                                     && statusIDs.Contains((int)entry.InternalComplianceStatusID) && CategoryID.Contains(entry.IComplianceCategoryID)
                                                     && entry.InternalScheduledOn <= EndDate)
                                                     .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                }
                else
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                              .Where(entry => entry.CustomerID == customerid
                              && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                              && CategoryID.Contains(entry.IComplianceCategoryID))
                              .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                              .Where(entry => entry.CustomerID == customerid
                              && statusIDs.Contains((int)entry.InternalComplianceStatusID) && CategoryID.Contains(entry.IComplianceCategoryID))
                              .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                }
            }
            if (branchlist.Count > 0)
            {
                detailView = detailView.Where(entry => branchlist.Contains((long)entry.CustomerBranchID)).ToList();
            }
            if (filter.Equals("Function"))
            {
                if (Risk != -1)
                {
                    detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                }
            }
            return detailView;
        }
        public static List<SP_GetCannedReportInternalCompliancesSummary_Result> FunctionPIEInternalGetManagementDetailViewNew(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportInternalCompliancesSummary_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {

                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMT")).ToList();
                }

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }

        public static List<InternalComplianceDashboardSummaryView> FunctionPIEInternalGetManagementDetailView(int customerid, List<long> branchlist, int barnchId, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            List<InternalComplianceDashboardSummaryView> detailView = new List<InternalComplianceDashboardSummaryView>();
            DateTime EndDate = DateTime.Today.Date;
            if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
            {
                if (approver == true)
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                    .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                    && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                    && CategoryID.Contains(entry.IComplianceCategoryID)
                    && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDateF)
                    .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                     .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                     && statusIDs.Contains((int)entry.InternalComplianceStatusID) && CategoryID.Contains(entry.IComplianceCategoryID)
                     && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDateF)
                     .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

            }
            else if (!FromDate.ToString().Contains("1900"))
            {
                if (approver == true)
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                        .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                        && statusIDs.Contains((int)entry.InternalComplianceStatusID) && CategoryID.Contains(entry.IComplianceCategoryID)
                        && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDate)
                        .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                          .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                          && statusIDs.Contains((int)entry.InternalComplianceStatusID) && CategoryID.Contains(entry.IComplianceCategoryID)
                          && entry.InternalScheduledOn >= FromDate && entry.InternalScheduledOn <= EndDate)
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

            }
            else if (!EndDateF.ToString().Contains("1900"))
            {
                if (pointername == "Not completed")
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                         .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                         && statusIDs.Contains((int)entry.InternalComplianceStatusID) && CategoryID.Contains(entry.IComplianceCategoryID)
                         && entry.InternalScheduledOn <= EndDateF)
                         .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                          .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                          && statusIDs.Contains((int)entry.InternalComplianceStatusID) && CategoryID.Contains(entry.IComplianceCategoryID)
                          && entry.InternalScheduledOn <= EndDateF)
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                     .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                     && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                     && CategoryID.Contains(entry.IComplianceCategoryID))
                     .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                      .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                      && statusIDs.Contains((int)entry.InternalComplianceStatusID) && CategoryID.Contains(entry.IComplianceCategoryID))
                      .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else
            {

                if (pointername == "Not completed")
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                          .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                          && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                          && CategoryID.Contains(entry.IComplianceCategoryID)
                          && entry.InternalScheduledOn <= EndDate)
                          .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                           .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                           && statusIDs.Contains((int)entry.InternalComplianceStatusID) && CategoryID.Contains(entry.IComplianceCategoryID)
                           && entry.InternalScheduledOn <= EndDate)
                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (approver == true)
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, true)
                        .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                        && statusIDs.Contains((int)entry.InternalComplianceStatusID)
                        && CategoryID.Contains(entry.IComplianceCategoryID))
                        .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal(customerid, userId, false)
                        .Where(entry => entry.CustomerID == customerid && branchlist.Contains((long)entry.CustomerBranchID) //&& entry.CustomerBranchID == barnchId
                        && statusIDs.Contains((int)entry.InternalComplianceStatusID) && CategoryID.Contains(entry.IComplianceCategoryID))
                        .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            if (filter.Equals("Function"))
            {
                if (Risk != -1)
                {
                    detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                }
            }
            return detailView;
        }
        public static List<SP_GetCannedReportInternalCompliancesSummary_Result> FunctionPIEInternalGetManagementDetailViewNew(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportInternalCompliancesSummary_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary(userId, customerid, "MGMT")).ToList();
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }

        #endregion
        public static List<InternalCompliancesCategory> GetFunctionDetailsInternal(int Userid, List<long> branchlist, int CustomerBranchId, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalCompliancesCategory> complianceCategorys = new List<InternalCompliancesCategory>();
                if (approver == true)
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               //&& row1.CustomerBranchID == CustomerBranchId
                                               && branchlist.Contains((long)row1.CustomerBranchID)
                                               && row1.RoleID == 6
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               //&& branchlist.Contains((long)row1.CustomerBranchID)
                                               && row1.RoleID == 6
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    List<InternalComplianceAssignedInstancesView> InternalComplianceCount = new List<InternalComplianceAssignedInstancesView>();
                    if (branchlist.Count > 0)
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where branchlist.Contains((long)row.CustomerBranchID) //&& row.CustomerBranchID == row1.BranchID
                                                && row1.UserID == Userid && row1.BranchID == CustomerBranchId
                                                   select row).Distinct().ToList();
                    }
                    else
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   // && branchlist.Contains((long)row.CustomerBranchID)
                                                   && row1.UserID == Userid
                                                   select row).Distinct().ToList();
                    }
                    InternalComplianceCount = InternalComplianceCount.GroupBy(a => (int?)a.InternalComplianceCategoryID).Select(a => a.FirstOrDefault()).ToList();
                    complianceCategorys = (from row in InternalComplianceCount
                                           join row1 in entities.InternalCompliancesCategories
                                           on row.InternalComplianceCategoryID equals row1.ID
                                           select row1).ToList();
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<sp_ComplianceAssignedCategory_Result> GetFunctionDetailsStatutory(int UserID, List<long> branchlist, int CustomerBranchId, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedCategory_Result> complianceCategorys = new List<sp_ComplianceAssignedCategory_Result>();
                if (approver == true)
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "APPR")
                                               where branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    
                        //complianceCategorys = (from row in entities.ComplianceCategories
                        //                       join row1 in entities.ComplianceAssignedInstancesViews
                        //                       on row.ID equals row1.ComplianceCategoryId
                        //                       where row1.UserID == Userid && branchlist.Contains((long)row1.CustomerBranchID)
                        //                       && row1.RoleID == 6
                        //                       select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "APPR")                                                                                    
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                        //complianceCategorys = (from row in entities.ComplianceCategories
                        //                       join row1 in entities.ComplianceAssignedInstancesViews
                        //                       on row.ID equals row1.ComplianceCategoryId
                        //                       where row1.UserID == Userid && row1.RoleID == 6                                               
                        //                       select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    //List<ComplianceAssignedInstancesView> ComplianceCount = new List<ComplianceAssignedInstancesView>();
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "MGMT")
                                               where branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();


                    
                     
                        //ComplianceCount = (from row in entities.ComplianceAssignedInstancesViews
                        //                   join row1 in entities.EntitiesAssignments
                        //                    on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                        //                   where row.CustomerBranchID == row1.BranchID
                        //                   && row1.UserID == Userid && branchlist.Contains((long)row1.BranchID) 
                        //                   select row).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "MGMT")                                             
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();


                        //ComplianceCount = (from row in entities.ComplianceAssignedInstancesViews
                        //                   join row1 in entities.EntitiesAssignments
                        //                    on (long)row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                        //                   where row.CustomerBranchID == row1.BranchID
                        //                   && row1.UserID == Userid 
                        //                   select row).ToList();
                    }

                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

                    //ComplianceCount = ComplianceCount.GroupBy(a => (int?)a.ComplianceCategoryId).Select(a => a.FirstOrDefault()).ToList();
                    //complianceCategorys = (from row in ComplianceCount
                    //                       join row1 in entities.ComplianceCategories
                    //                       on row.ComplianceCategoryId equals row1.ID
                    //                       select row1).ToList();
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        
    }
}
