﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class AvacomTokenDetails
    {
        public string UserEmail { get; set; }
        public string tokendetail { get; set; }
    }
    public class CommanClass
    {
        
        public static bool Exists(string productname,string customername)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from p in entities.Products
                             join pm in entities.ProductMappings
                             on p.Id equals pm.ProductID
                             join c in entities.Customers
                             on pm.CustomerID equals c.ID
                             where pm.IsActive == false
                             && c.Name.Equals(customername)
                             && p.Name.Equals(productname)
                             select pm);

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static List<ProductMappingDetails> GetAllProductMapping(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {               
                var productmapping = (from p in entities.Products
                                      join pm in entities.ProductMappings
                                      on p.Id equals pm.ProductID
                                      join c in entities.Customers
                                      on pm.CustomerID equals c.ID
                                      where pm.IsActive == false
                                      select new ProductMappingDetails()
                    {
                        ProductID = p.Id,
                        CustomerId = c.ID,
                        ProductName = p.Name,
                        CustomerName = c.Name
                    });

                if (customerID != -1)
                {
                    productmapping = productmapping.Where(entry => entry.CustomerId == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    productmapping = productmapping.Where(entry => entry.ProductName.Contains(filter) || entry.CustomerName.Contains(filter));
                }
                return productmapping.OrderBy(entry => entry.ProductName).ToList();
            }
        }

        public static object FillProduct()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Products
                             select row);

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
       

        public static ProductMapping GetByProductMappingID(int customerId, long ProductId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var mappings = (from row in entities.ProductMappings
                            where row.CustomerID == customerId
                            && row.ProductID == ProductId
                             && row.IsActive == false
                            select row).SingleOrDefault();

                return mappings;
            }
        }
        
        public static List<long> GetAllCompanyAdminUsers(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> users = new List<long>();
                users = (from row in entities.Users
                         where row.IsDeleted == false && row.CustomerID == customerID
                         && row.RoleID==2
                         select row.ID).ToList();

                if (users != null)
                {
                    return users;
                }
                else
                {
                    return users;
                }
            }
        }
        public static void UpdateUserRole(List<long> cadminuserlist, string Flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<User> userToUpdate = (from Row in entities.Users
                                           where cadminuserlist.Contains(Row.ID)
                                           select Row).ToList();

                if (Flag =="LIT")
                {
                    userToUpdate.ForEach(entry =>
                    {
                        entry.LitigationRoleID = 2;
                    });
                    entities.SaveChanges();
                }
                if (Flag == "LIC")
                {
                    userToUpdate.ForEach(entry =>
                    {
                        entry.LicenseRoleID = 2;
                    });
                    entities.SaveChanges();
                }
                if (Flag == "CONT")
                {
                    userToUpdate.ForEach(entry =>
                    {
                        entry.ContractRoleID = 2;
                    });
                    entities.SaveChanges();
                }
            }
        }
        public static void UpdateUserRoleRisk(List<long> cadminuserlist, string Flag)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
            {
                List< com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User> userToUpdate = (from Row in entities.mst_User
                                           where cadminuserlist.Contains(Row.ID)
                                           select Row).ToList();

                if (Flag == "LIT")
                {
                    userToUpdate.ForEach(entry =>
                    {
                        entry.LitigationRoleID = 2;
                    });
                    entities.SaveChanges();
                }
                if (Flag == "LIC")
                {
                    userToUpdate.ForEach(entry =>
                    {
                        entry.LicenseRoleID = 2;
                    });
                    entities.SaveChanges();
                }
                if (Flag == "CONT")
                {
                    userToUpdate.ForEach(entry =>
                    {
                        entry.ContractRoleID = 2;
                    });
                    entities.SaveChanges();
                }
            }
        }
        public static bool Create(com.VirtuosoITech.ComplianceManagement.Business.DataRisk.ProductMapping_Risk prodcutmappingrisk)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {

                        prodcutmappingrisk.CustomerID = prodcutmappingrisk.CustomerID;
                        prodcutmappingrisk.ProductID = prodcutmappingrisk.ProductID;
                        prodcutmappingrisk.IsActive = prodcutmappingrisk.IsActive;
                        prodcutmappingrisk.CreatedOn = prodcutmappingrisk.CreatedOn;
                        prodcutmappingrisk.CreatedBy = prodcutmappingrisk.CreatedBy;
                        entities.ProductMapping_Risk.Add(prodcutmappingrisk);
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }

        public static bool Create(ProductMapping prodcutmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {

                        prodcutmapping.CustomerID = prodcutmapping.CustomerID;
                        prodcutmapping.ProductID = prodcutmapping.ProductID;
                        prodcutmapping.IsActive = prodcutmapping.IsActive;
                        prodcutmapping.CreatedOn = prodcutmapping.CreatedOn;
                        prodcutmapping.CreatedBy = prodcutmapping.CreatedBy;
                        entities.ProductMappings.Add(prodcutmapping);
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }
        public static bool UpdateNewRisk(com.VirtuosoITech.ComplianceManagement.Business.DataRisk.ProductMapping_Risk prodcutmappingrisk)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
            {
                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.ProductMapping_Risk ProductMappingToUpdate
                    = (from row in entities.ProductMapping_Risk
                       where row.CustomerID == prodcutmappingrisk.CustomerID
                       && row.ProductID == prodcutmappingrisk.ProductID                       
                       select row).FirstOrDefault();


                if (ProductMappingToUpdate != null)
                {
                    ProductMappingToUpdate.CustomerID = prodcutmappingrisk.CustomerID;
                    ProductMappingToUpdate.ProductID = prodcutmappingrisk.ProductID;
                    ProductMappingToUpdate.IsActive = false;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }              
            }
        }
        public static bool UpdateNew(ProductMapping prodcutmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ProductMappingToUpdate = (from row in entities.ProductMappings
                                                         where row.CustomerID == prodcutmapping.CustomerID                                                          
                                                          && row.ProductID==prodcutmapping.ProductID
                                                         select row).FirstOrDefault();

                if (ProductMappingToUpdate !=null)
                {
                    ProductMappingToUpdate.CustomerID = prodcutmapping.CustomerID;
                    ProductMappingToUpdate.ProductID = prodcutmapping.ProductID;
                    ProductMappingToUpdate.IsActive = false;
                    entities.SaveChanges();
                    return true;
                }     
                else
                {
                    return false;
                }                         
            }
        }
        public static bool Update(ProductMapping prodcutmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ProductMapping ProductMappingToUpdate = (from row in entities.ProductMappings
                                               where row.CustomerID == prodcutmapping.CustomerID
                                               //&& row.ProductID==prodcutmapping.ProductID
                                                && row.IsActive == false
                                     select row).FirstOrDefault();


                ProductMappingToUpdate.CustomerID = prodcutmapping.CustomerID;
                ProductMappingToUpdate.ProductID = prodcutmapping.ProductID;                             
                entities.SaveChanges();
                return true;
            }
        }
        public static bool Update(com.VirtuosoITech.ComplianceManagement.Business.DataRisk.ProductMapping_Risk prodcutmappingrisk)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
            {
                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.ProductMapping_Risk ProductMappingToUpdate 
                    = (from row in entities.ProductMapping_Risk
                        where row.CustomerID == prodcutmappingrisk.CustomerID
                        //&& row.ProductID == prodcutmappingrisk.ProductID
                        && row.IsActive==false
                        select row).FirstOrDefault();
                ProductMappingToUpdate.CustomerID = prodcutmappingrisk.CustomerID;
                ProductMappingToUpdate.ProductID = prodcutmappingrisk.ProductID;
                entities.SaveChanges();
                return true;
            }
        }
        public static void Delete(int CustomerID, long ProductID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ProductMapping ProductMappingToDelete = (from row in entities.ProductMappings
                                     where row.CustomerID == CustomerID
                                     && row.ProductID == ProductID && row.IsActive==false
                                    
                                     select row).FirstOrDefault();

                ProductMappingToDelete.IsActive = true;

                entities.SaveChanges();
            }
        }
        public static void ProductMappingRiskDelete(int CustomerID, long ProductID)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.AuditControlEntities())
            {
                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.ProductMapping_Risk ProductMappingToDelete = (from row in entities.ProductMapping_Risk
                                                         where row.CustomerID == CustomerID
                                                         && row.ProductID == ProductID && row.IsActive == false
                                                         select row).FirstOrDefault();

                ProductMappingToDelete.IsActive = true;

                entities.SaveChanges();
            }
        }
    }
}
