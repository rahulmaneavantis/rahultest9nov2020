//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    using System;
    
    public partial class Sp_GetInternalAuditResultByID_Result
    {
        public int ID { get; set; }
        public long AuditScheduleOnID { get; set; }
        public long ProcessId { get; set; }
        public string FinancialYear { get; set; }
        public string ForPerid { get; set; }
        public long CustomerBranchId { get; set; }
        public bool IsDeleted { get; set; }
        public string AuditObjective { get; set; }
        public string AnalysisToBePerofrmed { get; set; }
        public string ProcessWalkthrough { get; set; }
        public string ActivityToBeDone { get; set; }
        public string Population { get; set; }
        public string Sample { get; set; }
        public string ObservationNumber { get; set; }
        public string ObservationTitle { get; set; }
        public string Observation { get; set; }
        public string Risk { get; set; }
        public string RootCost { get; set; }
        public string FinancialImpact { get; set; }
        public string Recomendation { get; set; }
        public string ManagementResponse { get; set; }
        public Nullable<System.DateTime> TimeLine { get; set; }
        public Nullable<long> PersonResponsible { get; set; }
        public Nullable<long> ObservationRating { get; set; }
        public Nullable<long> ObservationCategory { get; set; }
        public Nullable<long> InternalAuditInstance { get; set; }
        public long ATBDId { get; set; }
        public long UserID { get; set; }
        public long RoleID { get; set; }
        public string FixRemark { get; set; }
        public Nullable<int> AStatusId { get; set; }
        public Nullable<long> TransactionId { get; set; }
        public Nullable<long> ObservationSubCategory { get; set; }
        public Nullable<int> VerticalID { get; set; }
        public Nullable<decimal> AuditScores { get; set; }
        public Nullable<int> ISACPORMIS { get; set; }
        public Nullable<int> Owner { get; set; }
        public System.DateTime Dated { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<long> AuditID { get; set; }
        public Nullable<long> UHPersonResponsible { get; set; }
        public Nullable<long> PRESIDENTPersonResponsible { get; set; }
        public string UHComment { get; set; }
        public string PRESIDENTComment { get; set; }
        public string BodyContent { get; set; }
        public string BodySubContent { get; set; }
        public string AuditeeResponse { get; set; }
        public string BriefObservation { get; set; }
        public Nullable<int> DefeciencyType { get; set; }
        public string ObjBackground { get; set; }
        public string AnnexueTitle { get; set; }
        public string RootCauseOrImpact { get; set; }
        public string AuditSteps { get; set; }
        public Nullable<System.DateTime> ResponseDueDate { get; set; }
    }
}
