﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    public class AuditKickOffDetails
    {
        public long ProcessID { get; set; }
        public int CustomerBranchId { get; set; }
        public string ISAHQMP { get; set; }
        public string FinancialYear { get; set; }
        public Nullable<int> PhaseCount { get; set; }
        public string AssignedTo { get; set; }
        public Nullable<long> ExternalAuditorId { get; set; }
        public string TermName { get; set; }
        public string BranchName { get; set; }
        public Nullable<int> VerticalID { get; set; }
        public string VerticalName { get; set; }
        public string Period { get; set; }
    }
}
