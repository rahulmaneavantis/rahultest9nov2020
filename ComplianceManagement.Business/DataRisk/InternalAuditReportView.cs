//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class InternalAuditReportView
    {
        public Nullable<int> ResultID { get; set; }
        public long ATBDId { get; set; }
        public long CustomerbranchId { get; set; }
        public long processId { get; set; }
        public string SerialNumber { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCodeName { get; set; }
        public string Department { get; set; }
        public string locationname { get; set; }
        public string AuditPeriod { get; set; }
        public string FinancialYear { get; set; }
        public string FindingsInitiatives { get; set; }
        public string Observation { get; set; }
        public string RootCause { get; set; }
        public string Implication { get; set; }
        public string Recomendation { get; set; }
        public string fromPeriod { get; set; }
        public string toPeriod { get; set; }
        public string ProcessImprovement { get; set; }
        public Nullable<System.DateTime> status { get; set; }
        public string AgreedAction { get; set; }
        public string BusinessImplication { get; set; }
        public string PersonResponsible { get; set; }
        public Nullable<long> PersonResponsibleID { get; set; }
        public string CostImplications { get; set; }
        public string Currency { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> VerticalID { get; set; }
        public string jobNo { get; set; }
        public string JobName { get; set; }
        public string serialNoAsPerReport { get; set; }
        public string revisedTargetDate { get; set; }
        public string EarlierTargetDate { get; set; }
        public string Owner { get; set; }
        public string currentStatus { get; set; }
        public Nullable<long> AuditID { get; set; }
        public Nullable<int> ProcessOrder { get; set; }
        public string ObservationNumber { get; set; }
    }
}
