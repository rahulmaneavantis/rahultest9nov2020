﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    class RiskExportinfo
    {
    }
//private int _CustomerId;
//public int CustomerID
//{
//    get { return _CustomerID; }
//    set { _CustomerID = value; }
//}
    public class RiskExportinfoReport
    {
        
        public long ID { get; set; }
        public long RiskCreationId { get; set; }        
        public string ActivityDescription { get; set; }
        public string ControlObjective { get; set; }
        public long RiskCategory { get; set; }
        public long Industry { get; set; }
        public Nullable<int> Client { get; set; }
        public Nullable<int> LocationType { get; set; }
        public string Assertions { get; set; }
        public string ControlDescription { get; set; }
        public string MControlDescription { get; set; }
        public long PersonResponsible { get; set; }
        public Nullable<DateTime> EffectiveDate { get; set; }
        public long Key { get; set; }
        public long PreventiveControl { get; set; }
        public long AutomatedControl { get; set; }
        public long Frequency { get; set; }
        public Nullable<long> ProcessId { get; set; }
        public Nullable<long> SubProcessId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public string GapDescription { get; set; }
        public string Recommendations { get; set; }
        public string ActionRemediationplan { get; set; }
        public string IPE { get; set; }
        public string ERPsystem { get; set; }
        public string FRC { get; set; }
        public string UniqueReferred { get; set; }
        public string TestStrategy { get; set; }
        public string DocumentsExamined { get; set; }
        public string ActivityTobeDone { get; set; }
        public string RiskRating { get; set; }
        public string IsInternalAudit { get; set; }
        public string ControlNo { get; set; }
        public string ProcessName { get; set; }
        public string SubProcessName { get; set; }
    }
}
