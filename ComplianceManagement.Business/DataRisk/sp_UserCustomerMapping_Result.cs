//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    using System;
    
    public partial class sp_UserCustomerMapping_Result
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public long UserId { get; set; }
        public string FullName { get; set; }
        public long Id { get; set; }
        public Nullable<long> UsId { get; set; }
        public Nullable<int> CustId { get; set; }
        public string RoleName { get; set; }
    }
}
