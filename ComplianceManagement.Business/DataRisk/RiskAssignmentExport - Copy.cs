﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    public class RiskAssignmentExport
    {
        public long ID { get; set; }
        public long RiskCategoryCreationId { get; set; }
        public string ActivityDescription { get; set; }
        public string ControlObjective { get; set; }
        public string ControlDescription { get; set; }
        public string MitigatingControl { get; set; }
    }
}
