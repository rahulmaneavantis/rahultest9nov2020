﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;


namespace com.VirtuosoITech.ComplianceManagement.Business
{
   public class EmailsMngt
    {
        public class EmailData
        {
            public int Id { get; set; }
            public string Email { get; set; }
            public string Recepient { get; set; }
            public string Subjecttext { get; set; }
            public string MessageBody { get; set; }
        }
        public static List<EmailData> GetAll()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Emails
                            join u in entities.Users
                            on row.Id equals u.ID
                            select new EmailData
                            {
                                Id = row.Id,
                                Email = u.Email,
                                Recepient = row.Recepient,
                                Subjecttext = row.Subjecttext,
                                MessageBody = row.MessageBody
                            }).ToList();
               return acts.ToList();

            }
        }
    }
}



