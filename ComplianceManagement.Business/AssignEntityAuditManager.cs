﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class AssignEntityAuditManager
    {

        #region Department Head
        public static bool EntitiesAssignmentDepartmentHeadExist(int customerid, long Branchid, long UserID, int departmentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.EntitiesAssignmentDepartmentHeads
                             where row.CustomerID == customerid && row.BranchID == Branchid
                             && row.UserID == UserID && row.DepartmentID == departmentID
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CreateEntitiesAssignmentDepartmentHeadlist(List<EntitiesAssignmentDepartmentHead> branchverticals)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    branchverticals.ForEach(entry =>
                    {
                        entities.EntitiesAssignmentDepartmentHeads.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static void EditEntitiesAssignmentDepartmentHeadExist(int customerid, long Branchid, long UserID, int departmentID, int updatedby)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RiskToUpdate = (from row in entities.EntitiesAssignmentDepartmentHeads
                                    where row.CustomerID == customerid && row.BranchID == Branchid
                                    && row.UserID == UserID && row.DepartmentID == departmentID
                                    select row).FirstOrDefault();
                RiskToUpdate.ISACTIVE = true;
                RiskToUpdate.UpdatedBy = updatedby;
                RiskToUpdate.UpdatedOn = DateTime.Today.Date;
                entities.SaveChanges();
            };
        }
        public static bool UpdateEntitiesAssignmentDepartmentHead(int customerid, List<long> Branchlist, long UserID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var ids = (from row in entities.EntitiesAssignmentDepartmentHeads
                               where row.CustomerID == customerid
                               && row.UserID == UserID
                               select row).ToList();
                    if (Branchlist.Count > 0)
                    {
                        ids = ids.Where(a => Branchlist.Contains(a.BranchID)).ToList();
                    }
                    ids.ForEach(entry =>
                    {
                        EntitiesAssignmentDepartmentHead AssignmentDepartmentHeads = (from row in entities.EntitiesAssignmentDepartmentHeads
                                                                                      where row.ID == entry.ID
                                                                                      select row).FirstOrDefault();
                        AssignmentDepartmentHeads.ISACTIVE = false;
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        public static bool UpdateEntitiesAssignmentAuditManager(int customerid, List<long> Branchlist, long UserID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    //get all Assigned Process all Locations
                    var AllAssignedProcessForAllLocationslist = (from row in entities.EntitiesAssignmentAuditManagerRisks
                                                                 where row.CustomerID == customerid
                                                                 && row.UserID == UserID && row.ISACTIVE == true
                                                                 select row).ToList();
                    if (AllAssignedProcessForAllLocationslist.Count > 0)
                    {
                        if (Branchlist.Count > 0)
                        {
                            AllAssignedProcessForAllLocationslist = AllAssignedProcessForAllLocationslist.Where(a => Branchlist.Contains(a.BranchID)).ToList();
                        }
                        //Get All Scheduled Process For All Loactions
                        var internalauditscheduling = (from row in entities.InternalAuditSchedulings
                                                       where Branchlist.Contains(row.CustomerBranchId)
                                                       && row.IsDeleted == false
                                                       select row.Process).ToList();
                        if (internalauditscheduling.Count > 0)
                        {
                            //if Count Exists then remove this process from assigned list
                            AllAssignedProcessForAllLocationslist = AllAssignedProcessForAllLocationslist.Where(a => !internalauditscheduling.Contains(a.ProcessId)).ToList();
                            AllAssignedProcessForAllLocationslist.ForEach(entry =>
                            {

                                var EAAMR = (from row in entities.EntitiesAssignmentAuditManagerRisks
                                             where row.ID == entry.ID
                                             select row).FirstOrDefault();
                                EAAMR.ISACTIVE = false;
                            });
                        }
                        else
                        {
                            AllAssignedProcessForAllLocationslist.ForEach(entry =>
                            {
                                var EAAMR = (from row in entities.EntitiesAssignmentAuditManagerRisks
                                             where row.ID == entry.ID
                                             select row).FirstOrDefault();
                                EAAMR.ISACTIVE = false;
                            });
                        }
                        entities.SaveChanges();
                    }
                    return true;

                    #region Previous Code
                    //var ids = (from row in entities.EntitiesAssignmentAuditManagerRisks
                    //           where row.CustomerID == customerid
                    //           && row.UserID == UserID
                    //           select row).ToList();
                    //if (Branchlist.Count > 0)
                    //{
                    //    ids = ids.Where(a => Branchlist.Contains(a.BranchID)).ToList();
                    //}
                    //ids.ForEach(entry =>
                    //{

                    //    EntitiesAssignmentAuditManagerRisk EAAMR = (from row in entities.EntitiesAssignmentAuditManagerRisks
                    //                                                where row.ID == entry.ID
                    //                                                select row).FirstOrDefault();
                    //    EAAMR.ISACTIVE = false;
                    //});
                    //entities.SaveChanges();
                    //return true;
                    #endregion
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool UpdateEntitiesAssignmentManagementRisk(int customerid, List<long> Branchlist, long UserID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var ids = (from row in entities.EntitiesAssignmentManagementRisks
                               where row.CustomerID == customerid
                               && row.UserID == UserID
                               select row).ToList();
                    if (Branchlist.Count > 0)
                    {
                        ids = ids.Where(a => Branchlist.Contains(a.BranchID)).ToList();
                    }
                    ids.ForEach(entry =>
                    {
                        EntitiesAssignmentManagementRisk AssignmentManagements = (from row in entities.EntitiesAssignmentManagementRisks
                                                                                  where row.ID == entry.ID
                                                                                  select row).FirstOrDefault();
                        AssignmentManagements.ISACTIVE = false;
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static EntitiesAssignmentAuditManagerRisk SelectEntity(int branchId = -1, int userID = -1)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var EntitiesAssignmentData = (from row in entities.EntitiesAssignmentAuditManagerRisks
                                              where row.BranchID == branchId && row.UserID == userID
                                              && row.ISACTIVE == true
                                              select row).FirstOrDefault();
                return EntitiesAssignmentData;
            }
        }
        public static List<EntitiesAssignmentDepartmentHead> GetAllEntitiesAssignmentDepartmentHeadList(long custID, List<long> Branchid, long UserId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.EntitiesAssignmentDepartmentHeads
                               where row.UserID == UserId && (row.CustomerID == custID) && Branchid.Contains(row.BranchID)
                               && row.ISACTIVE == true
                               select row);
                return objcust.ToList();

            }
        }
        public static List<EntitiesAssignmentAuditManagerRisk> GetAllEntitiesAssignmentAuditManagerList(long custID, List<long> Branchid, long UserId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.EntitiesAssignmentAuditManagerRisks
                               where row.UserID == UserId && (row.CustomerID == custID)
                               && Branchid.Contains(row.BranchID)
                               && row.ISACTIVE == true
                               select row);
                return objcust.ToList();

            }
        }
        public static List<EntitiesAssignmentManagementRisk> GetAllEntitiesAssignmentManagementList(long custID, List<long> Branchid, long UserId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.EntitiesAssignmentManagementRisks
                               where row.UserID == UserId && (row.CustomerID == custID) && Branchid.Contains(row.BranchID)
                               && row.ISACTIVE == true
                               select row);
                return objcust.ToList();

            }
        }
        public static bool CreateEntitiesAssignmentAuditManagerRisklist(List<EntitiesAssignmentAuditManagerRisk> branchverticals)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    branchverticals.ForEach(entry =>
                    {
                        entities.EntitiesAssignmentAuditManagerRisks.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool CreateEntitiesAssignmentAuditManagerRisklist(EntitiesAssignmentAuditManagerRisk branchverticals)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.EntitiesAssignmentAuditManagerRisks.Add(branchverticals);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool CreateEntitiesAssignmentManagementRisklist(List<EntitiesAssignmentManagementRisk> branchverticals)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    branchverticals.ForEach(entry =>
                    {
                        entities.EntitiesAssignmentManagementRisks.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static void EditEntitiesAssignmentManagementRiskExist(int customerid, long Branchid, long UserID, int Processid, int updatedby)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RiskToUpdate = (from row in entities.EntitiesAssignmentManagementRisks
                                    where row.CustomerID == customerid && row.BranchID == Branchid
                                    && row.UserID == UserID && row.ProcessId == Processid
                                    select row).FirstOrDefault();
                RiskToUpdate.ISACTIVE = true;
                RiskToUpdate.UpdatedOn = DateTime.Today.Date;
                RiskToUpdate.UpdatedBy = updatedby;
                entities.SaveChanges();
            };
        }
        public static void EditEntitiesAssignmentAuditManagerRiskExist(int customerid, long Branchid, long UserID, int Processid, int updatedby)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RiskToUpdate = (from row in entities.EntitiesAssignmentAuditManagerRisks
                                    where row.CustomerID == customerid && row.BranchID == Branchid
                                    && row.UserID == UserID && row.ProcessId == Processid
                                    select row).FirstOrDefault();
                RiskToUpdate.ISACTIVE = true;
                RiskToUpdate.UpdatedOn = DateTime.Today.Date;
                RiskToUpdate.UpdatedBy = updatedby;
                entities.SaveChanges();
            };
        }
        public static bool EntitiesAssignmentManagementRiskExist(int customerid, long Branchid, long UserID, int Processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.EntitiesAssignmentManagementRisks
                             where row.CustomerID == customerid && row.BranchID == Branchid
                             && row.UserID == UserID && row.ProcessId == Processid
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool EntitiesAssignmentAuditManagerRiskExist(int customerid, long Branchid, long UserID, int Processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.EntitiesAssignmentAuditManagerRisks
                             where row.CustomerID == customerid && row.BranchID == Branchid
                             && row.UserID == UserID && row.ProcessId == Processid
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool EntitiesAssignmentAuditManagerLocationProcessAssigned(int customerID, long Branchid, int Processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityToBeDoneMappings
                             join row1 in entities.Mst_Process
                             on row.ProcessId equals row1.Id
                             where row.CustomerBranchID == Branchid
                             && row1.CustomerID == customerID
                             && row.ProcessId == Processid
                             select row1).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool checkInternalAuditSchedulingExist(long Branchid, int Processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditSchedulings
                             where row.CustomerBranchId == Branchid
                             && row.Process == Processid && row.IsDeleted == false
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static void CreateRisk(EntitiesAssignmentAuditManagerRisk objEntitiesAssignment)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.EntitiesAssignmentAuditManagerRisks.Add(objEntitiesAssignment);
                entities.SaveChanges();
            }
        }
        public static void AuditManagerDelete(int PorcessID, long branchid, long UserId, int Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                EntitiesAssignmentAuditManagerRisk eventToDelete = (from row in entities.EntitiesAssignmentAuditManagerRisks
                                                                    where row.UserID == UserId
                                                                    && row.BranchID == branchid
                                                                    && row.ProcessId == PorcessID
                                                                    && row.CustomerID == Customerid
                                                                    select row).FirstOrDefault();

                eventToDelete.ISACTIVE = false;
                entities.SaveChanges();
            }
        }
        public static void DepartmentHeadDelete(int DepartmentID, long branchid, long UserId, int Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                EntitiesAssignmentDepartmentHead eventToDelete = (from row in entities.EntitiesAssignmentDepartmentHeads
                                                                  where row.UserID == UserId
                                                                    && row.BranchID == branchid
                                                                    && row.DepartmentID == DepartmentID
                                                                    && row.CustomerID == Customerid
                                                                  select row).FirstOrDefault();

                eventToDelete.ISACTIVE = false;
                entities.SaveChanges();
            }
        }
        public static void ManagementDelete(int PorcessID, long branchid, long UserId, int Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                EntitiesAssignmentManagementRisk eventToDelete = (from row in entities.EntitiesAssignmentManagementRisks
                                                                  where row.UserID == UserId
                                                                    && row.BranchID == branchid
                                                                    && row.CustomerID == Customerid
                                                                    && row.ProcessId == PorcessID
                                                                  select row).FirstOrDefault();

                eventToDelete.ISACTIVE = false;
                entities.SaveChanges();
            }
        }
        public static List<AuditManagerAssignmentEntitiesView> SelectAllEntities(int branchId, int userID, int customerID, string UserRole)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ComplianceTransactionEntity = (from row in entities.AuditManagerAssignmentEntitiesViews
                                                   where row.Customerid == customerID
                                                   select row).ToList();

                if (branchId != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.BranchID == branchId).ToList();
                }
                if (userID != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
                }
                return ComplianceTransactionEntity;
            }
        }
    }
}
