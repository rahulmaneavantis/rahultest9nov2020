﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class FetchDataDeviation
    {
        public string Process { get; set; }
        public string SubProcess { get; set; }
        public string AuditStepDescription { get; set; }
        public string Title { get; set; }
        public string OriginalDocument { get; set; }
        public string RevisedDocument { get; set; }
        public string OriginalCratedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }
}
