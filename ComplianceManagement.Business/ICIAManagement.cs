﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Configuration;
namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ICIAManagement
    {
        public class ServiceProviderLimit_VM
        {
            public int customerID { get; set; }
            public int totalCustomerCreated { get; set; }
            public int totalEntityCreated { get; set; }

            public int totalCustomerLimit { get; set; }
            public int totalEntityLimit { get; set; }
        }

        public static ServiceProviderCustomerLimit Get_ServiceProviderCustomerLimit(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customer = (from row in entities.ServiceProviderCustomerLimits
                                where row.ServiceProviderID == customerID
                                select row).SingleOrDefault();

                return customer;
            }
        }
        //Create under serviceprovider/distributor User/Customer Limit
        public static void CreateServiceProviderCustomerLimit(ServiceProviderCustomerLimit obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ServiceProviderCustomerLimits.Add(obj);
                entities.SaveChanges();
            }
        }

        //Create Branch Limit while creating Customer under service provider/distributor
        public static void CreateCustomerBranchLimit(CustomerBranchLimit obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.CustomerBranchLimits.Add(obj);
                entities.SaveChanges();
            }
        }
        //While creating user under serviceprovider/distributor
        public static bool CheckServiceProviderUserLimit(int serviceProviderID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.ServiceProviderCustomerLimits
                                   where row.ServiceProviderID == serviceProviderID
                                   select row.UserLimit).FirstOrDefault();

                var serviceproviderusercount = (from row in entities.Users
                                                where row.CustomerID == serviceProviderID
                                                && row.IsDeleted == false
                                                select row.ID).ToList();


                //var serviceprovidercustomerUserCount = (from row in entities.Users
                //                                        join row1 in entities.Customers on row.CustomerID equals row1.ID
                //                                        where row1.ServiceProviderID == serviceProviderID
                //                                        select row.ID).ToList();


                //var UserCount = serviceproviderusercount.Union(serviceprovidercustomerUserCount).ToList();

                if (queryResult > serviceproviderusercount.Count())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //Check customer limit while creating new customer/Add Disable button under serviceprovider/distributor
        public static bool CheckCustomerLimit(int serviceProviderID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.ServiceProviderCustomerLimits
                                   where row.ServiceProviderID == serviceProviderID
                                   select row.CustomerLimit).FirstOrDefault();

                var CustomerCount = (from row in entities.Customers
                                     where row.ServiceProviderID == serviceProviderID
                                     && row.IsDeleted == false
                                     select row.ID).ToList();

                if (queryResult > CustomerCount.Count())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckCustomerLimitCompliance(int serviceProviderID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.ServiceProviderCustomerLimits
                                   where row.ServiceProviderID == serviceProviderID
                                   select row.CustomerLimit).FirstOrDefault();

                var CustomerCount = (from row in entities.Customers
                                     where row.ServiceProviderID == serviceProviderID
                                     && row.IsDeleted == false
                                     select row.ID).ToList();

                if (queryResult  >= CustomerCount.Count())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckCustomerLimitByDistributor(int distID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.ServiceProviderCustomerLimits
                                   where row.DistributorID == distID
                                   select row.CustomerLimit).FirstOrDefault();

                var CustomerCount = (from row in entities.Customers
                                     where row.ParentID == distID
                                     && row.IsDeleted == false
                                     select row.ID).ToList();

                if (queryResult > CustomerCount.Count())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckCustomerEntityLimitByDistributor(int distID)
        {
            bool canCreate = true;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var limitRecord = (from row in entities.ServiceProviderCustomerLimits
                                   where row.DistributorID == distID
                                   select row).FirstOrDefault();

                if (limitRecord != null)
                {
                    var totalCustomersCount = (from row in entities.Customers
                                               where row.ParentID == distID
                                               && row.IsDeleted == false
                                               select row.ID).Count();

                    var ParentBranchCount = (from cb in entities.CustomerBranches
                                             join cust in entities.Customers
                                             on cb.CustomerID equals cust.ID
                                             where cust.ParentID == distID
                                             && cb.ParentID == null
                                              && cb.IsDeleted == false
                                             select cb.ID).Count();

                    if (ParentBranchCount >= limitRecord.ParentBranchLimit)
                        canCreate = false;
                    //else if (totalCustomersCount >= limitRecord.CustomerLimit)
                    //    canCreate = false;
                    else
                        canCreate = true;
                }
            }

            return canCreate;
        }


        public static ServiceProviderLimit_VM GetCustomerEntityLimits(int distID)
        {
            ServiceProviderLimit_VM _obj = new ServiceProviderLimit_VM();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var limitRecord = (from row in entities.ServiceProviderCustomerLimits
                                   where row.DistributorID == distID
                                   select row).FirstOrDefault();

                if (limitRecord != null)
                {
                    var totalCustomersCount = (from row in entities.Customers
                                               where row.ParentID == distID
                                               && row.IsDeleted == false
                                               select row.ID).Count();

                    _obj.totalCustomerLimit = limitRecord.CustomerLimit;
                    _obj.totalCustomerCreated = totalCustomersCount;

                    var ParentBranchCount = (from cb in entities.CustomerBranches
                                             join cust in entities.Customers
                                             on cb.CustomerID equals cust.ID
                                             where cust.ParentID == distID
                                             && cb.ParentID == null
                                             && cb.IsDeleted == false
                                             select cb.ID).Count();

                    _obj.totalEntityLimit = limitRecord.ParentBranchLimit;
                    _obj.totalEntityCreated = ParentBranchCount;

                }
            }
            return _obj;
        }

        //Check User limit while creating new user/Add Disable button under serviceprovider/distributor
        public static bool ServiceProviderUserLimit(int serviceProviderID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.ServiceProviderCustomerLimits
                                   where row.ServiceProviderID == serviceProviderID
                                   select row.UserLimit).FirstOrDefault();

                var serviceproviderusercount = (from row in entities.Users
                                                where row.CustomerID == serviceProviderID
                                                && row.IsDeleted == false
                                                select row.ID).ToList();

                //var serviceprovidercustomerUserCount = (from row in entities.Users
                //                                        join row1 in entities.Customers on row.CustomerID equals row1.ID
                //                                        where row1.ServiceProviderID == serviceProviderID
                //                                        select row.ID).ToList();


                //var UserCount = serviceproviderusercount.Union(serviceprovidercustomerUserCount).ToList();
                if (queryResult > serviceproviderusercount.Count())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }          

        //changed by sushant 26 Aug 2020
        public static bool CheckCustoemrBranchLimit(int serviceProviderID, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ParentBranchLimitcount = (from row in entities.ServiceProviderCustomerLimits
                                              where row.ServiceProviderID == serviceProviderID
                                              //&& row.CustoemerID == customerID
                                              select row.ParentBranchLimit).FirstOrDefault();
                
                var ParentBranchCount = (from cb in entities.CustomerBranches
                                         join cust in entities.Customers
                                         on cb.CustomerID equals cust.ID
                                         where cust.ServiceProviderID == serviceProviderID
                                         && cb.ParentID == null
                                          && cb.IsDeleted == false
                                         select cb.ID).Count();



                if (ParentBranchLimitcount > ParentBranchCount)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckSubBrachLimit(long customerID, int serviceProviderID, int branchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var checkParent = (from row in entities.CustomerBranches
                                   where row.CustomerID == customerID
                                   && row.ParentID == branchID
                                   && row.IsDeleted==false
                                   select row).ToList();
                if (checkParent.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                    //var branchcount = (from row in entities.CustomerBranches
                    //                   where row.CustomerID == customerID
                    //                   && row.ID == branchID
                    //                   && row.IsDeleted == false
                    //                   select row).FirstOrDefault();
                    //if (branchcount != null)
                    //{
                    //    var ChildBranchLimitcount = (from row in entities.CustomerBranchLimits
                    //                                 where row.ServiceproviderID == serviceProviderID
                    //                                 && row.CustoemerID == customerID
                    //                                 select row.ParentBranchLimit).FirstOrDefault();

                    //    var childbranchcount = (from row in entities.CustomerBranches
                    //                            where row.CustomerID == customerID
                    //                            && row.IsDeleted == false
                    //                            select row.ID).ToList();

                    //    if (ChildBranchLimitcount > childbranchcount.Count)
                    //    {
                    //        return true;
                    //    }
                    //    else
                    //    {
                    //        return false;
                    //    }
                    //}
                    //else
                    //{
                    //    return true;
                    //}
                }
            }
        }

        

        #region HR ProductCompliance Limitations

        public static int GetParentIDforCustomer(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int? ParentID = (from x in entities.Customers
                                 where x.ID == CustomerID && x.IsDistributor == false && x.IsDeleted == false
                                 select x.ParentID).FirstOrDefault();
                if (ParentID == null)
                {
                    ParentID = CustomerID;
                }
                return Convert.ToInt32(ParentID);
            }

        }
        public static string GetClientIDforCustomer(int CustomerID, int ID)
        {
            string ClientID = "";
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ClientID = (from x in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                            where x.AVACOM_CustomerID == CustomerID && x.BranchType == "E" && x.AVACOM_BranchID == ID
                            select x.CM_ClientID).FirstOrDefault();

                return Convert.ToString(ClientID);
            }

        }
        public static bool CheckForLocationValidation(int NewRecord, int CustomerID, string ClientID)
        {
            bool Valid = false;
            int Record = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int? ServiceProviderID = GetParentIDforCustomer(CustomerID);
                    List<Customer> lstCustomers = new List<Customer>();
                    lstCustomers = GetAllCustomer(Convert.ToInt32(ServiceProviderID));

                    if (!string.IsNullOrEmpty(ClientID))
                    {
                        Record = (from c in lstCustomers
                                  join x in entities.RLCS_CustomerBranch_ClientsLocation_Mapping on c.ID equals x.AVACOM_CustomerID
                                  join y in entities.CustomerBranches on x.AVACOM_BranchID equals y.ID
                                  where y.ParentID!=null && y.IsDeleted == false
                                  select x).Count();
                    }

                    string MaxLimit = (from x in entities.ServiceProviderCustomerLimits
                                       where x.ServiceProviderID == ServiceProviderID 
                                       select x.BranchLimit).FirstOrDefault().ToString();

                    if ((Record + NewRecord) <= Convert.ToInt32(MaxLimit))
                    {
                        Valid = true;
                    }
                    return Valid;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CheckForEmployeeValidation(int NewRecord, int CustomerID)
        {
            bool Valid = false;
            int Record = 0;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int? ServiceProviderID = GetParentIDforCustomer(CustomerID);
                    Record = (from x in entities.RLCS_Employee_Master
                              where x.AVACOM_CustomerID == CustomerID 
                              select x).Count();

                    string MaxLimit = (from x in entities.ServiceProviderCustomerLimits
                                       where x.ServiceProviderID == ServiceProviderID 
                                       select x.EmployeeLimit).FirstOrDefault().ToString();

                    if ((Record + NewRecord) <= Convert.ToInt32(MaxLimit))
                    {
                        Valid = true;
                    }
                    return Valid;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }

        public static bool CheckForEntityValidation(int NewRecord, int CustomerID)
        {
            bool Valid = true;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int? ServiceProviderID = GetParentIDforCustomer(CustomerID);
                    List<Customer> lstCustomers = new List<Customer>();
                    lstCustomers = GetAllCustomer(Convert.ToInt32(ServiceProviderID));

                   var ParentBranchCount = (from c in lstCustomers
                              join x in entities.CustomerBranches on c.ID equals x.CustomerID
                              where x.ParentID == null && x.IsDeleted == false
                              select x.ID).Count();

                    var limitRecord = (from x in entities.ServiceProviderCustomerLimits 
                                       where x.ServiceProviderID == ServiceProviderID
                                       select x).FirstOrDefault();
                    
                    if ((ParentBranchCount + NewRecord) > Convert.ToInt32(limitRecord.ParentBranchLimit))
                    {
                        Valid = false;
                    }
                    return Valid;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }

        public static bool CheckForUserValidation(int NewRecord, int CustomerID)
        {
            bool Valid = false;
            int Record = 0;
            try
            {
                List<Customer> customers = new List<Customer>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int? ServiceProviderID = GetParentIDforCustomer(CustomerID);
                    Record = (from row in entities.Users
                              where row.CustomerID == ServiceProviderID
                              && row.IsDeleted == false
                              select row.ID).ToList().Count();

                    string MaxLimit = (from x in entities.ServiceProviderCustomerLimits
                                       where x.ServiceProviderID == ServiceProviderID
                                       select x.UserLimit).FirstOrDefault().ToString();

                    if ((Record + NewRecord) <= Convert.ToInt32(MaxLimit))
                    {
                        Valid = true;
                    }
                    return Valid;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }

        public static bool CheckForCustomerValidation(int NewRecord, int CustomerID)
        {
            bool Valid = false;
            int Record = 0;
            try
            {
                List<Customer> customers = new List<Customer>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int? ServiceProviderID = GetParentIDforCustomer(CustomerID);
                    var CustList = (from x in entities.Customers
                                    where x.ParentID == ServiceProviderID && x.IsDistributor == false && x.IsDeleted == false
                                    select x.ID).ToList();
                    foreach (var item in CustList)
                    {
                        Customer objcust = new Customer();
                        objcust.ID = Convert.ToInt32(item);
                        customers.Add(objcust);
                    }
                    if (customers.Count > 0)
                    {
                        Record = customers.Count;    //////+ 1;   ///1 AS ParentID
                    }
                    var MaxLimit = (from x in entities.ServiceProviderCustomerLimits
                                    where x.ServiceProviderID == ServiceProviderID
                                    select x.CustomerLimit).FirstOrDefault();

                    if ((Record + NewRecord) <= Convert.ToInt32(MaxLimit))
                    {
                        Valid = true;
                    }
                    return Valid;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }
        public static bool CheckAssesmentCoreEntityBranch(int CustomerID, string flag,int NewRecord)
        {
            int Entitycount = 0;
            bool valid = false;
            try
            {
                if (flag == "E")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Entitycount = (from x in entities.RLCS_CustomerAssessmentDetails
                                            where x.BranchType == "E" && x.CustomerID == CustomerID
                                       select x).Count();

                        valid = ICIAManagement.CheckForEntityValidation((Convert.ToInt32(Entitycount)+ NewRecord), CustomerID);
                    }
                }
                else if (flag == "B")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                         Entitycount = (from x in entities.RLCS_CustomerAssessmentDetails
                                            where x.BranchType == "B" && x.CustomerID == CustomerID
                                        select x).Count();
                        valid = ICIAManagement.CheckForLocationValidation((Convert.ToInt32(Entitycount) + NewRecord), CustomerID,"");
                    }
                }
                return valid;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static List<Customer> GetAllCustomer(int ParentID)
        {
            List<Customer> lstCustomer = new List<Customer>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                lstCustomer = (from x in entities.Customers
                               where x.ParentID == ParentID && x.IsDeleted == false
                               select x).ToList();
            }
            return lstCustomer;
        }
        //Check the Entity/Branch Created
        public static bool GetBranchCountCustomerwise(int CustomerID)
        {
            bool Valid = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int branchCnt = entities.CustomerBranches.Where(t => t.ParentID != null && t.CustomerID == CustomerID && t.IsDeleted == false).Count();
                    if (branchCnt > 0)
                    {
                        Valid = true;
                    }
                }
                return Valid;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Valid;
            }
        }
        #endregion
    }
}
