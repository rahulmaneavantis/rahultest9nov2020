﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class AssignEntityManagementRisk
    {
        public static List<long> CheckDepartMentHeadDepartments(int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.EntitiesAssignmentDepartmentHeads
                                         where row.UserID == userid && row.ISACTIVE == true
                                         select row.DepartmentID).Distinct().ToList();
                return transactionsQuery.ToList();
            }
        }
        public static List<long> CheckDepartMentHeadLocation(int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.EntitiesAssignmentDepartmentHeads
                                         where row.UserID == userid && row.ISACTIVE == true
                                         select row.BranchID).Distinct().ToList();
                return transactionsQuery.ToList();
            }
        }
        
        public static List<long> CheckAuditManagerLocation(int userid ,int CustomerBranchidID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.EntitiesAssignmentAuditManagerRisks
                                         where row.UserID == userid && row.BranchID== CustomerBranchidID && row.ISACTIVE == true
                                         select row.BranchID).Distinct().ToList();
                return transactionsQuery.ToList();
            }
        }
        public static List<long> CheckAuditManagerLocation(int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.EntitiesAssignmentAuditManagerRisks
                                         where row.UserID == userid && row.ISACTIVE==true
                                         select row.BranchID).Distinct().ToList();
                return transactionsQuery.ToList();
            }
        }
        public static List<long> CheckManngementLocation(int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.EntitiesAssignmentManagementRisks
                                         where row.UserID == userid && row.ISACTIVE == true
                                         select row.BranchID).Distinct().ToList();
                return transactionsQuery.ToList();
            }
        }
        private static void BindSumaryStatusChieldHierarchy(com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, List<AuditInstanceTransactionView> transactionsQuery, DataTable table, int period, DateTime EndDate)
        {
            foreach (var item in nvp.Children)
            {
                item.Level = nvp.Level + 1;
                DataRow tableRow = BindingManagementSummaryStatusTableRow(transactionsQuery, item, table, period, EndDate);
                table.Rows.Add(tableRow);
                BindSumaryStatusChieldHierarchy(item, transactionsQuery, table, period, EndDate);
            }

        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetLocation(int userID = -1)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                

                var query = (from Cbranch in entities.mst_CustomerBranch
                             where Cbranch.IsDeleted == false && Cbranch.Status == 1
                             select Cbranch).Distinct();
                query = query.Where(entry => entry.IsDeleted == false);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, false, entities);
                }
            }

            return hierarchy;

        }
        public static void LoadSubEntities(com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                where row.IsDeleted == false && row.Status == 1
                                                    select row);

            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntities(item, false, entities);
            }
        }
        public static DataTable GetManagementSummaryStatusReport(int userId, int year, int month, int period, bool approver = false)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                DateTime startDate = DateTime.Now;
                DateTime EndDate = DateTime.Now;

                //DateTime startDate = new DateTime(year, month, 1).AddMonths(-period);
                //DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);

                var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         where 
                                         //row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                         //&& (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         //&& 
                                         row.UserID == userId
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                DataTable table = new DataTable();
                table.Columns.Add("Location", typeof(string));
                table.Columns.Add("Approver", typeof(string));

                for (int i = period; i >= 1; i--)
                {
                    table.Columns.Add("Completed In Time_" + i, typeof(string));
                    table.Columns.Add("Completed After Due Date_" + i, typeof(long));
                    table.Columns.Add("Not Yet Completed_" + i, typeof(long));
                    table.Columns.Add("Total_" + i, typeof(long));
                    table.Columns.Add("Rating_" + i, typeof(string));
                    //table.Columns.Add(DateTime.UtcNow.AddMonths(-i).ToString("MMM") + " - " + DateTime.UtcNow.AddMonths(-i).Year, typeof(long));
                }

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> bracnhes = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy>();
                if (approver == true)
                {
                    transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    bracnhes = AssignEntityManagementRisk.GetAssignedLocationUserAndRoleWise(6, userId);
                }
                else
                {
                    bracnhes = AssignEntityManagementRisk.GetLocation(userId);
                }

                foreach (var item in bracnhes)
                {
                    item.Level = 0;
                    DataRow tableRow = BindingManagementSummaryStatusTableRow(transactionsQuery, item, table, period, EndDate);
                    table.Rows.Add(tableRow);
                    BindSumaryStatusChieldHierarchy(item, transactionsQuery, table, period, EndDate);
                }

                return table;


            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAssignedLocationUserAndRoleWise(int role, int userID = -1)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customerIds = (from row in entities.AuditAssignments
                                   join ci in entities.AuditInstances
                                   on row.RiskCreationId equals ci.RiskCreationId
                                   where row.UserID == userID && row.RoleID == role && ci.IsDeleted == false
                                   select ci.CustomerBranchID).Distinct();

                var query = (from row in entities.mst_CustomerBranch
                             where customerIds.Contains(row.ID) && row.IsDeleted == false && row.Status == 1
                             select row).ToList();

                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, false, entities);
                }
            }

            return hierarchy;
        }
        public static AuditInstanceTransactionView GetApproverName(int barnchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         where row.CustomerBranchID == barnchId && row.RoleID == 6                                    
                                         select row).ToList();

                if (transactionsQuery.Count != 0)
                {
                    return transactionsQuery[0];
                }
                else
                {
                    return null;
                }
            }
        }    
        private static DataRow BindingManagementSummaryStatusTableRow(List<AuditInstanceTransactionView> transactionsQuery, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy item, DataTable table, int period, DateTime EndDate)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var branchIDs = (from row in entities.mst_CustomerBranch
                                 where row.ParentID == item.ID && row.IsDeleted == false && row.Status == 1
                                 select row.ID).ToList();
                branchIDs.Add(item.ID);

                string color;
                DataRow tableRow = table.NewRow();

                tableRow["Location"] = item.Name + "#" + item.Level;
                tableRow["Approver"] = GetApproverName(item.ID) != null ? GetApproverName(item.ID).User : "";
                for (int i = period; i >= 1; i--)
                {
                    DateTime previousDate = EndDate.AddMonths(-i);
                    DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                    DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));



                    long Completedcount = transactionsQuery.Where(entry => (entry.AuditStatusID == 4 || entry.AuditStatusID == 7) && branchIDs.Contains((int)entry.CustomerBranchID)).Count();
                    tableRow["Completed In Time_" + i] = Completedcount;

                    long delayedcount = transactionsQuery.Where(entry => (entry.AuditStatusID == 9 || entry.AuditStatusID == 5) && branchIDs.Contains((int)entry.CustomerBranchID)).Count();
                    tableRow["Completed After Due Date_" + i] = delayedcount;

                    long pendingcount = transactionsQuery.Where(entry => (entry.AuditStatusID == 1 || entry.AuditStatusID == 2 || entry.AuditStatusID == 3 || entry.AuditStatusID == 6 || entry.AuditStatusID == 8 || entry.AuditStatusID == 10)
                                        && branchIDs.Contains((int)entry.CustomerBranchID)).Count();
                    tableRow["Not Yet Completed_" + i] = pendingcount;

                    long totalcount = delayedcount + Completedcount + pendingcount;
                    tableRow["Total_" + i] = totalcount;

                    long highRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID)).Count();
                    long MediumRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID)).Count();
                    long LowRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID)).Count();

                    long highRiskCompletedCount = transactionsQuery.Where(entry => (entry.AuditStatusID == 4 || entry.AuditStatusID == 5 || entry.AuditStatusID == 7 || entry.AuditStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID)).Count();
                    long MediumRiskCompletedCount = transactionsQuery.Where(entry => (entry.AuditStatusID == 4 || entry.AuditStatusID == 5 || entry.AuditStatusID == 7 || entry.AuditStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID)).Count();
                    long LowRiskCompletedCount = transactionsQuery.Where(entry => (entry.AuditStatusID == 4 || entry.AuditStatusID == 5 || entry.AuditStatusID == 7 || entry.AuditStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID)).Count();

                    double seventyfivePercentLow = (0.75 * LowRisktotalCount);
                    double seventyfivePercentHigh = (0.75 * highRisktotalCount);
                    if (highRisktotalCount != 0 || MediumRisktotalCount != 0 || LowRisktotalCount != 0)
                    {
                        if (highRisktotalCount == highRiskCompletedCount && MediumRisktotalCount == MediumRiskCompletedCount && LowRiskCompletedCount >= seventyfivePercentLow)
                        {
                            color = "GREEN";
                        }
                        else if (highRiskCompletedCount >= seventyfivePercentHigh && MediumRiskCompletedCount >= (MediumRisktotalCount / 2) && LowRiskCompletedCount >= (LowRisktotalCount / 2))
                        {
                            color = "BROWN";
                        }
                        else
                        {
                            color = "RED";
                        }
                    }
                    else
                    {
                        color = "RED";
                    }

                    tableRow["Rating_" + i] = color;

                }
                return tableRow;

            }
        }
        public static int GetCompanyOverview(int barnchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditInstanceTransactionView> transactionsQuery = new List<AuditInstanceTransactionView>();
                transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                     where row.CustomerBranchID == barnchId
                                     //&& (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                     select row).ToList();

                var userCount = transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;

                return userCount;

            }
        }
        public static List<AuditInstanceTransactionView> GetPerformerNameSatutory(int barnchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         where row.CustomerBranchID == barnchId && row.RoleID == 3
                                         //&& (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select row).ToList();

                return transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
            }
        }
        public static List<AuditInstanceTransactionView> GetReviwersName(int barnchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                         where row.CustomerBranchID == barnchId
                                         && (row.RoleID == 4 || row.RoleID == 5)
                                         //&& (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select row).ToList();

                return transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
            }
        }
        public static List<AuditDashboardSummaryview> GetManagementDetailView(int barnchId, string financialyear, string periodR, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, string TwelveMonth = "")
        {
           
            List<AuditDashboardSummaryview> detailView = new List<AuditDashboardSummaryview>();
           
            if (filter.Equals("Summary"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                                   .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.KeyId)
                                   && entry.ProcessId == functionId && entry.FinancialYear == financialyear)
                                   .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                                      .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.KeyId)
                                      && entry.ProcessId == functionId && entry.FinancialYear == financialyear
                                      && entry.ForMonth==periodR).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                                  .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear)
                                  .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                                     .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear
                                     && entry.ForMonth == periodR).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOD"))
            {
                List<int?> TODIds = new List<int?>();
                TODIds.Add(-1);
                TODIds.Add(1);
                TODIds.Add(2);
                TODIds.Add(3);
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear && TODIds.Contains((int?)entry.TOD)
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusIDs.Contains((int)entry.KeyId) && TODIds.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && TODIds.Contains((int?)entry.TOD)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && TODIds.Contains((int?)entry.TOD)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOE"))
            {
                List<int?> TOEIds = new List<int?>();
                TOEIds.Add(-1);
                TOEIds.Add(1);
                TOEIds.Add(2);
                TOEIds.Add(3);
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear && TOEIds.Contains((int?)entry.TOE)
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusIDs.Contains((int)entry.KeyId) && TOEIds.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && TOEIds.Contains((int?)entry.TOE)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && TOEIds.Contains((int?)entry.TOE)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TODNONKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.KeyId == 2
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TODKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }

            }
            else if (filter.Equals("TOENONKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.KeyId == 2
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOEKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId
                            && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetAuditDashboardSummary()
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }            
            return detailView;
        }
        
        public static DataTable GetManagementCompliancesSummary(int Customerid,int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId
                                             && row.FinancialYear == financialyear
                                             select new
                                             {
                                                 RiskCreationId = row.RiskCreationId,
                                                 row.ProcessId,
                                                 row.AuditStatusID,
                                                 row.KeyId,
                                                 row.ScheduledOnID,
                                                 row.RoleID,
                                                 row.UserID
                                             }).ToList();

                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long KeyCount;
                    long NonKeyCount;
                    long totalcount;
                  
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        KeyCount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        NonKeyCount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = KeyCount + NonKeyCount;
                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, KeyCount, NonKeyCount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId
                                             && row.FinancialYear == financialyear
                                              && row.ForMonth == periodR
                                             select new
                                             {
                                                 RiskCreationId = row.RiskCreationId,
                                                 row.ProcessId,
                                                 row.AuditStatusID,
                                                 row.KeyId,
                                                 row.ScheduledOnID,
                                                 row.RoleID,
                                                 row.UserID
                                             }).ToList();

                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long KeyCount;
                    long NonKeyCount;
                    long totalcount;
                    
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        KeyCount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        NonKeyCount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = KeyCount + NonKeyCount;
                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, KeyCount, NonKeyCount, totalcount);
                    }

                }
                return table;
            }
        }


        #region TOD
        public static DataTable GetCompliancesStatusTODWise(int Customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(2);
            TODIds.Add(3);
            TODIds.Add(4);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
              DataTable table = new DataTable();
              if (periodR == "All")
              {
                  var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                           where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear                                       
                                           select row).ToList();
                  if (approver == true)
                  {
                      transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                  }

                  transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                 
                  long Keycount;
                  long NonKeycount;
                  long totalcount;
                  table.Columns.Add("ID", typeof(int));
                  table.Columns.Add("Process", typeof(string));
                  table.Columns.Add("Key", typeof(long));
                  table.Columns.Add("NonKey", typeof(long));
                  table.Columns.Add("Total", typeof(long));
                  var ProcessList = ProcessManagement.GetAllNew(Customerid);
                  foreach (Mst_Process cc in ProcessList)
                  {
                      Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                      NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                      totalcount = Keycount + NonKeycount;

                      if (totalcount != 0)
                          table.Rows.Add(cc.Id, cc.Name, Keycount, NonKeycount, totalcount);
                  }    

              }
              else
              {
                  var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                           where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR
                                           select row).ToList();
                  if (approver == true)
                  {
                      transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                  }

                  transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                  long Keycount;
                  long NonKeycount;
                  long totalcount;
                  table.Columns.Add("ID", typeof(int));
                  table.Columns.Add("Process", typeof(string));
                  table.Columns.Add("Key", typeof(long));
                  table.Columns.Add("NonKey", typeof(long));
                  table.Columns.Add("Total", typeof(long));
                  var ProcessList = ProcessManagement.GetAllNew(Customerid);
                  foreach (Mst_Process cc in ProcessList)
                  {
                      Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                      NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                      totalcount = Keycount + NonKeycount;

                      if (totalcount != 0)
                          table.Rows.Add(cc.Id, cc.Name, Keycount, NonKeycount, totalcount);
                  }    

              }

                return table;


            }
        }
        public static DataTable GetCompliancesStatusTODKEYWise(int Customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(1);
            TODIds.Add(2);
            TODIds.Add(3);

            List<int> TODPassIds = new List<int>();
            TODPassIds.Add(1);
            TODPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                return table;
            }
        }
        public static DataTable GetCompliancesStatusTODNONKEYWise(int Customerid,int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(1);
            TODIds.Add(2);
            TODIds.Add(3);

            List<int> TODPassIds = new List<int>();
            TODPassIds.Add(1);
            TODPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {              
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                return table;
            }
        }
        #endregion
        #region TOE
        public static DataTable GetCompliancesTOEWise(int Customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Keycount;
                    long NonKeycount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Keycount + NonKeycount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Keycount, NonKeycount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Keycount;
                    long NonKeycount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Keycount + NonKeycount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Keycount, NonKeycount, totalcount);
                    }
                }
                return table;
            }
        }
        public static DataTable GetCompliancesStatusTOEKEYWise(int Customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);

            List<int> TOEPassIds = new List<int>();
            TOEPassIds.Add(1);
            TOEPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.TOE == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.TOE == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                return table;
            }
        }
        public static DataTable GetCompliancesStatusTOENONKEYWise(int Customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);

            List<int> TOEPassIds = new List<int>();
            TOEPassIds.Add(1);
            TOEPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.TOE == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.AuditInstanceTransactionViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var ProcessList = ProcessManagement.GetAllNew(Customerid);
                    foreach (Mst_Process cc in ProcessList)
                    {
                        Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.TOE == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                        totalcount = Passcount + Failcount;

                        if (totalcount != 0)
                            table.Rows.Add(cc.Id, cc.Name, Passcount, Failcount, totalcount);
                    }
                }
                return table;
            }
        }
        #endregion
    }
}
