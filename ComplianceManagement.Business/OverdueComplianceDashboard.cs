﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
   public class OverdueComplianceDashboard
    {
       public long ComplianceInstanceID { get; set; }
       public int? CustomerBranchID { get; set; }
       public long ComplianceID { get; set; }   
       //public string ComplianceTypeName { get; set; }
       public string State { get; set; }
       public int CustomerID { get; set; }
       public string ActName { get; set; }
       public string Section { get; set; }
       public DateTime? ScheduledOn { get; set; }
       public int ScheduledOnID { get; set; }
       public string ShortDescription { get; set; }
       public string Branch { get; set; }
       public string CustomerBranchName { get; set; }  
       public Nullable<int> ComplianceStatusID { get; set; }
       public string Performer { get; set; }
       public string Reviewer { get; set; }
       public string Approver { get; set; }
       public byte? Risk { get; set; }
       public string ForMonth { get; set; }
       public string Description { get; set; }       
       //public long PerformerID { get; set; }
       //public long ReviewerID { get; set; }
       //public long ApproverID { get; set; }
       public string FileName { get; set; }
       public string FilePath { get; set; }
       public string Status { get; set; }
       public Nullable<long> EventID { get; set; }
       public string EventName { get; set; }
       public string ReferenceMaterialText { get; set; }
       public Nullable<int> RoleID { get; set; }
       public Nullable<long> UserID { get; set; }
       public Nullable<byte> NonComplianceType { get; set; }


    }
}
