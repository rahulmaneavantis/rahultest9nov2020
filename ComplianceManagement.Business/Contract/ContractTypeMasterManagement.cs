﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Business.Contract
{
    public class ContractTypeMasterManagement
    {
        public static long GetContractTypeID(String ContractType, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objDocType = (from row in entities.Cont_tbl_TypeMaster
                                  where row.TypeName.Trim().ToUpper().Equals(ContractType.Trim().ToUpper())
                                  && row.CustomerID == CustomerID 
                                  && row.IsDeleted == false
                                  select row.ID).FirstOrDefault();

                return objDocType;
            }
        }

        public static long GetContractTypeID(List<Cont_tbl_TypeMaster> lstContractTypes, string ContractType, int CustomerID)
        {
            var objDocType = (from row in lstContractTypes
                              where row.TypeName.Trim().ToUpper().Equals(ContractType.Trim().ToUpper())
                              && row.CustomerID == CustomerID
                              && row.IsDeleted == false
                              select row.ID).FirstOrDefault();

            return objDocType;
        }

        public static bool ExistsContractType(String ContractType, int CustomerID, int ContractID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objDocType = (from row in entities.Cont_tbl_TypeMaster
                                  where row.TypeName.Equals(ContractType)
                                  && row.CustomerID == CustomerID && row.IsDeleted == false
                                  select row);

                if (ContractID != 0)
                {
                    objDocType = objDocType.Where(entry => entry.ID != ContractID);
                }

                return objDocType.Select(entry => true).SingleOrDefault();
            }
        }      
        
        public static List<Cont_tbl_TypeMaster> GetContractTypes_All(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractType = (from row in entities.Cont_tbl_TypeMaster
                                       where row.TypeName != null
                                       && row.IsDeleted == false
                                       && row.CustomerID == customerID
                                       select row).ToList();
                return lstContractType;
            }
        }

        public static List<Cont_SP_GetContractTypes_Paging_Result> GetContractTypes_Paging( int CutomerID, string Filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractType = entities.Cont_SP_GetContractTypes_Paging(CutomerID, Filter).ToList();

                return lstContractType;
            }
        }

        public static bool ExistsContractType(Cont_tbl_TypeMaster objcontype, long typeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objTypeMaster = (from row in entities.Cont_tbl_TypeMaster
                               where row.TypeName.Equals(objcontype.TypeName)
                               && row.CustomerID == objcontype.CustomerID
                               && row.IsDeleted == false
                               select row);

                if (objTypeMaster != null)
                {
                    if (typeID > 0)
                    {
                        objTypeMaster = objTypeMaster.Where(entry => entry.ID != typeID);
                    }
                }               

                return objTypeMaster.Select(entry => true).SingleOrDefault();
            }
        }

        public static int GetContractSubTypeIDByName(int customerID, long contractTypeID,string contractSubTypeName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int objSubType = Convert.ToInt32((from row in entities.Cont_tbl_SubTypeMaster
                                                  where row.SubTypeName.Trim().ToUpper().Equals(contractSubTypeName.Trim().ToUpper())
                                                  && row.ContractTypeID == contractTypeID
                                                  && row.CustomerID == customerID
                                                  && row.IsDeleted == false
                                                  select row.ID).FirstOrDefault());
                return objSubType;
            }
        }

        public static int GetContractSubTypeIDByName(List<Cont_tbl_SubTypeMaster> lstSubTypes, int customerID, long contractTypeID, string contractSubTypeName)
        {
            int objSubType = Convert.ToInt32((from row in lstSubTypes
                                              where row.SubTypeName.Trim().ToUpper().Equals(contractSubTypeName.Trim().ToUpper())
                                              && row.ContractTypeID == contractTypeID
                                              && row.CustomerID == customerID
                                              && row.IsDeleted == false
                                              select row.ID).FirstOrDefault());
            return objSubType;
        }

        public static Cont_tbl_TypeMaster GetContractTypeDetailByID(long contractTypeID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Objcase = (from row in entities.Cont_tbl_TypeMaster
                               where row.ID == contractTypeID
                               && row.CustomerID == customerID
                               select row).FirstOrDefault();
                return Objcase;
            }
        }

        public static string GetContTypeID()
        {
            string typeId = string.Empty;
            HttpContext context = HttpContext.Current;
            if (context.Session["TypeID"] != null)
            {
                typeId = context.Session["TypeID"].ToString();
            }
            return typeId;
        }

        public static string GetSubTypeID()
        {
            string typeId = string.Empty;
            HttpContext context = HttpContext.Current;
            if (context.Session["subTypeID"] != null)
            {
                typeId = context.Session["subTypeID"].ToString();
            }
            return typeId;
        }

        public static long CreateContractTypeDetails(Cont_tbl_TypeMaster objcontype)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Cont_tbl_TypeMaster.Add(objcontype);
                    entities.SaveChanges();

                    return objcontype.ID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool UpdateContractTypeDetails(Cont_tbl_TypeMaster objCase)
        {
            bool updateSuccess = false;
            try
            {


                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_TypeMaster objRecord = (from row in entities.Cont_tbl_TypeMaster
                                                     where row.ID == objCase.ID
                                                     && row.CustomerID == objCase.CustomerID
                                                     select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.TypeName = objCase.TypeName;
                        objRecord.UpdatedBy = objCase.UpdatedBy;
                        objRecord.UpdatedOn = objCase.UpdatedOn;

                        entities.SaveChanges();
                        updateSuccess = true;
                    }
                }
                return updateSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return updateSuccess;
            }
        }

        public static bool DeleteContractType(long TypeID, int customerID)
        {
            bool deleteSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_TypeMaster objCase = (from row in entities.Cont_tbl_TypeMaster
                                                   where row.ID == TypeID
                                                   && row.CustomerID == customerID
                                                   select row).FirstOrDefault();
                    if (objCase != null)
                    {
                        objCase.IsDeleted = true;
                        entities.SaveChanges();
                        deleteSuccess = true;
                    }
                }
                return deleteSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }

        public static List<Cont_SP_GetContractTypeSubType_Paging_Result> GetContractSubTypes_Paging(int customerID, long typeID, string Filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractSubType = entities.Cont_SP_GetContractTypeSubType_Paging(customerID, typeID, Filter).ToList();
                return lstContractSubType;
            }
        }

        public static List<Cont_SP_GetAllContractTypeSubType_Result> GetContractSubType_All(int customerID, long typeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractSubType = entities.Cont_SP_GetAllContractTypeSubType(customerID).Where(row => row.ContractTypeID == typeID).ToList();
                return lstContractSubType;
            }
        }

        public static bool ExistsSubContractType(Cont_tbl_SubTypeMaster objsubconttype, long subTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objsubcontype = (from row in entities.Cont_tbl_SubTypeMaster
                                     where row.SubTypeName.Equals(objsubconttype.SubTypeName)
                                        && row.CustomerID == objsubconttype.CustomerID
                                        && row.ContractTypeID==objsubconttype.ContractTypeID
                                        && row.IsDeleted == false
                                     select row);

                if (objsubcontype != null)
                {
                    if (subTypeID > 0)
                    {
                        objsubcontype = objsubcontype.Where(entry => entry.ID != subTypeID);
                    }
                }

                return objsubcontype.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool ExistsSubContractType(String ContractSubType, int CustomerID, long ContractID, long contractTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objDocType = (from row in entities.Cont_tbl_SubTypeMaster
                                  where row.SubTypeName.Trim().ToUpper().Equals(ContractSubType.Trim().ToUpper())
                                  && row.ContractTypeID == contractTypeID
                                  && row.CustomerID == CustomerID 
                                  && row.IsDeleted == false
                                  select row);

                if (ContractID != 0)
                {
                    objDocType = objDocType.Where(entry => entry.ID != ContractID);
                }

                return objDocType.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool UpdateContractSubType(Cont_tbl_SubTypeMaster objsubconttype)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_SubTypeMaster objRecord = (from row in entities.Cont_tbl_SubTypeMaster
                                                        where row.ID == objsubconttype.ID
                                                            && row.CustomerID == objsubconttype.CustomerID
                                                        select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.SubTypeName = objsubconttype.SubTypeName;
                        objRecord.UpdatedBy = objsubconttype.UpdatedBy;
                        objRecord.UpdatedOn = objsubconttype.UpdatedOn;

                        entities.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static long CreateContractSubType(Cont_tbl_SubTypeMaster objsubconttype)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Cont_tbl_SubTypeMaster.Add(objsubconttype);
                    entities.SaveChanges();
                }

                return objsubconttype.ID;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static Cont_tbl_SubTypeMaster GetContractSubTypeDetailByID(long subTypeID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objSubType = (from row in entities.Cont_tbl_SubTypeMaster
                                  where row.ID == subTypeID
                                  && row.CustomerID == customerID
                                  select row).FirstOrDefault();
                return objSubType;
            }
        }

        public static bool DeleteContractSubType(long subTypeID, int customerID)
        {
            bool deleteSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_SubTypeMaster objCase = (from row in entities.Cont_tbl_SubTypeMaster
                                                      where row.ID == subTypeID
                                                      && row.CustomerID == customerID
                                                      select row).FirstOrDefault();
                    if (objCase != null)
                    {
                        objCase.IsDeleted = true;
                        entities.SaveChanges();

                        deleteSuccess = true;
                    }
                    return deleteSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }

        public static List<Cont_tbl_SubTypeMaster> GetAllSubContractTypes(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                {
                    var lstContractsubType = (from row in entities.Cont_tbl_SubTypeMaster
                                              where row.SubTypeName != null
                                              && row.IsDeleted == false
                                              && row.CustomerID == customerID
                                              select row).ToList();
                    return lstContractsubType;
                }
            }
        }        
    }
}
