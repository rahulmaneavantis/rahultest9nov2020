﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.Contract
{
    public class DescriptionAttribute : Attribute
    {
        public string Value { get; private set; }

        public DescriptionAttribute(string Value)
        {
            this.Value = Value;
        }

        public override string ToString()
        {
            return this.Value;
        }
    }

    public enum PenaltyPerformer : byte
    {
        Status = 0,
        CompliedButPendingReview = 2,
        CompliedDelayedBuPendingReview = 3,
    }

    public class ContractEnumerations
    {

    }
}
