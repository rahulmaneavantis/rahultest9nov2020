﻿
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.Contract
{
    public class ContractCommonMethods
    {
        public static DateTime GetLastDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, DateTime.DaysInMonth(dateTime.Year, dateTime.Month));
        }

        public static DateTime GetFirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        public static string EncryptUsingKey(string input, string key)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string DecryptUsingKey(string input, string key)
        {
            byte[] inputArray = Convert.FromBase64String(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static bool matchLists<T>(List<T> lstA, List<T> lstB)
        {
            try
            {
                bool matchSuccess = false;

                if (lstA.Count != lstB.Count)
                {
                    matchSuccess = false;
                }
                else
                {
                    matchSuccess = lstA.Except(lstB).ToList().Count > 0 ? false : true;
                }

                return matchSuccess;
            }
            catch(Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }            
        }
        
        public static bool IsValidEmail(string Email)
        {
            return Regex.Match(Email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success;
        }

        public static bool IsValidPhoneNumber(string phoneNumber)
        {
            return Regex.Match(phoneNumber, @"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$").Success;
        }
               
        public static bool IsValidNumber(string Number)
        {
            int num;
            bool isNum = Int32.TryParse(Number, out num);

            return isNum;
        }

        public static bool CheckDate(string date)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(date);
                //DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool CheckValidDate(string date)
        {
            try
            {
                //DateTime dt = DateTime.Parse(date);
                string date1 = "";
                if (date.Contains("/"))
                {
                    date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains("-"))
                {
                    date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains(" "))
                {
                    date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
                }
                Convert.ToDateTime(date1);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool checkSheetExist(ExcelPackage xlWorkbook, string sheetNameUploaded)
        {
            try
            {
                bool matchFlag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (sheet.Name.Trim().Equals(sheetNameUploaded))
                    {
                        matchFlag = true;
                    }
                } //End ForEach
                return matchFlag;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool checkDuplicateDataExistExcelSheet(ExcelWorksheet xlWorksheet, int currentRowNumber, int colNum, string providedText)
        {
            bool matchSuccess = false;
            try
            {
                if (xlWorksheet != null)
                {
                    string TextToCompare = string.Empty;
                    int lastRow = xlWorksheet.Dimension.End.Row;

                    for (int i = 2; i <= lastRow; i++)
                    {
                        if (i != currentRowNumber)
                        {
                            TextToCompare = xlWorksheet.Cells[i, colNum].Text.ToString().Trim();
                            if (String.Equals(providedText, TextToCompare, StringComparison.OrdinalIgnoreCase))
                            {
                                matchSuccess = true;
                                i = lastRow; //exit from for Loop
                            }
                        }
                    }
                }

                return matchSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return matchSuccess;
            }
        }
                
    }
}
