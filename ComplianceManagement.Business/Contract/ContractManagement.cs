﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Business.Contract
{
    public class ContractManagement
    {
        #region Bind Controls

        public static List<ListItem> ReArrange_FileTags(CheckBoxList lstBoxFileTags)
        {
            List<ListItem> lstSelectedTags = new List<ListItem>();
            List<ListItem> lstNonSelectedTags = new List<ListItem>();

            foreach (ListItem eachListItem in lstBoxFileTags.Items)
            {
                if (eachListItem.Selected)
                    lstSelectedTags.Add(eachListItem);
                else
                    lstNonSelectedTags.Add(eachListItem);
            }

            return lstSelectedTags.Union(lstNonSelectedTags).ToList();
        }

        public static List<ListItem> GetSelectedItems(ListControl lst)
        {
            return lst.Items.OfType<ListItem>().Where(i => i.Selected).ToList();
        }

        public static void BindVendors(ListBox lstBoxtoBindData, int customerID)
        {
            var lstVendors = ContractMastersManagement.GetVendors_All(customerID);

            //Drop-Down at Modal Pop-up
            lstBoxtoBindData.DataTextField = "VendorName";
            lstBoxtoBindData.DataValueField = "ID";

            lstBoxtoBindData.DataSource = lstVendors;
            lstBoxtoBindData.DataBind();

            lstBoxtoBindData.Items.Add(new ListItem("Add New", "0"));
        }

        #endregion

        public static bool Save_ContractReminderLog(Cont_tbl_ReminderLog objRemind)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Cont_tbl_ReminderLog.Add(objRemind);
                    entities.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool Save_ContractReminderLogs(List<Cont_tbl_ReminderLog> lstObjRemind)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstObjRemind.ForEach(eachRecord =>
                    {
                        entities.Cont_tbl_ReminderLog.Add(eachRecord);
                    });

                    entities.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static void InsertLog(Exception ex, string ClassName, string FunctionName)
        {
            try
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> lst_LogMessages = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = ClassName;
                msg.FunctionName = FunctionName;
                msg.CreatedOn = DateTime.Now;
                if (ex != null)
                {
                    msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                    msg.StackTrace = ex.StackTrace;
                }
                else
                {
                    msg.Message = ClassName;
                    msg.StackTrace = ClassName;
                }
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;

                if (ex != null)
                {
                    lst_LogMessages.Add(new com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage() { LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ex.Message + "----\r\n" + ex.InnerException, StackTrace = ex.StackTrace, CreatedOn = DateTime.Now });
                }
                else
                {
                    lst_LogMessages.Add(new com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage() { LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ClassName, StackTrace = ClassName, CreatedOn = DateTime.Now });
                }
                Business.ComplianceManagement.InsertLogToDatabase(lst_LogMessages);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void InsertErrorMsg_DBLog(string recordID, string errorMessage, string ClassName, string FunctionName)
        {
            try
            {
                RLCS_LogMessage msg = new RLCS_LogMessage()
                {
                    RecordID = recordID,
                    ClassName = ClassName,
                    FunctionName = FunctionName,
                    Message = errorMessage,
                    CreatedOn = DateTime.Now,
                };

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.RLCS_LogMessage.Add(msg);
                    entities.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void InsertLogMessage(RLCS_LogMessage msg)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.RLCS_LogMessage.Add(msg);
                    entities.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void SendMailtoAssignedUsers(int customerID, int loggedInUserID, Cont_tbl_ContractInstance contractRecord, List<Tuple<int, int>> lstContractUserMapping, string emailTemplate, string accessURL) //List<int> assignedUserIDs
        {
            try
            {
                #region Sent Email to Assigned Users

                if (contractRecord != null)
                {
                    string customerName = CustomerManagement.GetByID(customerID).Name;

                    lstContractUserMapping.ForEach(eachUser =>
                    {
                        User User = UserManagement.GetByID(eachUser.Item1);

                        if (User != null)
                        {
                            if (User.Email != null && User.Email != "")
                            {  
                                string username = string.Format("{0} {1}", User.FirstName, User.LastName);

                                string message = emailTemplate
                                                .Replace("@User", username)
                                                .Replace("@ContractNo", contractRecord.ContractNo)
                                                .Replace("@Title", contractRecord.ContractTitle)
                                                .Replace("@Desc", contractRecord.ContractDetailDesc)
                                                .Replace("@AccessURL", accessURL)
                                                .Replace("@From", "Team " + customerName)
                                                .Replace("@PortalURL", accessURL);

                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { User.Email }), null, null, "Contract Notification-Assigned new Contract", message);

                                Cont_tbl_ReminderLog _objRemind = new Cont_tbl_ReminderLog()
                                {
                                    UserID = eachUser.Item1,
                                    Role = eachUser.Item2,
                                    TriggerType = "ContractAssignment",
                                    TriggerDate = DateTime.Now
                                };

                                bool saveSuccess = ContractManagement.Save_ContractReminderLog(_objRemind);

                                if (saveSuccess)
                                    ContractManagement.CreateAuditLog("C", contractRecord.ID, "Cont_tbl_ReminderLog", "Add", customerID, loggedInUserID, "Email Sent to Assigned User", true);
                            }
                        }
                    }); //End For Each 
                }

                #endregion
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<City> GetCityData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objCities = (from row in entities.Cities
                                 where row.IsDeleted == false
                                 select row).ToList();
                return objCities.ToList();
            }
        }

        public static List<State> GetStateData(int ContID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objStates = (from row in entities.States
                                 where row.CountryID == ContID
                                 && row.IsDeleted == false
                                 select row).ToList();
                return objStates.ToList();
            }
        }


        #region Contract

        public static List<int> GetAssignedRoles_Contract(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstAssignedRoles = (from row in entities.Cont_tbl_UserAssignment
                                        where row.UserID == userID
                                        && row.IsActive == true
                                        select row.RoleID).Distinct().ToList();

                return lstAssignedRoles.Distinct().ToList();
            }
        }

        public static Cont_SP_GetContractDetails_Result GetContractDetailsByContractID(int customerID, long contractID)
        {
            Cont_SP_GetContractDetails_Result record = new Cont_SP_GetContractDetails_Result();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    record = (from row in entities.Cont_SP_GetContractDetails(customerID, contractID)
                              select row).FirstOrDefault();
                    return record;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return record;
            }
        }

        public static Cont_tbl_ContractInstance GetContractByID(long contractID)
        {
            Cont_tbl_ContractInstance record = new Cont_tbl_ContractInstance();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    record = (from row in entities.Cont_tbl_ContractInstance
                              where row.ID == contractID
                              select row).FirstOrDefault();
                    return record;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return record;
            }
        }

        public static bool ExistsContractNo(int customerID, string contractNo, long contractID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Cont_tbl_ContractInstance
                                 where row.ContractNo.Trim().ToUpper().Equals(contractNo.Trim().ToUpper())
                                 && row.CustomerID == customerID
                                 && row.IsDeleted == false
                                 select row);

                    if (contractID != 0)
                    {
                        query = query.Where(entry => entry.ID != contractID);
                    }

                    return query.Select(entry => true).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistsContractNo(List<Cont_tbl_ContractInstance> masterlstExistingContracts, int customerID, string contractNo, long contractID)
        {
            var query = (from row in masterlstExistingContracts
                         where row.ContractNo.Trim().ToUpper().Equals(contractNo.Trim().ToUpper())
                         && row.CustomerID == customerID
                         && row.IsDeleted == false
                         select row);

            if (contractID != 0)
            {
                query = query.Where(entry => entry.ID != contractID);
            }

            return query.Select(entry => true).SingleOrDefault();
        }

        public static bool ExistsContractTitle(int customerID, string contractTitle, long contractID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (contractID != 0)
                {
                    var query = (from row in entities.Cont_tbl_ContractInstance
                                 where row.ContractTitle.Trim().ToUpper().Equals(contractTitle.Trim().ToUpper())
                                 && row.CustomerID == customerID
                                 && row.IsDeleted == false
                                 select row);

                    query = query.Where(entry => entry.ID != contractID);


                    //if (query != null)
                    //{
                    //    return true;
                    //}
                    //else
                    //{
                    //    return false;
                    //}
                    return query.Select(entry => true).SingleOrDefault();
                }
                else
                {
                    var query = (from row in entities.Cont_tbl_ContractInstance
                                 where row.IsDeleted == false
                                 && row.ContractTitle.ToUpper().Trim().Equals(contractTitle.ToUpper().Trim())
                                 select row).FirstOrDefault();

                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        public static bool ExistsContractTitle(List<Cont_tbl_ContractInstance> masterlstExistingContracts, int customerID, string contractTitle, long contractID)
        {
            if (contractID != 0)
            {
                var query = (from row in masterlstExistingContracts
                             where row.ContractTitle.Trim().ToUpper().Equals(contractTitle.Trim().ToUpper())
                             && row.CustomerID == customerID
                             && row.IsDeleted == false
                             select row);

                query = query.Where(entry => entry.ID != contractID);
              
                return query.Select(entry => true).SingleOrDefault();
            }
            else
            {
                var query = (from row in masterlstExistingContracts
                             where row.IsDeleted == false
                             && row.ContractTitle.Trim().ToUpper().Equals(contractTitle.Trim().ToUpper())
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static long CreateContract(Cont_tbl_ContractInstance contractRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    contractRecord.CreatedOn = DateTime.Now;
                    contractRecord.UpdatedOn = DateTime.Now;

                    entities.Cont_tbl_ContractInstance.Add(contractRecord);
                    entities.SaveChanges();

                    return contractRecord.ID;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }

        public static bool UpdateContract(Cont_tbl_ContractInstance contractRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var recordToUpdate = (from row in entities.Cont_tbl_ContractInstance
                                          where row.ID == contractRecord.ID
                                          select row).FirstOrDefault();

                    if (recordToUpdate != null)
                    {
                        recordToUpdate.ContractType = contractRecord.ContractType;
                        recordToUpdate.ContractNo = contractRecord.ContractNo;
                        recordToUpdate.ContractTitle = contractRecord.ContractTitle;
                        recordToUpdate.ContractDetailDesc = contractRecord.ContractDetailDesc;
                        recordToUpdate.CustomerBranchID = contractRecord.CustomerBranchID;
                        recordToUpdate.DepartmentID = contractRecord.DepartmentID;
                        recordToUpdate.ContractTypeID = contractRecord.ContractTypeID;
                        recordToUpdate.ContractSubTypeID = contractRecord.ContractSubTypeID;
                        recordToUpdate.ProposalDate = contractRecord.ProposalDate;
                        recordToUpdate.AgreementDate = contractRecord.AgreementDate;
                        recordToUpdate.EffectiveDate = contractRecord.EffectiveDate;
                        recordToUpdate.ReviewDate = contractRecord.ReviewDate;
                        recordToUpdate.ExpirationDate = contractRecord.ExpirationDate;
                        recordToUpdate.NoticeTermNumber = contractRecord.NoticeTermNumber;
                        recordToUpdate.NoticeTermType = contractRecord.NoticeTermType;
                        recordToUpdate.PaymentTermID = contractRecord.PaymentTermID;
                        recordToUpdate.ContractAmt = contractRecord.ContractAmt;
                        recordToUpdate.ProductItems = contractRecord.ProductItems;
                        recordToUpdate.PaymentType = contractRecord.PaymentType;
                        recordToUpdate.ContactPersonOfDepartment = contractRecord.ContactPersonOfDepartment;
                        recordToUpdate.AddNewClause = contractRecord.AddNewClause;
                        recordToUpdate.UpdatedBy = contractRecord.UpdatedBy;
                        recordToUpdate.UpdatedOn = contractRecord.UpdatedOn;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool DeleteContractByID(long contractID, int deletedByUserID)
        {
            try
            {
                bool deleteSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<Cont_tbl_FileData> lstContractDocs = entities.Cont_tbl_FileData
                                                                .Where(x => x.ContractID == contractID).ToList();

                    if (lstContractDocs.Count > 0)
                    {
                        lstContractDocs.ForEach(entry => entry.IsDeleted = true);
                        lstContractDocs.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        lstContractDocs.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    List<Cont_tbl_TaskInstance> tasksToDelete = entities.Cont_tbl_TaskInstance
                                                            .Where(x => x.ContractID == contractID).ToList();

                    if (tasksToDelete.Count > 0)
                    {
                        tasksToDelete.ForEach(entry => entry.IsActive = false);
                        tasksToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        tasksToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    var queryResult = (from row in entities.Cont_tbl_ContractInstance
                                       where row.ID == contractID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        entities.SaveChanges();
                        deleteSuccess = true;
                    }
                    else
                        deleteSuccess = false;

                    return deleteSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateContractTypeID(long contractID, long contractTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var caseRecordToUpdate = (from row in entities.Cont_tbl_ContractInstance
                                              where row.ID == contractID
                                              select row).FirstOrDefault();

                    if (caseRecordToUpdate != null)
                    {
                        caseRecordToUpdate.ContractTypeID = contractTypeID;
                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        
        public static List<Sp_Cont_View_ContractDetails_Result> GetContracts_All(int customerID)
        {
            try
            {
                List<Sp_Cont_View_ContractDetails_Result> lstContracts = new List<Sp_Cont_View_ContractDetails_Result>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstContracts = (from c in entities.Sp_Cont_View_ContractDetails(customerID)
                                    select c).ToList();


                    return lstContracts;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<Cont_SP_GetAssignedContracts_All_Result> GetAssignedContractsList(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int vendorID, int deptID, long contractStatusID, long contractTypeID) /*int branchID,*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_SP_GetAssignedContracts_All(customerID)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (vendorID != -1)
                        query = query.Where(entry => entry.VendorIDs.Split(',').ToList().Contains(vendorID.ToString())).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (contractStatusID != -1 && contractStatusID != 0)
                        query = query.Where(entry => entry.ContractStatusID == contractStatusID).ToList();

                    if (contractTypeID != -1 && contractTypeID != 0)
                        query = query.Where(entry => entry.ContractTypeID == contractTypeID).ToList();

                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.CreatedBy == loggedInUserID)).ToList();
                    else // In case of MGMT or CADMN 
                    {
                        query = query.Where(entry => entry.RoleID == roleID).Distinct().ToList();
                    }
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.ID, //ContractID
                                 g.CustomerID,
                                 g.ContractNo,
                                 g.ContractTitle,
                                 g.ContractDetailDesc,
                                 g.VendorIDs,
                                 g.VendorNames,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.ContractTypeID,
                                 g.TypeName,
                                 g.ContractSubTypeID,
                                 g.SubTypeName,
                                 g.ProposalDate,
                                 g.AgreementDate,
                                 g.EffectiveDate,
                                 g.ReviewDate,
                                 g.ExpirationDate,
                                 g.CreatedOn,
                                 g.UpdatedOn,
                                 g.StatusName,
                                 g.ContractAmt,
                                 g.ContactPersonOfDepartment,
                                 g.PaymentType,
                                 g.AddNewClause,
                                 g.Owner,
                                 g.AssignedUserID,
                                 g.IsDocument,
                                 g.OwnerIDs
                             } into GCS
                             select new Cont_SP_GetAssignedContracts_All_Result()
                             {
                                 ID = GCS.Key.ID, //ContractID
                                 CustomerID = GCS.Key.CustomerID,
                                 ContractNo = GCS.Key.ContractNo,
                                 ContractTitle = GCS.Key.ContractTitle,
                                 ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                 VendorIDs = GCS.Key.VendorIDs,
                                 VendorNames = GCS.Key.VendorNames,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 ContractTypeID = GCS.Key.ContractTypeID,
                                 TypeName = GCS.Key.TypeName,
                                 ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                 SubTypeName = GCS.Key.SubTypeName,
                                 ProposalDate = GCS.Key.ProposalDate,
                                 AgreementDate = GCS.Key.AgreementDate,
                                 EffectiveDate = GCS.Key.EffectiveDate,
                                 ReviewDate = GCS.Key.ReviewDate,
                                 ExpirationDate = GCS.Key.ExpirationDate,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 StatusName = GCS.Key.StatusName,
                                 ContractAmt = GCS.Key.ContractAmt,
                                 ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                 PaymentType=GCS.Key.PaymentType,
                                 AddNewClause=GCS.Key.AddNewClause,
                                 Owner = GCS.Key.Owner,
                                 AssignedUserID=GCS.Key.AssignedUserID,
                                IsDocument = GCS.Key.IsDocument,
                                  OwnerIDs= GCS.Key.OwnerIDs
                             }).ToList();
                }
                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                }

                return query.ToList();
            }
        }



        public static bool CreateContractDetails(Cont_tbl_ContractDetail objContRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objContRecord.CreatedOn = DateTime.Now;
                    entities.Cont_tbl_ContractDetail.Add(objContRecord);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static Cont_tbl_ContractDetail ExistsContractDetails(long contractID, int customerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var ExistingRecord = (from row in entities.Cont_tbl_ContractDetail
                                          where row.ContractID == contractID
                                          && row.CustomerID == customerID
                                          && row.IsActive == true
                                          select row).FirstOrDefault();

                    return ExistingRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Contract-Vendor Mapping

        public static bool CreateUpdate_VendorMapping(List<Cont_tbl_VendorMapping> lstVendorMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool saveSuccess = false;
                try
                {
                    lstVendorMapping.ForEach(eachRecord =>
                    {
                        var vendorMappingExists = (from row in entities.Cont_tbl_VendorMapping
                                                   where row.ContractID == eachRecord.ContractID
                                                   && row.VendorID == eachRecord.VendorID
                                                   select row).FirstOrDefault();

                        if (vendorMappingExists != null)
                        {
                            vendorMappingExists.IsActive = true;
                            vendorMappingExists.UpdatedBy = eachRecord.CreatedBy;
                            vendorMappingExists.UpdatedOn = DateTime.Now;
                            saveSuccess = true;
                        }
                        else
                        {
                            entities.Cont_tbl_VendorMapping.Add(eachRecord);
                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool DeActiveExistingVendorMapping(long contractID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var queryResult = (from row in entities.Cont_tbl_VendorMapping
                                       where row.ContractID == contractID
                                       && row.IsActive == true
                                       select row).ToList();

                    if (queryResult.Count > 0)
                    {
                        queryResult.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<long> GetVendorMapping(long contractID)
        {
            List<long> lstVendorMappingIDs = new List<long>();
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstVendorMappingIDs = (from row in entities.Cont_tbl_VendorMapping
                                           where row.ContractID == contractID
                                           && row.IsActive == true
                                           select row.VendorID).ToList();
                    return lstVendorMappingIDs;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstVendorMappingIDs;
            }
        }

        #endregion

        #region Contract User-Owner Assignment

        public static List<int> GetAssignedUsersByRoleID(long contractID, int roleID)
        {
            List<int> lstAssignedUserIDs = new List<int>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstAssignedUserIDs = (from row in entities.Cont_tbl_UserAssignment
                                          where row.ContractID == contractID
                                          && row.RoleID == roleID
                                          && row.IsActive == true
                                          select row.UserID).ToList();
                    return lstAssignedUserIDs;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstAssignedUserIDs;
            }
        }

        public static bool CreateContractUserAssignment(Cont_tbl_UserAssignment objNewAssignmentRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objNewAssignmentRecord.CreatedOn = DateTime.Now;
                    objNewAssignmentRecord.UpdatedOn = DateTime.Now;

                    entities.Cont_tbl_UserAssignment.Add(objNewAssignmentRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistContractUserAssignment(Cont_tbl_UserAssignment objAssignmentRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var record = (from row in entities.Cont_tbl_UserAssignment
                                  where row.ContractID == objAssignmentRecord.ContractID
                                  && row.UserID == objAssignmentRecord.UserID
                                  && row.RoleID == objAssignmentRecord.RoleID
                                  select row).FirstOrDefault();
                    if (record != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<Cont_tbl_UserAssignment> GetContractUserAssignment_All(long contractID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_tbl_UserAssignment
                                   where row.ContractID == contractID
                                   && row.IsActive == true
                                   select row).ToList();
                return queryResult;
            }
        }

        public static bool DeleteContractUserAssignment(int assignmentRecordID, long contractID)
        {
            bool saveSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var queryResult = (from row in entities.Cont_tbl_UserAssignment
                                       where row.ContractID == contractID
                                       && row.ID == assignmentRecordID
                                       select row).FirstOrDefault();
                    if (queryResult != null)
                    {
                        queryResult.IsActive = false;

                        entities.SaveChanges();
                        saveSuccess = true;
                    }

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        public static bool DeActiveContractUserAssignments(long contractID, int loggedInUserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var record = (from row in entities.Cont_tbl_UserAssignment
                                  where row.ContractID == contractID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = loggedInUserID);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateContractUserAssignments(Cont_tbl_UserAssignment _objAssignmentRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var record = (from row in entities.Cont_tbl_UserAssignment
                                  where row.ContractID == _objAssignmentRecord.ContractID
                                  && row.UserID == _objAssignmentRecord.UserID
                                  && row.RoleID == _objAssignmentRecord.RoleID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = true);
                        record.ForEach(entry => entry.UpdatedBy = _objAssignmentRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdateContractUserAssignment(Cont_tbl_UserAssignment _objAssignmentRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevAssignmentRecord = (from row in entities.Cont_tbl_UserAssignment
                                                where row.ContractID == _objAssignmentRecord.ContractID
                                                && row.UserID == _objAssignmentRecord.UserID
                                                && row.RoleID == _objAssignmentRecord.RoleID
                                                select row).FirstOrDefault();

                    if (prevAssignmentRecord != null)
                    {
                        prevAssignmentRecord.IsActive = true;
                        prevAssignmentRecord.UpdatedBy = _objAssignmentRecord.CreatedBy;
                        prevAssignmentRecord.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }
                    else
                    {
                        _objAssignmentRecord.CreatedOn = DateTime.Now;
                        _objAssignmentRecord.UpdatedOn = DateTime.Now;
                        entities.Cont_tbl_UserAssignment.Add(_objAssignmentRecord);
                        saveSuccess = true;
                    }
                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdate_ContractUserAssignments(List<Cont_tbl_UserAssignment> lstAssignmentRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstAssignmentRecord.ForEach(_objAssignmentRecord =>
                    {
                        var prevAssignmentRecord = (from row in entities.Cont_tbl_UserAssignment
                                                    where row.ContractID == _objAssignmentRecord.ContractID
                                                    && row.UserID == _objAssignmentRecord.UserID
                                                    && row.RoleID == _objAssignmentRecord.RoleID
                                                    select row).FirstOrDefault();

                        if (prevAssignmentRecord != null)
                        {
                            prevAssignmentRecord.IsActive = true;
                            prevAssignmentRecord.UpdatedBy = _objAssignmentRecord.CreatedBy;
                            prevAssignmentRecord.UpdatedOn = DateTime.Now;

                            saveSuccess = true;
                        }
                        else
                        {
                            _objAssignmentRecord.CreatedOn = DateTime.Now;
                            _objAssignmentRecord.UpdatedOn = DateTime.Now;
                            entities.Cont_tbl_UserAssignment.Add(_objAssignmentRecord);

                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Contract-Custom Field

        public static List<Cont_tbl_CustomField> GetCustomsFieldsByContractType(int customerID, long contractTypeID)
        {
            List<Cont_tbl_CustomField> customFieldsTypeWise = new List<Cont_tbl_CustomField>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    customFieldsTypeWise = (from row in entities.Cont_tbl_CustomField
                                            where row.TypeID == contractTypeID
                                            && row.CustomerID == customerID
                                            && row.IsActive == true
                                            select row).ToList();
                    return customFieldsTypeWise;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return customFieldsTypeWise;
            }
        }

        public static List<Cont_SP_GetCustomFieldsValues_Result> GetCustomsFieldsContractWise_All(int customerID, long contractID, long contractTypeID)
        {
            List<Cont_SP_GetCustomFieldsValues_Result> customFieldsContractWise = new List<Cont_SP_GetCustomFieldsValues_Result>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    customFieldsContractWise = (from row in entities.Cont_SP_GetCustomFieldsValues(contractTypeID, contractID, customerID)
                                                select row).ToList();

                    return customFieldsContractWise;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return customFieldsContractWise;
            }
        }

        public static bool CreateUpdateCustomsField(Cont_tbl_CustomFieldValue _objParameter)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.Cont_tbl_CustomFieldValue
                                      where row.ContractID == _objParameter.ContractID
                                      && row.LabelID == _objParameter.LabelID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.LabelValue = _objParameter.LabelValue;
                        prevRecord.IsDeleted = false;
                        prevRecord.UpdatedBy = _objParameter.UpdatedBy;
                        prevRecord.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }
                    else
                    {
                        entities.Cont_tbl_CustomFieldValue.Add(_objParameter);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdate_CustomsFields(List<Cont_tbl_CustomFieldValue> lstObjParameter)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstObjParameter.ForEach(_objParameter =>
                    {
                        var prevRecord = (from row in entities.Cont_tbl_CustomFieldValue
                                          where row.ContractID == _objParameter.ContractID
                                          && row.LabelID == _objParameter.LabelID
                                          select row).FirstOrDefault();

                        if (prevRecord != null)
                        {
                            prevRecord.LabelValue = _objParameter.LabelValue;

                            prevRecord.IsDeleted = false;
                            prevRecord.UpdatedBy = _objParameter.UpdatedBy;
                            prevRecord.UpdatedOn = DateTime.Now;
                            saveSuccess = true;
                        }
                        else
                        {
                            entities.Cont_tbl_CustomFieldValue.Add(_objParameter);
                            saveSuccess = true;
                        }

                        entities.SaveChanges();
                    });

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeActiveExistingCustomsFieldsByContractID(long contractID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var QueryResult = (from row in entities.Cont_tbl_CustomFieldValue
                                       where row.ContractID == contractID
                                       && row.IsDeleted == false
                                       select row).ToList();

                    if (QueryResult.Count > 0)
                    {
                        QueryResult.ForEach(entry => entry.IsDeleted = true);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteCustomFieldsContractWise(long contractID, long labelID)
        {
            try
            {
                using (ComplianceDBEntities Entities = new ComplianceDBEntities())
                {
                    var QueryResult = (from row in Entities.Cont_tbl_CustomFieldValue
                                       where row.ContractID == contractID
                                       && row.LabelID == labelID
                                       && row.IsDeleted == false
                                       select row).FirstOrDefault();

                    if (QueryResult != null)
                    {
                        QueryResult.IsDeleted = true;

                        Entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeletePreviousCustomFields(long contractID, long contractTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var recordToDelete = (from CFV in entities.Cont_tbl_CustomFieldValue
                                          join CF in entities.Cont_tbl_CustomField
                                          on CFV.LabelID equals CF.ID
                                          where CFV.ContractID == contractID
                                          && CF.TypeID != contractTypeID
                                          select CFV).ToList();

                    if (recordToDelete.Count > 0)
                    {
                        recordToDelete.ForEach(row => row.IsDeleted = true);
                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        #endregion

        #region Contract-Audit Log

        public static bool CreateAuditLog(string noticeOrCase, long contractID, string TableName, string Action, int customerID, int Createdby, string Remark, bool IsVisibleToUser)
        {
            //noticeOrCase 1-Case 2-Notice
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    Cont_tbl_AuditLog auditLog_Record = new Cont_tbl_AuditLog()
                    {
                        Type = noticeOrCase,
                        ContractID = contractID,
                        TableName = TableName,
                        Action = Action,
                        CustomerID = customerID,
                        CreatedBy = Createdby,
                        CreatedOn = DateTime.Now,
                        IsActive = true,
                        Remark = Remark,
                        SentMail = false,
                        IsVisibleToUser = IsVisibleToUser,
                    };

                    entities.Cont_tbl_AuditLog.Add(auditLog_Record);
                    entities.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static List<Cont_SP_AuditLogs_Paging_Result> GetContractAuditLogs_Paging(int customerID, long contractID, int pageSize, int pageNumber)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstAuditLogs = (from row in entities.Cont_SP_AuditLogs_Paging(pageSize, pageNumber, contractID, customerID)
                                    select row).OrderByDescending(entry => entry.CreatedOn).ToList();
                return lstAuditLogs.ToList();
            }
        }

        public static List<Cont_SP_AuditLogs_All_Result> GetContractAuditLogs_All(int customerID, long contractID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstAuditLogs = (from row in entities.Cont_SP_AuditLogs_All(customerID, contractID)
                                    select row).OrderByDescending(entry => entry.CreatedOn).ToList();
                return lstAuditLogs.ToList();
            }
        }

        #endregion


        #region Contract Linking

        public static bool CreateUpdateContractLinking(Cont_tbl_ContractLinking objNewRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.Cont_tbl_ContractLinking
                                      where row.ContractID == objNewRecord.ContractID
                                      && row.LinkedContractID == objNewRecord.LinkedContractID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.IsActive = true;
                        prevRecord.UpdatedBy = objNewRecord.UpdatedBy;
                        prevRecord.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }
                    else
                    {
                        objNewRecord.CreatedOn = DateTime.Now;
                        objNewRecord.UpdatedOn = DateTime.Now;

                        entities.Cont_tbl_ContractLinking.Add(objNewRecord);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteContractLinking(long contractID, long linkedContractID, int loggedInUserID)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.Cont_tbl_ContractLinking
                                      where row.ContractID == contractID
                                      && row.LinkedContractID == linkedContractID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.IsActive = false;
                        prevRecord.UpdatedBy = loggedInUserID;
                        prevRecord.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<Cont_SP_GetLinkedContracts_All_Result> GetLinkedContractList_All(long contractID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_SP_GetLinkedContracts_All(contractID, customerID)
                             select row).ToList();

                return query.ToList();
            }
        }

        public static List<long> GetLinkedContractIDs(long contractID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_tbl_ContractLinking
                             where row.ContractID == contractID
                             && row.IsActive == true
                             select row.LinkedContractID).ToList();

                return query.ToList();
            }
        }

        #endregion

        #region Contract Status-Transaction

        public static bool CreateContractStatusTransaction(Cont_tbl_ContractStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objNewStatusRecord.CreatedOn = DateTime.Now;
                    objNewStatusRecord.UpdatedOn = DateTime.Now;

                    entities.Cont_tbl_ContractStatusTransaction.Add(objNewStatusRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistContractStatusTransaction(Cont_tbl_ContractStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var record = (from row in entities.Cont_tbl_ContractStatusTransaction
                                  where row.ContractID == objNewStatusRecord.ContractID
                                  && row.StatusID == objNewStatusRecord.StatusID
                                   && row.IsActive == true
                                  select row).FirstOrDefault();
                    if (record != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool Exist_ContractStatusTransaction(Cont_tbl_ContractStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var record = (from row in entities.Cont_tbl_ContractStatusTransaction
                                  where row.ContractID == objNewStatusRecord.ContractID
                                  //&& row.StatusID == objNewStatusRecord.StatusID
                                   && row.IsActive == true
                                  select row).Count();
                    if (record > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateCaseStatusTransaction(tbl_LegalCaseStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var record = (from row in entities.tbl_LegalCaseStatusTransaction
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.CaseInstanceID == objNewStatusRecord.CaseInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = objNewStatusRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();

                        var recordToUpdate = record.Where(entry => entry.StatusID == objNewStatusRecord.StatusID).FirstOrDefault();

                        if (recordToUpdate != null)
                        {
                            recordToUpdate.IsActive = true;
                            entities.SaveChanges();
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeActiveCaseStatusTransaction(tbl_LegalCaseStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var record = (from row in entities.tbl_LegalCaseStatusTransaction
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.CaseInstanceID == objNewStatusRecord.CaseInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = objNewStatusRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        public static bool CreateUpdate_ContractToContractMapping(Cont_tbl_ContractToContractMapping objMapping)
        {
            using (ComplianceDBEntities Entities = new ComplianceDBEntities())
            {
                var prevMappingRecord = (from row in Entities.Cont_tbl_ContractToContractMapping
                                         where row.OldContractID == objMapping.OldContractID
                                         && row.NewContractID == objMapping.NewContractID
                                         select row).FirstOrDefault();
                if (prevMappingRecord != null)
                {
                    prevMappingRecord.IsActive = true;

                    prevMappingRecord.UpdatedBy = objMapping.UpdatedBy;
                    prevMappingRecord.UpdatedOn = objMapping.UpdatedOn;
                }
                else
                    Entities.Cont_tbl_ContractToContractMapping.Add(objMapping);

                Entities.SaveChanges();
                return true;

            }
        }
    }
}
