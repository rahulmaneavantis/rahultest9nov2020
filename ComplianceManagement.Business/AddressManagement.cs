﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class AddressManagement
    {
        public static List<Country> GetAllCountry()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var country = (from row in entities.Countries
                               where row.IsDeleted == false
                              orderby row.Name ascending              
                              select row).ToList();

                return country.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<State> GetAllStates()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var states = (from row in entities.States
                              orderby row.Name ascending
                              select row).ToList();

                return states.OrderBy(entry=>entry.Name).ToList();
            }
        }

        public static List<City> GetAllCitiesByState(int stateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var cities = (from row in entities.Cities
                              where row.StateId == stateID
                              orderby row.Name ascending
                              select row).ToList();

                return cities.OrderBy(entry=>entry.Name).ToList();
            }
        }
    }
}
