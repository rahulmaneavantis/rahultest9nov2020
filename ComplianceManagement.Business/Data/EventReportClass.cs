﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
   public class EventReportClass
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? CustomerBranchID { get; set; }
        public string CustomerBranchName { get; set; }
        public int? CustomerID { get; set; }
        public string ComplianceShortDescription { get; set; }
        public long? ComplianceID { get; set; }
    }
}
