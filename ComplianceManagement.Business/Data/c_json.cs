﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class c_json
    {
        public DataTable fetch(int customerid, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.ComplianceInstanceTransactionViews
                             where row.CustomerID == customerid && row.UserID == userid
                             select new
                             {
                                 ScheduledOn = row.ScheduledOn,
                             }).Distinct().ToList();

                var Query2 = (from row in entities.InternalComplianceInstanceTransactionViews
                              where row.CustomerID == customerid && row.UserID == userid
                              select new
                              {
                                  ScheduledOn = row.InternalScheduledOn,
                              }).Distinct().ToList();

                DataTable table = new DataTable();
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                if (Query.Count > 0)
                {
                    foreach (var item in Query)
                    {
                        table.Rows.Add(item.ScheduledOn);
                    }
                }
                if (Query2.Count > 0)
                {
                    foreach (var item in Query2)
                    {
                        table.Rows.Add(item.ScheduledOn);
                    }
                }
                return table;
            }
        }
    }
}
