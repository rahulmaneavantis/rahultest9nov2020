//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TEMP_RLCS_Yearly_LeaveCredit_Master
    {
        public int LM_ID { get; set; }
        public string LM_ClientID { get; set; }
        public string LM_StateID { get; set; }
        public string LM_BranchID { get; set; }
        public string LM_LeaveType { get; set; }
        public Nullable<double> LM_NoOfDays { get; set; }
        public Nullable<int> LM_Year { get; set; }
        public string LM_CreatedBy { get; set; }
        public Nullable<System.DateTime> LM_CreatedDate { get; set; }
        public string LM_ModifiedBy { get; set; }
        public Nullable<System.DateTime> LM_ModifiedDate { get; set; }
        public Nullable<int> user_id { get; set; }
        public Nullable<int> fileid { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<System.DateTime> deleted_at { get; set; }
        public Nullable<int> AVACOM_CustomerID { get; set; }
        public Nullable<int> AVACOM_BranchID { get; set; }
        public Nullable<bool> ISProcessed { get; set; }
        public Nullable<bool> ISActive { get; set; }
        public Nullable<bool> ISValidated { get; set; }
        public Nullable<int> excel_row_no { get; set; }
        public string LM_BranchName { get; set; }
    }
}
