//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RLCS_tbl_CheckListMaster
    {
        public long Id { get; set; }
        public string ActID { get; set; }
        public string ChecklistID { get; set; }
        public string StateID { get; set; }
        public string NatureOfCompliance { get; set; }
        public string Section { get; set; }
        public string Checklist_Rule { get; set; }
        public string Form { get; set; }
        public string TypeOfCompliance { get; set; }
        public string Frequency { get; set; }
        public string Type { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public string Risk { get; set; }
        public string Description { get; set; }
        public string Consequences { get; set; }
        public Nullable<int> customerid { get; set; }
    }
}
