﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    public enum DocumentFilterNewStatusRevise : byte
    {
        //All = 0,
        [Description("All")]
        Status = 0,
        ClosedTimely,
        ClosedDelayed,
        [Description("Pending for Review")]
        PendingForReview,
    }
    public class DescriptionAttribute : Attribute
    {
      
        public string Value { get; private set; }

        public DescriptionAttribute(string Value)
        {
            this.Value = Value;
        }

        public override string ToString()
        {
            return this.Value;
        }
    }
    public enum VFrequency : byte
    {
        Monthly = 1,
        HalfYearly,
        Annual,
        Quarterly
    }
    public enum EventBasedFrequency : byte
    {
        Monthly = 0,
        Quarterly = 1,
        HalfYearly = 2,
        Annual = 3,
        FourMonthly = 4,
        TwoYearly = 5,
        SevenYearly = 6,
        Weekly = 8,
        Fortnight = 9
    }


    public enum PenaltyPerformer : byte
    {
        Status = 0,
        CompliedButPendingReview = 2,
        CompliedDelayedBuPendingReview = 3,
    }
    public enum PenaltyReviewer : byte
    {
        Status = 0,
        ClosedDelayed = 5,
    }
    public enum CheckListCannedReportPerformer : byte
    {
        //All = 0,
        [Description("All")]
        Status = 0,
        Upcoming,
        ClosedTimely,
        Overdue,
        NotApplicable
    }

    public enum CheckListCannedReportPerformerAddedNotCompliedStatus : byte
    {
        //All = 0,
        [Description("All")]
        Status = 0,
        Upcoming,
        ClosedTimely,
        Overdue,
        NotApplicable,
        NotComplied
    }
    public enum Performer : byte
    {
        Status = 0,
        Upcoming,
        Overdue,     
        Rejected,
        PendingForReview,
    }

    public enum Reviewer : byte
    {
        Status = 0,
        Upcoming,
        DueButNotSubmitted,
        PendingForReview,
        Rejected,
    }

    public enum DataType : byte
    {
        Integer = 0,
        String,
        Double,
        DateTime
    }

    public enum EventType : byte
    {
        Parent = 0,
        // Sub
    }

    public enum BranchType : byte
    {
        LegalEntity = 0,
        Company,
        Plant,
        Warehouse
    }

    public enum LegalRelationship : byte
    {
        Parent = 0,
        SubsidiaryCompany,
        JointVenture
    }

    public enum LegalStatus : byte
    {
        ManufacturingUnit = 0,
        MarketingOffice,
        Branch,
        Warehouse
    }

    public enum Days : byte
    {
        Sunday = 0,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday
    }

    public enum Frequency : byte
    {
        Monthly = 0,
        Quarterly,
        HalfYearly,
        Annual,
        FourMonthly,
        TwoYearly,
        SevenYearly,
        Daily,
        Weekly
        //Eventbased,
        //Onetime,
        //OnetimeOrEeventbased,
        //Ongoing,
        //Weekly,
        //Priliminary,
        //Timebased

    }

    public enum Month : byte
    {
        January = 1,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    }

    public enum ReminderStatus : byte
    {
        Pending = 0,
        Sent,
        Cancelled
    }
    public enum CheckListStatus : byte
    {
        [Description("All")]
        Status = 0,
        Overdue,
        ClosedTimely,
    }

    //public enum NatureOfCompliance : byte
    //{
    //    Returns = 0,
    //    Payments,
    //    ABC,
    //    Report,
    //    Training,
    //    Inspection,
    //    Meeting,
    //    Registers,
    //    [Description("Certificates/Licensing")]
    //    CertificatesLicensing,
    //    [Description("Holiday List")]
    //    HolidayList,
    //    Cleanliness,
    //    [Description("Minimum Wages")]
    //    MinimumWages,
    //    Canteen,
    //    Examination,
    //    Maintenance,
    //    [Description("Notices/Correspondences")]
    //    NoticesCorrespondences,
    //    [Description("Safety and Welfare")]
    //    SafetyandWelfare,
    //    Others,
    //    Challans
    //}

    public enum CannedReportFilterForPerformer : byte
    {
        //All = 0,
        [Description("All")]
        Status=0,
        Upcoming,
        Completed,
        Overdue,
        [Description("Pending for Review")]
        PendingForReview,
        Rejected
    }
    public enum CannedReportFilterNewStatus : byte
    {
        //All = 0,
        [Description("All")]
        Status = 0,
        Upcoming,
        Overdue,
        [Description("Pending for Review")]
        PendingForReview,
        Rejected,
        ClosedTimely,
        ClosedDelayed
    }

    public enum RLCS_CannedReportFilterStatus : byte
    {
        //All = 0,
        [Description("All")]
        Status = 0,
        Upcoming,
        Overdue,
        [Description("Pending for Review")]
        PendingForReview,
        [Description("Closed-Timely")]
        ClosedTimely,
        [Description("Closed-Delayed")]
        ClosedDelayed
    }
     

    public enum DocumentFilterNewStatus : byte
    {
        //All = 0,
        [Description("All")]
        Status = 0,
        [Description("Pending for Review")]
        PendingForReview,
        Rejected,
        ClosedTimely,
        ClosedDelayed
    }

    public enum ManagementFilterNewStatus : byte
    {
        //All = 0,
        [Description("All")]
        Status = 0,      
        Overdue,
        [Description("Pending for Review")]
        PendingForReview,
        Rejected,
        ClosedTimely,
        ClosedDelayed
    }
    public enum CheckListReportFilterForPerformerCompletedNotCompleted : byte
    {
        All = 0,
        NotCompleted,
        Completed,
        Overdue     
    }
    public enum CheckListReportFilterForPerformer : byte
    {        
        Assigned = 0,
        NotAssigned
         
    }

    public enum EventFrequency : byte
    {
        Monthly = 0,
        Quarterly,
        HalfYearly,
        Annual,
        FourMonthly,
        NA = 7,
    }

    public enum EventCannedReportFilter : byte
    {
        //[Description("Event Not Assiged")]
        //EventNotAssiged = 0,
        [Description("Compliance Assiged")]
        ComplianceAssiged,
        //[Description("Event And Compliance Not Assiged")]
        //EventAndComplianceNotAssiged,
    }

    public enum ComplianceCannedReportFilter : byte
    {
        [Description("Assiged Compliances")]
         AssigedCompliances = 0,
        [Description("Not Assiged Compliances")]
        NotAssigedCompliances,
    }

    public enum CannedReportFilterForReviewer : byte
    {
        //All = 0,
        Status =0,
        [Description("Pending for Review")]
        PendingForReview,
        Delayed,
        Open,
        Rejected
    }

    public enum CannedReportFilterForApprover : byte
    {
        All = 0,
        [Description("Per Entity, By Category")]
        EntityByCategory,
        [Description("Per Category, By Entity")]
        CategoryByEntity,
        [Description("Per Risk, By Entity")]
        RiskByEntity,
        [Description("For Final Approval")]
        ForFinalApproval,
        [Description("Consecutive Defaulters")]
        ConsecutiveDefaulters
    }

    public enum RiskType : byte
    {
        High = 0,
        Medium,
        Low
    }

    public enum PerformanceSummaryForPerformer : byte
    {
        Reportee=0,
        Risk,
        Category,
        Location,
        
    }
    public enum InternalPerformanceSummaryForPerformer : byte
    {
        Reportee = 0,
        Risk,
        Category,
        Location,

    }
    public enum CustomerStatus : byte
    {
        Inactive= 0,
        Active,
        Suspended
    }

    public enum RLCSReviewerStatus : byte
    {        
        Upcoming,
        Overdue,
        [Description("Pending for Review")]
        PendingForReview,
        [Description("Closed-Timely")]
        ClosedTimely,
        [Description("Closed-Delayed")]
        ClosedDelayed,
    }
    public enum AuditRole : byte
    {
        Executive = 1,
        Manager = 2,
        //AuditHead = 3,
        Partner = 4,
    }
    public static class Enumerations
    {
        public static List<NameValue> GetAll<EnumType>() where EnumType : struct
        {
            var enumerationType = typeof(EnumType);

            if (!enumerationType.IsEnum)
                throw new ArgumentException("Enumeration type is expected.");

            var dictionary = new List<NameValue>();

            foreach (var value in Enum.GetValues(enumerationType))
            {
                var memInfo = value.GetType().GetMember(value.ToString());
                var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                var description = attributes.Length > 0 ? ((DescriptionAttribute)attributes[0]).Value : value.ToString();

                dictionary.Add(new NameValue() { ID = (byte)value, Name = description });
            }


            if (enumerationType.Name == "Month" || enumerationType.Name.Equals("PerformanceSummaryForPerformer") || enumerationType.Name.Equals("CustomerStatus") || enumerationType.Name.Equals("CheckListReportFilterForPerformer"))
                return dictionary.ToList();
            else
                return dictionary.OrderBy(entry=>entry.Name).ToList();
        }
        
        // added by sudarsha for excel Utility
        public static int GetEnumByName<EnumType>(string name) where EnumType : struct
        {
            var enumerationType = typeof(EnumType);

            if (!enumerationType.IsEnum)
                throw new ArgumentException("Enumeration type is expected.");

            var dictionary = new List<NameValue>();

            foreach (var value in Enum.GetValues(enumerationType))
            {
                dictionary.Add(new NameValue() { ID = (byte)value, Name = value.ToString() });
            }
            var id = (from en in dictionary
                      where en.Name.Equals(name.Trim())
                      select en.ID).FirstOrDefault();

            return id;
        }

        public static string GetEnumByID<EnumType>(int id) where EnumType : struct
        {
            var enumerationType = typeof(EnumType);

            if (!enumerationType.IsEnum)
                throw new ArgumentException("Enumeration type is expected.");

            var dictionary = new List<NameValue>();

            foreach (var value in Enum.GetValues(enumerationType))
            {
                dictionary.Add(new NameValue() { ID = (byte)value, Name = value.ToString() });
            }
            var typeName = (from en in dictionary
                           where en.ID == id
                           select en.Name).FirstOrDefault();

            return typeName;
        }

    }
}