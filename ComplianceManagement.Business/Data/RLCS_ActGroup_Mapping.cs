//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RLCS_ActGroup_Mapping
    {
        public long ID { get; set; }
        public Nullable<int> AVACOM_ActID { get; set; }
        public Nullable<int> Litigation_ActID { get; set; }
        public string RS_ActID { get; set; }
        public string RS_ActGroup { get; set; }
        public string RS_ActName { get; set; }
        public string RS_PDDisplayName { get; set; }
        public string RS_Status { get; set; }
        public string RS_Registers { get; set; }
        public string RS_Returns { get; set; }
        public string RS_Challans { get; set; }
        public Nullable<int> RS_Rank { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
