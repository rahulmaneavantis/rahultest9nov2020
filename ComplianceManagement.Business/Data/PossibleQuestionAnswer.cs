//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class PossibleQuestionAnswer
    {
        public long ID { get; set; }
        public Nullable<long> QuestionId { get; set; }
        public Nullable<long> ControllID { get; set; }
        public string Answer { get; set; }
    }
}
