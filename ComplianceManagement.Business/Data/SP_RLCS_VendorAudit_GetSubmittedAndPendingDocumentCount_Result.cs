//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_RLCS_VendorAudit_GetSubmittedAndPendingDocumentCount_Result
    {
        public string VendorName { get; set; }
        public int AuditID { get; set; }
        public int CustomerID { get; set; }
        public int CustomerBranchID { get; set; }
        public Nullable<long> ScheduledOnID { get; set; }
        public string Branch { get; set; }
        public string ForMonth { get; set; }
        public int Total { get; set; }
        public int Open { get; set; }
        public int Submitted { get; set; }
        public int Team_Review { get; set; }
        public int Closed { get; set; }
    }
}
