//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_LIC_GetUserLicenseTypeMappingData_Result
    {
        public Nullable<long> ID { get; set; }
        public Nullable<long> UserID { get; set; }
        public long LicenseTypeID { get; set; }
        public string LicenseTypeName { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
