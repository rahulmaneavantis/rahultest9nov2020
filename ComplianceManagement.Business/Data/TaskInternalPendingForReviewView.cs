//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaskInternalPendingForReviewView
    {
        public long TaskID { get; set; }
        public string Branch { get; set; }
        public long TaskInstanceID { get; set; }
        public Nullable<int> TaskStatusID { get; set; }
        public long TaskTransactionID { get; set; }
        public int CustomerBranchID { get; set; }
        public int CustomerID { get; set; }
        public string ForMonth { get; set; }
        public string Role { get; set; }
        public int RoleID { get; set; }
        public System.DateTime ScheduledOn { get; set; }
        public long TaskScheduledOnID { get; set; }
        public string TaskTitle { get; set; }
        public string IShortDescription { get; set; }
        public System.DateTime newScheduledOn { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> StatusChangedOn { get; set; }
        public string User { get; set; }
        public long UserID { get; set; }
        public Nullable<long> ReportingToID { get; set; }
    }
}
