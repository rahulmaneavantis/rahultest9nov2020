﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    public class ComplianceInfo
    {

    }
    public class LinkAllFilterData
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
    public class ShareDetail
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public long UserPermissionFileId { get; set; }
        public string FileType { get; set; }
    }
    public class ComplianceList
    {
        public long ID { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<bool> EventFlag { get; set; }
        public string ShortDescription { get; set; }
        public int ActID { get; set; }
    }
    public class ComplianceCannedReport
    {
        public Nullable<long> ComplianceInstanceID { get; set; }
        public int? CustomerBranchID { get; set; }
        public string ComplianceTypeName { get; set; }
        public string State { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public string ActName { get; set; }
        public string Section { get; set; }
        public DateTime? ScheduledOn { get; set; }
        public int ScheduledOnID { get; set; }
        public string ShortDescription { get; set; }
        public string CustomerBranchName { get; set; }
        public int ComplianceStatusID { get; set; }
        public string Performer { get; set; }
        public string Reviewer { get; set; }
        public string Approver { get; set; }
        public byte? Risk { get; set; }
        public string ForMonth { get; set; }
        public long ComplianceID { get; set; }
        public long PerformerID { get; set; }
        public long ReviewerID { get; set; }
        public long ApproverID { get; set; }

    }



    public class ComplianceCannedReportNew
    {
        public long ActID { get; set; }
        public string ActName { get; set; }
        public int? ComplianceTypeID { get; set; }
        public long ComplianceInstanceID { get; set; }
        public int? ComplianceCategoryID { get; set; }
        public int? ComplianceStatusID { get; set; }
        public long ScheduledOnID { get; set; }
        public string Status { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public int CustomerID { get; set; }
        public int CustomerBranchID { get; set; }
        public string CustomerBranchName { get; set; }
        public DateTime? ScheduledOn { get; set; }
        public string ForMonth { get; set; }
        public byte? RiskID { get; set; }
        public String Risk { get; set; }

        public bool? IsActive { get; set; }
        public bool? IsUpcomingNotDeleted { get; set; }

        public int PerformerRoleID { get; set; }
        public int ReviewerRoleID { get; set; }

        public int RoleID { get; set; }
        public long UserID { get; set; }

        public string Performer { get; set; }
        public string Reviewer { get; set; }
        public string Approver { get; set; }

        public long PerformerID { get; set; }
        public long ReviewerID { get; set; }
        public long ApproverID { get; set; }
    }


    public class DashboardDataDisplayClass
    {
        public string ShortDescription { get; set; }
        public string ActName { get; set; }
        public string PenaltyStatus { get; set; }
        public long ActID { get; set; }
        public int CustomerBranchID { get; set; }
        public long ComplianceInstanceID { get; set; }
        public long ComplianceID { get; set; }
        public Nullable<int> ComplianceStatusID { get; set; }
        public System.DateTime ScheduledOn { get; set; }
        public Nullable<byte> Risk { get; set; }
        public long ScheduledOnID { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public string User { get; set; }
        public Nullable<int> ComplianceCategoryId { get; set; }
        public Nullable<decimal> Penalty { get; set; }
        public Nullable<decimal> Interest { get; set; }
        public Nullable<decimal> totalvalue { get; set; }        
    }

    public class DashboardPenaltyDataDisplayClass
    {
        public int? ComplianceCategoryID { get; set; }
        public string PenaltyStatus { get; set; }
        public string Branch { get; set; }
        public int CustomerBranchID { get; set; }
        public int Status { get; set; }
        public int ComplianceTypeID { get; set; }
        public long ComplianceID { get; set; }
        public long ActID { get; set; }
        public string ActName { get; set; }
        public string Section { get; set; }
        public string ShortDescription { get; set; }
        public string User { get; set; }
        public decimal Penalty { get; set; }
        public decimal Interest { get; set; }
        public System.DateTime ScheduledOn { get; set; }

    }

    public class EscalationClass
    {
        
        public string Frequency { get; set; }
        public long complianceID { get; set; }
        public string Branch { get; set; }
        public string ShortDescription { get; set; }
        public int? DueDate { get; set; }
        public int? IntreimDays { get; set; }
        public int? EscDays { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public Boolean? EventFlag { get; set; }
        public long ActID { get; set; }
        public string ActName { get; set; }
        public string User { get; set; }
        public int RiskType { get; set; }
    }
    public class PenaltyDataDisplayClass
    {
        public int? ComplianceCategoryID { get; set; }
        public long UserID { get; set; }
        public int RiskType { get; set; }
        public long ComplianceTransactionID { get; set; }
        public int ComplianceStatusID { get; set; }
        public long ComplianceInstanceID { get; set; }
        public long ComplianceScheduledID { get; set; }
        public string Branch { get; set; }
        public int CustomerBranchID { get; set; }
        public string Status { get; set; }
        public int ComplianceTypeID { get; set; }
        public long ComplianceID { get; set; }
        public long ActID { get; set; }
        public string ActName { get; set; }
        public string Section { get; set; }
        public string ShortDescription { get; set; }
        public string User { get; set; }
        public decimal? Penalty { get; set; }
        public System.DateTime ScheduledOn { get; set; }

    }

    public class ManagmentRemindClass
    {

        public string Frequency { get; set; }
        public long complianceID { get; set; }
        public string Branch { get; set; }
        public string ShortDescription { get; set; }
        public int? DueDate { get; set; }
        public string RemindFlag { get; set; }
        public string UserFlag { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public Boolean? EventFlag { get; set; }
        public long ActID { get; set; }
        public string ActName { get; set; }
        public string User { get; set; }
        public int RiskType { get; set; }
    }
}
