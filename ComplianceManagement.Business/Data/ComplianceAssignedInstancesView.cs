//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ComplianceAssignedInstancesView
    {
        public int ComplianceAssignmentID { get; set; }
        public long ComplianceInstanceID { get; set; }
        public long ComplianceID { get; set; }
        public string ShortDescription { get; set; }
        public int CustomerBranchID { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public string Section { get; set; }
        public string Description { get; set; }
        public int CustomerID { get; set; }
        public string Branch { get; set; }
        public string Role { get; set; }
        public string User { get; set; }
        public int ActID { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ComplianceTypeName { get; set; }
        public System.DateTime ScheduledOn { get; set; }
        public byte ComplianceType { get; set; }
        public Nullable<bool> EventFlag { get; set; }
        public Nullable<int> ComplianceCategoryId { get; set; }
        public int ComplianceTypeID { get; set; }
        public Nullable<byte> RiskType { get; set; }
        public Nullable<byte> Frequency { get; set; }
        public Nullable<int> DueDate { get; set; }
        public System.DateTime StartDate { get; set; }
        public string Label { get; set; }
    }
}
