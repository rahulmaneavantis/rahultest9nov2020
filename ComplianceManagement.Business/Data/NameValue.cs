﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    public class NameValue
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class TaskValue
    {
        public int ID { get; set; }
        public int ParentID { get; set; }
    }
}
