//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class sp_GetLawyerDetails_Notice_Result
    {
        public Nullable<long> SLNo { get; set; }
        public string LawyerName { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public string Specialisation { get; set; }
        public Nullable<decimal> Rating { get; set; }
        public Nullable<int> OwnerID { get; set; }
        public int AssignedUser { get; set; }
        public int RoleID { get; set; }
        public Nullable<int> NoticeCreatedBy { get; set; }
    }
}
