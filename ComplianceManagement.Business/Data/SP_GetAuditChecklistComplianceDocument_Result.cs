//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_GetAuditChecklistComplianceDocument_Result
    {
        public long ID { get; set; }
        public long FileID { get; set; }
        public long TransactionID { get; set; }
        public Nullable<long> ScheduledOnID { get; set; }
        public string FileName { get; set; }
        public Nullable<int> FileType { get; set; }
        public string FilePath { get; set; }
        public string FileKey { get; set; }
        public string Version { get; set; }
        public Nullable<System.DateTime> VersionDate { get; set; }
        public string VersionComment { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<long> ComplianceInstanceID { get; set; }
        public string ForMonth { get; set; }
        public System.DateTime ScheduleOn { get; set; }
        public int BranchID { get; set; }
        public string Branch { get; set; }
        public string ShortDescription { get; set; }
        public long ComplianceID { get; set; }
        public long ChecklistID { get; set; }
        public long ChecklistMappingID { get; set; }
        public long AuditDetailID { get; set; }
        public int CustomerID { get; set; }
        public string DocumentType { get; set; }
        public string EnType { get; set; }
        public string CreatedByText { get; set; }
        public Nullable<System.DateTime> Dated { get; set; }
    }
}
