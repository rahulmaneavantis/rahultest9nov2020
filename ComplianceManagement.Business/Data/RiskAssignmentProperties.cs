﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{


    [Serializable()]
    public class RiskAssignmentProperties
    {
        private int riskcreationid;
        private bool performer; 
        public int CustomerBranchID;
        public long ProcessId;
        public long SubProcessId;
        public int Riskcreationid
        {
            get { return riskcreationid; }
            set { riskcreationid = value; }
        }
        public bool Performer
        {
            get { return performer; }
            set { performer = value; }
        }
        public int CustomerBranchid
        {
            get { return CustomerBranchID; }
            set { CustomerBranchID = value; }
        }
        public long ProcessID
        {
            get { return ProcessId; }
            set { ProcessId = value; }
        }
        public long SubProcessID
        {
            get { return SubProcessId; }
            set { SubProcessId = value; }
        }
    }
}
