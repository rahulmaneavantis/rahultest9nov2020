﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    class InternalCompilanceInfo
    {
    }
    public class InternalComplianceCannedReport
    {
        public Nullable<long> InternalComplianceInstanceID { get; set; }
        public int? CustomerBranchID { get; set; }
        public Nullable<long> InternalComplianceID { get; set; }
        
        public string InternalComplianceTypeName { get; set; }      
        public Nullable<int> CustomerID { get; set; }
        public DateTime? InternalScheduledOn { get; set; }
        public int InternalScheduledOnID { get; set; }
        public string ShortDescription { get; set; }
        public string CustomerBranchName { get; set; }
        public int InternalComplianceStatusID { get; set; }
        public string IPerformer { get; set; }
        public string IReviewer { get; set; }
        public string IApprover { get; set; }
        public byte? Risk { get; set; }
        public string ForMonth { get; set; }

        public long IPerformerID { get; set; }
        public long IReviewerID { get; set; }
        public long IApproverID { get; set; }

    }
    public class InternalComplianceCannedReportNew
    {
        public long? InternalComplianceID { get; set; }
        public int? InternalComplianceTypeID { get; set; }
        public int? InternalComplianceCategoryID { get; set; }
        public long InternalComplianceInstanceID { get; set; }
        public int? InternalComplianceStatusID { get; set; }
        public string Status { get; set; }
        public int? CustomerBranchID { get; set; }
        public string InternalComplianceTypeName { get; set; }
        public int? CustomerID { get; set; }
        public DateTime? InternalScheduledOn { get; set; }
        public Nullable<long> InternalScheduledOnID { get; set; }
        public string ForMonth { get; set; }
        public string ShortDescription { get; set; }
        public string CustomerBranchName { get; set; }
        public string IPerformer { get; set; }
        public string IReviewer { get; set; }
        public string IApprover { get; set; }
        public byte? RiskID { get; set; }
        public string Risk { get; set; }
        public Nullable<int> IPerformerRoleID { get; set; }
        public Nullable<int> IReviewerRoleID { get; set; }
        public Nullable<int> RoleID { get; set; }
        public Nullable<long> UserID { get; set; }
        public Nullable<long> IPerformerID { get; set; }
        public Nullable<long> IReviewerID { get; set; }
        public Nullable<long> IApproverID { get; set; }
    }
}
