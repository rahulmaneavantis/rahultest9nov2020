//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class sp_GetCaseDocuments_Result
    {
        public string CaseType { get; set; }
        public Nullable<System.DateTime> OpenDate { get; set; }
        public long ID { get; set; }
        public string InternalCaseNo { get; set; }
        public string CaseRefNo { get; set; }
        public string CaseTitle { get; set; }
        public string CaseDetailDesc { get; set; }
        public int CustomerBranchID { get; set; }
        public string CustomerBranch { get; set; }
        public string DocumentName { get; set; }
        public string CourtOrderDocument { get; set; }
        public string HearingDocument { get; set; }
        public int AssignedUser { get; set; }
        public int RoleID { get; set; }
        public Nullable<int> OwnerID { get; set; }
        public Nullable<int> CaseCreatedBy { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
    }
}
