﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    public class MatrixInformation
    {
        public string Row { get; set; }
        public string Column { get; set; }
        public int Count { get; set; }
    }
}
