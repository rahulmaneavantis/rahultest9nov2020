//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TEMP_RLCS_LWF_Bank_Details
    {
        public int LBD_ID { get; set; }
        public string LBD_ClientID { get; set; }
        public string LBD_StateID { get; set; }
        public string LBD_LocationID { get; set; }
        public string LBD_BranchName { get; set; }
        public Nullable<int> LBD_Year { get; set; }
        public string LBD_Cheque_No { get; set; }
        public string LBD_Bank_Name { get; set; }
        public Nullable<System.DateTime> LBD_Date { get; set; }
        public string LBD_Cheque_Favour { get; set; }
        public Nullable<int> LBD_Version { get; set; }
        public string LBD_Created_By { get; set; }
        public Nullable<System.DateTime> LBD_Created_Date { get; set; }
        public string LBD_Modified_By { get; set; }
        public Nullable<System.DateTime> LBD_Modified_Date { get; set; }
        public Nullable<int> user_id { get; set; }
        public Nullable<int> fileid { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<System.DateTime> deleted_at { get; set; }
        public Nullable<int> AVACOM_CustomerID { get; set; }
        public Nullable<int> AVACOM_BranchID { get; set; }
        public Nullable<bool> ISProcessed { get; set; }
        public Nullable<bool> ISActive { get; set; }
        public Nullable<bool> ISValidated { get; set; }
        public Nullable<int> excel_row_no { get; set; }
    }
}
