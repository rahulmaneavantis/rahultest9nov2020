//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class sp_ChangeShortDetailedDescriptionLog_Result
    {
        public string flag { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string NewShortDescripton { get; set; }
        public string NewDetailedDescription { get; set; }
        public Nullable<System.DateTime> Startdate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
    }
}
