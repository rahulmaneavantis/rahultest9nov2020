//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class Sp_Act_Document_Result
    {
        public int ID { get; set; }
        public long Act_ID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Version { get; set; }
        public Nullable<System.DateTime> VersionDate { get; set; }
        public Nullable<System.DateTime> Act_TypeVersionDate { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int Createdby { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public int Updatedby { get; set; }
        public string DocumentType { get; set; }
        public int DocumentTypeID { get; set; }
    }
}
