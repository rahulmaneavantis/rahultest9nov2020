//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class sp_CompliancesforMappedLocation_Result
    {
        public long ID { get; set; }
        public string Description { get; set; }
        public Nullable<bool> UploadDocument { get; set; }
        public Nullable<byte> Frequency { get; set; }
        public Nullable<int> DueDate { get; set; }
        public Nullable<byte> RiskType { get; set; }
        public Nullable<byte> NonComplianceType { get; set; }
        public string NonComplianceEffects { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public int ActID { get; set; }
        public string Sections { get; set; }
        public byte ComplianceType { get; set; }
        public Nullable<double> FixedMinimum { get; set; }
        public Nullable<double> FixedMaximum { get; set; }
        public Nullable<double> VariableAmountPerDay { get; set; }
        public Nullable<double> VariableAmountPerDayMax { get; set; }
        public Nullable<double> VariableAmountPercent { get; set; }
        public Nullable<double> VariableAmountPercentMax { get; set; }
        public Nullable<bool> Imprisonment { get; set; }
        public string Designation { get; set; }
        public Nullable<int> MinimumYears { get; set; }
        public Nullable<int> MaximumYears { get; set; }
        public string Others { get; set; }
        public string RequiredForms { get; set; }
        public Nullable<byte> NatureOfCompliance { get; set; }
        public Nullable<double> VariableAmountPerMonth { get; set; }
        public string ShortDescription { get; set; }
        public Nullable<long> EventID { get; set; }
        public Nullable<byte> EventComplianceType { get; set; }
        public Nullable<byte> SubComplianceType { get; set; }
        public Nullable<int> FixedGap { get; set; }
        public Nullable<byte> ReminderType { get; set; }
        public Nullable<int> ReminderBefore { get; set; }
        public Nullable<int> ReminderGap { get; set; }
        public Nullable<long> SubEventID { get; set; }
        public Nullable<int> CheckListTypeID { get; set; }
        public Nullable<System.DateTime> OneTimeDate { get; set; }
        public string PenaltyDescription { get; set; }
        public string ReferenceMaterialText { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<bool> EventFlag { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<bool> UpDocs { get; set; }
        public Nullable<bool> ComplinceVisible { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> DeactivateOn { get; set; }
        public string DeactivateDesc { get; set; }
        public string SampleFormLink { get; set; }
        public Nullable<int> ComplianceSubTypeID { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<byte> DueWeekDay { get; set; }
    }
}
