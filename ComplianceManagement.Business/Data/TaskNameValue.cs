﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    public class TaskNameValue
    {
        public long TaskID { get; set; }
        public int? ParentID { get; set; }
        public int ActualDueDays { get; set; }

    }
}
