//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class InternalComplianceTransactionView
    {
        public long ComplianceTransactionID { get; set; }
        public long InternalComplianceInstanceID { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public Nullable<long> InternalComplianceScheduledOnID { get; set; }
        public Nullable<int> ComplianceStatusID { get; set; }
        public Nullable<int> FileID { get; set; }
        public string Status { get; set; }
        public long CreatedBy { get; set; }
        public string Remarks { get; set; }
        public string Name { get; set; }
        public System.DateTime Dated { get; set; }
        public string CreatedByText { get; set; }
        public Nullable<System.DateTime> StatusChangedOn { get; set; }
    }
}
