//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class EventEscalationNotification
    {
        public long ID { get; set; }
        public Nullable<long> EventInstanceID { get; set; }
        public Nullable<long> UserID { get; set; }
        public Nullable<System.DateTime> SentOn { get; set; }
        public Nullable<System.DateTime> ClosedOn { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    
        public virtual EventInstance EventInstance { get; set; }
        public virtual User User { get; set; }
    }
}
