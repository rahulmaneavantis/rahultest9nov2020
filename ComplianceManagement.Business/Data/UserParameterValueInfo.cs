﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    public class UserParameterValueInfo
    {
        public long UserID { get; set; }
        public int ParameterID { get; set; }
        public int ValueID { get; set; }
        public string Name { get; set; }
        public DataType DataType { get; set; }
        public int Length { get; set; }
        public string Value { get; set; }
    }
}
