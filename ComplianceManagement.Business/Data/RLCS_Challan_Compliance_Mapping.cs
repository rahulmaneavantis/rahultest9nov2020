//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RLCS_Challan_Compliance_Mapping
    {
        public long ID { get; set; }
        public string ChallanType { get; set; }
        public string State_Code { get; set; }
        public Nullable<long> AVACOM_ComplianceID { get; set; }
        public bool IsActive { get; set; }
        public string Condition { get; set; }
        public Nullable<int> EmployeeLimit { get; set; }
        public Nullable<decimal> MinBasicSalaryLimit { get; set; }
        public Nullable<decimal> MinGrossSalaryLimit { get; set; }
        public string AVACOM_ActGroup { get; set; }
        public string AVACOM_ActType { get; set; }
        public Nullable<decimal> MaxBasicSalaryLimit { get; set; }
        public Nullable<decimal> MaxGrossSalaryLimit { get; set; }
    }
}
