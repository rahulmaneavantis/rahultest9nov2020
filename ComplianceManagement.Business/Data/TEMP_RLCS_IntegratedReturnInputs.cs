//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TEMP_RLCS_IntegratedReturnInputs
    {
        public long IRI_Id { get; set; }
        public string IRI_ClientID { get; set; }
        public int IRI_Year { get; set; }
        public string IRI_StateID { get; set; }
        public string IRI_LocationID { get; set; }
        public string IRI_BranchName { get; set; }
        public Nullable<int> NoOfContractorsEngaged { get; set; }
        public Nullable<int> NoOfMaleEmployee { get; set; }
        public Nullable<int> NoOfFemaleEmployee { get; set; }
        public Nullable<int> NoOfAdolescentEmployee { get; set; }
        public Nullable<int> NoOfChildEmployee { get; set; }
        public Nullable<int> TotalMandays { get; set; }
        public Nullable<int> MaxNoOfMaleEmployee { get; set; }
        public Nullable<int> MaxNoOfFemaleEmployee { get; set; }
        public Nullable<int> MaxNoOfAdolescentEmployee { get; set; }
        public Nullable<int> MaxNoOfChildEmployee { get; set; }
        public Nullable<int> NoOfPersonRetiredOnSuperannuation { get; set; }
        public Nullable<int> NoOfPersonRetrenched { get; set; }
        public Nullable<int> NoOfPersonDischarged { get; set; }
        public Nullable<int> ManDaysLostDueToStrike { get; set; }
        public Nullable<int> ManDaysLostDueToLockout { get; set; }
        public Nullable<int> NoOfWorkersProvidedAmbulanceFacility { get; set; }
        public Nullable<int> NoOfWorkersAvailedCanteenFacility { get; set; }
        public Nullable<int> NoOfRestRooms { get; set; }
        public Nullable<decimal> TotalAmountBonusPayable { get; set; }
        public string SettlementIfAnyReached { get; set; }
        public Nullable<decimal> PercentageBonusDeclared { get; set; }
        public Nullable<decimal> TotalBonusPaid { get; set; }
        public Nullable<System.DateTime> DateOfPaymentOfBonus { get; set; }
        public Nullable<decimal> UnpaidLWFAmount { get; set; }
        public Nullable<System.DateTime> DateOfChange { get; set; }
        public string InformationFurnishedAtTimeOfRegistration { get; set; }
        public string ChangedInformation { get; set; }
        public string IsworksCommitteeFuncitoningUnderIDAct { get; set; }
        public Nullable<System.DateTime> DateOfConstitutionUnderIDAct { get; set; }
        public Nullable<int> NoOfElectedMembersUnderIDAct { get; set; }
        public Nullable<int> NoOfNominatedMembersUnderIDAct { get; set; }
        public Nullable<int> UnderIDActNoOfMeetingHeld { get; set; }
        public Nullable<int> NoOfInterStateMigrantMaleWorkmenEmployed { get; set; }
        public Nullable<int> NoOfInterStateMigrantFemaleWorkmenEmployed { get; set; }
        public Nullable<int> NoOfInterStateMigrantAdolescentWorkmenEmployed { get; set; }
        public Nullable<int> NoOfInterStateMigrantChildWorkmenEmployed { get; set; }
        public Nullable<int> NoOfEmployeesEligibleForLabourWelfareFund { get; set; }
        public Nullable<decimal> EmployeesContributionForLabourWelfareFund { get; set; }
        public Nullable<decimal> EmployerContributionForLabourWelfareFund { get; set; }
        public Nullable<decimal> UnderLabourWelfareFundUnpaidAmount { get; set; }
        public string IsBonusPaidToAllEmployees { get; set; }
        public string ReasonsForNonPaymentOfBonusToAnyEmployee { get; set; }
        public Nullable<int> NoOfAccidentsLessThan48Hrs { get; set; }
        public Nullable<int> AcidentCategory_I_NoOfWorkerInvolvedInAccident { get; set; }
        public Nullable<int> AcidentCategory_I_NoOfMandaysLost { get; set; }
        public Nullable<int> NoOfAccidentsMoreThan48Hrs { get; set; }
        public Nullable<int> AcidentCategory_II_NoOfWorkerInvolvedInAccident { get; set; }
        public Nullable<int> AcidentCategory_II_NoOfMandaysLost { get; set; }
        public Nullable<int> NoOfAccidentsResultInPermanentPartial { get; set; }
        public Nullable<int> AcidentCategory_III_NoOfWorkerInvolvedInAccident { get; set; }
        public Nullable<int> AcidentCategory_III_NoOfMandaysLost { get; set; }
        public Nullable<int> NumberOfAccidentsResultInDeath { get; set; }
        public Nullable<int> NoOfMandaysLostResultingInDeath { get; set; }
        public Nullable<System.DateTime> UnderIDActMeetingHeldWithDates { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> Version { get; set; }
        public Nullable<double> Lossin_termsof_moneyfor_Strike { get; set; }
        public Nullable<double> Lossin_termsof_moneyfor_Lockout { get; set; }
        public Nullable<double> Lossin_termsof_moneyfor_FatalAccidents { get; set; }
        public Nullable<double> Lossin_termsof_moneyfor_Non_FatalAccidents { get; set; }
        public string committee_difficulties_encountered_constitution { get; set; }
        public Nullable<int> No_of_Unions_in_establishment { get; set; }
        public Nullable<double> Lossin_termsof_moneyfor_AnyotherLOP { get; set; }
        public Nullable<double> ManDaysLostDueToLay_Off { get; set; }
        public string Details_of_contractors_under_Inter_State_Migrant_Workmen_Act { get; set; }
        public string Details_of_building_or_other_construction_work { get; set; }
        public string Whether_covered_under_gratuity_insurance_scheme { get; set; }
        public string Detail_of_Casual_workers_Male_employees { get; set; }
        public string Detail_of_Casual_workers_Female_employees { get; set; }
        public string Details_of_Seasonal_workers_Male_employees { get; set; }
        public string Details_of_Seasonal_workers_Female_employees { get; set; }
        public string Detail_of_Badli_workers_Male_employees { get; set; }
        public string Detail_of_Badli_workers_Female_employees { get; set; }
        public Nullable<double> No_of_days_Layoff { get; set; }
        public Nullable<int> user_id { get; set; }
        public Nullable<int> fileid { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<System.DateTime> deleted_at { get; set; }
        public string Applicant_AadharNo { get; set; }
        public Nullable<double> Compensation_Amnt_Paid { get; set; }
        public string Canteen { get; set; }
        public string Creches { get; set; }
        public string RestRoomsLunchRooms { get; set; }
        public string TransportFacility { get; set; }
        public Nullable<int> NoofEmployee_Breachofcontract { get; set; }
        public Nullable<double> Amntfor_Breachofcontract { get; set; }
        public Nullable<int> AVACOM_CustomerID { get; set; }
        public Nullable<int> AVACOM_BranchID { get; set; }
        public Nullable<bool> ISProcessed { get; set; }
        public Nullable<bool> ISActive { get; set; }
        public Nullable<bool> ISValidated { get; set; }
        public Nullable<int> excel_row_no { get; set; }
    }
}
