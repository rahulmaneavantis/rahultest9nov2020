//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class sp_GetServiceProviderAgainstCustomerLimit_Result
    {
        public Nullable<int> ServiceProviderID { get; set; }
        public string ServiceProvider { get; set; }
        public string ContactNumber { get; set; }
        public string BuyerEmail { get; set; }
        public string BuyerName { get; set; }
        public int TotalCustomers { get; set; }
        public int TotalBranches { get; set; }
        public int TotalUsers { get; set; }
        public int ParentBranchLimit { get; set; }
    }
}
