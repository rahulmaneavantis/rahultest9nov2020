﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Threading;
using System.Net.Mail;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class AuditKickOff_NewDetails
    {
        public static object FillLocationdata(long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_CustomerBranch
                             where row.CustomerID == CustomerID
                             && row.IsDeleted == false && row.Status == 1
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillApplytoLowestDDL(int branchid, int customerID, int lowestBrachId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_CustomerBranch
                             where row.CustomerID == customerID && row.ParentID == branchid
                             && row.IsDeleted == false && row.Status == 1
                             select row);

                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;

            }
        }
        public static object FillFnancialYear()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.Mst_FinancialYear
                             select row).Distinct();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.FinancialYear }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillNonAdminUsers(int Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_User
                             where row.IsDeleted == false && row.IsActive == true
                             && row.RoleID == 7 && row.CustomerID == Customerid
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillLegalEntityData(long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_CustomerBranch
                             where row.CustomerID == CustomerID &&
                             row.IsDeleted == false && row.ParentID == null && row.Status == 1
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillSubEntityDataScheduleingApplyTO(long Branchid, long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                // List<mst_CustomerBranch> Frequency = new List<mst_CustomerBranch>();
                var query = (from row in entities.mst_CustomerBranch
                             where row.CustomerID == CustomerID && row.ID == Branchid
                             && row.IsDeleted == false && row.Status == 1
                             select row.ParentID).FirstOrDefault(); ;
                if (query != null)
                {
                    var querydetails = (from row in entities.mst_CustomerBranch
                                        where row.CustomerID == CustomerID && row.ParentID == query
                                        && row.IsDeleted == false && row.Status == 1
                                        select row);
                    querydetails = querydetails.Where(a => a.ID != Branchid);

                    var Frequency = (from row in querydetails
                                     select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return Frequency;
                }
                else
                {
                    return null;
                }
            }
        }
        public static object FillSubEntityData(long ParentID, long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.CustomerID == CustomerID && row.ParentID == ParentID
                             && row.IsDeleted == false && row.Status == 1
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        public static object FillSubEntityDataList(List<long> Parentlist, long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.CustomerID == CustomerID && Parentlist.Contains((long)row.ParentID)
                             && row.IsDeleted == false && row.Status == 1
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }
        // 25 Jan 2018

        #region AuditManager

        public static List<AuditManagerClass> AuditManagerFillSubEntityData(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();
                var query = (from row in entities.SP_BindRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;
                }
                else
                {

                    return null;
                }
            }
        }
        public static object AuditManagerFillSubEntityData1(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<object> mainFrequency = new List<object>();
                List<object> Frequency = new List<object>();
                var query = (from row in entities.SP_BindRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new { ID = row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new { ID = row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    return mainFrequency.Distinct().ToList();
                }
                else
                {

                    return null;
                }
            }
        }
        public static List<AuditManagerClass> AuditManagerFillSubEntityDataList(int UserID, int CustomerID, List<long> ParentID)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();
                var query = (from row in entities.SP_BindRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID.Count > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null &&
                                     ParentID.Contains((long)row.SecondParentID)
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && ParentID.Contains((long)row.ThirdParentID)
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && ParentID.Contains((long)row.FourthParentID)
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && ParentID.Contains((long)row.FifthParentID)
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;
                }
                else
                {

                    return null;
                }
            }
        }
        public static object AuditManagerFillSubEntityDataList1(int UserID, int CustomerID, List<long> ParentID)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<object> mainFrequency = new List<object>();
                List<object> Frequency = new List<object>();
                var query = (from row in entities.SP_BindRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID.Count > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null &&
                                     ParentID.Contains((long)row.SecondParentID)
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && ParentID.Contains((long)row.ThirdParentID)
                                 select new { ID = row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && ParentID.Contains((long)row.FourthParentID)
                                 select new { ID = row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && ParentID.Contains((long)row.FifthParentID)
                                 select new { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    return mainFrequency.Distinct().ToList();
                }
                else
                {

                    return null;
                }
            }
        }
        #endregion

        #region Management
        public static List<AuditManagerClass> ManagementFillSubEntityData(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();
                var query = (from row in entities.SP_BindManagementRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;
                }
                else
                {

                    return null;
                }
            }
        }
        public static object ManagementFillSubEntityData1(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<object> mainFrequency = new List<object>();
                List<object> Frequency = new List<object>();
                var query = (from row in entities.SP_BindManagementRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new { ID = row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new { ID = row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    return mainFrequency.Distinct().ToList();
                }
                else
                {

                    return null;
                }
            }
        }
        public static List<AuditManagerClass> ManagementFillSubEntityDataList(int UserID, int CustomerID, List<long> ParentID)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();
                var query = (from row in entities.SP_BindManagementRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID.Count > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null &&
                                     ParentID.Contains((long)row.SecondParentID)
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && ParentID.Contains((long)row.ThirdParentID)
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && ParentID.Contains((long)row.FourthParentID)
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && ParentID.Contains((long)row.FifthParentID)
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;
                }
                else
                {

                    return null;
                }
            }
        }
        public static object ManagementFillSubEntityDataList1(int UserID, int CustomerID, List<long> ParentID)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<object> mainFrequency = new List<object>();
                List<object> Frequency = new List<object>();
                var query = (from row in entities.SP_BindManagementRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID.Count > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null &&
                                     ParentID.Contains((long)row.SecondParentID)
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && ParentID.Contains((long)row.ThirdParentID)
                                 select new { ID = row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && ParentID.Contains((long)row.FourthParentID)
                                 select new { ID = row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && ParentID.Contains((long)row.FifthParentID)
                                 select new { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    return mainFrequency.Distinct().ToList();
                }
                else
                {

                    return null;
                }
            }
        }
        #endregion

        #region PersonResponsible
        public static List<AuditManagerClass> PersonResponsibleFillSubEntityData(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();
                var query = (from row in entities.SP_BindRestrictedDropDownPersonResponsible(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;
                }
                else
                {

                    return null;
                }
            }
        }
        public static object PersonResponsibleFillSubEntityData1(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<object> mainFrequency = new List<object>();
                List<object> Frequency = new List<object>();
                var query = (from row in entities.SP_BindRestrictedDropDownPersonResponsible(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new { ID = row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new { ID = row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    return mainFrequency.Distinct().ToList();
                }
                else
                {

                    return null;
                }
            }
        }
        public static List<AuditManagerClass> PersonResponsibleFillSubEntityDataICFR(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();

                var query = (from row in entities.SP_BindRestrictedDropDownPersonResponsibleICFR(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;
                }
                else
                {

                    return null;
                }
            }
        }

        public static object PersonResponsibleFillSubEntityDataICFR1(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<object> mainFrequency = new List<object>();
                List<object> Frequency = new List<object>();
                var query = (from row in entities.SP_BindRestrictedDropDownPersonResponsibleICFR(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new { ID = row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new { ID = row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    return mainFrequency.Distinct().ToList();
                }
                else
                {

                    return null;
                }
            }
        }
        public static List<AuditManagerClass> PersonResponsibleFillSubEntityDataICFR_PR(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();

                var query = (from row in entities.SP_BindRestrictedDropDownPersonResponsibleICFR_PR(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;
                }
                else
                {

                    return null;
                }
            }
        }

        public static object PersonResponsibleFillSubEntityDataICFR_PR1(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<object> mainFrequency = new List<object>();
                List<object> Frequency = new List<object>();
                var query = (from row in entities.SP_BindRestrictedDropDownPersonResponsibleICFR_PR(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new { ID = row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new { ID = row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    return mainFrequency.Distinct().ToList();
                }
                else
                {

                    return null;
                }
            }
        }
        #endregion

        #region Performer And Reviewer
        public static List<AuditManagerClass> PerformerReviewerFillSubEntityDataICFR(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();

                var query = (from row in entities.SP_BindRestrictedDropDownPerformerReviewerICFR(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    Frequency = (from row in query
                                 where row.SecondParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID == ParentID
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;
                }
                else
                {

                    return null;
                }
            }
        }
        public static object PerformerReviewerFillSubEntityDataICFR1(int UserID, int CustomerID, int ParentID)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<object> mainFrequency = new List<object>();
                List<object> Frequency = new List<object>();
                var query = (from row in entities.SP_BindRestrictedDropDownPerformerReviewerICFR(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    Frequency = (from row in query
                                 where row.SecondParentID == ParentID
                                 select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID == ParentID
                                 select new { ID = row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID == ParentID
                                 select new { ID = row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID == ParentID
                                 select new { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    return mainFrequency.Distinct().ToList();
                }
                else
                {

                    return null;
                }
            }
        }
        public static List<AuditManagerClass> PerformerReviewerFillSubEntityData(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();

                var query = (from row in entities.SP_BindRestrictedDropDownPerformerReviewer(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    Frequency = (from row in query
                                 where row.SecondParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID == ParentID
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;

                }
                else
                {

                    return null;
                }
            }
        }

        public static object PerformerReviewerFillSubEntityData1(int UserID, int CustomerID, int ParentID)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<object> mainFrequency = new List<object>();
                List<object> Frequency = new List<object>();
                var query = (from row in entities.SP_BindRestrictedDropDownPerformerReviewer(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    Frequency = (from row in query
                                 where row.SecondParentID == ParentID
                                 select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID == ParentID
                                 select new { ID = row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID == ParentID
                                 select new { ID = row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID == ParentID
                                 select new { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    return mainFrequency.Distinct().ToList();
                }
                else
                {

                    return null;
                }
            }
        }
        #endregion

        #region Department Head
        public static List<AuditManagerClass> DepartmentHeadFillSubEntityData(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();

                var query = (from row in entities.SP_BindDepartmentHeadRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;

                }
                else
                {

                    return null;
                }
            }
        }
        public static object DepartmentHeadFillSubEntityData1(int UserID, int CustomerID, int ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<object> mainFrequency = new List<object>();
                List<object> Frequency = new List<object>();
                var query = (from row in entities.SP_BindDepartmentHeadRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null && row.SecondParentID == ParentID
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && row.ThirdParentID == ParentID
                                 select new { ID = row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && row.FourthParentID == ParentID
                                 select new { ID = row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && row.FifthParentID == ParentID
                                 select new { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    return mainFrequency.Distinct().ToList();
                }
                else
                {

                    return null;
                }
            }
        }
        public static List<AuditManagerClass> DepartmentHeadFillSubEntityDataList(int UserID, int CustomerID, List<long> ParentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();
                var query = (from row in entities.SP_BindDepartmentHeadRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID.Count > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null &&
                                     ParentID.Contains((long)row.SecondParentID)
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && ParentID.Contains((long)row.ThirdParentID)
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && ParentID.Contains((long)row.FourthParentID)
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && ParentID.Contains((long)row.FifthParentID)
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;
                }
                else
                {

                    return null;
                }
            }
        }

        public static object DepartmentHeadFillSubEntityDataList1(int UserID, int CustomerID, List<long> ParentID)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<object> mainFrequency = new List<object>();
                List<object> Frequency = new List<object>();
                var query = (from row in entities.SP_BindDepartmentHeadRestrictedDropDown(UserID, CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID.Count > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null &&
                                     ParentID.Contains((long)row.SecondParentID)
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new { ID = row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).ToList<object>();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && ParentID.Contains((long)row.ThirdParentID)
                                 select new { ID = row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && ParentID.Contains((long)row.FourthParentID)
                                 select new { ID = row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && ParentID.Contains((long)row.FifthParentID)
                                 select new { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).ToList<object>();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    return mainFrequency.Distinct().ToList();
                }
                else
                {

                    return null;
                }
            }
        }


        // added on 15-06-2020
        public static List<mst_CustomerBranch> GetBranchAgainstCustomerForIFC(long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_CustomerBranch
                             where row.CustomerID == CustomerID &&
                             row.IsDeleted == false && row.ParentID == null && row.Status == 1
                             select row).OrderBy(entry => entry.Name).ToList();
                if (query.Count > 0)
                {
                    return query;
                }
                else
                {
                    return null;
                }
            }
        }

        #region added on 17-09-2020
        public static object FillLegalEntityDataForDashBoard(List<int?> CustomerIDList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_CustomerBranch
                             where CustomerIDList.Contains(row.CustomerID) &&
                             row.IsDeleted == false && row.ParentID == null && row.Status == 1
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }


        public static List<AuditManagerClass> AuditManagerFillSubEntityDataListForDashBoard(int UserID, List<int> CustomerIDsList, List<long> ParentID)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();
                var query = (from row in entities.SP_BindRestrictedDropDownForAuditHeadDashBoard(UserID)
                             where CustomerIDsList.Contains(row.CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    if (ParentID.Count > 0)
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null &&
                                     ParentID.Contains((long)row.SecondParentID)
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    else
                    {
                        Frequency = (from row in query
                                     where row.SecondParentID != null
                                     select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();
                    }
                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.ThirdParentID != null && ParentID.Contains((long)row.ThirdParentID)
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FourthParentID != null && ParentID.Contains((long)row.FourthParentID)
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where row.FifthParentID != null && ParentID.Contains((long)row.FifthParentID)
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;
                }
                else
                {

                    return null;
                }
            }
        }

        public static List<AuditManagerClass> PerformerReviewerFillSubEntityDataForPerformerMultiSelect(int UserID, List<int?> CustomerIDList, List<int?> ParentIDList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditManagerClass> mainFrequency = new List<AuditManagerClass>();
                List<AuditManagerClass> Frequency = new List<AuditManagerClass>();

                var query = (from row in entities.SP_BindRestrictedDropDownPerformerReviewerForMultiSelect(UserID)
                             where CustomerIDList.Contains(row.CustomerID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    Frequency = (from row in query
                                 where ParentIDList.Contains(row.SecondParentID)
                                 select new AuditManagerClass { ID = (int)row.SecondID, Name = row.SecondParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where ParentIDList.Contains(row.ThirdParentID)
                                 select new AuditManagerClass { ID = (int)row.ThirdID, Name = row.ThirdParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where ParentIDList.Contains(row.FourthParentID)
                                 select new AuditManagerClass { ID = (int)row.FourthID, Name = row.FourthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }

                    Frequency = (from row in query
                                 where ParentIDList.Contains(row.FifthParentID)
                                 select new AuditManagerClass { ID = row.FifthID, Name = row.FifthParentName }).OrderBy(entry => entry.Name).Distinct().ToList();

                    if (Frequency.Count > 0)
                    {
                        foreach (var item in Frequency)
                        {
                            mainFrequency.Add(item);
                        }
                    }
                    mainFrequency = mainFrequency.GroupBy(elem => elem.ID).Select(group => group.First()).ToList();
                    return mainFrequency;

                }
                else
                {

                    return null;
                }
            }
        }

        #endregion
        #endregion
    }
}
