﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ObservationsDetails
    {
        public int id { get; set; }
        public string obsName { get; set; }
        public int ObscatID { get; set; }        
        public string Name { get; set; }     
        public bool? obsIsActive { get; set; }

    }
}
