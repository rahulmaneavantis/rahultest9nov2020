﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ClientComplianceDeptMap
    {
        public int CustomerId { get; set; }
        public long ComplianceID { get; set; }

        public long DepartmentID { get; set; }
        public string ShortDescription { get; set; }
        public string CustomerName { get; set; }
        public string DepartmentName { get; set; }

    }
    public class ClientComplianceMap
    {
        public int CustomerId { get; set; }
        public string KeyID { get; set; }
        public long ComplianceID { get; set; }
        public string ShortDescription { get; set; }
        public string CustomerName { get; set; }
        public string Type { get; set; }

    }

    public class ClientEventMap
    {
        public int CustomerId { get; set; }
        public string KeyID { get; set; }
        public long EventID { get; set; }
        public string EventName { get; set; }
        public string CustomerName { get; set; }

    }
}
