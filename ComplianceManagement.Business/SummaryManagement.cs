﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class SummaryManagement
    {
        public static List<object> GetUsersPerCustomerSummaryForSuperAdmin()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers
                                 where row.IsDeleted == false
                                 select new { row.ID, row.Name }).ToList();

                var usersPerCustomer = (from row in entities.Users
                                        where row.IsDeleted == false && row.CustomerID.HasValue
                                        group row by row.CustomerID.Value into grp
                                        select new { ID = grp.Key, Count = grp.Count() }).ToList();

                var summaryList = (from customerRow in customers
                                   join countRow in usersPerCustomer on customerRow.ID equals countRow.ID into summary
                                   from summaryRow in summary.DefaultIfEmpty()
                                   select new { Name = customerRow.Name, Quantity = (summaryRow != null ? summaryRow.Count : 0) }).ToList<object>();
                //select new { customerRow, summaryRow }).ToList<object>();

                return summaryList;
            }
        }

        public static object GetEntitiesPerCustomersSummaryForSuperAdmin()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers
                                 where row.IsDeleted == false
                                 select new { row.ID, row.Name }).ToList();

                var entitiesPerCustomer = (from row in entities.CustomerBranches
                                           where row.IsDeleted == false
                                           group row by row.CustomerID into grp
                                           select new { ID = grp.Key, Count = grp.Count() }).ToList();

                var summaryList = (from customerRow in customers
                                   join countRow in entitiesPerCustomer on customerRow.ID equals countRow.ID into summary
                                   from summaryRow in summary.DefaultIfEmpty()
                                   select new { Name = customerRow.Name, Quantity = (summaryRow != null ? summaryRow.Count : 0) }).ToList<object>();

                return summaryList;
            }
        }

        public static List<object> GetUsersPerEntitySummaryForCustomerAdmin(int customerID, int? parentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             select row);
                if (parentID.HasValue)
                {
                    query = query.Where(row => row.ParentID == parentID);
                }
                else
                {
                    query = query.Where(row => row.ParentID == null);
                }
                var customerBranches = query.Select(row => new { row.ID, row.Name }).ToList();
                var branchIDs = customerBranches.Select(entry => entry.ID).ToList();

                var usersPerCustomerBranch = (from row in entities.Users
                                              where row.IsDeleted == false && branchIDs.Contains(row.CustomerBranchID ?? -1)
                                              group row by row.CustomerID.Value into grp
                                              select new { ID = grp.Key, Count = grp.Count() }).ToList();

                var summaryList = (from customerRow in customerBranches
                                   join countRow in usersPerCustomerBranch on customerRow.ID equals countRow.ID into summary
                                   from summaryRow in summary.DefaultIfEmpty()
                                   select new { Name = customerRow.Name, Quantity = (summaryRow != null ? summaryRow.Count : 0) }).ToList<object>();
                //select new { customerRow, summaryRow }).ToList<object>();

                return summaryList;
            }
        }



        #region Compliances Summary

        public static List<object> GetTransactionsSummaryForChart(int customerID, long? userID = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = from row in entities.ComplianceInstanceTransactionViews
                            select row;

                if (userID.HasValue)
                {
                    query = query.Where(entry => entry.UserID == userID);
                }
                else
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }

                var transactionsQuery = (from row in query
                                         group row by row.Role into roleGroup
                                         select new { Name = roleGroup.Key, Quantity = roleGroup.Count() }).ToList<object>();

                return transactionsQuery;
            }
        }


        public static List<object> GetTransactionsSummaryByRoleForChart(int customerID, List<int> roleIDs, long? userID = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime today = DateTime.Now.Date;

                List<object> summary = new List<object>();

                var query = from row in entities.ComplianceInstanceTransactionViews
                            select row;

                if (userID.HasValue)
                {
                    query = query.Where(entry => entry.UserID == userID);
                }
                else
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }

                var transactions = (from row in query
                                    where roleIDs.Contains((int)row.RoleID)
                                    select row).ToList();

                var statusList = (from row in entities.ComplianceStatusTransitions
                                  where roleIDs.Contains(row.RoleID)
                                  select row.InitialStateID).ToList();

                var pendingCount = (from row in transactions
                                    where statusList.Contains((int)row.ComplianceStatusID) && row.ScheduledOn >= today
                                    select 1).Count();

                var pendingOverDueCount = (from row in transactions
                                           where statusList.Contains((int)row.ComplianceStatusID) && row.ScheduledOn < today
                                           select 1).Count();

                var completedCount = transactions.Count - (pendingCount + pendingOverDueCount);


                summary.Add(new { Name = "Completed", Quantity = completedCount });
                summary.Add(new { Name = "Pending", Quantity = pendingCount });
                summary.Add(new { Name = "Overdue", Quantity = pendingOverDueCount });

                return summary;
            }
        }

        public static object GetCompliancesSummaryForChart(int customerID, long? userID = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = from row in entities.ComplianceInstanceTransactionViews
                            select row;

                if (userID.HasValue)
                {
                    query = query.Where(entry => entry.UserID == userID);
                }
                else
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }

                var transactionsQuery = (from row in query
                                         group row by row.Branch into branchGroup
                                         select new { Name = branchGroup.Key, Quantity = branchGroup.Count() }).ToList<object>();

                return transactionsQuery;
            }
        }

        #endregion
    }
}
