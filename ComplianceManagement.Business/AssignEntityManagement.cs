﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
   public class AssignEntityManagement
    {
       public static EntitiesAssignment SelectEntity(int branchId = -1, int userID = -1, int catagoryId = -1)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var EntitiesAssignmentData = (from row in entities.EntitiesAssignments
                                             where row.BranchID == branchId && row.UserID == userID && row.ComplianceCatagoryID == catagoryId
                                             select row).FirstOrDefault();
               return EntitiesAssignmentData;
           }
       }
       public static void Create(EntitiesAssignment objEntitiesAssignment)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               entities.EntitiesAssignments.Add(objEntitiesAssignment);
               entities.SaveChanges();
           }
       }

       public static void Create(List<EntitiesAssignment> objEntitiesAssignment)
       {

           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               objEntitiesAssignment.ForEach(entry =>
               {
                   entities.EntitiesAssignments.Add(entry);
               });
               entities.SaveChanges();
           }

       }

       public static List<ComplianceAssignmentEntitiesView> SelectAllEntities(int branchId = -1, int userID = -1, int customerID = -1)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var ComplianceTransactionEntity = (from row in entities.ComplianceAssignmentEntitiesViews
                                                  select row).ToList();

               var branchIds = (from row in entities.CustomerBranches
                                where row.IsDeleted == false && row.CustomerID == customerID
                                select row.ID).ToList();

               if (branchIds != null)
               {
                   ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => branchIds.Contains((int)entry.BranchID)).ToList();
               }
               if (branchId != -1)
               {
                   ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.BranchID == branchId).ToList();
               }
               if (userID != -1)
               {
                   ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
               }

               return ComplianceTransactionEntity;
           }
       }
        //sandesh
        public static List<ComplianceAssignmentEntitiesView> SelectAllEntitiesList(int userID, int CustomerID, List<long> CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceTransactionEntity = (from row in entities.ComplianceAssignmentEntitiesViews                                                                                        
                                                   select row).ToList();

                if (userID != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
                }

                if (CustomerBranchID.Count > 0)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => CustomerBranchID.Contains((int)entry.BranchID)).ToList();                   
                }     
                return ComplianceTransactionEntity;
            }
        }
        public static List<NameValueHierarchy> GetLocationNew(int BranchID, int userID, List<long> Branchlist)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetManagerAssignedBranch_Result> ManagerAssignedBranchlist = new List<SP_GetManagerAssignedBranch_Result>();
                ManagerAssignedBranchlist = (entities.SP_GetManagerAssignedBranch(Convert.ToInt32(userID))).ToList();

                if (Branchlist.Count > 0)
                {
                    var query = (from row in ManagerAssignedBranchlist
                                 where Branchlist.Contains(row.ID)
                                 select row).ToList();

                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                }
                else
                {
                    if (BranchID != -1)
                    {
                        var query = (from row in ManagerAssignedBranchlist
                                     where row.ID == BranchID
                                     select row).ToList();

                        hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();                        
                    }
                    else
                    {
                        var query = (from row in ManagerAssignedBranchlist
                                     select row).ToList();

                        hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();                       
                    }
                }                    
            }
            return hierarchy;
        }
        public static List<NameValueHierarchy> GetLocationNew(int BranchID, int userID)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetManagerAssignedBranch_Result> ManagerAssignedBranchlist = new List<SP_GetManagerAssignedBranch_Result>();
                ManagerAssignedBranchlist = (entities.SP_GetManagerAssignedBranch(Convert.ToInt32(userID))).ToList();

                if (BranchID != -1)
                {
                    var query = (from row in ManagerAssignedBranchlist
                                 where row.ID == BranchID
                                 select row).ToList();

                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    //foreach (var item in hierarchy)
                    //{
                    //    LoadSubEntitiesNew(item, false, entities);
                    //}
                }
                else
                {
                    var query = (from row in ManagerAssignedBranchlist
                                 select row).ToList();

                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    //foreach (var item in hierarchy)
                    //{
                    //    LoadSubEntitiesNew(item, false, entities);
                    //}
                }
            }

            return hierarchy;

        }
        public static List<NameValueHierarchy> GetLocation(int BranchID, int userID)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;                
                List<SP_GetManagerAssignedBranch_Result> ManagerAssignedBranchlist = new List<SP_GetManagerAssignedBranch_Result>();
                ManagerAssignedBranchlist = (entities.SP_GetManagerAssignedBranch(Convert.ToInt32(userID))).ToList();

                if (BranchID != -1)
                {

                    var query = (from row in ManagerAssignedBranchlist
                                 where row.ID == BranchID
                                 select row).ToList();

                    #region Previous Code
                    //var query = (from row in entities.EntitiesAssignments
                    //             join Cbranch in entities.CustomerBranches
                    //             on row.BranchID equals Cbranch.ID
                    //             where row.UserID == userID && row.BranchID == BranchID
                    //             select new
                    //             {
                    //                 ID = Cbranch.ID,
                    //                 Name = Cbranch.Name,
                    //                 IsDeleted = Cbranch.IsDeleted
                    //             }).Distinct();

                    //query = query.Where(entry => entry.IsDeleted == false);
                    #endregion
                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int) entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    foreach (var item in hierarchy)
                    {
                        LoadSubEntities(item, false, entities);
                    }
                }
                else
                {
                    var query = (from row in ManagerAssignedBranchlist                                 
                                 select row).ToList();
                    #region Previous Code
                    //var query = (from row in entities.EntitiesAssignments
                    //             join Cbranch in entities.CustomerBranches
                    //             on row.BranchID equals Cbranch.ID
                    //             where row.UserID == userID
                    //             select new
                    //             {
                    //                 ID = Cbranch.ID,
                    //                 Name = Cbranch.Name,
                    //                 IsDeleted = Cbranch.IsDeleted
                    //             }).Distinct();

                    //query = query.Where(entry => entry.IsDeleted == false);
                    #endregion
                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int) entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    foreach (var item in hierarchy)
                    {
                        LoadSubEntities(item, false, entities);
                    }
                }
            }

            return hierarchy;

        }

        public static List<NameValueHierarchy> GetLocation(int userID = -1)
       {
           List<NameValueHierarchy> hierarchy = null;
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               entities.Database.CommandTimeout = 180;
               var query = (from row in entities.EntitiesAssignments
                            join Cbranch in entities.CustomerBranches
                            on row.BranchID equals Cbranch.ID
                            where row.UserID == userID
                            select new {
                                        ID= Cbranch.ID,
                                        Name= Cbranch.Name,
                                        IsDeleted= Cbranch.IsDeleted
                            } ).Distinct();

               query = query.Where(entry => entry.IsDeleted == false);
               hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
               foreach (var item in hierarchy)
               {
                   LoadSubEntities(item, false, entities);
               }
           }

           return hierarchy;

       }


        public static List<NameValueHierarchy> GetAssignedLocationUserAndRoleWise(int role, int BranchID, int userID)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetApproverAssignedBranch_Result> ApproverAssignedBranchlist = new List<SP_GetApproverAssignedBranch_Result>();
                ApproverAssignedBranchlist = (entities.SP_GetApproverAssignedBranch(Convert.ToInt32(userID))).ToList();

                if (BranchID != -1)
                {

                    var customerBranchIds = (from row in ApproverAssignedBranchlist                                             
                                             where row.CustomerBranchID == BranchID 
                                             select row.CustomerBranchID).Distinct();

                    #region PreviousCode
                    //var customerBranchIds = (from row in entities.ComplianceAssignments
                    //                   join ci in entities.ComplianceInstances
                    //                   on row.ComplianceInstanceID equals ci.ID
                    //                   where row.UserID == userID && row.RoleID == role
                    //                   && ci.CustomerBranchID == BranchID && ci.IsDeleted == false
                    //                   select ci.CustomerBranchID).Distinct();
                    #endregion
                    var query = (from row in entities.CustomerBranches
                                 where customerBranchIds.Contains(row.ID) && row.IsDeleted == false
                                 select new
                                 {
                                     ID = row.ID,
                                     Name = row.Name
                                 }).ToList();

                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int) entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    foreach (var item in hierarchy)
                    {
                        LoadSubEntities(item, false, entities);
                    }
                }
                else
                {
                    #region PreviousCode
                    //var customerBranchIds = (from row in entities.ComplianceAssignments
                    //                   join ci in entities.ComplianceInstances
                    //                   on row.ComplianceInstanceID equals ci.ID
                    //                   where row.UserID == userID && row.RoleID == role
                    //                   && ci.IsDeleted == false
                    //                   select ci.CustomerBranchID).Distinct();
                    #endregion
                    var customerBranchIds = (from row in ApproverAssignedBranchlist                                           
                                             select row.CustomerBranchID).Distinct();   
                                     
                    var query = (from row in entities.CustomerBranches
                                 where customerBranchIds.Contains(row.ID) && row.IsDeleted == false
                                 select new
                                 {
                                     ID = row.ID,
                                     Name = row.Name
                                 }).ToList();

                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int) entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    foreach (var item in hierarchy)
                    {
                        LoadSubEntities(item, false, entities);
                    }
                }
            }

            return hierarchy;
        }
        public static List<NameValueHierarchy> GetAssignedLocationUserAndRoleWise(int role, int userID = -1)
       {
           List<NameValueHierarchy> hierarchy = null;
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               entities.Database.CommandTimeout = 180;
               var customerBranchIds =    (from row in entities.ComplianceAssignments
                                    join ci in entities.ComplianceInstances
                                    on row.ComplianceInstanceID equals ci.ID
                                     where row.UserID == userID && row.RoleID == role && ci.IsDeleted==false
                                    select ci.CustomerBranchID).Distinct();

               var query = (from row in entities.CustomerBranches
                            where customerBranchIds.Contains(row.ID) && row.IsDeleted==false
                            select new
                            {
                                ID = row.ID,
                                Name = row.Name                                
                            }).ToList();

               hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
               foreach (var item in hierarchy)
               {
                   LoadSubEntities(item, false, entities);
               }
           }

           return hierarchy;
       }

       public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, long userid)
       {

           List<NameValueHierarchy> hierarchy1 = null;
           var query = (from row in entities.EntitiesAssignments
                        join Cbranch in entities.CustomerBranches
                        on row.BranchID equals Cbranch.ID
                        where row.UserID == userid
                        && Cbranch.CustomerID == nvp.ID
                        select Cbranch).Distinct();


           query = query.Where(entry => entry.IsDeleted == false);
           hierarchy1 = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
           foreach (var item in hierarchy1)
           {
               nvp.Children.Add(item);
               LoadSubEntities(item, false, entities);
           }
       }

       

       public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
       {                     
           IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                               where row.IsDeleted == false
                                               select row);

           if (isClient)
           {
               query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
           }
           else
           {
               query = query.Where(entry => entry.ParentID == nvp.ID);
           }

           var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name}).OrderBy(entry => entry.Name).ToList();

           foreach (var item in subEntities)
           {
               nvp.Children.Add(item);
               LoadSubEntities(item, false, entities);
           }
       }

       public static DataTable GetMangementSummaryReport(int barnchId,int userID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
              
               var branchIDs = (from row in entities.CustomerBranches
                                      where row.ParentID == barnchId
                                      select row.ID).ToList();
               branchIDs.Add(barnchId);

               var catagoryIDs = (from row in entities.EntitiesAssignments
                                  where row.UserID == userID
                                  select row.ComplianceCatagoryID).ToList();

               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        join acte in entities.Acts on row.ActID equals acte.ID
                                        where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 5) && branchIDs.Contains((int)row.CustomerBranchID)
                                        && (row.IsActive != false || row.IsUpcomingNotDeleted != false) 
                                        select new 
                                         {
                                             ComplianceInstanceID = row.ComplianceInstanceID,
                                             row.ComplianceID,
                                             acte.ComplianceCategoryId,
                                             row.Risk,
                                             row.CustomerBranchID,
                                             row.ScheduledOnID
                                         }).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());


               transactionsQuery = transactionsQuery.Where(enrty => catagoryIDs.Contains((int)enrty.ComplianceCategoryId));

               DataTable table = new DataTable();
               table.Columns.Add("High", typeof(long));
               table.Columns.Add("Medium", typeof(long));
               table.Columns.Add("Low", typeof(long));

               long high = transactionsQuery.Where(enrty => enrty.Risk == 0).Count();
               long Medium = transactionsQuery.Where(enrty => enrty.Risk == 1).Count();
               long Low = transactionsQuery.Where(enrty => enrty.Risk == 2).Count();

               table.Rows.Add(high, Medium, Low);
               return table;


           }
       }

       public static List<object> GetMangementSummaryGraph(int barnchId, int userID, string status,out bool displayFlag)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               bool flag = false;
               List<object> summary = new List<object>();
               List<int> statusId = new List<int>();
               if (status == "pending")
               {
                   statusId.Add(1);
                   statusId.Add(2);
                   statusId.Add(3);
               }
               else
               {
                   statusId.Add(5);
               }

               var branchIDs = (from row in entities.CustomerBranches
                                where row.ParentID == barnchId
                                select row.ID).ToList();

               branchIDs.Add(barnchId);

               var catagoryIDs = (from row in entities.EntitiesAssignments
                                  where row.UserID == userID
                                  select row.ComplianceCatagoryID).ToList();

               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        join acte in entities.Acts on row.ActID equals acte.ID
                                        where statusId.Contains((int)row.ComplianceStatusID) && branchIDs.Contains((int)row.CustomerBranchID)
                                        && (row.IsActive != false || row.IsUpcomingNotDeleted != false) 
                                        select new
                                        {
                                            ComplianceInstanceID = row.ComplianceInstanceID,
                                            row.ComplianceID,
                                            acte.ComplianceCategoryId,
                                            row.Risk,
                                            row.CustomerBranchID,
                                            acte.ComplianceCategory,
                                            row.ScheduledOnID
                                        }).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

               if (catagoryIDs.Count == 1)
               {
                   transactionsQuery = transactionsQuery.Where(enrty => catagoryIDs.Contains((int)enrty.ComplianceCategoryId));
                   long high = transactionsQuery.Where(enrty => enrty.Risk == 0).Count();
                   long Medium = transactionsQuery.Where(enrty => enrty.Risk == 1).Count();
                   long Low = transactionsQuery.Where(enrty => enrty.Risk == 2).Count();

                   summary.Add(new { Name = "Low", Quantity = Low });
                   summary.Add(new { Name = "Medium", Quantity = Medium });
                   summary.Add(new { Name = "High", Quantity = high });
                   flag = (Low + Medium + high) != 0 ? true : false;
               }
               else
               {
                   int cnt = 0;
                   foreach (int cc in catagoryIDs)
                   {
                       int Count = transactionsQuery.Where(entry => entry.ComplianceCategoryId == cc && entry.Risk == 0).Count();
                       summary.Add(new { Name = ComplianceCategoryManagement.GetByID(cc).Name, Quantity = Count });
                       cnt += Count;
                   }
                   flag = cnt != 0 ? true : false;
               }
               displayFlag = flag;
               return summary;


           }
       }

       public static int GetCategoryByUserID(int userID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var catagoryIDs = (from row in entities.EntitiesAssignments
                              where row.UserID == userID
                              select row.ComplianceCatagoryID).ToList();

               return catagoryIDs.Count;
           }
       }

       public static DataTable GetMangementSummaryReport1(int barnchId, int userID)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {

               var branchIDs = (from row in entities.CustomerBranches
                                where row.ParentID == barnchId
                                select row.ID).ToList();
               branchIDs.Add(barnchId);

               var catagoryIDs = (from row in entities.EntitiesAssignments
                                  where row.UserID == userID
                                  select row.ComplianceCatagoryID).ToList();

               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        join acte in entities.Acts on row.ActID equals acte.ID
                                        where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 5) && branchIDs.Contains((int)row.CustomerBranchID)
                                        && (row.IsActive != false || row.IsUpcomingNotDeleted != false) 
                                        select new
                                        {
                                            ComplianceInstanceID = row.ComplianceInstanceID,
                                            row.ComplianceID,
                                            acte.ComplianceCategoryId,
                                            row.Risk,
                                            row.CustomerBranchID,
                                            row.ComplianceStatusID,
                                            row.ScheduledOnID
                                        }).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());


               transactionsQuery = transactionsQuery.Where(enrty => catagoryIDs.Contains((int)enrty.ComplianceCategoryId));

               DataTable table = new DataTable();
               table.Columns.Add("HighPending", typeof(long));
               table.Columns.Add("Highdelayed", typeof(long));
               table.Columns.Add("Medium", typeof(long));
               table.Columns.Add("Low", typeof(long));

               long HighPending = transactionsQuery.Where(enrty => enrty.Risk == 0 && (enrty.ComplianceStatusID == 1 || enrty.ComplianceStatusID == 2 || enrty.ComplianceStatusID == 3)).Count();
               long Highdelayed = transactionsQuery.Where(enrty => enrty.Risk == 0 && enrty.ComplianceStatusID == 5).Count();
               long Medium = transactionsQuery.Where(enrty => enrty.Risk == 1).Count();
               long Low = transactionsQuery.Where(enrty => enrty.Risk == 2).Count();

               table.Rows.Add(HighPending, Highdelayed, Medium, Low);
               return table;


           }
       }

       //For Management Dashboard
       public static int GetCompanyOverview(int customerid, int barnchId)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
               //transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
               //                     where row.CustomerBranchID == barnchId
               //                     && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
               //                     select row).ToList();

               transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                    where row.CustomerID==customerid  
                                     select row).ToList();

               transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == barnchId
                   && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                                                                                                         

              var userCount = transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;

              return userCount;

           }
       }
       public static List<ComplianceInstanceTransactionView> GetApproverNameSatutory(int customerid, int barnchId)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {

               //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
               //                         where row.CustomerBranchID == barnchId && row.RoleID == 6
               //                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
               //                         select row).ToList();



               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        where row.CustomerID == customerid
                                        select row).ToList();

               transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == barnchId && entry.RoleID == 6
                                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();



               return transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
           }
       }      
       public static ComplianceInstanceTransactionView GetApproverName(int customerid,int barnchId)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               entities.Database.CommandTimeout = 180;

               
               //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
               //                         where row.CustomerBranchID == barnchId && row.RoleID==6
               //                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
               //                         select row).ToList();

               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        where row.CustomerID==customerid 
                                         select row).ToList();

               transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == barnchId && entry.RoleID == 6
                                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();
                                        

                if (transactionsQuery.Count != 0)
                {
                    return transactionsQuery[0];
                }
                else
                {
                    return null;
                }
           }
       }
       public static List<ComplianceInstanceTransactionView> GetReviwersName(int customerid, int barnchId)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        where  row.CustomerID==customerid   
                                          select row).ToList();
               transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == barnchId && (entry.RoleID == 4 || entry.RoleID == 5)
                                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();                                                                                                                    

               return transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
           }
       }


       //For Approver Dashboard.
       public static List<ComplianceInstanceTransactionView> GetPerformerName(int customerid, int barnchId)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
               //                         where row.CustomerBranchID == barnchId
               //                         && row.RoleID == 3
               //                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
               //                         select row).ToList();

               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        where row.CustomerID == customerid   
                                          select row).ToList();

                 transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == barnchId && entry.RoleID == 3
                                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();                                                                                                                    
                                      
               return transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
           }
       }


       public static DataTable GetCompliancesStatusRiskWise(int customerid, int barnchId, int year, int month, int userId = -1, bool approver = false)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if(month<4)
               {
                   startDateyear--;
               }

               DateTime startDate = new DateTime(startDateyear,Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
               
               //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
               //                         where row.CustomerBranchID == barnchId && (row.IsActive != false || row.IsUpcomingNotDeleted != false)  && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
               //                         select row).ToList();

               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        where row.CustomerID == customerid                                        
                                        select row).ToList();

               transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == barnchId && entry.ScheduledOn >= startDate && entry.ScheduledOn <= EndDate
                                        && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)).ToList();

               
               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
               long delayedcount;
               long Completedcount;
               long pendingcount;
               long totalcount;
            
               DataTable table = new DataTable();
               table.Columns.Add("ID", typeof(int));
               table.Columns.Add("RiskCatagory", typeof(string));
               table.Columns.Add("Completed", typeof(long));
               table.Columns.Add("Delayed", typeof(long));
               table.Columns.Add("Pending", typeof(long));
               table.Columns.Add("Total", typeof(long));

               // for Heigh risk Compliances
               delayedcount = transactionsQuery.Where(entry =>(entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)&& entry.Risk == 0).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.Risk == 0).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.Risk == 0).Count();
               totalcount = delayedcount + Completedcount + pendingcount;

               if (totalcount!=0)
               table.Rows.Add(0,"High", Completedcount, delayedcount, pendingcount, totalcount);

               // for Medium risk Compliances
               delayedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.Risk == 1).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.Risk == 1).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.Risk == 1).Count();
               totalcount = delayedcount + Completedcount + pendingcount;

               if (totalcount != 0)
               table.Rows.Add(1,"Medium", Completedcount, delayedcount, pendingcount, totalcount);

               // for Low risk Compliances
               delayedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.Risk == 2).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.Risk == 2).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.Risk == 2).Count();
               totalcount = delayedcount + Completedcount + pendingcount;
               
               if (totalcount != 0)
               table.Rows.Add(2,"Low", Completedcount, delayedcount, pendingcount, totalcount);

               return table;


           }
       }

       public static DataTable GetCompliancesStatusFunctionWise(int barnchId, int year, int month, int userId = -1, bool approver = false)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if (month < 4)
               {
                   startDateyear--;
               }
               DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));


               
               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        join acte in entities.Acts
                                        on row.ActID equals acte.ID
                                        where row.CustomerBranchID == barnchId && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                        && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                        select new
                                        {
                                            ComplianceInstanceID = row.ComplianceInstanceID,
                                            row.ComplianceID,
                                            acte.ComplianceCategoryId,
                                            row.ComplianceStatusID,
                                            row.ScheduledOn,
                                            row.ScheduledOnID,
                                            row.RoleID,
                                            row.UserID
                                        }).ToList();

              


             

               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

               long delayedcount;
               long CompletedCount;
               long pendingcount;
               long totalcount;
               //DateTime now = DateTime.UtcNow.Date;

               DataTable table = new DataTable();
               table.Columns.Add("ID", typeof(int));
               table.Columns.Add("RiskCatagory", typeof(string));
               table.Columns.Add("Completed", typeof(long));
               table.Columns.Add("Delayed", typeof(long));
               table.Columns.Add("Pending", typeof(long));
               table.Columns.Add("Total", typeof(long));

               var CatagoryList = ComplianceCategoryManagement.GetAll();

               foreach (ComplianceCategory cc in CatagoryList)
               {
                   delayedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                   CompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();
                   pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();
                   totalcount = delayedcount + CompletedCount + pendingcount;

                   if (totalcount != 0)
                   table.Rows.Add(cc.ID,cc.Name, CompletedCount, delayedcount, pendingcount, totalcount);

               }

               return table;


           }
       }

       public static DataTable GetManagementCompliancesSummary( int barnchId, int year, int month, int userId = -1, bool approver = false)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if (month < 4)
               {
                   startDateyear--;
               }
               DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        join acte in entities.Acts
                                        on row.ActID equals acte.ID
                                        where row.CustomerBranchID == barnchId && (row.IsActive != false || row.IsUpcomingNotDeleted != false)  && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                        select new
                                        {
                                            ComplianceInstanceID = row.ComplianceInstanceID,
                                            row.ComplianceID,
                                            acte.ComplianceCategoryId,
                                            row.ComplianceStatusID,
                                            row.ScheduledOn,
                                            row.Risk,
                                            row.ScheduledOnID,
                                            row.RoleID,
                                            row.UserID
                                        }).ToList();

               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

               long highcount;
               long mediumCount;
               long lowcount;
               long totalcount;
               //DateTime now = DateTime.UtcNow.Date;

               DataTable table = new DataTable();
               table.Columns.Add("ID", typeof(int));
               table.Columns.Add("Catagory", typeof(string));
               table.Columns.Add("High", typeof(long));
               table.Columns.Add("Medium", typeof(long));
               table.Columns.Add("Low", typeof(long));
               table.Columns.Add("Total", typeof(long));

               var CatagoryList = ComplianceCategoryManagement.GetAll();

               foreach (ComplianceCategory cc in CatagoryList)
               {
                   highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID !=11).Count();
                   mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                   lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                   totalcount = highcount + mediumCount + lowcount;

                   if(totalcount != 0)
                   table.Rows.Add(cc.ID,cc.Name, highcount, mediumCount, lowcount, totalcount);

               }

               return table;


           }
       }

       public static DataTable GetPastTwelveMonthSummary(int customerid, int barnchId, int year, int month, int userId = -1, bool approver = false)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if (month < 4)
               {
                   startDateyear--;
               }
               DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);

               //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
               //                         where row.CustomerBranchID == barnchId && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
               //                         select row).ToList();

               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        where row.CustomerID == customerid && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                        select row).ToList();

               transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == barnchId).ToList();

           
               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

               DataTable table = new DataTable();
               table.Columns.Add("Compliances", typeof(string));
               for (int i = 1; i < 13; i++)
               {
                   //table.Columns.Add(EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                   table.Columns.Add(i.ToString(), typeof(string));
               }


               long monthCount = 0;
               DataRow CompletedInTime = table.NewRow();
               CompletedInTime["Compliances"] = "Completed In Time";
               for (int i = 1; i < 13; i++)
               {
                   DateTime previousDate = EndDate.AddMonths(-i);
                   DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                   DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));
                   monthCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                   //CompletedInTime[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = monthCount;
                   CompletedInTime[i] = monthCount;
               }
               table.Rows.Add(CompletedInTime);


               DataRow DelayedTime = table.NewRow();
               DelayedTime["Compliances"] = "Completed after Due Date";
               for (int i = 1; i < 13; i++)
               {
                   DateTime previousDate = EndDate.AddMonths(-i);
                   DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                   DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));
                   monthCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                   //DelayedTime[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = monthCount;
                   DelayedTime[i] = monthCount;
               }
               table.Rows.Add(DelayedTime);


               DataRow NotCompletedTime = table.NewRow();
               NotCompletedTime["Compliances"] = "Not Yet Completed";
               for (int i = 1; i < 13; i++)
               {
                   DateTime previousDate = EndDate.AddMonths(-i);
                   DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                   DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));

                   monthCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)
                                && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();

                   //NotCompletedTime[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = monthCount;
                   NotCompletedTime[i] = monthCount;
               }
               table.Rows.Add(NotCompletedTime);

               return table;


           }
       }

     

       private static void BindBranchesHierarchy(NameValueHierarchy nvp, List<ComplianceInstanceTransactionView> transactionsQuery, DataTable table, int period, DateTime EndDate, int customerid)
       {
            foreach (var item in nvp.Children)
            {
                item.Level = nvp.Level+1;
                DataRow tableRow = BindingGradingTableRow(transactionsQuery, item, table, period, EndDate, customerid);
                table.Rows.Add(tableRow);
                BindBranchesHierarchy(item, transactionsQuery, table, period, EndDate, customerid);
            }
         
       }
        private static void BindBranchesHierarchy(NameValueHierarchy nvp, List<SP_StatutoryGradingReport_Result> transactionsQuery, DataTable table, int period, DateTime EndDate, int customerid)
        {
            foreach (var item in nvp.Children)
            {
                item.Level = nvp.Level + 1;
                DataRow tableRow = BindingGradingTableRow(transactionsQuery, item, table, period, EndDate, customerid);
                table.Rows.Add(tableRow);
                BindBranchesHierarchy(item, transactionsQuery, table, period, EndDate, customerid);
            }

        }
        private static DataRow BindingGradingTableRow(List<SP_StatutoryGradingReport_Result> transactionsQuery, NameValueHierarchy item, DataTable table, int period, DateTime EndDate, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                //comment by rahul on 2 MAY 2018
                //var branchIDs = (from row in entities.CustomerBranches
                //                 where row.ParentID == item.ID
                //                 select row.ID).ToList();
                //branchIDs.Add(item.ID);

                var branchIDs = (from row in entities.CustomerBranches                                     
                                 where row.ID == item.ID
                                 select row.ID).ToList();

                int color;
                DataRow tableRow = table.NewRow();

                tableRow["Location"] = item.Name + "#" + item.Level;
                //tableRow["Approver"] = ""; // GetApproverName(customerid, item.ID) != null ? GetApproverName(customerid,item.ID).User : "";

                // tableRow["Approver"] = GetApproverName(customerid, item.ID) != null ? GetApproverName(customerid,item.ID).User : "";
                for (int i = period; i >= 1; i--)
                {
                    DateTime previousDate = EndDate.AddMonths(-i);
                    DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                    DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));

                    long highRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 0 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                    long MediumRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 1 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                    long LowRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 2 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();

                    long highRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 0 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                    long MediumRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 1 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                    long LowRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 2 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();

                    double seventyfivePercentLow = (0.75 * LowRisktotalCount);
                    double seventyfivePercentHigh = (0.75 * highRisktotalCount);

                    if (highRisktotalCount != 0 || MediumRisktotalCount != 0 || LowRiskCompletedCount != 0)
                    {
                        if (highRisktotalCount == highRiskCompletedCount && MediumRisktotalCount == MediumRiskCompletedCount && LowRiskCompletedCount >= seventyfivePercentLow)
                        {
                            color = 1;
                        }
                        else if (highRiskCompletedCount >= seventyfivePercentHigh && MediumRiskCompletedCount >= (MediumRisktotalCount / 2) && LowRiskCompletedCount >= (LowRisktotalCount / 2))
                        {
                            color = 2;
                        }
                        else
                        {
                            color = 3;
                        }
                    }
                    else
                    {
                        color = 4;
                    }

                    tableRow[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = color;

                }


                return tableRow;

            }
        }
        private static DataRow BindingGradingTableRow(List<ComplianceInstanceTransactionView> transactionsQuery, NameValueHierarchy item, DataTable table, int period, DateTime EndDate,int customerid)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               entities.Database.CommandTimeout = 180;
               var branchIDs = (from row in entities.CustomerBranches
                                where row.ParentID == item.ID
                                select row.ID).ToList();

               branchIDs.Add(item.ID);
               int color;
               DataRow tableRow = table.NewRow();
              
               tableRow["Location"] = item.Name + "#" + item.Level;
               tableRow["Approver"] = ""; // GetApproverName(customerid, item.ID) != null ? GetApproverName(customerid,item.ID).User : "";

               // tableRow["Approver"] = GetApproverName(customerid, item.ID) != null ? GetApproverName(customerid,item.ID).User : "";
                for (int i = period; i >= 1; i--)
               {
                   DateTime previousDate = EndDate.AddMonths(-i);
                   DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                   DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));

                   long highRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 0 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                   long MediumRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 1 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                   long LowRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 2 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();

                   long highRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 0 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                   long MediumRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 1 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                   long LowRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 2 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();

                   double seventyfivePercentLow = (0.75 * LowRisktotalCount);
                   double seventyfivePercentHigh = (0.75 * highRisktotalCount);

                   if (highRisktotalCount != 0 || MediumRisktotalCount != 0 || LowRiskCompletedCount != 0)
                   {
                       if (highRisktotalCount == highRiskCompletedCount && MediumRisktotalCount == MediumRiskCompletedCount && LowRiskCompletedCount >= seventyfivePercentLow)
                       {
                           color = 1;
                       }
                       else if (highRiskCompletedCount >= seventyfivePercentHigh && MediumRiskCompletedCount >= (MediumRisktotalCount / 2) && LowRiskCompletedCount >= (LowRisktotalCount / 2))
                       {
                           color = 2;
                       }
                       else
                       {
                           color = 3;
                       }
                   }
                   else
                   {
                       color = 4;
                   }

                   tableRow[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = color;

               }

               
                return tableRow;
              
           }
       }

       private static void BindSumaryStatusChieldHierarchy(NameValueHierarchy nvp, List<ComplianceInstanceTransactionView> transactionsQuery, DataTable table, int period, DateTime EndDate, int customerid)
       {
           foreach (var item in nvp.Children)
           {
               item.Level = nvp.Level + 1;
               DataRow tableRow = BindingManagementSummaryStatusTableRow(transactionsQuery, item, table, period, EndDate, customerid);
               table.Rows.Add(tableRow);
               BindSumaryStatusChieldHierarchy(item, transactionsQuery, table, period, EndDate, customerid);
           }

       }

       public static DataTable GetManagementSummaryStatusReport(int customerid, int userId, int year, int month, int period, bool approver = false)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               entities.Database.CommandTimeout = 180;
               DateTime startDate = new DateTime(year, month, 1).AddMonths(-period);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);

               //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
               //                         where row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate                                        
               //                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)\    
               //                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


               //Changed By Sachin 04 Nov 2016

               //var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
               //                         where row.CustomerID == customerid && row.UserID == userId && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
               //                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)

               //                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

               var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                        where row.CustomerID == customerid && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                        && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                        select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
              
               //Changed By Sachin 04 Nov 2016

               DataTable table = new DataTable();
               table.Columns.Add("Location", typeof(string));
               table.Columns.Add("Approver", typeof(string));
              
               for (int i = period; i >= 1; i--)
               {
                   table.Columns.Add("Completed In Time_" + i, typeof(string));
                   table.Columns.Add("Completed After Due Date_" + i, typeof(long));
                   table.Columns.Add("Not Yet Completed_" + i, typeof(long));
                   table.Columns.Add("Total_" + i, typeof(long));
                   table.Columns.Add("Rating_" + i, typeof(string));
                   //table.Columns.Add(DateTime.UtcNow.AddMonths(-i).ToString("MMM") + " - " + DateTime.UtcNow.AddMonths(-i).Year, typeof(long));
               }

               List<NameValueHierarchy> bracnhes = new List<NameValueHierarchy>();
               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                   bracnhes = AssignEntityManagement.GetAssignedLocationUserAndRoleWise(6,userId);
               }
               else
               {
                   bracnhes = AssignEntityManagement.GetLocation(userId);
               }

               foreach (var item in bracnhes)
               {
                   item.Level = 0;
                   DataRow tableRow = BindingManagementSummaryStatusTableRow(transactionsQuery, item, table, period, EndDate, customerid);
                   table.Rows.Add(tableRow);
                   BindSumaryStatusChieldHierarchy(item, transactionsQuery, table, period, EndDate, customerid);
               }

               return table;


           }
       }

       private static DataRow BindingManagementSummaryStatusTableRow(List<ComplianceInstanceTransactionView> transactionsQuery, NameValueHierarchy item, DataTable table, int period, DateTime EndDate, int customerid)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               entities.Database.CommandTimeout = 180;
               var branchIDs = (from row in entities.CustomerBranches
                                where row.ParentID == item.ID
                                select row.ID).ToList();
               branchIDs.Add(item.ID);

               string color;
               DataRow tableRow = table.NewRow();

               tableRow["Location"] = item.Name + "#" + item.Level;
               tableRow["Approver"] = GetApproverName(customerid, item.ID) != null ? GetApproverName(customerid,item.ID).User : "";
               for (int i = period; i >= 1; i--)
               {
                   DateTime previousDate = EndDate.AddMonths(-i);
                   DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                   DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));


                   
                   long Completedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                   tableRow["Completed In Time_" + i] = Completedcount;

                   long delayedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 9 ||entry.ComplianceStatusID == 5) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                   tableRow["Completed After Due Date_" + i] = delayedcount;

                   long pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)
                                       && branchIDs.Contains((int)entry.CustomerBranchID) && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                   tableRow["Not Yet Completed_" + i] = pendingcount;

                   long totalcount = delayedcount + Completedcount + pendingcount;
                   tableRow["Total_" + i] = totalcount;

                   long highRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 0 && entry.ScheduledOn > date1 && entry.ScheduledOn < date2).Count();
                   long MediumRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 1 && entry.ScheduledOn > date1 && entry.ScheduledOn < date2).Count();
                   long LowRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 2 && entry.ScheduledOn > date1 && entry.ScheduledOn < date2).Count();

                   long highRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 0 && entry.ScheduledOn > date1 && entry.ScheduledOn < date2).Count();
                   long MediumRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 1 && entry.ScheduledOn > date1 && entry.ScheduledOn < date2).Count();
                   long LowRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 2 && entry.ScheduledOn > date1 && entry.ScheduledOn < date2).Count();

                   double seventyfivePercentLow = (0.75 * LowRisktotalCount);
                   double seventyfivePercentHigh = (0.75 * highRisktotalCount);
                   if (highRisktotalCount != 0 || MediumRisktotalCount != 0 || LowRisktotalCount != 0)
                   {
                       if (highRisktotalCount == highRiskCompletedCount && MediumRisktotalCount == MediumRiskCompletedCount && LowRiskCompletedCount >= seventyfivePercentLow)
                       {
                           color = "GREEN";
                       }
                       else if (highRiskCompletedCount >= seventyfivePercentHigh && MediumRiskCompletedCount >= (MediumRisktotalCount / 2) && LowRiskCompletedCount >= (LowRisktotalCount / 2))
                       {
                           color = "BROWN";
                       }
                       else
                       {
                           color = "RED";
                       }
                   }
                   else
                   {
                       color = "RED";
                   }

                   tableRow["Rating_" + i] = color;

               }


               return tableRow;

           }
       }
     
       public static List<ComplianceDashboardSummaryView> GetManagementDetailView(int Customerid,int barnchId, int year, int month,List<int>statusIDs ,string filter,int? functionId,string TwelveMonth = "")
       {
           int startDateyear = year;
           if (month < 4)
           {
               startDateyear--;
           }
            List<ComplianceDashboardSummaryView> detailView = new List<ComplianceDashboardSummaryView>();
           DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
           DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));

           if (filter.Equals("Summary"))
           {
               if (functionId != -1)
               {
                   detailView = DashboardManagement.GetComplianceDashboardSummary(Customerid).ToList()
                              .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.Risk) && entry.ScheduledOn >= startDate && entry.ScheduledOn <= EndDate
                              && entry.ComplianceCategoryId == functionId)
                              .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
               }
               else
               {
                   detailView = DashboardManagement.GetComplianceDashboardSummary(Customerid).ToList()
                             .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.Risk) && entry.ScheduledOn >= startDate && entry.ScheduledOn <= EndDate)
                             .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
               }
           }
           else if (filter.Equals("TwelveMonthSummary"))
           {
               string[] token = TwelveMonth.Split('/');
               int fourDigitYear = CultureInfo.CurrentCulture.Calendar.ToFourDigitYear(Convert.ToInt32(token[1]));
               DateTime sdate = new DateTime(fourDigitYear, Convert.ToInt32(token[0]), 1);
               DateTime edate = new DateTime(fourDigitYear, Convert.ToInt32(token[0]), DateTime.DaysInMonth(fourDigitYear, Convert.ToInt32(token[0])));
               detailView = DashboardManagement.GetComplianceDashboardSummary(Customerid).ToList()
                           .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.ComplianceStatusID) && entry.ScheduledOn >= sdate && entry.ScheduledOn <= edate)
                           .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
           }
           else
           {


               detailView = DashboardManagement.GetComplianceDashboardSummary(Customerid).ToList()
                               .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains(entry.ComplianceStatusID) && entry.ScheduledOn >= startDate && entry.ScheduledOn <= EndDate)
                               .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


               
               if (filter.Equals("Risk"))
               {
                   if (functionId != -1)
                   {
                       detailView = detailView.Where(entry => entry.Risk == functionId).ToList();
                   }

               }
               else if (filter.Equals("Function"))
               {
                   if (functionId != -1)
                   {
                       detailView = detailView.Where(entry => entry.ComplianceCategoryId == functionId).ToList();
                   }
               }              
           }
          
           return detailView;
       }


       
       public static DataTable GetCompliancesForGraphFunctionWise(int barnchId,int categoryID ,int year, int month, int userId = -1, bool approver = false)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if (month < 4)
               {
                   startDateyear--;
               }

               DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));

               var transactionsQuery = (from row in entities.ComplianceDashboardSummaryViews
                                        where row.CustomerBranchID == barnchId && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate && row.ComplianceCategoryId == categoryID
                                        select row).ToList();
               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
               long delayedcount;
               long Completedcount;
               long pendingcount;
               long totalcount;

               DataTable table = new DataTable();
               table.Columns.Add("RiskCatagory", typeof(string));
               table.Columns.Add("Completed", typeof(long));
               table.Columns.Add("Delayed", typeof(long));
               table.Columns.Add("Pending", typeof(long));
               table.Columns.Add("Total", typeof(long));

               // for Heigh risk Compliances
               delayedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.Risk == 0).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.Risk == 0).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.Risk == 0).Count();
               totalcount = delayedcount + Completedcount + pendingcount;
               table.Rows.Add("High", Completedcount, delayedcount, pendingcount, totalcount);

               // for Medium risk Compliances
               delayedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.Risk == 1).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.Risk == 1).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.Risk == 1).Count();
               totalcount = delayedcount + Completedcount + pendingcount;
               table.Rows.Add("Medium", Completedcount, delayedcount, pendingcount, totalcount);

               // for Low risk Compliances
               delayedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.Risk == 2).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.Risk == 2).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.Risk == 2).Count();
               totalcount = delayedcount + Completedcount + pendingcount;
               table.Rows.Add("Low", Completedcount, delayedcount, pendingcount, totalcount);

      
               return table;
            


           }
       }

       public static int GetGradeForManagementDashboard(int barnchId, int categoryID, int year, int month, int userId = -1, bool approver = false)
       {

           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if (month < 4)
               {
                   startDateyear--;
               }

               int color = 0;
               DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));

               var transactionsQuery = (from row in entities.ComplianceDashboardSummaryViews
                                        where row.CustomerBranchID == barnchId && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate && row.ComplianceCategoryId == categoryID
                                        select row).ToList();
               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               long highRisktotalCount = transactionsQuery.Where(entry => entry.Risk == 0).Count();
               long MediumRisktotalCount = transactionsQuery.Where(entry =>entry.Risk == 1 ).Count();
               long LowRisktotalCount = transactionsQuery.Where(entry => entry.Risk == 2 ).Count();

               long highRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && entry.Risk == 0).Count();
               long MediumRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && entry.Risk == 1).Count();
               long LowRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9)  && entry.Risk == 2).Count();

               double seventyfivePercentLow = (0.75 * LowRisktotalCount);
               double seventyfivePercentHigh = (0.75 * highRisktotalCount);

               if (highRisktotalCount != 0 || MediumRisktotalCount != 0 || LowRiskCompletedCount != 0)
               {
                   if (highRisktotalCount == highRiskCompletedCount && MediumRisktotalCount == MediumRiskCompletedCount && LowRiskCompletedCount >= seventyfivePercentLow)
                   {
                       color = 1;
                   }
                   else if (highRiskCompletedCount >= seventyfivePercentHigh && MediumRiskCompletedCount >= (MediumRisktotalCount / 2) && LowRiskCompletedCount >= (LowRisktotalCount / 2))
                   {
                       color = 2;
                   }
                   else
                   {
                       color = 3;
                   }
               }
               else
               {
                   color = 3;
               }

               return color;
           }
          
       }

        //Added by Rahul on 14 Dec 2015 for Internal

       
        public static List<NameValueHierarchy> GetLocationInternalNew(int BranchID, int userID)
        {

            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (BranchID != -1)
                {
                    var query = (from row in entities.EntitiesAssignmentInternals
                                 join Cbranch in entities.CustomerBranches
                                 on row.BranchID equals Cbranch.ID
                                 where row.UserID == userID && row.BranchID == BranchID
                                 select Cbranch).Distinct();

                    query = query.Where(entry => entry.IsDeleted == false);
                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    //foreach (var item in hierarchy)
                    //{
                    //    LoadSubEntities(item, false, entities);
                    //}
                }
                else
                {
                    var query = (from row in entities.EntitiesAssignmentInternals
                                 join Cbranch in entities.CustomerBranches
                                 on row.BranchID equals Cbranch.ID
                                 where row.UserID == userID
                                 select Cbranch).Distinct();

                    query = query.Where(entry => entry.IsDeleted == false);
                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    //foreach (var item in hierarchy)
                    //{
                    //    LoadSubEntities(item, false, entities);
                    //}
                }
            }

            return hierarchy;

        }
        public static List<NameValueHierarchy> GetLocationInternal(int BranchID, int userID)
        {

            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (BranchID != -1)
                {
                    var query = (from row in entities.EntitiesAssignmentInternals
                                 join Cbranch in entities.CustomerBranches
                                 on row.BranchID equals Cbranch.ID
                                 where row.UserID == userID && row.BranchID == BranchID
                                 select Cbranch).Distinct();

                    query = query.Where(entry => entry.IsDeleted == false);
                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    foreach (var item in hierarchy)
                    {
                        LoadSubEntities(item, false, entities);
                    }
                }
                else
                {
                    var query = (from row in entities.EntitiesAssignmentInternals
                                 join Cbranch in entities.CustomerBranches
                                 on row.BranchID equals Cbranch.ID
                                 where row.UserID == userID
                                 select Cbranch).Distinct();

                    query = query.Where(entry => entry.IsDeleted == false);
                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    foreach (var item in hierarchy)
                    {
                        LoadSubEntities(item, false, entities);
                    }
                }
            }

            return hierarchy;

        }
        public static List<InternalComplianceInstanceTransactionView> GetReviwersNameInternal(int barnchId)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        where row.CustomerBranchID == barnchId
                                        && (row.RoleID == 4 || row.RoleID == 5)
                                        select row).ToList();

               return transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
           }
       }
       public static int GetCompanyOverviewInternal(int barnchId)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
               transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                    where row.CustomerBranchID == barnchId
                                    select row).ToList();

               var userCount = transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;

               return userCount;

           }
       }
       public static List<InternalComplianceInstanceTransactionView> GetPerformerNameInternal(int barnchId)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        where row.CustomerBranchID == barnchId
                                        && row.RoleID == 3
                                        select row).ToList();

               return transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
           }
       }

       public static List<InternalComplianceInstanceTransactionView> GetApproverNameInternalNew(int barnchId)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        where row.CustomerBranchID == barnchId && row.RoleID == 6
                                        select row).ToList();

               return transactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
           }
       }

       public static InternalComplianceInstanceTransactionView GetApproverNameInternal(int barnchId)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        where row.CustomerBranchID == barnchId && row.RoleID == 6
                                        select row).ToList();

               if (transactionsQuery.Count != 0)
               {
                   return transactionsQuery[0];
               }
               else
               {
                   return null;
               }
           }
       }
        public static List<NameValueHierarchy> GetAssignedLocationUserAndRoleWiseInternal(int role, int BranchID, int userID)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (BranchID != -1)
                {
                    var customerIds = (from row in entities.InternalComplianceAssignments
                                       join ci in entities.InternalComplianceInstances
                                       on row.InternalComplianceInstanceID equals ci.ID
                                       where row.UserID == userID && row.RoleID == role
                                       && ci.CustomerBranchID == BranchID && ci.IsDeleted == false
                                       select ci.CustomerBranchID).Distinct();

                    var query = (from row in entities.CustomerBranches
                                 where customerIds.Contains(row.ID) && row.IsDeleted == false
                                 select row).ToList();

                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    foreach (var item in hierarchy)
                    {
                        LoadSubEntities(item, false, entities);
                    }


                }
                else
                {
                    var customerIds = (from row in entities.InternalComplianceAssignments
                                       join ci in entities.InternalComplianceInstances
                                       on row.InternalComplianceInstanceID equals ci.ID
                                       where row.UserID == userID && row.RoleID == role
                                       select ci.CustomerBranchID).Distinct();

                    var query = (from row in entities.CustomerBranches
                                 where customerIds.Contains(row.ID) && row.IsDeleted == false
                                 select row).ToList();

                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    foreach (var item in hierarchy)
                    {
                        LoadSubEntities(item, false, entities);
                    }
                }

            }

            return hierarchy;
        }

        public static DataTable GetCompliancesStatusRiskWiseInternal(int barnchId, int year, int month, int userId = -1, bool approver = false)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if (month < 4)
               {
                   startDateyear--;
               }

               DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));

               var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        where row.CustomerBranchID == barnchId && row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                        select row).ToList();
               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               transactionsQuery = transactionsQuery.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
               long delayedcount;
               long Completedcount;
               long pendingcount;
               long totalcount;

               DataTable table = new DataTable();
               table.Columns.Add("ID", typeof(int));
               table.Columns.Add("RiskCatagory", typeof(string));
               table.Columns.Add("Completed", typeof(long));
               table.Columns.Add("Delayed", typeof(long));
               table.Columns.Add("Pending", typeof(long));
               table.Columns.Add("Total", typeof(long));

               // for Heigh risk Compliances
               delayedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 0).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 0).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10) && entry.Risk == 0).Count();
               totalcount = delayedcount + Completedcount + pendingcount;

               if (totalcount != 0)
                   table.Rows.Add(0, "High", Completedcount, delayedcount, pendingcount, totalcount);

               // for Medium risk Compliances
               delayedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 1).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 1).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10) && entry.Risk == 1).Count();
               totalcount = delayedcount + Completedcount + pendingcount;

               if (totalcount != 0)
                   table.Rows.Add(1, "Medium", Completedcount, delayedcount, pendingcount, totalcount);

               // for Low risk Compliances
               delayedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 2).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 2).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10) && entry.Risk == 2).Count();
               totalcount = delayedcount + Completedcount + pendingcount;

               if (totalcount != 0)
                   table.Rows.Add(2, "Low", Completedcount, delayedcount, pendingcount, totalcount);

               return table;


           }
       }

       public static DataTable GetCompliancesStatusFunctionWiseInternal(int Customerid,int barnchId, int year, int month, int userId = -1, bool approver = false)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if (month < 4)
               {
                   startDateyear--;
               }
               DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));

               var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        //join acte in entities.Acts
                                       // on row.ActID equals acte.ID
                                        where row.CustomerBranchID == barnchId && row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                        select new
                                        {
                                            ComplianceInstanceID = row.InternalComplianceInstanceID,
                                            row.InternalComplianceID,
                                            //acte.ComplianceCategoryId,
                                            row.IComplianceCategoryID,
                                            row.InternalComplianceStatusID,
                                            row.InternalScheduledOn,
                                            row.InternalScheduledOnID,
                                            row.RoleID,
                                            row.UserID
                                        }).ToList();

               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               transactionsQuery = transactionsQuery.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

               long delayedcount;
               long CompletedCount;
               long pendingcount;
               long totalcount;
               //DateTime now = DateTime.UtcNow.Date;

               DataTable table = new DataTable();
               table.Columns.Add("ID", typeof(int));
               table.Columns.Add("RiskCatagory", typeof(string));
               table.Columns.Add("Completed", typeof(long));
               table.Columns.Add("Delayed", typeof(long));
               table.Columns.Add("Pending", typeof(long));
               table.Columns.Add("Total", typeof(long));

            
               var CatagoryList = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(Customerid);

               foreach (InternalCompliancesCategory cc in CatagoryList)
               {
                   delayedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.IComplianceCategoryID == cc.ID).Count();
                   CompletedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.IComplianceCategoryID == cc.ID).Count();
                   pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10) && entry.IComplianceCategoryID == cc.ID).Count();                  
                   totalcount = delayedcount + CompletedCount + pendingcount;

                   if (totalcount != 0)
                       table.Rows.Add(cc.ID, cc.Name, CompletedCount, delayedcount, pendingcount, totalcount);

               }

               return table;


           }
       }


       public static DataTable GetManagementCompliancesSummaryInternal(int customerid , int barnchId, int year, int month, int userId = -1, bool approver = false)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if (month < 4)
               {
                   startDateyear--;
               }
               DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
               var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                       // join acte in entities.Acts
                                        //on row.ActID equals acte.ID
                                        where row.CustomerBranchID == barnchId && row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                        select new
                                        {
                                            ComplianceInstanceID = row.InternalComplianceInstanceID,
                                            row.InternalComplianceID,
                                            //acte.ComplianceCategoryId,
                                            row.IComplianceCategoryID,
                                            row.InternalComplianceStatusID,
                                            row.InternalScheduledOn,
                                            row.Risk,
                                            row.InternalScheduledOnID,
                                            row.RoleID,
                                            row.UserID
                                        }).ToList();

               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               transactionsQuery = transactionsQuery.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

               long highcount;
               long mediumCount;
               long lowcount;
               long totalcount;
               //DateTime now = DateTime.UtcNow.Date;

               DataTable table = new DataTable();
               table.Columns.Add("ID", typeof(int));
               table.Columns.Add("Catagory", typeof(string));
               table.Columns.Add("High", typeof(long));
               table.Columns.Add("Medium", typeof(long));
               table.Columns.Add("Low", typeof(long));
               table.Columns.Add("Total", typeof(long));

               var CatagoryList = ComplianceCategoryManagement.GetAllInternalCompliancesCategories(customerid);

               foreach (InternalCompliancesCategory cc in CatagoryList)
               {
                   highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.IComplianceCategoryID == cc.ID && entry.InternalComplianceStatusID != 11).Count();
                   mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.IComplianceCategoryID == cc.ID && entry.InternalComplianceStatusID != 11).Count();
                   lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.IComplianceCategoryID == cc.ID && entry.InternalComplianceStatusID != 11).Count();

       
                   totalcount = highcount + mediumCount + lowcount;

                   if (totalcount != 0)
                       table.Rows.Add(cc.ID, cc.Name, highcount, mediumCount, lowcount, totalcount);

               }

               return table;


           }
       }

       public static DataTable GetPastTwelveMonthSummaryInternal(int barnchId, int year, int month, int userId = -1, bool approver = false)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if (month < 4)
               {
                   startDateyear--;
               }
               DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);

               var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                        where row.CustomerBranchID == barnchId
                                        select row).ToList();

              

               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               transactionsQuery = transactionsQuery.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

               DataTable table = new DataTable();
               table.Columns.Add("Compliances", typeof(string));
               for (int i = 1; i < 13; i++)
               {
                   //table.Columns.Add(EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                   table.Columns.Add(i.ToString(), typeof(string));
               }


               long monthCount = 0;
               DataRow CompletedInTime = table.NewRow();
               CompletedInTime["Compliances"] = "Completed In Time";
               for (int i = 1; i < 13; i++)
               {
                   DateTime previousDate = EndDate.AddMonths(-i);
                   DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                   DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));
                   monthCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.InternalScheduledOn >= date1 && entry.InternalScheduledOn <= date2).Count();
                   //CompletedInTime[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = monthCount;
                   CompletedInTime[i] = monthCount;
               }
               table.Rows.Add(CompletedInTime);


               DataRow DelayedTime = table.NewRow();
               DelayedTime["Compliances"] = "Completed after Due Date";
               for (int i = 1; i < 13; i++)
               {
                   DateTime previousDate = EndDate.AddMonths(-i);
                   DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                   DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));
                   monthCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.InternalScheduledOn >= date1 && entry.InternalScheduledOn <= date2).Count();
                   //DelayedTime[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = monthCount;
                   DelayedTime[i] = monthCount;
               }
               table.Rows.Add(DelayedTime);


               DataRow NotCompletedTime = table.NewRow();
               NotCompletedTime["Compliances"] = "Not Yet Completed";
               for (int i = 1; i < 13; i++)
               {
                   DateTime previousDate = EndDate.AddMonths(-i);
                   DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                   DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));

                   monthCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10)
                                && entry.InternalScheduledOn >= date1 && entry.InternalScheduledOn <= date2).Count();

                   //NotCompletedTime[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = monthCount;
                   NotCompletedTime[i] = monthCount;
               }
               table.Rows.Add(NotCompletedTime);

               return table;


           }
       }

       public static List<InternalComplianceDashboardSummaryView> GetManagementDetailViewInternal(int barnchId, int year, int month, List<int> statusIDs, string filter, int? functionId, string TwelveMonth = "")
       {
           int startDateyear = year;
           if (month < 4)
           {
               startDateyear--;
           }
           List<InternalComplianceDashboardSummaryView> detailView = new List<InternalComplianceDashboardSummaryView>();
           DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
           DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));

           if (filter.Equals("Summary"))
           {
               if (functionId != -1)
               {
                   detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal()
                              .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.Risk) && entry.InternalScheduledOn >= startDate && entry.InternalScheduledOn <= EndDate
                              && entry.IComplianceCategoryID == functionId)
                              .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                   
               }
               else
               {
                   detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal()
                             .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.Risk) && entry.InternalScheduledOn >= startDate && entry.InternalScheduledOn <= EndDate)
                             .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
               }
           }
           else if (filter.Equals("TwelveMonthSummary"))
           {
               string[] token = TwelveMonth.Split('/');
               int fourDigitYear = CultureInfo.CurrentCulture.Calendar.ToFourDigitYear(Convert.ToInt32(token[1]));
               DateTime sdate = new DateTime(fourDigitYear, Convert.ToInt32(token[0]), 1);
               DateTime edate = new DateTime(fourDigitYear, Convert.ToInt32(token[0]), DateTime.DaysInMonth(fourDigitYear, Convert.ToInt32(token[0])));
               detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal()
                           .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.InternalComplianceStatusID) && entry.InternalScheduledOn >= sdate && entry.InternalScheduledOn <= edate)
                           .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
           }
           else
           {

               detailView = InternalDashboardManagement.GetComplianceDashboardSummaryInternal()
                               .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.InternalComplianceStatusID) && entry.InternalScheduledOn >= startDate && entry.InternalScheduledOn <= EndDate)
                               .GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

               if (filter.Equals("Risk"))
               {
                   if (functionId != -1)
                   {
                       detailView = detailView.Where(entry => entry.Risk == functionId).ToList();
                   }

               }
               else if (filter.Equals("Function"))
               {
                   if (functionId != -1)
                   {
                       //detailView = detailView.Where(entry => entry.ComplianceCategoryId == functionId).ToList();
                       //detailView = detailView.Where(entry => entry.ComplianceCategoryId == functionId).ToList();
                   }
               }

           }

           return detailView;
       }

       //for graph
       public static DataTable GetCompliancesForGraphFunctionWiseInternal(int barnchId, int categoryID, int year, int month, int userId = -1, bool approver = false)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if (month < 4)
               {
                   startDateyear--;
               }

               DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));

               var transactionsQuery = (from row in entities.InternalComplianceDashboardSummaryViews
                                        where row.CustomerBranchID == barnchId && row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate && row.IComplianceCategoryID == categoryID
                                        select row).ToList();
               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               transactionsQuery = transactionsQuery.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
               long delayedcount;
               long Completedcount;
               long pendingcount;
               long totalcount;

               DataTable table = new DataTable();
               table.Columns.Add("RiskCatagory", typeof(string));
               table.Columns.Add("Completed", typeof(long));
               table.Columns.Add("Delayed", typeof(long));
               table.Columns.Add("Pending", typeof(long));
               table.Columns.Add("Total", typeof(long));

               // for Heigh risk Compliances
               delayedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 0).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 0).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10) && entry.Risk == 0).Count();
               totalcount = delayedcount + Completedcount + pendingcount;
               table.Rows.Add("High", Completedcount, delayedcount, pendingcount, totalcount);

               // for Medium risk Compliances
               delayedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 1).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 1).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10) && entry.Risk == 1).Count();
               totalcount = delayedcount + Completedcount + pendingcount;
               table.Rows.Add("Medium", Completedcount, delayedcount, pendingcount, totalcount);

               // for Low risk Compliances
               delayedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.Risk == 2).Count();
               Completedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.Risk == 2).Count();
               pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10) && entry.Risk == 2).Count();
               totalcount = delayedcount + Completedcount + pendingcount;
               table.Rows.Add("Low", Completedcount, delayedcount, pendingcount, totalcount);


               return table;



           }
       }

       public static int GetGradeForManagementDashboardInternal(int barnchId, int categoryID, int year, int month, int userId = -1, bool approver = false)
       {

           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               int startDateyear = year;
               if (month < 4)
               {
                   startDateyear--;
               }

               int color = 0;
               DateTime startDate = new DateTime(startDateyear, Util.FinancialMonth(), 1);
               DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));

               var transactionsQuery = (from row in entities.InternalComplianceDashboardSummaryViews
                                        where row.CustomerBranchID == barnchId && row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate && row.IComplianceCategoryID == categoryID
                                       // where row.CustomerBranchID == barnchId && row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                        select row).ToList();
               if (approver == true)
               {
                   transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
               }

               long highRisktotalCount = transactionsQuery.Where(entry => entry.Risk == 0).Count();
               long MediumRisktotalCount = transactionsQuery.Where(entry => entry.Risk == 1).Count();
               long LowRisktotalCount = transactionsQuery.Where(entry => entry.Risk == 2).Count();

               long highRiskCompletedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 9) && entry.Risk == 0).Count();
               long MediumRiskCompletedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 9) && entry.Risk == 1).Count();
               long LowRiskCompletedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 9) && entry.Risk == 2).Count();

               double seventyfivePercentLow = (0.75 * LowRisktotalCount);
               double seventyfivePercentHigh = (0.75 * highRisktotalCount);

               if (highRisktotalCount != 0 || MediumRisktotalCount != 0 || LowRiskCompletedCount != 0)
               {
                   if (highRisktotalCount == highRiskCompletedCount && MediumRisktotalCount == MediumRiskCompletedCount && LowRiskCompletedCount >= seventyfivePercentLow)
                   {
                       color = 1;
                   }
                   else if (highRiskCompletedCount >= seventyfivePercentHigh && MediumRiskCompletedCount >= (MediumRisktotalCount / 2) && LowRiskCompletedCount >= (LowRisktotalCount / 2))
                   {
                       color = 2;
                   }
                   else
                   {
                       color = 3;
                   }
               }
               else
               {
                   color = 3;
               }

               return color;
           }

       }
        public static DataTable GetGradingReportOfManagement(int customerid, int userId, int year, int month, int period, int BranchID, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime startDate = new DateTime(year, month, 1).AddMonths(-period);
                DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);
                entities.Database.CommandTimeout = 180;
                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                List<NameValueHierarchy> bracnhes = new List<NameValueHierarchy>();
                if (BranchID != -1)
                {
                    if (approver == true)
                    {
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && row.RoleID == 6 && row.UserID == userId
                                             && row.CustomerBranchID == BranchID
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        bracnhes = AssignEntityManagement.GetAssignedLocationUserAndRoleWise(6, BranchID, userId);
                    }
                    else
                    {
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             join row2 in entities.EntitiesAssignments
                                             on row.CustomerBranchID equals row2.BranchID
                                             where row.CustomerID == customerid && row2.UserID == userId
                                             && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && row.CustomerBranchID == BranchID
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        bracnhes = AssignEntityManagement.GetLocation(BranchID, userId);
                    }
                }
                else
                {
                    if (approver == true)
                    {
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             && row.RoleID == 6 && row.UserID == userId
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        bracnhes = AssignEntityManagement.GetAssignedLocationUserAndRoleWise(6, userId);
                    }
                    else
                    {
                        transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                             join row2 in entities.EntitiesAssignments
                                             on row.CustomerBranchID equals row2.BranchID
                                             where row.CustomerID == customerid && row2.UserID == userId
                                             && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                             && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        bracnhes = AssignEntityManagement.GetLocation(userId);
                    }
                }
                DataTable table = new DataTable();
                table.Columns.Add("Location", typeof(string));
                table.Columns.Add("Approver", typeof(string));

                // for (int i = period; i >= 1; i--)
                for (int i = 1; i <= period; i++)
                {
                    table.Columns.Add(EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                    // table.Columns.Add(EndDate.AddMonths(i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                }

                foreach (var item in bracnhes)
                {
                    item.Level = 0;
                    DataRow tableRow = BindingGradingTableRow(transactionsQuery, item, table, period, EndDate, customerid);
                    table.Rows.Add(tableRow);
                    BindBranchesHierarchy(item, transactionsQuery, table, period, EndDate, customerid);
                }

                return table;


            }
        }
        public static List<long> BranchlistGrading = new List<long>();
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchyGreding(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    BranchlistGrading.Add(item.ID);
                    LoadSubEntitiesGreding(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntitiesGreding(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                BranchlistGrading.Add(item.ID);
                LoadSubEntitiesGreding(customerid, item, false, entities);
            }
        }

        public static DataTable GetGradingReportOfManagement(int customerid, int userId, int year, int month, int period, int BranchID,List<SP_StatutoryGradingReport_Result> MasterTransactionQuery, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime startDate = new DateTime(year, month, 1).AddMonths(-period);
                DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);
                entities.Database.CommandTimeout = 180;
                List<SP_StatutoryGradingReport_Result> transactionsQuery = new List<SP_StatutoryGradingReport_Result>();                
                List<NameValueHierarchy> bracnhes = new List<NameValueHierarchy>();
                BranchlistGrading.Clear();
                if (BranchID != -1)
                {
                    if (approver == true)
                    {
                        transactionsQuery = (from row in MasterTransactionQuery
                                             where row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate                                             
                                             && row.RoleID == 6 && row.UserID == userId
                                             && row.CustomerBranchID == BranchID
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        
                        bracnhes = AssignEntityManagement.GetAssignedLocationUserAndRoleWise(6, BranchID, userId);
                    }
                    else
                    {
                        GetAllHierarchyGreding(customerid, BranchID);
                        if (BranchlistGrading.Count > 0)
                        {
                            transactionsQuery = (from row in MasterTransactionQuery
                                                 join row2 in entities.EntitiesAssignments
                                                 on row.CustomerBranchID equals row2.BranchID
                                                 where row2.UserID == userId
                                                 && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                                  && BranchlistGrading.Contains(row.CustomerBranchID)
                                                 select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                            bracnhes = AssignEntityManagement.GetLocationNew(BranchID, userId, BranchlistGrading);                            
                        }
                        else
                        {
                            transactionsQuery = (from row in MasterTransactionQuery
                                                 join row2 in entities.EntitiesAssignments
                                                 on row.CustomerBranchID equals row2.BranchID
                                                 where row2.UserID == userId
                                                 && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                                 && row.CustomerBranchID == BranchID
                                                 select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();                            

                            BranchlistGrading.Add(BranchID);                           
                            bracnhes = AssignEntityManagement.GetLocationNew(BranchID, userId, BranchlistGrading);
                        }
                                                
                    }
                }
                else
                {
                    if (approver == true)
                    {
                        transactionsQuery = (from row in MasterTransactionQuery
                                             where row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                             && row.RoleID == 6 && row.UserID == userId                                             
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        
                        bracnhes = AssignEntityManagement.GetAssignedLocationUserAndRoleWise(6, BranchID, userId);
                    }
                    else
                    {
                        transactionsQuery = (from row in MasterTransactionQuery
                                             join row2 in entities.EntitiesAssignments
                                             on row.CustomerBranchID equals row2.BranchID
                                             where row2.UserID == userId
                                             && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate                                             
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        
                        bracnhes = AssignEntityManagement.GetLocationNew(BranchID, userId);
                    }
                }
                DataTable table = new DataTable();
                table.Columns.Add("Location", typeof(string));
                //table.Columns.Add("Approver", typeof(string));

                // for (int i = period; i >= 1; i--)
                for (int i = 1; i <= period; i++)
                {
                    table.Columns.Add(EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                    // table.Columns.Add(EndDate.AddMonths(i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                }
               
                foreach (var item in bracnhes)
                {
                    item.Level = 0;
                    DataRow tableRow = BindingGradingTableRow(transactionsQuery, item, table, period, EndDate, customerid);
                    table.Rows.Add(tableRow);
                    //BindBranchesHierarchy(item, transactionsQuery, table, period, EndDate, customerid);
                }

                return table;


            }
        }
        public static DataTable GetGradingReportOfManagementInternal(int customerid, int userId, int year, int month, int period, int BranchID, List<long> gbranchlist, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (gbranchlist != null)
                {
                    DateTime startDate = new DateTime(year, month, 1).AddMonths(-period);
                    DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);
                    List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                    List<NameValueHierarchy> bracnhes = new List<NameValueHierarchy>();
                    if (gbranchlist.Count > 0)
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                                 where row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                                 && row.RoleID == 6 && row.UserID == userId
                                                  && gbranchlist.Contains((long)row.CustomerBranchID)
                                                 select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                            bracnhes = AssignEntityManagement.GetAssignedLocationUserAndRoleWiseInternal(6, BranchID, userId);
                        }
                        else
                        {
                            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                                 join row2 in entities.EntitiesAssignmentInternals
                                                 on (long)row.CustomerBranchID equals row2.BranchID
                                                 where row2.UserID == userId && gbranchlist.Contains((long)row.CustomerBranchID)
                                                 && row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                                 select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                            bracnhes = AssignEntityManagement.GetLocationInternalNew(BranchID, userId);
                        }
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                                 where row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                                 && row.RoleID == 6 && row.UserID == userId
                                                 select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                            bracnhes = AssignEntityManagement.GetAssignedLocationUserAndRoleWiseInternal(6, BranchID, userId);
                        }
                        else
                        {
                            transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                                 join row2 in entities.EntitiesAssignmentInternals
                                                 on (long)row.CustomerBranchID equals row2.BranchID
                                                 where row2.UserID == userId && row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                                 select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                            bracnhes = AssignEntityManagement.GetLocationInternalNew(BranchID, userId);
                        }

                    }

                    DataTable table = new DataTable();
                    table.Columns.Add("Location", typeof(string));
                    //table.Columns.Add("Approver", typeof(string));


                    // for (int i = period; i >= 1; i--)
                    for (int i = 1; i <= period; i++)
                    {
                        table.Columns.Add(EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                        // table.Columns.Add(EndDate.AddMonths(i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                    }
                    foreach (var item in bracnhes)
                    {
                        item.Level = 0;
                        DataRow tableRow = BindingGradingTableRowInternal(transactionsQuery, item, table, period, EndDate, customerid);
                        table.Rows.Add(tableRow);
                        //BindBranchesHierarchyInternal(item, transactionsQuery, table, period, EndDate, customerid);
                    }
                    return table;
                }
                else
                {
                    return null;
                }
            }
        }        
        private static DataRow BindingGradingTableRowInternal(List<InternalComplianceInstanceTransactionView> transactionsQuery, NameValueHierarchy item, DataTable table, int period, DateTime EndDate, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                //comment by rahul on 2 MAY 2018
                //var branchIDs = (from row in entities.CustomerBranches
                //                 where row.ParentID == item.ID
                //                 select row.ID).ToList();
                //branchIDs.Add(item.ID);

                var branchIDs = (from row in entities.CustomerBranches
                                 where row.ID == item.ID
                                 select row.ID).ToList();
                int color;
                DataRow tableRow = table.NewRow();

                tableRow["Location"] = item.Name + "#" + item.Level;
                //tableRow["Approver"] = ""; // GetApproverNameInternal(item.ID) != null ? GetApproverNameInternal(item.ID).User : "";
                for (int i = period; i >= 1; i--)
                {
                    DateTime previousDate = EndDate.AddMonths(-i);
                    DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                    DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));

                    long highRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 0 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();
                    long MediumRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 1 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();
                    long LowRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 2 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();

                    long highRiskCompletedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 9) && branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 0 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();
                    long MediumRiskCompletedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 9) && branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 1 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();
                    long LowRiskCompletedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 9) && branchIDs.Contains((int) entry.CustomerBranchID) && entry.Risk == 2 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();

                    double seventyfivePercentLow = (0.75 * LowRisktotalCount);
                    double seventyfivePercentHigh = (0.75 * highRisktotalCount);

                    if (highRisktotalCount != 0 || MediumRisktotalCount != 0 || LowRiskCompletedCount != 0)
                    {
                        if (highRisktotalCount == highRiskCompletedCount && MediumRisktotalCount == MediumRiskCompletedCount && LowRiskCompletedCount >= seventyfivePercentLow)
                        {
                            color = 1;
                        }
                        else if (highRiskCompletedCount >= seventyfivePercentHigh && MediumRiskCompletedCount >= (MediumRisktotalCount / 2) && LowRiskCompletedCount >= (LowRisktotalCount / 2))
                        {
                            color = 2;
                        }
                        else
                        {
                            color = 3;
                        }
                    }
                    else
                    {
                        color = 4; 
                    }
                    tableRow[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = color;
                }
                return tableRow;

            }
        }

        private static void BindBranchesHierarchyInternal(NameValueHierarchy nvp, List<InternalComplianceInstanceTransactionView> transactionsQuery, DataTable table, int period, DateTime EndDate, int customerid)
        {
            foreach (var item in nvp.Children)
            {
                item.Level = nvp.Level + 1;
                DataRow tableRow = BindingGradingTableRowInternal(transactionsQuery, item, table, period, EndDate, customerid);
                table.Rows.Add(tableRow);
                BindBranchesHierarchyInternal(item, transactionsQuery, table, period, EndDate, customerid);
            }
        }

        public static DataTable GetManagementSummaryStatusReportInternal(int userId, int year, int month, int period, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                DateTime startDate = new DateTime(year, month, 1).AddMonths(-period);
                DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);

                var transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                         select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                DataTable table = new DataTable();
                table.Columns.Add("Location", typeof(string));
                table.Columns.Add("Approver", typeof(string));

                for (int i = period; i >= 1; i--)
                {
                    table.Columns.Add("Completed In Time_" + i, typeof(string));
                    table.Columns.Add("Completed After Due Date_" + i, typeof(long));
                    table.Columns.Add("Not Yet Completed_" + i, typeof(long));
                    table.Columns.Add("Total_" + i, typeof(long));
                    table.Columns.Add("Rating_" + i, typeof(string));
                    //table.Columns.Add(DateTime.UtcNow.AddMonths(-i).ToString("MMM") + " - " + DateTime.UtcNow.AddMonths(-i).Year, typeof(long));
                }

                List<NameValueHierarchy> bracnhes = new List<NameValueHierarchy>();
                if (approver == true)
                {
                    transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    bracnhes = AssignEntityManagement.GetAssignedLocationUserAndRoleWiseInternal(6, -1, userId);
                }
                else
                {
                    bracnhes = AssignEntityManagement.GetLocationInternal(-1, userId);
                }

                foreach (var item in bracnhes)
                {
                    item.Level = 0;
                    DataRow tableRow = BindingManagementSummaryStatusTableRowInternal(transactionsQuery, item, table, period, EndDate);
                    table.Rows.Add(tableRow);
                    BindSumaryStatusChieldHierarchyInternal(item, transactionsQuery, table, period, EndDate);
                }

                return table;


            }
        }

        private static DataRow BindingManagementSummaryStatusTableRowInternal(List<InternalComplianceInstanceTransactionView> transactionsQuery, NameValueHierarchy item, DataTable table, int period, DateTime EndDate)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {

               var branchIDs = (from row in entities.CustomerBranches
                                where row.ParentID == item.ID
                                select row.ID).ToList();
               branchIDs.Add(item.ID);

               string color;
               DataRow tableRow = table.NewRow();

               tableRow["Location"] = item.Name + "#" + item.Level;
               tableRow["Approver"] = GetApproverNameInternal(item.ID) != null ? GetApproverNameInternal(item.ID).User : "";
               for (int i = period; i >= 1; i--)
               {
                   DateTime previousDate = EndDate.AddMonths(-i);
                   DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                   DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));



                   long Completedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.InternalScheduledOn >= date1 && entry.InternalScheduledOn <= date2).Count();
                   tableRow["Completed In Time_" + i] = Completedcount;

                   long delayedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 9 || entry.InternalComplianceStatusID == 5) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.InternalScheduledOn >= date1 && entry.InternalScheduledOn <= date2).Count();
                   tableRow["Completed After Due Date_" + i] = delayedcount;

                   long pendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10)
                                       && branchIDs.Contains((int)entry.CustomerBranchID) && entry.InternalScheduledOn >= date1 && entry.InternalScheduledOn <= date2).Count();
                   tableRow["Not Yet Completed_" + i] = pendingcount;

                   long totalcount = delayedcount + Completedcount + pendingcount;
                   tableRow["Total_" + i] = totalcount;

                   long highRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 0 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();
                   long MediumRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 1 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();
                   long LowRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 2 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();

                   long highRiskCompletedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 0 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();
                   long MediumRiskCompletedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 1 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();
                   long LowRiskCompletedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 2 && entry.InternalScheduledOn > date1 && entry.InternalScheduledOn < date2).Count();

                   double seventyfivePercentLow = (0.75 * LowRisktotalCount);
                   double seventyfivePercentHigh = (0.75 * highRisktotalCount);
                   if (highRisktotalCount != 0 || MediumRisktotalCount != 0 || LowRisktotalCount != 0)
                   {
                       if (highRisktotalCount == highRiskCompletedCount && MediumRisktotalCount == MediumRiskCompletedCount && LowRiskCompletedCount >= seventyfivePercentLow)
                       {
                           color = "GREEN";
                       }
                       else if (highRiskCompletedCount >= seventyfivePercentHigh && MediumRiskCompletedCount >= (MediumRisktotalCount / 2) && LowRiskCompletedCount >= (LowRisktotalCount / 2))
                       {
                           color = "BROWN";
                       }
                       else
                       {
                           color = "RED";
                       }
                   }
                   else
                   {
                       color = "RED";
                   }

                   tableRow["Rating_" + i] = color;

               }


               return tableRow;

           }
       }
       private static void BindSumaryStatusChieldHierarchyInternal(NameValueHierarchy nvp, List<InternalComplianceInstanceTransactionView> transactionsQuery, DataTable table, int period, DateTime EndDate)
       {
           
           foreach (var item in nvp.Children)
           {
               item.Level = nvp.Level + 1;
               DataRow tableRow = BindingManagementSummaryStatusTableRowInternal(transactionsQuery, item, table, period, EndDate);
               table.Rows.Add(tableRow);
               BindSumaryStatusChieldHierarchyInternal(item, transactionsQuery, table, period, EndDate);
           }

       }

       public static EntitiesAssignmentInternal SelectEntityInternal(int branchId = -1, int userID = -1, int catagoryId = -1)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var EntitiesAssignmentData = (from row in entities.EntitiesAssignmentInternals
                                             where row.BranchID == branchId && row.UserID == userID && row.ComplianceCatagoryID == catagoryId
                                             select row).FirstOrDefault();
               return EntitiesAssignmentData;
           }
       }
       public static void CreateInternal(EntitiesAssignmentInternal objEntitiesAssignment)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               entities.EntitiesAssignmentInternals.Add(objEntitiesAssignment);
               entities.SaveChanges();
           }
       }

       public static void CreateInternal(List<EntitiesAssignmentInternal> objEntitiesAssignment)
       {

           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               objEntitiesAssignment.ForEach(entry =>
               {
                   entities.EntitiesAssignmentInternals.Add(entry);
               });
               entities.SaveChanges();
           }

       }
        public static List<InternalComplianceAssignmentEntitiesView> SelectAllEntitiesInternalList(int userID, int CustomerID, List<long> CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceTransactionEntity = (from row in entities.InternalComplianceAssignmentEntitiesViews
                                                   select row).ToList();

                if (userID != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
                }

                if (CustomerBranchID.Count > 0)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => CustomerBranchID.Contains((int)entry.BranchID)).ToList();
                }
                return ComplianceTransactionEntity;
            }
        }
        public static List<InternalComplianceAssignmentEntitiesView> SelectAllEntitiesInternal(int branchId = -1, int userID = -1, int customerID = -1)
       {
           using (ComplianceDBEntities entities = new ComplianceDBEntities())
           {
               var ComplianceTransactionEntity = (from row in entities.InternalComplianceAssignmentEntitiesViews
                                                  select row).ToList();

               var branchIds = (from row in entities.CustomerBranches
                                where row.IsDeleted == false && row.CustomerID == customerID
                                select row.ID).ToList();

               if (branchIds != null)
               {
                   ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => branchIds.Contains((int)entry.BranchID)).ToList();
               }
               if (branchId != -1)
               {
                   ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.BranchID == branchId).ToList();
               }
               if (userID != -1)
               {
                   ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
               }

               return ComplianceTransactionEntity;
           }
       }

    }
}
