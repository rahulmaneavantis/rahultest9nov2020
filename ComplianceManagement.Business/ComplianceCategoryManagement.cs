﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ComplianceCategoryManagement
    {
        public static List<ComplianceCategory> GetEventBasedComplianceCategories(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.ComplianceCategories
                                           join row1 in entities.Acts
                                           on row.ID equals row1.ComplianceCategoryId
                                           join row2 in entities.Compliances
                                           on row1.ID equals row2.ActID
                                           where row2.IsDeleted == false
                                           && row2.EventFlag != null
                                           select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceCategorys = complianceCategorys.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }


                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

                var complianceCategoryList = complianceCategorys.Distinct().ToList();

                return complianceCategoryList.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static void CreateInternalCompliancesCategory(List<InternalCompliancesCategory> complianceCatagory)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                complianceCatagory.ForEach(entry =>
                {
                    entities.InternalCompliancesCategories.Add(entry);
                });
                entities.SaveChanges();
            }

        }
        public static bool ExistsInternalComplianceCategory(string Name, int? CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalCompliancesCategories
                             where row.Name.Equals(Name) && row.CustomerID == CustomerID
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                //return query.Select(entry => true).First();
            }
        }
        public static List<ComplianceSubType> GetComplianceSubTypes(int NatureOfComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.ComplianceSubTypes
                                           where row.NatureOfComplianceID == NatureOfComplianceID
                                           select row);

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }               
        public static List<sp_ComplianceAssignedCategory_Result> GetNewAll(int UserID, int CustomerBranchId, List<long> Branchlist, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedCategory_Result> complianceCategorys = new List<sp_ComplianceAssignedCategory_Result>();
                if (approver == true)
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "APPR")
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();                        
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "APPR")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                        
                    }
                }
                else
                {                    
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "MGMT")
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();                        
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "MGMT")
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();


                    }
                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();
                   
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<InternalCompliancesCategory> GetFunctionDetailsInternal(int Userid, List<long> branchlist, int CustomerBranchId, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalCompliancesCategory> complianceCategorys = new List<InternalCompliancesCategory>();
                if (approver == true)
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               && branchlist.Contains((long)row1.CustomerBranchID) //row1.CustomerBranchID == CustomerBranchId
                                               && row1.RoleID == 6
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               && row1.RoleID == 6
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    List<InternalComplianceAssignedInstancesView> InternalComplianceCount = new List<InternalComplianceAssignedInstancesView>();
                    if (branchlist.Count > 0)
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   && row1.UserID == Userid && branchlist.Contains((long)row1.BranchID) //row1.BranchID == CustomerBranchId
                                                   select row).Distinct().ToList();
                    }
                    else
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   && row1.UserID == Userid

                                                   select row).Distinct().ToList();

                    }
                    InternalComplianceCount = InternalComplianceCount.GroupBy(a => (int?)a.InternalComplianceCategoryID).Select(a => a.FirstOrDefault()).ToList();
                    complianceCategorys = (from row in InternalComplianceCount
                                           join row1 in entities.InternalCompliancesCategories
                                           on row.InternalComplianceCategoryID equals row1.ID
                                           select row1).ToList();
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<ComplianceCategory> GetAll(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.ComplianceCategories
                                           //where row.IsDeleted == false
                                           select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceCategorys = complianceCategorys.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);
                
                return complianceCategorys.OrderBy(entry=>entry.Name).ToList();
            }
        }             
        public static List<InternalCompliancesCategory> GetAllInternal(int CustomerID,string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               where row.CustomerID == CustomerID
                                           select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceCategorys = complianceCategorys.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static ComplianceCategory GetByID(int complianceCategoryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategory = (from row in entities.ComplianceCategories
                                          where row.ID == complianceCategoryID
                                          select row).SingleOrDefault();

                return complianceCategory;
            }
        }
        public static void Delete(int complianceCategoryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                
                ComplianceCategory complianceCategoryToDelete = (from row in entities.ComplianceCategories
                                                                 where row.ID == complianceCategoryID
                                                                 select row).FirstOrDefault();

                entities.ComplianceCategories.Remove(complianceCategoryToDelete);

                entities.SaveChanges();
            }
        }
        public static bool Exists(ComplianceCategory complianceCategory)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceCategories
                             where row.Name.Equals(complianceCategory.Name)
                             select row);

                if (complianceCategory.ID > 0)
                {
                    query = query.Where(entry => entry.ID != complianceCategory.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void Update(ComplianceCategory complianceCategory)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                
                ComplianceCategory complianceCategoryToUpdate = (from row in entities.ComplianceCategories
                                                                 where row.ID == complianceCategory.ID
                                                                 select row).FirstOrDefault();
                complianceCategoryToUpdate.UpdatedOn = DateTime.Now;
                complianceCategoryToUpdate.Name = complianceCategory.Name;
                complianceCategoryToUpdate.Description = complianceCategory.Description;
                entities.SaveChanges();
            }
        }    
        public static void Create(ComplianceCategory complianceCategory)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                complianceCategory.CreatedOn = DateTime.Now;
                complianceCategory.UpdatedOn = DateTime.Now;
                entities.ComplianceCategories.Add(complianceCategory);

                entities.SaveChanges();
            }
        }
        public static bool CanDelete(int categoryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var exists = (from row in entities.Acts
                             where row.ComplianceCategoryId == categoryID
                             select true).FirstOrDefault();

                return !exists;
            }
        }        
        public static int GetIdByName(string complianceCatagory)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategoryID = (from row in entities.ComplianceCategories
                                            where row.Name.ToUpper().Trim().Equals(complianceCatagory.ToUpper().Trim())
                                            select row.ID).SingleOrDefault();

                return complianceCategoryID;
            }
        }
        public static void Create(List<ComplianceCategory> complianceCatagory)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                complianceCatagory.ForEach(entry =>
                {
                    entities.ComplianceCategories.Add(entry);
                });
                entities.SaveChanges();
            }

        }
        public static bool Exists(string ComplianceCategoryName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceCategories
                             where row.Name.Contains(ComplianceCategoryName)
                             select row);

                return query.Select(entry => true).SingleOrDefault();
            }
        }             
        public static int GetIdByNameInternal(string complianceCatagory)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategoryID = (from row in entities.InternalCompliancesCategories
                                            where row.Name.ToUpper().Trim().Equals(complianceCatagory.ToUpper().Trim())
                                            select row.ID).SingleOrDefault();

                return complianceCategoryID;
            }
        }
        public static List<InternalCompliancesCategory> GetAll(int customerID,string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.InternalCompliancesCategories
                                           where row.CustomerID == customerID
                                           select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceCategorys = complianceCategorys.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static InternalCompliancesCategory GetInternalComplianceByID(int complianceCategoryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategory = (from row in entities.InternalCompliancesCategories
                                          where row.ID == complianceCategoryID
                                          select row).SingleOrDefault();

                return complianceCategory;
            }
        }
        public static void DeleteInternalCategory(int complianceCategoryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {   
                InternalCompliancesCategory complianceCategoryToDelete = (from row in entities.InternalCompliancesCategories
                                                                          where row.ID == complianceCategoryID
                                                                          select row).FirstOrDefault();

                entities.InternalCompliancesCategories.Remove(complianceCategoryToDelete);

                entities.SaveChanges();
            }
        }
        public static bool Exists(InternalCompliancesCategory complianceCategory,long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalCompliancesCategories
                             where row.Name.Equals(complianceCategory.Name)
                             && row.CustomerID == CustomerID
                             select row);

                if (complianceCategory.ID > 0)
                {
                    query = query.Where(entry => entry.ID != complianceCategory.ID);
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static void Update(InternalCompliancesCategory complianceCategory)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
              
                InternalCompliancesCategory complianceCategoryToUpdate = (from row in entities.InternalCompliancesCategories
                                                                          where row.ID == complianceCategory.ID
                                                                          select row).FirstOrDefault();

                complianceCategoryToUpdate.Name = complianceCategory.Name;
                complianceCategoryToUpdate.Description = complianceCategory.Description;
                complianceCategoryToUpdate.CustomerID = complianceCategory.CustomerID;
                entities.SaveChanges();
            }
        }
        public static void Create(InternalCompliancesCategory complianceCategory)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.InternalCompliancesCategories.Add(complianceCategory);

                entities.SaveChanges();
            }
        }        
        public static List<InternalCompliancesCategory> GetAllInternalCompliancesCategories(int CustomerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.InternalCompliancesCategories
                                           where row.CustomerID == CustomerID
                                           select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceCategorys = complianceCategorys.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
    }
}
