﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActiveEventDashboardsCA.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.ActiveEventDashboardsCA" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script type="text/javascript">
    $(function () {
        $('#divOptionalCompliances').dialog({
            height: 610,
            width: 865,
            autoOpen: false,
            draggable: true,
            title: "Active Event Compliance Details",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });
    });

    $(function () {
        $('#divShortNotice').dialog({
            height: 250,
            width: 500,
            autoOpen: false,
            draggable: true,
            title: "Do you want to conduct meeting by short notice?",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });
    });
    function initializeDatePicker(date) {
        var startDate = new Date();
        $(".StartDate").datepicker({
            dateFormat: 'dd-mm-yy',
            setDate: startDate,
            numberOfMonths: 1
        });
    }

    function setDate() {
        $(".StartDate").datepicker();
    }

    function initializeJQueryUI(textBoxID, divID) {
        $("#" + textBoxID).unbind('click');

        $("#" + textBoxID).click(function () {
            $("#" + divID).toggle("blind", null, 500, function () { });
        });
    }
    function postbackOnCheck() {
        var o = window.event.srcElement;
        if (o.tagName == 'INPUT' && o.type == 'checkbox' && o.name != null && o.name.indexOf('CheckBox') > -1)
        { __doPostBack("", ""); }
    }

    function divexpandcollapse(divname) {
        var div = document.getElementById(divname);
        var img = document.getElementById('img' + divname);
        if (div.style.display == "none") {
            div.style.display = "inline";
            img.src = "../Images/minus.gif";
        } else {
            div.style.display = "none";
            img.src = "../Images/plus.gif";
        }
    }
    function divexpandcollapseChild(divname) {
        var div1 = document.getElementById(divname);
        var img = document.getElementById('img' + divname);
        if (div1.style.display == "none") {
            div1.style.display = "inline";
            img.src = "../Images/minus.gif";
        } else {
            div1.style.display = "none";
            img.src = "../Images/plus.gif";;
        }
    }



</script>

<style>
    .circle {
        background-color: green;
        width: 10px;
        height: 10px;
        border-radius: 50%;
        display: inline-block;
        margin-right: 20px;
    }
</style>
<script type="text/javascript">
    function DeleteItem() {
        if (confirm("Are you sure you want to delete ...?")) {
            return true;
        }
        return false;
    }
</script>

<asp:UpdateProgress ID="updateProgress" runat="server">
    <ProgressTemplate>
        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<div style="margin-top: 40px">
    <div style="margin-bottom: 4px">
        <%-- <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />--%>
    </div>
    <asp:UpdatePanel ID="upEventInstanceList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table>
                <tr>
                    <td style="width: 95%">
                        <asp:Label ID="Label2" Text="Activated Events" runat="server" Font-Bold="true" Font-Size="Medium" ForeColor="#996633"></asp:Label>
                    </td>
                    <td style="width: 50px">
                        <asp:Button ID="btnBackMyEvent" CssClass="button" runat="server" Text="Back" OnClick="btnBackMyEvent_Click" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel3" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdEventList1" AutoGenerateColumns="false" GridLines="Vertical" OnRowCancelingEdit="grdEventList_RowCancelingEdit"
                    OnRowEditing="grdEventList_RowEditing" OnRowUpdating="grdEventList_RowUpdating"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdEventList_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdEventList_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdEventList_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderStyle-Height="22px" HeaderText="Location" SortExpression="CustomerBranchName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="lblAssignmnetId" runat="server" Visible="false" Text='<%# Eval("EventAssignmentID") %>' ToolTip='<%# Eval("EventAssignmentID") %>'></asp:Label>
                                    <asp:Label ID="lblEventScheduleOnID" runat="server" Visible="false" Text='<%# Eval("EventScheduledOnId") %>' ToolTip='<%# Eval("EventScheduledOnId") %>'></asp:Label>
                                    <asp:Label ID="lblEventInstanceID" runat="server" Visible="false" Text='<%# Eval("EventInstanceID") %>' ToolTip='<%# Eval("EventInstanceID") %>'></asp:Label>
                                    <asp:Label ID="lblBranch" runat="server" Visible="false" Text='<%# Eval("CustomerBranchID") %>' ToolTip='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                    <asp:Label ID="lblBranchName" runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" ItemStyle-Height="22px" SortExpression="Name">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label ID="lbleventName" runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nature of event" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Label ID="lbleventDesc" runat="server" Text='<%# Eval("EventDescription") %>' ToolTip='<%# Eval("EventDescription") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Held On" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="7%" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblHeldOn" runat="server" Text='<%# Convert.ToDateTime(Eval("HeldOn")).ToString("dd-MMM-yyyy") %>' ToolTip='<%# Convert.ToDateTime(Eval("HeldOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User" SortExpression="UserName">
                            <ItemTemplate>
                                <asp:Label ID="lblUserID" runat="server" Visible="false" Text='<%# Eval("UserID") %>' ToolTip='<%# Eval("UserID") %>'></asp:Label>
                                <asp:Label ID="lblUser" runat="server" Text='<%# Eval("UserName") %>' ToolTip='<%# Eval("UserName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Role" SortExpression="Role" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>' ToolTip='<%# Eval("Role") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btn_Update" runat="server" CommandName="Update" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Event"/></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <%--  <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Button ID="btn_Update" runat="server" Text="Edit" Visible='<%# visibleEdit(Convert.ToInt64(Eval("RoleID"))) %>' CommandName="Update" />
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divShortNotice">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin-bottom: 4px; margin-left: 95px">
                    <asp:Label runat="server" ID="lblMessage" ForeColor="Red"></asp:Label>
                </div>
                <div>
                    <div style="margin: 5px; margin-left: 165px;">
                        <asp:RadioButtonList ID="rbtnShortnotice" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="rbtnShortnotice_SelectedIndexChanged">
                            <asp:ListItem Text="Yes" Value="1" Selected="True" />
                            <asp:ListItem Text="No" Value="0" />
                        </asp:RadioButtonList>
                    </div>
                    <div style="margin-top: 25px; margin-left: 70px;" id="divDays" runat="server">
                        <asp:Label ID="lbl" Text="Short Notice Days" Width="125px" runat="server"></asp:Label>
                        <%-- <asp:Label ID="Label1" Font-Size="Large" Font-Bold="true" Text ="(-)" Width="16px" runat="server"></asp:Label>--%>
                        <asp:TextBox ID="txtShortNoticeDays" Width="80px" runat="server"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                            TargetControlID="txtShortNoticeDays" />
                    </div>
                    <div style="margin-top: 50px; margin-left: 125px">
                        <asp:Button Text="Save" runat="server" ValidationGroup="ComplianceValidationGroup" ID="btnSaveShortNotice" OnClick="btnSaveShortNotice_Click"
                            CssClass="button" />
                        <asp:Button Text="Close" runat="server" ID="btnShortClose" CssClass="button" OnClick="btnShortClose_Click" />
                    </div>
                </div>
                <asp:HiddenField ID="HiddenField1" runat="server" />
            </ContentTemplate>
            <%--     <Triggers>
                <asp:PostBackTrigger  ControlID="btnSaveShortNotice" />
            </Triggers>--%>
        </asp:UpdatePanel>
    </div>
    <div id="divOptionalCompliances">
        <asp:UpdatePanel ID="upOptionalCompliances" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary runat="server" CssClass="vdsummary"
                        ValidationGroup="ComplianceValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                    <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                </div>
                <div style="margin: 5px">
                    <asp:Panel ID="Panel1" Height="450px" ScrollBars="Auto" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <div style="margin-bottom: 7px;" runat="server" id="divSubEventmode">
                                        <div style="z-index: 10" id="divSubevent">
                                            <asp:gridview id="gvParentGrid" runat="server" onrowupdating="gvParentGrid_RowUpdating" gridlines="None" autogeneratecolumns="false"
                                                showfooter="true" width="790px" datakeynames="Type,Date"
                                                onrowdatabound="gvParentGrid_OnRowDataBound" xmlns:asp="#unknown">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("EventType") %>');">
                                                                <img id="imgdiv<%# Eval("EventType") %>" width="9px" border="0"
                                                                    src="../Images/plus.gif" alt="" /></a>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField ItemStyle-Width="150px" DataField="ParentEventID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Parent Event Name" HeaderStyle-HorizontalAlign="Left"/>
                                                    <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                         <ItemTemplate>
                                                            <%-- <asp:TextBox ID="txtgvParentGridDate" CssClass="StartDate" Text='<%# Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy") %>' Width="100px" runat="server"></asp:TextBox>--%>
                                                          <asp:TextBox ID="txtgvParentGridDate" CssClass="StartDate" BackColor ='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'  Enabled ='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' Text='<%# Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy") %>' Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                       <ItemTemplate>
                                                            <asp:Button ID="BtnAddgvParentGrid" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Save" Visible='<%# visibleParentEventAddButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Add" />
                                                            <asp:Button ID="BtnUpdategvParentGrid" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Update" Visible='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                                        <asp:GridView ID="gvParentToComplianceGrid" GridLines="None" runat="server" Width="98%" DataKeyNames="EventType,SequenceID" onrowdatabound="gvParentToComplianceGrid_OnRowDataBound"
                                                                            AutoGenerateColumns="false">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/minus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="122px" DataField="ComplianceID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Compliance Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblParentStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                           <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                                        <asp:GridView ID="gvChildGrid" runat="server" Width="98%" GridLines="None"
                                                                            AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                                            OnRowDataBound="gvChildGrid_OnRowDataBound" OnRowUpdating="gvChildGrid_RowUpdating" xmlns:asp="#unknown">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="130px" DataField="SubEventID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left"/>
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Sub Event Name" HeaderStyle-HorizontalAlign="Left"/>
                                                                                <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtgvChildGridDate" CssClass="StartDate" Visible='<%# visibleTxtgvChildGridDate(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' BackColor ='<%# CheckSubEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'  Enabled ='<%# enableSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' Text='<%# Eval("Date") != null? Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy"):"" %>'  Width="100px" runat="server"></asp:TextBox>
                                                                                     </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:Button ID="BtnAddgvChildGrid" runat="server" Text="Save" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                        <asp:Button ID="BtnUpdategvChildGrid" runat="server" Text="Update" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td colspan="100%">
                                                                                                <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                    <asp:GridView ID="gvComplianceGrid" GridLines="None" runat="server" Width="98%" OnRowDataBound="gvComplianceGrid_OnRowDataBound"
                                                                                                        AutoGenerateColumns="false" DataKeyNames="SequenceID">
                                                                                                        <Columns>
                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left"/>
                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center" >
                                                                                                            <ItemTemplate>
                                                                                                            <asp:Label ID="lblStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                                            </ItemTemplate>
                                                                                                           </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                       <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                        <%--<RowStyle BorderStyle="Solid" BorderColor="Black" BorderWidth="1px" />--%>
                                                                                                    </asp:GridView>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                           <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%-- Parent -> Intermediate Event -> Sub Event -> Compliance--%>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                                        <asp:GridView ID="gvIntermediateGrid" runat="server" Width="98%" GridLines="None"
                                                                            AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                                            OnRowDataBound="gvIntermediateGrid_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="130px" DataField="IntermediateEventID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Intermediate Event Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                <%--Sub Event--%>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td colspan="100%">
                                                                                                <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                    <asp:GridView ID="gvIntermediateSubEventGrid" GridLines="None" OnRowDataBound="gvIntermediateSubEventGrid_RowDataBound"
                                                                                                        OnRowUpdating="gvIntermediateSubEventGrid_RowUpdating" runat="server" Width="98%" DataKeyNames="IntermediateEventID,ParentEventID"
                                                                                                        AutoGenerateColumns="false">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField ItemStyle-Width="20px">
                                                                                                                <ItemTemplate>
                                                                                                                    <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                                        <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                                            alt="" /></a>
                                                                                                                  <%--  <asp:Label ID="lblIntermediateEventID" runat="server" Visible="false" Text='<%# Eval("IntermediateEventID") %>'></asp:Label>--%>
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="SubEventID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Sub Event Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:TextBox ID="txtgvIntermediateSubEventGridDate" Visible='<%# visibleTxtgvIntermediateSubEventGridDate(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' BackColor ='<%# CheckIntermediateEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'  Enabled ='<%# enableIntermediateSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' Text='<%# Eval("Date") != null? Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy"):"" %>'  CssClass="StartDate" Width="100px" runat="server"></asp:TextBox>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Button ID="BtnAddIntermediateSubEventGrid" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Save" Visible='<%# visibleIntermediateSubEventAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                                                    <asp:Button ID="BtnUpdateIntermediateSubEventGrid" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Update" Visible='<%# visibleIntermediateSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField>
                                                                                                                <ItemTemplate>
                                                                                                                    <tr>
                                                                                                                        <td colspan="100%">
                                                                                                                            <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                                                <asp:GridView ID="gvIntermediateComplainceGrid" GridLines="None" runat="server" Width="98%" OnRowDataBound="gvIntermediateComplainceGrid_OnRowDataBound"
                                                                                                                                    AutoGenerateColumns="false">
                                                                                                                                    <Columns>
                                                                                                                                        <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left"/>
                                                                                                                                        <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                                        <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                                                                        <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                                                                                        <ItemTemplate>
                                                                                                                                         <asp:Label ID="lblIntermediateStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                                                                         </ItemTemplate>
                                                                                                                                       </asp:TemplateField>
                                                                                                                                    </Columns>
                                                                                                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                                                    <%--<AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                                                </asp:GridView>
                                                                                                                            </div>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                       <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                    </asp:GridView>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                           <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                               <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                            </asp:gridview>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="Panel2" Height="70px" ScrollBars="Auto" runat="server">
                        <table width="100%">
                            <tr>
                                <td style="width: 400px;">
                                    <label style="width: 390px; display: block; float: left; font-size: 13px; color: #333;">
                                        Nature of event
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 400px;">
                                    <asp:TextBox ID="txtDescription" Width="785px" Enabled="false" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <table width="100%">
                        <tr>
                            <td style="display: none">
                                <label style="width: 50px; display: block; float: left; font-size: 13px; color: #333;">
                                    Note :
                                </label>
                            </td>

                            <td style="font-size: 10px">
                                <div class="circle" style="float: left"></div>
                                <div style="float: left;">
                                    Completed / Closed compliance
                                </div>
                            </td>
                            <td style="font-size: 10px">
                                <div style="height: 11px; width: 70px; background-color: lightgreen; float: left; margin-right: 5px; padding: 3px;">DD-MM-YYYY </div>
                                <div style="float: left;">
                                    Completed steps
                                </div>
                            </td>
                            <td style="font-size: 10px">
                                <div style="height: 11px; width: 10px; background-color: DarkGray; float: left; margin-right: 5px"></div>
                                <div style="float: left;">
                                    Informative task no action required.
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>
                <asp:HiddenField ID="hdnTitle" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
