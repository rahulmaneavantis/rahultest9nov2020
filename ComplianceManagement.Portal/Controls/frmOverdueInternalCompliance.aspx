﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="frmOverdueInternalCompliance.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.frmOverdueInternalCompliance" %>

<%@ Register Src="~/Controls/InternalComplianceStatusTransactionCA.ascx" TagName="InternalComplianceStatusTransactionCA"
    TagPrefix="vit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

           



        function initializeDatePicker(date) {

            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
      <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <div style="margin-bottom: 4px">
                <div style="margin-bottom: 4px">
                    <table width="100%" style="height: 35px">
                        <tr>
                            <td>
                                <div style="margin-bottom: 4px">
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="margin-bottom: 10px">
                <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" AllowSorting="true"
                    GridLines="Both" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid"
                    BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" OnSorting="grdComplianceTransactions_Sorting"
                    Width="100%" Font-Size="12px" DataKeyNames="InternalScheduledOnID"
                    OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging"
                    OnRowEditing="grdComplianceTransactions_RowEditing"
                    OnRowUpdating="grdComplianceTransactions_RowUpdating"
                    OnRowCancelingEdit="grdComplianceTransactions_RowCancelingEdit"
                    OnRowDataBound="grdComplianceTransactions_RowDataBound"
                    OnRowCommand="grdComplianceTransactions_RowCommand"
                    OnRowCreated="grdComplianceTransactions_RowCreated">
                    <Columns>
                         <asp:TemplateField HeaderText="ID" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblInternalComplianceID" runat="server" Text='<%# Eval("InternalComplianceID") %>'></asp:Label>                             
                        </ItemTemplate>
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location-wise Compliance" ItemStyle-Width="30%" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 450px;">
                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Performer" ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                    <asp:Label ID="lblPerformer" runat="server" Text='<%# GetPerformer((long)Eval("InternalComplianceInstanceID")) %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                              <asp:Label ID="lblReviewer" runat="server" Text='<%# GetReviewer((long)Eval("InternalComplianceInstanceID")) %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>                     
                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="Risk Category">
                            <ItemTemplate>
                                <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' Visible="false"></asp:Label>
                                <asp:Image runat="server" ID="imtemplat" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Due Date" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="InternalScheduledOn">
                            <ItemTemplate>
                                <%# Convert.ToDateTime(Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txt_DueDate" CssClass="StartDate" runat="server" Text='<%# Convert.ToDateTime(Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:TextBox>
                                <asp:Label ID="lblErrorMsgI" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="For Month" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth">
                            <ItemTemplate>
                                <%# Eval("ForMonth") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Height="20px" SortExpression="Status" />--%>
                        <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="Status">
                            <ItemTemplate>
                                <%# Eval("Status") %>
                                <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("UserID") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblRoleID" runat="server" Text='<%# Eval("RoleID") %>' Visible="false"></asp:Label>                               
                                <asp:Label ID="lblInternalScheduledOnID" runat="server" Text='<%# Eval("InternalScheduledOnID") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblInternalComplianceInstanceID" runat="server" Text='<%# Eval("InternalComplianceInstanceID") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblInternalScheduledOn" runat="server" Text='<%# Eval("InternalScheduledOn") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnChangeStatus" runat="server" Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("InternalComplianceStatusID")) %>'
                                    CommandName="CHANGE_STATUS" OnClick="btnChangeStatus_Click"
                                    CommandArgument='<%# Eval("InternalScheduledOnID") + "," + Eval("InternalComplianceInstanceID") %>'><img src='<%# ResolveUrl("~/Images/change_status_icon.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update" />
                                <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel" />
                            </EditItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

                <asp:HiddenField ID="hdnTitle" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <vit:InternalComplianceStatusTransactionCA runat="server" ID="udcStatusTranscation" />

</asp:Content>
