﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Data;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class InternalCanned_ReportPerformer : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFilters();
                dlFilters.SelectedIndex = 0;
                dlFilters_SelectedIndexChanged(null, null);
            }

        }

        private void BindFilters()
        {
            try
            {
                dlFilters.DataSource = Enumerations.GetAll<CannedReportFilterForPerformer>();
                dlFilters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //CannedReportFilterForPerformer filter = (CannedReportFilterForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                //grdComplianceTransactions.DataSource = InternalCanned_ReportManagement.GetCannedReportDataForPerformer(AuthenticationHelper.UserID, filter);
                //grdComplianceTransactions.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                dlFilters_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DataTable GetGrid()
        {
            dlFilters_SelectedIndexChanged(null, null);
            //return (grdComplianceTransactions.DataSource as List<ComplianceInstanceTransactionView>).ToDataTable();
            return (grdComplianceTransactions.DataSource as List<InternalComplianceInstanceTransactionView>).ToDataTable();
        }

        public string GetFilter()
        {
            return Enumerations.GetEnumByID<CannedReportFilterForPerformer>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                
                //CannedReportFilterForPerformer filter = (CannedReportFilterForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                ////var assignmentList = CannedReportManagement.GetCannedReportDataForPerformer(AuthenticationHelper.UserID, filter);
                //var assignmentList = InternalCanned_ReportManagement.GetCannedReportDataForPerformer(AuthenticationHelper.UserID, filter);
                //if (direction == SortDirection.Ascending)
                //{
                //    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Descending;
                //}
                //else
                //{
                //    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Ascending;
                //}

                //foreach (DataControlField field in grdComplianceTransactions.Columns)
                //{
                //    if (field.SortExpression == e.SortExpression)
                //    {
                //        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                //    }
                //}

                //grdComplianceTransactions.DataSource = assignmentList;
                //grdComplianceTransactions.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
    }
}