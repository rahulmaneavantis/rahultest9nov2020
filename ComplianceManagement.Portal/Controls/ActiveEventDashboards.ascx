﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActiveEventDashboards.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.ActiveEventDashboards" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script type="text/javascript">
    $(document).on("click", "div#ContentPlaceHolder1_ucActiveEventDashboards_upEventInstanceList", function (event) {

        if (event.target.id == "") {
            var idvid = $(event.target).closest('div');
            if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocationActive') > -1) {
                $("#ContentPlaceHolder1_ucActiveEventDashboards_divFilterLocationActive").show();
            } else {
                $("#ContentPlaceHolder1_ucActiveEventDashboards_divFilterLocationActive").hide();
            }
        }
        else if (event.target.id != "ContentPlaceHolder1_ucActiveEventDashboards_tbxFilterLocationActive") {
            $("#ContentPlaceHolder1_ucActiveEventDashboards_divFilterLocationActive").hide();
        } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocationActive') > -1) {
            $("#ContentPlaceHolder1_ucActiveEventDashboards_divFilterLocationActive").show();
        } else if (event.target.id == "ContentPlaceHolder1_ucActiveEventDashboards_tbxFilterLocationActive") {
            $("#ContentPlaceHolder1_ucActiveEventDashboards_tbxFilterLocationActive").unbind('click');

            $("#ContentPlaceHolder1_ucActiveEventDashboards_tbxFilterLocationActive").click(function () {
                $("#ContentPlaceHolder1_ucActiveEventDashboards_divFilterLocationActive").toggle("blind", null, 500, function () { });
            });
        }
    });

    function Confirm() {
        var a = document.getElementById("BodyContent_tbxStartDate").value;
        if (a != "") {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            confirm_value.value = "";
            confirm_value
            if (confirm("Start date is " + Date + ", to continue saving click OK!!")) {
                return true;
            } else {
                return false;
            }
        }
        else {
            alert("Start date should not be blank..!");
        }
    };
    function initializeDatePicker(date) {
        var startDate = new Date();
        $(".StartDate").datepicker({
            dateFormat: 'dd-mm-yy',
            setDate: startDate,
            numberOfMonths: 1
        });
    }

    function setDate() {
        $(".StartDate").datepicker();
    }

    function initializeJQueryUI(textBoxID, divID) {
        $("#" + textBoxID).unbind('click');

        $("#" + textBoxID).click(function () {
            $("#" + divID).toggle("blind", null, 500, function () { });
        });
    }
    function postbackOnCheck() {
        var o = window.event.srcElement;
        if (o.tagName == 'INPUT' && o.type == 'checkbox' && o.name != null && o.name.indexOf('CheckBox') > -1)
        { __doPostBack("", ""); }
    }

    function divexpandcollapse(divname) {
        var div = document.getElementById(divname);
        var img = document.getElementById('img' + divname);
        if (div.style.display == "none") {
            div.style.display = "inline";
            img.src = "../Images/minus.gif";
        } else {
            div.style.display = "none";
            img.src = "../Images/plus.gif";
        }
    }
    function divexpandcollapseChild(divname) {
        var div1 = document.getElementById(divname);
        var img = document.getElementById('img' + divname);
        if (div1.style.display == "none") {
            div1.style.display = "inline";
            img.src = "../Images/minus.gif";
        } else {
            div1.style.display = "none";
            img.src = "../Images/plus.gif";;
        }
    }

    function ClosemodalPopup() {
        $('#modalPopup').modal('hide');

        return true;
    }
    function ClosemodaelShortNotice() {
        $('#modalShortNotice').modal('hide');

        return true;
    }
    function openmodalPopup() {
        if (Displays() == true) {
            $('#modalPopup').modal('show');
        }
        return true;
    }
    function openmodaelShortNotice() {
        $('#modalShortNotice').modal('show');
        return true;
    }

</script>

<style>
    .hiddencol {
        display: none;
    }

    .circle {
        background-color: green;
        width: 29px;
        height: 30px;
        border-radius: 50%;
        display: inline-block;
        margin-right: 5px;
    }

    .ui-widget-header {
        border-bottom: 2px solid #dddddd;
        background: #999 url(images/ui-bg_gloss-wave_35_f6a828_500x100.png) 50% 50% repeat-x;
    }

        .ui-widget-header > th {
            color: #fff;
        }

    .ui-widget-header {
        border: none;
    }

    .Inforamative {
        color: Chocolate !important;
    }

    tr.Inforamative > td {
        color: Chocolate !important;
    }

    .Actionable {
        color: SlateBlue !important;
    }

    tr.Actionable > td {
        color: SlateBlue !important;
    }

    .btnss {
        background-image: url(../Images/edit_icon_new.png);
        border: 0px;
        width: 24px;
        height: 24px;
        background-color: transparent;
    }


    .btnview {
        background-image: url(../Images/view-icon-new.png);
        border: 0px;
        width: 24px;
        height: 24px;
        background-color: transparent;
    }
</style>
<script type="text/javascript">
    function DeleteItem() {
        if (confirm("Are you sure you want to delete ...?")) {
            return true;
        }
        return false;
    }
</script>

<div style="margin-top: 0px;">
    <div style="margin-bottom: 4px">
    </div>
    <asp:UpdatePanel ID="upEventInstanceList" runat="server" UpdateMode="Conditional" OnLoad="upEventInstanceListActive_Load">
        <ContentTemplate>

            <div class="panel-body">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-12 colpadding0" style="text-align: right; float: right">
                        <div class="col-md-2 colpadding0 entrycount">
                            <div class="col-md-3 colpadding0">
                                <p style="color: #999; margin-top: 5px">Show </p>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlpageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlpageSize_SelectedIndexChanged" class="form-control m-bot15" Style="width: 64px; float: left; margin-left: 10px;">
                                <asp:ListItem Text="5" />
                                <asp:ListItem Text="10" Selected="True" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2 colpadding0 entrycount" style="float: left; margin-right: -2%; width: 60.667% !important">
                            <asp:DropDownList runat="server" ID="ddlCheckListTypeActive" OnSelectedIndexChanged="ddlCheckListTypeActive_SelectedIndexChanged" AutoPostBack="true" class="form-control m-bot15 search-select">
                                <asp:ListItem Text="<Select>" Value="-1" />
                                <asp:ListItem Text="Secretrial" Value="1" />
                                <asp:ListItem Text="Non Secretrial" Value="2" />
                            </asp:DropDownList>
                        </div>

                        <div class="col-md-4 colpadding0 entrycount" style="float: left; margin-left: -50%; width: 31.667% !important">
                            <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocationActive" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 280px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                CssClass="txtbox" />
                            <div style="margin-left: 11%; position: absolute; z-index: 10;" id="divFilterLocationActive" runat="server">
                                <asp:TreeView runat="server" ID="tvFilterLocationActive" SelectedNodeStyle-Font-Bold="true" Width="280px" NodeStyle-ForeColor="#8e8e93"
                                    Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                    OnSelectedNodeChanged="tvFilterLocationActive_SelectedNodeChanged">
                                </asp:TreeView>
                            </div>
                        </div>
                        <div class="col-md-4 colpadding0 entrycount" style="float: left; margin-left: -17%; width: 13.333333% !important;">
                            <asp:TextBox runat="server" Style="padding-left: 7px; width: 300px;" placeholder="Type to Filter" class="form-group form-control" ID="txtSearchType" CssClass="form-control" onkeydown="return (event.keyCode!=13);" />
                        </div>

                        <div class="col-md-2 colpadding0" style="width: 17.333333%; float: right">
                            <div class="col-md-6 colpadding0">
                                <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" OnClick="btnSearch_Click" Style="margin-left: 100px !important;" runat="server" Text="Apply" />
                            </div>
                        </div>
                    </div>

                    <div runat="server" id="DivRecordsScrum" style="float: right;">

                        <p style="color: #999; padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
            </div>

            <asp:Panel ID="Panel3" Width="100%" runat="server">
                <asp:GridView runat="server" ID="grdEventList1" AutoGenerateColumns="false" CssClass="table" GridLines="None" OnRowCancelingEdit="grdEventList_RowCancelingEdit"
                    OnRowEditing="grdEventList_RowEditing" OnRowUpdating="grdEventList_RowUpdating"
                    OnRowCreated="grdEventList_RowCreated" OnRowCommand="grdEventList1_RowCommand"
                    AllowPaging="True" PageSize="10" OnSorting="grdEventList_Sorting"
                    DataKeyNames="ID" OnPageIndexChanging="grdEventList_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" ItemStyle-Height="22px" SortExpression="Name">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label ID="lbleventName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Height="22px" HeaderText="Location" SortExpression="CustomerBranchName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="lblEventScheduleOnID" runat="server" Visible="false" Text='<%# Eval("EventScheduledOnId") %>'></asp:Label>
                                    <asp:Label ID="lblAssignmnetId" runat="server" Visible="false" Text='<%# Eval("EventAssignmentID") %>'></asp:Label>
                                    <asp:Label ID="lblEventInstanceID" runat="server" Visible="false" Text='<%# Eval("EventInstanceID") %>'></asp:Label>
                                    <asp:Label ID="lblBranch" runat="server" Visible="false" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                    <asp:Label ID="lblEventClassificationID" runat="server" Visible="false" Text='<%# Eval("EventClassificationID") %>'></asp:Label>
                                    <asp:Label ID="lblBranchName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User" SortExpression="UserName">
                            <ItemTemplate>
                                <asp:Label ID="lblUserID" runat="server" Visible="false" Text='<%# Eval("UserID") %>' ToolTip='<%# Eval("UserID") %>'></asp:Label>
                                <asp:Label ID="lblUser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("UserName") %>' ToolTip='<%# Eval("UserName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nature of event" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lbleventDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EventDescription") %>' ToolTip='<%# Eval("EventDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Held On" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="7%" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblHeldOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Convert.ToDateTime(Eval("HeldOn")).ToString("dd-MMM-yyyy") %>' ToolTip='<%# Convert.ToDateTime(Eval("HeldOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                            <ItemTemplate>
                                <asp:Button ID="btn_Update" OnClientClick="return openmodalPopup()" runat="server" ToolTip="Edit Event" CommandArgument='<%# Eval("ID") %>' CommandName="Update" CssClass="btnss" />
                                <asp:Button ID="btn_View" runat="server" Style="margin-left: 8px;" ToolTip="View Event" CommandName="View" CssClass="btnview" CommandArgument='<%# ((GridViewRow) Container).RowIndex + "," + Eval("CustomerBranchID") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerStyle HorizontalAlign="Right" />
                    <PagerTemplate>
                        <table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
                <div class="col-md-12 colpadding0">
                    <div class="col-md-6 colpadding0">
                    </div>
                    <div class="col-md-6 colpadding0">
                        <div class="table-paging" style="margin-bottom: 20px">
                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />
                            <div class="table-paging-text">
                                <p>
                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                    <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div>
        <div class="modal fade" id="modalPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 922px">
                <div class="modal-content">
                    <div class="modal-header" style="width: 900px">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="width: 900px">
                        <asp:UpdatePanel ID="upOptionalCompliances" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary runat="server" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ComplianceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                                    <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                                </div>
                                <div style="margin: 5px">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div style="margin-bottom: 7px;" runat="server" id="divSubEventmode">
                                                    <div style="z-index: 10" id="divSubevent">
                                                        <asp:gridview id="gvParentGrid" runat="server" onrowupdating="gvParentGrid_RowUpdating" autogeneratecolumns="false"
                                                            showfooter="true" width="790px" borderwidth="0px" gridlines="None" class="tablenew" datakeynames="Type,Date"
                                                            onrowdatabound="gvParentGrid_OnRowDataBound" xmlns:asp="#unknown">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("EventType") %>');">
                                                                <img id="imgdiv<%# Eval("EventType") %>" width="9px" border="0"
                                                                    src="../Images/plus.gif" alt="" /></a>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField ItemStyle-Width="150px" DataField="ParentEventID" HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="true" />
                                                    <asp:BoundField ItemStyle-Width="750px" DataField="Name"  HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="true"/>
                                                    <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                         <ItemTemplate>
                                                          <asp:TextBox ID="txtgvParentGridDate" CssClass="StartDate" style="border-radius: 9px;border-color: #c7c7cc; text-align :center" BackColor ='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'  Enabled ='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' Text='<%# Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy") %>' Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                       <ItemTemplate>
                                                            <asp:Button ID="BtnAddgvParentGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Save" Visible='<%# visibleParentEventAddButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Add" />
                                                            <asp:Button ID="BtnUpdategvParentGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Update" Visible='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                                        <asp:GridView ID="gvParentToComplianceGrid" borderwidth="0px"  gridlines="None" style="color: #666666;font-size:14px;" runat="server" Width="98%" DataKeyNames="EventType,SequenceID" onrowdatabound="gvParentToComplianceGrid_OnRowDataBound"
                                                                            AutoGenerateColumns="false" CellSpacing="5" >
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/minus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="122px" DataField="ComplianceID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Compliance name" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblParentStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                                        <asp:GridView ID="gvChildGrid" runat="server" Width="98%" borderwidth="0px" gridlines="None" style="color: #666666;font-size:14px;"
                                                                            AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                                            OnRowDataBound="gvChildGrid_OnRowDataBound" OnRowUpdating="gvChildGrid_RowUpdating" OnRowCommand="gvChildGrid_RowCommand" xmlns:asp="#unknown">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="130px" DataField="SubEventID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left"/>
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Sub event name" HeaderStyle-HorizontalAlign="Left"/>
                                                                                <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtgvChildGridDate" CssClass="StartDate" style=" text-align :center; border-radius: 9px;border-color: #c7c7cc;" Visible='<%# visibleTxtgvChildGridDate(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' BackColor ='<%# CheckSubEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'  Enabled ='<%# enableSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' Text='<%# Eval("Date") != null? Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy"):"" %>'  Width="100px" runat="server"></asp:TextBox>
                                                                                     </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                 <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="IntermediateEventID" runat="server" Text='<%# Eval("IntermediateEventID") %>' Style="display: none;"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="255px" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:Button ID="BtnAddgvChildGrid" runat="server" style="margin-left: 6px;" class="btn btn-search" Text="Save" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                        <asp:Button ID="BtnUpdategvChildGrid" runat="server" class="btn btn-search" Text="Update" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                        <asp:Button ID="btnNASubEvent" runat="server" style="margin-left: 7px;" class="btn btn-search" Text="NA" ValidationGroup="ComplianceValidationGroup" CommandName="NotApplicable" OnClientClick="return confirm('Are you sure you want to not applicable event step?');"  CommandArgument='<%# Eval("ParentEventID") + "," + Eval("SubEventID") + "," + Convert.ToInt32(Session["EventScheduledOnId"]) %>' Visible='<%# CheckSubEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>'  />
                                                                                        <asp:LinkButton ID="btnAddIComplianceChildGrid" runat="server" style="margin-left: -9px;" ToolTip="Add internal compliance to event" data-toggle="tooltip" data-placement="bottom"  ValidationGroup="ComplianceValidationGroup" CommandName="InternalCompliance" CommandArgument='<%# Eval("ParentEventID") + "," + Eval("SubEventID") + "," + Convert.ToInt32(Session["EventScheduledOnId"]) + ","+ Convert.ToDateTime(Eval("Date"))%>' Visible='<%# visibleInternalComplianceAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' >
                                                                                             <img src='<%# ResolveUrl("~/Images/Add new icon.png")%>' alt="View"/>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                     <%--Intermdaite Event--%>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td colspan="100%">
                                                                                                <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                    <asp:GridView ID="gvIntermediateSubEventGrid" borderwidth="0px" gridlines="None" class="tablenew"  OnRowDataBound="gvIntermediateSubEventGrid_RowDataBound"
                                                                                                        OnRowUpdating="gvIntermediateSubEventGrid_RowUpdating" OnRowCommand="gvIntermediateSubEventGrid_RowCommand" runat="server" Width="98%" DataKeyNames="IntermediateEventID,ParentEventID"
                                                                                                        AutoGenerateColumns="false">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField ItemStyle-Width="20px">
                                                                                                                <ItemTemplate>
                                                                                                                    <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                                        <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif" alt="" /></a>
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="SubEventID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Sub Event Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                                                                                <ItemTemplate> 
                                                                                                                    <asp:TextBox ID="txtgvIntermediateSubEventGridDate" style="border-radius: 9px; border-color: #c7c7cc; text-align:center;"  Visible='<%# visibleTxtgvIntermediateSubEventGridDate(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' BackColor ='<%# CheckIntermediateEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'   Text='<%# Eval("Date") != null? Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy"):"" %>'  CssClass="StartDate" Width="100px" runat="server"></asp:TextBox>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField ItemStyle-Width="240px" ItemStyle-HorizontalAlign="Center">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Button ID="BtnAddIntermediateSubEventGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Save" Visible='<%# visibleIntermediateSubEventAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                                                    <asp:Button ID="BtnUpdateIntermediateSubEventGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Update" Visible='<%# visibleIntermediateSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                                                    <asp:Button ID="btnNAIntermediateSubEvent" runat="server" style="margin-left: 7px;" class="btn btn-search" Text="NA" ValidationGroup="ComplianceValidationGroup" CommandName="IntermediateNotApplicable" OnClientClick="return confirm('Are you sure you want to not applicable event step?');"  CommandArgument='<%# Eval("ParentEventID") + "," + Eval("IntermediateEventID") + "," + Eval("SubEventID") + "," + Convert.ToInt32(Session["EventScheduledOnId"]) %>' Visible='<%# CheckIntermediateEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>'  />
                                                                                                                    <asp:LinkButton ID="btnAddIComplianceIntermediateSubEventGrid" runat="server" style="margin-left: -9px;" ToolTip="Add internal compliance to event" data-toggle="tooltip" data-placement="bottom"  ValidationGroup="ComplianceValidationGroup" CommandName="IntermediateInternalCompliance" CommandArgument='<%# Eval("ParentEventID") + "," + Eval("IntermediateEventID") + "," + Eval("SubEventID") + "," + Convert.ToInt32(Session["EventScheduledOnId"]) +","+ Convert.ToDateTime(Eval("Date"))%>' Visible='<%# visibleIntermediateEventInternalComplianceAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' >
                                                                                                                         <img src='<%# ResolveUrl("~/Images/Add new icon.png")%>' alt="View"/>
                                                                                                                    </asp:LinkButton>
                                                                                                            </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                            <asp:TemplateField>
                                                                                                                <ItemTemplate>
                                                                                                                    <tr>
                                                                                                                        <td colspan="100%">
                                                                                                                            <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                                                <asp:GridView ID="gvIntermediateComplainceGrid" borderwidth="0px" gridlines="None" class="tablenew" runat="server" Width="98%" OnRowDataBound="gvIntermediateComplainceGrid_OnRowDataBound"
                                                                                                                                    AutoGenerateColumns="false" OnRowUpdating="gvIntermediateComplainceGrid_RowUpdating">
                                                                                                                                    <Columns>
                                                                                                                                        <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left"/>
                                                                                                                                        <asp:BoundField ItemStyle-Width="700px" DataField="Name" HeaderText="Compliance Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                                        <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                                                                        <asp:BoundField DataField="IsIntermediateInternal" HeaderText="IsIntermediateInternal" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"/>
                                                                                                                                        <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                                                                                                            <ContentTemplate>
                                                                                                                                               <%-- Visible='<%# visibleClosure(Convert.ToInt32(Eval("ComplianceID"))) %>'--%>
                                                                                                                                             <asp:Button ID="btngvIntermediateComplainceGridClosure" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Closure" CommandName="Update" />
                                                                                                                                          </ContentTemplate>
                                                                                                                                          </asp:UpdatePanel>
                                                                                                                                           <asp:Label ID="lblIntermediateStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                                                                           <asp:Label ID="lblgvIIntermediateParentEventID" runat="server" Visible="false" Text='<%# Eval("ParentEventID") %>'></asp:Label>
                                                                                                                                           <asp:Label ID="lblgvIIntermediateEventID" runat="server" Visible="false" Text='<%# Eval("IntermediateEventID") %>'></asp:Label>
                                                                                                                                           <asp:Label ID="lblgvIIntermediateSubEventID" runat="server" Visible="false" Text='<%# Eval("SubEventID") %>'></asp:Label>
                                                                                                                                         </ItemTemplate>
                                                                                                                                       </asp:TemplateField>
                                                                                                                                    </Columns>
                                                                                                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                                                </asp:GridView>
                                                                                                                            </div>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                    </asp:GridView>
                                                                                                     <%--Complaince--%>
                                                                                                      <asp:GridView ID="gvComplianceGrid" borderwidth="0px" gridlines="None" class="tablenew" runat="server" Width="98%" OnRowDataBound="gvComplianceGrid_OnRowDataBound"
                                                                                                        AutoGenerateColumns="false" DataKeyNames="SequenceID" OnRowUpdating="gvComplianceGrid_RowUpdating">
                                                                                                        <Columns>
                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left"/>
                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField DataField="IsInternal" HeaderText="IsInternal" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"/>
                                                                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center" >
                                                                                                            <ItemTemplate>
                                                                                                              <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                                                                              <ContentTemplate>
                                                                                                                 <%-- Visible='<%# visibleClosure(Convert.ToInt32(Eval("ComplianceID"))) %>'--%>
                                                                                                              <asp:Button ID="btngvComplianceGridClosure" class="btn btn-search" ValidationGroup="ComplianceValidationGroup"  runat="server" Text="Closure" CommandName="Update" />
                                                                                                              </ContentTemplate>
                                                                                                             </asp:UpdatePanel>
                                                                                                             <asp:Label ID="lblStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                                             <asp:Label ID="lblgvSParentEventID" runat="server" Visible="false" Text='<%# Eval("ParentEventID") %>'></asp:Label>
                                                                                                             <asp:Label ID="lblgvSIntermediateEventID" runat="server" Visible="false" Text='<%# Eval("IntermediateEventID") %>'></asp:Label>
                                                                                                             <asp:Label ID="lblgvSSubeventID" runat="server" Visible="false" Text='<%# Eval("SubEventID") %>'></asp:Label>
                                                                                                            </ItemTemplate>
                                                                                                           </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                    </asp:GridView>
                                                                                                </div>
                                                                                               </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ></HeaderStyle>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:gridview>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <div class="clearfix"></div>
                                        <tr>
                                            <td style="width: 400px;">
                                                <div style="margin-bottom: 7px;" runat="server" id="div1111">
                                                    <label style="width: 390px; display: block; float: left; font-size: 16px; color: #333;">
                                                        Nature of event
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 400px;">
                                                <asp:TextBox ID="txtDescription" class="form-control" Rows="3" Width="785px" Enabled="false" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <table width="100%" id="eventnaturelegend" style="margin-top: 10px">
                                                    <tr>
                                                        <td>
                                                            <label style="width: 50px; display: block; float: left; font-size: 13px; color: #333;">
                                                                Note :
                                                            </label>
                                                        </td>
                                                        <td style="font-size: 10px">
                                                            <div class="circle" style="float: left"></div>
                                                            <div style="float: left; color: #666666; padding: 9px; font-family: 'Roboto',sans-serif;">
                                                                Completed / Closed compliance
                                                            </div>
                                                        </td>
                                                        <td style="font-size: 10px; color: #666666;">
                                                            <div style="height: 30px; width: 81px; border-radius: 14px; background-color: lightgreen; float: left; margin-right: 5px; padding: 9px;">DD-MM-YYYY </div>
                                                            <div style="float: left; padding: 9px;">
                                                                Completed steps
                                                            </div>
                                                        </td>
                                                        <td style="font-size: 10px">
                                                            <div style="height: 30px; width: 30px; background-color: Chocolate; float: left; margin-right: 5px"></div>
                                                            <div style="float: left; color: #666666; padding: 9px;">
                                                                Informative checklist no action required.
                                                            </div>
                                                        </td>
                                                        <td style="font-size: 10px">
                                                            <div style="height: 30px; width: 30px; background-color: SlateBlue; float: left; margin-right: 5px"></div>
                                                            <div style="float: left; color: #666666; padding: 9px;">
                                                                Actionable checklist.
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:HiddenField ID="hdnTitle" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalShortNotice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 450px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="width: 450px;">
                    <asp:UpdatePanel ID="upshortnotice" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 4px; margin-left: 95px">
                                <asp:Label runat="server" Text="Do you want to conduct meeting by short notice?" ID="Label1" Style="color: #999; margin-left: -72px"></asp:Label>
                            </div>

                            <div style="margin-bottom: 4px; margin-left: 95px">
                                <asp:Label runat="server" ID="lblMessage" ForeColor="Red"></asp:Label>
                            </div>
                            <div>
                                <div style="margin: 5px; margin-left: 165px;">
                                    <asp:RadioButtonList ID="rbtnShortnotice" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                                        OnSelectedIndexChanged="rbtnShortnotice_SelectedIndexChanged">
                                        <asp:ListItem Text="Yes" Value="1" Selected="True" />
                                        <asp:ListItem Text="No" Value="0" />
                                    </asp:RadioButtonList>
                                </div>
                                <div style="margin-top: 25px; margin-left: 70px;" id="divDays" runat="server">
                                    <asp:Label ID="lbl" Text="Short Notice Days" Style="color: #999;" Width="125px" runat="server"></asp:Label>
                                    <%-- <asp:Label ID="Label1" Font-Size="Large" Font-Bold="true" Text ="(-)" Width="16px" runat="server"></asp:Label>--%>
                                    <asp:TextBox ID="txtShortNoticeDays" Width="80px" runat="server" MaxLength="2"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                        TargetControlID="txtShortNoticeDays" />
                                </div>
                                <div style="margin-top: 50px; margin-left: 125px">
                                    <asp:Button Text="Save" runat="server" class="btn btn-search" Style="margin-top: -30px;" ValidationGroup="ComplianceValidationGroup" ID="btnSaveShortNotice" OnClick="btnSaveShortNotice_Click" />
                                    <asp:Button Text="Close" runat="server" class="btn btn-search" Style="margin-top: -30px; margin-left: 15px" data-dismiss="modal" ID="btnShortClose" />
                                    <%--OnClick="btnShortClose_Click"--%>
                                </div>
                            </div>
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

      <div class="modal fade" id="modalClosure" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 680px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="width:680px;">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div>
                                <asp:ValidationSummary runat="server" CssClass="vdsummary" ForeColor="Red"
                                    ValidationGroup="ComplianceValidationGroup1" />
                                <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceValidationGroup1" ForeColor="Red" Display="None" />
                                <asp:Label runat="server" ID="lblImsg1" ForeColor="Red"></asp:Label>
                            </div>
                            <div style="margin-bottom: 4px; margin-left: 95px">
                                <asp:Label runat="server" Text="Do you want to stop compliance schedule?" ID="Label2" Style="color: #999; margin-left: -72px"></asp:Label>
                            </div>
                            <div style="margin-bottom: 4px;">
                                  <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                 <asp:Label ID="Label6" Text="Schedule From Date" Style="color: #999;" Width="135px" runat="server"></asp:Label>
                                 <asp:DropDownList runat="server" ID="ddlScheduleDates" style="width:190px;" AutoPostBack="true"
                                     dataTextFormatString="{0:dd/MMM/yyyy}">
                                    </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select schedule from date."
                                    ControlToValidate="ddlScheduleDates" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup1" Display="None" />
                            </div>
                            <div style="margin-bottom: 4px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <asp:Label ID="Label5" Text="Remark" Style="color: #999;" Width="135px" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtClosureRemark" TextMode="MultiLine" Width="420px" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Remark can not be empty."
                                    ControlToValidate="txtClosureRemark" runat="server" ValidationGroup="ComplianceValidationGroup1"
                                    Display="None" />
                            </div>
                            <div style="margin-top: 50px; margin-left: 125px">
                                <asp:Button Text="Save" runat="server" class="btn btn-search" Style="margin-top: -30px;" ValidationGroup="ComplianceValidationGroup1" ID="btnClosure" OnClick="btnClosure_Click" />
                                <asp:Button Text="Close" runat="server" class="btn btn-search" Style="margin-top: -30px; margin-left: 15px" data-dismiss="modal" ID="btnCloseClosure" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalInternalCompliance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 700px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="width: 620px;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 4px; margin-left: 95px">
                                <asp:Label runat="server" ID="lblImsg" ForeColor="Red"></asp:Label>
                                <asp:CustomValidator ID="CustomValidator1" runat="server" class="alert alert-block alert-danger fade in" EnableClientScript="False"
                                    ValidationGroup="InternalComplianceValidationGroup" Display="None" />
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="InternalComplianceValidationGroup" />
                            </div>
                            <div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Internal Compliance
                                    </label>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList runat="server" ID="ddlInternalCompliance" CssClass="form-control" Style="padding: 0px; margin: 0px; width: 400px;" AutoPostBack="true" />
                                            <asp:CompareValidator ErrorMessage="Please select internal Compliance." ControlToValidate="ddlInternalCompliance" ID="vaInternalCompliance"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="InternalComplianceValidationGroup"
                                                Display="None" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Days
                                    </label>
                                    <asp:TextBox ID="txtDays" Width="80px" runat="server" MaxLength="2"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                        TargetControlID="txtDays" />
                                    <asp:RequiredFieldValidator ID="rfvDays" ErrorMessage="Please enter Days."
                                        ControlToValidate="txtDays" runat="server" ValidationGroup="InternalComplianceValidationGroup"
                                        Display="None" />
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        OneTime/Recurring
                                    </label>
                                    <asp:RadioButtonList ID="rbtInternalOccurance" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                        <asp:ListItem Text="Recurring" Value="0" />
                                        <asp:ListItem Text="OneTime" Value="1" Selected="True" />
                                    </asp:RadioButtonList>
                                </div>
                                <div style="margin-top: 50px; margin-left: 215px">
                                    <asp:Button Text="Save" runat="server" class="btn btn-search" Style="margin-top: -30px;" ValidationGroup="InternalComplianceValidationGroup" ID="btnInternalComplianceAdd" OnClick="btnInternalComplianceAdd_Click" />
                                    <asp:Button Text="Close" runat="server" class="btn btn-search" Style="margin-top: -30px; margin-left: 15px" data-dismiss="modal" ID="btnInternal" />
                                </div>
                            </div>
                            <asp:HiddenField ID="HiddenField2" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</div>
