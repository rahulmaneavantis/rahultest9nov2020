﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserSummarySuperAdmin.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.UserSummarySuperAdmin" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<div style="float: left; clear: both; margin-top: 5px; margin-left: 50px">
    <asp:Chart ID="chrtUsersPerCustomer" runat="server" Width="500px" Height="500px">
        <Titles>
            <asp:Title Text="Users per Customer" Alignment="TopCenter" Font="Tahoma, 12pt, style=Bold" />
        </Titles>
        <Legends>
            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                LegendStyle="Table" />
        </Legends>
        <Series>
            <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Area3DStyle-Enable3D="false">
               
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</div>
<div style="float: left; margin-top: 5px; margin-left: 50px">
    <asp:Chart ID="chrtEntitiesPerCustomers" runat="server" Width="500px" Height="500px">
        <Titles>
            <asp:Title Text="Entities per Customer" Alignment="TopCenter" Font="Tahoma, 12pt, style=Bold" />
        </Titles>
        <Legends>
            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                LegendStyle="Table" />
        </Legends>
        <Series>
            <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity"  IsValueShownAsLabel="true">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Area3DStyle-Enable3D="false">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</div>
