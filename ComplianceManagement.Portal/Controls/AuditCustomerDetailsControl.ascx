﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuditCustomerDetailsControl.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.AuditCustomerDetailsControl" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<script type="text/javascript">


    //This is used for Close Popup after button click.
    function caller1() {
        setInterval(CloseWin1, 3000);
    };


    function initializeDatePicker(date) {

        var startDate = new Date();
        $("#<%= txtStartDate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: startDate,
            numberOfMonths: 1,
            minDate: startDate,
            onClose: function (startDate) {
                $("#<%= txtEndDate.ClientID %>").datepicker("option", "minDate", startDate);
            }
        });

            $("#<%= txtEndDate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: startDate,
            numberOfMonths: 1,
            minDate: startDate,
            onClose: function (startDate) {
                $("#<%= txtStartDate.ClientID %>").datepicker("option", "maxDate", startDate);
            }
            });


            if (date != null) {
                $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
                $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }

        function initializeCombobox() {
            $("#<%= ddlCustomerStatus.ClientID %>").combobox();
            $("#<%= ddlLocationType.ClientID %>").combobox();
            $("#<%= ddlComplianceApplicable.ClientID %>").combobox();
        }

</script>

<div id="divCustomersDialog">
    <asp:UpdatePanel ID="upCustomers" runat="server" UpdateMode="Conditional" OnLoad="upCustomers_Load">
        <ContentTemplate>
            <div>
                <div style="margin-bottom: 7px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="CustomerValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="CustomerValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Name</label>
                    <asp:TextBox runat="server" ID="tbxName" CssClass="form-control" Style="width: 390px;" MaxLength="50" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty."
                        ControlToValidate="tbxName" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" ValidationExpression="^[a-zA-Z_&]+[a-zA-Z0-9&_ .-]*$"></asp:RegularExpressionValidator>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Address</label>
                    <asp:TextBox runat="server" ID="tbxAddress" CssClass="form-control" Style="width: 390px;" MaxLength="500"
                        TextMode="MultiLine" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Location Type:</label>
                    <asp:DropDownList runat="server" ID="ddlLocationType" CssClass="form-control m-bot15" Style="padding: 0px; margin: 0px; width: 390px;" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Name</label>
                    <asp:TextBox runat="server" ID="tbxBuyerName" CssClass="form-control" Style="width: 390px;"
                        MaxLength="50" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Buyer Name can not be empty."
                        ControlToValidate="tbxBuyerName" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Contact No</label>
                    <asp:TextBox runat="server" ID="tbxBuyerContactNo" CssClass="form-control" Style="width: 390px;"
                        MaxLength="15" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Buyer Contact Number can not be empty."
                        ControlToValidate="tbxBuyerContactNo" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid contact number." ControlToValidate="tbxBuyerContactNo"
                        ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxBuyerContactNo" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                        ValidationGroup="CustomerValidationGroup" ErrorMessage="Please enter only 10 digit Contact Number."
                        ControlToValidate="tbxBuyerContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Email</label>
                    <asp:TextBox runat="server" ID="tbxBuyerEmail" CssClass="form-control m-bot15" Style="width: 390px;"
                        MaxLength="200" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Buyer Email can not be empty."
                        ControlToValidate="tbxBuyerEmail" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid email." ControlToValidate="tbxBuyerEmail"
                        ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Customer Status</label>
                    <asp:DropDownList runat="server" ID="ddlCustomerStatus" CssClass="form-control m-bot15" Style="padding: 0px; margin: 0px; width: 390px;" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Start Date</label>
                    <asp:TextBox runat="server" ID="txtStartDate" CssClass="form-control" Style="width: 390px;" ReadOnly="true"
                        MaxLength="200" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        End Date</label>
                    <asp:TextBox runat="server" ID="txtEndDate" CssClass="form-control" Style="width: 390px;" ReadOnly="true"
                        MaxLength="200" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Disk Space</label>
                    <asp:TextBox runat="server" ID="txtDiskSpace" CssClass="form-control" Style="width: 390px;"
                        MaxLength="200" />
                </div>
                 <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Verticals Applicable</label>
                    <asp:DropDownList runat="server" ID="ddlVerticalsApplicable" Style="padding: 0px; margin: 0px; width: 150px;"
                        CssClass="form-control m-bot15">
                        <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes" Value="1" />
                    </asp:DropDownList>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Internal Compliance Applicable</label>
                    <asp:DropDownList runat="server" ID="ddlComplianceApplicable" CssClass="form-control m-bot15" Style="padding: 0px; margin: 0px; width: 390px;">
                        <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes" Value="1" />
                    </asp:DropDownList>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Task Management Applicable</label>
                    <asp:DropDownList runat="server" ID="ddlTaskApplicable" CssClass="form-control m-bot15" Style="padding: 0px; margin: 0px; width: 390px;">
                        <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes" Value="1" />
                    </asp:DropDownList>
                </div>
                <div style="margin-bottom: 7px">
                    </div>
               
                <div style="margin-bottom: 7px; margin-left: 230px; margin-top: 25px;">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                        ValidationGroup="CustomerValidationGroup" />
                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                </div>
                <div class="clearfix" style="height: 50px">
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
