﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Configuration;
using System.Threading;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Users;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Globalization;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class AuditUserDetailsControl : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rblAuditRole.Enabled = true;
                BindCustomersListMultiSelect();
                BindRoles();
                //BindRolesRisk();
                BindRolesHR();
                BindRolesSEC();
                BindDepartment();
                UserImageUpload.Attributes["onchange"] = "UploadFile(this)";
                ddlRole.Enabled = true;
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "CADMN")
                {
                    divAuditRole.Visible = true;
                    CompareValidatorRoleAdmin.Enabled = true;
                    DivAuditHeadRole.Visible = false;
                    CampareValAuditHeadRole.Enabled = false;
                    CompareValidatorRoleAdmin.Enabled = true;
                    DivCustomerName.Visible = false;
                    divCustomerList.Style.Add("display", "none");
                }
                else
                {
                    DivAuditHeadRole.Visible = true;
                    CampareValAuditHeadRole.Enabled = true;
                    divAuditRole.Visible = false;
                    CompareValidatorRoleAdmin.Enabled = false;
                    CompareValidatorRoleAdmin.Enabled = false;
                    DivCustomerName.Visible = true;
                }
                ViewState["Mode"] = 0;
            }
        }
        public static List<long> GetByProductID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                // List<long> plist = new List<long>();
                //var ServiceproviderId = (from cust in entities.Customers
                //                      where cust.ID == customerID
                //                         select cust.ServiceProviderID).FirstOrDefault();

                // if (ServiceproviderId !=null)
                // {
                //plist = (from row in entities.ProductMappings
                //                          where row.CustomerID == ServiceproviderId && row.IsActive == false
                //                          select (long)row.ProductID).ToList();
                //}

                //return plist;


                var productmapping = (from row in entities.ProductMappings
                                      where row.CustomerID == customerID && row.IsActive == false
                                      select (long)row.ProductID).ToList();

                return productmapping;
            }
        }
        public bool GetServiseProviderId(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ServiceproviderId = false;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select (bool)cust.IsDistributor).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }
        public void EnableDisableRole(int CustomerID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ab = false;
                int customerID = -1;
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = CustomerID;
                }
                else
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                var Listofproduct = GetByProductID(customerID);
                if (Listofproduct.Count > 0)
                {

                    divComplianceRole.Visible = false;
                    ddlRole.Enabled = false;

                    divAuditRole.Visible = false;
                    ddlAuditRole.Enabled = false;

                    divSECRole.Visible = false;
                    ddlSECRole.Enabled = false;

                    divHRRole.Visible = false;
                    ddlHRRole.Enabled = false;

                    if (Listofproduct.Contains(1))
                    {
                        divComplianceRole.Visible = true;
                        ddlRole.Enabled = true;

                    }

                    if (Listofproduct.Contains(8))
                    {
                        divSECRole.Visible = true;
                        ddlSECRole.Enabled = true;
                    }

                    if (Listofproduct.Contains(9))
                    {
                        divHRRole.Visible = true;
                        ddlHRRole.Enabled = true;
                    }

                    if ((Listofproduct.Contains(3) || Listofproduct.Contains(4)))
                    {
                        divAuditRole.Visible = true;
                        ddlAuditRole.Enabled = true;
                    }
                }
            }
        }
        protected void Upload(object sender, EventArgs e)
        {
            if (UserImageUpload.HasFile)
            {
                string[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg" };

                string ext = System.IO.Path.GetExtension(UserImageUpload.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Invalid File. Please upload a File with extension " + string.Join(",", validFileTypes);
                }

            }
        }

        protected void upUsers_Load(object sender, EventArgs e)
        {
        }
        protected void rblAuditRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblAuditRole.SelectedItem.Text == "Is Audit Head" || rblAuditRole.SelectedItem.Text == "Is Audit Manager")
            {
                divCustomerList.Style.Add("display", "block");
            }
            else
            {
                divCustomerList.Style.Add("display", "none");
            }
        }
        private void BindDepartment()
        {
            try
            {

                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";
                ddlDepartment.DataSource = CompDeptManagement.FillDepartment(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("Select Department", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRoles()
        {
            try
            {
                ddlRole.DataTextField = "Name";
                ddlRole.DataValueField = "ID";

                var roles = GetAll(false);
                ddlRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("Select Role", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<Role> GetAll(bool? onlyForCompliance = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.ID == 2 || row.ID == 7 || row.ID == 8
                             select row);

                if (onlyForCompliance.HasValue)
                {
                    roles = roles.Where(entry => entry.IsForCompliance == onlyForCompliance.Value && entry.Code != "APPR");
                }
                return roles.ToList();
            }
        }

        public static List<Role> GetSECLimitedRole()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.IsForSecretarial == true
                             select row);

                return roles.ToList();
            }
        }
        private void BindRolesHR()
        {
            try
            {
                ddlHRRole.DataTextField = "Name";
                ddlHRRole.DataValueField = "ID";

                var roles = RoleManagement.GetAll_HRCompliance_Roles();

                ddlHRRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlHRRole.DataBind();

                ddlHRRole.Items.Insert(0, new ListItem("Select Role", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRolesSEC()
        {
            try
            {
                ddlSECRole.DataTextField = "Name";
                ddlSECRole.DataValueField = "ID";

                var roles = GetSECLimitedRole();
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN")).ToList();
                }

                if (roles.Count > 0)
                {
                    List<string> secRoleCodes = new List<string> { "CEXCT", "DADMN", "CSMGR" };

                    roles = roles.Where(row => secRoleCodes.Contains(row.Code)).ToList();
                }

                ddlSECRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlSECRole.DataBind();

                ddlSECRole.Items.Insert(0, new ListItem("Select Role", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRolesRisk()
        {
            try
            {
                ddlAuditRole.DataTextField = "Name";
                ddlAuditRole.DataValueField = "ID";

                var roles = RoleManagement.GetLimitedRole(false);
                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN")).ToList();
                }

                ddlAuditRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlAuditRole.DataBind();

                ddlAuditRole.Items.Insert(0, new ListItem("Select Role", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlAuditRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "CADMN")
                {
                   // BindCustomersListMultiSelect();
                    if (ddlAuditRole.SelectedItem.Text == "Partner")
                    {
                        divCustomerList.Style.Add("display", "block");
                        divCompanyAdminRole.Visible = true;
                        rblCompanyAdminRole.SelectedValue = "1";
                    }
                    else if (ddlAuditRole.SelectedItem.Text == "Manager")
                    {
                        divCustomerList.Style.Add("display", "block");
                        divCompanyAdminRole.Visible = true;
                        rblCompanyAdminRole.SelectedValue = "0";
                    }
                    else
                    {
                        divCustomerList.Style.Add("display", "none");
                        divCompanyAdminRole.Visible = false;
                    }
                }
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlAuditRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));

                if (divReportingTo.Visible)
                {
                    BindReportingTo();
                }

                if (Convert.ToInt32(ddlAuditRole.SelectedValue) == 2 || Convert.ToInt32(ddlAuditRole.SelectedValue) == 1 || Convert.ToInt32(ddlAuditRole.SelectedValue) == 8 || Convert.ToInt32(ddlAuditRole.SelectedValue) == 12)
                {
                    rblAuditRole.Enabled = false;
                    rblAuditRole.ClearSelection();
                }
                else
                {
                    rblAuditRole.Enabled = true;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                
                if (divReportingTo.Visible)
                {
                    BindReportingTo();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private void BindCustomersListMultiSelect()
        {
            try
            {
                long ServiceProviderID = AuthenticationHelper.ServiceProviderID;
                var customerList = RiskCategoryManagement.GetCompanyAdminServiceProviderCustomer(ServiceProviderID);
                if (customerList != null)
                {
                    ddlCustomerChk.DataTextField = "CustomerName";
                    ddlCustomerChk.DataValueField = "CustomerId";
                    ddlCustomerChk.DataSource = customerList;
                    ddlCustomerChk.DataBind();
                }
                else
                {
                    ddlCustomerChk.DataSource = null;
                    ddlCustomerChk.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindReportingTo()
        {
            try
            {
                ddlReportingTo.DataTextField = "Name";
                ddlReportingTo.DataValueField = "ID";
                int CustomerID = 0;
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "CADMN")
                {
                    CustomerID = (int)AuthenticationHelper.CustomerID;
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["customerId"])))
                    {
                        if (Convert.ToString(Session["customerId"]) != "-1")
                        {
                            CustomerID = Convert.ToInt32(Session["customerId"]);
                        }
                    }
                }

                ddlReportingTo.DataSource = UserManagement.GetAllByCustomerID(CustomerID, null);
                ddlReportingTo.DataBind();

                ddlReportingTo.Items.Insert(0, new ListItem("Select Reporting to person", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindParameters(List<UserParameterValueInfo> userParameterValues = null)
        {
            try
            {
                if (userParameterValues == null)
                {
                    userParameterValues = new List<UserParameterValueInfo>();
                    var userParameters = UserManagement.GetAllUserParameters();
                    userParameters.ForEach(entry =>
                    {
                        userParameterValues.Add(new UserParameterValueInfo()
                        {
                            UserID = -1,
                            ParameterID = entry.ID,
                            ValueID = -1,
                            Name = entry.Name,
                            DataType = (DataType)entry.DataType,
                            Length = (int)entry.Length,
                            Value = string.Empty
                        });
                    });
                }

                repParameters.DataSource = userParameterValues;
                repParameters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void AddNewUser(int CustomerID, string CustomerName)
        {
            try
            {
                tbxFirstName.Text = string.Empty;
                tbxLastName.Text = string.Empty;
                tbxDesignation.Text = string.Empty;
                tbxEmail.Text = string.Empty;
                tbxContactNo.Text = string.Empty;
                if (ddlRole.Items.Count > 0)
                {
                    ddlRole.SelectedValue = "-1";
                }
                if (ddlAuditRole.Items.Count > 0)
                {
                    ddlAuditRole.SelectedValue = "-1";
                }
                if (ddlSECRole.Items.Count > 0)
                {
                    ddlSECRole.SelectedValue = "-1";
                }

                if (ddlHRRole.Items.Count > 0)
                {
                    ddlHRRole.SelectedValue = "-1";
                }
                txtStartDate.Text = string.Empty;
                txtEndDate.Text = string.Empty;
                txtperiodStartDate.Text = string.Empty;
                txtperiodStartDate.Text = string.Empty;
                if (ddlReportingTo.Items.Count > 0)
                {
                    ddlReportingTo.SelectedValue = "-1";
                }
                tbxAddress.Text = string.Empty;
                cvEmailError.Attributes["class"] = "hidden";
                cvEmailError.ErrorMessage = string.Empty;
                upUsers.Update();
                ViewState["Mode"] = 0;
                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                if (!string.IsNullOrEmpty(CustomerName))
                {
                    lblCustomerName.InnerText = CustomerName;
                }
                Session["customerId"] = CustomerID;

                EnableDisableRole(CustomerID);
                tbxEmail.Enabled = true;
                rblAuditRole.Enabled = true;
                rblAuditRole.ClearSelection();
                ddlDepartment.ClearSelection();

                ddlRole.ClearSelection();
                ddlSECRole.ClearSelection();
                ddlAuditRole.ClearSelection();
                ddlHRRole.ClearSelection();

                ddlRole.SelectedValue = "-1";
                ddlAuditRole.SelectedValue = "-1";
                ddlSECRole.SelectedValue = "-1";
                ddlHRRole.SelectedValue = "-1";
                BindParameters();
                upUsers.Update(); upUsers_Load(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void AddNewUser()
        {
            try
            {
                ViewState["Mode"] = 0;

                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                tbxEmail.Enabled = true;
                rblAuditRole.Enabled = true;
                rblAuditRole.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlRole.ClearSelection();
                ddlSECRole.ClearSelection();
                ddlAuditRole.ClearSelection();
                ddlHRRole.ClearSelection();

                ddlRole.SelectedValue = "-1";
                ddlAuditRole.SelectedValue = "-1";
                ddlSECRole.SelectedValue = "-1";
                ddlHRRole.SelectedValue = "-1";
                BindParameters();

                int CustomerID = -1;
                CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                EnableDisableRole(CustomerID);

                upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void EditUserInformation(int userID, string customerName, int customerId)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "disableCombobox", "disableCombobox();", true);
                ViewState["Mode"] = 1;
                ViewState["UserID"] = userID;
                Session["MappedUserId"] = userID;
                User user = UserManagement.GetByID(userID);

                List<UserParameterValueInfo> userParameterValues = UserManagement.GetParameterValuesByUserID(userID);
                if (!string.IsNullOrEmpty(customerName))
                {
                    lblCustomerName.InnerText = customerName;
                }
                Session["customerId"] = customerId;
                tbxFirstName.Text = user.FirstName;
                tbxLastName.Text = user.LastName;
                tbxDesignation.Text = user.Designation;
                tbxEmail.Text = user.Email;
                tbxContactNo.Text = user.ContactNumber;
                tbxAddress.Text = user.Address;


                if (user.IsHead == true)
                    chkHead.Checked = true;
                else
                    chkHead.Checked = false;

                divSECRole.Visible = false;
                divAuditRole.Visible = false;
                divComplianceRole.Visible = false;
                divHRRole.Visible = false;

                #region Added by rahul 8 AUg 2020
                var Productdetails = GetByProductID(Convert.ToInt32(user.CustomerID));

                if (Productdetails.Contains(1))
                {
                    divComplianceRole.Visible = true;
                }

                if (Productdetails.Contains(3))
                {
                    divAuditRole.Visible = true;
                }
                else if (Productdetails.Contains(4))
                {
                    divAuditRole.Visible = true;
                }
                else
                {
                    divAuditRole.Visible = false;
                }

                if (Productdetails.Contains(8))
                {
                    divSECRole.Visible = true;
                }
                else
                {
                    divSECRole.Visible = false;
                }

                if (Productdetails.Contains(9))
                {
                    divHRRole.Visible = true;
                }
                else
                {
                    divHRRole.Visible = false;
                }
                ddlRole.SelectedValue = user.RoleID != null ? user.RoleID.ToString() : "-1";
                ddlRole_SelectedIndexChanged(null, null);
                ddlHRRole.SelectedValue = user.HRRoleID != null ? user.HRRoleID.ToString() : "-1";
                ddlSECRole.SelectedValue = user.SecretarialRoleID != null ? user.SecretarialRoleID.ToString() : "-1";

                #endregion

                mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                if (mstuser != null)
                {
                    BindCustomersListMultiSelect();
                    if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "SADMN")
                    {
                        if (mstuser.PrimaryRoleAudit == "Partner")
                        {
                            ddlAuditRole.SelectedValue = "4";
                            ddlAuditRole_SelectedIndexChanged(null, null);
                            if (mstuser.IsAuditHeadOrMgr != null)
                            {
                                if (mstuser.IsAuditHeadOrMgr == "AH")
                                {
                                    if (mstuser.RoleID==2)
                                    {
                                        rblCompanyAdminRole.SelectedValue = "1";
                                    }
                                    else
                                    {
                                        rblCompanyAdminRole.SelectedValue = "0";
                                    }
                                    rblAuditRole.SelectedValue = "IAH";
                                    rblAuditRole_SelectedIndexChanged(null, null);
                                    List<int?> AH_AMCustomerID = UserManagementRisk.getCustomerId(userID, "Partner");
                                    for (int i = 0; i < ddlCustomerChk.Items.Count; i++)
                                    {
                                        if (AH_AMCustomerID != null)
                                        {
                                            foreach (var item in AH_AMCustomerID)
                                            {
                                                if (ddlCustomerChk.Items[i].Value == Convert.ToString(item))
                                                {
                                                    ddlCustomerChk.Items[i].Selected = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (mstuser.IsAuditHeadOrMgr == "AM")
                                {
                                    rblAuditRole.SelectedValue = "IAM";
                                    rblAuditRole_SelectedIndexChanged(null, null); List<int?> AH_AMCustomerID = UserManagementRisk.getCustomerId(userID, "Audit Manager");
                                    for (int i = 0; i < ddlCustomerChk.Items.Count; i++)
                                    {
                                        if (AH_AMCustomerID != null)
                                        {
                                            foreach (var item in AH_AMCustomerID)
                                            {
                                                if (ddlCustomerChk.Items[i].Value == Convert.ToString(item))
                                                {
                                                    ddlCustomerChk.Items[i].Selected = true;

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (mstuser.PrimaryRoleAudit == "Audit Head")
                            {
                                ddlAuditRole.SelectedValue = "3";
                                ddlAuditRole.Enabled = true;
                                ddlAuditRole_SelectedIndexChanged(null, null);
                                List<int?> AH_AMCustomerID = UserManagementRisk.getCustomerId(userID, "Audit Head");
                                for (int i = 0; i < ddlCustomerChk.Items.Count; i++)
                                {
                                    if (AH_AMCustomerID != null)
                                    {
                                        foreach (var item in AH_AMCustomerID)
                                        {
                                            if (ddlCustomerChk.Items[i].Value == Convert.ToString(item))
                                            {
                                                ddlCustomerChk.Items[i].Selected = true;

                                            }
                                        }
                                    }
                                }
                            }
                            else if (mstuser.PrimaryRoleAudit == "Manager")
                            {
                                ddlAuditRole.SelectedValue = "2";
                                ddlAuditRole_SelectedIndexChanged(null, null);
                                ddlAuditRole.Enabled = true;
                                List<int?> AH_AMCustomerID = UserManagementRisk.getCustomerId(userID, "Manager");
                                if (AH_AMCustomerID != null)
                                {
                                    foreach (var item in AH_AMCustomerID)
                                    {
                                        for (int i = 0; i < ddlCustomerChk.Items.Count; i++)
                                        {
                                            if (ddlCustomerChk.Items[i].Value == Convert.ToString(item))
                                            {
                                                ddlCustomerChk.Items[i].Selected = true;
                                            }
                                        }
                                    }
                                }
                                if (mstuser.RoleID == 2)
                                {
                                    divCompanyAdminRole.Visible = true;
                                    rblCompanyAdminRole.SelectedValue = "1";
                                }
                            }
                            else if (mstuser.PrimaryRoleAudit == "Executive")
                            {
                                ddlAuditRole.SelectedValue = "1";
                                ddlAuditRole_SelectedIndexChanged(null, null);
                                ddlAuditRole.Enabled = true;
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(user.PrimaryRoleAudit))
                                {
                                    if (mstuser.RoleID == 2)
                                    {
                                        List<int?> AH_AMCustomerID = UserManagementRisk.getCustomerId(userID, "Audit Head");
                                        for (int i = 0; i < ddlCustomerChk.Items.Count; i++)
                                        {
                                            if (AH_AMCustomerID != null)
                                            {
                                                foreach (var item in AH_AMCustomerID)
                                                {
                                                    if (ddlCustomerChk.Items[i].Value == Convert.ToString(item))
                                                    {
                                                        ddlCustomerChk.Items[i].Selected = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ddlAuditRole.SelectedValue = "-1";
                                ddlAuditRole_SelectedIndexChanged(null, null);
                                ddlAuditRole.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        if (mstuser.IsHead == true)
                        {
                            ddlroleAH.SelectedValue = "3";
                        }
                        else if (mstuser.RoleID == 8)
                        {
                            ddlroleAH.SelectedValue = "2";
                        }
                        else
                        {
                            ddlroleAH.SelectedValue = "1";
                        }
                    }
                }
                if (user.RoleID == 2)
                {
                    ddlAuditRole.Enabled = false;
                    divrblAuditRole.Style.Add("display", "block");
                }
                else
                {
                    ddlAuditRole.Enabled = true;
                    divrblAuditRole.Style.Add("display", "none");
                }
                ddlReportingTo.SelectedValue = user.ReportingToID != null ? user.ReportingToID.ToString() : "-1";

                BindParameters(userParameterValues);
                upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int UserID = 0;
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);


                int getproductCOMPLIANCE = -1;
                int getproductAudit = -1;
                int getproductHR = -1;
                int getproductSec = -1;
                var Productdetails = GetByProductID(Convert.ToInt32(customerID));
                string Isauditheadormgr = null;
                if (Productdetails.Contains(1))
                {
                    getproductCOMPLIANCE = Convert.ToInt32(ddlRole.SelectedValue);
                }
                if (Productdetails.Contains(8))
                {
                    getproductSec = Convert.ToInt32(ddlSECRole.SelectedValue);
                }
                if (Productdetails.Contains(9))
                {
                    getproductHR = Convert.ToInt32(ddlHRRole.SelectedValue);
                }


                List<int?> customerIdList = new List<int?>();
                customerIdList.Clear();
                if (ddlCustomerChk.Items.Count > 0)
                {
                    for (int i = 0; i < ddlCustomerChk.Items.Count; i++)
                    {
                        if (ddlCustomerChk.Items[i].Selected)
                        {
                            customerIdList.Add(Convert.ToInt32(ddlCustomerChk.Items[i].Value));
                        }
                    }
                }
                string RoleNameVal = string.Empty;
                #region Compliance User
                User user = new User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    Address = tbxAddress.Text,
                    RoleID = getproductCOMPLIANCE,
                    DepartmentID = -1,
                    IsExternal = false
                };
                //user.SecretarialRoleID = getproductSec;
                if (getproductSec == -1)
                {
                    user.SecretarialRoleID = null;
                }
                else
                {
                    user.SecretarialRoleID = getproductSec;
                }
                user.HRRoleID = getproductHR;

                List<UserParameterValue> parameters = new List<UserParameterValue>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                    parameters.Add(new UserParameterValue()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }


                #endregion

                #region Risk User

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    Address = tbxAddress.Text,
                    DepartmentID = -1,
                    IsExternal = false,
                    EnType = "A",
                };
                if (getproductSec == -1)
                {
                    mstUser.SecretarialRoleID = null;
                }
                else
                {
                    mstUser.SecretarialRoleID = getproductSec;
                }
                mstUser.HRRoleID = getproductHR;
                List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk> parametersRisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                    parametersRisk.Add(new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }

                #endregion
                if (ddlReportingTo.SelectedValue != "-1" && ddlReportingTo.SelectedValue != "")
                {
                    user.ReportingToID = Convert.ToInt64(ddlReportingTo.SelectedValue);
                    mstUser.ReportingToID = Convert.ToInt64(ddlReportingTo.SelectedValue);
                }
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "SADMN")
                {
                    if (ddlAuditRole.SelectedIndex != -1)
                    {
                        //if (ddlAuditRole.SelectedItem.Text == "Audit Head")
                        //{
                        //    user.IsAuditHeadOrMgr = "AH";
                        //    mstUser.IsAuditHeadOrMgr = "AH";
                        //    mstUser.RoleID = 7;
                        //    RoleNameVal = "Audit Head";
                        //    user.PrimaryRoleAudit = "Audit Head";
                        //    mstUser.PrimaryRoleAudit = "Audit Head";
                        //    if (rblCompanyAdminRole.SelectedItem.Text == "Yes")
                        //    {
                        //        mstUser.RoleID = 2;
                        //    }
                        //}else
                        if (ddlAuditRole.SelectedItem.Text == "Manager")
                        {
                            user.IsAuditHeadOrMgr = "AM";
                            mstUser.IsAuditHeadOrMgr = "AM";
                            mstUser.RoleID = 7;
                            RoleNameVal = "Manager";
                            user.PrimaryRoleAudit = "Manager";
                            mstUser.PrimaryRoleAudit = "Manager";
                            if (rblCompanyAdminRole.SelectedItem.Text == "Yes")
                            {
                                mstUser.RoleID = 2;
                            }
                        }
                        else if (ddlAuditRole.SelectedItem.Text == "Partner")
                        {
                            mstUser.RoleID = 2;
                            user.PrimaryRoleAudit = "Partner";
                            mstUser.PrimaryRoleAudit = "Partner";
                            user.IsAuditHeadOrMgr = "AH";
                            mstUser.IsAuditHeadOrMgr = "AH";
                            RoleNameVal = "Partner";
                            if (rblCompanyAdminRole.SelectedItem.Text == "Yes")
                            {
                                mstUser.RoleID = 2;
                            }
                            else
                            {
                                mstUser.RoleID = 7;
                            }
                        }
                        else
                        {
                            user.PrimaryRoleAudit = "Executive";
                            mstUser.PrimaryRoleAudit = "Executive";
                            mstUser.RoleID = 7;
                        }
                    }
                    user.CustomerID = (int)AuthenticationHelper.CustomerID;
                    mstUser.CustomerID = (int)AuthenticationHelper.CustomerID;
                }
                else
                {
                    if (ddlroleAH.SelectedIndex != -1)
                    {
                        if (ddlroleAH.SelectedItem.Text == "Auditee")
                        {
                            mstUser.RoleID = 7;
                        }
                        else if (ddlroleAH.SelectedItem.Text == "Management")
                        {
                            mstUser.RoleID = 8;
                        }
                        else if (ddlroleAH.SelectedItem.Text == "Department Head")
                        {
                            mstUser.RoleID = 7;
                            user.IsHead = true;
                            mstUser.IsHead = true;
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(Session["customerId"])))
                    {
                        if (Convert.ToString(Session["customerId"]) != "-1")
                        {
                            user.CustomerID = Convert.ToInt32(Session["customerId"]);
                            mstUser.CustomerID = Convert.ToInt32(Session["customerId"]);
                        }
                    }
                }

                if ((int)ViewState["Mode"] == 1)
                {
                    user.ID = Convert.ToInt32(ViewState["UserID"]);
                    mstUser.ID = Convert.ToInt32(ViewState["UserID"]);
                }

                bool emailExists;
                UserManagement.Exists(user, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                UserManagementRisk.Exists(mstUser, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                bool result = false;
                int resultValue = 0;
                if ((int)ViewState["Mode"] == 0)
                {
                    bool Result = true;
                    if (AuthenticationHelper.Role.Equals("CADMN"))
                    {
                        Result = ICIAManagement.ServiceProviderUserLimit(AuthenticationHelper.ServiceProviderID, AuthenticationHelper.CustomerID);
                    }
                    if (Result)
                    {
                        user.CreatedBy = AuthenticationHelper.UserID;
                        user.CreatedByText = AuthenticationHelper.User;
                        string passwordText = Util.CreateRandomPassword(10);
                        //user.Password = Util.CalculateMD5Hash(passwordText);
                        user.Password = Util.CalculateAESHash(passwordText);
                        string message = SendNotificationEmail(user, passwordText);

                        mstUser.CreatedBy = AuthenticationHelper.UserID;
                        mstUser.CreatedByText = AuthenticationHelper.User;
                        mstUser.Password = Util.CalculateAESHash(passwordText);
                        //mstUser.Password = Util.CalculateMD5Hash(passwordText);
                        //result = UserManagement.Create(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        if (user.RoleID == -1)
                        {
                            user.RoleID = 7;
                        }
                        resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        if (resultValue > 0)
                        {
                            if (mstUser.RoleID == -1)
                            {
                                mstUser.RoleID = 7;
                            }
                            UserID = resultValue;
                            result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                            if (result == false)
                            {
                                UserManagement.deleteUser(resultValue);
                            }
                        }
                        UserCustomerMapping objUserCustMapping = new UserCustomerMapping()
                        {
                            UserID = resultValue,
                            CustomerID = (int)AuthenticationHelper.CustomerID,
                            ProductID = 4,
                            IsActive = true,
                            CreatedOn = DateTime.Now,
                        };

                        UserManagementRisk.CreateUserCustomerMapping(objUserCustMapping);

                        #region Usercustomer Mapping
                        if (!string.IsNullOrEmpty(RoleNameVal))
                        {
                            foreach (int? custId in customerIdList)
                            {
                                Tbl_UserCustomerMapping objectUserCustomerMapping = new Tbl_UserCustomerMapping()
                                {
                                    // CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue),
                                    CustomerId = Convert.ToInt32(custId),
                                    RoleName = RoleNameVal,
                                    UserId = resultValue,
                                    IsActive = true,
                                };
                                long alreadyExistedId = 0;
                                alreadyExistedId = RiskCategoryManagement.CheckUserCustomerMappingExist(objectUserCustomerMapping.UserId, objectUserCustomerMapping.CustomerId);
                                //string recordForUpdate = Session["recordForUpdate"].ToString();
                                if (alreadyExistedId == 0 && (int)ViewState["Mode"] == 0)
                                {
                                    objectUserCustomerMapping.CreatedOn = DateTime.Now;
                                    objectUserCustomerMapping.CreatedBy = AuthenticationHelper.UserID;
                                    RiskCategoryManagement.SaveUserCustomerMapping(objectUserCustomerMapping);
                                }
                                else if (alreadyExistedId > 0 && (int)ViewState["Mode"] == 1)
                                {
                                    objectUserCustomerMapping.Id = alreadyExistedId;
                                    objectUserCustomerMapping.UpdatedOn = DateTime.Now;
                                    objectUserCustomerMapping.UpdatedBy = AuthenticationHelper.UserID;
                                    RiskCategoryManagement.UpdateUserCustomerMapping(objectUserCustomerMapping);
                                }
                            }
                        }

                        #endregion
                        cvEmailError.IsValid = false;
                        cvEmailError.ErrorMessage = "User Created Successfully.";
                        ddlCustomerChk.ClearSelection();
                    }
                    else
                    {
                        cvEmailError.IsValid = false;
                        cvEmailError.ErrorMessage = "Facility to create more users is not available in the Free version. Kindly contact Avantis to activate the same.";
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    int UpdateUserID = 0;
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["MappedUserId"])))
                    {
                        UpdateUserID = Convert.ToInt32(Session["MappedUserId"]);
                    }
                    List<int?> GetAssignedCustomerIdsList = RiskCategoryManagement.GetAssignedCustomerIds(Convert.ToInt16(Session["MappedUserId"].ToString()));
                    if (GetAssignedCustomerIdsList != null)
                    {
                        List<int?> excudedCustomerIds = GetAssignedCustomerIdsList.Except(customerIdList).ToList();
                        if (excudedCustomerIds.Count > 0)
                        {
                            foreach (int custid in excudedCustomerIds)
                            {
                                RiskCategoryManagement.InactiveUserMapping(UpdateUserID, custid);
                            }
                        }
                    }
                    foreach (int? custId in customerIdList)
                    {
                        Tbl_UserCustomerMapping objectUserCustomerMapping = new Tbl_UserCustomerMapping()
                        {
                            CustomerId = Convert.ToInt32(custId),
                            RoleName = RoleNameVal,
                            UserId = UpdateUserID,
                            IsActive = true,
                        };
                        long inactiveCustomerId = RiskCategoryManagement.GetInInactiveUserMapping(objectUserCustomerMapping.UserId, objectUserCustomerMapping.CustomerId);
                        if (inactiveCustomerId != 0)
                        {
                            objectUserCustomerMapping.Id = inactiveCustomerId;
                            objectUserCustomerMapping.UpdatedOn = DateTime.Now;
                            objectUserCustomerMapping.UpdatedBy = AuthenticationHelper.UserID;
                            RiskCategoryManagement.UpdateUserCustomerMapping(objectUserCustomerMapping);
                        }
                        else
                        {
                            long alreadyExistedId = 0;
                            alreadyExistedId = RiskCategoryManagement.CheckUserCustomerMappingExist(objectUserCustomerMapping.UserId, objectUserCustomerMapping.CustomerId);
                            if (alreadyExistedId == 0)
                            {
                                objectUserCustomerMapping.CreatedOn = DateTime.Now;
                                objectUserCustomerMapping.CreatedBy = AuthenticationHelper.UserID;
                                RiskCategoryManagement.SaveUserCustomerMapping(objectUserCustomerMapping);
                            }
                            else if (alreadyExistedId > 0)
                            {
                                objectUserCustomerMapping.Id = alreadyExistedId;
                                objectUserCustomerMapping.UpdatedOn = DateTime.Now;
                                objectUserCustomerMapping.UpdatedBy = AuthenticationHelper.UserID;
                                RiskCategoryManagement.UpdateUserCustomerMapping(objectUserCustomerMapping);
                            }
                        }
                    }
                    UserID = Convert.ToInt32(ViewState["UserID"]);//for Unit assignment
                    //Get Existing Users Auditor ID and Compliance-Audit Product RoleID
                    User complianceUser = UserManagement.GetByID(UserID);
                    mst_User auditUser = UserManagementRisk.GetByID(UserID);

                    if (complianceUser != null)
                    {
                        user.AuditorID = complianceUser.AuditorID;
                    }

                    if (auditUser != null)
                    {
                        mstUser.AuditorID = auditUser.AuditorID;
                        //mstUser.RoleID = auditUser.RoleID;
                    }

                    User User = UserManagement.GetByID(Convert.ToInt32(user.ID));
                    if (tbxEmail.Text.Trim() != User.Email)
                    {
                        string message = SendNotificationEmailChanged(user);
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                    }
                    if (user.RoleID == -1)
                    {
                        user.RoleID = 7;
                    }
                    if (mstUser.RoleID == -1)
                    {
                        mstUser.RoleID = 7;
                    }
                    result = UserManagement.Update(user, parameters);
                    result = UserManagementRisk.Update(mstUser, parametersRisk);
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User Updated Successfully.";
                }
                else
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }

                if (ddlAuditRole.SelectedItem.Text == "Manager" || ddlAuditRole.SelectedItem.Text == "Partner")
                {
                    List<EntitiesAssignmentAuditManagerRisk> EntitiesAssignmentAuditManagerRisklist = new List<EntitiesAssignmentAuditManagerRisk>();
                    foreach (var Customeritem in customerIdList)
                    {
                        List<long> Processlist = ProcessManagement.GetProcessListID(Customeritem);
                        if (Processlist.Count == 0)
                        {
                            ProcessManagement.CreateProcessSubprocessPredefined((int)Customeritem, AuthenticationHelper.UserID);
                            Processlist = ProcessManagement.GetProcessListID(Customeritem);
                        }
                        List<int> Branchlist = CustomerBranchManagementRisk.GetCustomerBranchList(Customeritem);
                        if (Processlist.Count > 0)
                        {
                            foreach (var aItem in Processlist)
                            {
                                if (Branchlist.Count > 0)
                                {
                                    for (int i = 0; i < Branchlist.Count; i++)
                                    {
                                        if (AssignEntityAuditManager.EntitiesAssignmentAuditManagerRiskExist((int)Customeritem, Branchlist[i], UserID, (int)aItem))
                                        {
                                            AssignEntityAuditManager.EditEntitiesAssignmentAuditManagerRiskExist((int)Customeritem, Branchlist[i], UserID, (int)aItem, AuthenticationHelper.UserID);
                                        }
                                        else
                                        {
                                            EntitiesAssignmentAuditManagerRisk objEntitiesAssignmentrisk = new EntitiesAssignmentAuditManagerRisk();
                                            objEntitiesAssignmentrisk.UserID = UserID;
                                            objEntitiesAssignmentrisk.CustomerID = (int)Customeritem;
                                            objEntitiesAssignmentrisk.BranchID = Branchlist[i];
                                            objEntitiesAssignmentrisk.CreatedOn = DateTime.Today.Date;
                                            objEntitiesAssignmentrisk.ISACTIVE = true;
                                            objEntitiesAssignmentrisk.ProcessId = (int)aItem;
                                            objEntitiesAssignmentrisk.CretedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            EntitiesAssignmentAuditManagerRisklist.Add(objEntitiesAssignmentrisk);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    bool sucess = AssignEntityAuditManager.CreateEntitiesAssignmentAuditManagerRisklist(EntitiesAssignmentAuditManagerRisklist);
                }

                if (result)
                {
                    if (UserImageUpload.HasFile)
                    {
                        string fileName = user.ID + "-" + user.FirstName + " " + user.LastName + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                        UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);

                        string filepath = "~/UserPhotos/" + fileName;

                        UserManagement.UpdateUserPhoto(Convert.ToInt32(user.ID), filepath, fileName);
                        UserManagementRisk.UpdateUserPhoto(Convert.ToInt32(mstUser.ID), filepath, fileName);
                    }
                }
                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    ;

                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        public event EventHandler OnSaved;

        protected void ddlroleAH_SelectedIndexChanged(object sender, EventArgs e)
        {
            divReportingTo.Visible = false;
        }

    }
}