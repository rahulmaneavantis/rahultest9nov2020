﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using OfficeOpenXml;
using System.IO;
using System.Drawing;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Configuration;
using System.Data;
using System.Net.Mail;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class ApproverSummaryStatusReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["RahulRoleID"] != null)
                {
                    if (Session["RahulRoleID"].ToString().Trim() == "6" || Session["RahulRoleID"].ToString().Trim() == "3")
                    {
                        BindMonth(DateTime.UtcNow.Year);
                        BindYear();
                        BindSummaryStatusReport(Convert.ToInt32(ddlPeriod.SelectedValue));
                    }
                }                
            }
        }
        protected void upManagementDashboard_Load(object sender, EventArgs e)
        {
            try
            {
            
                ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnExcel);
                ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnPrint);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMonth(Convert.ToInt32(ddlyear.SelectedValue));
            BindSummaryStatusReport(Convert.ToInt32(ddlPeriod.SelectedValue));
        }

        protected void ddlmonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSummaryStatusReport(Convert.ToInt32(ddlPeriod.SelectedValue));
        }

        private void BindMonth(int year)
        {
            try
            {
                ddlmonths.DataTextField = "Name";
                ddlmonths.DataValueField = "ID";
                int selectedMonth = 0;
                List<NameValue> months;
                if (year == DateTime.UtcNow.Year)
                {
                    months = Enumerations.GetAll<Month>().Where(entry => entry.ID <= DateTime.UtcNow.Month).ToList();
                    selectedMonth = months.Count;
                }
                else
                {
                    months = Enumerations.GetAll<Month>();
                    selectedMonth = 1;
                }

                ddlmonths.DataSource = months;
                ddlmonths.DataBind();
                ddlmonths.SelectedValue = Convert.ToString(selectedMonth);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindYear()
        {

            int CurrentYear = DateTime.Now.Year;
            for (int i = 1; i < 100; ++i)
            {
                System.Web.UI.WebControls.ListItem tmp = new System.Web.UI.WebControls.ListItem();
                tmp.Value = CurrentYear.ToString();
                tmp.Text = CurrentYear.ToString();
                ddlyear.Items.Add(tmp);
                CurrentYear = DateTime.Now.AddYears(-i).Year;

            }

        }

        private void BindSummaryStatusReport(int period)
        {
            try
            {
                int year = Convert.ToInt32(ddlyear.SelectedValue);
                int month = Convert.ToInt32(ddlmonths.SelectedValue);
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                grdSummaryStatusReport.DataSource = AssignEntityManagement.GetManagementSummaryStatusReport(customerid,AuthenticationHelper.UserID, year, month, period, true);
                grdSummaryStatusReport.DataBind();
                //divSumaaryReport.InnerHtml = GetHtmlTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSummaryStatusReport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].Visible = false;
                    e.Row.Cells[1].Visible = false;
                }

                int period = Convert.ToInt32(ddlPeriod.SelectedValue);
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                    int margin = Convert.ToInt32(e.Row.Cells[0].Text.Split('#')[1]) * 20;
                    e.Row.Cells[0].Style.Add("padding-left", margin + "px");
                    e.Row.Cells[0].Text = "-" + e.Row.Cells[0].Text.Split('#')[0];
                    e.Row.Cells[1].Style.Add("width", "200px");
                    e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Left;
                    e.Row.Cells[1].Visible = false;

                    for (int i = 2; i <= (period * 5) + 1; i++)
                    {

                        if (e.Row.Cells[i].Text == "GREEN")
                        {
                            e.Row.Cells[i].Style.Add("background", "#00CC33");
                            e.Row.Cells[i].Text = "";
                        }
                        else if (e.Row.Cells[i].Text == "BROWN")
                        {
                            e.Row.Cells[i].Style.Add("background", "#FF9933");
                            e.Row.Cells[i].Text = "";
                        }
                        else if (e.Row.Cells[i].Text == "RED")
                        {
                            e.Row.Cells[i].Style.Add("background", "#FF0033");
                            e.Row.Cells[i].Text = "";
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblTableTitel.Text = "Past " + ddlPeriod.SelectedValue + " Months's Entity-wise Compliance Summary";
            BindSummaryStatusReport(Convert.ToInt32(ddlPeriod.SelectedValue));
        }

        protected void grdSummaryStatusReport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    SetTitleOfCell(e.Row);
                    int period = Convert.ToInt32(ddlPeriod.SelectedValue);
                    int year = Convert.ToInt32(ddlyear.SelectedValue);
                    int month = Convert.ToInt32(ddlmonths.SelectedValue);
                    //DateTime startDate = new DateTime(year, month, 1);
                    DateTime startDate = new DateTime(year, month, 1).AddMonths(-period);

                    GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                    TableCell HeaderCell = new TableCell();
                    HeaderCell.Text = "<label style=\"width:350px;display: block;\">Legal Entity/Location</label>";
                    HeaderCell.RowSpan = 2;
                    //HeaderCell.Style.Add("width", "300px");
                    HeaderCell.Style.Add("background", "url('images/ui-bg_gloss-wave_55_5c9ccc_500x100.png') 50% 50% repeat-x");
                    HeaderCell.Style.Add("text-align", "center !important");
                    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderRow.Cells.Add(HeaderCell);

                    HeaderCell = new TableCell();
                    HeaderCell.Text = "<label style=\"width:200px;display: block;\">Approver Name</label>";
                    HeaderCell.RowSpan = 2;
                    //HeaderCell.Style.Add("padding-right", "100px");
                    HeaderCell.Style.Add("background", "url('images/ui-bg_gloss-wave_55_5c9ccc_500x100.png') 50% 50% repeat-x");
                    HeaderCell.Style.Add("text-align", "center");
                    HeaderCell.Style.Add("border-color", "White");
                    HeaderCell.Visible = false;
                    HeaderCell.HorizontalAlign = HorizontalAlign.Left;
                    HeaderRow.Cells.Add(HeaderCell);

                    for (int i = 1; i <= period; i++)
                    {
                        HeaderCell = new TableCell();
                        //HeaderCell.Text = DateTime.UtcNow.AddMonths(-i).ToString("MMM") + " - " + DateTime.UtcNow.AddMonths(-i).Year;
                        HeaderCell.Text = startDate.AddMonths(i).Month + "/" + startDate.AddMonths(i).ToString("yy");
                        HeaderCell.Style.Add("background", "url('images/ui-bg_gloss-wave_55_5c9ccc_500x100.png') 50% 50% repeat-x");
                        HeaderCell.Style.Add("border-color", "White");
                        HeaderCell.ColumnSpan = 5;
                        HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                        HeaderRow.Cells.Add(HeaderCell);
                    }

                    grdSummaryStatusReport.Controls[0].Controls.AddAt(0, HeaderRow);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void SetTitleOfCell(GridViewRow cell)
        {
            try
            {
                cell.Cells[2].Text = "Completed In Time";
                cell.Cells[3].Text = "Completed After Due Date";
                cell.Cells[4].Text = "Not Yet Completed";
                cell.Cells[5].Text = "Total";
                cell.Cells[6].Text = "Rating";
                cell.Cells[7].Text = "Completed In Time";
                cell.Cells[8].Text = "Completed After Due Date";
                cell.Cells[9].Text = "Not Yet Completed";
                cell.Cells[10].Text = "Total";
                cell.Cells[11].Text = "Rating";
                cell.Cells[12].Text = "Completed In Time";
                cell.Cells[13].Text = "Completed After Due Date";
                cell.Cells[14].Text = "Not Yet Completed";
                cell.Cells[15].Text = "Total";
                cell.Cells[16].Text = "Rating";
                if (Convert.ToInt32(ddlPeriod.SelectedValue) == 6)
                {
                    cell.Cells[17].Text = "Completed In Time";
                    cell.Cells[18].Text = "Completed After Due Date";
                    cell.Cells[19].Text = "Not Yet Completed";
                    cell.Cells[20].Text = "Total";
                    cell.Cells[21].Text = "Rating";
                    cell.Cells[22].Text = "Completed In Time";
                    cell.Cells[23].Text = "Completed After Due Date";
                    cell.Cells[24].Text = "Not Yet Completed";
                    cell.Cells[25].Text = "Total";
                    cell.Cells[26].Text = "Rating";
                    cell.Cells[27].Text = "Completed In Time";
                    cell.Cells[28].Text = "Completed After Due Date";
                    cell.Cells[29].Text = "Not Yet Completed";
                    cell.Cells[30].Text = "Total";
                    cell.Cells[31].Text = "Rating";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {

                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "StatusSummary.xls"));
                    Response.ContentType = "application/ms-excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);

                    string DashboardAsOn = "<h3>Summary Status Report as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text + "</h3>";
                    sw.Write(DashboardAsOn);
                    sw.Write("<br/>");
                    sw.Write("Past " + ddlPeriod.SelectedValue + " Months's Entity-wise Compliance Summary");
                    grdSummaryStatusReport.HeaderRow.Style.Add("background-color", "#5c9ccc");
                    grdSummaryStatusReport.RenderControl(htw);

                    Response.Write(sw.ToString().Replace("url('images/ui-bg_gloss-wave_55_5c9ccc_500x100.png')", "#5c9ccc"));
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void ExprotToExcel(GridView grdv)
        {
            try
            {
                grdv.AllowPaging = false;
                //Change the Header Row back to white color
                grdv.HeaderRow.Style.Add("background-color", "#FFFFFF");
                //Applying stlye to gridview header cells
                for (int i = 0; i < grdv.HeaderRow.Cells.Count; i++)
                {
                    grdv.HeaderRow.Cells[i].Style.Add("background-color", "#507CD1");
                }
                int j = 1;
                //This loop is used to apply stlye to cells based on particular row
                foreach (GridViewRow gvrow in grdv.Rows)
                {
                    gvrow.BackColor = Color.White;
                    if (j <= grdv.Rows.Count)
                    {
                        if (j % 2 != 0)
                        {
                            for (int k = 0; k < gvrow.Cells.Count; k++)
                            {
                                gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                            }
                        }
                    }
                    j++;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //public override void VerifyRenderingInServerForm(Control control)
        //{
        //    /* Verifies that the control is rendered */
        //}

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string Script = string.Empty;
                StringWriter stringWrite = new StringWriter();
                System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
                if (grdSummaryStatusReport is WebControl)
                {
                    Unit w = new Unit(100, UnitType.Percentage); ((WebControl)grdSummaryStatusReport).Width = w;
                }
                Page pg = new Page();
                pg.EnableEventValidation = false;
                if (Script != string.Empty)
                {
                    pg.ClientScript.RegisterStartupScript(pg.GetType(), "PrintJavaScript", Script);
                }

                HtmlForm frm = new HtmlForm();
                pg.Controls.Add(frm);
                frm.Attributes.Add("runat", "server");
                Label management = new Label();
                management.Text = "Approver Summary Status Report as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text;
                management.Style.Add("margin-bottom", "10px");
                management.Font.Bold = true;
                frm.Controls.Add(management);
                Label space = new Label();
                space.Width = Unit.Percentage(100);
                space.Height = Unit.Pixel(20);
                Label location = new Label();
                location.Text = "Logged in as on " + AuthenticationHelper.User;
                location.Font.Bold = true;
                frm.Controls.Add(location);
                frm.Controls.Add(space);
                frm.Controls.Add(grdSummaryStatusReport);
                pg.DesignerInitialize();
                pg.RenderControl(htmlWrite);
                string strHTML = stringWrite.ToString();
                strHTML = strHTML + "<style type='text/css'>th{background: #5c9ccc;} .ui-widget-header{background: #5c9ccc;}</style>";
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Write(strHTML);
                HttpContext.Current.Response.Write("<script>window.onload= function () { window.print();window.close();   }  </script>");
                HttpContext.Current.Response.End();
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            BindSummaryStatusReport(Convert.ToInt32(ddlPeriod.SelectedValue));

            Document pdfDoc = new Document(PageSize.A4_LANDSCAPE, 0, 0, 0, 0);
            DataTable dataToSummary = grdSummaryStatusReport.DataSource as DataTable;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                writer.CloseStream = false;
                pdfDoc.Open();

                try
                {
                    pdfDoc.Add(iTextSharp.text.Chunk.NEWLINE);
                    iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(PDFExportHelper.FormatPageHeaderPhrase("Summary Status Report"));
                    paragraph.SpacingAfter = 10f;
                    pdfDoc.Add(paragraph);
                    paragraph = new iTextSharp.text.Paragraph(PDFExportHelper.FormatHeaderPhrase("Summary Status Report as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text));
                    paragraph.SpacingAfter = 5f;
                    pdfDoc.Add(paragraph);

                    pdfDoc.Add(new iTextSharp.text.Paragraph(PDFExportHelper.FormatHeaderPhrase("Past " + ddlPeriod.SelectedValue + " Months's Entity-wise Compliance Summary")) { SpacingAfter = 5f });

                    PdfPTable dataTable = new PdfPTable(dataToSummary.Columns.Count);

                    pdfDoc.Add(SetStyleForPDFDoc(dataTable, dataToSummary));

                }
                finally
                {
                    pdfDoc.Close();
                }

                memoryStream.Position = 0;

                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                List<Attachment> attachment = new List<Attachment>();
                attachment.Add(new Attachment(new MemoryStream(bytes), "SummaryStatus.pdf"));

                User user = UserManagement.GetByID(AuthenticationHelper.UserID);
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (AuthenticationHelper.Role == "SADMN")
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }
                string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                 + "Dear " + user.FirstName + " " + user.LastName + ",<br /><br />"
                                 + "Here is Attached Management dashboard summary pdf.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "Management Dashboards.", message, attachment);

            }

        }

        protected PdfPTable SetStyleForPDFDoc(PdfPTable dataTable, DataTable dataToExport)
        {
            dataTable.DefaultCell.Padding = 3;
            dataTable.WidthPercentage = 100;//in percentage
            dataTable.DefaultCell.BorderWidth = 1;
            dataTable.DefaultCell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            dataTable.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;

            for (int colIndex = 0; colIndex < dataToExport.Columns.Count; colIndex++)
            {
                dataTable.AddCell(PDFExportHelper.FormatHeaderPhrase(dataToExport.Columns[colIndex].ColumnName));
            }

            dataTable.HeaderRows = 0;  // this is the end of the table header

            for (int rowIndex = 0; rowIndex < dataToExport.Rows.Count; rowIndex++)
            {
                for (int colIndex = 0; colIndex < dataToExport.Columns.Count; colIndex++)
                {
                    if (colIndex == 0)
                        dataTable.AddCell(PDFExportHelper.FormatPhrase(dataToExport.Rows[rowIndex][dataToExport.Columns[colIndex].ColumnName].ToString().Split('#')[0]));
                    else
                        dataTable.AddCell(PDFExportHelper.FormatPhrase(dataToExport.Rows[rowIndex][dataToExport.Columns[colIndex].ColumnName].ToString()));
                }
            }

            return dataTable;
        }
    }
}