﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Configuration;
using System.Threading;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Users;
using System.IO;
using System.Globalization;
using System.Text;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class UserDetailsControl : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomers();
                BindRoles();                
                BindRolesHR();
                BindRolesSEC();
                txtDepartment.Attributes.Add("readonly", "readonly");
                txtDepartment.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);                                
            }
        }
        protected void Upload(object sender, EventArgs e)
        {
            if (UserImageUpload.HasFile)
            {
                string[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg" };

                string ext = System.IO.Path.GetExtension(UserImageUpload.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Invalid File. Please upload a File with extension " + string.Join(",", validFileTypes);
                }
            }
        }
        protected void upUsers_Load(object sender, EventArgs e)
        {
            try
            {
                
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvDept\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeDatePickerFunctionEndDate", string.Format("initializeDatePickerFunctionEndDate(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeDatePickerFunctionEndDate1", string.Format("initializeDatePickerFunctionEndDate1(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeDatePickerFunctionEndDate2", string.Format("initializeDatePickerFunctionEndDate2(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeDatePickerFunctionEndDate3", string.Format("initializeDatePickerFunctionEndDate3(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewEdit", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDepartment(int CustomerID)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = CustomerID;
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        if (ddlCustomer.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                    }
                }
                if (customerID != -1)
                {
                    rptDepartment.DataSource = CompDeptManagement.FillDepartment(customerID);
                    rptDepartment.DataBind();


                    foreach (RepeaterItem aItem in rptDepartment.Items)
                    {
                        CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");

                        if (!chkDepartment.Checked)
                        {
                            chkDepartment.Checked = true;
                        }
                    }
                    CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                    DepartmentSelectAll.Checked = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upDepeList_Load(object sender, EventArgs e)
        {
        }
       
      
        public static List<Role> GetAll(bool? onlyForCompliance = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.ID == 28 || row.ID == 7 || row.ID == 8
                             select row);

                if (onlyForCompliance.HasValue)
                {
                    roles = roles.Where(entry => entry.IsForCompliance == onlyForCompliance.Value && entry.Code != "APPR");
                }
                return roles.ToList();
            }
        }

        private void BindRoles()
        {
            try
            {
                ddlRole.DataTextField = "Name";
                ddlRole.DataValueField = "ID";

                var roles = GetAll(false);
                ddlRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        public static List<Role> GetSECLimitedRole()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.IsForSecretarial == true
                             select row);

                return roles.ToList();
            }
        }

        public static List<Role> GetHRLimitedRole()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.ID == 14 || row.ID == 15 || row.ID == 16 || row.ID == 17 || row.ID == 18
                             select row);

                return roles.ToList();
            }
        }
        private void BindRolesHR()
        {
            try
            {
                ddlHRRole.DataTextField = "Name";
                ddlHRRole.DataValueField = "ID";

                var roles = RoleManagement.GetAll_HRCompliance_Roles();

                ddlHRRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlHRRole.DataBind();

                ddlHRRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRolesSEC()
        {
            try
            {
                ddlSECRole.DataTextField = "Name";
                ddlSECRole.DataValueField = "ID";

                var roles = GetSECLimitedRole();
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN")).ToList();
                }

                if (roles.Count > 0)
                {
                    List<string> secRoleCodes = new List<string> { "CEXCT", "DADMN", "CSMGR" };

                    roles = roles.Where(row => secRoleCodes.Contains(row.Code)).ToList();
                }

                ddlSECRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlSECRole.DataBind();

                ddlSECRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlAuditRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int Aurole = -1;
                if (ddlAuditRole.SelectedValue == "1")  //EXecutive
                {
                    Aurole = 7;
                }
                else if (ddlAuditRole.SelectedValue == "2") //Manager
                {
                    Aurole = 7;
                }
                else if (ddlAuditRole.SelectedValue == "4") //Partner
                {
                    Aurole = 2;
                }


                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(Aurole));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                if (roleCode.Equals("VAUDT"))
                {
                    divCustomerBranch.Visible = roleCode.Equals("VAUDT");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlSECRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlSECRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                if (roleCode.Equals("VAUDT"))
                {
                    divCustomerBranch.Visible = roleCode.Equals("VAUDT");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlHRRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                               
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlHRRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                if (roleCode.Equals("VAUDT"))
                {
                    divCustomerBranch.Visible = roleCode.Equals("VAUDT");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                if (roleCode.Equals("VAUDT"))
                {
                    divCustomerBranch.Visible = roleCode.Equals("VAUDT");
                }
                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewRoleChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }
                reqAudit1.Visible = false;
                reqAudit2.Visible = false;
                reqAudit3.Visible = false;
                reqAudit4.Visible = false;
                if (roleCode.Equals("AUDT"))
                {
                    Auditor1.Visible = true;
                    Auditor2.Visible = true;

                    reqAudit1.Visible = true;
                    reqAudit2.Visible = true;
                    reqAudit3.Visible = true;
                    reqAudit4.Visible = true;
                }
                else
                {
                    Auditor1.Visible = false;
                    Auditor2.Visible = false;
                    reqAudit1.Visible = true;
                    reqAudit2.Visible = true;
                    reqAudit3.Visible = true;
                    reqAudit4.Visible = true;
                }
                if (divCustomerBranch.Visible)
                {
                    ddlCustomer_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewRoleChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                //int customerID = -1;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                //}
                int distributorID = -1;
                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                ddlCustomer.DataSource = CustomerManagement.GetAllCustomers(customerID, AuthenticationHelper.Role, distributorID);               
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = string.Empty;
                BindCustomerBranches();
                BindReportingTo();
                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewCustomerChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
      
        private void BindReportingTo()
        {
            try
            {
                ddlReportingTo.DataTextField = "Name";
                ddlReportingTo.DataValueField = "ID";

                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }

                ddlReportingTo.DataSource = UserManagement.GetAllByCustomerID(Convert.ToInt32(ddlCustomer.SelectedValue), roleCode);
                ddlReportingTo.DataBind();

                ddlReportingTo.Items.Insert(0, new ListItem("< Select Reporting to person >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Branch >";
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (customerID != -1)
                {
                    tvBranches.Nodes.Clear();
                    NameValueHierarchy branch = null;
                    var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    if (branchs.Count > 0)
                    {
                        branch = branchs[0];
                    }
                    tbxBranch.Text = "< Select Location >";
                    List<TreeNode> nodes = new List<TreeNode>();
                    BindBranchesHierarchy(null, branch, nodes);
                    foreach (TreeNode item in nodes)
                    {
                        tvBranches.Nodes.Add(item);
                    }
                    tvBranches.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindParameters(List<UserParameterValueInfo> userParameterValues = null)
        {
            try
            {
                if (userParameterValues == null)
                {
                    userParameterValues = new List<UserParameterValueInfo>();
                    var userParameters = UserManagement.GetAllUserParameters();
                    userParameters.ForEach(entry =>
                    {
                        userParameterValues.Add(new UserParameterValueInfo()
                        {
                            UserID = -1,
                            ParameterID = entry.ID,
                            ValueID = -1,
                            Name = entry.Name,
                            DataType = (DataType)entry.DataType,
                            Length = (int)entry.Length,
                            Value = string.Empty
                        });
                    });
                }

                repParameters.DataSource = userParameterValues;
                repParameters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<long> GetByProductID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                
                var productmapping = (from row in entities.ProductMappings
                                      where row.CustomerID == customerID && row.IsActive == false
                                      select (long)row.ProductID).ToList();

                return productmapping;
            }
        }
        public void EnableDisableRole(int CustomerID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ab = false;
                int customerID = -1;
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = CustomerID;
                }
                else
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                var Listofproduct = GetByProductID(customerID);
                if (Listofproduct.Count > 0)
                {

                    divComplianceRole.Visible = false;
                    ddlRole.Enabled = false;

                    divAuditRole.Visible = false;
                    ddlAuditRole.Enabled = false;

                    divSECRole.Visible = false;
                    ddlSECRole.Enabled = false;

                    divHRRole.Visible = false;
                    ddlHRRole.Enabled = false;

                    if (Listofproduct.Contains(1))
                    {
                        divComplianceRole.Visible = true;
                        ddlRole.Enabled = true;                      
                    }
                    if (Listofproduct.Contains(8))
                    {
                      
                        divSECRole.Visible = true;
                        ddlSECRole.Enabled = true;
                    }
                    if (Listofproduct.Contains(9))
                    {
                        
                        divHRRole.Visible = true;
                        ddlHRRole.Enabled = true;
                    }
                    if ((Listofproduct.Contains(3) || Listofproduct.Contains(4)))
                    {
                      
                        divAuditRole.Visible = true;
                        ddlAuditRole.Enabled = true;
                        
                    }
                }
            }
        }
        public void AddNewUser(int CustomerID)
        {

            try
            {
                ViewState["Mode"] = 0;
                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;                
                tbxEmail.Enabled = true;
                ddlCustomer.Enabled = true;
                ddlDepartment.ClearSelection();

                ddlRole.ClearSelection();
                ddlSECRole.ClearSelection();
                ddlAuditRole.ClearSelection();
                ddlHRRole.ClearSelection();

                ddlRole.SelectedValue = "-1";
                ddlCustomer.SelectedValue = "-1";
                ddlRole_SelectedIndexChanged(null, null);                
                chkHead.Checked = false;
                txtEndDate.Text = "";
                txtStartDate.Text = "";
                txtperiodStartDate.Text = "";
                txtperiodEndDate.Text = "";
                BindParameters();                                
                EnableDisableRole(CustomerID);
                BindDepartment(CustomerID);                
                try
                {
                    if (rptDepartment.Items.Count > 0)
                    {
                        foreach (RepeaterItem aItem in rptDepartment.Items)
                        {
                            CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                            chkDepartment.Checked = false;
                            CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                            DepartmentSelectAll.Checked = false;
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                upUsers.Update();
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        public void AddNewUser()
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;                
                tbxEmail.Enabled = true;
                ddlCustomer.Enabled = true;
                ddlDepartment.ClearSelection();
                ddlRole.ClearSelection();
                ddlSECRole.ClearSelection();
                ddlAuditRole.ClearSelection();
                ddlHRRole.ClearSelection();
                ddlRole.SelectedValue = "-1";
                ddlCustomer.SelectedValue = "-1";
                ddlRole_SelectedIndexChanged(null, null);               
                BindParameters();
               
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;                    
                    EnableDisableRole(customerID);
                }
                try
                {
                    if (rptDepartment.Items.Count > 0)
                    {
                        foreach (RepeaterItem aItem in rptDepartment.Items)
                        {
                            CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                            chkDepartment.Checked = false;
                            CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");
                            DepartmentSelectAll.Checked = false;
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }

                upUsers.Update();
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
      
        public void EditUserInformation(int userID)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "initializeDatePickerFunctionEndDate", string.Format("initializeDatePickerFunctionEndDate(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "initializeDatePickerFunctionEndDate1", string.Format("initializeDatePickerFunctionEndDate1(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "initializeDatePickerFunctionEndDate2", string.Format("initializeDatePickerFunctionEndDate2(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "initializeDatePickerFunctionEndDate3", string.Format("initializeDatePickerFunctionEndDate3(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox(0);", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "disableCombobox", "disableCombobox();", true);
                ViewState["Mode"] = 1;
                ViewState["UserID"] = userID;
                User user = UserManagement.GetByID(userID);
                List<UserParameterValueInfo> userParameterValues = UserManagement.GetParameterValuesByUserID(userID);

                tbxFirstName.Text = user.FirstName;
                tbxLastName.Text = user.LastName;
                tbxDesignation.Text = user.Designation;
                tbxEmail.Text = user.Email;
                tbxContactNo.Text = user.ContactNumber;
                tbxAddress.Text = user.Address;                
                if (user.RoleID == 9)
                {
                    txtStartDate.Text = Convert.ToString(Convert.ToDateTime(user.Startdate).ToString("dd-MM-yyyy"));
                    txtEndDate.Text = Convert.ToString(Convert.ToDateTime(user.Enddate).ToString("dd-MM-yyyy"));
                    txtperiodStartDate.Text = Convert.ToString(Convert.ToDateTime(user.AuditStartPeriod).ToString("dd-MM-yyyy"));
                    txtperiodEndDate.Text = Convert.ToString(Convert.ToDateTime(user.AuditEndPeriod).ToString("dd-MM-yyyy"));
                }
                var Productdetails = GetByProductID(Convert.ToInt32(user.CustomerID));
                #region Department
                BindDepartment((int)user.CustomerID);
                var vGetIndustryMappedIDs = Business.DepartmentHeadManagement.GetDepartmentData(userID);

                foreach (RepeaterItem aItem in rptDepartment.Items)
                {
                    CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                    chkDepartment.Checked = false;
                    CheckBox DepartmentSelectAll = (CheckBox)rptDepartment.Controls[0].Controls[0].FindControl("DepartmentSelectAll");

                    for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                    {
                        if (((Label)aItem.FindControl("lblDeptID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                        {
                            chkDepartment.Checked = true;
                        }
                    }
                    if ((rptDepartment.Items.Count) == (vGetIndustryMappedIDs.Count))
                    {
                        DepartmentSelectAll.Checked = true;
                    }
                    else
                    {
                        DepartmentSelectAll.Checked = false;
                    }
                }
                #endregion

               
                if (Productdetails.Contains(3))
                {
                    divAuditRole.Visible = true;
                }
                else if (Productdetails.Contains(4))
                {
                    divAuditRole.Visible = true;
                }
                else
                {
                    divAuditRole.Visible = false;
                }

                if (Productdetails.Contains(8))
                {
                    divSECRole.Visible = true;
                }
                else
                {
                    divSECRole.Visible = false;
                }

                if (Productdetails.Contains(9))
                {
                    divHRRole.Visible = true;
                }
                else
                {
                    divHRRole.Visible = false;
                }
                if (Productdetails.Contains(1))
                {
                    divComplianceRole.Visible = true;
                }
                else
                {
                    divComplianceRole.Visible = false;
                }
                ddlRole.SelectedValue = user.RoleID != null ? user.RoleID.ToString() : "-1";
                ddlRole_SelectedIndexChanged(null, null);

                ddlHRRole.SelectedValue = user.HRRoleID != null ? user.HRRoleID.ToString() : "-1";


                ddlSECRole.SelectedValue = user.SecretarialRoleID != null ? user.SecretarialRoleID.ToString() : "-1";

                if (user.IsHead == true)
                    chkHead.Checked = true;
                else
                    chkHead.Checked = false;
                
                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                if (mstuser.IsAuditHeadOrMgr == "AM")
                {
                    ddlAuditRole.SelectedValue = "2";

                }
                else if (mstuser.RoleID == 28)
                {
                    ddlAuditRole.SelectedValue = "4";
                }
                else if (mstuser.RoleID == 7)
                {
                    ddlAuditRole.SelectedValue = "1";
                }
                else
                {
                    ddlAuditRole.SelectedValue = "-1";
                }
                ddlAuditRole_SelectedIndexChanged(null, null);
                if (divCustomer.Visible && user.CustomerID.HasValue)
                {
                    ddlCustomer.SelectedValue = user.CustomerID.Value.ToString();
                    ddlCustomer_SelectedIndexChanged(null, null);
                }
                ddlReportingTo.SelectedValue = user.ReportingToID != null ? user.ReportingToID.ToString() : "-1";
                if (divCustomerBranch.Visible && user.CustomerBranchID.HasValue)
                {
                    Queue<TreeNode> queue = new Queue<TreeNode>();
                    foreach (TreeNode node in tvBranches.Nodes)
                    {
                        queue.Enqueue(node);
                    }
                    while (queue.Count > 0)
                    {
                        TreeNode node = queue.Dequeue();
                        if (node.Value == user.CustomerBranchID.Value.ToString())
                        {
                            node.Selected = true;
                            break;
                        }
                        else
                        {
                            foreach (TreeNode child in node.ChildNodes)
                            {
                                queue.Enqueue(child);
                            }
                        }
                    }
                    tvBranches_SelectedNodeChanged(null, null);
                }

                if (user.ImagePath != null)


                    BindParameters(userParameterValues);
                upUsers.Update();
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open')", true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewEdit", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public bool GetServiseProviderId(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ServiceproviderId = false;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select (bool)cust.IsDistributor).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {                
                int customerID = -1;
                customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                int getproductCOMPLIANCE = -1;
                int getproductAudit = -1;
                int getproductHR = -1;
                int getproductSec = -1;
                string PrimaryRoleAudit = "";

                //bool valid = false;
                //int? ParentID = ICIAManagement.GetParentIDforCustomer(customerID);
                //valid = ICIAManagement.CheckServiceProviderUserLimit(Convert.ToInt32(ParentID), customerID);
                //if (valid)
                //{
                    var Productdetails = GetByProductID(Convert.ToInt32(customerID));
                    string Isauditheadormgr = null;

                    if (Productdetails.Contains(1))
                    {
                        getproductCOMPLIANCE = Convert.ToInt32(ddlRole.SelectedValue);
                    }
                    if (Productdetails.Contains(3) || Productdetails.Contains(4))
                    {

                        if (ddlAuditRole.SelectedItem.Text == "Manager")
                        {
                            Isauditheadormgr = "AM";
                            getproductAudit = 7;
                            PrimaryRoleAudit = "Manager";
                        }
                        else if (ddlAuditRole.SelectedItem.Text == "Partner")
                        {
                            getproductAudit = 2;
                            Isauditheadormgr = "AH";
                            PrimaryRoleAudit = "Partner";
                        }
                        else if (ddlAuditRole.SelectedItem.Text == "Executive")
                        {
                            getproductAudit = 7;
                            Isauditheadormgr = null;
                            PrimaryRoleAudit = "Executive";
                        }
                        else
                        {
                            getproductAudit = -1;
                            //PrimaryRoleAudit = "Executive";
                        }
                    }
                    else
                    {
                        getproductAudit = -1;
                        //PrimaryRoleAudit = "Executive";
                    }
                    if (Productdetails.Contains(8))
                    {
                        getproductSec = Convert.ToInt32(ddlSECRole.SelectedValue);
                    }
                    if (Productdetails.Contains(9))
                    {
                        getproductHR = Convert.ToInt32(ddlHRRole.SelectedValue);
                    }
                    bool val = true;
                    if (getproductSec == -1 && getproductCOMPLIANCE == -1 && getproductAudit == -1 && getproductHR == -1)
                    {
                        val = false;
                    }
                    if (val)
                    {

                        if (getproductCOMPLIANCE == -1)
                        {
                            getproductCOMPLIANCE = -1;
                        }

                        if (getproductAudit == -1)
                        {
                            getproductAudit = -1;
                        }
                        #region limit
                        #region Compliance User
                        User user = new User()
                        {
                            FirstName = tbxFirstName.Text,
                            LastName = tbxLastName.Text,
                            Designation = tbxDesignation.Text,
                            Email = tbxEmail.Text,
                            ContactNumber = tbxContactNo.Text,
                            Address = tbxAddress.Text,
                            RoleID = getproductCOMPLIANCE,
                            IsExternal = false,
                            IsAuditHeadOrMgr = Isauditheadormgr,
                        };

                        if (getproductSec != -1)
                        {
                            user.SecretarialRoleID = getproductSec;
                        }
                        else
                        {
                            user.SecretarialRoleID = null;
                        }

                        user.HRRoleID = getproductHR;

                        if (divCustomer.Visible)
                        {
                            user.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }

                        if (divCustomerBranch.Visible)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(tvBranches.SelectedNode)))
                            {
                                user.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                            }
                        }

                        if (ddlReportingTo.SelectedValue != "-1" && ddlReportingTo.SelectedValue != "")
                        {
                            user.ReportingToID = Convert.ToInt64(ddlReportingTo.SelectedValue);
                        }

                        if (ddlRole.SelectedValue == "9" && Auditor1.Visible)
                        {
                            try
                            {
                                user.Startdate = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                user.Enddate = DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            catch { }
                        }

                        if (ddlRole.SelectedValue == "9" && Auditor2.Visible)
                        {
                            try
                            {
                                user.AuditStartPeriod = DateTime.ParseExact(txtperiodStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                user.AuditEndPeriod = DateTime.ParseExact(txtperiodEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            }
                            catch { }
                        }
                        List<UserParameterValue> parameters = new List<UserParameterValue>();
                        foreach (var item in repParameters.Items)
                        {
                            RepeaterItem entry = item as RepeaterItem;
                            TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                            HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                            HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                            parameters.Add(new UserParameterValue()
                            {
                                ID = Convert.ToInt32(hdnID.Value),
                                UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                                Value = tbxValue.Text
                            });
                        }




                        #endregion

                        #region Risk User

                        com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                        {
                            FirstName = tbxFirstName.Text,
                            LastName = tbxLastName.Text,
                            Designation = tbxDesignation.Text,
                            Email = tbxEmail.Text,
                            ContactNumber = tbxContactNo.Text,
                            Address = tbxAddress.Text,
                            RoleID = getproductAudit,
                            IsExternal = false,
                            IsAuditHeadOrMgr = Isauditheadormgr,
                        };

                        if (getproductSec != -1)
                        {
                            mstUser.SecretarialRoleID = getproductSec;
                        }
                        else
                        {
                            mstUser.SecretarialRoleID = null;
                        }

                        mstUser.HRRoleID = getproductHR;
                        if (ddlRole.SelectedValue == "9" && Auditor1.Visible)
                        {
                            try
                            {
                                mstUser.Startdate = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                mstUser.Enddate = DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            catch { }
                        }

                        if (ddlRole.SelectedValue == "9" && Auditor2.Visible)
                        {
                            try
                            {
                                mstUser.AuditStartPeriod = DateTime.ParseExact(txtperiodStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                mstUser.AuditEndPeriod = DateTime.ParseExact(txtperiodEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            catch { }
                        }
                        if (chkHead.Checked)
                        {
                            mstUser.IsHead = true;
                            user.IsHead = true;
                        }
                        else
                        {
                            mstUser.IsHead = false;
                            user.IsHead = false;
                        }

                        if (divCustomer.Visible)
                        {
                            mstUser.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                        if (divCustomerBranch.Visible)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(tvBranches.SelectedNode)))
                            {
                                mstUser.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                            }
                        }

                        List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk> parametersRisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>();
                        foreach (var item in repParameters.Items)
                        {
                            RepeaterItem entry = item as RepeaterItem;
                            TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                            HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                            HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                            parametersRisk.Add(new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk()
                            {
                                ID = Convert.ToInt32(hdnID.Value),
                                UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                                Value = tbxValue.Text
                            });
                        }
                        #endregion

                        if (getproductCOMPLIANCE == 2)
                        {

                            user.VendorRoleID = 2;
                            mstUser.VendorRoleID = 2;

                            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                            if (ProductMappingDetails.Contains(2))
                            {
                                if (user.LitigationRoleID == null)
                                {
                                    user.LitigationRoleID = 2;
                                    mstUser.LitigationRoleID = 2;
                                }
                            }
                            if (ProductMappingDetails.Contains(5))
                            {
                                if (user.ContractRoleID == null)
                                {
                                    user.ContractRoleID = 2;
                                    mstUser.ContractRoleID = 2;
                                }
                            }
                            if (ProductMappingDetails.Contains(6))
                            {
                                if (user.LicenseRoleID == null)
                                {
                                    user.LicenseRoleID = 2;
                                    mstUser.LicenseRoleID = 2;
                                }
                            }
                        }
                        if ((int)ViewState["Mode"] == 1)
                        {
                            user.ID = Convert.ToInt32(ViewState["UserID"]);
                            mstUser.ID = Convert.ToInt32(ViewState["UserID"]);
                        }

                        bool emailExists;
                        UserManagement.Exists(user, out emailExists);
                        if (emailExists)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "User with Same Email Already Exists.";
                            return;
                        }
                        UserManagementRisk.Exists(mstUser, out emailExists);
                        if (emailExists)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "User with Same Email Already Exists.";
                            return;
                        }
                        bool result = false;
                        int resultValue = 0;

                        long cid = -1;
                        if (divCustomer.Visible)
                        {
                            cid = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                        else
                        {
                            cid = AuthenticationHelper.UserID;
                        }

                        user.PrimaryRoleAudit = PrimaryRoleAudit;
                        mstUser.PrimaryRoleAudit = PrimaryRoleAudit;

                        if ((int)ViewState["Mode"] == 0)
                        {

                            user.CreatedBy = AuthenticationHelper.UserID;
                            user.CreatedByText = AuthenticationHelper.User;
                            string passwordText = Util.CreateRandomPassword(10);
                            user.Password = Util.CalculateAESHash(passwordText);
                            string message = SendNotificationEmail(user, passwordText);

                            mstUser.CreatedBy = AuthenticationHelper.UserID;
                            mstUser.CreatedByText = AuthenticationHelper.User;
                            mstUser.Password = Util.CalculateAESHash(passwordText);
                            resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                            if (resultValue > 0)
                            {

                                result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                                if (result == false)
                                {
                                    UserManagement.deleteUser(resultValue);
                                }
                                #region Department
                                List<DepartmentMapping> objDepartmentMapping = new List<DepartmentMapping>();
                                List<int> DeptIds = new List<int>();
                                foreach (RepeaterItem aItem in rptDepartment.Items)
                                {
                                    CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                                    if (chkDepartment.Checked)
                                    {
                                        DeptIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblDeptID")).Text.Trim()));


                                        DepartmentMapping DeptMapping = new DepartmentMapping()
                                        {
                                            UserID = resultValue,
                                            DepartmentID = Convert.ToInt32(((Label)aItem.FindControl("lblDeptID")).Text.Trim()),
                                            IsActive = true,
                                            CustomerID = cid,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };
                                        objDepartmentMapping.Add(DeptMapping);
                                    }
                                }
                                if (objDepartmentMapping.Count > 0)
                                {
                                    Business.DepartmentHeadManagement.CreateDepartmentMapping(objDepartmentMapping);
                                }
                                #endregion



                                if (result == true)
                                {

                                    widget swid = new widget()
                                    {
                                        UserId = (Int32)user.ID,
                                        Performer = true,
                                        Reviewer = true,
                                        PerformerLocation = true,
                                        ReviewerLocation = true,
                                        DailyUpdate = true,
                                        NewsLetter = true,
                                        ComplianceSummary = true,
                                        FunctionSummary = true,
                                        RiskCriteria = true,
                                        EventOwner = true,
                                        PenaltySummary = true,
                                        TaskSummary = true,
                                        ReviewerTaskSummary = true,
                                        CustomWidget = false,
                                    };
                                    result = UserManagement.Create(swid);

                                    if (result == false)
                                    {
                                        UserManagement.deleteUser(resultValue);
                                        UserManagementRisk.deleteMstUser(resultValue);
                                    }
                                }
                            }
                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            User User = UserManagement.GetByID(Convert.ToInt32(user.ID));
                            if (tbxEmail.Text.Trim() != User.Email)
                            {
                                string message = SendNotificationEmailChanged(user);
                                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                            }
                            result = UserManagement.Update(user, parameters);
                            result = UserManagementRisk.Update(mstUser, parametersRisk);

                            #region Department
                            List<int> DeptIds = new List<int>();
                            List<DepartmentMapping> objDepartmentMapping = new List<DepartmentMapping>();
                            foreach (RepeaterItem aItem in rptDepartment.Items)
                            {
                                CheckBox chkDepartment = (CheckBox)aItem.FindControl("chkDepartment");
                                if (chkDepartment.Checked)
                                {
                                    DeptIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblDeptID")).Text.Trim()));

                                    DepartmentMapping DeptMapping = new DepartmentMapping()
                                    {
                                        UserID = user.ID,
                                        DepartmentID = Convert.ToInt32(((Label)aItem.FindControl("lblDeptID")).Text.Trim()),
                                        IsActive = true,
                                        CustomerID = cid,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };
                                    objDepartmentMapping.Add(DeptMapping);
                                }
                            }
                            if (objDepartmentMapping.Count > 0)
                            {
                                Business.DepartmentHeadManagement.DeselectAllDepartment(user.ID);
                                Business.DepartmentHeadManagement.CreateDepartmentMapping(objDepartmentMapping);
                            }
                            #endregion
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        }

                        if (result)
                        {
                            if (UserImageUpload.HasFile)
                            {
                                string fileName = user.ID + "-" + user.FirstName + " " + user.LastName + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                                UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);
                                string filepath = "~/UserPhotos/" + fileName;
                                UserManagement.UpdateUserPhoto(Convert.ToInt32(user.ID), filepath, fileName);
                                UserManagementRisk.UpdateUserPhoto(Convert.ToInt32(mstUser.ID), filepath, fileName);
                            }
                        }
                        ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "CloseDialog", "$(\"#divUsersDialog\").dialog('close')", true);
                        if (OnSaved != null)
                        {
                            OnSaved(this, null);
                        }
                        #endregion limit               
                    }
                    else
                    {
                        cvDuplicateEntry.ErrorMessage = "Please select atleast one role";
                        cvDuplicateEntry.IsValid = false;
                    }
                //}
                //else
                //{
                //    cvDuplicateEntry.IsValid = false;
                //    cvDuplicateEntry.ErrorMessage = "Facility to create more users is not available in the Free version. Kindly contact Avantis to activate the same.";
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    ;

                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        public event EventHandler OnSaved;

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvDept\").hide(\"blind\", null, 5, function () { });", true);
        }
    }
}