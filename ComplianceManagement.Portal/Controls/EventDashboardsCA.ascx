﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventDashboardsCA.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.EventDashboardsCA" %>
<script type="text/javascript">
    $(function () {
        $('#divOptionalCompliances').dialog({
            height: 550,
            width: 700,
            autoOpen: false,
            draggable: true,
            title: "Event Complianes Details",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });

    });

    $(function () {
        $('#divNotAssignedCompliances').dialog({
            height: 550,
            width: 700,
            autoOpen: false,
            draggable: true,
            title: "Not Assigned Complianes List",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });

    });

    function initializeDatePicker(date) {

        var startDate = new Date();
        $(".StartDate").datepicker({
            dateFormat: 'dd-mm-yy',
            setDate: startDate,
            numberOfMonths: 1
        });
    }

    function setDate() {
        $(".StartDate").datepicker();
    }

    function initializeJQueryUI(textBoxID, divID) {
        $("#" + textBoxID).unbind('click');

        $("#" + textBoxID).click(function () {
            $("#" + divID).toggle("blind", null, 500, function () { });
        });
    }
    function postbackOnCheck() {
        var o = window.event.srcElement;
        if (o.tagName == 'INPUT' && o.type == 'checkbox' && o.name != null && o.name.indexOf('CheckBox') > -1)
        { __doPostBack("", ""); }
    }

    function divexpandcollapse(divname) {
        var div = document.getElementById(divname);
        var img = document.getElementById('img' + divname);
        if (div.style.display == "none") {
            div.style.display = "inline";
            img.src = "../Images/minus.gif";
        } else {
            div.style.display = "none";
            img.src = "../Images/plus.gif";
        }
    }
    function divexpandcollapseChild(divname) {
        var div1 = document.getElementById(divname);
        var img = document.getElementById('img' + divname);
        if (div1.style.display == "none") {
            div1.style.display = "inline";
            img.src = "../Images/minus.gif";
        } else {
            div1.style.display = "none";
            img.src = "../Images/plus.gif";;
        }
    }
</script>
<script type="text/javascript">
    function DeleteItem() {
        if (confirm("Are you sure you want to delete ...?")) {
            return true;
        }
        return false;
    }
</script>

<style type="text/css">
    .btnss {
        background-image: url(../Images/edit_icon.png);
        border: 0px;
        width: 24px;
        height: 24px;
        background-color: transparent;
    }

    .btnview {
        background-image: url(../Images/package_icon.png);
        border: 0px;
        width: 24px;
        height: 24px;
        background-color: transparent;
    }
</style>

<asp:UpdateProgress ID="updateProgress" runat="server">
    <ProgressTemplate>
        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<div style="margin-top: 40px">
    <div style="margin-bottom: 4px">
        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
    </div>
    <div runat="server" id="divEventInstance">
        <asp:UpdatePanel ID="upEventInstanceList" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="width: 95%">
                            <asp:Label ID="Label2" Text="Assigned Events" runat="server" Font-Bold="true" Font-Size="Medium" ForeColor="#996633"></asp:Label>
                        </td>
                        <td style="width: 50px">
                            <asp:Button ID="btnBackMyEvent" CssClass="button" runat="server" Text="Back" OnClick="btnBackMyEvent_Click" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel3" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                    <asp:GridView runat="server" ID="grdEventList" AutoGenerateColumns="false" GridLines="Vertical" OnRowCancelingEdit="grdEventList_RowCancelingEdit"
                        OnRowEditing="grdEventList_RowEditing" OnRowUpdating="grdEventList_RowUpdating" OnRowCommand="grdEventList_RowCommand"
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdEventList_RowCreated"
                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdEventList_Sorting"
                        Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdEventList_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Height="22px" HeaderText="Location" SortExpression="CustomerBranchName">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                        <asp:Label ID="lblAssignmnetId" runat="server" Visible="false" Text='<%# Eval("EventAssignmentID") %>' ToolTip='<%# Eval("EventAssignmentID") %>'></asp:Label>
                                        <asp:Label ID="lblEventInstanceID" runat="server" Visible="false" Text='<%# Eval("EventInstanceID") %>' ToolTip='<%# Eval("EventInstanceID") %>'></asp:Label>
                                        <%--<asp:Label ID="lblEventScheduleOnID" runat="server" Visible="false" Text='<%# Eval("EventScheduledOnId") %>' ToolTip='<%# Eval("EventScheduledOnId") %>'></asp:Label>--%>
                                        <asp:Label ID="lblBranch" runat="server" Visible="false" Text='<%# Eval("CustomerBranchID") %>' ToolTip='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                        <asp:Label ID="lblBranchName" runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" ItemStyle-Height="18px" SortExpression="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbleventName" runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User" SortExpression="UserName" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserID" runat="server" Visible="false" Text='<%# Eval("UserID") %>' ToolTip='<%# Eval("UserID") %>'></asp:Label>
                                    <asp:Label ID="lblUser" runat="server" Text='<%# Eval("UserName") %>' ToolTip='<%# Eval("UserName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Role" SortExpression="Role" ItemStyle-Width="7%">
                                <ItemTemplate>
                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>' ToolTip='<%# Eval("Role") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nature of event" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txteventNature" Width="200px" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txt_startdate" Visible="false" CssClass="StartDate" runat="server" ReadOnly="true"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                  <%--  <asp:LinkButton ID="btn_Edit" runat="server" CommandName="Edit" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"><img src="../Images/edit_icon.png" alt="Edit Event" title ="Edit Event"/></asp:LinkButton>
                                    <asp:LinkButton ID="btn_View" runat="server" CommandName="View" CommandArgument="<%# ((GridViewRow) Container).RowIndex + "," + Eval("CustomerBranchID") %>"><img src="../Images/package_icon.png" alt="View Event" title ="View Detail"/></asp:LinkButton>--%>
                                    <asp:Button ID="btn_Edit" runat="server" ToolTip="Edit Event" CommandName="Edit"  CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" CssClass="btnss" />
                                    <asp:Button ID="btn_View" runat="server" Style="margin-left: 8px;" ToolTip="View Event" CommandName="View" CssClass="btnview" CommandArgument= '<%# ((GridViewRow) Container).RowIndex + "," + Eval("CustomerBranchID") %>'/>

                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btn_Update" runat="server" Text="Activate" CommandName="Update" />
                                    <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div runat="server" id="divOptionalCompliance">
        <div id="divOptionalCompliances">
            <asp:UpdatePanel ID="upOptionalCompliances" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin: 5px">

                        <asp:Panel ID="Panel2" Height="330px" ScrollBars="Auto" runat="server">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div style="margin-bottom: 7px;" runat="server" id="divSubEventmode">
                                            <div style="z-index: 10" id="divSubevent">
                                                <asp:GridView ID="gvParentGrid" runat="server" GridLines="None" AutoGenerateColumns="false"
                                                    ShowFooter="true" Width="630px" DataKeyNames="Type,SequenceID"
                                                    OnRowDataBound="gvParentGrid_OnRowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-Width="20px">
                                                            <ItemTemplate>
                                                                <a href="JavaScript:divexpandcollapse('div<%# Eval("EventType") %>');">
                                                                    <img id="imgdiv<%# Eval("EventType") %>" width="9px" border="0"
                                                                        src="../Images/plus.gif" alt="" /></a>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:BoundField ItemStyle-Width="150px" DataField="ParentEventID" HeaderText="ID" />
                                                        <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Parent Event Name" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td colspan="100%">
                                                                        <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                                            <asp:GridView ID="gvParentToComplianceGrid" GridLines="None" runat="server" Width="95%" DataKeyNames="EventType,SequenceID"
                                                                                AutoGenerateColumns="false">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                                                        <ItemTemplate>
                                                                                            <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/minus.gif"
                                                                                                    alt="" /></a>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField ItemStyle-Width="122px" DataField="ComplianceID" HeaderText="ID" />
                                                                                    <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Compliance Name" />
                                                                                    <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                </Columns>
                                                                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td colspan="100%">
                                                                        <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                                            <asp:GridView ID="gvChildGrid" runat="server" Width="95%" GridLines="None"
                                                                                AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                                                OnRowDataBound="gvChildGrid_OnRowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                                                        <ItemTemplate>
                                                                                            <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                    alt="" /></a>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField ItemStyle-Width="130px" DataField="SubEventID" HeaderText="ID" />
                                                                                    <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Sub Event Name" />
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <tr>
                                                                                                <td colspan="100%">
                                                                                                    <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                        <asp:GridView ID="gvComplianceGrid" GridLines="None" runat="server" Width="95%"
                                                                                                            AutoGenerateColumns="false" DataKeyNames="SequenceID">
                                                                                                            <Columns>
                                                                                                                <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" />
                                                                                                                <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" />
                                                                                                                <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                                            </Columns>
                                                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                            <%--  <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                        </asp:GridView>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                <%--<AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%-- Parent -> Intermediate Event -> Sub Event -> Compliance--%>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td colspan="100%">
                                                                        <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                                            <asp:GridView ID="gvIntermediateGrid" runat="server" Width="95%" GridLines="None"
                                                                                AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                                                OnRowDataBound="gvIntermediateGrid_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                                                        <ItemTemplate>
                                                                                            <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                    alt="" /></a>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField ItemStyle-Width="130px" DataField="IntermediateEventID" HeaderText="ID" />
                                                                                    <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Intermediate Event Name" />
                                                                                    <%--Sub Event--%>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <tr>
                                                                                                <td colspan="100%">
                                                                                                    <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                        <asp:GridView ID="gvIntermediateSubEventGrid" GridLines="None" OnRowDataBound="gvIntermediateSubEventGrid_RowDataBound" runat="server" Width="95%" DataKeyNames="ParentEventID"
                                                                                                            AutoGenerateColumns="false">
                                                                                                            <Columns>
                                                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                                                    <ItemTemplate>
                                                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                                                alt="" /></a>
                                                                                                                    </ItemTemplate>
                                                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:BoundField ItemStyle-Width="120px" DataField="SubEventID" HeaderText="ID" />
                                                                                                                <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Sub Event Name" />
                                                                                                                <asp:TemplateField>
                                                                                                                    <ItemTemplate>
                                                                                                                        <tr>
                                                                                                                            <td colspan="100%">
                                                                                                                                <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                                                    <asp:GridView ID="gvIntermediateComplainceGrid" GridLines="None" runat="server" Width="95%"
                                                                                                                                        AutoGenerateColumns="false">
                                                                                                                                        <Columns>
                                                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" />
                                                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" />
                                                                                                                                            <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                                                                        </Columns>
                                                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                                                        <%--   <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                                                    </asp:GridView>
                                                                                                                                </div>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                            </Columns>
                                                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                            <%--  <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                                                        </asp:GridView>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                <%--   <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <%-- <HeaderStyle BackColor="#0063A6" ForeColor="White" />--%>
                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="Panel1" Height="70px" ScrollBars="Auto" runat="server">
                            <table width="100%">
                                <tr>
                                    <td style="width: 400px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 390px; display: block; float: left; font-size: 13px; color: #333;">
                                            Kindly describe nature of event
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 400px;">
                                        <asp:TextBox ID="txtDescription" Width="632px" runat="server" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDescription" ErrorMessage="Please enter event description."
                                            ControlToValidate="txtDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <div style="margin-bottom: 7px; float: right; margin-right: 240px; margin-top: 10px; clear: both">
                            <asp:Button Text="Proceed" runat="server" ValidationGroup="ComplianceValidationGroup" ID="btnOptionalComplianceSave" OnClick="btnOptionalComplianceSave_Click"
                                CssClass="button" />
                            <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClick="btnClose_Click" />
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnTitle" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divNotAssignedCompliances">
            <asp:UpdatePanel ID="upNotAssignedCompliances" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="margin: 5px">
                        Please assign below compliance List and then update event held date.
                    </div>
                    <div style="margin: 5px">
                        <asp:Panel ID="Panel4" Height="380px" ScrollBars="Auto" runat="server">
                            <asp:GridView runat="server" ID="grdNoAsignedComplinace" AutoGenerateColumns="false" GridLines="Vertical"
                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="14" Width="100%"
                                Font-Size="12px">
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Height="22px" HeaderText="ID" SortExpression="ID">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name" ItemStyle-Height="22px" SortExpression="Name">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#CCCC99" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                <PagerSettings Position="Top" />
                                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                <AlternatingRowStyle BackColor="#E6EFF7" />
                                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </asp:Panel>
                        <div style="margin-bottom: 7px; float: right; margin-right: 240px; margin-top: 10px; clear: both">
                            <asp:Button Text="Close" runat="server" ID="btnDvNotAssignedClose" CssClass="button" OnClick="btnDvNotAssignedClose_Click" />
                        </div>
                    </div>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

