﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class DashboardPendingforApprovelsInternal : System.Web.UI.UserControl
    {
        public string Role;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["Role"] = Role;
                BindComplianceTransactions();
                udcStatusTranscation.OnSaved += (inputForm, args) => { BindComplianceTransactions(); };
            }
           
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                List<InternalComplianceInstanceTransactionView> assignmentList = null;
                if (ViewState["Role"].Equals("Performer"))
                {
                    assignmentList = InternalDashboardManagement.DashboardDataForPerformer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview);
                }
                else if (ViewState["Role"].Equals("Reviewer"))
                {
                    assignmentList = InternalDashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview);
                }
                else if (ViewState["Role"].Equals("Approver"))
                {
                    assignmentList = InternalDashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview);
                }

                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceTransactions.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                    }
                }

                grdComplianceTransactions.DataSource = assignmentList;
                grdComplianceTransactions.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    InternalComplianceInstanceTransactionView rowView = (InternalComplianceInstanceTransactionView)e.Row.DataItem;
                    if (rowView.InternalComplianceInstanceID != -1)
                    {
                        Label lblImpact = (Label)e.Row.FindControl("lblImpact");
                        Label lblRisk = (Label)e.Row.FindControl("lblRisk");
                        Label lblComplianceId = (Label)e.Row.FindControl("lblComplianceId");
                        Image imtemplat = (Image)e.Row.FindControl("imtemplat");

                        //if (!string.IsNullOrEmpty(lblImpact.Text))
                        //{
                        //    if (Convert.ToInt32(lblImpact.Text) == 0)
                        //    {
                        //        lblImpact.Text = "Monitory";
                        //    }
                        //    else if (Convert.ToInt32(lblImpact.Text) == 1)
                        //    {
                        //        lblImpact.Text = "Non-Monitory";
                        //    }
                        //    else if (Convert.ToInt32(lblImpact.Text) == 2)
                        //    {
                        //        lblImpact.Text = "Monitory & Non-Monitory";
                        //    }
                        //}
                        //else
                        //{
                        //    lblImpact.Text = "";
                        //}

                       // long Complianceid = Convert.ToInt64(lblComplianceId.Text);
                        //lblImpact.ToolTip = ComplianceManagement.Business.InternalComplianceManagement.GetPanalty(ComplianceManagement.Business.InternalComplianceManagement.GetByID(Complianceid));

                        if (Convert.ToInt32(lblRisk.Text) == 0)
                        {
                            imtemplat.ImageUrl = "~/Images/red.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 1)
                        {
                            imtemplat.ImageUrl = "~/Images/yellow.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 2)
                        {
                            imtemplat.ImageUrl = "~/Images/green.png";
                        }


                        LinkButton btnChangeStatus = (LinkButton)e.Row.FindControl("btnChangeStatus");
                        if (btnChangeStatus.Visible == true)
                        {
                            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdComplianceTransactions, "CHANGE_STATUS$" + btnChangeStatus.CommandArgument);
                            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");
                            e.Row.ToolTip = "Click on row to change Compliance Status";

                        }
                    }
                    else
                    {

                        e.Row.Cells[0].Style.Add("border-style", "none");
                        e.Row.Cells[0].Style.Add("font-weight", "700");
                        e.Row.Cells[1].Style.Add("border-style", "none");
                        e.Row.Cells[2].Style.Add("border-style", "none");
                        e.Row.Cells[3].Style.Add("border-style", "none");
                        e.Row.Cells[3].Text = "";
                        e.Row.Cells[6].Text = "";
                        e.Row.Cells[4].Style.Add("border-style", "none");
                        e.Row.Cells[5].Style.Add("border-style", "none");
                        e.Row.Style.Add("background-color", "#9FCBEA");
                        e.Row.Cells[0].Style.Add("Height", "20px");
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandName.Equals("Sort")))
                {
                   
                    if (e.CommandName.Equals("CHANGE_STATUS"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                        int complianceInstanceID = Convert.ToInt32(commandArgs[1]);

                        udcStatusTranscation.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                        
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        /// <summary>
        /// this function bind approvalTopending Compliances for grid.
        /// </summary>
        protected void BindComplianceTransactions()
        {
            try
            {
                if (ViewState["Role"] != null)
                {
                    if (ViewState["Role"].Equals("Performer"))
                    {
                        grdComplianceTransactions.Columns[5].Visible = false;
                       // grdComplianceTransactions.Columns[8].Visible = false; //comment by rahul on 14 Dec 2015
                        grdComplianceTransactions.Columns[6].Visible = false; //Change by Rahul on 14 Dec 2015
                        grdComplianceTransactions.DataSource = InternalDashboardManagement.DashboardDataForPerformer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview);
                    }
                    else if (ViewState["Role"].Equals("Reviewer"))
                    {
                        grdComplianceTransactions.Columns[5].Visible = false;
                        //grdComplianceTransactions.Columns[8].Visible = false;  //comment by rahul on 14 Dec 2015
                        grdComplianceTransactions.Columns[6].Visible = false; //Change by Rahul on 14 Dec 2015
                        grdComplianceTransactions.DataSource = InternalDashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview);
                    }
                    else if (ViewState["Role"].Equals("Approver"))
                    {
                        grdComplianceTransactions.Columns[5].Visible = true;
                        grdComplianceTransactions.DataSource = InternalDashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview);
                    }
                    grdComplianceTransactions.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is set the sorting image to grid view.
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="headerRow"></param>
        private void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        /// <summary>
        /// this function is return the string values for compliance type.
        /// </summary>
        /// <param name="userID">long</param>
        protected string ShowType(long? eventID)
        {
            try
            {
                if (eventID == null)
                {
                    return "Non-Event Based";
                }
                else
                {
                    return "Event Based";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }



    }
}