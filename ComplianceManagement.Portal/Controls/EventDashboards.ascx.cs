﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Configuration;
using System.Threading;
using System.Text;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class EventDashboards : System.Web.UI.UserControl
    {
        static int Type = 0;
        static int PageCount = 0;
        static DataTable table = new DataTable();
        static List<Tuple<long, long, int, DateTime, int>> EventScheDuleOnIDList = new List<Tuple<long, long, int, DateTime, int>>();
        static Boolean IsFromSingle = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "CustomerBranchName";

                divOptionalCompliance.Visible = false;
                if (Page.RouteData.Values["filter"] != null)
                {
                    if (Page.RouteData.Values["filter"].ToString().Equals("AssignedEvents"))
                    {
                        if (Page.RouteData.Values["Type"].ToString().Equals("AssignedEvents"))
                        {
                            ViewState["Role"] = Page.RouteData.Values["Role"].ToString();
                        }
                    }
                }
                BindLocationFilter();
                BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));
                setDateToGridView();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocationAssigned');", tbxFilterLocationAssigned.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocationAssigned\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeConfirmDatePickerActivation", "initializeConfirmDatePickerActivation(null);", true);
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", "initializeDatePicker11(null);", true);
            }
        }

        private void BindEventData(int EventClassificationID)
        {
            try
            {
                int EventRoleID = 10;
                int location = -1;
                if (!string.IsNullOrEmpty(tvFilterLocationAssigned.SelectedValue))
                {
                    if (Convert.ToInt32(tvFilterLocationAssigned.SelectedValue) != -1)
                    {
                        location = Convert.ToInt32(tvFilterLocationAssigned.SelectedValue);
                    }
                }
                PageCount = BindEventInstances(EventClassificationID, EventRoleID, location);
                if (SelectedPageNo.Text == "")
                {
                    SelectedPageNo.Text = "1";
                }
                TotalRows.Value = PageCount.ToString();
                Session["TotalRows"] = PageCount;
                GetPageDisplaySummary();
                upEventInstanceListAssigned.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                tvFilterLocationAssigned.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);

                var LocationList = CustomerBranchManagement.GetAssignedEventLocationList(AuthenticationHelper.UserID, customerID);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocationAssigned.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocationAssigned.Nodes.Add(node);
                }

                tvFilterLocationAssigned.CollapseAll();
                divFilterLocationAssigned.Style.Add("display", "none");

                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue)); setDateToGridView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public int BindEventInstances(int EventClassificationID, int eventRoleID, int location, int pageIndex = 0, bool isBranchChanged = false)
        {
            try
            {
                try
                {
                    var dataSource = EventManagement.GetAllAssignedInstancesCount(EventClassificationID, eventRoleID, txtSearchType.Text, location, AuthenticationHelper.UserID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));

                    if (ViewState["SortOrder"].ToString() == "Asc")
                    {
                        if (ViewState["SortExpression"].ToString() == "CustomerBranchName")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.CustomerBranchName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Name")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.Name).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "UserName")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.UserName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.Role).ToList();
                        }
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        if (ViewState["SortExpression"].ToString() == "CustomerBranchName")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.CustomerBranchName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Name")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.Name).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "UserName")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.UserName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.Role).ToList();
                        }
                        direction = SortDirection.Ascending;
                    }
                    Session["TotalRows"] = dataSource.Count;

                    grdEventList.DataSource = dataSource;
                    grdEventList.DataBind();
                    upEventInstanceListAssigned.Update();
                    GetPageDisplaySummary();
                    return dataSource.Count();

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
                    return 0;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
                return 0;
            }
        }

        protected void grdEventList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void grdEventList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int EventClassificationID = Convert.ToInt32(ddlCheckListType.SelectedValue);
                var assignmentList = EventManagement.GetAllAssignedInstancesCount(EventClassificationID, 10, txtSearchType.Text, -1, AuthenticationHelper.UserID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdEventList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdEventList.Columns.IndexOf(field);
                    }
                }
                grdEventList.DataSource = assignmentList;
                grdEventList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));
            setDateToGridView();
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;
            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void grdEventList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int index = Convert.ToInt32(commandArgs[0]);
                    int CustomerBranchID = Convert.ToInt32(commandArgs[1]);

                    int eventId = Convert.ToInt32(grdEventList.DataKeys[index].Values[0]);
                    Session["eventId"] = eventId;
                    Session["CustomerBranchID"] = CustomerBranchID;
                    Session["eventType"] = "AssignedEvent";
                    Response.Redirect("~/Event/EventDetails.aspx", false);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindNotAssignedComplianceList()
        {
            try
            {
                table.Rows.Clear();
                List<long> EventList = new List<long>();
                foreach (GridViewRow rw in grdEventList.Rows)
                {
                    int rowIndex = rw.RowIndex;
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");

                    if (chkBx != null && chkBx.Checked)
                    {
                        string dt = DateTime.Now.ToString("dd-MM-yyyy");
                        string startdate = dt;

                        string eventnature = ((TextBox)rw.FindControl("txteventNatureGrid")).Text;  //Request[((TextBox)grdEventList.Rows[rw].FindControl("txteventNature")).UniqueID].ToString();
                        Session["startdate"] = null;
                        Session["startdate"] = startdate;

                        if (!string.IsNullOrEmpty(eventnature))
                        {
                            long branch = Convert.ToInt64(((Label)rw.FindControl("lblBranch")).Text);  //Convert.ToInt64((grdEventList.Rows[e.RowIndex].FindControl("lblBranch") as Label).Text);
                            Session["Rbranch"] = null;
                            Session["Rbranch"] = branch;
                            long eventId = (long)grdEventList.DataKeys[rowIndex].Value; //Convert.ToInt64(grdEventList.DataKeys[e.RowIndex].Value);
                            Session["eventId"] = null;
                            Session["eventId"] = eventId;

                            var exceptComplianceIDsList = EventManagement.CheckAllEventCompliance(Convert.ToInt32(eventId), Convert.ToInt32(branch));
                            Session["exceptComplianceIDs"] = exceptComplianceIDsList;
                            EventList.Add(Convert.ToInt32(Session["eventId"].ToString()));
                            int CustomerBranchId = Convert.ToInt32(Session["Rbranch"]);

                            if (exceptComplianceIDsList.Count > 0)
                            {
                                var data = EventManagement.GetAllNotAssignedComplinceListForEvent(1, exceptComplianceIDsList, EventList, CustomerBranchId, -1); // EventManagement.GetAllNoAssignedComplinceList(exceptComplianceIDs);
                                foreach (var item in data)
                                {
                                    DataRow dr = table.NewRow();
                                    dr["ID"] = item.ID;
                                    dr["EventID"] = item.EventID;
                                    dr["EvnetName"] = item.EvnetName;
                                    dr["ComplianceName"] = item.ComplianceName;
                                    dr["CustomerBranchName"] = item.CustomerBranchName;
                                    dr["CustomerBranchID"] = item.CustomerBranchID;

                                    table.Rows.Add(dr);
                                }
                            }
                        }
                    }
                }

                if (table.Rows.Count > 0)
                {
                    Session["EventList"] = EventList;
                    List<long> EventList1 = new List<long>();
                    EventList1 = (List<long>)Session["EventList"];
                    List<long> EvtList = EventList1.Select(i => (long)i).ToList();
                    gvComplianceListAssign.DataSource = null;
                    gvComplianceListAssign.DataBind();

                    gvComplianceListAssign.DataSource = table;
                    gvComplianceListAssign.DataBind();
                    upCompliance.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindSingleEventAssignedComplianceList(int EventID, int CustomerBranchID)
        {
            try
            {
                table.Rows.Clear();
                List<long> EventList = new List<long>();

                var exceptComplianceIDsList = EventManagement.CheckAllEventCompliance(Convert.ToInt32(EventID), Convert.ToInt32(CustomerBranchID));
                Session["exceptComplianceIDs"] = exceptComplianceIDsList;
                EventList.Add(Convert.ToInt32(Session["eventId"].ToString()));
                int CustomerBranchId = Convert.ToInt32(Session["Rbranch"]);

                if (exceptComplianceIDsList.Count > 0)
                {
                    var data = EventManagement.GetAllNotAssignedComplinceListForEvent(1, exceptComplianceIDsList, EventList, CustomerBranchId, -1); // EventManagement.GetAllNoAssignedComplinceList(exceptComplianceIDs);
                                                                                                                                                    //table.Clear();
                    foreach (var item in data)
                    {
                        DataRow dr = table.NewRow();
                        dr["ID"] = item.ID;
                        dr["EventID"] = item.EventID;
                        dr["EvnetName"] = item.EvnetName;
                        dr["ComplianceName"] = item.ComplianceName;
                        dr["CustomerBranchName"] = item.CustomerBranchName;
                        dr["CustomerBranchID"] = item.CustomerBranchID;

                        table.Rows.Add(dr);
                    }
                }
                if (table.Rows.Count > 0)
                {
                    Session["EventList"] = EventList;
                    List<long> EventList1 = new List<long>();
                    EventList1 = (List<long>)Session["EventList"];
                    List<long> EvtList = EventList1.Select(i => (long)i).ToList();

                    gvComplianceListAssign.DataSource = null;
                    gvComplianceListAssign.DataBind();

                    gvComplianceListAssign.DataSource = table;
                    gvComplianceListAssign.DataBind();
                    upCompliance.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void SaveComplianceList_Click(object sender, EventArgs e)
        {
            try
            {
                List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();

                foreach (GridViewRow item in gvComplianceListAssign.Rows)
                {
                    string ComplianceID = (item.FindControl("lblID") as Label).Text;

                    CheckBox chkAssign = (CheckBox)item.FindControl("chkCompliance");
                    if (chkAssign.Checked)
                    {
                        Label lblCustomerBranchID = (Label)item.FindControl("lblCustomerBranchID");

                        ComplianceInstance instance = new ComplianceInstance();
                        instance.ComplianceId = Convert.ToInt64(ComplianceID);
                        instance.CustomerBranchID = Convert.ToInt32(lblCustomerBranchID.Text);
                        instance.ScheduledOn = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        if (ddlFilterPerformer.SelectedValue.ToString() != "-1")
                        {
                            ComplianceAssignment assignment = new ComplianceAssignment();
                            assignment.UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue);
                            assignment.RoleID = RoleManagement.GetByCode("PERF").ID;
                            assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment));
                        }
                        if (ddlFilterReviewer.SelectedValue.ToString() != "-1")
                        {

                            ComplianceAssignment assignment1 = new ComplianceAssignment();
                            assignment1.UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue);
                            assignment1.RoleID = RoleManagement.GetByCode("RVW1").ID;
                            assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));
                        }

                        if (ddlFilterApprover.SelectedValue.ToString() != "-1")
                        {
                            ComplianceAssignment assignment2 = new ComplianceAssignment();
                            assignment2.UserID = Convert.ToInt32(ddlFilterApprover.SelectedValue);
                            assignment2.RoleID = RoleManagement.GetByCode("APPR").ID;
                            assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment2));
                        }
                    }
                }

                if (assignments.Count != 0)
                {
                    Business.ComplianceManagement.CreateInstancesEventBased(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User);

                    if (IsFromSingle == false)
                    {
                        BindNotAssignedComplianceList();
                    }
                    else
                    {
                        int BranchID = Convert.ToInt32(Session["BranchID"]);
                        int EventID = Convert.ToInt32(Session["EventID"]);

                        BindSingleEventAssignedComplianceList(BranchID, EventID);
                    }

                    ddlEvent.SelectedValue = "-1";
                    upCompliance.Update();

                    if (table.Rows.Count <= 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#AssignedComplianceList').modal('hide');", true);
                        upNotAssignedCompliances.Update();

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal1", "$('#NotAssignedComplianceList').modal('hide');", true);
                        upEventInstanceListAssigned.Update();

                    }
                }


                ddlFilterPerformer.SelectedValue = "-1";
                ddlFilterReviewer.SelectedValue = "-1";
                ddlFilterApprover.SelectedValue = "-1";

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void chkComplianceCheck_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)gvComplianceListAssign.HeaderRow.FindControl("chkComplianceCheck");
            foreach (GridViewRow row in gvComplianceListAssign.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkCompliance");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }
        }
        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: false);
                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindApproverUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);
                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnComplianceList_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "AssignedComplianceListModal();", true);

                BindUsers(ddlFilterPerformer);
                BindUsers(ddlFilterReviewer);
                BindApproverUsers(ddlFilterApprover);
                gvComplianceListAssign.DataSource = table; // EventManagement.GetAllNotAssignedComplinceListForEvent(lstComplaince, EventList, CustomerBranchID, -1);
                gvComplianceListAssign.DataBind();
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void SaveCheckedValues()
        {
            try
            {
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdNoAsignedComplinace_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdNoAsignedComplinace.PageIndex = e.NewPageIndex;
            List<Tuple<int, bool, bool>> eventList = new List<Tuple<int, bool, bool>>();
            SaveCheckedValues();
            eventList = ViewState["CHECKED_ITEMS"] as List<Tuple<int, bool, bool>>;

            List<long> EventList = new List<long>();
            for (int i = 0; i < eventList.Count; i++)
            {
                int EventID = eventList[i].Item1;
                int branchID = Convert.ToInt32(Session["Rbranch"]); // Convert.ToInt32(tvBranches.SelectedNode.Value);
                bool FlgCheck = false;
                FlgCheck = EventManagement.CheckEventAssigned(EventID, branchID);

                if (FlgCheck == true)
                {
                    continue;
                }
                EventList.Add(EventID);
            }

            int branchID1 = Convert.ToInt32(Session["Rbranch"]); // Convert.ToInt32(tvBranches.SelectedNode.Value);
            var exceptComplianceIDs = EventManagement.CheckAllEventComplianceNotAssigned(1, EventList, branchID1);
            grdNoAsignedComplinace.DataSource = EventManagement.GetAllNotAssignedComplinceListForEvent(1, exceptComplianceIDs, EventList, branchID1, -1);
            grdNoAsignedComplinace.DataBind();
            upNotAssignedCompliances.Update();
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", string.Format("initializeDatePicker11(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", "initializeDatePicker11(null);", true);
                }

                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(txtActivateDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeConfirmDatePickerActivation", string.Format("initializeConfirmDatePickerActivation(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeConfirmDatePickerActivation", "initializeConfirmDatePickerActivation(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rbtnShortnotice1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                if (rbtnShortnotice1.SelectedValue.Equals("1"))
                {
                    txtShortNoticeDays1.Text = "";
                    divDays.Visible = true;
                }
                else
                {
                    txtShortNoticeDays1.Text = "";
                    divDays.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {
            try
            {
                IsFromSingle = true;
                Boolean chkNatureActiveDateflag = false;

                string eventnature = Request[((TextBox)grdEventList.Rows[e.RowIndex].FindControl("txteventNatureGrid")).UniqueID].ToString();
                Session["eventnature"] = eventnature;
                var Date1 = Request[((TextBox)grdEventList.Rows[e.RowIndex].FindControl("txtActivateDateGrid")).UniqueID].ToString(); // ((TextBox)rw.FindControl("txtActivateDateGrid")).Text;
                Session["Date1"] = Date1;
                int EventClassificationID = Convert.ToInt32((grdEventList.Rows[e.RowIndex].FindControl("lblEventClassificationID") as Label).Text);
                Session["EventClassificationID"] = EventClassificationID;

                if (eventnature == "" || Date1 == "")
                {
                    chkNatureActiveDateflag = true;
                }
                if (chkNatureActiveDateflag == false)
                {
                    if (EventClassificationID == 1)
                    {
                        #region Secretrial
                        long branchID = Convert.ToInt64((grdEventList.Rows[e.RowIndex].FindControl("lblBranch") as Label).Text);

                        Session["BranchID"] = branchID;

                        Type = EventManagement.GetCompanyType(Convert.ToInt32(branchID));
                        long assignmnetID = Convert.ToInt64((grdEventList.Rows[e.RowIndex].FindControl("lblAssignmnetId") as Label).Text);
                        Session["assignmnetID"] = assignmnetID;

                        string branchName = Convert.ToString((grdEventList.Rows[e.RowIndex].FindControl("lblBranchName") as Label).Text);
                        Session["branchName"] = branchName;

                        string eventname = Convert.ToString((grdEventList.Rows[e.RowIndex].FindControl("lbleventName") as Label).Text);
                        int UserId = Convert.ToInt32((grdEventList.Rows[e.RowIndex].FindControl("lblUserID") as Label).Text);

                        Session["UserId"] = UserId;
                        long eventId = Convert.ToInt64(grdEventList.DataKeys[e.RowIndex].Value);
                        Session["EventID"] = eventId;

                        long EventInstanceID = Convert.ToInt64((grdEventList.Rows[e.RowIndex].FindControl("lblEventInstanceID") as Label).Text);
                        Session["EventInstanceID"] = EventInstanceID;

                        List<long> EventList = new List<long>();

                        #region Check Assignment

                        table = new DataTable();
                        table.Columns.Add("ID", typeof(long));
                        table.Columns.Add("EventID", typeof(long));
                        table.Columns.Add("EvnetName", typeof(string));
                        table.Columns.Add("ComplianceName", typeof(string));
                        table.Columns.Add("CustomerBranchName", typeof(string));
                        table.Columns.Add("CustomerBranchID", typeof(long));
                        string dt = DateTime.Now.ToString("dd-MM-yyyy");
                        string startdate = dt;
                        if (!string.IsNullOrEmpty(eventnature))
                        {
                            var exceptComplianceIDsList = EventManagement.CheckAllEventCompliance(Convert.ToInt32(eventId), Convert.ToInt32(branchID));
                            Session["exceptComplianceIDs"] = exceptComplianceIDsList;
                            EventList.Add(Convert.ToInt64(eventId));
                            int CustomerBranchId = Convert.ToInt32(branchID);

                            if (exceptComplianceIDsList.Count > 0)
                            {
                                var data = EventManagement.GetAllNotAssignedComplinceListForEvent(1, exceptComplianceIDsList, EventList, CustomerBranchId, -1);
                                foreach (var item in data)
                                {
                                    DataRow dr = table.NewRow();
                                    dr["ID"] = item.ID;
                                    dr["EventID"] = item.EventID;
                                    dr["EvnetName"] = item.EvnetName;
                                    dr["ComplianceName"] = item.ComplianceName;
                                    dr["CustomerBranchName"] = item.CustomerBranchName;
                                    dr["CustomerBranchID"] = item.CustomerBranchID;

                                    table.Rows.Add(dr);
                                }
                            }
                        }
                        #endregion

                        if (table.Rows.Count > 0)
                        {
                            Session["EventList"] = EventList;
                            List<long> EventList1 = new List<long>();
                            EventList1 = (List<long>)Session["EventList"];
                            List<long> EvtList = EventList1.Select(i => (long)i).ToList();

                            BindCheckdEvents(EvtList);

                            grdNoAsignedComplinace.DataSource = table;
                            grdNoAsignedComplinace.DataBind();
                            upNotAssignedCompliances.Update();
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NotAssignedComplianceListModal();", true);
                        }
                        else
                        {
                            Boolean FlagSingle = false;
                            Boolean FlgShortNoticeCheck = false;

                            #region Check Short Notice Compliance
                            BindParentEventData(Convert.ToInt32(eventId));

                            foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                            {
                                int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                                Session["ParentEventID"] = ParentEventID;
                                GridView gvEvenToCompliance = Eventrow.FindControl("gvParentToComplianceGrid") as GridView;
                                //ParentEvent -> compliance
                                foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                                {
                                    int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[1].Text);
                                    FlgShortNoticeCheck = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                                    if (FlgShortNoticeCheck == true)
                                    {
                                        FlgShortNoticeCheck = true;
                                        break;
                                    }
                                }
                                //ParentEvent -> SubEvent-> compliance
                                GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                                foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                                {
                                    Label lblIntermediateEventID = (Label)SubEventrow.FindControl("IntermediateEventID");
                                    int IntermediateEventID = Convert.ToInt32(lblIntermediateEventID.Text);
                                    if (lblIntermediateEventID.Text == "0")
                                    {
                                        int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                        GridView gvSubCompliance = SubEventrow.FindControl("gvComplianceGrid") as GridView;
                                        foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                                        {
                                            int ComplinaceID1 = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                                            FlgShortNoticeCheck = EventManagement.CheckShortnoticeCompliance(ComplinaceID1);
                                            if (FlgShortNoticeCheck == true)
                                            {
                                                FlgShortNoticeCheck = true;
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                    else
                                    {
                                        GridView gvIntermediateSubEvent = SubEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                        foreach (GridViewRow SubEventrow1 in gvIntermediateSubEvent.Rows)
                                        {
                                            int SubEventID = Convert.ToInt32(SubEventrow1.Cells[1].Text);
                                            GridView gvSubCompliance = SubEventrow1.FindControl("gvIntermediateComplainceGrid") as GridView;
                                            foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                                            {
                                                int ComplinaceID2 = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                                                FlgShortNoticeCheck = EventManagement.CheckShortnoticeCompliance(ComplinaceID2);
                                                if (FlgShortNoticeCheck == true)
                                                {
                                                    FlgShortNoticeCheck = true;
                                                    break;
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            #endregion

                            if (FlgShortNoticeCheck == true)
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalShortNotice1').modal();", true);
                                upOptionalCompliances.Update();
                            }
                            else
                            {
                                FlagSingle = false;
                                #region Save single Event Nature
                                DateTime ActivateDate = DateTime.ParseExact(Date1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                if (!string.IsNullOrEmpty(eventnature))
                                {
                                    #region Email for Assign compliance
                                    //var exceptComplianceIDs = EventManagement.CheckAllEventCompliance(eventId, branch);
                                    //if (exceptComplianceIDs.Count > 0)
                                    //{
                                    //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Associated compliances of this event are not assigned to location. Please assign compliances first and then update event held date.')", true);
                                    //    int customerID = -1;
                                    //    string ReplyEmailAddressName = "";
                                    //    if (AuthenticationHelper.Role.Equals("SADMN"))
                                    //        ReplyEmailAddressName = "Avantis";
                                    //    else
                                    //    {
                                    //        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                                    //        ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                                    //    }
                                    //    new Thread(() =>
                                    //    {
                                    //        User user = UserManagement.GetByID(UserId);
                                    //        List<User> emailsId = UserManagement.GetAdminUsers(CustomerBranchManagement.GetByID(branch).CustomerID);
                                    //        emailsId.Add(user);
                                    //        foreach (var us in emailsId)
                                    //        {
                                    //            string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                    //                             + "Dear " + us.FirstName + " " + us.LastName + ",<br /><br />"
                                    //                             + "Associated compliance of " + "'" + eventname + " have not been assigned to " + branchName + "'.<br> Please assign the compliance and update your event held on date.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                                    //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { us.Email }), null, null, "AVACOM Alert :: Missing compliances.", message);
                                    //        }
                                    //    }).Start();
                                    //}
                                    //else
                                    //{
                                    #endregion

                                    BindParentEventData(Convert.ToInt32(eventId));

                                    List<long> EventList1 = new List<long>();
                                    EventList1.Add(Convert.ToInt64(eventId));

                                    Session["EventList"] = EventList1;

                                    #region Save Event Nature
                                    if (startdate != null)
                                    {
                                        DateTime Date = DateTime.ParseExact(startdate.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                        DateTime NextDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            EventScheduleOn eventScheduleon = new EventScheduleOn();
                                            eventScheduleon.EventInstanceID = EventInstanceID;
                                            eventScheduleon.StartDate = Date;
                                            eventScheduleon.EndDate = Date;
                                            eventScheduleon.ClosedDate = Date;
                                            eventScheduleon.HeldOn = Date;
                                            eventScheduleon.Period = "0";
                                            eventScheduleon.CreatedBy = AuthenticationHelper.UserID;
                                            eventScheduleon.CreatedOn = DateTime.Now;
                                            eventScheduleon.Description = eventnature; // txtDescription.Text;
                                            eventScheduleon.IsDeleted = false;
                                            entities.EventScheduleOns.Add(eventScheduleon);
                                            entities.SaveChanges();

                                            long eventAssignmentID = Business.EventManagement.GetEventAssignmentID(Convert.ToInt64(eventScheduleon.EventInstanceID));

                                            Business.EventManagement.CreateEventReminders(Convert.ToInt64(eventId), EventInstanceID, Convert.ToInt64(eventAssignmentID), AuthenticationHelper.UserID, Convert.ToDateTime(Date));

                                            foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                                            {
                                                int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                                                //Parent
                                                EventAssignDate eventAssignDate = new EventAssignDate()
                                                {
                                                    ParentEventID = ParentEventID,
                                                    EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                    IntermediateEventID = 0,
                                                    SubEventID = 0,
                                                    Date = Date,
                                                    IsActive = true,
                                                    CreatedDate = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                    Type = Type,
                                                };
                                                Business.EventManagement.CreateEventAssignDate(eventAssignDate);

                                                // Subevent
                                                GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                                                foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                                                {
                                                    Label lblIntermediateEventID = (Label)SubEventrow.FindControl("IntermediateEventID");
                                                    int IntermediateEventID = Convert.ToInt32(lblIntermediateEventID.Text);
                                                    if (lblIntermediateEventID.Text == "0")
                                                    {
                                                        int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                                        EventAssignDate subeventAssignDate = new EventAssignDate()
                                                        {
                                                            ParentEventID = ParentEventID,
                                                            EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                            IntermediateEventID = 0,
                                                            SubEventID = SubEventID,
                                                            Date = NextDate,
                                                            IsActive = true,
                                                            CreatedDate = DateTime.Now,
                                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                            Type = Type,
                                                        };
                                                        Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                                    }
                                                    else
                                                    {
                                                        EventAssignDate IntermediatesubeventAssignDate = new EventAssignDate()
                                                        {
                                                            ParentEventID = ParentEventID,
                                                            EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                            IntermediateEventID = IntermediateEventID,
                                                            SubEventID = 0,
                                                            Date = NextDate,
                                                            IsActive = true,
                                                            CreatedDate = DateTime.Now,
                                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                            Type = Type,
                                                        };
                                                        Business.EventManagement.CreateEventAssignDate(IntermediatesubeventAssignDate);

                                                        GridView gvIntermediateSubEvent = SubEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                                        foreach (GridViewRow SubEventrow1 in gvIntermediateSubEvent.Rows)
                                                        {
                                                            int SubEventID = Convert.ToInt32(SubEventrow1.Cells[1].Text);
                                                            EventAssignDate subeventAssignDate = new EventAssignDate()
                                                            {
                                                                ParentEventID = ParentEventID,
                                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                                IntermediateEventID = IntermediateEventID,
                                                                SubEventID = SubEventID,
                                                                Date = NextDate,
                                                                IsActive = true,
                                                                CreatedDate = DateTime.Now,
                                                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                                Type = Type,
                                                            };
                                                            Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                                        }
                                                    }
                                                }
                                                if (FlagSingle == false)
                                                {
                                                    EventScheDuleOnIDList.Add(new Tuple<long, long, int, DateTime, int>(eventScheduleon.ID, ParentEventID, Type, ActivateDate, Convert.ToInt32(branchID)));
                                                    FlagSingle = true;
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                #endregion

                                #region Set first Date to Activated Event
                                if (EventScheDuleOnIDList.Count > 0)
                                {
                                    for (int i = 0; i < EventScheDuleOnIDList.Count; i++)
                                    {
                                        #region EventStructure : MainEvent -> SubEvent -> Compliance
                                        try
                                        {
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {
                                                var EventSchedueleData = EventManagement.GetActivatedEventData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));

                                                int Type = Convert.ToInt32(EventScheDuleOnIDList[i].Item3);

                                                int CusomerBranchID = Convert.ToInt32(EventScheDuleOnIDList[i].Item5);

                                                int ParentEventID = EventSchedueleData.ParentEventID; // Convert.ToInt32(childGrid.DataKeys[0].Value.ToString());

                                                int SubEventID = EventSchedueleData.SubEventID;

                                                int IntermediateEventID = EventSchedueleData.IntermediateEventID;

                                                int EventScheduledOnID = Convert.ToInt32(EventScheDuleOnIDList[i].Item1);
                                                DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);

                                                if (IntermediateEventID == 0)
                                                {
                                                    #region Subvent 
                                                    var SubEventToComplianceList = EventManagement.GetSubEventToCompliance(ParentEventID, SubEventID, EventScheduledOnID, Type, CusomerBranchID);
                                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                                    {
                                                        ParentEventID = ParentEventID,
                                                        EventScheduleOnID = EventScheduledOnID,
                                                        IntermediateEventID = 0,
                                                        SubEventID = SubEventID,
                                                        Date = ActiveDate,
                                                        IsActive = true,
                                                        CreatedDate = DateTime.Now,
                                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                        Type = Type,
                                                    };
                                                    Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                                                    for (int j = 0; j < SubEventToComplianceList.Count; j++)
                                                    {
                                                        int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[j].ComplianceID);   //Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);
                                                        int IsInternal = Convert.ToInt32(SubEventToComplianceList[j].IsInternal);
                                                        if (IsInternal == 0) //Statutory
                                                        {
                                                            Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                                            if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                            {
                                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);

                                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                                if (IsComplianceChecklistStatutory == true)
                                                                {
                                                                    //Change Generate flag Schedule change
                                                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                                }
                                                                string days = Convert.ToString(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                                Boolean FlgCheck = false;
                                                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                                if (FlgCheck == false)
                                                                {
                                                                    EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                                }
                                                            }
                                                        }
                                                        else if (IsInternal == 1) //Internal
                                                        {
                                                            int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);

                                                            int days = Convert.ToInt32(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                            Boolean FlgCheck = false;
                                                            FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, 0, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                            if (FlgCheck == false)
                                                            {
                                                                EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    //DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);
                                                    var IntermediateSubEventSchedueleData = EventManagement.GetActivatedIntermediateSubEventSchedueleData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), IntermediateEventID, Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));
                                                    int SubEventID1 = IntermediateSubEventSchedueleData.SubEventID;
                                                    var SubEventToComplianceList = EventManagement.GetIntermediateSubEventToCompliance(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, Type, CusomerBranchID);
                                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                                    {
                                                        ParentEventID = ParentEventID,
                                                        EventScheduleOnID = EventScheduledOnID,
                                                        IntermediateEventID = IntermediateEventID,
                                                        SubEventID = SubEventID1,
                                                        Date = ActiveDate,
                                                        IsActive = true,
                                                        CreatedDate = DateTime.Now,
                                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                        Type = Type,
                                                    };
                                                    Business.EventManagement.UpdateEventAssignDates(eventAssignDate);
                                                    for (int k = 0; k < SubEventToComplianceList.Count; k++)
                                                    {
                                                        int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[k].ComplianceID);

                                                        int IsIntermediateInternal = Convert.ToInt32(SubEventToComplianceList[k].IsIntermediateInternal);
                                                        if (IsIntermediateInternal == 0) //Statutory
                                                        {
                                                            Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                                            if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                            {
                                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);

                                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                                if (IsComplianceChecklistStatutory == true)
                                                                {
                                                                    //Change Generate flag Schedule change
                                                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                                }
                                                                string days = Convert.ToString(SubEventToComplianceList[k].Days);
                                                                Boolean FlgCheck = false;
                                                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                                if (FlgCheck == false)
                                                                {
                                                                    EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                                }
                                                            }
                                                        }
                                                        else if (IsIntermediateInternal == 1) //Internal
                                                        {
                                                            int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);

                                                            int days = Convert.ToInt32(SubEventToComplianceList[k].Days);
                                                            Boolean FlgCheck = false;
                                                            FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                            if (FlgCheck == false)
                                                            {
                                                                EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }
                                        #endregion
                                    }
                                }
                                #endregion

                                BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));
                                txtActivateDate.Text = "";
                                txtEventNature.Text = "";
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Event activated successfully')", true);
                            }
                        }
                        #endregion
                    }
                    else if (EventClassificationID == 2)
                    {
                        #region Non Secretrial
                        long branchID = Convert.ToInt64((grdEventList.Rows[e.RowIndex].FindControl("lblBranch") as Label).Text);

                        Session["BranchID"] = branchID;

                        Type = 4;

                        long assignmnetID = Convert.ToInt64((grdEventList.Rows[e.RowIndex].FindControl("lblAssignmnetId") as Label).Text);

                        Session["assignmnetID"] = assignmnetID;

                        string branchName = Convert.ToString((grdEventList.Rows[e.RowIndex].FindControl("lblBranchName") as Label).Text);

                        Session["branchName"] = branchName;

                        string eventname = Convert.ToString((grdEventList.Rows[e.RowIndex].FindControl("lbleventName") as Label).Text);

                        int UserId = Convert.ToInt32((grdEventList.Rows[e.RowIndex].FindControl("lblUserID") as Label).Text);

                        Session["UserId"] = UserId;

                        long eventId = Convert.ToInt64(grdEventList.DataKeys[e.RowIndex].Value);

                        Session["EventID"] = eventId;

                        long EventInstanceID = Convert.ToInt64((grdEventList.Rows[e.RowIndex].FindControl("lblEventInstanceID") as Label).Text);

                        Session["EventInstanceID"] = EventInstanceID;

                        List<long> EventList = new List<long>();

                        #region Check Assignment

                        table = new DataTable();
                        table.Columns.Add("ID", typeof(long));
                        table.Columns.Add("EventID", typeof(long));
                        table.Columns.Add("EvnetName", typeof(string));
                        table.Columns.Add("ComplianceName", typeof(string));
                        table.Columns.Add("CustomerBranchName", typeof(string));
                        table.Columns.Add("CustomerBranchID", typeof(long));

                        string dt = DateTime.Now.ToString("dd-MM-yyyy");
                        string startdate = dt;

                        if (!string.IsNullOrEmpty(eventnature))
                        {
                            var exceptComplianceIDsList = EventManagement.CheckAllEventComplianceNonSecretrial(Convert.ToInt32(eventId), Convert.ToInt32(branchID));
                            Session["exceptComplianceIDs"] = exceptComplianceIDsList;

                            EventList.Add(Convert.ToInt64(eventId));
                            int CustomerBranchId = Convert.ToInt32(branchID);

                            if (exceptComplianceIDsList.Count > 0)
                            {
                                var data = EventManagement.GetAllNotAssignedComplinceListForEvent(2, exceptComplianceIDsList, EventList, CustomerBranchId, -1);
                                foreach (var item in data)
                                {
                                    DataRow dr = table.NewRow();
                                    dr["ID"] = item.ID;
                                    dr["EventID"] = item.EventID;
                                    dr["EvnetName"] = item.EvnetName;
                                    dr["ComplianceName"] = item.ComplianceName;
                                    dr["CustomerBranchName"] = item.CustomerBranchName;
                                    dr["CustomerBranchID"] = item.CustomerBranchID;
                                    table.Rows.Add(dr);
                                }
                            }
                        }
                        #endregion

                        if (table.Rows.Count > 0)
                        {
                            Session["EventList"] = EventList;
                            List<long> EventList1 = new List<long>();
                            EventList1 = (List<long>)Session["EventList"];
                            List<long> EvtList = EventList1.Select(i => (long)i).ToList();

                            BindCheckdEvents(EvtList);

                            grdNoAsignedComplinace.DataSource = table;
                            grdNoAsignedComplinace.DataBind();
                            upNotAssignedCompliances.Update();
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NotAssignedComplianceListModal();", true);
                        }
                        else
                        {
                            Boolean FlagSingle = false;

                            FlagSingle = false;
                            #region Save single Event Nature
                            DateTime ActivateDate = DateTime.ParseExact(Date1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            if (!string.IsNullOrEmpty(eventnature))
                            {
                                #region Email for Assign compliance
                                //var exceptComplianceIDs = EventManagement.CheckAllEventCompliance(eventId, branch);
                                //if (exceptComplianceIDs.Count > 0)
                                //{
                                //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Associated compliances of this event are not assigned to location. Please assign compliances first and then update event held date.')", true);
                                //    int customerID = -1;
                                //    string ReplyEmailAddressName = "";
                                //    if (AuthenticationHelper.Role.Equals("SADMN"))
                                //        ReplyEmailAddressName = "Avantis";
                                //    else
                                //    {
                                //        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                                //        ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                                //    }
                                //    new Thread(() =>
                                //    {
                                //        User user = UserManagement.GetByID(UserId);
                                //        List<User> emailsId = UserManagement.GetAdminUsers(CustomerBranchManagement.GetByID(branch).CustomerID);
                                //        emailsId.Add(user);
                                //        foreach (var us in emailsId)
                                //        {
                                //            string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                //                             + "Dear " + us.FirstName + " " + us.LastName + ",<br /><br />"
                                //                             + "Associated compliance of " + "'" + eventname + " have not been assigned to " + branchName + "'.<br> Please assign the compliance and update your event held on date.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                                //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { us.Email }), null, null, "AVACOM Alert :: Missing compliances.", message);
                                //        }
                                //    }).Start();
                                //}
                                //else
                                //{
                                #endregion

                                BindParentEventData(Convert.ToInt32(eventId));

                                List<long> EventList1 = new List<long>();
                                EventList1.Add(Convert.ToInt64(eventId));

                                Session["EventList"] = EventList1;

                                #region Save Event Nature
                                if (startdate != null)
                                {
                                    DateTime Date = DateTime.ParseExact(startdate.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                    DateTime NextDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        EventScheduleOn eventScheduleon = new EventScheduleOn();
                                        eventScheduleon.EventInstanceID = EventInstanceID;
                                        eventScheduleon.StartDate = Date;
                                        eventScheduleon.EndDate = Date;
                                        eventScheduleon.ClosedDate = Date;
                                        eventScheduleon.HeldOn = Date;
                                        eventScheduleon.Period = "0";
                                        eventScheduleon.CreatedBy = AuthenticationHelper.UserID;
                                        eventScheduleon.CreatedOn = DateTime.Now;
                                        eventScheduleon.Description = eventnature;
                                        eventScheduleon.IsDeleted = false;
                                        entities.EventScheduleOns.Add(eventScheduleon);
                                        entities.SaveChanges();

                                        long eventAssignmentID = Business.EventManagement.GetEventAssignmentID(Convert.ToInt64(eventScheduleon.EventInstanceID));
                                        Business.EventManagement.CreateEventReminders(Convert.ToInt64(eventId), EventInstanceID, Convert.ToInt64(eventAssignmentID), AuthenticationHelper.UserID, Convert.ToDateTime(Date));

                                        foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                                        {
                                            int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                                            //Parent
                                            EventAssignDate eventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = 0,
                                                SubEventID = 0,
                                                Date = Date,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                Type = Type,
                                            };
                                            Business.EventManagement.CreateEventAssignDate(eventAssignDate);
                                            // Subevent
                                            GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                                            foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                                            {
                                                Label lblIntermediateEventID = (Label)SubEventrow.FindControl("IntermediateEventID");
                                                int IntermediateEventID = Convert.ToInt32(lblIntermediateEventID.Text);
                                                if (lblIntermediateEventID.Text == "0")
                                                {
                                                    int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                                    EventAssignDate subeventAssignDate = new EventAssignDate()
                                                    {
                                                        ParentEventID = ParentEventID,
                                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                        IntermediateEventID = 0,
                                                        SubEventID = SubEventID,
                                                        Date = NextDate,
                                                        IsActive = true,
                                                        CreatedDate = DateTime.Now,
                                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                        Type = Type,
                                                    };
                                                    Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                                }
                                                else
                                                {
                                                    EventAssignDate IntermediatesubeventAssignDate = new EventAssignDate()
                                                    {
                                                        ParentEventID = ParentEventID,
                                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                        IntermediateEventID = IntermediateEventID,
                                                        SubEventID = 0,
                                                        Date = NextDate,
                                                        IsActive = true,
                                                        CreatedDate = DateTime.Now,
                                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                        Type = Type,
                                                    };
                                                    Business.EventManagement.CreateEventAssignDate(IntermediatesubeventAssignDate);

                                                    GridView gvIntermediateSubEvent = SubEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                                    foreach (GridViewRow SubEventrow1 in gvIntermediateSubEvent.Rows)
                                                    {
                                                        int SubEventID = Convert.ToInt32(SubEventrow1.Cells[1].Text);
                                                        EventAssignDate subeventAssignDate = new EventAssignDate()
                                                        {
                                                            ParentEventID = ParentEventID,
                                                            EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                            IntermediateEventID = IntermediateEventID,
                                                            SubEventID = SubEventID,
                                                            Date = NextDate,
                                                            IsActive = true,
                                                            CreatedDate = DateTime.Now,
                                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                            Type = Type,
                                                        };
                                                        Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                                    }
                                                }
                                            }
                                            if (FlagSingle == false)
                                            {
                                                EventScheDuleOnIDList.Add(new Tuple<long, long, int, DateTime, int>(eventScheduleon.ID, ParentEventID, Type, ActivateDate, Convert.ToInt32(branchID)));
                                                FlagSingle = true;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion

                            #region Set first Date to Activated Event
                            if (EventScheDuleOnIDList.Count > 0)
                            {
                                for (int i = 0; i < EventScheDuleOnIDList.Count; i++)
                                {
                                    #region EventStructure : MainEvent -> SubEvent -> Compliance
                                    try
                                    {
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            var EventSchedueleData = EventManagement.GetActivatedEventData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));

                                            int Type = 4;

                                            int CusomerBranchID = Convert.ToInt32(EventScheDuleOnIDList[i].Item5);

                                            int ParentEventID = EventSchedueleData.ParentEventID; // Convert.ToInt32(childGrid.DataKeys[0].Value.ToString());

                                            int SubEventID = EventSchedueleData.SubEventID;

                                            int IntermediateEventID = EventSchedueleData.IntermediateEventID;

                                            int EventScheduledOnID = Convert.ToInt32(EventScheDuleOnIDList[i].Item1);
                                            if (IntermediateEventID == 0)
                                            {
                                                #region Subvent
                                                DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);  //childGrid.Rows[0].FindControl("txtgvChildGridDate") as TextBox;
                                                var SubEventToComplianceList = EventManagement.GetNonSecretrialSubEventToCompliance(ParentEventID, SubEventID, EventScheduledOnID, Type, CusomerBranchID);
                                                EventAssignDate eventAssignDate = new EventAssignDate()
                                                {
                                                    ParentEventID = ParentEventID,
                                                    EventScheduleOnID = EventScheduledOnID,
                                                    IntermediateEventID = 0,
                                                    SubEventID = SubEventID,
                                                    Date = ActiveDate,
                                                    IsActive = true,
                                                    CreatedDate = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                    Type = Type,
                                                };
                                                Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                                                for (int j = 0; j < SubEventToComplianceList.Count; j++)
                                                {
                                                    int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[j].ComplianceID);   //Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                                                    Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                                    if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                    {
                                                        int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                        Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);
                                                        if (IsComplianceChecklistStatutory == true)
                                                        {
                                                            //Change Generate flag Schedule change
                                                            EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                        }
                                                        string days = Convert.ToString(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                        Boolean FlgCheck = false;
                                                        FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                        if (FlgCheck == false)
                                                        {
                                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                #region Intermediate Event
                                                DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);
                                                var IntermediateSubEventSchedueleData = EventManagement.GetActivatedIntermediateSubEventSchedueleData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), IntermediateEventID, Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));
                                                int SubEventID1 = IntermediateSubEventSchedueleData.SubEventID;
                                                var SubEventToComplianceList = EventManagement.GetNonSecretrialIntermediateSubEventToCompliance(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, Type, CusomerBranchID);
                                                EventAssignDate eventAssignDate = new EventAssignDate()
                                                {
                                                    ParentEventID = ParentEventID,
                                                    EventScheduleOnID = EventScheduledOnID,
                                                    IntermediateEventID = IntermediateEventID,
                                                    SubEventID = SubEventID1,
                                                    Date = ActiveDate,
                                                    IsActive = true,
                                                    CreatedDate = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                    Type = Type,
                                                };
                                                Business.EventManagement.UpdateEventAssignDates(eventAssignDate);
                                                for (int k = 0; k < SubEventToComplianceList.Count; k++)
                                                {
                                                    int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[k].ComplianceID);

                                                    Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                                    if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                    {
                                                        int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                        Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);
                                                        if (IsComplianceChecklistStatutory == true)
                                                        {
                                                            //Change Generate flag Schedule change
                                                            EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                        }
                                                        string days = Convert.ToString(SubEventToComplianceList[k].Days);
                                                        Boolean FlgCheck = false;
                                                        FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                        if (FlgCheck == false)
                                                        {
                                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                    #endregion
                                }
                            }
                            #endregion

                            BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));
                            txtActivateDate.Text = "";
                            txtEventNature.Text = "";
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Event activated successfully')", true);
                        }
                        #endregion
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Selected event activation date or nature of event can not be empty.')", true);
                    setDateToGridView();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindParentEventData(int ParentEventID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var ParentEvent = entities.SP_GetParentEventSelected(ParentEventID, Type).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();
                upOptionalCompliances.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindParentEventDataSecretrial(int ParentEventID, int Type)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var ParentEvent = entities.SP_GetParentEventSelected(ParentEventID, Type).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();
                upOptionalCompliances.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private string getEventName(int EventID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var eventName = (from row in entities.Events
                                 where row.ID == EventID
                                 select row).SingleOrDefault();

                return eventName.Name;
                // UpdatePanel1.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return "";
        }
        protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int ParentEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                ComplianceDBEntities entities = new ComplianceDBEntities();
                GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");

                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                var compliance = entities.SP_GetParentEventToComplianceAssignDays(ParentEventID, Type).ToList();
                gv1.DataSource = compliance;
                gv1.DataBind();

                GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
                string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var SubEvent = entities.SP_GetEventData(ParentEventID, type, Type).ToList();
                gv.DataSource = SubEvent;
                gv.DataBind();
            }
        }

        protected void gvIntermediateSubEventGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateComplainceGrid");

                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int intermediateEventID = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    int parentEventID = Convert.ToInt32(Session["eventId"]);

                    var Compliance = entities.SP_GetIntermediateComplianceAssignDays(parentEventID, intermediateEventID, SubEventID, Type).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gvChildGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIntermediateEventID = (Label)e.Row.FindControl("IntermediateEventID");
                if (lblIntermediateEventID.Text == "0")
                {
                    try
                    {
                        GridView gv = (GridView)e.Row.FindControl("gvComplianceGrid");
                        int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                        GridView childGrid1 = (GridView)sender;
                        int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                        ComplianceDBEntities entities = new ComplianceDBEntities();
                        var Compliance = entities.SP_GetComplianceWithDays(Parentid, SubEventID, Type).ToList();
                        gv.DataSource = Compliance;
                        gv.DataBind();
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                else
                {
                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
                    int IntermediateEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    var Compliance = entities.SP_GetIntermediateSubEvent(IntermediateEventID, Parentid, Type).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
            }
        }

        protected bool visibleEdit(long roleID)
        {
            try
            {
                bool result = true;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnOptionalComplianceSave_Click(object sender, EventArgs e)
        {
            try
            {
                int EventClassificationID = Convert.ToInt32(Session["EventClassificationID"]);

                if (EventClassificationID == 1)
                {
                    #region Secretrial 
                    var exceptComplianceIDs = EventManagement.CheckAllEventCompliance(Convert.ToInt32(Session["eventId"].ToString()), Convert.ToInt32(Session["Rbranch"].ToString()));

                    if (exceptComplianceIDs.Count > 0)
                    {
                        grdNoAsignedComplinace.DataSource = EventManagement.GetAllNoAssignedComplinceList(exceptComplianceIDs);
                        grdNoAsignedComplinace.DataBind();
                        upNotAssignedCompliances.Update();
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divNotAssignedCompliances", "$(\"#divNotAssignedCompliances\").dialog('open')", true);
                    }
                    else
                    {
                        grdNoAsignedComplinace.DataSource = null;
                        grdNoAsignedComplinace.DataBind();
                        upOptionalCompliances.Update();

                        //foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                        //{
                        //    int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                        //    Session["ParentEventID"] = ParentEventID;
                        //    GridView gvEvenToCompliance = Eventrow.FindControl("gvParentToComplianceGrid") as GridView;
                        //    //ParentEvent -> compliance
                        //    foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                        //    {
                        //        int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[1].Text);
                        //    }

                        //    //ParentEvent -> SubEvent-> compliance
                        //    GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                        //    foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                        //    {
                        //        int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                        //        GridView gvSubCompliance = SubEventrow.FindControl("gvComplianceGrid") as GridView;
                        //        foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                        //        {
                        //            int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                        //        }
                        //    }

                        //    //Intermediatre-> Subevent-> Compliance
                        //    GridView gvIntermediateEvent = Eventrow.FindControl("gvIntermediateGrid") as GridView;
                        //    foreach (GridViewRow intermediateEventrow in gvIntermediateEvent.Rows)
                        //    {
                        //        int IntermediateEventID = Convert.ToInt32(intermediateEventrow.Cells[1].Text);
                        //        GridView gvIntermediateSubEvent = intermediateEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                        //        foreach (GridViewRow SubEventrow in gvIntermediateSubEvent.Rows)
                        //        {
                        //            int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                        //            GridView gvSubCompliance = SubEventrow.FindControl("gvIntermediateComplainceGrid") as GridView;
                        //            foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                        //            {
                        //                int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                        //            }
                        //        }
                        //    }
                        //}

                        if (Session["startdate"] != null && Session["assignmnetID"] != null && Session["eventId"] != null)
                        {
                            DateTime Date = DateTime.ParseExact(Session["startdate"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            DateTime NextDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                EventScheduleOn eventScheduleon = new EventScheduleOn();
                                eventScheduleon.EventInstanceID = Convert.ToInt32(Session["EventInstanceID"]);
                                eventScheduleon.StartDate = Date;
                                eventScheduleon.EndDate = Date;
                                eventScheduleon.ClosedDate = Date;
                                eventScheduleon.HeldOn = Date;
                                eventScheduleon.Period = "0";
                                eventScheduleon.CreatedBy = AuthenticationHelper.UserID;
                                eventScheduleon.CreatedOn = DateTime.Now;
                                eventScheduleon.Description = txtDescription.Text;
                                eventScheduleon.IsDeleted = false;
                                entities.EventScheduleOns.Add(eventScheduleon);
                                entities.SaveChanges();

                                long eventAssignmentID = Business.EventManagement.GetEventAssignmentID(Convert.ToInt64(eventScheduleon.EventInstanceID));
                                Business.EventManagement.CreateEventReminders(Convert.ToInt64(Session["ParentEventID"]), Convert.ToInt64(Session["EventInstanceID"]), Convert.ToInt64(eventAssignmentID), AuthenticationHelper.UserID, Convert.ToDateTime(Date));
                                foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                                {
                                    int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                                    //Parent
                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                        IntermediateEventID = 0,
                                        SubEventID = 0,
                                        Date = Date,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(Session["userID"]),
                                        Type = Type,
                                    };
                                    Business.EventManagement.CreateEventAssignDate(eventAssignDate);

                                    // Subevent
                                    GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                                    foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                                    {

                                        Label lblIntermediateEventID = (Label)SubEventrow.FindControl("IntermediateEventID");
                                        int IntermediateEventID = Convert.ToInt32(lblIntermediateEventID.Text);
                                        if (lblIntermediateEventID.Text == "0")
                                        {
                                            int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                            EventAssignDate subeventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = 0,
                                                SubEventID = SubEventID,
                                                Date = NextDate,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                                Type = Type,
                                            };
                                            Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                        }
                                        else
                                        {
                                            EventAssignDate IntermediatesubeventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = IntermediateEventID,
                                                SubEventID = 0,
                                                Date = NextDate,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                                Type = Type,
                                            };
                                            Business.EventManagement.CreateEventAssignDate(IntermediatesubeventAssignDate);

                                            GridView gvIntermediateSubEvent = SubEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                            foreach (GridViewRow SubEventrow1 in gvIntermediateSubEvent.Rows)
                                            {
                                                int SubEventID = Convert.ToInt32(SubEventrow1.Cells[1].Text);
                                                EventAssignDate subeventAssignDate = new EventAssignDate()
                                                {
                                                    ParentEventID = ParentEventID,
                                                    EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                    IntermediateEventID = IntermediateEventID,
                                                    SubEventID = SubEventID,
                                                    Date = NextDate,
                                                    IsActive = true,
                                                    CreatedDate = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                                    Type = Type,
                                                };
                                                Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                            }

                                        }
                                    }
                                }
                            }
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divOptionalCompliances", "$(\"#divOptionalCompliances\").dialog('close')", true);
                        }
                        grdEventList.EditIndex = -1;

                        BindEventData(EventClassificationID);
                    }
                    #endregion
                }
                else
                {
                    #region Secretrial 
                    var exceptComplianceIDs = EventManagement.CheckAllEventComplianceNonSecretrial(Convert.ToInt32(Session["eventId"].ToString()), Convert.ToInt32(Session["Rbranch"].ToString()));

                    if (exceptComplianceIDs.Count > 0)
                    {
                        grdNoAsignedComplinace.DataSource = EventManagement.GetAllNoAssignedComplinceList(exceptComplianceIDs);
                        grdNoAsignedComplinace.DataBind();
                        upNotAssignedCompliances.Update();
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divNotAssignedCompliances", "$(\"#divNotAssignedCompliances\").dialog('open')", true);
                    }
                    else
                    {
                        grdNoAsignedComplinace.DataSource = null;
                        grdNoAsignedComplinace.DataBind();
                        upOptionalCompliances.Update();

                        //foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                        //{
                        //    int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                        //    Session["ParentEventID"] = ParentEventID;
                        //    GridView gvEvenToCompliance = Eventrow.FindControl("gvParentToComplianceGrid") as GridView;
                        //    //ParentEvent -> compliance
                        //    foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                        //    {
                        //        int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[1].Text);
                        //    }

                        //    //ParentEvent -> SubEvent-> compliance
                        //    GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                        //    foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                        //    {
                        //        int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                        //        GridView gvSubCompliance = SubEventrow.FindControl("gvComplianceGrid") as GridView;
                        //        foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                        //        {
                        //            int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                        //            //ActualCheckedOptionalCompliances.Add(ComplinaceID);
                        //        }
                        //    }

                        //    //Intermediatre-> Subevent-> Compliance
                        //    GridView gvIntermediateEvent = Eventrow.FindControl("gvIntermediateGrid") as GridView;
                        //    foreach (GridViewRow intermediateEventrow in gvIntermediateEvent.Rows)
                        //    {
                        //        int IntermediateEventID = Convert.ToInt32(intermediateEventrow.Cells[1].Text);
                        //        GridView gvIntermediateSubEvent = intermediateEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                        //        foreach (GridViewRow SubEventrow in gvIntermediateSubEvent.Rows)
                        //        {
                        //            int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                        //            GridView gvSubCompliance = SubEventrow.FindControl("gvIntermediateComplainceGrid") as GridView;
                        //            foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                        //            {
                        //                int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                        //                //ActualCheckedOptionalCompliances.Add(ComplinaceID);
                        //            }
                        //        }
                        //    }
                        //}

                        if (Session["startdate"] != null && Session["assignmnetID"] != null && Session["eventId"] != null)
                        {
                            DateTime Date = DateTime.ParseExact(Session["startdate"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            DateTime NextDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                EventScheduleOn eventScheduleon = new EventScheduleOn();
                                eventScheduleon.EventInstanceID = Convert.ToInt32(Session["EventInstanceID"]);
                                eventScheduleon.StartDate = Date;
                                eventScheduleon.EndDate = Date;
                                eventScheduleon.ClosedDate = Date;
                                eventScheduleon.HeldOn = Date;
                                eventScheduleon.Period = "0";
                                eventScheduleon.CreatedBy = AuthenticationHelper.UserID;
                                eventScheduleon.CreatedOn = DateTime.Now;
                                eventScheduleon.Description = txtDescription.Text;
                                eventScheduleon.IsDeleted = false;
                                entities.EventScheduleOns.Add(eventScheduleon);
                                entities.SaveChanges();

                                long eventAssignmentID = Business.EventManagement.GetEventAssignmentID(Convert.ToInt64(eventScheduleon.EventInstanceID));

                                Business.EventManagement.CreateEventReminders(Convert.ToInt64(Session["ParentEventID"]), Convert.ToInt64(Session["EventInstanceID"]), Convert.ToInt64(eventAssignmentID), AuthenticationHelper.UserID, Convert.ToDateTime(Date));

                                foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                                {
                                    int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);

                                    //Parent
                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                        IntermediateEventID = 0,
                                        SubEventID = 0,
                                        Date = Date,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(Session["userID"]),
                                        Type = Type,
                                    };
                                    Business.EventManagement.CreateEventAssignDate(eventAssignDate);

                                    // Subevent
                                    GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                                    foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                                    {
                                        Label lblIntermediateEventID = (Label)SubEventrow.FindControl("IntermediateEventID");
                                        int IntermediateEventID = Convert.ToInt32(lblIntermediateEventID.Text);
                                        if (lblIntermediateEventID.Text == "0")
                                        {
                                            int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                            EventAssignDate subeventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = 0,
                                                SubEventID = SubEventID,
                                                Date = NextDate,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                                Type = Type,
                                            };
                                            Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                        }
                                        else
                                        { 
                                            EventAssignDate IntermediatesubeventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = IntermediateEventID,
                                                SubEventID = 0,
                                                Date = NextDate,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                                Type = Type,
                                            };
                                            Business.EventManagement.CreateEventAssignDate(IntermediatesubeventAssignDate);

                                            GridView gvIntermediateSubEvent = SubEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                            foreach (GridViewRow SubEventrow1 in gvIntermediateSubEvent.Rows)
                                            {
                                                int SubEventID = Convert.ToInt32(SubEventrow1.Cells[1].Text);
                                                EventAssignDate subeventAssignDate = new EventAssignDate()
                                                {
                                                    ParentEventID = ParentEventID,
                                                    EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                    IntermediateEventID = IntermediateEventID,
                                                    SubEventID = SubEventID,
                                                    Date = NextDate,
                                                    IsActive = true,
                                                    CreatedDate = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                                    Type = Type,
                                                };
                                                Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                            }
                                        }
                                    }
                                }
                            }
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divOptionalCompliances", "$(\"#divOptionalCompliances\").dialog('close')", true);
                        }
                        grdEventList.EditIndex = -1;
                        BindEventData(EventClassificationID);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divOptionalCompliances", "$(\"#divOptionalCompliances\").dialog('close')", true);
                grdEventList.EditIndex = -1;
                BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnBackMyEvent_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Session["MyEventBack"] = "true";
            //    Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //}
        }

        protected void ddlpageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdEventList.PageSize = int.Parse(((DropDownList)sender).SelectedValue);

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdEventList.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdEventList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                txtActivateDate.Text = "";
                txtEventNature.Text = "";

                BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));
                setDateToGridView();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {
                }

                if (!(StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdEventList.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdEventList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                txtActivateDate.Text = "";
                txtEventNature.Text = "";

                BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));
                setDateToGridView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlpageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdEventList.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdEventList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                txtActivateDate.Text = "";
                txtEventNature.Text = "";

                BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));
                setDateToGridView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;
                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlpageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = PageCount.ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlpageSize.SelectedValue);
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlpageSize.SelectedValue);
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocationAssigned.Text = tvFilterLocationAssigned.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upEventInstanceListAssigned_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocationAssigned');", tbxFilterLocationAssigned.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocationAssigned\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSaveShortNotice_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtShortNoticeDays1.Text == "" && rbtnShortnotice1.SelectedValue.Equals("1"))
                {
                    lblMessage.Text = "Please enter short notice days";
                }
                else
                {
                    lblMessage.Text = "";
                    SaveShortNotice();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void SaveShortNotice()
        {
            try
            {
                if (IsFromSingle == true)
                {
                    #region Click On single grid save button 

                    #region Save single Event Nature
                    string dt = Session["Date1"].ToString();
                    DateTime ActivateDate = DateTime.ParseExact(dt, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    string eventnature = Session["eventnature"].ToString();
                    int eventId = Convert.ToInt32(Session["eventId"]);
                    int branchID = Convert.ToInt32(Session["BranchID"]);
                    int EventInstanceID = Convert.ToInt32(Session["EventInstanceID"]);

                    if (!string.IsNullOrEmpty(eventnature))
                    {
                        BindParentEventData(Convert.ToInt32(eventId));

                        List<long> EventList1 = new List<long>();
                        EventList1.Add(Convert.ToInt64(eventId));

                        Session["EventList"] = EventList1;
                        string startdate = DateTime.Now.ToString("dd-MM-yyyy");
                        #region Save Event Nature
                        if (startdate != null)
                        {
                            DateTime Date = DateTime.ParseExact(startdate.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            DateTime NextDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                EventScheduleOn eventScheduleon = new EventScheduleOn();
                                eventScheduleon.EventInstanceID = EventInstanceID;
                                eventScheduleon.StartDate = Date;
                                eventScheduleon.EndDate = Date;
                                eventScheduleon.ClosedDate = Date;
                                eventScheduleon.HeldOn = Date;
                                eventScheduleon.Period = "0";
                                eventScheduleon.CreatedBy = AuthenticationHelper.UserID;
                                eventScheduleon.CreatedOn = DateTime.Now;
                                eventScheduleon.Description = eventnature; // txtDescription.Text;
                                eventScheduleon.IsDeleted = false;
                                entities.EventScheduleOns.Add(eventScheduleon);
                                entities.SaveChanges();

                                long eventAssignmentID = Business.EventManagement.GetEventAssignmentID(Convert.ToInt64(eventScheduleon.EventInstanceID));

                                Business.EventManagement.CreateEventReminders(Convert.ToInt64(eventId), EventInstanceID, Convert.ToInt64(eventAssignmentID), AuthenticationHelper.UserID, Convert.ToDateTime(Date));

                                foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                                {
                                    int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                                    //Parent
                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                        IntermediateEventID = 0,
                                        SubEventID = 0,
                                        Date = Date,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                        Type = Type,
                                    };
                                    Business.EventManagement.CreateEventAssignDate(eventAssignDate);

                                    // Subevent
                                    GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                                    foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                                    {
                                        Label lblIntermediateEventID = (Label)SubEventrow.FindControl("IntermediateEventID");
                                        int IntermediateEventID = Convert.ToInt32(lblIntermediateEventID.Text);
                                        if (lblIntermediateEventID.Text == "0")
                                        {
                                            int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                            EventAssignDate subeventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = 0,
                                                SubEventID = SubEventID,
                                                Date = NextDate,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                Type = Type,
                                            };
                                            Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                        }
                                        else
                                        {
                                            //Intermediatre-> Subevent-> Compliance
                                            //int IntermediateEventID = Convert.ToInt32(intermediateEventrow.Cells[1].Text);
                                            EventAssignDate IntermediatesubeventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = IntermediateEventID,
                                                SubEventID = 0,
                                                Date = NextDate,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                Type = Type,
                                            };
                                            Business.EventManagement.CreateEventAssignDate(IntermediatesubeventAssignDate);
                                            GridView gvIntermediateSubEvent = SubEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                            foreach (GridViewRow SubEventrow1 in gvIntermediateSubEvent.Rows)
                                            {
                                                int SubEventID = Convert.ToInt32(SubEventrow1.Cells[1].Text);
                                                EventAssignDate subeventAssignDate = new EventAssignDate()
                                                {
                                                    ParentEventID = ParentEventID,
                                                    EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                    IntermediateEventID = IntermediateEventID,
                                                    SubEventID = SubEventID,
                                                    Date = NextDate,
                                                    IsActive = true,
                                                    CreatedDate = DateTime.Now,
                                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                    Type = Type,
                                                };
                                                Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                            }
                                        }
                                    }
                                    EventScheDuleOnIDList.Add(new Tuple<long, long, int, DateTime, int>(eventScheduleon.ID, ParentEventID, Type, ActivateDate, Convert.ToInt32(branchID)));
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion

                    #region Set first Date to Activated Event
                    if (EventScheDuleOnIDList.Count > 0)
                    {
                        for (int i = 0; i < EventScheDuleOnIDList.Count; i++)
                        {
                            //var EventStructure = EventManagement.CheckEventStructure(EventScheDuleOnIDList[i].Item1);

                            //if (EventStructure == false) // EventStructure : MainEvent -> SubEvent -> Compliance
                            //{
                            #region EventStructure : MainEvent -> SubEvent -> Compliance
                            try
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    var EventSchedueleData = EventManagement.GetActivatedEventData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));

                                    int Type = Convert.ToInt32(EventScheDuleOnIDList[i].Item3);

                                    int CusomerBranchID = Convert.ToInt32(EventScheDuleOnIDList[i].Item5);

                                    int ParentEventID = EventSchedueleData.ParentEventID; // Convert.ToInt32(childGrid.DataKeys[0].Value.ToString());

                                    int SubEventID = EventSchedueleData.SubEventID;

                                    int IntermediateEventID = EventSchedueleData.IntermediateEventID;

                                    int EventScheduledOnID = Convert.ToInt32(EventScheDuleOnIDList[i].Item1);

                                    DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);  //childGrid.Rows[0].FindControl("txtgvChildGridDate") as TextBox;

                                    if (IntermediateEventID == 0)
                                    {
                                        #region Sub Event
                                        var SubEventToComplianceList = EventManagement.GetSubEventToCompliance(ParentEventID, SubEventID, EventScheduledOnID, Type, CusomerBranchID);

                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                        {
                                            ParentEventID = ParentEventID,
                                            EventScheduleOnID = EventScheduledOnID,
                                            IntermediateEventID = 0,
                                            SubEventID = SubEventID,
                                            Date = ActiveDate,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                            Type = Type,
                                        };
                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                                        for (int j = 0; j < SubEventToComplianceList.Count; j++)
                                        {
                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[j].ComplianceID);   //Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                                            Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                            if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);

                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                if (IsComplianceChecklistStatutory == true)
                                                {
                                                    //Change Generate flag Schedule change
                                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                }

                                                string days = Convert.ToString(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    if (rbtnShortnotice1.SelectedValue.Equals("1")) //Short Notice Selected 
                                                    {
                                                        var FlgCheckShortnotice = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                                                        if (FlgCheckShortnotice == false)
                                                        {
                                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                        }
                                                        else
                                                        {
                                                            //Short Notice
                                                            int shortnoticedays = Convert.ToInt32(txtShortNoticeDays1.Text);
                                                            int shortdays = shortnoticedays * -1;
                                                            EventManagement.SaveShortNoticeDays(ParentEventID, 0, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID, shortdays);

                                                            EventManagement.GenerateEventComplianceSchedueleForShortNotice(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, shortdays);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var FlgCheckShortnotice = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                                                        if (FlgCheckShortnotice == false)
                                                        {
                                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Intermediate Event

                                        var IntermediateSubEventSchedueleData = EventManagement.GetActivatedIntermediateSubEventSchedueleData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), IntermediateEventID, Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));
                                        int SubEventID1 = IntermediateSubEventSchedueleData.SubEventID;
                                        var SubEventToComplianceList = EventManagement.GetIntermediateSubEventToCompliance(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, Type, CusomerBranchID);
                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                        {
                                            ParentEventID = ParentEventID,
                                            EventScheduleOnID = EventScheduledOnID,
                                            IntermediateEventID = IntermediateEventID,
                                            SubEventID = SubEventID1,
                                            Date = ActiveDate,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                            Type = Type,
                                        };
                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);
                                        for (int k = 0; k < SubEventToComplianceList.Count; k++)
                                        {
                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[k].ComplianceID);

                                            Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                            if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                if (IsComplianceChecklistStatutory == true)
                                                {
                                                    //Change Generate flag Schedule change
                                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                }
                                                string days = Convert.ToString(SubEventToComplianceList[k].Days);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    if (rbtnShortnotice1.SelectedValue.Equals("1")) //Short Notice Selected 
                                                    {
                                                        var FlgCheckShortnotice = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                                                        if (FlgCheckShortnotice == false)
                                                        {
                                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                        }
                                                        else
                                                        {
                                                            //Short Notice
                                                            int shortnoticedays = Convert.ToInt32(txtShortNoticeDays1.Text);
                                                            int shortdays = shortnoticedays * -1;
                                                            EventManagement.SaveShortNoticeDays(ParentEventID, IntermediateEventID, SubEventID1, ComplinaceID, EventScheduledOnID, ComplianceInstanceID, shortdays);
                                                            EventManagement.GenerateEventComplianceSchedueleForShortNotice(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, shortdays);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var FlgCheckShortnotice = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                                                        if (FlgCheckShortnotice == false)
                                                        {
                                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                            #endregion
                            //}
                            //else // EventStructure : MainEvent ->IntermediateEvent -> SubEvent -> Compliance
                            //{
                            //    #region EventStructure : MainEvent -> IntermediateEvent -> SubEvent -> Compliance

                            //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            //    {
                            //        var IntermediateEventSchedueleData = EventManagement.GetIntermediateEventSchedueleData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));

                            //        int Type = Convert.ToInt32(EventScheDuleOnIDList[i].Item3);

                            //        int CusomerBranchID = Convert.ToInt32(EventScheDuleOnIDList[i].Item5);

                            //        int ParentEventID = IntermediateEventSchedueleData.ParentEventID;

                            //        int IntermediateEventID = IntermediateEventSchedueleData.IntermediateEventID;

                            //        int EventScheduledOnID = Convert.ToInt32(EventScheDuleOnIDList[i].Item1);


                            //    }
                            //    #endregion
                            //}
                        }
                    }
                    #endregion

                    BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalShortNotice1').modal('hide');", true);
                    upshortnotice1.Update();

                    txtActivateDate.Text = "";
                    txtEventNature.Text = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Event activated successfully')", true);

                    txtShortNoticeDays1.Text = "";

                    #endregion
                }
                else
                {
                    #region Click On multiple Save button 
                    Boolean FlagEntry = false;
                    foreach (GridViewRow rw in grdEventList.Rows)
                    {
                        FlagEntry = false;
                        int rowIndex = rw.RowIndex;
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");

                        if (chkBx != null && chkBx.Checked)
                        {
                            #region Save multiple Event Nature

                            string dt = DateTime.Now.ToString("dd-MM-yyyy");
                            string startdate = dt;

                            string eventnature = ((TextBox)rw.FindControl("txteventNatureGrid")).Text;

                            var Date1 = ((TextBox)rw.FindControl("txtActivateDateGrid")).Text;

                            DateTime ActivateDate = DateTime.ParseExact(Date1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            if (!string.IsNullOrEmpty(eventnature))
                            {
                                long branchID = Convert.ToInt64(((Label)rw.FindControl("lblBranch")).Text);

                                Type = EventManagement.GetCompanyType(Convert.ToInt32(branchID));

                                long assignmnetID = Convert.ToInt64(((Label)rw.FindControl("lblAssignmnetId")).Text);
                                string branchName = ((Label)rw.FindControl("lblBranchName")).Text;

                                string eventname = ((Label)rw.FindControl("lbleventName")).Text;

                                int UserId = Convert.ToInt32(((Label)rw.FindControl("lblUserID")).Text);

                                long eventId = (long)grdEventList.DataKeys[rowIndex].Value;

                                long EventInstanceID = Convert.ToInt32(((Label)rw.FindControl("lblEventInstanceID")).Text);

                                BindParentEventData(Convert.ToInt32(eventId));

                                List<long> EventList1 = new List<long>();
                                EventList1.Add(Convert.ToInt64(eventId));

                                Session["EventList"] = EventList1;

                                #region Save Event Nature
                                if (startdate != null)
                                {
                                    DateTime Date = DateTime.ParseExact(startdate.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                    DateTime NextDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        EventScheduleOn eventScheduleon = new EventScheduleOn();
                                        eventScheduleon.EventInstanceID = EventInstanceID;
                                        eventScheduleon.StartDate = Date;
                                        eventScheduleon.EndDate = Date;
                                        eventScheduleon.ClosedDate = Date;
                                        eventScheduleon.HeldOn = Date;
                                        eventScheduleon.Period = "0";
                                        eventScheduleon.CreatedBy = AuthenticationHelper.UserID;
                                        eventScheduleon.CreatedOn = DateTime.Now;
                                        eventScheduleon.Description = eventnature; // txtDescription.Text;
                                        eventScheduleon.IsDeleted = false;
                                        entities.EventScheduleOns.Add(eventScheduleon);
                                        entities.SaveChanges();

                                        long eventAssignmentID = Business.EventManagement.GetEventAssignmentID(Convert.ToInt64(eventScheduleon.EventInstanceID));

                                        Business.EventManagement.CreateEventReminders(Convert.ToInt64(eventId), EventInstanceID, Convert.ToInt64(eventAssignmentID), AuthenticationHelper.UserID, Convert.ToDateTime(Date));

                                        foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                                        {
                                            int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                                            //Parent
                                            EventAssignDate eventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = 0,
                                                SubEventID = 0,
                                                Date = Date,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                Type = Type,
                                            };
                                            Business.EventManagement.CreateEventAssignDate(eventAssignDate);

                                            // Subevent
                                            GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                                            foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                                            {
                                                Label lblIntermediateEventID = (Label)SubEventrow.FindControl("IntermediateEventID");
                                                int IntermediateEventID = Convert.ToInt32(lblIntermediateEventID.Text);
                                                if (lblIntermediateEventID.Text == "0")
                                                {
                                                    int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                                    EventAssignDate subeventAssignDate = new EventAssignDate()
                                                    {
                                                        ParentEventID = ParentEventID,
                                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                        IntermediateEventID = 0,
                                                        SubEventID = SubEventID,
                                                        Date = NextDate,
                                                        IsActive = true,
                                                        CreatedDate = DateTime.Now,
                                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                        Type = Type,
                                                    };
                                                    Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                                }
                                                else
                                                {
                                                    EventAssignDate IntermediatesubeventAssignDate = new EventAssignDate()
                                                    {
                                                        ParentEventID = ParentEventID,
                                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                        IntermediateEventID = IntermediateEventID,
                                                        SubEventID = 0,
                                                        Date = NextDate,
                                                        IsActive = true,
                                                        CreatedDate = DateTime.Now,
                                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                        Type = Type,
                                                    };
                                                    Business.EventManagement.CreateEventAssignDate(IntermediatesubeventAssignDate);

                                                    GridView gvIntermediateSubEvent = SubEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                                    foreach (GridViewRow SubEventrow1 in gvIntermediateSubEvent.Rows)
                                                    {
                                                        int SubEventID = Convert.ToInt32(SubEventrow1.Cells[1].Text);
                                                        EventAssignDate subeventAssignDate = new EventAssignDate()
                                                        {
                                                            ParentEventID = ParentEventID,
                                                            EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                            IntermediateEventID = IntermediateEventID,
                                                            SubEventID = SubEventID,
                                                            Date = NextDate,
                                                            IsActive = true,
                                                            CreatedDate = DateTime.Now,
                                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                            Type = Type,
                                                        };
                                                        Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                                    }
                                                }
                                            }
                                            if (FlagEntry == false)
                                            {
                                                EventScheDuleOnIDList.Add(new Tuple<long, long, int, DateTime, int>(eventScheduleon.ID, ParentEventID, Type, ActivateDate, Convert.ToInt32(branchID)));
                                                FlagEntry = true;
                                            }

                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion
                        }
                    }

                    #region Set first Date to Activated Event
                    if (EventScheDuleOnIDList.Count > 0)
                    {
                        for (int i = 0; i < EventScheDuleOnIDList.Count; i++)
                        {
                            #region EventStructure : MainEvent -> SubEvent -> Compliance
                            try
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    var EventSchedueleData = EventManagement.GetActivatedEventData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));

                                    int Type = Convert.ToInt32(EventScheDuleOnIDList[i].Item3);

                                    int CusomerBranchID = Convert.ToInt32(EventScheDuleOnIDList[i].Item5);

                                    int ParentEventID = EventSchedueleData.ParentEventID; // Convert.ToInt32(childGrid.DataKeys[0].Value.ToString());

                                    int SubEventID = EventSchedueleData.SubEventID;

                                    int IntermediateEventID = EventSchedueleData.IntermediateEventID;

                                    int EventScheduledOnID = Convert.ToInt32(EventScheDuleOnIDList[i].Item1);

                                    DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);  //childGrid.Rows[0].FindControl("txtgvChildGridDate") as TextBox;
                                    if (IntermediateEventID == 0)
                                    {
                                        #region Subvent
                                        var SubEventToComplianceList = EventManagement.GetSubEventToCompliance(ParentEventID, SubEventID, EventScheduledOnID, Type, CusomerBranchID);
                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                        {
                                            ParentEventID = ParentEventID,
                                            EventScheduleOnID = EventScheduledOnID,
                                            IntermediateEventID = 0,
                                            SubEventID = SubEventID,
                                            Date = ActiveDate,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                            Type = Type,
                                        };
                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                                        for (int j = 0; j < SubEventToComplianceList.Count; j++)
                                        {
                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[j].ComplianceID);   //Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                                            Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                            if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                if (IsComplianceChecklistStatutory == true)
                                                {
                                                    //Change Generate flag Schedule change
                                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                }
                                                string days = Convert.ToString(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    if (rbtnShortnotice1.SelectedValue.Equals("1")) //Short Notice Selected 
                                                    {
                                                        var FlgCheckShortnotice = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                                                        if (FlgCheckShortnotice == false)
                                                        {
                                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                        }
                                                        else
                                                        {
                                                            //Short Notice
                                                            int shortnoticedays = Convert.ToInt32(txtShortNoticeDays1.Text);
                                                            int shortdays = shortnoticedays * -1;
                                                            EventManagement.SaveShortNoticeDays(ParentEventID, 0, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID, shortdays);
                                                            EventManagement.GenerateEventComplianceSchedueleForShortNotice(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, shortdays);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var FlgCheckShortnotice = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                                                        if (FlgCheckShortnotice == false)
                                                        {
                                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Inntermediate
                                        //DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);
                                        var IntermediateSubEventSchedueleData = EventManagement.GetActivatedIntermediateSubEventSchedueleData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), IntermediateEventID, Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));
                                        int SubEventID1 = IntermediateSubEventSchedueleData.SubEventID;
                                        var SubEventToComplianceList = EventManagement.GetIntermediateSubEventToCompliance(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, Type, CusomerBranchID);
                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                        {
                                            ParentEventID = ParentEventID,
                                            EventScheduleOnID = EventScheduledOnID,
                                            IntermediateEventID = IntermediateEventID,
                                            SubEventID = SubEventID1,
                                            Date = ActiveDate,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                            Type = Type,
                                        };
                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);
                                        for (int k = 0; k < SubEventToComplianceList.Count; k++)
                                        {
                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[k].ComplianceID);

                                            Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                            if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                            {
                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                if (IsComplianceChecklistStatutory == true)
                                                {
                                                    //Change Generate flag Schedule change
                                                    EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                }
                                                string days = Convert.ToString(SubEventToComplianceList[k].Days);
                                                Boolean FlgCheck = false;
                                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                if (FlgCheck == false)
                                                {
                                                    if (rbtnShortnotice1.SelectedValue.Equals("1")) //Short Notice Selected 
                                                    {
                                                        var FlgCheckShortnotice = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                                                        if (FlgCheckShortnotice == false)
                                                        {
                                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                        }
                                                        else
                                                        {
                                                            //Short Notice
                                                            int shortnoticedays = Convert.ToInt32(txtShortNoticeDays1.Text);
                                                            int shortdays = shortnoticedays * -1;
                                                            EventManagement.SaveShortNoticeDays(ParentEventID, IntermediateEventID, SubEventID1, ComplinaceID, EventScheduledOnID, ComplianceInstanceID, shortdays);

                                                            EventManagement.GenerateEventComplianceSchedueleForShortNotice(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, shortdays);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var FlgCheckShortnotice = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                                                        if (FlgCheckShortnotice == false)
                                                        {
                                                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                            #endregion
                        }
                    }
                    #endregion

                    BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalShortNotice1').modal('hide');", true);
                    upshortnotice1.Update();

                    txtActivateDate.Text = "";
                    txtEventNature.Text = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Event activated successfully')", true);

                    txtShortNoticeDays1.Text = "";
                    #endregion 
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAllsave_click(object sender, EventArgs e)
        {
            try
            {
                int chkValidationflag = 0;
                IsFromSingle = false;

                Boolean chkNatureActiveDateflag = false;
                foreach (GridViewRow rw in grdEventList.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    if (chkBx != null && chkBx.Checked)
                    {
                        chkValidationflag = chkValidationflag + 1;
                        break;
                    }
                }

                foreach (GridViewRow rw in grdEventList.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    if (chkBx != null && chkBx.Checked)
                    {
                        string eventnature = ((TextBox)rw.FindControl("txteventNatureGrid")).Text;

                        var Date1 = ((TextBox)rw.FindControl("txtActivateDateGrid")).Text;

                        if (eventnature == "" || Date1 == "")
                        {
                            chkNatureActiveDateflag = true;
                            break;
                        }
                    }
                }

                if (chkValidationflag > 0)
                {
                    if (chkNatureActiveDateflag == false)
                    {
                        List<long> EventList = new List<long>();

                        #region Check Assignment
                        table = new DataTable();
                        table.Columns.Add("ID", typeof(long));
                        table.Columns.Add("EventID", typeof(long));
                        table.Columns.Add("EvnetName", typeof(string));
                        table.Columns.Add("ComplianceName", typeof(string));
                        table.Columns.Add("CustomerBranchName", typeof(string));
                        table.Columns.Add("CustomerBranchID", typeof(long));

                        foreach (GridViewRow rw in grdEventList.Rows)
                        {
                            int rowIndex = rw.RowIndex;
                            CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");

                            if (chkBx != null && chkBx.Checked)
                            {
                                string dt = DateTime.Now.ToString("dd-MM-yyyy");
                                string startdate = dt;

                                string eventnature = ((TextBox)rw.FindControl("txteventNatureGrid")).Text;

                                if (!string.IsNullOrEmpty(eventnature))
                                {
                                    int EventClassificationID = Convert.ToInt32(((Label)rw.FindControl("lblEventClassificationID")).Text);

                                    long branch = Convert.ToInt64(((Label)rw.FindControl("lblBranch")).Text);

                                    long eventId = (long)grdEventList.DataKeys[rowIndex].Value;

                                    if (EventClassificationID == 1) //Secretrial 
                                    {
                                        var exceptComplianceIDsList = EventManagement.CheckAllEventCompliance(Convert.ToInt32(eventId), Convert.ToInt32(branch));
                                        Session["exceptComplianceIDs"] = exceptComplianceIDsList;

                                        EventList.Add(Convert.ToInt64(eventId));
                                        int CustomerBranchId = Convert.ToInt32(branch);

                                        if (exceptComplianceIDsList.Count > 0)
                                        {
                                            var data = EventManagement.GetAllNotAssignedComplinceListForEvent(1, exceptComplianceIDsList, EventList, CustomerBranchId, -1);
                                            foreach (var item in data)
                                            {
                                                DataRow dr = table.NewRow();
                                                dr["ID"] = item.ID;
                                                dr["EventID"] = item.EventID;
                                                dr["EvnetName"] = item.EvnetName;
                                                dr["ComplianceName"] = item.ComplianceName;
                                                dr["CustomerBranchName"] = item.CustomerBranchName;
                                                dr["CustomerBranchID"] = item.CustomerBranchID;

                                                table.Rows.Add(dr);
                                            }
                                        }
                                    }
                                    else if (EventClassificationID == 2) //NonSecretrial
                                    {
                                        var exceptComplianceIDsList = EventManagement.CheckAllEventComplianceNonSecretrial(Convert.ToInt32(eventId), Convert.ToInt32(branch));
                                        Session["exceptComplianceIDs"] = exceptComplianceIDsList;
                                        EventList.Add(Convert.ToInt32(eventId));
                                        int CustomerBranchId = Convert.ToInt32(branch);

                                        if (exceptComplianceIDsList.Count > 0)
                                        {
                                            var data = EventManagement.GetAllNotAssignedComplinceListForEvent(1, exceptComplianceIDsList, EventList, CustomerBranchId, -1);
                                            foreach (var item in data)
                                            {
                                                DataRow dr = table.NewRow();
                                                dr["ID"] = item.ID;
                                                dr["EventID"] = item.EventID;
                                                dr["EvnetName"] = item.EvnetName;
                                                dr["ComplianceName"] = item.ComplianceName;
                                                dr["CustomerBranchName"] = item.CustomerBranchName;
                                                dr["CustomerBranchID"] = item.CustomerBranchID;

                                                table.Rows.Add(dr);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        if (table.Rows.Count > 0)
                        {
                            Session["EventList"] = EventList;
                            List<long> EventList1 = new List<long>();
                            EventList1 = (List<long>)Session["EventList"];
                            List<long> EvtList = EventList1.Select(i => (long)i).ToList();

                            BindCheckdEvents(EvtList);

                            grdNoAsignedComplinace.DataSource = table;
                            grdNoAsignedComplinace.DataBind();
                            upNotAssignedCompliances.Update();
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NotAssignedComplianceListModal();", true);
                        }
                        else
                        {
                            Boolean Flag = false;
                            Boolean FlgShortNoticeCheck = false;

                            foreach (GridViewRow rw in grdEventList.Rows)
                            {
                                #region Check Short Notice Compliance
                                CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");

                                if (chkBx != null && chkBx.Checked)
                                {
                                    int EventClassificationID = Convert.ToInt32(((Label)rw.FindControl("lblEventClassificationID")).Text);

                                    if (EventClassificationID == 1)
                                    {
                                        int rowIndex = rw.RowIndex;
                                        long eventId = (long)grdEventList.DataKeys[rowIndex].Value;
                                        long branchID = Convert.ToInt64(((Label)rw.FindControl("lblBranch")).Text);
                                        Type = EventManagement.GetCompanyType(Convert.ToInt32(branchID));

                                        BindParentEventData(Convert.ToInt32(eventId));

                                        foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                                        {
                                            int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                                            Session["ParentEventID"] = ParentEventID;
                                            GridView gvEvenToCompliance = Eventrow.FindControl("gvParentToComplianceGrid") as GridView;
                                            
                                            //ParentEvent -> compliance
                                            foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                                            {
                                                int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[1].Text);
                                                FlgShortNoticeCheck = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                                                if (FlgShortNoticeCheck == true)
                                                {
                                                    FlgShortNoticeCheck = true;
                                                    break;
                                                }
                                            }
                                            //ParentEvent -> SubEvent-> compliance
                                            GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                                            foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                                            {
                                                Label lblIntermediateEventID = (Label)SubEventrow.FindControl("IntermediateEventID");
                                                int IntermediateEventID = Convert.ToInt32(lblIntermediateEventID.Text);
                                                if (lblIntermediateEventID.Text == "0")
                                                {
                                                    int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                                    GridView gvSubCompliance = SubEventrow.FindControl("gvComplianceGrid") as GridView;
                                                    foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                                                    {
                                                        int ComplinaceID1 = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                                                        FlgShortNoticeCheck = EventManagement.CheckShortnoticeCompliance(ComplinaceID1);
                                                        if (FlgShortNoticeCheck == true)
                                                        {
                                                            FlgShortNoticeCheck = true;
                                                            break;
                                                        }
                                                    }
                                                    break;
                                                }
                                                else
                                                {
                                                    GridView gvIntermediateSubEvent = SubEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                                    foreach (GridViewRow SubEventrow1 in gvIntermediateSubEvent.Rows)
                                                    {
                                                        int SubEventID = Convert.ToInt32(SubEventrow1.Cells[1].Text);
                                                        GridView gvSubCompliance = SubEventrow1.FindControl("gvIntermediateComplainceGrid") as GridView;
                                                        foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                                                        {
                                                            int ComplinaceID2 = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                                                            FlgShortNoticeCheck = EventManagement.CheckShortnoticeCompliance(ComplinaceID2);
                                                            if (FlgShortNoticeCheck == true)
                                                            {
                                                                FlgShortNoticeCheck = true;
                                                                break;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (FlgShortNoticeCheck == true)
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalShortNotice1').modal();", true);
                                upOptionalCompliances.Update();
                            }
                            else
                            {
                                foreach (GridViewRow rw in grdEventList.Rows)
                                {
                                    Flag = false;
                                    int rowIndex = rw.RowIndex;
                                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");

                                    if (chkBx != null && chkBx.Checked)
                                    {
                                        #region Save multiple Event Nature

                                        string dt = DateTime.Now.ToString("dd-MM-yyyy");
                                        string startdate = dt;

                                        string eventnature = ((TextBox)rw.FindControl("txteventNatureGrid")).Text;

                                        var Date1 = ((TextBox)rw.FindControl("txtActivateDateGrid")).Text;

                                        DateTime ActivateDate = DateTime.ParseExact(Date1, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                        if (!string.IsNullOrEmpty(eventnature))
                                        {
                                            long branchID = Convert.ToInt64(((Label)rw.FindControl("lblBranch")).Text);

                                            int EventClassificationID1 = Convert.ToInt32(((Label)rw.FindControl("lblEventClassificationID")).Text);

                                            if(EventClassificationID1 == 1)
                                            {
                                                Type = EventManagement.GetCompanyType(Convert.ToInt32(branchID));

                                            }
                                            else
                                            {
                                                Type = 4;
                                            }

                                            long assignmnetID = Convert.ToInt64(((Label)rw.FindControl("lblAssignmnetId")).Text);
                                            string branchName = ((Label)rw.FindControl("lblBranchName")).Text;

                                            string eventname = ((Label)rw.FindControl("lbleventName")).Text;

                                            int UserId = Convert.ToInt32(((Label)rw.FindControl("lblUserID")).Text);

                                            long eventId = (long)grdEventList.DataKeys[rowIndex].Value;

                                            long EventInstanceID = Convert.ToInt32(((Label)rw.FindControl("lblEventInstanceID")).Text);

                                            #region Email for Assign compliance
                                            //var exceptComplianceIDs = EventManagement.CheckAllEventCompliance(eventId, branch);
                                            //if (exceptComplianceIDs.Count > 0)
                                            //{
                                            //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Associated compliances of this event are not assigned to location. Please assign compliances first and then update event held date.')", true);
                                            //    int customerID = -1;
                                            //    string ReplyEmailAddressName = "";
                                            //    if (AuthenticationHelper.Role.Equals("SADMN"))
                                            //        ReplyEmailAddressName = "Avantis";
                                            //    else
                                            //    {
                                            //        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                                            //        ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                                            //    }
                                            //    new Thread(() =>
                                            //    {
                                            //        User user = UserManagement.GetByID(UserId);
                                            //        List<User> emailsId = UserManagement.GetAdminUsers(CustomerBranchManagement.GetByID(branch).CustomerID);
                                            //        emailsId.Add(user);
                                            //        foreach (var us in emailsId)
                                            //        {
                                            //            string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                            //                             + "Dear " + us.FirstName + " " + us.LastName + ",<br /><br />"
                                            //                             + "Associated compliance of " + "'" + eventname + " have not been assigned to " + branchName + "'.<br> Please assign the compliance and update your event held on date.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                                            //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { us.Email }), null, null, "AVACOM Alert :: Missing compliances.", message);
                                            //        }
                                            //    }).Start();
                                            //}
                                            //else
                                            //{
                                            #endregion

                                            BindParentEventData(Convert.ToInt32(eventId));

                                            List<long> EventList1 = new List<long>();
                                            EventList1.Add(Convert.ToInt64(eventId));

                                            Session["EventList"] = EventList1;

                                            #region Save Event Nature
                                            if (startdate != null)
                                            {
                                                DateTime Date = DateTime.ParseExact(startdate.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                                DateTime NextDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                                {
                                                    EventScheduleOn eventScheduleon = new EventScheduleOn();
                                                    eventScheduleon.EventInstanceID = EventInstanceID;
                                                    eventScheduleon.StartDate = Date;
                                                    eventScheduleon.EndDate = Date;
                                                    eventScheduleon.ClosedDate = Date;
                                                    eventScheduleon.HeldOn = Date;
                                                    eventScheduleon.Period = "0";
                                                    eventScheduleon.CreatedBy = AuthenticationHelper.UserID;
                                                    eventScheduleon.CreatedOn = DateTime.Now;
                                                    eventScheduleon.Description = eventnature; // txtDescription.Text;
                                                    eventScheduleon.IsDeleted = false;
                                                    entities.EventScheduleOns.Add(eventScheduleon);
                                                    entities.SaveChanges();

                                                    long eventAssignmentID = Business.EventManagement.GetEventAssignmentID(Convert.ToInt64(eventScheduleon.EventInstanceID));

                                                    Business.EventManagement.CreateEventReminders(Convert.ToInt64(eventId), EventInstanceID, Convert.ToInt64(eventAssignmentID), AuthenticationHelper.UserID, Convert.ToDateTime(Date));

                                                    foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                                                    {
                                                        int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                                                        //Parent
                                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                                        {
                                                            ParentEventID = ParentEventID,
                                                            EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                            IntermediateEventID = 0,
                                                            SubEventID = 0,
                                                            Date = Date,
                                                            IsActive = true,
                                                            CreatedDate = DateTime.Now,
                                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                            Type = Type,
                                                        };
                                                        Business.EventManagement.CreateEventAssignDate(eventAssignDate);

                                                        // Subevent
                                                        GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                                                        foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                                                        {
                                                            Label lblIntermediateEventID = (Label)SubEventrow.FindControl("IntermediateEventID");
                                                            int IntermediateEventID = Convert.ToInt32(lblIntermediateEventID.Text);
                                                            if (lblIntermediateEventID.Text == "0")
                                                            {

                                                                int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                                                EventAssignDate subeventAssignDate = new EventAssignDate()
                                                                {
                                                                    ParentEventID = ParentEventID,
                                                                    EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                                    IntermediateEventID = 0,
                                                                    SubEventID = SubEventID,
                                                                    Date = NextDate,
                                                                    IsActive = true,
                                                                    CreatedDate = DateTime.Now,
                                                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                                    Type = Type,
                                                                };
                                                                Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                                            }
                                                            else
                                                            {
                                                                int IntermediateEventID1 = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                                                EventAssignDate IntermediatesubeventAssignDate = new EventAssignDate()
                                                                {
                                                                    ParentEventID = ParentEventID,
                                                                    EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                                    IntermediateEventID = IntermediateEventID1,
                                                                    SubEventID = 0,
                                                                    Date = NextDate,
                                                                    IsActive = true,
                                                                    CreatedDate = DateTime.Now,
                                                                    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                                    Type = Type,
                                                                };
                                                                Business.EventManagement.CreateEventAssignDate(IntermediatesubeventAssignDate);

                                                                GridView gvIntermediateSubEvent = SubEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                                                foreach (GridViewRow SubEventrow1 in gvIntermediateSubEvent.Rows)
                                                                {
                                                                    int SubEventID = Convert.ToInt32(SubEventrow1.Cells[1].Text);
                                                                    EventAssignDate subeventAssignDate = new EventAssignDate()
                                                                    {
                                                                        ParentEventID = ParentEventID,
                                                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                                        IntermediateEventID = IntermediateEventID,
                                                                        SubEventID = SubEventID,
                                                                        Date = NextDate,
                                                                        IsActive = true,
                                                                        CreatedDate = DateTime.Now,
                                                                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                                        Type = Type,
                                                                    };
                                                                    Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                                                }
                                                            }
                                                        }
                                                        if (Flag == false)
                                                        {
                                                            EventScheDuleOnIDList.Add(new Tuple<long, long, int, DateTime, int>(eventScheduleon.ID, ParentEventID, Type, ActivateDate, Convert.ToInt32(branchID)));
                                                            Flag = true;
                                                        }

                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                        #endregion
                                    }
                                }

                                #region Set first Date to Activated Event
                                if (EventScheDuleOnIDList.Count > 0)
                                {
                                    for (int i = 0; i < EventScheDuleOnIDList.Count; i++)
                                    {
                                        #region EventStructure : MainEvent -> SubEvent -> Compliance
                                        try
                                        {
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {
                                                var EventSchedueleData = EventManagement.GetActivatedEventData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));
                                                int Type = Convert.ToInt32(EventScheDuleOnIDList[i].Item3);
                                                int CusomerBranchID = Convert.ToInt32(EventScheDuleOnIDList[i].Item5);
                                                int ParentEventID = EventSchedueleData.ParentEventID;
                                                int SubEventID = EventSchedueleData.SubEventID;
                                                int IntermediateEventID = EventSchedueleData.IntermediateEventID;
                                                int EventScheduledOnID = Convert.ToInt32(EventScheDuleOnIDList[i].Item1);
                                                DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);
                                                long? EventClassificationID1 = EventManagement.GetEventClassification(ParentEventID);
                                                if (IntermediateEventID == 0)
                                                {
                                                    #region Sub Event
                                                    if (EventClassificationID1 == 1)
                                                    {
                                                        var SubEventToComplianceList = EventManagement.GetSubEventToCompliance(ParentEventID, SubEventID, EventScheduledOnID, Type, CusomerBranchID);

                                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                                        {
                                                            ParentEventID = ParentEventID,
                                                            EventScheduleOnID = EventScheduledOnID,
                                                            IntermediateEventID = 0,
                                                            SubEventID = SubEventID,
                                                            Date = ActiveDate,
                                                            IsActive = true,
                                                            CreatedDate = DateTime.Now,
                                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                            Type = Type,
                                                        };
                                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                                                        for (int j = 0; j < SubEventToComplianceList.Count; j++)
                                                        {
                                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[j].ComplianceID);   //Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);
                                                            int IsInternal = Convert.ToInt32(SubEventToComplianceList[j].IsInternal);

                                                            if (IsInternal == 0) //Statutory
                                                            {
                                                                Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                                                if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                                {
                                                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                                    Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);
                                                                    if (IsComplianceChecklistStatutory == true)
                                                                    {
                                                                        //Change Generate flag Schedule change
                                                                        EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                                    }
                                                                    string days = Convert.ToString(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                                    Boolean FlgCheck = false;
                                                                    FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                                    if (FlgCheck == false)
                                                                    {
                                                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                                    }
                                                                }
                                                            }
                                                            else if (IsInternal == 1) //Internal
                                                            {
                                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                                Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                                int days = Convert.ToInt32(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                                Boolean FlgCheck = false;
                                                                FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, 0, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                                if (FlgCheck == false)
                                                                {
                                                                    EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else if (EventClassificationID1 == 2)
                                                    {
                                                        var SubEventToComplianceList = EventManagement.GetNonSecretrialSubEventToCompliance(ParentEventID, SubEventID, EventScheduledOnID, Type, CusomerBranchID);

                                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                                        {
                                                            ParentEventID = ParentEventID,
                                                            EventScheduleOnID = EventScheduledOnID,
                                                            IntermediateEventID = 0,
                                                            SubEventID = SubEventID,
                                                            Date = ActiveDate,
                                                            IsActive = true,
                                                            CreatedDate = DateTime.Now,
                                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                            Type = Type,
                                                        };
                                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                                                        for (int j = 0; j < SubEventToComplianceList.Count; j++)
                                                        {
                                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[j].ComplianceID);   //Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                                                            int IsInternal = Convert.ToInt32(SubEventToComplianceList[j].IsInternal);

                                                            if (IsInternal == 0) //Statutory
                                                            {
                                                                Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                                                                if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                                {
                                                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                                    Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                                    if (IsComplianceChecklistStatutory == true)
                                                                    {
                                                                        //Change Generate flag Schedule change
                                                                        EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                                    }
                                                                    string days = Convert.ToString(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                                    Boolean FlgCheck = false;
                                                                    FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                                    if (FlgCheck == false)
                                                                    {
                                                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                                    }
                                                                }
                                                            }
                                                            else if (IsInternal == 1) //Internal
                                                            {
                                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                                int days = Convert.ToInt32(SubEventToComplianceList[j].Days); // Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                                                                Boolean FlgCheck = false;
                                                                FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, 0, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                                if (FlgCheck == false)
                                                                {
                                                                    EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Intermediate Event
                                                    //DateTime ActiveDate = Convert.ToDateTime(EventScheDuleOnIDList[i].Item4);
                                                    var IntermediateSubEventSchedueleData = EventManagement.GetActivatedIntermediateSubEventSchedueleData(Convert.ToInt32(EventScheDuleOnIDList[i].Item2), IntermediateEventID, Convert.ToInt32(EventScheDuleOnIDList[i].Item1), Convert.ToInt32(EventScheDuleOnIDList[i].Item3));
                                                    int SubEventID1 = IntermediateSubEventSchedueleData.SubEventID;
                                                    if (EventClassificationID1 == 1)
                                                    {
                                                        var SubEventToComplianceList = EventManagement.GetIntermediateSubEventToCompliance(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, Type, CusomerBranchID);
                                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                                        {
                                                            ParentEventID = ParentEventID,
                                                            EventScheduleOnID = EventScheduledOnID,
                                                            IntermediateEventID = IntermediateEventID,
                                                            SubEventID = SubEventID1,
                                                            Date = ActiveDate,
                                                            IsActive = true,
                                                            CreatedDate = DateTime.Now,
                                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                            Type = Type,
                                                        };
                                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);
                                                        for (int k = 0; k < SubEventToComplianceList.Count; k++)
                                                        {
                                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[k].ComplianceID);
                                                            int IsIntermediateInternal = Convert.ToInt32(SubEventToComplianceList[k].IsIntermediateInternal);

                                                            if (IsIntermediateInternal == 0) //Statutory
                                                            {
                                                                Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                                                if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                                {
                                                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                                    Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                                    if (IsComplianceChecklistStatutory == true)
                                                                    {
                                                                        //Change Generate flag Schedule change
                                                                        EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                                    }
                                                                    string days = Convert.ToString(SubEventToComplianceList[k].Days);
                                                                    Boolean FlgCheck = false;
                                                                    FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                                    if (FlgCheck == false)
                                                                    {
                                                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                                    }
                                                                }
                                                            }
                                                            else if (IsIntermediateInternal == 1) //Internal
                                                            {
                                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                                int days = Convert.ToInt32(SubEventToComplianceList[k].Days);
                                                                Boolean FlgCheck = false;
                                                                FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                                if (FlgCheck == false)
                                                                {
                                                                    EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else if (EventClassificationID1 == 2)
                                                    {
                                                        var SubEventToComplianceList = EventManagement.GetNonSecretrialIntermediateSubEventToCompliance(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, Type, CusomerBranchID);

                                                        EventAssignDate eventAssignDate = new EventAssignDate()
                                                        {
                                                            ParentEventID = ParentEventID,
                                                            EventScheduleOnID = EventScheduledOnID,
                                                            IntermediateEventID = IntermediateEventID,
                                                            SubEventID = SubEventID1,
                                                            Date = ActiveDate,
                                                            IsActive = true,
                                                            CreatedDate = DateTime.Now,
                                                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                                            Type = Type,
                                                        };
                                                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);
                                                        for (int k = 0; k < SubEventToComplianceList.Count; k++)
                                                        {
                                                            int ComplinaceID = Convert.ToInt32(SubEventToComplianceList[k].ComplianceID);
                                                            int IsIntermediateInternal = Convert.ToInt32(SubEventToComplianceList[k].IsIntermediateInternal);
                                                            if (IsIntermediateInternal == 0) //Statutory
                                                            {
                                                                Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                                                if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                                                {
                                                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                                    Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                                                                    if (IsComplianceChecklistStatutory == true)
                                                                    {
                                                                        //Change Generate flag Schedule change
                                                                        EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                                                                    }
                                                                    string days = Convert.ToString(SubEventToComplianceList[k].Days);
                                                                    Boolean FlgCheck = false;
                                                                    FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                                    if (FlgCheck == false)
                                                                    {
                                                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                                    }
                                                                }
                                                            }
                                                            else if (IsIntermediateInternal == 1) //Internal
                                                            {
                                                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(CusomerBranchID)).ID);
                                                                int days = Convert.ToInt32(SubEventToComplianceList[k].Days);
                                                                Boolean FlgCheck = false;
                                                                FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID1, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                                                if (FlgCheck == false)
                                                                {
                                                                    EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID1, EventScheduledOnID, ActiveDate, ComplinaceID, ComplianceInstanceID, days);
                                                                }
                                                            }
                                                        }
                                                    }

                                                    #endregion
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }
                                        #endregion

                                    }
                                }
                                #endregion
                                BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));
                                txtActivateDate.Text = "";
                                txtEventNature.Text = "";
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Event activated successfully')", true);
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Selected event activation date or nature of event can not be empty.')", true);
                        setDateToGridView();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Please select at least one event.')", true);
                    setDateToGridView();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlEvent.SelectedValue != "-1")
                {
                    int EventID = Convert.ToInt32(ddlEvent.SelectedValue);
                    GetAssignCompliance(EventID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        public void GetAssignCompliance(int EventID)
        {
            try
            {
                List<NotAssignedComplianceClass> lst = new List<NotAssignedComplianceClass>();
                lst = ConvertDataTable<NotAssignedComplianceClass>(table);
                var EventFilterrdList = lst.Where(entry => entry.EventID == EventID).ToList();
                gvComplianceListAssign.DataSource = EventFilterrdList;
                gvComplianceListAssign.DataBind();
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCheckdEvents(List<long> EventList)
        {
            try
            {
                ddlEvent.DataTextField = "Name";
                ddlEvent.DataValueField = "ID";
                ddlEvent.DataSource = EventManagement.GetCheckdEvent(EventList);
                ddlEvent.DataBind();
                ddlEvent.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void txtActivateDate_TextChanged(object sender, EventArgs e)
        {
            setDateToGridView();
        }

        protected void txtEventNature_TextChanged(object sender, EventArgs e)
        {
            setEventNatureToGridView();
        }

        public void setDateToGridView()
        {
            try
            {
                for (int i = 0; i < grdEventList.Rows.Count; i++)
                {
                    TextBox txt = (TextBox)grdEventList.Rows[i].FindControl("txtActivateDateGrid");
                    txt.Text = txtActivateDate.Text;

                    DateTime date1 = DateTime.MinValue;
                    if (DateTime.TryParseExact(txt.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerGrid", string.Format("initializeDatePickerGrid(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerGrid", "initializeDatePickerGrid(null);", true);
                    }
                }
                DateTime date = DateTime.Now;
                if (!string.IsNullOrEmpty(txtActivateDate.Text.Trim()))
                {
                    date = DateTime.ParseExact(txtActivateDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void setEventNatureToGridView()
        {
            try
            {
                for (int i = 0; i < grdEventList.Rows.Count; i++)
                {
                    TextBox txt = (TextBox)grdEventList.Rows[i].FindControl("txteventNatureGrid");
                    txt.Text = txtEventNature.Text;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        public void ClearFields()
        {
            txtEventNature.Text = "";
            txtActivateDate.Text = "";

            BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));
            setDateToGridView();
        }

        protected void ddlCheckListType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindEventData(Convert.ToInt32(ddlCheckListType.SelectedValue));
                setDateToGridView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}