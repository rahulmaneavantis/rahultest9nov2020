﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class UsersSummaryCompanyAdmin : System.Web.UI.UserControl
    {
        private int m_CustomerID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                m_CustomerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                BindCustomerBranches();
                BindCustomers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                ddlLocation.DataSource = CustomerBranchManagement.GetAllNVP(m_CustomerID);
                ddlLocation.DataBind();

                ddlLocation.Items.Insert(0, new ListItem("<-- Root -->", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCustomers();
        }

        private void BindCustomers()
        {
            try
            {
                chrtCustomers.DataSource = SummaryManagement.GetUsersPerEntitySummaryForCustomerAdmin(m_CustomerID, (ddlLocation.SelectedValue == "-1" ? null : (int?)Convert.ToInt32(ddlLocation.SelectedValue)));
                chrtCustomers.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}