﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Data;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class CannedReportPerformerCA : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BindFilters();
                dlFilters.SelectedIndex = 0;
                dlFilters_SelectedIndexChanged(null, null);
            }
        }

        private void BindFilters()
        {
            try
            {
                dlFilters.DataSource = Enumerations.GetAll<CannedReportFilterForPerformer>();
                dlFilters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                CannedReportFilterForPerformer filter = (CannedReportFilterForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                grdComplianceTransactions.DataSource = GetCannedReportDataForPerformer(customerid,AuthenticationHelper.UserID, filter);
                grdComplianceTransactions.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<ComplianceInstanceTransactionView> GetCannedReportDataForPerformer(int Customerid, int userID, CannedReportFilterForPerformer filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "PERF"
                                       select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);


                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                transactionsQuery = transactionsQuery.
                Where(entry => entry.UserID == userID && entry.RoleID == performerRoleID
                && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                ).ToList();


                switch (filter)
                {
                    case CannedReportFilterForPerformer.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && entry.ComplianceStatusID == 1).ToList();
                        break;
                    case CannedReportFilterForPerformer.Completed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 7).ToList();
                        break;
                    case CannedReportFilterForPerformer.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1 && entry.ScheduledOn < now).ToList();
                        break;
                    case CannedReportFilterForPerformer.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5).ToList();
                        break;
                    //Added by Sachin 30 Aug 2016
                    case CannedReportFilterForPerformer.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }
        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                dlFilters_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DataTable GetGrid()
        {
            dlFilters_SelectedIndexChanged(null, null);
            return (grdComplianceTransactions.DataSource as List<ComplianceInstanceTransactionView>).ToDataTable();
        }

        public string GetFilter()
        {
            return Enumerations.GetEnumByID<CannedReportFilterForPerformer>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected string GetReviewer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
                return "";
            }

        }
        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                int ScheduledOnID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("View_History"))
                {
                    BindTransactions(ScheduledOnID);
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog", "$(\"#divActDialog111\").dialog('open')", true);

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                int filtervalue = Convert.ToInt32(dlFilters.SelectedIndex);

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lnkbtnHistory = (LinkButton)e.Row.FindControl("lnkbtnHistory");
                    Label lblScheduledOnID = (Label)e.Row.FindControl("lblScheduledOnID");
                    //BindTransactions(Convert.ToInt32(lblScheduledOnID.Text));
                    if (filtervalue == 4)
                    {
                        lnkbtnHistory.Visible = true;
                        //BindTransactions(Convert.ToInt32(lblScheduledOnID.Text));
                    }
                    else
                    {
                        lnkbtnHistory.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {

                // throw new NotImplementedException();

                if (ScheduledOnID != 0)
                {
                    grdTransactionHistory.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                    grdTransactionHistory.DataBind();
                    UpDetailView.Update();
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog", "$(\"#divActDialog111\").dialog('open')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
        }
        protected string GetApprover(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 6);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);          
                return "";
            }
        }
        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                CannedReportFilterForPerformer filter = (CannedReportFilterForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                var assignmentList = GetCannedReportDataForPerformer(customerid,AuthenticationHelper.UserID, filter);
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceTransactions.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                    }
                }
              
                grdComplianceTransactions.DataSource = assignmentList;
                grdComplianceTransactions.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

    }
}