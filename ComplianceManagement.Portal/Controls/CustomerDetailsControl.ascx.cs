﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class CustomerDetailsControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ChkSp();
                BindCustomerStatus();
                BindLocationType();

                BindServiceProvider();
                BindRegistrationBy();

                txtProductType.Attributes.Add("readonly", "readonly");
                txtProductType.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "initializeJQueryUI", "initializeJQueryUI();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
            }
        }
        public static object FillProduct(int serviceproviderid, bool istrue)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (istrue)
                {
                    var query = (from row in entities.Products
                                 join row1 in entities.ProductMappings
                                 on row.Id equals row1.ProductID
                                 where row1.CustomerID == serviceproviderid
                                 select row);


                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();
                    return users;

                }
                else
                {
                    var query = (from row in entities.Products
                                 select row);

                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();
                    return users;

                }
            }
        }
        public void BindProductType(int servicepid, bool isTrue)
        {
            try
            {
                if (isTrue)
                {
                    rptProductType.DataSource = FillProduct(Convert.ToInt32(servicepid), isTrue);
                    rptProductType.DataBind();
                    foreach (RepeaterItem aItem in rptProductType.Items)
                    {
                        CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                        if (!chkProduct.Checked)
                        {
                            chkProduct.Checked = true;
                        }
                    }
                    CheckBox ProductSelectAll = (CheckBox)rptProductType.Controls[0].Controls[0].FindControl("ProductSelectAll");
                    ProductSelectAll.Checked = true;
                }
                else
                {
                    rptProductType.DataSource = FillProduct(-1, isTrue);
                    rptProductType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindLocationType()
        {
            try
            {
                ddlLocationType.DataTextField = "Name";
                ddlLocationType.DataValueField = "ID";
                ddlLocationType.Items.Clear();
                ddlLocationType.DataSource = ProcessManagement.FillLocationType();
                ddlLocationType.DataBind();
                ddlLocationType.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void ChkSp()
        {
            if (chkSp.Checked == true)
            {

                lblast.Visible = false;
                lblsp.Visible = false;
                ddlSPName.Visible = false;

                lblRegAst.Visible = true;
                lblRegBy.Visible = true;
                ddlRegistrationBy.Visible = true;
            }
            else
            {
                lblast.Visible = true;
                lblsp.Visible = true;
                ddlSPName.Visible = true;

                lblRegAst.Visible = false;
                lblRegBy.Visible = false;
                ddlRegistrationBy.Visible = false;
            }
        }
        public void BindServiceProvider()
        {
            try
            {
                int serviceProviderID = -1;
                if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlSPName.DataTextField = "Name";
                ddlSPName.DataValueField = "ID";
                ddlSPName.Items.Clear();
                ddlSPName.DataSource = CustomerManagement.FillServiceProvider(serviceProviderID);
                ddlSPName.DataBind();

                ddlSPName.Items.Insert(0, new ListItem("< Select Service Provider >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindRegistrationBy()
        {
            try
            {
                ddlRegistrationBy.DataTextField = "Name";
                ddlRegistrationBy.DataValueField = "ID";

                ddlRegistrationBy.Items.Clear();

                ddlRegistrationBy.DataSource = CustomerManagement.Fill_RegistrationBy();
                ddlRegistrationBy.DataBind();

                ddlRegistrationBy.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<long> GetProductData(int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DeptIDs = (from row in entities.ProductMappings
                               where row.CustomerID == customerid && row.IsActive == false
                               select (long)row.ProductID).ToList();

                return DeptIDs;
            }
        }
        public void EditCustomerInformation(int customerID)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "initializeJQueryUI", "initializeJQueryUI();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
                lblErrorMassage.Text = "";
                ViewState["Mode"] = 1;
                ViewState["CustomerID"] = customerID;
                if (AuthenticationHelper.Role.Equals("IMPT"))
                {
                    issprd.Visible = true;
                }
                Customer customer = CustomerManagement.GetByID(customerID);
                tbxName.Text = customer.Name;
                tbxAddress.Text = customer.Address;
                tbxBuyerName.Text = customer.BuyerName;
                tbxBuyerContactNo.Text = customer.BuyerContactNumber;
                tbxBuyerEmail.Text = customer.BuyerEmail;
                txtStartDate.Text = customer.StartDate != null ? customer.StartDate.Value.ToString("dd-MM-yyyy") : " ";
                txtEndDate.Text = customer.EndDate != null ? customer.EndDate.Value.ToString("dd-MM-yyyy") : " ";
                txtDiskSpace.Text = customer.DiskSpace;
                if (customer.LocationType != null)
                {
                    ddlLocationType.SelectedValue = Convert.ToString(customer.LocationType);
                }
                if (customer.IComplianceApplicable != null)
                {
                    ddlComplianceApplicable.SelectedValue = Convert.ToString(customer.IComplianceApplicable);
                }
                if (customer.TaskApplicable != null)
                {
                    ddlTaskApplicable.SelectedValue = Convert.ToString(customer.TaskApplicable);
                }
                if (customer.IsLabelApplicable != null)
                {
                    ddlLabelApplicable.SelectedValue = Convert.ToString(customer.IsLabelApplicable);
                }

                if (customer.ComplianceProductType != null)
                {
                    if (ddlComplianceProductType.Items.FindByValue(Convert.ToString(customer.ComplianceProductType)) != null)
                    {
                        ddlComplianceProductType.ClearSelection();
                        ddlComplianceProductType.Items.FindByValue(Convert.ToString(customer.ComplianceProductType)).Selected = true;
                    }
                }
                #region Product    
                if (customer.ServiceProviderID == 0 || customer.ServiceProviderID == null)
                {
                    BindProductType(-1, false);
                }
                else
                {
                    BindProductType((int)customer.ServiceProviderID, true);
                }

                var vGetProductMappedIDs = GetProductData(customerID);
                foreach (RepeaterItem aItem in rptProductType.Items)
                {
                    CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                    chkProduct.Checked = false;
                    CheckBox ProductSelectAll = (CheckBox)rptProductType.Controls[0].Controls[0].FindControl("ProductSelectAll");

                    for (int i = 0; i <= vGetProductMappedIDs.Count - 1; i++)
                    {
                        if (((Label)aItem.FindControl("lblProductID")).Text.Trim() == vGetProductMappedIDs[i].ToString())
                        {
                            chkProduct.Checked = true;
                        }
                    }
                    if ((rptProductType.Items.Count) == (vGetProductMappedIDs.Count))
                    {
                        ProductSelectAll.Checked = true;
                    }
                    else
                    {
                        ProductSelectAll.Checked = false;
                    }
                }
                #endregion
                if (customer.Status != null)
                {
                    ddlCustomerStatus.SelectedValue = Convert.ToString(customer.Status);
                }
                if (customer.IsServiceProvider == true)
                {
                    chkSp.Checked = true;
                    ChkSp();
                    customer.ServiceProviderID = null;
                }
                else
                {
                    BindServiceProvider();
                    chkSp.Checked = false;
                    ChkSp();
                    if (customer.ServiceProviderID != null)
                    {
                        ddlSPName.SelectedValue = Convert.ToString(customer.ServiceProviderID);
                    }
                }

                //Added for ICAI/ICSI
                if (customer.RegistrationBy != null)
                {
                    ddlRegistrationBy.ClearSelection();
                    if (ddlRegistrationBy.Items.FindByValue(Convert.ToString(customer.RegistrationBy)) != null)
                        ddlRegistrationBy.Items.FindByValue(Convert.ToString(customer.RegistrationBy)).Selected = true;
                }

                upCustomers.Update();
                ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "OpenDialog", "$(\"#divCustomersDialog\").dialog('open')", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void AddCustomer()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lblErrorMassage.Text = string.Empty;
                    ViewState["Mode"] = 0;
                    chkSp.Checked = false;
                    chkSp_CheckedChanged(null, null);
                    tbxName.Text = tbxAddress.Text = tbxBuyerName.Text = tbxBuyerContactNo.Text = tbxBuyerEmail.Text = txtStartDate.Text = txtEndDate.Text = txtDiskSpace.Text = string.Empty;
                    ddlCustomerStatus.SelectedIndex = -1;
                    txtProductType.Text = "< Select >";

                    var ParentBranchLimitcount = (from row in entities.Customers
                                                  join row1 in entities.Users
                                                  on row.ID equals row1.CustomerID
                                                  where row1.ID==AuthenticationHelper.UserID
                                                  select row).FirstOrDefault();

                    ddlSPName.SelectedIndex = -1;
                    if (ParentBranchLimitcount !=null)
                    {
                        if (ParentBranchLimitcount.IsServiceProvider == true)
                        {
                            ddlSPName.SelectedValue = Convert.ToString(ParentBranchLimitcount.ID);
                        }
                    }                                                       
                    ddlRegistrationBy.ClearSelection();

                    if (AuthenticationHelper.Role.Equals("IMPT"))
                    {
                        issprd.Visible = true;
                    }
                    foreach (RepeaterItem aItem in rptProductType.Items)
                    {
                        CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                        chkProduct.Checked = false;
                        CheckBox ProductSelectAll = (CheckBox)rptProductType.Controls[0].Controls[0].FindControl("ProductSelectAll");
                        ProductSelectAll.Checked = false;
                    }
                    ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "OpenDialog", "$(\"#divCustomersDialog\").dialog('open');initializeDatePicker(null); ", true);
                    ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "initializeJQueryUI", "initializeJQueryUI();", true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
                    upCustomers.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlComplianceApplicable_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        public static bool CheckCustoemrBranchLimit(int serviceProviderID, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ParentBranchLimitcount = (from row in entities.CustomerBranchLimits
                                              where row.ServiceproviderID == serviceProviderID
                                              && row.CustoemerID == customerID
                                              select row.ParentBranchLimit).FirstOrDefault();

                var Parentbranchcount = (from row in entities.CustomerBranches
                                         where row.CustomerID == customerID
                                          && row.IsDeleted == false
                                         select row.ID).ToList();

                if (ParentBranchLimitcount > Parentbranchcount.Count)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected void ddlSPName_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSave.Visible = true;
            if (!string.IsNullOrEmpty(ddlSPName.SelectedValue))
            {
                if (ddlSPName.SelectedValue != "-1")
                {
                    BindProductType(Convert.ToInt32(ddlSPName.SelectedValue), true);
                    bool CheckCustomerLimit = true;
                    CheckCustomerLimit = ICIAManagement.CheckCustomerLimitCompliance(Convert.ToInt32(ddlSPName.SelectedValue));

                    if (CheckCustomerLimit)
                    {
                        btnSave.Visible = false;
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Facility to create more customers is not available in the Free version. Kindly contact Avantis to activate the same.";
                    }
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool CheckCustomerLimit = true;
                    if (!string.IsNullOrEmpty(ddlSPName.SelectedValue))
                    {
                        if (ddlSPName.SelectedValue != "-1")
                        {
                            CheckCustomerLimit = ICIAManagement.CheckCustomerLimitCompliance(Convert.ToInt32(ddlSPName.SelectedValue));
                        }
                        else
                        {
                            CheckCustomerLimit = false;
                        }
                    }
                    else
                    {
                        CheckCustomerLimit = false;
                    }
                    if (!CheckCustomerLimit)
                    {

                        #region
                        Customer customer = new Customer();
                        customer.Name = tbxName.Text.Trim();
                        customer.Address = tbxAddress.Text;
                        customer.BuyerName = tbxBuyerName.Text;
                        customer.BuyerContactNumber = tbxBuyerContactNo.Text;
                        customer.BuyerEmail = tbxBuyerEmail.Text;
                        string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                        string enddate = Request[txtEndDate.UniqueID].ToString().Trim();
                        if (strtdate != "" && enddate != "")
                        {
                            customer.StartDate = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            customer.EndDate = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        customer.DiskSpace = txtDiskSpace.Text;
                        if ((int)ViewState["Mode"] == 1)
                        {
                            customer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                        }
                        if (CustomerManagement.Exists(customer))
                        {
                            cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }
                        if (chkSp.Checked == false)
                        {
                            if (ddlSPName.SelectedIndex == 0)
                            {
                                cvDuplicateEntry.ErrorMessage = "Please Select Service Provider Name.";
                                cvDuplicateEntry.IsValid = false;
                                return;
                            }
                        }

                        if (chkSp.Checked)
                        {
                            if (string.IsNullOrEmpty(ddlRegistrationBy.SelectedValue) || ddlRegistrationBy.SelectedValue == "-1")
                            {
                                cvDuplicateEntry.ErrorMessage = "Please Select Registration By";
                                cvDuplicateEntry.IsValid = false;
                                return;
                            }
                        }

                        customer.LocationType = Convert.ToInt32(ddlLocationType.SelectedValue);
                        customer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                        customer.IComplianceApplicable = Convert.ToInt32(ddlComplianceApplicable.SelectedValue);
                        customer.TaskApplicable = Convert.ToInt32(ddlTaskApplicable.SelectedValue);
                        customer.IsLabelApplicable = Convert.ToInt32(ddlLabelApplicable.SelectedValue);
                        customer.IsServiceProvider = chkSp.Checked;

                        if (chkSp.Checked == true)
                        {
                            customer.IsServiceProvider = true;
                            customer.ServiceProviderID = null;
                            customer.IsDistributor = true;

                            //Added for ICAI/ICSI
                            if (!string.IsNullOrEmpty(ddlRegistrationBy.SelectedValue))
                            {
                                if (ddlRegistrationBy.SelectedValue != "-1")
                                {
                                    customer.RegistrationBy = Convert.ToInt32(ddlRegistrationBy.SelectedValue);
                                }
                            }
                        }
                        else
                        {
                            customer.ParentID = Convert.ToInt32(ddlSPName.SelectedValue);
                            customer.IsDistributor = false;
                            customer.IsServiceProvider = false;
                            customer.ServiceProviderID = Convert.ToInt32(ddlSPName.SelectedValue);

                            //Added for ICAI/ICSI
                            var serviceProviderCustomerRecord = CustomerManagement.GetByID(Convert.ToInt32(ddlSPName.SelectedValue));

                            if (serviceProviderCustomerRecord != null)
                                customer.RegistrationBy = serviceProviderCustomerRecord.RegistrationBy;
                        }

                        if (string.IsNullOrEmpty(ddlComplianceProductType.SelectedValue))
                        {
                            customer.ComplianceProductType = Convert.ToInt32(ddlComplianceProductType.SelectedValue);
                        }
                        else
                            customer.ComplianceProductType = 0;

                        com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer mstCustomer = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer();
                        mstCustomer.Name = tbxName.Text.Trim();
                        mstCustomer.Address = tbxAddress.Text;
                        mstCustomer.BuyerName = tbxBuyerName.Text;
                        mstCustomer.BuyerContactNumber = tbxBuyerContactNo.Text;
                        mstCustomer.BuyerEmail = tbxBuyerEmail.Text;
                        if (strtdate != "" && enddate != "")
                        {
                            mstCustomer.StartDate = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            mstCustomer.EndDate = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        if ((int)ViewState["Mode"] == 1)
                        {
                            mstCustomer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                        }
                        if (CustomerManagementRisk.Exists(mstCustomer))
                        {
                            cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }
                        mstCustomer.LocationType = Convert.ToInt32(ddlLocationType.SelectedValue);
                        mstCustomer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                        mstCustomer.IComplianceApplicable = Convert.ToInt32(ddlComplianceApplicable.SelectedValue);
                        mstCustomer.TaskApplicable = Convert.ToInt32(ddlTaskApplicable.SelectedValue);
                        mstCustomer.IsLabelApplicable = Convert.ToInt32(ddlLabelApplicable.SelectedValue);
                        mstCustomer.IsServiceProvider = chkSp.Checked;

                        if (chkSp.Checked == true)
                        {
                            mstCustomer.IsServiceProvider = true;
                            mstCustomer.ServiceProviderID = null;
                            mstCustomer.IsDistributor = true;
                        }
                        else
                        {
                            mstCustomer.ParentID = Convert.ToInt32(ddlSPName.SelectedValue);
                            mstCustomer.IsDistributor = false;
                            mstCustomer.IsServiceProvider = false;
                            mstCustomer.ServiceProviderID = Convert.ToInt32(ddlSPName.SelectedValue);
                        }

                        if (string.IsNullOrEmpty(ddlComplianceProductType.SelectedValue))
                        {
                            mstCustomer.ComplianceProductType = Convert.ToInt32(ddlComplianceProductType.SelectedValue);
                        }
                        else
                            mstCustomer.ComplianceProductType = 0;

                        mstCustomer.RegistrationBy = customer.RegistrationBy;

                        if ((int)ViewState["Mode"] == 0)
                        {

                            bool resultDataChk = false;
                            int serviceProviderID = 0;
                            int resultData = 0;

                            resultData = CustomerManagement.Create(customer);
                            if (resultData > 0)
                            {
                                resultDataChk = CustomerManagementRisk.Create(mstCustomer);
                                if (resultDataChk == false)
                                {
                                    CustomerManagement.deleteCustReset(resultData);
                                }
                                else
                                {
                                    var masterlimit = entities.Master_Limitations.FirstOrDefault();
                                    if (chkSp.Checked == true)
                                    {
                                        ServiceProviderCustomerLimit obj = new ServiceProviderCustomerLimit();
                                        if (masterlimit != null)
                                        {
                                            obj.ServiceProviderID = mstCustomer.ID;
                                            obj.CustomerLimit = masterlimit.CustomerLimit;
                                            obj.UserLimit = masterlimit.UserLimit;
                                            obj.DistributorID = mstCustomer.ID;
                                            obj.ParentBranchLimit = 25;
                                            obj.BranchLimit = 1;
                                            obj.EmployeeLimit = 25;
                                        }
                                        else
                                        {
                                            obj.ServiceProviderID = mstCustomer.ID;
                                            obj.CustomerLimit = 25;
                                            obj.UserLimit = 5;
                                            obj.DistributorID = mstCustomer.ID;
                                            obj.ParentBranchLimit = 25;
                                            obj.BranchLimit = 1;
                                            obj.EmployeeLimit = 25;
                                        }
                                        ICIAManagement.CreateServiceProviderCustomerLimit(obj);
                                    }
                                    else
                                    {
                                        CustomerBranchLimit cobj = new CustomerBranchLimit();
                                        if (masterlimit != null)
                                        {
                                            cobj.ServiceproviderID = Convert.ToInt32(ddlSPName.SelectedValue);
                                            cobj.CustoemerID = mstCustomer.ID;
                                            cobj.ParentBranchLimit = masterlimit.ParentBranchLimit;
                                            cobj.DistributorID = Convert.ToInt32(ddlSPName.SelectedValue);
                                            cobj.BranchLimit = masterlimit.BranchLimit;
                                            cobj.EmployeeLimit = masterlimit.EmployeeLimit;
                                        }
                                        else
                                        {
                                            cobj.ServiceproviderID = Convert.ToInt32(ddlSPName.SelectedValue);
                                            cobj.CustoemerID = mstCustomer.ID;
                                            cobj.ParentBranchLimit = 25;
                                            cobj.DistributorID = Convert.ToInt32(ddlSPName.SelectedValue);
                                            cobj.BranchLimit = 1;
                                            cobj.EmployeeLimit = 25;
                                        }
                                        ICIAManagement.CreateCustomerBranchLimit(cobj);
                                    }
                                    List<int> ProductIds = new List<int>();
                                    foreach (RepeaterItem aItem in rptProductType.Items)
                                    {
                                        CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                                        if (chkProduct.Checked)
                                        {
                                            ProductIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblProductID")).Text.Trim()));
                                            Business.Data.ProductMapping productmapping = new Business.Data.ProductMapping()
                                            {
                                                CustomerID = mstCustomer.ID,
                                                ProductID = Convert.ToInt32(((Label)aItem.FindControl("lblProductID")).Text.Trim()),
                                                IsActive = false,
                                                CreatedOn = DateTime.Now,
                                                CreatedBy = AuthenticationHelper.UserID
                                            };
                                            if (productmapping.ProductID == 3 || productmapping.ProductID == 4)
                                            {
                                                //add if audit product
                                                ProcessManagement.CreateProcessSubprocessPredefined(resultData, AuthenticationHelper.UserID);
                                            }
                                            Business.ComplianceManagement.CreateProductMapping(productmapping);
                                        }
                                    }
                                }
                            }

                            // Added by SACHIN 28 April 2016
                            string ReplyEmailAddressName = "Avantis";
                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerCreaded
                                                .Replace("@NewCustomer", customer.Name)
                                                .Replace("@LoginUser", AuthenticationHelper.User)
                                                .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                .Replace("@From", ReplyEmailAddressName)
                                                .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                            ;

                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM customer account created.", message);

                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            CustomerManagement.Update(customer);
                            CustomerManagementRisk.Update(mstCustomer);//added by rahul on 18 April 2016


                            List<int> ProductIds = new List<int>();
                            Business.ComplianceManagement.UpdateProductMapping(customer.ID);
                            foreach (RepeaterItem aItem in rptProductType.Items)
                            {
                                CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                                if (chkProduct.Checked)
                                {
                                    ProductIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblProductID")).Text.Trim()));

                                    Business.Data.ProductMapping productmapping = new Business.Data.ProductMapping()
                                    {
                                        CustomerID = mstCustomer.ID,
                                        ProductID = Convert.ToInt32(((Label)aItem.FindControl("lblProductID")).Text.Trim()),
                                        IsActive = false,
                                        CreatedOn = DateTime.Now,
                                        CreatedBy = AuthenticationHelper.UserID
                                    };
                                    Business.ComplianceManagement.CreateProductMapping(productmapping);
                                }
                            }
                        }
                        ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "CloseDialog", "$(\"#divCustomersDialog\").dialog('close')", true);
                        if (OnSaved != null)
                        {
                            OnSaved(this, null);
                        }
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);

                        BindServiceProvider();
                        #endregion
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Facility to create more customers is not available in the Free version. Kindly contact Avantis to activate the same.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCustomers_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeCombobox", "initializeCombobox();", true);
                ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "initializeJQueryUI", "initializeJQueryUI();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// This method for binding customer status.
        /// </summary>
        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();

                if (ddlCustomerStatus.Items.FindByText("Suspended") != null)
                    ddlCustomerStatus.Items.Remove(ddlCustomerStatus.Items.FindByText("Suspended"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public event EventHandler OnSaved;

        protected void chkSp_CheckedChanged(object sender, EventArgs e)
        {
            ChkSp();
            BindProductType(-1, false);
        }

        public static string Invoke(string Method, string Uri, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            int _TimeoutSec = 90;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //var authValue = "5E8E3D45-172D-4D58-8DF8-EB0B0E";
            //cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));
            //var _UserAgent = "f2UKmAm0KlI98bEYGGxEHQ==";
            //// You can actually also set the User-Agent via a built-in property
            //cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);
            //// You get the following exception when trying to set the "Content-Type" header like this:
            //// cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
            //// "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "POSTJson":
                    {


                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }

        #region 
        public class Compliance
        {
            public object filingFrequency { get; set; }
        }

        public class Addr
        {
            public string st { get; set; }
            public string lg { get; set; }
            public string stcd { get; set; }
            public string pncd { get; set; }
            public string bno { get; set; }
            public string flno { get; set; }
            public string dst { get; set; }
            public string loc { get; set; }
            public string bnm { get; set; }
            public string lt { get; set; }
            public string city { get; set; }
        }

        public class Pradr
        {
            public string ntr { get; set; }
            public Addr addr { get; set; }
        }

        public class TaxpayerInfo
        {
            public object errorMsg { get; set; }
            public string ctj { get; set; }
            public Pradr pradr { get; set; }
            public string cxdt { get; set; }
            public string sts { get; set; }
            public string gstin { get; set; }
            public string ctjCd { get; set; }
            public string ctb { get; set; }
            public string stj { get; set; }
            public string dty { get; set; }
            public List<object> adadr { get; set; }
            public object frequencyType { get; set; }
            public string lgnm { get; set; }
            public List<string> nba { get; set; }
            public string rgdt { get; set; }
            public string stjCd { get; set; }
            public string tradeNam { get; set; }
            public string panNo { get; set; }
        }

        public class Root
        {
            public Compliance compliance { get; set; }
            public TaxpayerInfo taxpayerInfo { get; set; }
            public List<object> filing { get; set; }
        }

        #endregion
        protected void btrgetGstdetails_Click(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(tbxGstNumber.Text))
                {
                    //string postURL = "https://appyflow.in/api/verifyGST?gstNo=27BHLPS8412D1Z5&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                    string postURL = "https://appyflow.in/api/verifyGST?gstNo=" + tbxGstNumber.Text.Trim() + "&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                    string responseData = Invoke("GET", postURL, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        var myDeserializedClass = JsonConvert.DeserializeObject<Root>(responseData);

                        var panno = myDeserializedClass.taxpayerInfo.panNo;
                        var tradeNam = myDeserializedClass.taxpayerInfo.tradeNam;
                        var address = myDeserializedClass.taxpayerInfo.pradr.addr.bnm + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.lt + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.st + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.bno + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.dst + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.loc + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.pncd;

                        tbxName.Text = tradeNam;
                        tbxAddress.Text = address;
                        tbxpan.Text = panno;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void btnAddEmail_Click(object sender, EventArgs e)
        {
            using (PracticeEntities db = new PracticeEntities())
            {
                TM_tbl_Email customer  = new TM_tbl_Email();
                customer.Email = tbxBuyerEmail.Text.Trim();
                //customer.CustomerId = tbxe.Text;
            }
        }
    }
}