﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Data;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class CannedReportReviewerCA : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFilters();
                dlFilters.SelectedIndex = 0;
                dlFilters_SelectedIndexChanged(null, null);
            }
        }

        private void BindFilters()
        {
            try
            {
                dlFilters.DataSource = Enumerations.GetAll<CannedReportFilterForReviewer>();
                dlFilters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                CannedReportFilterForReviewer filter = (CannedReportFilterForReviewer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                grdComplianceTransactions.DataSource = GetCannedReportDataForReviewer(customerid,AuthenticationHelper.UserID, filter);
                grdComplianceTransactions.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<ComplianceInstanceTransactionView> GetCannedReportDataForReviewer(int Customerid, int userID, CannedReportFilterForReviewer filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var reviewerRoleIDs = (from row in entities.Roles
                                       where row.Code.StartsWith("RVW")
                                       select row.ID).ToList();


                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == Customerid
                                         select row).ToList();

                transactionsQuery = transactionsQuery.
                Where(entry => entry.UserID == userID && reviewerRoleIDs.Contains((int)entry.RoleID)
                && (entry.IsActive != false || entry.IsUpcomingNotDeleted != false)
                ).ToList();

            

                switch (filter)
                {
                    case CannedReportFilterForReviewer.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5).ToList();
                        break;
                    case CannedReportFilterForReviewer.Delayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 5).ToList();
                        break;
                    case CannedReportFilterForReviewer.Open:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 1).ToList();
                        break;
                    case CannedReportFilterForReviewer.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                    default:
                        break;
                }

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                    grdComplianceTransactions.PageIndex = e.NewPageIndex;
                    dlFilters_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DataTable GetGrid()
        {
            dlFilters_SelectedIndexChanged(null, null);
            return (grdComplianceTransactions.DataSource as List<ComplianceInstanceTransactionView>).ToDataTable();
        }
       
        public string GetFilter()
        {
            return Enumerations.GetEnumByID<CannedReportFilterForReviewer>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                CannedReportFilterForReviewer filter = (CannedReportFilterForReviewer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                var cannedReportReviewerList = GetCannedReportDataForReviewer(customerid,AuthenticationHelper.UserID, filter);
                if (direction == SortDirection.Ascending)
                {
                    cannedReportReviewerList = cannedReportReviewerList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    cannedReportReviewerList = cannedReportReviewerList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceTransactions.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                    }
                }

                grdComplianceTransactions.DataSource = cannedReportReviewerList;
                grdComplianceTransactions.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }


    }
}