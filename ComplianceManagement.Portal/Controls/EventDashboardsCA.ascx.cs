﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Configuration;
using System.Threading;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class EventDashboardsCA : System.Web.UI.UserControl
    {
        static int Type = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "CustomerBranchName";

               // Multiview1.ActiveViewIndex = 0;
                divOptionalCompliance.Visible = false;
                if (Page.RouteData.Values["filter"] != null)
                {
                    if (Page.RouteData.Values["filter"].ToString().Equals("AssignedEvents"))
                    {
                        if (Page.RouteData.Values["Type"].ToString().Equals("AssignedEvents"))
                        {
                            ViewState["Role"] = Page.RouteData.Values["Role"].ToString();
                            //hdnTitle.Value = Page.RouteData.Values["filter"].ToString() + " Compliances";
                            //lbltagLine.Text = Page.RouteData.Values["filter"].ToString() + " Compliances for " + Page.RouteData.Values["Role"].ToString();
                        }
                    }
                }
               
                //var AssignedRoles = ComplianceManagement.Business.ComplianceManagement.GetUserRoles(AuthenticationHelper.UserID);
                //int EventOwnerRoleID = 0;
                //int EventReviewerRoleID = 0;
                //foreach (var ar in AssignedRoles)
                //{
                //    string name = ar.Name;
                //    if (name.Equals("Event Owner"))
                //    {
                //        EventOwnerRoleID = 10;
                //    }
                //    if (name.Equals("Event Reviewer"))
                //    {
                //        EventReviewerRoleID = 11;
                //    }
                //}

                //if (EventOwnerRoleID == 10 && EventReviewerRoleID == 11)
                //{
                //    Session["EventRoleID"] = 10;
                //}
                //if (EventOwnerRoleID == 10 && EventReviewerRoleID == 0)
                //{
                //    Session["EventRoleID"] = 10;
                //}
                //else
                //{
                //    Session["EventRoleID"] = 11;
                //}
                Session["EventRoleID"] = 10;
                BindEventInstances(Convert.ToInt32(Session["EventRoleID"]));
            }
        }
        public void BindEventInstances(int eventRoleID,int pageIndex = 0, bool isBranchChanged = false)
        {
            try
            {
                try
                {
                    var dataSource = EventManagement.GetAllAssignedInstancesCount(eventRoleID ,- 1, AuthenticationHelper.UserID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));

                    if (ViewState["SortOrder"].ToString() == "Asc")
                    {
                        if (ViewState["SortExpression"].ToString() == "CustomerBranchName")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.CustomerBranchName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Name")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.Name).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "UserName")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.UserName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.Role).ToList();
                        }
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        if (ViewState["SortExpression"].ToString() == "CustomerBranchName")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.CustomerBranchName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Name")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.Name).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "UserName")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.UserName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.Role).ToList();
                        }
                    
                        direction = SortDirection.Ascending;
                    }
                    
                    grdEventList.PageIndex = pageIndex;
                    grdEventList.DataSource = dataSource;
                    grdEventList.DataBind();
                    upEventInstanceList.Update();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void grdEventList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var assignmentList = EventManagement.GetAllAssignedInstancesCount(10, -1, AuthenticationHelper.UserID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));
                //var assignmentList = EventManagement.GetAllAssignedInstancesCount(-1, AuthenticationHelper.UserID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdEventList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdEventList.Columns.IndexOf(field);
                    }
                }
                grdEventList.DataSource = assignmentList;
                grdEventList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindEventInstances(Convert.ToInt32(Session["EventRoleID"]),pageIndex: e.NewPageIndex);
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;
            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void grdEventList_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
                //grdEventList.EditIndex = e.NewEditIndex;
                //DateTime date = DateTime.Now;
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                //BindEventInstances(Convert.ToInt32(Session["EventRoleID"]));
        }

        protected void grdEventList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit"))
                {
                    divEventInstance.Visible = true;
                    grdEventList.EditIndex = Convert.ToInt32(e.CommandArgument);

                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    BindEventInstances(Convert.ToInt32(Session["EventRoleID"]));
                }
                if (e.CommandName.Equals("View"))
                {
                    //grdEventList.SelectedIndex = Convert.ToInt32(e.CommandArgument);
                    //int index = Convert.ToInt32(e.CommandArgument);

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int index = Convert.ToInt32(commandArgs[0]);
                    int CustomerBranchID = Convert.ToInt32(commandArgs[1]);

                    int eventId = Convert.ToInt32(grdEventList.DataKeys[index].Values[0]);
                    Session["eventId"] = eventId;
                    Session["CustomerBranchID"] = CustomerBranchID;
                    Response.Redirect("~/Event/EventDetailsCA.aspx", false);

                    //Session["eventId"]=eventId;
                    //int eventId = Convert.ToInt32(e.CommandArgument);
                    //lblEventName.Text = getEventName(eventId);
                    //BindEventComplianceDetails(eventId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdEventList_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {
            try
            {

                string dt = DateTime.Now.ToString("dd-MM-yyyy");
                string startdate = dt;
                string eventnature = Request[((TextBox)grdEventList.Rows[e.RowIndex].FindControl("txteventNature")).UniqueID].ToString();
                Session["startdate"] = null;
                Session["startdate"] = startdate;
                if (!string.IsNullOrEmpty(eventnature))
                {
                    long branch = Convert.ToInt64((grdEventList.Rows[e.RowIndex].FindControl("lblBranch") as Label).Text);
                    Session["Rbranch"] = null;
                    Session["Rbranch"] = branch;

                    Type = EventManagement.GetCompanyType(Convert.ToInt32(branch));

                    long assignmnetID = Convert.ToInt64((grdEventList.Rows[e.RowIndex].FindControl("lblAssignmnetId") as Label).Text);
                    Session["assignmnetID"] = null;
                    Session["assignmnetID"] = assignmnetID;
                    string branchName = (grdEventList.Rows[e.RowIndex].FindControl("lblBranchName") as Label).Text;
                    Session["RbranchName"] = null;
                    Session["RbranchName"] = branchName;
                    string eventname = (grdEventList.Rows[e.RowIndex].FindControl("lbleventName") as Label).Text;
                    Session["Reventname"] = null;
                    Session["Reventname"] = eventname;
                    int UserId = Convert.ToInt32((grdEventList.Rows[e.RowIndex].FindControl("lblUserID") as Label).Text);
                    Session["RUserId"] = null;
                    Session["RUserId"] = UserId;
                    long eventId = Convert.ToInt64(grdEventList.DataKeys[e.RowIndex].Value);
                    Session["eventId"] = null;
                    Session["eventId"] = eventId;

                    Session["EventInstanceID"] = null;
                    Session["EventInstanceID"] = Convert.ToInt64((grdEventList.Rows[e.RowIndex].FindControl("lblEventInstanceID") as Label).Text);

                    #region Email for Assign compliance
                    //var exceptComplianceIDs = EventManagement.CheckAllEventCompliance(eventId, branch);
                    //if (exceptComplianceIDs.Count > 0)
                    //{
                    //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Associated compliances of this event are not assigned to location. Please assign compliances first and then update event held date.')", true);
                    //    int customerID = -1;
                    //    string ReplyEmailAddressName = "";
                    //    if (AuthenticationHelper.Role.Equals("SADMN"))
                    //        ReplyEmailAddressName = "Avantis";
                    //    else
                    //    {
                    //        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    //        ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                    //    }
                    //    new Thread(() =>
                    //    {
                    //        User user = UserManagement.GetByID(UserId);
                    //        List<User> emailsId = UserManagement.GetAdminUsers(CustomerBranchManagement.GetByID(branch).CustomerID);
                    //        emailsId.Add(user);
                    //        foreach (var us in emailsId)
                    //        {
                    //            string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                    //                             + "Dear " + us.FirstName + " " + us.LastName + ",<br /><br />"
                    //                             + "Associated compliance of " + "'" + eventname + " have not been assigned to " + branchName + "'.<br> Please assign the compliance and update your event held on date.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                    //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { us.Email }), null, null, "AVACOM Alert :: Missing compliances.", message);
                    //        }
                    //    }).Start();
                    //}
                    //else
                    //{
                    #endregion

                    BindParentEventData(Convert.ToInt32(eventId));

                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
     
                    //Add Code 30 Sept 2016

                    var exceptComplianceIDs = EventManagement.CheckAllEventCompliance(Convert.ToInt32(Session["eventId"].ToString()), Convert.ToInt32(Session["Rbranch"].ToString()));

                    if (exceptComplianceIDs.Count > 0)
                    {
                        grdNoAsignedComplinace.DataSource = EventManagement.GetAllNoAssignedComplinceList(exceptComplianceIDs);
                        grdNoAsignedComplinace.DataBind();
                        upNotAssignedCompliances.Update();
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divNotAssignedCompliances", "$(\"#divNotAssignedCompliances\").dialog('open')", true);
                        #region Send Email for assign Complaice 

                        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Associated compliances of this event are not assigned. Please assign compliances first and then update event held date.')", true);
                        //int customerID = -1;
                        //string ReplyEmailAddressName = "";
                        //if (AuthenticationHelper.Role.Equals("SADMN"))
                        //    ReplyEmailAddressName = "Avantis";
                        //else
                        //{
                        //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                        //    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                        //}
                        //new Thread(() =>
                        //{
                        //    User user = UserManagement.GetByID(Convert.ToInt32(Session["RUserId"].ToString()));
                        //    List<User> emailsId = UserManagement.GetAdminUsers(CustomerBranchManagement.GetByID(Convert.ToInt64(Session["Rbranch"].ToString())).CustomerID);
                        //    emailsId.Add(user);
                        //    foreach (var us in emailsId)
                        //    {
                        //        string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                        //                         + "Dear " + us.FirstName + " " + us.LastName + ",<br /><br />"
                        //                         + "Associated compliance of " + "'" + Session["Reventname"].ToString() + " have not been assigned to " + Session["RbranchName"].ToString() + "'.<br> Please assign the compliance and update your event held on date.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                        //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { us.Email }), null, null, "AVACOM Alert :: Missing compliances.", message);
                        //    }
                        //}).Start();
                        #endregion
                    }
                    else
                    {
                        grdNoAsignedComplinace.DataSource = null;
                        grdNoAsignedComplinace.DataBind();
                        upOptionalCompliances.Update();

                        foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                        {
                            int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                            Session["ParentEventID"] = ParentEventID;
                            GridView gvEvenToCompliance = Eventrow.FindControl("gvParentToComplianceGrid") as GridView;
                            //ParentEvent -> compliance
                            foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                            {
                                int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[1].Text);
                                ActualCheckedOptionalCompliances.Add(ComplinaceID);
                            }

                            //ParentEvent -> SubEvent-> compliance
                            GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                            foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                            {
                                int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                GridView gvSubCompliance = SubEventrow.FindControl("gvComplianceGrid") as GridView;
                                foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                                {
                                    int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                                    ActualCheckedOptionalCompliances.Add(ComplinaceID);
                                }
                            }

                            //Intermediatre-> Subevent-> Compliance
                            GridView gvIntermediateEvent = Eventrow.FindControl("gvIntermediateGrid") as GridView;
                            foreach (GridViewRow intermediateEventrow in gvIntermediateEvent.Rows)
                            {
                                int IntermediateEventID = Convert.ToInt32(intermediateEventrow.Cells[1].Text);
                                GridView gvIntermediateSubEvent = intermediateEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                foreach (GridViewRow SubEventrow in gvIntermediateSubEvent.Rows)
                                {
                                    int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                    GridView gvSubCompliance = SubEventrow.FindControl("gvIntermediateComplainceGrid") as GridView;
                                    foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                                    {
                                        int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                                        ActualCheckedOptionalCompliances.Add(ComplinaceID);
                                    }
                                }
                            }
                        }

                        if (Session["startdate"] != null && Session["assignmnetID"] != null && Session["eventId"] != null)
                        {
                            DateTime Date = DateTime.ParseExact(Session["startdate"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            DateTime NextDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                EventScheduleOn eventScheduleon = new EventScheduleOn();
                                eventScheduleon.EventInstanceID = Convert.ToInt32(Session["EventInstanceID"]);
                                eventScheduleon.StartDate = Date;
                                eventScheduleon.EndDate = Date;
                                eventScheduleon.ClosedDate = Date;
                                eventScheduleon.HeldOn = Date;
                                eventScheduleon.Period = "0";
                                eventScheduleon.CreatedBy = AuthenticationHelper.UserID;
                                eventScheduleon.CreatedOn = DateTime.Now;
                                eventScheduleon.Description = eventnature; // txtDescription.Text;
                                eventScheduleon.IsDeleted = false;
                                entities.EventScheduleOns.Add(eventScheduleon);
                                entities.SaveChanges();

                                long eventAssignmentID = Business.EventManagement.GetEventAssignmentID(Convert.ToInt64(eventScheduleon.EventInstanceID));

                                Business.EventManagement.CreateEventReminders(Convert.ToInt64(Session["ParentEventID"]), Convert.ToInt64(Session["EventInstanceID"]), Convert.ToInt64(eventAssignmentID), AuthenticationHelper.UserID, Convert.ToDateTime(Date));

                                foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                                {
                                    int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);

                                    //Parent
                                    EventAssignDate eventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                        IntermediateEventID = 0,
                                        SubEventID = 0,
                                        Date = Date,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(Session["userID"]),
                                        Type = Type,
                                    };
                                    Business.EventManagement.CreateEventAssignDate(eventAssignDate);

                                    // Subevent
                                    GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                                    foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                                    {
                                        int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                        EventAssignDate subeventAssignDate = new EventAssignDate()
                                        {
                                            ParentEventID = ParentEventID,
                                            EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                            IntermediateEventID = 0,
                                            SubEventID = SubEventID,
                                            Date = NextDate,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(Session["userID"]),
                                            Type = Type,
                                        };
                                        Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                    }

                                    //Intermediatre-> Subevent-> Compliance
                                    GridView gvIntermediateEvent = Eventrow.FindControl("gvIntermediateGrid") as GridView;
                                    foreach (GridViewRow intermediateEventrow in gvIntermediateEvent.Rows)
                                    {
                                        int IntermediateEventID = Convert.ToInt32(intermediateEventrow.Cells[1].Text);
                                        EventAssignDate IntermediatesubeventAssignDate = new EventAssignDate()
                                        {
                                            ParentEventID = ParentEventID,
                                            EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                            IntermediateEventID = IntermediateEventID,
                                            SubEventID = 0,
                                            Date = NextDate,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(Session["userID"]),
                                            Type = Type,
                                        };
                                        Business.EventManagement.CreateEventAssignDate(IntermediatesubeventAssignDate);

                                        GridView gvIntermediateSubEvent = intermediateEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                        foreach (GridViewRow SubEventrow in gvIntermediateSubEvent.Rows)
                                        {
                                            int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                            EventAssignDate subeventAssignDate = new EventAssignDate()
                                            {
                                                ParentEventID = ParentEventID,
                                                EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                                IntermediateEventID = IntermediateEventID,
                                                SubEventID = SubEventID,
                                                Date = NextDate,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                                Type = Type,
                                            };
                                            Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                        }
                                    }
                                }
                            }

                            //EventManagement.EventScheduledOnUpdateNew(txtDescription.Text, DateTime.ParseExact(Convert.ToString(Session["startdate"]), "dd-MM-yyyy", CultureInfo.InvariantCulture), Convert.ToInt64(Session["EventInstanceID"]), Convert.ToInt64(Session["assignmnetID"]), Convert.ToInt64(Session["eventId"]), AuthenticationHelper.UserID);
                           // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divOptionalCompliances", "$(\"#divOptionalCompliances\").dialog('close')", true);
                        }
                        grdEventList.EditIndex = -1;
                        BindEventInstances(Convert.ToInt32(Session["EventRoleID"]));
                    }

                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Event activated successfully')", true);
                    ////////////////////////////////////////////////
                   //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divOptionalCompliances", "$(\"#divOptionalCompliances\").dialog('open')", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Please enter event nature.')", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day), true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        List<long> CheckedMandatoryCompliances = new List<long>();
        List<long> UnCheckedOptionalCompliances = new List<long>();
        List<long> ActualCheckedOptionalCompliances = new List<long>();

        List<long> ComplianceList = new List<long>();

        private void BindParentEventData(int ParentEventID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var ParentEvent = entities.SP_GetParentEventSelected(ParentEventID, Type).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();
                upOptionalCompliances.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private string getEventName(int EventID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var eventName = (from row in entities.Events
                           where row.ID == EventID
                           select row).SingleOrDefault();

                return eventName.Name;
                // UpdatePanel1.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return "";
        }
        protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int ParentEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                ComplianceDBEntities entities = new ComplianceDBEntities();
                GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
                var compliance = entities.SP_GetParentEventToComplianceAssignDays(ParentEventID, Type).ToList();
                gv1.DataSource = compliance;
                gv1.DataBind();

                GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
                string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var SubEvent = entities.SP_GetSubEvent(ParentEventID, type, Type).ToList();
                gv.DataSource = SubEvent;
                gv.DataBind();

                GridView gv2 = (GridView)e.Row.FindControl("gvIntermediateGrid");
                string type2 = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var Intermediate = entities.SP_GetIntermediateEvent(ParentEventID, Type).ToList();
                gv2.DataSource = Intermediate;
                gv2.DataBind();
            }
        }

        protected void gvIntermediateSubEventGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateComplainceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int intermediateEventID = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    int parentEventID = Convert.ToInt32(Session["eventId"]);

                    var Compliance = entities.SP_GetIntermediateComplianceAssignDays(parentEventID, intermediateEventID, SubEventID, Type).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gvIntermediateGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView gv = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
                int IntermediateEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                GridView childGrid1 = (GridView)sender;
                int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var Compliance = entities.SP_GetIntermediateSubEvent(IntermediateEventID, Parentid, Type).ToList();
                gv.DataSource = Compliance;
                gv.DataBind();
            }
        }
      
        protected void gvChildGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView gv = (GridView)e.Row.FindControl("gvComplianceGrid");
                int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);

                GridView childGrid1 = (GridView)sender;
                //Retreiving the GridView DataKey Value
                int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                //int Parentid = Convert.ToInt32(gv.DataKeys[e.Row.RowIndex].Values[1]);
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var Compliance = entities.SP_GetComplianceWithDays(Parentid, SubEventID, Type).ToList();
                gv.DataSource = Compliance;
                gv.DataBind();
            }
        }

        protected void grdEventList_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
        {
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            grdEventList.EditIndex = -1;
            BindEventInstances(Convert.ToInt32(Session["EventRoleID"]));
        }
        protected bool visibleEdit(long roleID)
        {
            try
            {
                bool result = true;

                //if (roleID == 11)
                //{
                //    result = false;
                //}
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnOptionalComplianceSave_Click(object sender, EventArgs e)
        {
            try
            {

                var exceptComplianceIDs = EventManagement.CheckAllEventCompliance(Convert.ToInt32(Session["eventId"].ToString()), Convert.ToInt32(Session["Rbranch"].ToString()));
      
                if (exceptComplianceIDs.Count > 0)
                {
                    grdNoAsignedComplinace.DataSource = EventManagement.GetAllNoAssignedComplinceList(exceptComplianceIDs);
                    grdNoAsignedComplinace.DataBind();
                    upNotAssignedCompliances.Update();
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divNotAssignedCompliances", "$(\"#divNotAssignedCompliances\").dialog('open')", true);

                    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Associated compliances of this event are not assigned. Please assign compliances first and then update event held date.')", true);
                    //int customerID = -1;
                    //string ReplyEmailAddressName = "";
                    //if (AuthenticationHelper.Role.Equals("SADMN"))
                    //    ReplyEmailAddressName = "Avantis";
                    //else
                    //{
                    //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    //    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                    //}
                    //new Thread(() =>
                    //{
                    //    User user = UserManagement.GetByID(Convert.ToInt32(Session["RUserId"].ToString()));
                    //    List<User> emailsId = UserManagement.GetAdminUsers(CustomerBranchManagement.GetByID(Convert.ToInt64(Session["Rbranch"].ToString())).CustomerID);
                    //    emailsId.Add(user);
                    //    foreach (var us in emailsId)
                    //    {
                    //        string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                    //                         + "Dear " + us.FirstName + " " + us.LastName + ",<br /><br />"
                    //                         + "Associated compliance of " + "'" + Session["Reventname"].ToString() + " have not been assigned to " + Session["RbranchName"].ToString() + "'.<br> Please assign the compliance and update your event held on date.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                    //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { us.Email }), null, null, "AVACOM Alert :: Missing compliances.", message);
                    //    }
                    //}).Start();
                }
                else
                {
                     grdNoAsignedComplinace .DataSource = null;
                     grdNoAsignedComplinace.DataBind();
                     upOptionalCompliances.Update();

                    foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                    {
                        int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                        Session["ParentEventID"] = ParentEventID;
                        GridView gvEvenToCompliance = Eventrow.FindControl("gvParentToComplianceGrid") as GridView;
                        //ParentEvent -> compliance
                        foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                        {
                            int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[1].Text);
                            ActualCheckedOptionalCompliances.Add(ComplinaceID);
                        }

                        //ParentEvent -> SubEvent-> compliance
                        GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                        foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                        {
                            int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                            GridView gvSubCompliance = SubEventrow.FindControl("gvComplianceGrid") as GridView;
                            foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                            {
                                int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                                ActualCheckedOptionalCompliances.Add(ComplinaceID);
                            }
                        }

                        //Intermediatre-> Subevent-> Compliance
                        GridView gvIntermediateEvent = Eventrow.FindControl("gvIntermediateGrid") as GridView;
                        foreach (GridViewRow intermediateEventrow in gvIntermediateEvent.Rows)
                        {
                            int IntermediateEventID = Convert.ToInt32(intermediateEventrow.Cells[1].Text);
                            GridView gvIntermediateSubEvent = intermediateEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                            foreach (GridViewRow SubEventrow in gvIntermediateSubEvent.Rows)
                            {
                                int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                GridView gvSubCompliance = SubEventrow.FindControl("gvIntermediateComplainceGrid") as GridView;
                                foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                                {
                                    int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                                    ActualCheckedOptionalCompliances.Add(ComplinaceID);
                                }
                            }
                        }
                    }

                    if (Session["startdate"] != null  && Session["assignmnetID"] != null && Session["eventId"] != null)
                    {
                        DateTime Date = DateTime.ParseExact(Session["startdate"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                       DateTime NextDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture); 

                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            EventScheduleOn eventScheduleon = new EventScheduleOn();
                            eventScheduleon.EventInstanceID = Convert.ToInt32(Session["EventInstanceID"]);
                            eventScheduleon.StartDate = Date;
                            eventScheduleon.EndDate = Date;
                            eventScheduleon.ClosedDate = Date;
                            eventScheduleon.HeldOn = Date;
                            eventScheduleon.Period = "0";
                            eventScheduleon.CreatedBy = AuthenticationHelper.UserID;
                            eventScheduleon.CreatedOn = DateTime.Now;
                            eventScheduleon.Description = txtDescription.Text;
                            eventScheduleon.IsDeleted = false;
                            entities.EventScheduleOns.Add(eventScheduleon);
                            entities.SaveChanges();

                            long eventAssignmentID = Business.EventManagement.GetEventAssignmentID(Convert.ToInt64(eventScheduleon.EventInstanceID));

                            Business.EventManagement.CreateEventReminders(Convert.ToInt64(Session["ParentEventID"]), Convert.ToInt64(Session["EventInstanceID"]), Convert.ToInt64(eventAssignmentID), AuthenticationHelper.UserID, Convert.ToDateTime(Date));

                            foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                            {
                                int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);

                                //Parent
                                EventAssignDate eventAssignDate = new EventAssignDate()
                                {
                                    ParentEventID = ParentEventID,
                                    EventScheduleOnID =Convert.ToInt32(eventScheduleon.ID),
                                    IntermediateEventID = 0,
                                    SubEventID = 0,
                                    Date = Date,
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    Type = Type,
                                };
                                Business.EventManagement.CreateEventAssignDate(eventAssignDate);

                                // Subevent
                                GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                                foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                                {
                                    int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                    EventAssignDate subeventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                        IntermediateEventID = 0,
                                        SubEventID = SubEventID,
                                        Date = NextDate,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(Session["userID"]),
                                        Type = Type,
                                    };
                                    Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                }

                                //Intermediatre-> Subevent-> Compliance
                                GridView gvIntermediateEvent = Eventrow.FindControl("gvIntermediateGrid") as GridView;
                                foreach (GridViewRow intermediateEventrow in gvIntermediateEvent.Rows)
                                {
                                    int IntermediateEventID = Convert.ToInt32(intermediateEventrow.Cells[1].Text);
                                    EventAssignDate IntermediatesubeventAssignDate = new EventAssignDate()
                                    {
                                        ParentEventID = ParentEventID,
                                        EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                        IntermediateEventID = IntermediateEventID,
                                        SubEventID = 0,
                                        Date = NextDate,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = Convert.ToInt32(Session["userID"]),
                                        Type = Type,
                                    };
                                    Business.EventManagement.CreateEventAssignDate(IntermediatesubeventAssignDate);

                                    GridView gvIntermediateSubEvent = intermediateEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;
                                     foreach (GridViewRow SubEventrow in gvIntermediateSubEvent.Rows)
                                    {
                                        int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                                        EventAssignDate subeventAssignDate = new EventAssignDate()
                                        {
                                            ParentEventID = ParentEventID,
                                            EventScheduleOnID = Convert.ToInt32(eventScheduleon.ID),
                                            IntermediateEventID = IntermediateEventID,
                                            SubEventID = SubEventID,
                                            Date = NextDate,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = Convert.ToInt32(Session["userID"]),
                                            Type = Type,
                                        };
                                        Business.EventManagement.CreateEventAssignDate(subeventAssignDate);
                                    }
                                }
                            }
                        }

                        //EventManagement.EventScheduledOnUpdateNew(txtDescription.Text, DateTime.ParseExact(Convert.ToString(Session["startdate"]), "dd-MM-yyyy", CultureInfo.InvariantCulture), Convert.ToInt64(Session["EventInstanceID"]), Convert.ToInt64(Session["assignmnetID"]), Convert.ToInt64(Session["eventId"]), AuthenticationHelper.UserID);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divOptionalCompliances", "$(\"#divOptionalCompliances\").dialog('close')", true);
                    }
                    grdEventList.EditIndex = -1;
                    BindEventInstances(Convert.ToInt32(Session["EventRoleID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divOptionalCompliances", "$(\"#divOptionalCompliances\").dialog('close')", true);
                grdEventList.EditIndex = -1;
                BindEventInstances(Convert.ToInt32(Session["EventRoleID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDvNotAssignedClose_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divNotAssignedCompliances", "$(\"#divNotAssignedCompliances\").dialog('close')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnBackMyEvent_Click(object sender, EventArgs e)
        {
            try
            {
                Session["MyEventBack"] = "true";
                Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      
        //private void BindLocation(int eventID)
        //{
        //    try
        //    {
        //        var subEvents = EventManagement.GetAllHierarchyRahul(eventID);
        //        foreach (var item in subEvents)
        //        {
        //            TreeNode node = new TreeNode(item.Name, item.ID.ToString());
        //            node.SelectAction = TreeNodeSelectAction.Expand;
        //            BindBranchesHierarchy(node, item);
        //            tvEvent.Nodes.Add(node);
        //        }
        //        tvEvent.ExpandAll();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        //private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        //{
        //    try
        //    {
        //        foreach (var item in nvp.Children)
        //        {
        //            TreeNode node = new TreeNode(item.Name, item.ID.ToString());
        //            BindBranchesHierarchy(node, item);
        //            parent.ChildNodes.Add(node);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //protected void tvEvent_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        //{
        //    foreach (TreeNode node in tvEvent.Nodes)
        //    {
        //        if (node.Checked == false)
        //        {
        //            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divOptionalCompliances1", "$(\"#divOptionalCompliances1\").dialog('close')", true);
        //        }

        //    }
        //}
    }
}