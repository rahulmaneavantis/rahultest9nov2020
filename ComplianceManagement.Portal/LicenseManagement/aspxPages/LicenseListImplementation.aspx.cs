﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web;
using System.IO;
using Ionic.Zip;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages
{
    public partial class LicenseListImplementation : System.Web.UI.Page
    {
        protected bool flag;
        protected static List<Int32> roles;
        protected static string ClickChangeflag = "P";
        protected static string user_Roles;
        public static string LicenseDocPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        string status = Request.QueryString["Status"].ToString().Trim();
                        ddlLicenseStatus.SelectedValue = status;
                    }
                    else
                    {
                        ddlLicenseStatus.SelectedValue = "Status";
                    }
                    string isstatutoryinternal = "S";
                    if (!String.IsNullOrEmpty(Request.QueryString["ISI"]))
                    {
                        isstatutoryinternal = Request.QueryString["ISI"].ToString().Trim();                       
                    }                   
                    if (isstatutoryinternal == "S")
                    {
                        ddlfilterStatutoryNonStatutory.SelectedValue = "S";
                    }
                    else if (isstatutoryinternal == "I")
                    {
                        ddlfilterStatutoryNonStatutory.SelectedValue = "I";
                    }
                    user_Roles = AuthenticationHelper.Role;
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    BindCustomerFilter();
                    BindLocationFilter();
                    BindLicenseType();
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCustomerFilter()
        {
            List<Customer> lstCustomers = CustomerManagement.GetAll(-1, null);

            if (lstCustomers.Count > 0)
                lstCustomers = lstCustomers.OrderBy(entry => entry.Name).ToList();

            ddlCustomer.DataTextField = "Name";
            ddlCustomer.DataValueField = "ID";
            ddlCustomer.DataSource = lstCustomers;
            ddlCustomer.DataBind();
        }
        public void BindLicenseType()
        {
            List<Sp_BindLicenseType_Result> data = new List<Sp_BindLicenseType_Result>();
            string isstatutoryinternal = "S";
            if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
            {
                isstatutoryinternal = "S";
            }
            else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
            {
                isstatutoryinternal = "I";
            }
            if (user_Roles.Contains("CADMN") || user_Roles.Contains("IMPT"))
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);               
            }
            else
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);
            }
            //Page DropDown
            ddlLicenseType.DataTextField = "Name";
            ddlLicenseType.DataValueField = "ID";
            ddlLicenseType.DataSource = data;
            ddlLicenseType.DataBind();
            ddlLicenseType.Items.Insert(0, new ListItem("License Type Name", "-1"));
        }
        private void BindLocationFilter()
        {
            try
            {
                tvFilterLocation.Nodes.Clear();
                int CustomerID = 0;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(CustomerID));
                var LocationList = new List<int>();
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "All";
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationFilterPopup()
        {
            try
            {
                tvFilterLocationPopup.Nodes.Clear();
                int CustomerID = 0;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(CustomerID));
                var LocationList = new List<int>();
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocationPopup.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item);
                    tvFilterLocationPopup.Nodes.Add(node);
                }
                tvFilterLocationPopup.CollapseAll();
                tbxFilterLocationPopup.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocationPopup.SelectedNode.Text : "All";
                tvFilterLocationPopup_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocationPopup_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocationPopup.Text = tvFilterLocationPopup.SelectedNode != null ? tvFilterLocationPopup.SelectedNode.Text : "All";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();
                NameValueHierarchy branch = null;            
                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }
                tbxFilterLocation.Text = "Select Entity/Location";
                TreeNode node = new TreeNode("All", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);
                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }
                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlLicenseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter1", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlLicenseStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter2", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlFilterCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter();
            BindGrid();
        }
        public void BindDepartments()
        {
            var lstDepts = CompDeptManagement.GetAllDepartmentMasterList(0);
            if (lstDepts.Count > 0)
                lstDepts = lstDepts.OrderBy(row => row.Name).ToList();
            ddlDepartment.DataTextField = "Name";
            ddlDepartment.DataValueField = "ID";
            ddlDepartment.DataSource = lstDepts;
            ddlDepartment.DataBind();
        }
        public void BindGrid()
        {
            try
            {
                int CustomerID = 0;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                int branchID = -1;
                int deptID = -1;
                string licenseStatus = string.Empty;
                long licenseTypeID = -1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (ddlLicenseStatus.SelectedValue != "Status")
                {
                    licenseStatus = ddlLicenseStatus.SelectedValue;
                }
                if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                {
                    licenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                }
                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(CustomerID), branchID);
                List<Lic_SP_MyWorkspaceDetail_Result> MasterTransction = new List<Lic_SP_MyWorkspaceDetail_Result>();
                string isstatutoryinternal = "S";
                if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }

                MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(CustomerID), AuthenticationHelper.UserID,
                  branchList, deptID, licenseStatus, licenseTypeID, "IMPT", isstatutoryinternal);
                MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();


                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);
                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            MasterTransction = MasterTransction.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            MasterTransction = MasterTransction.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                Session["TotalRows"] = null;
                if (MasterTransction.Count > 0)
                {
                    flag = true;
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = MasterTransction.Count;
                }
                else
                {
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = null;
                }
                MasterTransction.Clear();
                MasterTransction = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }          
        protected void grdLicenseList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    string isstatutoryinternal = "S";
                    if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
                    {
                        isstatutoryinternal = "I";
                    }

                    clearLicenseControls();
                    BindDepartments();
                    BindLocationFilterPopup();
                    long licenseID = 0;
                    long complianceinstanceid = 0;
                    long compliancescheduleonid = 0;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (!string.IsNullOrEmpty(Convert.ToString(commandArgs[0])))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(commandArgs[1])))
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(commandArgs[2])))
                            {
                                if (isstatutoryinternal == "S")
                                {
                                    #region 
                                    licenseID = Convert.ToInt32(commandArgs[0]);
                                    complianceinstanceid = Convert.ToInt32(commandArgs[1]);
                                    compliancescheduleonid = Convert.ToInt32(commandArgs[2]);

                                    ViewState["LicenseID"] = licenseID;
                                    ViewState["complianceinstanceid"] = complianceinstanceid;
                                    ViewState["compliancescheduleonid"] = compliancescheduleonid;
                                    if (licenseID != 0)
                                    {
                                        var LicensedocumentVersionData = LicenseDocumentManagement.GetFileData(licenseID, -1).Select(x => new
                                        {
                                            ID = x.ID,
                                            ScheduledOnID = x.ScheduledOnID,
                                            LicenseID = x.LicenseID,
                                            FileID = x.FileID,
                                            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                            VersionDate = x.VersionDate,
                                            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                                        }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();


                                        rptLicenseVersion.DataSource = LicensedocumentVersionData;
                                        rptLicenseVersion.DataBind();

                                        rptLicenseDocumnets.DataSource = null;
                                        rptLicenseDocumnets.DataBind();


                                        var licenseRecord = LicenseMgmt.GetLicenseByID(licenseID);
                                        if (licenseRecord != null)
                                        {
                                            if (licenseRecord.IsStatutory == true)
                                                ddlLicenseTypePopup.SelectedItem.Value = "1";
                                            else
                                                ddlLicenseTypePopup.SelectedItem.Value = "0";

                                            if (Convert.ToString(licenseRecord.LicenseTypeID) != null)
                                                ddlLicenseTypePopup.SelectedItem.Value = Convert.ToString(licenseRecord.LicenseTypeID);

                                            tbxLicenseNo.Text = licenseRecord.LicenseNo;
                                            tbxLicenseTitle.Text = licenseRecord.LicenseTitle;

                                            if (licenseRecord.CustomerBranchID != 0)
                                            {
                                                foreach (TreeNode node in tvFilterLocationPopup.Nodes)
                                                {
                                                    if (node.Value == licenseRecord.CustomerBranchID.ToString())
                                                    {
                                                        node.Selected = true;
                                                    }
                                                    foreach (TreeNode item1 in node.ChildNodes)
                                                    {
                                                        if (item1.Value == licenseRecord.CustomerBranchID.ToString())
                                                            item1.Selected = true;
                                                    }
                                                }
                                            }

                                            tvFilterLocationPopup_SelectedNodeChanged(null, null);

                                            ddlDepartment.ClearSelection();

                                            if (licenseRecord.DepartmentID != null)
                                                ddlDepartment.SelectedItem.Value = Convert.ToString(licenseRecord.DepartmentID);


                                            if (licenseRecord.StartDate != null)
                                                tbxStartDate.Text = Convert.ToDateTime(licenseRecord.StartDate).ToString("dd-MM-yyyy");
                                            else
                                                tbxStartDate.Text = string.Empty;

                                            if (licenseRecord.EndDate != null)
                                                tbxEndDate.Text = Convert.ToDateTime(licenseRecord.EndDate).ToString("dd-MM-yyyy");
                                            else
                                                tbxEndDate.Text = string.Empty;


                                            if (licenseRecord.EndDate != null && licenseRecord.RemindBeforeNoOfDays > 0)
                                                tbxApplicationdays.Text = Convert.ToString(licenseRecord.RemindBeforeNoOfDays);
                                            else
                                                tbxApplicationdays.Text = string.Empty;

                                        }
                                        upLicense.Update();
                                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divLicenseDialog\").dialog('open')", true);
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPopup');", tbxFilterLocationPopup.ClientID), true);
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter3", "$(\"#divFilterLocationPopup\").hide(\"blind\", null, 500, function () { });", true);
                                    }
                                    #endregion
                                }
                                else if (isstatutoryinternal == "I")
                                {
                                    #region 
                                    licenseID = Convert.ToInt32(commandArgs[0]);
                                    complianceinstanceid = Convert.ToInt32(commandArgs[1]);
                                    compliancescheduleonid = Convert.ToInt32(commandArgs[2]);

                                    ViewState["LicenseID"] = licenseID;
                                    ViewState["complianceinstanceid"] = complianceinstanceid;
                                    ViewState["compliancescheduleonid"] = compliancescheduleonid;
                                    if (licenseID != 0)
                                    {
                                        var LicensedocumentVersionData = InternalLicenseMgmt.GetInternalFileData(licenseID, -1).Select(x => new
                                        {
                                            ID = x.ID,
                                            ScheduledOnID = x.ScheduledOnID,
                                            LicenseID = x.LicenseID,
                                            FileID = x.FileID,
                                            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                            VersionDate = x.VersionDate,
                                            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                                        }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();


                                        rptLicenseVersion.DataSource = LicensedocumentVersionData;
                                        rptLicenseVersion.DataBind();

                                        rptLicenseDocumnets.DataSource = null;
                                        rptLicenseDocumnets.DataBind();


                                        var licenseRecord = InternalLicenseMgmt.GetInternalLicenseByID(licenseID);
                                        if (licenseRecord != null)
                                        {
                                            if (licenseRecord.IsStatutory == true)
                                                ddlLicenseTypePopup.SelectedItem.Value = "1";
                                            else
                                                ddlLicenseTypePopup.SelectedItem.Value = "0";

                                            if (Convert.ToString(licenseRecord.LicenseTypeID) != null)
                                                ddlLicenseTypePopup.SelectedItem.Value = Convert.ToString(licenseRecord.LicenseTypeID);

                                            tbxLicenseNo.Text = licenseRecord.LicenseNo;
                                            tbxLicenseTitle.Text = licenseRecord.LicenseTitle;

                                            if (licenseRecord.CustomerBranchID != 0)
                                            {
                                                foreach (TreeNode node in tvFilterLocationPopup.Nodes)
                                                {
                                                    if (node.Value == licenseRecord.CustomerBranchID.ToString())
                                                    {
                                                        node.Selected = true;
                                                    }
                                                    foreach (TreeNode item1 in node.ChildNodes)
                                                    {
                                                        if (item1.Value == licenseRecord.CustomerBranchID.ToString())
                                                            item1.Selected = true;
                                                    }
                                                }
                                            }

                                            tvFilterLocationPopup_SelectedNodeChanged(null, null);

                                            ddlDepartment.ClearSelection();

                                            if (licenseRecord.DepartmentID != null)
                                                ddlDepartment.SelectedItem.Value = Convert.ToString(licenseRecord.DepartmentID);


                                            if (licenseRecord.StartDate != null)
                                                tbxStartDate.Text = Convert.ToDateTime(licenseRecord.StartDate).ToString("dd-MM-yyyy");
                                            else
                                                tbxStartDate.Text = string.Empty;

                                            if (licenseRecord.EndDate != null)
                                                tbxEndDate.Text = Convert.ToDateTime(licenseRecord.EndDate).ToString("dd-MM-yyyy");
                                            else
                                                tbxEndDate.Text = string.Empty;


                                            if (licenseRecord.EndDate != null && licenseRecord.RemindBeforeNoOfDays > 0)
                                                tbxApplicationdays.Text = Convert.ToString(licenseRecord.RemindBeforeNoOfDays);
                                            else
                                                tbxApplicationdays.Text = string.Empty;

                                        }
                                        upLicense.Update();
                                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divLicenseDialog\").dialog('open')", true);
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPopup');", tbxFilterLocationPopup.ClientID), true);
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter3", "$(\"#divFilterLocationPopup\").hide(\"blind\", null, 500, function () { });", true);
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
                else if (e.CommandName.Equals("DELETE_License"))
                {
                    int licenseId = Convert.ToInt32(e.CommandArgument);
                    string isstatutoryinternal = "S";
                    if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
                    {
                        isstatutoryinternal = "I";
                    }
                    if (isstatutoryinternal == "S")
                    {
                        if (!CheckLicenseransactionExists(licenseId))
                        {
                            Delete(licenseId, AuthenticationHelper.UserID);
                            BindGrid();
                        }
                        else
                        {
                            cvDuplicateEntryTop.IsValid = false;
                            cvDuplicateEntryTop.ErrorMessage = "License can not be deleted ,because it is tagged to compliances which are already performed.";
                        }
                    }
                    else if (isstatutoryinternal == "I")
                    {
                        if (!CheckInternalLicenseransactionExists(licenseId))
                        {
                            DeleteInternal(licenseId, AuthenticationHelper.UserID);
                            BindGrid();
                        }
                        else
                        {
                            cvDuplicateEntryTop.IsValid = false;
                            cvDuplicateEntryTop.ErrorMessage = "License can not be deleted ,because it is tagged to compliances which are already performed.";
                        }                        
                    }                       
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdLicenseList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdLicenseList.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void DeleteInternal(int licenseId, long updatedbyID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ltlmToUpdate = (from row in entities.Lic_tbl_InternalLicenseInstance
                                    where row.ID == licenseId
                                    select row).FirstOrDefault();



                ltlmToUpdate.UpdatedBy = updatedbyID;
                ltlmToUpdate.IsDeleted = true;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static void Delete(int licenseId, long updatedbyID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ltlmToUpdate = (from row in entities.Lic_tbl_LicenseInstance
                                                           where row.ID == licenseId
                                                           select row).FirstOrDefault();

                

                ltlmToUpdate.UpdatedBy = updatedbyID;
                ltlmToUpdate.IsDeleted = true;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static bool CheckLicenseransactionExists(long LicenseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.LIC_Sp_CheckLicenseISAssigned(LicenseID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckInternalLicenseransactionExists(long LicenseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                var query = (from row in entities.LIC_Sp_CheckInternalComplianceISAssigned(LicenseID)
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public void clearLicenseControls()
        {
            try
            {
                tbxLicenseNo.Text = "";
                tbxLicenseTitle.Text = "";
                tbxFilterLocationPopup.Text = "";
                tbxFilterLocationPopup.Text = "Select Entity/Location";
                ddlDepartment.ClearSelection();
                tbxStartDate.Text = "";
                tbxEndDate.Text = "";
                tbxApplicationdays.Text = "";                
                ddlLicenseType.ClearSelection();
                ddlDepartment.ClearSelection();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "All";
                BindGrid();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter4", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        protected void ddlfilterStatutoryNonStatutory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLicenseType();
            BindGrid();
        }
        protected void upLicenseList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter5", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter();
            BindGrid();
        }      
        protected void upLicense_Load(object sender, EventArgs e)
        {            
            DateTime date = DateTime.MinValue;
            if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
            }

            DateTime date1 = DateTime.MinValue;
            if (DateTime.TryParseExact(tbxEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
            {                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date1.Year, date1.Month, date1.Day), true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
            }

            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPopup');", tbxFilterLocationPopup.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter6", "$(\"#divFilterLocationPopup\").hide(\"blind\", null, 500, function () { });", true);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool formValidateSuccess = false;
            bool saveSuccess = false;
            List<string> lstErrorMsg = new List<string>();
            List<Tuple<int, int>> lstLicenseUserMapping = new List<Tuple<int, int>>();
            #region Validation for Start Date End Date  
            int customerID = 0;
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
            }
            if (String.IsNullOrEmpty(tbxLicenseNo.Text))
            {
                lstErrorMsg.Add("Required license number");
                return;
            }
            else
            {
                formValidateSuccess = true;
            }
            if (String.IsNullOrEmpty(tbxLicenseTitle.Text))
            {
                lstErrorMsg.Add("Required license title");
                return;
            }
            else
            {
                formValidateSuccess = true;
            }
            if (ddlLicenseTypePopup.SelectedValue != "" && ddlLicenseTypePopup.SelectedValue != "-1")
            {
                formValidateSuccess = true;
            }
            else
            {
                lstErrorMsg.Add("Please select license type");
                return;
            }

            //if (fuSampleFile.HasFile)
            if (fuSampleFile.PostedFiles.Count > 0)
            {
                //{
                string[] validFileTypes = { "exe", "bat", "dll", "css", "js", };
                string ext = System.IO.Path.GetExtension(fuSampleFile.PostedFile.FileName);
                if (ext == "")
                {
                    lstErrorMsg.Add("Please do not upload virus file or blank files or file has no extention.");
                }
                else
                {
                    for (int i = 0; i < validFileTypes.Length; i++)
                    {
                        if (ext == "." + validFileTypes[i])
                        {
                            lstErrorMsg.Add("Please do not upload virus file or blank files.");
                            break;
                        }
                    }
                }
            }
            if (lstErrorMsg.Count > 0)
            {
                formValidateSuccess = false;
            }
            else
                formValidateSuccess = true;

            #endregion

            if (formValidateSuccess == true)
            {
                long LicenseID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["LicenseID"])))
                {
                    LicenseID = Convert.ToInt32(ViewState["LicenseID"]);
                }
                else
                {
                    LicenseID = Convert.ToInt32(Request.QueryString["AccessID"]);
                }
                if (LicenseID != -1)
                {                    
                    if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                    {
                        #region Save License Instance and Status                   
                        Lic_tbl_LicenseInstance licenseRecord = new Lic_tbl_LicenseInstance()
                        {
                            UpdatedBy = AuthenticationHelper.UserID,
                        };
                        if (!string.IsNullOrEmpty(tbxLicenseNo.Text))
                            licenseRecord.LicenseNo = Convert.ToString(tbxLicenseNo.Text);

                        if (!string.IsNullOrEmpty(tbxLicenseTitle.Text))
                            licenseRecord.LicenseTitle = Convert.ToString(tbxLicenseTitle.Text);

                        if (!string.IsNullOrEmpty(tbxStartDate.Text))
                            licenseRecord.StartDate = DateTimeExtensions.GetDate(tbxStartDate.Text);

                        if (!string.IsNullOrEmpty(tbxEndDate.Text))
                            licenseRecord.EndDate = DateTimeExtensions.GetDate(tbxEndDate.Text);

                        if (!string.IsNullOrEmpty(tbxApplicationdays.Text))
                            licenseRecord.RemindBeforeNoOfDays = Convert.ToInt32(tbxApplicationdays.Text);
                        else
                            licenseRecord.RemindBeforeNoOfDays = 0;

                        if (!string.IsNullOrEmpty(Convert.ToString(tvFilterLocationPopup.SelectedNode.Value)))
                        {
                            licenseRecord.CustomerBranchID = Convert.ToInt32(tvFilterLocationPopup.SelectedNode.Value);
                        }
                        if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                        {
                            licenseRecord.DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                        }

                        licenseRecord.ID = LicenseID;

                        bool existLicNo = LicenseMgmt.ExistsLicenseNo(Convert.ToInt32(customerID), licenseRecord.LicenseNo);
                        if (!existLicNo)
                        {
                            saveSuccess = LicenseMgmt.UpdateLicense(licenseRecord);
                            if (saveSuccess)
                            {

                                clearLicenseControls();
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "License Details Updated Sucessfully";
                                BindGrid();
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Something went wrong.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "License Details with Same License Number already exists";
                            formValidateSuccess = false;
                        }
                        #endregion

                        #region Save Document Against Current ComplianceScheduleonID
                        long complianceInstanceId = 0;
                        long complianceScheduleOnId = 0;
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["complianceinstanceid"])))
                        {
                            complianceInstanceId = Convert.ToInt32(ViewState["complianceinstanceid"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["compliancescheduleonid"])))
                        {
                            complianceScheduleOnId = Convert.ToInt32(ViewState["compliancescheduleonid"]);
                        }
                        if (complianceScheduleOnId > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<FileData> files = new List<FileData>();
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            HttpFileCollection fileCollection = Request.Files;
                            bool blankfileCount = true;
                            if (fileCollection.Count > 0)
                            {
                                var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(complianceInstanceId));
                                string directoryPath = null;
                                string version = null;
                                version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnId + "/" + version);
                                }
                                DocumentManagement.CreateDirectory(directoryPath);
                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = fileCollection[i];
                                    string[] keys = fileCollection.Keys[i].Split('$');
                                    String fileName = "";
                                    if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                    {
                                        fileName = "ComplianceDoc_" + uploadfile.FileName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                    if (uploadfile.ContentLength > 0)
                                    {
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        string filepathvalue = string.Empty;
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale.Replace(@"\", "/");
                                        }
                                        else
                                        {
                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        }
                                        
                                        FileData file = new FileData()
                                        {
                                            Name = fileName,
                                            FilePath = filepathvalue,
                                            FileKey = fileKey.ToString(),
                                            Version = version,
                                            VersionDate = DateTime.UtcNow,
                                            FileSize= uploadfile.ContentLength,
                                        };
                                        files.Add(file);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(uploadfile.FileName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            bool flag = false;
                            if (blankfileCount)
                            {
                                var tranid = GetTransactionID(complianceScheduleOnId);
                                flag = CreateTransaction(complianceScheduleOnId, tranid, files, list, Filelist);
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            }
                        }//complianceScheduleOnId
                        #endregion
                    }
                    else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
                    {
                        #region Save License Instance and Status                   
                        Lic_tbl_InternalLicenseInstance licenseRecord = new Lic_tbl_InternalLicenseInstance()
                        {
                            UpdatedBy = AuthenticationHelper.UserID,
                        };
                        if (!string.IsNullOrEmpty(tbxLicenseNo.Text))
                            licenseRecord.LicenseNo = Convert.ToString(tbxLicenseNo.Text);

                        if (!string.IsNullOrEmpty(tbxLicenseTitle.Text))
                            licenseRecord.LicenseTitle = Convert.ToString(tbxLicenseTitle.Text);

                        if (!string.IsNullOrEmpty(tbxStartDate.Text))
                            licenseRecord.StartDate = DateTimeExtensions.GetDate(tbxStartDate.Text);

                        if (!string.IsNullOrEmpty(tbxEndDate.Text))
                            licenseRecord.EndDate = DateTimeExtensions.GetDate(tbxEndDate.Text);

                        if (!string.IsNullOrEmpty(tbxApplicationdays.Text))
                            licenseRecord.RemindBeforeNoOfDays = Convert.ToInt32(tbxApplicationdays.Text);
                        else
                            licenseRecord.RemindBeforeNoOfDays = 0;

                        if (!string.IsNullOrEmpty(Convert.ToString(tvFilterLocationPopup.SelectedNode.Value)))
                        {
                            licenseRecord.CustomerBranchID = Convert.ToInt32(tvFilterLocationPopup.SelectedNode.Value);
                        }
                        if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                        {
                            licenseRecord.DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                        }

                        licenseRecord.ID = LicenseID;

                        bool existLicNo = InternalLicenseMgmt.ExistsInternalLicenseNo(Convert.ToInt32(customerID), licenseRecord.LicenseNo);
                        if (!existLicNo)
                        {
                            saveSuccess = InternalLicenseMgmt.UpdateLicense(licenseRecord);
                            if (saveSuccess)
                            {

                                clearLicenseControls();
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "License Details Updated Sucessfully";
                                BindGrid();
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Something went wrong.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "License Details with Same License Number already exists";
                            formValidateSuccess = false;
                        }
                        #endregion

                        #region Save Document Against Current ComplianceScheduleonID
                        long complianceInstanceId = 0;
                        long complianceScheduleOnId = 0;
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["complianceinstanceid"])))
                        {
                            complianceInstanceId = Convert.ToInt32(ViewState["complianceinstanceid"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["compliancescheduleonid"])))
                        {
                            complianceScheduleOnId = Convert.ToInt32(ViewState["compliancescheduleonid"]);
                        }
                        if (complianceScheduleOnId > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<InternalFileData> files = new List<InternalFileData>();
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            HttpFileCollection fileCollection = Request.Files;
                            bool blankfileCount = true;
                            if (fileCollection.Count > 0)
                            {
                                var InstanceData = DocumentManagement.GetInternalComplianceInstanceData(Convert.ToInt32(complianceInstanceId));
                                string directoryPath = null;
                                string version = null;
                                version = DocumentManagement.GetDocumnetVersionInternal(Convert.ToInt32(complianceScheduleOnId));
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnId + "/" + version);
                                }
                                DocumentManagement.CreateDirectory(directoryPath);
                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = fileCollection[i];
                                    string[] keys = fileCollection.Keys[i].Split('$');
                                    String fileName = "";
                                    if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                    {
                                        fileName = "ComplianceDoc_" + uploadfile.FileName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                    if (uploadfile.ContentLength > 0)
                                    {
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        string filepathvalue = string.Empty;
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale.Replace(@"\", "/");
                                        }
                                        else
                                        {
                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        }
                                        InternalFileData file = new InternalFileData()
                                        {
                                            Name = fileName,
                                            FilePath = filepathvalue,
                                            FileKey = fileKey.ToString(),
                                            Version = version,
                                            VersionDate = DateTime.UtcNow,
                                        };
                                        files.Add(file);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(uploadfile.FileName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            bool flag = false;
                            if (blankfileCount)
                            {
                                var tranid = GetInternalTransactionID(complianceScheduleOnId);
                                flag = CreateInternalTransaction(complianceScheduleOnId, tranid, files, list, Filelist);
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            }
                        }//complianceScheduleOnId
                        #endregion
                    }
                }
            }// Scheduleon checklicexist
            else
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong.";                
            }
        }
        public static bool CreateTransaction(long complinaceScheduleonID, long ComplianceTransactionID, List<FileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Statutory_SaveDocFiles(filesList);
                            if (files != null)
                            {
                                foreach (FileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.FileDatas.Add(fl);
                                    entities.SaveChanges();

                                    FileDataMapping fileMapping = new FileDataMapping();
                                    fileMapping.TransactionID = ComplianceTransactionID;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = complinaceScheduleonID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.FileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as CreateTransaction Function", "CreateTransaction");
                return false;
            }
        }
        public static bool CreateInternalTransaction(long complinaceScheduleonID, long ComplianceTransactionID, List<InternalFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Internal_SaveDocFiles(filesList);
                            if (files != null)
                            {
                                foreach (InternalFileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileDatas.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping fileMapping = new InternalFileDataMapping();
                                    fileMapping.TransactionID = ComplianceTransactionID;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.InternalScheduledOnID = complinaceScheduleonID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                List<string> TO = new List<string>();
                TO.Add("sachin@avantis.info");
                TO.Add("narendra@avantis.info");
                TO.Add("rahul@avantis.info");
                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                    null, "Error Occured as CreateTransaction Function", "CreateTransaction");
                return false;
            }
        }
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static long GetTransactionID(long scheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long tranid = 0;
                tranid = (from row in entities.ComplianceTransactions
                                where row.ComplianceScheduleOnID == scheduledOnID
                                select row.ID).OrderByDescending(a => a).FirstOrDefault();
                if (tranid > 0)
                    return tranid;
                else
                    return 0;                
            }
        }
        public static long GetInternalTransactionID(long scheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long tranid = 0;
                tranid = (from row in entities.InternalComplianceTransactions
                          where row.InternalComplianceScheduledOnID == scheduledOnID
                          select row.ID).OrderByDescending(a => a).FirstOrDefault();
                if (tranid > 0)
                    return tranid;
                else
                    return 0;
            }
        }
        #region License Documents
        public void DownloadFile(int fileId)
        {
            try
            {
                if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                {
                    #region Statutory
                    var file = Business.ComplianceManagement.GetFile(fileId);
                    if (file.FilePath != null)
                    {
                        string filePath = string.Empty;
                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                            filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                        }
                        else
                        {
                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        }
                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    #endregion
                }
                else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
                {
                    #region Internal
                    var file = Business.InternalComplianceManagement.GetFile(fileId);
                    if (file.FilePath != null)
                    {
                        string filePath = string.Empty;
                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                            filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                        }
                        else
                        {
                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        }
                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    #endregion
                }
            }
            catch (Exception)
            {
                throw;                
            }
        }
        protected void rptLicenseVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblLicenseDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblLicenseDocumentVersion);
            }
        }
        protected void rptLicenseVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Statutory")
                {
                    #region Statutory
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<SP_GetLicenseDocument_Result> ComplianceFileData = new List<SP_GetLicenseDocument_Result>();
                    List<SP_GetLicenseDocument_Result> ComplianceDocument = new List<SP_GetLicenseDocument_Result>();
                    ComplianceDocument = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();

                    if (commandArgs[2].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptLicenseDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptLicenseDocumnets.DataBind();

                            rptLicenseWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptLicenseWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                LicenseID = x.LicenseID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                            }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();

                            //upComplianceDetails1.Update();                    
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            var LicenseData = LicenseDocumentManagement.GetLicense(Convert.ToInt32(commandArgs[0]));

                            ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);

                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    #endregion
                }
                else if (ddlfilterStatutoryNonStatutory.SelectedItem.Text == "Internal")
                {
                    #region Statutory
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<SP_GetInternalLicenseDocument_Result> ComplianceFileData = new List<SP_GetInternalLicenseDocument_Result>();
                    List<SP_GetInternalLicenseDocument_Result> ComplianceDocument = new List<SP_GetInternalLicenseDocument_Result>();
                    ComplianceDocument = InternalLicenseMgmt.GetInternalFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();

                    if (commandArgs[2].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptLicenseDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptLicenseDocumnets.DataBind();

                            rptLicenseWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptLicenseWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                LicenseID = x.LicenseID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                            }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();

                            //upComplianceDetails1.Update();                    
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            var LicenseData = InternalLicenseMgmt.GetLicense(Convert.ToInt32(commandArgs[0]));

                            ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);

                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }
        protected void rptLicenseDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }
        protected void rptLicenseWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        #endregion
    }
}