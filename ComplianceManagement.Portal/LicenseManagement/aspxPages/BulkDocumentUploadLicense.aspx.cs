﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Data;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.License;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.aspxPages
{
    public partial class BulkDocumentUploadLicense : System.Web.UI.Page
    {
        List<string> lstErrorMsg = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }       
        public static List<TempDocument> getUploadedDocList(int ProductID, string productype)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AllDocument = (from row in entities.TempDocuments
                                   where row.IsActive == true
                                   && row.ProductID == ProductID
                                   && row.FileType == productype
                                   select row).ToList();


                return AllDocument;
            }
        }
        public void BindGrid()
        {
            try
            {
                grdBulkDocUpload.DataSource = null;
                grdBulkDocUpload.DataBind();
                if (rdostatutoryinternal.SelectedItem.Text == "Statutory")
                {
                    var AllDocument = getUploadedDocList(6, "S");
                    grdBulkDocUpload.DataSource = AllDocument;
                    grdBulkDocUpload.DataBind();
                    Session["TotalRows"] = AllDocument.Count;
                }
                else if (rdostatutoryinternal.SelectedItem.Text == "Internal")
                {
                    var AllDocument = getUploadedDocList(6, "I");
                    grdBulkDocUpload.DataSource = AllDocument;
                    grdBulkDocUpload.DataBind();
                    Session["TotalRows"] = AllDocument.Count;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvBulkDocUpload.IsValid = false;
                cvBulkDocUpload.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void grdBulkDocUpload_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var fileno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "FileNo"));
                if (fileno == "")
                {
                    Label lbl = (Label)e.Row.FindControl("lblFileNo");
                    lbl.Visible = false;
                    DropDownList ddlFile = (DropDownList)e.Row.FindControl("ddlFileNo");
                    BindFileno(ddlFile);
                }
                else
                {
                    DropDownList ddlFile = (DropDownList)e.Row.FindControl("ddlFileNo");
                    ddlFile.Visible = false;
                }
            }
        }
        private void BindFileno(DropDownList ddlfiles)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    if (rdostatutoryinternal.SelectedItem.Text == "Statutory")
                    {
                        var FileNoList = (from tom in entities.Lic_tbl_LicenseInstance
                                          join row in entities.Lic_tbl_LicenseComplianceInstanceMapping
                                          on tom.ID equals row.LicenseID
                                          where tom.IsDeleted == false && tom.CustomerID == customerID
                                          select new
                                          {
                                              ID = tom.ID,
                                              Fileno = tom.LicenseNo,
                                          }).ToList();
                        ddlfiles.Items.Clear();
                        ddlfiles.DataTextField = "Fileno";
                        ddlfiles.DataValueField = "ID";
                        ddlfiles.DataSource = FileNoList;
                        ddlfiles.DataBind();
                        ddlfiles.Items.Insert(0, new ListItem(" Select ", "-1"));
                    }
                    else if (rdostatutoryinternal.SelectedItem.Text == "Internal")
                    {
                        var FileNoList = (from tom in entities.Lic_tbl_InternalLicenseInstance
                                          join row in entities.Lic_tbl_LicenseInternalComplianceInstanceMapping
                                          on tom.ID equals row.LicenseID
                                          where tom.IsDeleted == false && tom.CustomerID == customerID
                                          select new
                                          {
                                              ID = tom.ID,
                                              Fileno = tom.LicenseNo,
                                          }).ToList();

                        ddlfiles.Items.Clear();
                        ddlfiles.DataTextField = "Fileno";
                        ddlfiles.DataValueField = "ID";
                        ddlfiles.DataSource = FileNoList;
                        ddlfiles.DataBind();
                        ddlfiles.Items.Insert(0, new ListItem(" Select ", "-1"));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvBulkDocUpload.IsValid = false;
                cvBulkDocUpload.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;
                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;
                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static long CreateTempDocument(TempDocument tempComplianceDocument)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.TempDocuments.Add(tempComplianceDocument);
                entities.SaveChanges();
                if (tempComplianceDocument.ID > 0)
                {
                    return tempComplianceDocument.ID;
                }
                else
                {
                    return 0;
                }
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //Save in temp Table
            #region Upload Document       
            try
            {

                if (fuBulkDocUpload.HasFiles)
                {
                    TempDocument ObjTempDoc = null;
                    HttpFileCollection fileCollection = Request.Files;
                    if (fileCollection.Count > 0)
                    {
                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                        int customerID = -1;
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string directoryPath = null;

                            if (!string.IsNullOrEmpty(fileName))
                            {
                                string[] keys = fileCollection.Keys[i].Split('$');
                                if (keys[keys.Count() - 1].Equals("fuBulkDocUpload"))
                                {
                                    directoryPath = Server.MapPath("~/CommonDocuments/" + customerID + "/" + 1 + ".0");
                                }
                                DocumentManagement.CreateDirectory(directoryPath);
                                string finalPath = Path.Combine(directoryPath, fileName);
                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                fileCollection[i].SaveAs(Server.MapPath(finalPath));
                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                ObjTempDoc = new TempDocument()
                                {
                                    CustomerId = customerID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsActive = true,
                                    ProductID = 6,
                                    FilePath = finalPath,
                                    FileKey = bytes,
                                    FileName = fileCollection[i].FileName,
                                };
                                int index = fileName.LastIndexOf('.');
                                var fileno = index == -1 ? fileName : fileName.Substring(0, index);
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    if (rdostatutoryinternal.SelectedItem.Text == "Statutory")
                                    {
                                        var FileNoList = (from tom in entities.Lic_tbl_LicenseInstance
                                                          where tom.LicenseNo == fileno
                                                          select new
                                                          {
                                                              ID = tom.ID,
                                                              Fileno = tom.LicenseNo,
                                                          }).FirstOrDefault();
                                        if (FileNoList != null)
                                        {
                                            ObjTempDoc.FileNo = FileNoList.Fileno;
                                            ObjTempDoc.fileNoID = Convert.ToInt32(FileNoList.ID);
                                        }
                                    }
                                    if (rdostatutoryinternal.SelectedItem.Text == "Internal")
                                    {
                                        var FileNoList = (from tom in entities.Lic_tbl_InternalLicenseInstance
                                                          where tom.LicenseNo == fileno
                                                          select new
                                                          {
                                                              ID = tom.ID,
                                                              Fileno = tom.LicenseNo,
                                                          }).FirstOrDefault();
                                        if (FileNoList != null)
                                        {
                                            ObjTempDoc.FileNo = FileNoList.Fileno;
                                            ObjTempDoc.fileNoID = Convert.ToInt32(FileNoList.ID);
                                        }
                                    }
                                }

                                ObjTempDoc.Version = 1 + ".0";
                                ObjTempDoc.CreatedOn = DateTime.Now;

                                if (rdostatutoryinternal.SelectedItem.Text == "Statutory")
                                {
                                    ObjTempDoc.FileType = "S";
                                }
                                else if (rdostatutoryinternal.SelectedItem.Text == "Internal")
                                {
                                    ObjTempDoc.FileType = "I";
                                }
                                long _objTempDocumentID = CreateTempDocument(ObjTempDoc);
                            }
                        }
                    }
                }
                else
                {
                    cvBulkDocUpload.IsValid = false;
                    cvBulkDocUpload.ErrorMessage = "File not selected, Please select file. ";
                    VsBulkDocUpload.CssClass = "alert alert-danger";
                    if (lstErrorMsg.Count > 0)
                    {
                        showErrorMessages(lstErrorMsg, cvBulkDocUpload);
                    }
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            #endregion
            //Call bindGrid();
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        protected void btnProcessDocUpload_Click(object sender, EventArgs e)
        {
            try
            {
                bool DocUploadSuccess = false;
                bool saveSuccess = false;
                List<string> lstErrorMsg = new List<string>();
                long cid = -1;

                cid = Convert.ToInt64(AuthenticationHelper.CustomerID);
                if (cid != -1)
                {

                    #region Statutoty
                    if (rdostatutoryinternal.SelectedItem.Text == "Statutory")
                    {
                        int chkflg = 0;
                        for (int i = 0; i < grdBulkDocUpload.Rows.Count; i++)
                        {
                            CheckBox chk = ((CheckBox)grdBulkDocUpload.Rows[i].FindControl("rechild"));
                            if (chk.Checked)
                            {
                                chkflg = 1;
                                #region Validation                                
                                // var fileno = Convert.ToString(DataBinder.Eval(grdBulkDocUpload.Rows[i].DataItem, "FileNo"));
                                var fileno = Convert.ToString(((Label)grdBulkDocUpload.Rows[i].FindControl("lblFileNo")).Text);
                                if (fileno == "" || fileno == null)
                                {
                                    if (!string.IsNullOrEmpty(((DropDownList)grdBulkDocUpload.Rows[i].FindControl("ddlFileNo")).Text))
                                    {
                                        try
                                        {
                                            DropDownList FileNo = (DropDownList)grdBulkDocUpload.Rows[i].FindControl("ddlFileNo");
                                            if (FileNo.SelectedValue == "-1")
                                            {
                                                lstErrorMsg.Add("Please Select License No. At Row No  - " + (i + 1) + "  or License No. should not be blank");
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            lstErrorMsg.Add("Please Select License No. At Row No  - " + (i + 1) + "  or License No. should not be blank");
                                        }
                                    }
                                    else
                                    {
                                        lstErrorMsg.Add("Please Select License No. At Row No  - " + (i + 1) + "  or License No. should not be blank");
                                    }
                                }


                                if (lstErrorMsg.Count > 0)
                                {
                                    DocUploadSuccess = false;
                                    showErrorMessages(lstErrorMsg, cvBulkDocUpload);
                                }
                                else
                                    DocUploadSuccess = true;

                                #endregion
                            }
                        }
                        if (chkflg == 0)
                        {
                            lstErrorMsg.Add("Please select at least one CheckBox for Process");
                            if (lstErrorMsg.Count > 0)
                            {
                                DocUploadSuccess = false;
                                showErrorMessages(lstErrorMsg, cvBulkDocUpload);
                            }
                            else
                                DocUploadSuccess = true;
                        }

                        #region Save Multiple Documents upload

                        if (grdBulkDocUpload.Rows.Count > 0 && DocUploadSuccess == true)
                        {
                            foreach (GridViewRow row in grdBulkDocUpload.Rows)
                            {
                                CheckBox chk = ((CheckBox)row.FindControl("rechild"));
                                if (chk.Checked)
                                {
                                    int RowID = Convert.ToInt32(((HiddenField)row.FindControl("HiddenID")).Value);
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        List<FileData> files = new List<FileData>();
                                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                                        bool blankfileCount = true;
                                        var FileNoList = (from tom in entities.TempDocuments
                                                          where tom.ID == RowID && tom.FileType == "S"
                                                          select tom).FirstOrDefault();

                                        if (FileNoList != null)
                                        {
                                            long LicenseID = -1;
                                            if (FileNoList.fileNoID != null)
                                            {
                                                LicenseID = Convert.ToInt32(FileNoList.fileNoID);
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty((((DropDownList)row.FindControl("ddlFileNo")).SelectedValue)))
                                                {
                                                    LicenseID = Convert.ToInt32(((DropDownList)row.FindControl("ddlFileNo")).SelectedValue);
                                                }
                                            }
                                            DateTime scheduleon;
                                            var Licensedetails = (from tom in entities.Lic_tbl_LicenseInstance
                                                                  where tom.ID == LicenseID
                                                                  select tom).FirstOrDefault();
                                            if (Licensedetails != null)
                                            {
                                                int days = 0;
                                                if (Licensedetails.RemindBeforeNoOfDays != 0)
                                                {
                                                    days = Convert.ToInt32(Licensedetails.RemindBeforeNoOfDays);
                                                }
                                                scheduleon = Licensedetails.EndDate.Value.AddDays(-days);

                                                var LicenseCompliancedetails = (from tom in entities.SP_LicenseBulkDocumentUpload((int)LicenseID, "S")
                                                                                where tom.Scheduleon.Value.Date == scheduleon.Date
                                                                                select tom).FirstOrDefault();
                                                if (LicenseCompliancedetails != null)
                                                {
                                                    #region Upload File                                                                                                                                
                                                    long complianceInstanceId = LicenseCompliancedetails.ComplianceInstanceID;
                                                    long complianceScheduleOnId = LicenseCompliancedetails.ComplianceScheduleOnID;
                                                    int CustomerBranchID = LicenseCompliancedetails.CustomerBranchID;
                                                    long complianceTransactionId = LicenseCompliancedetails.ComplianceTransactionID;
                                                    int customerID = Convert.ToInt32(LicenseCompliancedetails.CustomerID);
                                                    var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(complianceInstanceId));
                                                    string directoryPath = null;
                                                    string version = null;
                                                    version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(complianceScheduleOnId));
                                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                    {
                                                        directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;
                                                    }
                                                    else
                                                    {
                                                        directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnId + "/" + version);
                                                    }
                                                    DocumentManagement.CreateDirectory(directoryPath);
                                                    String fileName = "";
                                                    fileName = "ComplianceDoc_" + FileNoList.FileName;
                                                    list.Add(new KeyValuePair<string, int>(fileName, 1));
                                                    Guid fileKey = Guid.NewGuid();
                                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(FileNoList.FileName));
                                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, FileNoList.FileKey));
                                                    if (FileNoList.FileKey.Length > 0)
                                                    {
                                                        string filepathvalue = string.Empty;
                                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                        {
                                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                            filepathvalue = vale.Replace(@"\", "/");
                                                        }
                                                        else
                                                        {
                                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                        }
                                                        FileData file = new FileData()
                                                        {
                                                            Name = fileName,
                                                            FilePath = filepathvalue,
                                                            FileKey = fileKey.ToString(),
                                                            Version = version,
                                                            VersionDate = DateTime.UtcNow,
                                                        };

                                                        files.Add(file);
                                                    }
                                                    else
                                                    {
                                                        if (!string.IsNullOrEmpty(FileNoList.FileName))
                                                            blankfileCount = false;
                                                    }
                                                    if (blankfileCount)
                                                    {
                                                        saveSuccess = LicenseMgmt.UploadLicenseCreateDocuments(complianceTransactionId, complianceScheduleOnId, files, list, Filelist);
                                                        if (saveSuccess)
                                                        {
                                                            FileNoList.IsActive = false;
                                                            entities.SaveChanges();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                                        cvBulkDocUpload.IsValid = false;
                                                        cvBulkDocUpload.ErrorMessage = "Please do not upload virus file or blank files.";
                                                    }
                                                    #endregion
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //cvBulkDocUpload.IsValid = false;
                                    //cvBulkDocUpload.ErrorMessage = "Please check checkbox first";
                                }
                            }
                        }
                        #endregion

                        if (saveSuccess)
                        {
                            BindGrid();
                            bindPageNumber();
                            ShowGridDetail();
                            cvBulkDocUpload.ErrorMessage = "Document Upload Successfully.";
                            VsBulkDocUpload.CssClass = "alert alert-success";
                        }
                    }
                    #endregion
                    #region Internal
                    else if (rdostatutoryinternal.SelectedItem.Text == "Internal")
                    {
                        int chkflg = 0;
                        for (int i = 0; i < grdBulkDocUpload.Rows.Count; i++)
                        {
                           
                            CheckBox chk = ((CheckBox)grdBulkDocUpload.Rows[i].FindControl("rechild"));
                            if (chk.Checked)
                            {
                                chkflg = 1;
                                #region Validation
                                // var fileno = Convert.ToString(DataBinder.Eval(grdBulkDocUpload.Rows[i].DataItem, "FileNo"));
                                var fileno = Convert.ToString(((Label)grdBulkDocUpload.Rows[i].FindControl("lblFileNo")).Text);
                                if (fileno == "" || fileno == null)
                                {
                                    if (!string.IsNullOrEmpty(((DropDownList)grdBulkDocUpload.Rows[i].FindControl("ddlFileNo")).Text))
                                    {
                                        try
                                        {
                                            DropDownList FileNo = (DropDownList)grdBulkDocUpload.Rows[i].FindControl("ddlFileNo");
                                            if (FileNo.SelectedValue == "-1")
                                            {
                                                lstErrorMsg.Add("Please Select License No. At Row No  - " + (i + 1) + "  or License No. should not be blank");
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            lstErrorMsg.Add("Please Select License No. At Row No  - " + (i + 1) + "  or License No. should not be blank");
                                        }
                                    }
                                    else
                                    {
                                        lstErrorMsg.Add("Please Select License No. At Row No  - " + (i + 1) + "  or License No. should not be blank");
                                    }
                                }
                                if (lstErrorMsg.Count > 0)
                                {
                                    DocUploadSuccess = false;
                                    showErrorMessages(lstErrorMsg, cvBulkDocUpload);
                                }
                                else
                                    DocUploadSuccess = true;

                                #endregion
                            }
                        }
                        if (chkflg == 0)
                        {
                            lstErrorMsg.Add("Please select at least one CheckBox for Process");
                            if (lstErrorMsg.Count > 0)
                            {
                                DocUploadSuccess = false;
                                showErrorMessages(lstErrorMsg, cvBulkDocUpload);
                            }
                            else
                                DocUploadSuccess = true;
                        }

                            #region Save Multiple Documents upload

                            if (grdBulkDocUpload.Rows.Count > 0 && DocUploadSuccess == true)
                        {
                            foreach (GridViewRow row in grdBulkDocUpload.Rows)
                            {
                                CheckBox chk = ((CheckBox)row.FindControl("rechild"));
                                if (chk.Checked)
                                {
                                    int RowID = Convert.ToInt32(((HiddenField)row.FindControl("HiddenID")).Value);
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        List<InternalFileData> files = new List<InternalFileData>();
                                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                                        bool blankfileCount = true;
                                        var FileNoList = (from tom in entities.TempDocuments
                                                          where tom.ID == RowID && tom.FileType == "I"
                                                          select tom).FirstOrDefault();

                                        if (FileNoList != null)
                                        {
                                            long LicenseID = -1;
                                            if (FileNoList.fileNoID != null)
                                            {
                                                LicenseID = Convert.ToInt32(FileNoList.fileNoID);
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty((((DropDownList)row.FindControl("ddlFileNo")).SelectedValue)))
                                                {
                                                    LicenseID = Convert.ToInt32(((DropDownList)row.FindControl("ddlFileNo")).SelectedValue);
                                                }
                                            }
                                            DateTime scheduleon;
                                            var Licensedetails = (from tom in entities.Lic_tbl_InternalLicenseInstance
                                                                  where tom.ID == LicenseID
                                                                  select tom).FirstOrDefault();
                                            if (Licensedetails != null)
                                            {
                                                int days = 0;
                                                if (Licensedetails.RemindBeforeNoOfDays != 0)
                                                {
                                                    days = Convert.ToInt32(Licensedetails.RemindBeforeNoOfDays);
                                                }
                                                scheduleon = Licensedetails.EndDate.Value.AddDays(-days);

                                                var LicenseCompliancedetails = (from tom in entities.SP_LicenseBulkDocumentUpload((int)LicenseID, "I")
                                                                                where tom.Scheduleon.Value.Date == scheduleon.Date
                                                                                select tom).FirstOrDefault();
                                                if (LicenseCompliancedetails != null)
                                                {
                                                    #region Upload File                                                                                                                                
                                                    long complianceInstanceId = LicenseCompliancedetails.ComplianceInstanceID;
                                                    long complianceScheduleOnId = LicenseCompliancedetails.ComplianceScheduleOnID;
                                                    int CustomerBranchID = LicenseCompliancedetails.CustomerBranchID;
                                                    long complianceTransactionId = LicenseCompliancedetails.ComplianceTransactionID;
                                                    int customerID = Convert.ToInt32(LicenseCompliancedetails.CustomerID);
                                                    var InstanceData = DocumentManagement.GetInternalComplianceInstanceData(Convert.ToInt32(complianceInstanceId));
                                                    string directoryPath = null;
                                                    string version = null;
                                                    version = DocumentManagement.GetDocumnetVersionInternal(Convert.ToInt32(complianceScheduleOnId));
                                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                    {
                                                        directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\InternalAvacomDocuments\\" + customerID + "\\" + CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + complianceScheduleOnId + "\\" + version;
                                                    }
                                                    else
                                                    {
                                                        directoryPath = Server.MapPath("~/InternalAvacomDocuments/" + customerID + "/" + CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + complianceScheduleOnId + "/" + version);
                                                    }
                                                    DocumentManagement.CreateDirectory(directoryPath);
                                                    String fileName = "";
                                                    fileName = "ComplianceDoc_" + FileNoList.FileName;
                                                    list.Add(new KeyValuePair<string, int>(fileName, 1));
                                                    Guid fileKey = Guid.NewGuid();
                                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(FileNoList.FileName));
                                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, FileNoList.FileKey));
                                                    if (FileNoList.FileKey.Length > 0)
                                                    {
                                                        string filepathvalue = string.Empty;
                                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                        {
                                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                            filepathvalue = vale.Replace(@"\", "/");
                                                        }
                                                        else
                                                        {
                                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                        }
                                                        InternalFileData file = new InternalFileData()
                                                        {
                                                            Name = fileName,
                                                            FilePath = filepathvalue,
                                                            FileKey = fileKey.ToString(),
                                                            Version = version,
                                                            VersionDate = DateTime.UtcNow,
                                                        };

                                                        files.Add(file);
                                                    }
                                                    else
                                                    {
                                                        if (!string.IsNullOrEmpty(FileNoList.FileName))
                                                            blankfileCount = false;
                                                    };
                                                    if (blankfileCount)
                                                    {
                                                        saveSuccess = InternalLicenseMgmt.UploadInternalLicenseCreateDocuments(complianceTransactionId, complianceScheduleOnId, files, list, Filelist);
                                                        if (saveSuccess)
                                                        {
                                                            FileNoList.IsActive = false;
                                                            entities.SaveChanges();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                                        cvBulkDocUpload.IsValid = false;
                                                        cvBulkDocUpload.ErrorMessage = "Please do not upload virus file or blank files.";
                                                    }
                                                    #endregion


                                                    FileNoList.IsActive = false;
                                                    entities.SaveChanges();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        if (saveSuccess)
                        {
                            BindGrid();
                            bindPageNumber();
                            ShowGridDetail();
                            cvBulkDocUpload.ErrorMessage = "Document Upload Successfully.";
                            VsBulkDocUpload.CssClass = "alert alert-success";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdBulkDocUpload.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdBulkDocUpload.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdBulkDocUpload.PageIndex = chkSelectedPage - 1;
            grdBulkDocUpload.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGrid();

            ShowGridDetail();
        }
        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
            VsBulkDocUpload.CssClass = "alert alert-danger";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
        }
    }
}