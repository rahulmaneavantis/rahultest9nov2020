﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Dashboard
{
    public partial class LicenseMangDashboard : System.Web.UI.Page
    {
        protected static string seriesData_GraphLicenseStatus;
        protected static string seriesData_GraphLicenseExpired;
        protected static string seriesData_GraphLicensePendingRenew;
        protected static string seriesData_GraphLicenseOverdue;

        protected static string seriesData_GraphDept;
        protected static string graph_Dept_Categories;

        protected static string seriesData_GraphBranch;
        protected static string graph_Branch_Categories;

        protected static string seriesData_GraphLicenseType;
        protected static string graph_LicenseType_Categories;
        protected string user_Roles;

        protected static string queryStringFlag = "";
        protected static string IsSatutoryInternal;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {                    
                    IsSatutoryInternal = "Statutory";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {                    
                    IsSatutoryInternal = "Internal";
                }
                user_Roles = AuthenticationHelper.Role;
                if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "CADMN")
                {
                    HiddenField home = (HiddenField)Master.FindControl("Ishome");
                    home.Value = "true";                    
                    BindLocationFilter();
                    if (ddlComplianceType.SelectedItem.Text == "Statutory")
                    {
                        BindDashboard();
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Internal")
                    {
                        BindDashboardInternal();
                    }
                    
                    BindLicenseType();
                }
            }
        }       
        private void BindLocationFilter()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    tvFilterLocation.Nodes.Clear();
                    string isstatutoryinternal = "S";
                    if (ddlComplianceType.SelectedItem.Text == "Statutory")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Internal")
                    {
                        isstatutoryinternal = "I";
                    }
                    var LocationList = LicenseMgmt.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);
                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);
                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                        tvFilterLocation.Nodes.Add(node);
                    }
                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {                    
                    IsSatutoryInternal = "Statutory";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {                    
                    IsSatutoryInternal = "Internal";
                }

                BindLicenseType();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindLicenseType()
        {
            List<Sp_BindLicenseType_Result> data = new List<Sp_BindLicenseType_Result>();
            string isstatutoryinternal = "S";
            if (ddlComplianceType.SelectedItem.Text == "Statutory")
            {
                isstatutoryinternal = "S";
            }
            else if (ddlComplianceType.SelectedItem.Text == "Internal")
            {
                isstatutoryinternal = "I";
            }
            user_Roles = AuthenticationHelper.Role;
            if (user_Roles.Contains("CADMN") || user_Roles.Contains("IMPT") || user_Roles.Contains("MGMT"))
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);
            }
            else
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);
            }
            //Page DropDown
            ddlLicenseType.DataTextField = "Name";
            ddlLicenseType.DataValueField = "ID";

            ddlLicenseType.DataSource = data;
            ddlLicenseType.DataBind();

            ddlLicenseType.Items.Insert(0, new ListItem("< All >", "-1"));
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    BindDashboard();
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    BindDashboardInternal();
                }
                //BindLicenseType();
                collapseDivFilters.Attributes.Remove("class");
                collapseDivFilters.Attributes.Add("class", "panel-collapse in");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTreeViewSelection(tvFilterLocation);
                tbxFilterLocation.Text = "Select Entity/Location";              
                ddlLicenseType.ClearSelection();
                btnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkEditLicense_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    long licenseInstanceID = Convert.ToInt64(btn.CommandArgument);

                    if (licenseInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowLicenseDialog(" + licenseInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLicenseDashboard.IsValid = false;
                cvLicenseDashboard.ErrorMessage = "Something went wrong, Please try again";
            }
        }      
        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }
        public void BindDashboardInternal()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var MastertransactionsQuery = (entities.SP_InternalLicenseInstanceTransactionCount(AuthenticationHelper.UserID, AuthenticationHelper.CustomerID, AuthenticationHelper.Role)).ToList();

                    int branchID = -1;
                    long licenseTypeID = -1;
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                    {
                        licenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                    }
                    if (licenseTypeID != -1)
                    {
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
                    }
                    if (branchID != -1)
                    {
                        var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => branchList.Contains(entry.CustomerBranchID)).ToList();
                    }
                    BindInternalLicenseStatusCounts(MastertransactionsQuery);
                    BindExpiredGriphInternal(MastertransactionsQuery);
                    ShowChart_InternalLicenseType(MastertransactionsQuery);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindDashboard()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);               
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var MastertransactionsQuery = (entities.SP_LicenseInstanceTransactionCount(AuthenticationHelper.UserID, AuthenticationHelper.CustomerID, AuthenticationHelper.Role)).ToList();

                    int branchID = -1;                   
                    long licenseTypeID = -1;
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }                    
                    if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                    {
                        licenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                    }
                    if (licenseTypeID !=-1)
                    {
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => entry.LicenseTypeID == licenseTypeID).ToList();
                    }
                    if (branchID != -1)
                    {
                        var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => branchList.Contains(entry.CustomerBranchID)).ToList();
                    }
                    BindLicenseStatusCounts(MastertransactionsQuery);
                    BindExpiredGriph(MastertransactionsQuery);                   
                    ShowChart_LicenseType(MastertransactionsQuery);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<int> GetAssignedLocationList(int UserID, int custID, String Role)
        {
            List<int> LocationList = new List<int>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (Role == "MGMT")
                {
                    var query = (from row in entities.EntitiesAssignments
                                 where row.UserID == UserID
                                 select row).Distinct().ToList();

                    if (query != null)
                        LocationList = query.Select(a => (int)a.BranchID).Distinct().ToList();

                }
                else if (Role == "CADMN")
                {
                    var query = (from row in entities.CustomerBranches                                 
                                 where row.CustomerID == custID
                                 select row).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

                    if (query != null)
                        LocationList = query.Select(a => a.ID).Distinct().ToList();
                }                                
                return LocationList;
            }
        }
        public static List<SP_LicenseInstanceTransactionCount_Result> GetMastertransactionsQuery(int customerID, int UserID, List<int> branchList, int deptID, string licenseStatus, long licenseTypeID, string IsPERMGMTCA)
        {
            List<int> statusId = new List<int>();
            statusId.Add(4);
            statusId.Add(5);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_LicenseInstanceTransactionCount(UserID, customerID, IsPERMGMTCA)
                             select row).ToList();

                if (query.Count > 0)
                {
                    if (IsPERMGMTCA == "MGMT" || IsPERMGMTCA == "CADMN")
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList();
                        query = query.GroupBy(a => a.LicenseID).Select(a => a.FirstOrDefault()).ToList();
                    }

                    if (branchList.Count > 0)
                        query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (deptID != -1)
                        query = query.Where(entry => entry.DepartmentID == deptID).ToList();


                }
                return query.ToList();
            }
        }
        public void BindInternalLicenseStatusCounts(List<SP_InternalLicenseInstanceTransactionCount_Result> MastertransactionsQuery)
        {
            try
            {

                List<int> statusId = new List<int>();
                statusId.Add(4);
                statusId.Add(5);

                List<int> cstatusId = new List<int>();
                cstatusId.Add(1);
                cstatusId.Add(2);
                cstatusId.Add(3);
                cstatusId.Add(6);
                cstatusId.Add(8);
                cstatusId.Add(10);

                if (MastertransactionsQuery != null && MastertransactionsQuery.Count > 0)
                {
                    MastertransactionsQuery = MastertransactionsQuery.Where(entry => entry.RoleID == 3).ToList();

                    divExpiringcount.InnerText = MastertransactionsQuery.Where(row => row.MGRStatus == "Expiring"
                    //&& !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count().ToString();

                    divActiveCount.InnerText = MastertransactionsQuery.Where(row => row.MGRStatus == "Active"
                    //&& !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count().ToString();

                    divExpiredCount.InnerText = MastertransactionsQuery.Where(row => row.MGRStatus == "Expired"
                    //&& !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count().ToString();


                    divAppliedcount.InnerText = MastertransactionsQuery.Where(row => row.MGRStatus == "Applied"
                    && statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();

                    var transactionquery = MastertransactionsQuery.Where(row => row.MGRStatus == "Expiring"
                    && cstatusId.Contains((int)row.ComplianceStatusID)).ToList();


                    if (transactionquery.Count > 0)
                        btnExportExcelExpiring.Visible = true;
                    else
                        btnExportExcelExpiring.Visible = false;

                    if (transactionquery.Count > 2)
                        lnkShowDetailLicense.Visible = true;
                    else
                        lnkShowDetailLicense.Visible = false;


                    grdLicenseExpiring.DataSource = transactionquery;
                    grdLicenseExpiring.DataBind();

                    Session["grdLicenseExpiringData"] = null;
                    Session["grdLicenseExpiringData"] = (grdLicenseExpiring.DataSource as List<SP_InternalLicenseInstanceTransactionCount_Result>).ToDataTable();

                    var transactionqueryExpired = MastertransactionsQuery.Where(row => row.MGRStatus == "Expired"
                    && cstatusId.Contains((int)row.ComplianceStatusID)).ToList();

                    if (transactionqueryExpired.Count > 0)
                        btnExportExcelExpired.Visible = true;
                    else
                        btnExportExcelExpired.Visible = false;

                    if (transactionqueryExpired.Count > 2)
                        lnkLicExpiredshowMore.Visible = true;
                    else
                        lnkLicExpiredshowMore.Visible = false;

                    grdLicenseExpired.DataSource = transactionqueryExpired;
                    grdLicenseExpired.DataBind();

                    Session["grdLicenseExpiredData"] = null;
                    Session["grdLicenseExpiredData"] = (grdLicenseExpired.DataSource as List<SP_InternalLicenseInstanceTransactionCount_Result>).ToDataTable();

                }
                else
                {
                    divExpiringcount.InnerText = "0";
                    divActiveCount.InnerText = "0";
                    divExpiredCount.InnerText = "0";
                    divAppliedcount.InnerText = "0";
                    grdLicenseExpiring.DataSource = null;
                    grdLicenseExpiring.DataBind();
                    grdLicenseExpired.DataSource = null;
                    grdLicenseExpired.DataBind();
                    lnkLicExpiredshowMore.Visible = false;
                    lnkShowDetailLicense.Visible = false;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindLicenseStatusCounts(List<SP_LicenseInstanceTransactionCount_Result> MastertransactionsQuery)
        {
            try
            {
                
                List<int> statusId = new List<int>();
                statusId.Add(4);
                statusId.Add(5);

                List<int> cstatusId = new List<int>();
                cstatusId.Add(1);
                cstatusId.Add(2);
                cstatusId.Add(3);
                cstatusId.Add(6);
                cstatusId.Add(8);
                cstatusId.Add(10);

                if (MastertransactionsQuery != null && MastertransactionsQuery.Count > 0)
                {
                    MastertransactionsQuery = MastertransactionsQuery.Where(entry => entry.RoleID == 3).ToList();

                    divExpiringcount.InnerText = MastertransactionsQuery.Where(row => row.MGRStatus == "Expiring"
                    //&& !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count().ToString();

                    divActiveCount.InnerText = MastertransactionsQuery.Where(row => row.MGRStatus == "Active"
                    //&& !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count().ToString();

                    divExpiredCount.InnerText = MastertransactionsQuery.Where(row => row.MGRStatus == "Expired"
                    //&& !statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count().ToString();


                    divAppliedcount.InnerText = MastertransactionsQuery.Where(row => row.MGRStatus == "Applied"
                    && statusId.Contains((int)row.ComplianceStatusID)).Count().ToString();

                    var transactionquery = MastertransactionsQuery.Where(row => row.MGRStatus == "Expiring"
                    && cstatusId.Contains((int)row.ComplianceStatusID)).ToList();


                    if (transactionquery.Count > 0)
                        btnExportExcelExpiring.Visible = true;
                    else
                        btnExportExcelExpiring.Visible = false;

                    if (transactionquery.Count > 2)
                        lnkShowDetailLicense.Visible = true;
                    else
                        lnkShowDetailLicense.Visible = false;
                   

                    grdLicenseExpiring.DataSource = transactionquery;
                    grdLicenseExpiring.DataBind();

                    Session["grdLicenseExpiringData"] = null;
                    Session["grdLicenseExpiringData"] = (grdLicenseExpiring.DataSource as List<SP_LicenseInstanceTransactionCount_Result>).ToDataTable();

                    var transactionqueryExpired = MastertransactionsQuery.Where(row => row.MGRStatus == "Expired"
                    && cstatusId.Contains((int)row.ComplianceStatusID)).ToList();

                    if (transactionqueryExpired.Count > 0)
                        btnExportExcelExpired.Visible = true;
                    else
                        btnExportExcelExpired.Visible = false;

                    if (transactionqueryExpired.Count > 2)
                        lnkLicExpiredshowMore.Visible = true;
                    else
                        lnkLicExpiredshowMore.Visible = false;

                    grdLicenseExpired.DataSource = transactionqueryExpired;
                    grdLicenseExpired.DataBind();

                    Session["grdLicenseExpiredData"] = null;
                    Session["grdLicenseExpiredData"] = (grdLicenseExpired.DataSource as List<SP_LicenseInstanceTransactionCount_Result>).ToDataTable();

                }
                else
                {
                    divExpiringcount.InnerText = "0";
                    divActiveCount.InnerText = "0";
                    divExpiredCount.InnerText = "0";
                    divAppliedcount.InnerText = "0";
                    grdLicenseExpiring.DataSource = null;
                    grdLicenseExpiring.DataBind();
                    grdLicenseExpired.DataSource = null;
                    grdLicenseExpired.DataBind();
                    lnkLicExpiredshowMore.Visible = false;
                    lnkShowDetailLicense.Visible = false;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        protected void lnkBtnTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                    long taskID = Convert.ToInt64(commandArgs[0]);
                    long licenseID = Convert.ToInt64(commandArgs[1]);
                    int roleID = Convert.ToInt32(commandArgs[2]);

                    if (taskID != 0 && licenseID != 0)
                    {
                        var strLicenseID = CryptographyManagement.Encrypt(licenseID.ToString());
                        var strTaskID = CryptographyManagement.Encrypt(taskID.ToString());
                        var strUserID = CryptographyManagement.Encrypt(AuthenticationHelper.UserID.ToString());
                        var strRoleID = CryptographyManagement.Encrypt(roleID.ToString());

                        string checkSum = Util.CalculateMD5Hash(licenseID.ToString() + taskID.ToString() + AuthenticationHelper.UserID.ToString() + roleID.ToString());

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowTaskDialog('" + strLicenseID + "','" + strTaskID + "','" + strUserID + "','" + strRoleID + "','" + checkSum + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLicenseDashboard.IsValid = false;
                cvLicenseDashboard.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        #region Graphs

        #region Statutory
        public void BindExpiredGriph(List<SP_LicenseInstanceTransactionCount_Result> MastertransactionsQuery) //filteredRecords
        {
            try
            {
                List<int> statusId = new List<int>();
                statusId.Add(4);
                statusId.Add(5);
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                seriesData_GraphLicenseExpired = null;
                int branchID = -1;
                int deptID = -1;
                long typeID = -1;

                if (MastertransactionsQuery.Count > 0)
                {
                    //Filters
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                    if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                    {
                        typeID = Convert.ToInt64(ddlLicenseType.SelectedValue);
                    }

                    if (branchList.Count > 0)
                        MastertransactionsQuery = MastertransactionsQuery.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (deptID != -1)
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (typeID != 0 && typeID != -1)
                    {
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => entry.LicenseTypeID == typeID).ToList();
                    }

                    if (MastertransactionsQuery.Count > 0)
                    {
                        MastertransactionsQuery = MastertransactionsQuery.OrderBy(entry => entry.EndDate).ToList();
                    }
                    MastertransactionsQuery = MastertransactionsQuery.Where(entry => entry.RoleID == 3).ToList();

                    List<int> cstatusId = new List<int>();
                    cstatusId.Add(1);
                    cstatusId.Add(2);
                    cstatusId.Add(3);
                    cstatusId.Add(6);
                    cstatusId.Add(8);
                    cstatusId.Add(10);


                    var expiring = MastertransactionsQuery.Where(row => (row.MGRStatus == "Expiring" || row.MGRStatus == "Active")
                    && row.EndDate.Value.Subtract(TimeSpan.FromDays((double)row.RemindBeforeNoOfDays)) < DateTime.Now
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count();


                    var expired = MastertransactionsQuery.Where(row => row.MGRStatus == "Expired"
                    //&& !statusId.Contains((int)row.ComplianceStatusID)).Count();
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count();

                    var Applied = MastertransactionsQuery.Where(row => row.MGRStatus == "Applied"
                    && statusId.Contains((int)row.ComplianceStatusID)                    
                    //&& row.EndDate < DateTime.Now //comment by rahul on 5 June 2020 because upcoming applied not display
                    ).Count();

                    var active = MastertransactionsQuery.Where(row => row.MGRStatus == "Active"
                    //&& !statusId.Contains((int)row.ComplianceStatusID)).Count();
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count();

                    seriesData_GraphLicenseStatus = string.Empty;
                    if (expiring > 0)
                    {
                        seriesData_GraphLicenseExpired += "{name:'Application Overdue (Expiring but not applied)', y:" + expiring + ",color:'#1d86c8'," +
                         "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Statutory','ApplicationOverduebutnotapplied') }} },";
                    }
                    else
                    {
                        seriesData_GraphLicenseExpired += "{name:'Application Overdue (Expiring but not applied)', y:0,color:'#1d86c8'," +
                         "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Statutory','ApplicationOverduebutnotapplied') }} },";
                    }
                    if (expired > 0)
                    {
                        seriesData_GraphLicenseExpired += "{name:'Expired', y:" + expired + ",color:'red'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Statutory','Expired') }} },";
                    }
                    else
                    {
                        seriesData_GraphLicenseExpired += "{name:'Expired', y:0,color:'red'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Statutory','Expired') }} },";
                    }
                    if (Applied > 0)
                    {
                        seriesData_GraphLicenseExpired += "{name:'Expired applied but not renewed', y:" + Applied + ",color:'#ffcd70'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Statutory','Expiredappliedbutnotrenewed') }} },";
                    }
                    else
                    {
                        seriesData_GraphLicenseExpired += "{name:'Expired applied but not renewed', y:0,color:'#ffcd70'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Statutory','Expiredappliedbutnotrenewed') }} },";
                    }
                    if (active > 0)
                    {
                        seriesData_GraphLicenseExpired += "{name:'Active', y:" + active + ",color:'#1FD9E1'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Statutory','Active') }} },";
                    }
                    else
                    {
                        seriesData_GraphLicenseExpired += "{name:'Active', y:0,color:'#1FD9E1'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Statutory','Active') }} },";
                    }
                    seriesData_GraphLicenseExpired = seriesData_GraphLicenseExpired.Trim(',');
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void ShowChart_LicenseType(List<SP_LicenseInstanceTransactionCount_Result> lstMasterRecords)
        {
            try
            {
                List<int> cstatusId = new List<int>();
                cstatusId.Add(1);
                cstatusId.Add(2);
                cstatusId.Add(3);
                cstatusId.Add(6);
                cstatusId.Add(8);
                cstatusId.Add(10);

                List<int> statusId = new List<int>();
                statusId.Add(4);
                statusId.Add(5);
                graph_LicenseType_Categories = string.Empty;
                seriesData_GraphLicenseType = string.Empty;
                int branchID = -1;
                int deptID = -1;
                long typeID = -1;
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (lstMasterRecords.Count > 0)
                {

                    //Filters
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }

                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);


                    if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                    {
                        typeID = Convert.ToInt64(ddlLicenseType.SelectedValue);
                    }

                    if (branchList.Count > 0)
                        lstMasterRecords = lstMasterRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (deptID != -1)
                        lstMasterRecords = lstMasterRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (typeID != 0 && typeID != -1)
                    {
                        lstMasterRecords = lstMasterRecords.Where(entry => entry.LicenseTypeID == typeID).ToList();
                    }

                    if (lstMasterRecords.Count > 0)
                    {
                        lstMasterRecords = lstMasterRecords.OrderBy(entry => entry.EndDate).ToList();
                    }


                    lstMasterRecords = lstMasterRecords.Where(entry => entry.RoleID == 3).ToList();
                    if (lstMasterRecords.Count > 0)
                    {
                        var lstDistinctStatus = (from g in lstMasterRecords
                                                 select new SP_LicenseInstanceTransactionCount_Result()
                                                 {
                                                     LicenseID = g.LicenseID,
                                                     LicenseTypeID = g.LicenseTypeID,
                                                     LicensetypeName = g.LicensetypeName,
                                                     EndDate = g.EndDate,
                                                     RemindBeforeNoOfDays = g.RemindBeforeNoOfDays,
                                                     Status = g.Status,
                                                     ComplianceStatusID = g.ComplianceStatusID,
                                                     MGRStatus = g.MGRStatus,
                                                 }).ToList();

                        string highCountList = string.Empty;
                        string lowCountList = string.Empty;
                        string mediumCountList = string.Empty;
                        string activeCountList = string.Empty;

                        var LicenseTypeList = (from row in lstDistinctStatus

                                               select new { row.LicenseTypeID, row.LicensetypeName }).Distinct().ToList();

                        LicenseTypeList.ForEach(typeL =>
                        {
                            var totalLicenseStatusWise = (from row in lstDistinctStatus
                                                          where row.LicenseTypeID == typeL.LicenseTypeID
                                                          select row).Count();

                            if (totalLicenseStatusWise > 0)
                            {
                                graph_LicenseType_Categories += "'" + typeL.LicensetypeName + "',";

                                var expiring = lstDistinctStatus.Where(row => row.LicenseTypeID == typeL.LicenseTypeID
                                && (row.MGRStatus == "Expiring" || row.MGRStatus == "Active")
                                && row.EndDate.Value.Subtract(TimeSpan.FromDays((double)row.RemindBeforeNoOfDays)) < DateTime.Now
                                && cstatusId.Contains((int)row.ComplianceStatusID)).Count();

                                var expired = lstDistinctStatus.Where(row => row.LicenseTypeID == typeL.LicenseTypeID
                                && row.MGRStatus == "Expired"
                                //&& !statusId.Contains((int)row.ComplianceStatusID)).Count();
                                && cstatusId.Contains((int)row.ComplianceStatusID)).Count();

                                var Applied = lstDistinctStatus.Where(row => row.LicenseTypeID == typeL.LicenseTypeID
                                && row.MGRStatus == "Applied" 
                                //&& row.EndDate < DateTime.Now //comment by rahul on 5 June 2020 because upcoming applied not display
                                && statusId.Contains((int)row.ComplianceStatusID)).Count();

                                var active = lstDistinctStatus.Where(row => row.LicenseTypeID == typeL.LicenseTypeID
                                && row.MGRStatus == "Active"
                                && cstatusId.Contains((int)row.ComplianceStatusID)).Count();
                                //&& !statusId.Contains((int)row.ComplianceStatusID)).Count();

                                if (expiring > 0)
                                {
                                    highCountList += "{ y:" + expiring + ",events: { click: function(e) {  ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Statutory','ApplicationOverduebutnotapplied') }} },";
                                }
                                //else part added by rahul on 5 June 2020
                                else
                                {
                                    highCountList += "{ y:0,events: { click: function(e) {  ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Statutory','ApplicationOverduebutnotapplied') }} },";
                                }
                                if (expired > 0)
                                {
                                    mediumCountList += "{ y:" + expired + ",events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Statutory','Expired')}} },";
                                }
                                //else part added by rahul on 5 June 2020
                                else
                                {
                                    mediumCountList += "{ y:0,events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Statutory','Expired')}} },";
                                }
                                if (Applied > 0)
                                {
                                    lowCountList += "{ y:" + Applied + ",events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Statutory','Expiredappliedbutnotrenewed') }} },";
                                }
                                //else part added by rahul on 5 June 2020
                                else
                                {
                                    lowCountList += "{ y:0,events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Statutory','Expiredappliedbutnotrenewed') }} },";
                                }
                                //if (active > 0)
                                //{
                                //    lowCountList += "{ y:" + Applied + ",events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Statutory','Expiredappliedbutnotrenewed') }} },";
                                //}
                                if (active > 0)
                                {
                                    activeCountList += "{ y:" + active + ",events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Statutory','Active') }} },";
                                }
                                else
                                {
                                    activeCountList += "{ y:0,events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Statutory','Active') }} },";
                                }
                               // seriesData_GraphLicenseType += " { y: " + totalLicenseStatusWise + ",events: { click: function(e) { ShowGraphDetail('B', 'B', -1, -1, 12, -1, 1, -1, 0, 0) } } },";
                            }
                        });
                        highCountList = "{ name: 'Application Overdue (Expiring but not applied)', color: '#1d86c8', data: [" + highCountList.Trim(',') + "] },";
                        mediumCountList = "{ name: 'Expired', color: 'red', data: [" + mediumCountList.Trim(',') + "] },";
                        lowCountList = "{ name: 'Expired applied but not renewed', color: '#ffcd70', data: [" + lowCountList.Trim(',') + "] },";
                        activeCountList = "{ name: 'Active', color: '#1FD9E1', data: [" + activeCountList.Trim(',') + "] },";

                        seriesData_GraphLicenseType = highCountList + mediumCountList + lowCountList + activeCountList;
                        seriesData_GraphLicenseType = seriesData_GraphLicenseType.Trim(',');
                        graph_LicenseType_Categories = graph_LicenseType_Categories.Trim(',');
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Internal
        public void BindExpiredGriphInternal(List<SP_InternalLicenseInstanceTransactionCount_Result> MastertransactionsQuery) //filteredRecords
        {
            try
            {
                List<int> statusId = new List<int>();
                statusId.Add(4);
                statusId.Add(5);
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                seriesData_GraphLicenseExpired = null;
                int branchID = -1;
                int deptID = -1;
                long typeID = -1;

                if (MastertransactionsQuery.Count > 0)
                {
                    //Filters
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                    if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                    {
                        typeID = Convert.ToInt64(ddlLicenseType.SelectedValue);
                    }

                    if (branchList.Count > 0)
                        MastertransactionsQuery = MastertransactionsQuery.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (deptID != -1)
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (typeID != 0 && typeID != -1)
                    {
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => entry.LicenseTypeID == typeID).ToList();
                    }

                    if (MastertransactionsQuery.Count > 0)
                    {
                        MastertransactionsQuery = MastertransactionsQuery.OrderBy(entry => entry.EndDate).ToList();
                    }
                    MastertransactionsQuery = MastertransactionsQuery.Where(entry => entry.RoleID == 3).ToList();

                    List<int> cstatusId = new List<int>();
                    cstatusId.Add(1);
                    cstatusId.Add(2);
                    cstatusId.Add(3);
                    cstatusId.Add(6);
                    cstatusId.Add(8);
                    cstatusId.Add(10);


                    var expiring = MastertransactionsQuery.Where(row => (row.MGRStatus == "Expiring" || row.MGRStatus == "Active")
                    && row.EndDate.Value.Subtract(TimeSpan.FromDays((double)row.RemindBeforeNoOfDays)) < DateTime.Now
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count();


                    var expired = MastertransactionsQuery.Where(row => row.MGRStatus == "Expired"
                    //&& !statusId.Contains((int)row.ComplianceStatusID)).Count();
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count();

                    var Applied = MastertransactionsQuery.Where(row => row.MGRStatus == "Applied"
                    && statusId.Contains((int)row.ComplianceStatusID)                     
                    //&& row.EndDate < DateTime.Now //comment by rahul on 5 June 2020 because upcoming applied not display
                    ).Count();

                    var active = MastertransactionsQuery.Where(row => row.MGRStatus == "Active"
                    //&& !statusId.Contains((int)row.ComplianceStatusID)).Count();
                    && cstatusId.Contains((int)row.ComplianceStatusID)).Count();

                    seriesData_GraphLicenseStatus = string.Empty;
                    if (expiring > 0)
                    {
                        seriesData_GraphLicenseExpired += "{name:'Application Overdue (Expiring but not applied)', y:" + expiring + ",color:'#1d86c8'," +
                         "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Internal','ApplicationOverduebutnotapplied') }} },";
                    }
                    else
                    {
                        seriesData_GraphLicenseExpired += "{name:'Application Overdue (Expiring but not applied)', y:0,color:'#1d86c8'," +
                         "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Internal','ApplicationOverduebutnotapplied') }} },";
                    }
                    if (expired > 0)
                    {
                        seriesData_GraphLicenseExpired += "{name:'Expired', y:" + expired + ",color:'red'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Internal','Expired') }} },";
                    }
                    else
                    {
                        seriesData_GraphLicenseExpired += "{name:'Expired', y:0,color:'red'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Internal','Expired') }} },";
                    }
                    if (Applied > 0)
                    {
                        seriesData_GraphLicenseExpired += "{name:'Expired applied but not renewed', y:" + Applied + ",color:'#ffcd70'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Internal','Expiredappliedbutnotrenewed') }} },";
                    }
                    else
                    {
                        seriesData_GraphLicenseExpired += "{name:'Expired applied but not renewed', y:0,color:'#ffcd70'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Internal','Expiredappliedbutnotrenewed') }} },";
                    }
                    if (active > 0)
                    {
                        seriesData_GraphLicenseExpired += "{name:'Active', y:" + active + ",color:'#1FD9E1'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Internal','Active') }} },";
                    }
                    else
                    {
                        seriesData_GraphLicenseExpired += "{name:'Active', y:0,color:'#1FD9E1'," +
                          "events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeID + "','Internal','Active') }} },";
                    }
                    seriesData_GraphLicenseExpired = seriesData_GraphLicenseExpired.Trim(',');
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void ShowChart_InternalLicenseType(List<SP_InternalLicenseInstanceTransactionCount_Result> lstMasterRecords)
        {
            try
            {
                List<int> cstatusId = new List<int>();
                cstatusId.Add(1);
                cstatusId.Add(2);
                cstatusId.Add(3);
                cstatusId.Add(6);
                cstatusId.Add(8);
                cstatusId.Add(10);

                List<int> statusId = new List<int>();
                statusId.Add(4);
                statusId.Add(5);
                graph_LicenseType_Categories = string.Empty;
                seriesData_GraphLicenseType = string.Empty;
                int branchID = -1;
                int deptID = -1;
                long typeID = -1;
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (lstMasterRecords.Count > 0)
                {

                    //Filters
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }

                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);


                    if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                    {
                        typeID = Convert.ToInt64(ddlLicenseType.SelectedValue);
                    }

                    if (branchList.Count > 0)
                        lstMasterRecords = lstMasterRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (deptID != -1)
                        lstMasterRecords = lstMasterRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                    if (typeID != 0 && typeID != -1)
                    {
                        lstMasterRecords = lstMasterRecords.Where(entry => entry.LicenseTypeID == typeID).ToList();
                    }

                    if (lstMasterRecords.Count > 0)
                    {
                        lstMasterRecords = lstMasterRecords.OrderBy(entry => entry.EndDate).ToList();
                    }


                    lstMasterRecords = lstMasterRecords.Where(entry => entry.RoleID == 3).ToList();
                    if (lstMasterRecords.Count > 0)
                    {
                        var lstDistinctStatus = (from g in lstMasterRecords
                                                 select new SP_InternalLicenseInstanceTransactionCount_Result()
                                                 {
                                                     LicenseID = g.LicenseID,
                                                     LicenseTypeID = g.LicenseTypeID,
                                                     LicensetypeName = g.LicensetypeName,
                                                     EndDate = g.EndDate,
                                                     RemindBeforeNoOfDays = g.RemindBeforeNoOfDays,
                                                     Status = g.Status,
                                                     ComplianceStatusID = g.ComplianceStatusID,
                                                     MGRStatus = g.MGRStatus,
                                                 }).ToList();

                        string highCountList = string.Empty;
                        string lowCountList = string.Empty;
                        string mediumCountList = string.Empty;
                        string activeCountList = string.Empty;

                        var LicenseTypeList = (from row in lstDistinctStatus

                                               select new { row.LicenseTypeID, row.LicensetypeName }).Distinct().ToList();

                        LicenseTypeList.ForEach(typeL =>
                        {
                            var totalLicenseStatusWise = (from row in lstDistinctStatus
                                                          where row.LicenseTypeID == typeL.LicenseTypeID
                                                          select row).Count();

                            if (totalLicenseStatusWise > 0)
                            {
                                graph_LicenseType_Categories += "'" + typeL.LicensetypeName + "',";

                                var expiring = lstDistinctStatus.Where(row => row.LicenseTypeID == typeL.LicenseTypeID
                                && (row.MGRStatus == "Expiring" || row.MGRStatus == "Active")
                                && row.EndDate.Value.Subtract(TimeSpan.FromDays((double)row.RemindBeforeNoOfDays)) < DateTime.Now
                                && cstatusId.Contains((int)row.ComplianceStatusID)).Count();

                                var expired = lstDistinctStatus.Where(row => row.LicenseTypeID == typeL.LicenseTypeID
                                && row.MGRStatus == "Expired"
                                //&& !statusId.Contains((int)row.ComplianceStatusID)).Count();
                                && cstatusId.Contains((int)row.ComplianceStatusID)).Count();

                                var Applied = lstDistinctStatus.Where(row => row.LicenseTypeID == typeL.LicenseTypeID
                                && row.MGRStatus == "Applied"
                                //&& row.EndDate < DateTime.Now //comment by rahul on 5 June 2020 because upcoming applied not display                                
                                && statusId.Contains((int)row.ComplianceStatusID)).Count();

                                var active = lstDistinctStatus.Where(row => row.LicenseTypeID == typeL.LicenseTypeID
                                && row.MGRStatus == "Active"
                                && cstatusId.Contains((int)row.ComplianceStatusID)).Count();
                                //&& !statusId.Contains((int)row.ComplianceStatusID)).Count();

                                if (expiring > 0)
                                {
                                    highCountList += "{ y:" + expiring + ",events: { click: function(e) {  ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Internal','ApplicationOverduebutnotapplied') }} },";
                                }
                                //else part added by rahul on 5 June 2020
                                else
                                {
                                    highCountList += "{ y:0,events: { click: function(e) {  ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Internal','ApplicationOverduebutnotapplied') }} },";
                                }
                                if (expired > 0)
                                {
                                    mediumCountList += "{ y:" + expired + ",events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Internal','Expired')}} },";
                                }
                                //else part added by rahul on 5 June 2020
                                else
                                {
                                    mediumCountList += "{ y:0,events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Internal','Expired')}} },";
                                }
                                if (Applied > 0)
                                {
                                    lowCountList += "{ y:" + Applied + ",events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Internal','Expiredappliedbutnotrenewed') }} },";
                                }
                                //else part added by rahul on 5 June 2020
                                else
                                {
                                    lowCountList += "{ y:0,events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Internal','Expiredappliedbutnotrenewed') }} },";
                                }
                                //if (active > 0)
                                //{
                                //    lowCountList += "{ y:" + Applied + ",events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Internal','Expiredappliedbutnotrenewed') }} },";
                                //}
                                if (active > 0)
                                {
                                    activeCountList += "{ y:" + active + ",events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Internal','Active') }} },";
                                }
                                else
                                {
                                    activeCountList += "{ y:0,events: { click: function(e) { ShowGraphDetail('" + branchID + "','" + deptID + "','','" + typeL.LicenseTypeID + "','Internal','Active') }} },";

                                }
                                seriesData_GraphLicenseType += " { y: " + totalLicenseStatusWise + ",events: { click: function(e) { ShowGraphDetail('B', 'B', -1, -1, 12, -1, 1, -1, 0, 0) } } },";
                            }
                        });
                        highCountList = "{ name: 'Application Overdue (Expiring but not applied)', color: '#1d86c8', data: [" + highCountList.Trim(',') + "] },";
                        mediumCountList = "{ name: 'Expired', color: 'red', data: [" + mediumCountList.Trim(',') + "] },";
                        lowCountList = "{ name: 'Expired applied but not renewed', color: '#ffcd70', data: [" + lowCountList.Trim(',') + "] },";
                        activeCountList = "{ name: 'Active', color: '#1FD9E1', data: [" + activeCountList.Trim(',') + "] },";

                        seriesData_GraphLicenseType = highCountList + mediumCountList + lowCountList + activeCountList;
                        seriesData_GraphLicenseType = seriesData_GraphLicenseType.Trim(',');
                        graph_LicenseType_Categories = graph_LicenseType_Categories.Trim(',');
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #endregion

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnExportExcelExpired_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    if (Session["grdLicenseExpiredData"] != null)
                    {
                        String FileName = String.Empty;
                        FileName = "LicenseExpiredReport";
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView((DataTable)Session["grdLicenseExpiredData"]);
                        ExcelData = view.ToTable("Selected", false, "CustomerBrach", "LicenseNo", "LicenseTitle", "LicensetypeName", "MGRStatus", "StartDate", "EndDate");
                        if (ExcelData.Rows.Count > 0)
                        {
                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);
                            int rowCount = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;
                                if (item["StartDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["StartDate"])))
                                    {
                                        item["StartDate"] = Convert.ToDateTime(item["StartDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                                if (item["EndDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["EndDate"])))
                                    {
                                        item["EndDate"] = Convert.ToDateTime(item["EndDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                            }
                            exWorkSheet.Cells["A1:B1"].Merge = true;
                            exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["C1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                            exWorkSheet.Cells["C1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A2:C2"].Merge = true;
                            exWorkSheet.Cells["A2"].Value = CustomerManagement.CustomerGetByIDName((int)AuthenticationHelper.CustomerID);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A3:C3"].Merge = true;
                            exWorkSheet.Cells["A3"].Value = "License Report";
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].AutoFitColumns(15);

                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A5"].Value = "S.No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(5);

                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B5"].Value = "Entity/Branch/Location";
                            exWorkSheet.Cells["B5"].AutoFitColumns(40);

                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C5"].Value = "License No";
                            exWorkSheet.Cells["C5"].AutoFitColumns(40);

                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D5"].Value = "License Title";
                            exWorkSheet.Cells["D5"].AutoFitColumns(40);

                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E5"].Value = "License Type";
                            exWorkSheet.Cells["E5"].AutoFitColumns(40);

                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F5"].Value = "Status";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);

                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G5"].Value = "Start Date";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);

                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["H5"].Value = "End Date";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);


                            //Assign borders
                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 8])
                            {
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.WrapText = true;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                            using (ExcelRange col = exWorkSheet.Cells[5, 7, 7 + ExcelData.Rows.Count, 8])
                            {
                                col.Style.Numberformat.Format = "dd/MM/yyyy";
                            }
                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }                       
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnExportExcelExpiring_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    if (Session["grdLicenseExpiringData"] != null)
                    {
                        String FileName = String.Empty;
                        FileName = "LicenseExpiringReport";
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView((DataTable)Session["grdLicenseExpiringData"]);
                        ExcelData = view.ToTable("Selected", false, "CustomerBrach", "LicenseNo", "LicenseTitle", "LicensetypeName", "MGRStatus", "StartDate", "EndDate");
                        if (ExcelData.Rows.Count > 0)
                        {
                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);
                            int rowCount = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;
                                if (item["StartDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["StartDate"])))
                                    {
                                        item["StartDate"] = Convert.ToDateTime(item["StartDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                                if (item["EndDate"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(item["EndDate"])))
                                    {
                                        item["EndDate"] = Convert.ToDateTime(item["EndDate"]).ToString("dd-MMM-yyyy");
                                    }
                                }
                            }
                            exWorkSheet.Cells["A1:B1"].Merge = true;
                            exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["C1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                            exWorkSheet.Cells["C1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A2:C2"].Merge = true;
                            exWorkSheet.Cells["A2"].Value = CustomerManagement.CustomerGetByIDName((int)AuthenticationHelper.CustomerID);
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A3:C3"].Merge = true;
                            exWorkSheet.Cells["A3"].Value = "License Report";
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].AutoFitColumns(15);

                            exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A5"].Value = "S.No.";
                            exWorkSheet.Cells["A5"].AutoFitColumns(5);

                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B5"].Value = "Entity/Branch/Location";
                            exWorkSheet.Cells["B5"].AutoFitColumns(40);

                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C5"].Value = "License No";
                            exWorkSheet.Cells["C5"].AutoFitColumns(40);

                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D5"].Value = "License Title";
                            exWorkSheet.Cells["D5"].AutoFitColumns(40);

                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E5"].Value = "License Type";
                            exWorkSheet.Cells["E5"].AutoFitColumns(40);

                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F5"].Value = "Status";
                            exWorkSheet.Cells["F5"].AutoFitColumns(20);

                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G5"].Value = "Start Date";
                            exWorkSheet.Cells["G5"].AutoFitColumns(20);

                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                            exWorkSheet.Cells["H5"].Value = "End Date";
                            exWorkSheet.Cells["H5"].AutoFitColumns(20);


                            //Assign borders
                            using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 8])
                            {                                
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.WrapText = true;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                            using (ExcelRange col = exWorkSheet.Cells[5, 7, 7 + ExcelData.Rows.Count, 8])
                            {
                                col.Style.Numberformat.Format = "dd/MM/yyyy";
                            }
                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        else
                        {
                            //cvGraphDetail.IsValid = false;
                            //cvGraphDetail.ErrorMessage = "No data available to export for current selection(s)";
                            //cvGraphDetail.CssClass = "alert alert-danger;";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}