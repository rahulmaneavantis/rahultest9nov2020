﻿using System;
using System.Web;
using System.Web.UI;

using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Business.License;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Masters
{
    public partial class AddNewDepartment : System.Web.UI.Page
    {
        //private long CustomerID = AuthenticationHelper.CustomerID;
        private long CustomerID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(Request.QueryString["DepartmentID"]))
                {
                     
                   
                    if (Convert.ToInt32(Request.QueryString["DepartmentID"]) > 0)
                    {
                        int DeptID = Convert.ToInt32(Request.QueryString["DepartmentID"]);
                        Department RPD = CompDeptManagement.DepartmentMasterGetByID(DeptID, CustomerID);//, CustomerID
                        txtFName.Text = RPD.Name;
                       
                        ViewState["DeptID"] = DeptID;
                        ViewState["Mode"] = 1;
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }                   
                  
                }
                else
                {
                    ViewState["Mode"] = 0;
                }
               
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtFName.Text.Trim()))
                {
                    Department objDeptcompliance = new Department()
                    {
                        Name = txtFName.Text.Trim(),
                        IsDeleted = false,
                        CustomerID = (int)CustomerID
                    };
                    mst_Department objDeptaudit = new mst_Department()
                    {
                        Name = txtFName.Text.Trim(),
                        IsDeleted = false,
                        CustomerID = (int)CustomerID
                    };
                    if ((int)ViewState["Mode"] == 1)
                    {
                        objDeptcompliance.ID = Convert.ToInt32(ViewState["DeptID"]);
                        objDeptaudit.ID = Convert.ToInt32(ViewState["DeptID"]);
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (CompDeptManagement.DepartmentExists(objDeptcompliance))
                        {
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Already Exists";
                        }
                        else
                        {
                            int newDeptID = 0;
                            newDeptID = CompDeptManagement.CreateComplianceDepartmentMaster(objDeptcompliance);
                            if (newDeptID > 0)
                            {
                                cvDeptPopup.IsValid = false;
                                cvDeptPopup.ErrorMessage = "Department Save Successfully.";
                                vsAddEditDept.CssClass = "alert alert-success";

                                txtFName.Text = string.Empty;
                                Session["DID"] = newDeptID;
                            }
                        }
                        if (CompDeptManagement.DepartmentExistsAudit(objDeptaudit))
                        {
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Already Exists.";
                        }
                        else
                        {
                            CompDeptManagement.CreateDepartmentMasterAudit(objDeptaudit);
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Save Successfully.";
                            vsAddEditDept.CssClass = "alert alert-success";
                            txtFName.Text = string.Empty;
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        if (CompDeptManagement.DepartmentExists(objDeptcompliance))
                        {
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Already Exists";
                        }
                        else
                        {
                            CompDeptManagement.UpdateDepartmentMaster(objDeptcompliance);

                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Updated Successfully.";
                            vsAddEditDept.CssClass = "alert alert-success";
                        }

                        if (CompDeptManagement.DepartmentExistsAudit(objDeptaudit))
                        {
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department already Exists.";
                        }
                        else
                        {
                            CompDeptManagement.UpdateDepartmentMasterAudit(objDeptaudit);
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Department Updated Successfully";
                            vsAddEditDept.CssClass = "alert alert-success";
                        }
                    }

                }
                else
                {
                    cvDeptPopup.IsValid = false;
                    cvDeptPopup.ErrorMessage = "Please Enter Department.";
                    vsAddEditDept.CssClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDeptPopup.IsValid = false;
                cvDeptPopup.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        [WebMethod]
        public static string getDepatmentID()
        {
            string depatmentID = string.Empty;
            try
            {

                depatmentID = LicenseMastersManagement.GetDepartmenID();
                return depatmentID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return depatmentID;
            }
        }
        protected void btnCancelDeptPopUp_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}