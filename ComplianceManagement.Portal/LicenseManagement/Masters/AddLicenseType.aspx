﻿<%@ Page Title="License Type Master :: License" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="AddLicenseType.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Masters.AddLicenseType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upLicenseList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                    ValidationGroup="LicenseValidationGroupTop" />
                <asp:CustomValidator ID="cvDuplicateEntryTop" runat="server" EnableClientScript="False"
                    ValidationGroup="LicenseValidationGroupTop" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td align="right" style="display: none;">                        
                    </td>
                    <td align="right" style="width: 20%"></td>
                    <td align="right" style="width: 25%">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddLicense" OnClick="btnAddLicense_Click" Visible="false" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdLicense" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdLicense_RowDataBound"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdLicense_RowCreated"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" Width="100%" OnSorting="grdLicense_Sorting"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdLicense_RowCommand" OnPageIndexChanging="grdLicense_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="License ID" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10px" />
                    <asp:TemplateField HeaderText="Name" ItemStyle-Height="25px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" SortExpression="Name">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px">
                                <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sub Entity">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="VIEW_Compliances" CommandArgument='<%# Eval("ID") %>'>compliances</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_License" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon.png" alt="Edit License" title="Edit License" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_License" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this License type?');"><img src="../../Images/delete_icon.png" alt="Delete License" title="Delete License" /></asp:LinkButton>

                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divLicenseDialog">
        <asp:UpdatePanel ID="upLicense" runat="server" UpdateMode="Conditional" OnLoad="upLicense_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="LicenseValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="LicenseValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 141px; display: block; float: left; font-size: 13px; color: #333;">
                            Type</label>
                        <asp:RadioButtonList ID="rdbLicenseType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="rdbLicenseType_SelectedIndexChanged">
                            <asp:ListItem Text="Statutory" Value="S" Selected="True" />                            
                        </asp:RadioButtonList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            License Type Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 35px; width: 430px;"
                            ToolTip="" />
                        <asp:RequiredFieldValidator ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="LicenseValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Comma Separated Compliance ID:
                        </label>
                        <asp:TextBox runat="server" ID="txtComplianceIDList" TextMode="MultiLine"
                            Style="height: 100px; width: 430px;" />

                        <asp:RegularExpressionValidator ID="regValidator" runat="server"
                            ControlToValidate="txtComplianceIDList" ErrorMessage="Please Enter Valid Comma Separated ComplianceID !"
                            ValidationGroup="LicenseValidationGroup" Display="None"
                            ValidationExpression="^([0-9\,]+)">Please Enter Valid Compliance ID</asp:RegularExpressionValidator>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="txtComplianceIDList" Display="None"
                            ValidationGroup="LicenseValidationGroup"
                            ErrorMessage="please enter comma separated ComplianceID"></asp:RequiredFieldValidator>


                    </div>
                    <div style="margin-bottom: 14px; margin-left: 163px; margin-top: 48px">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="LicenseValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button"
                            OnClientClick="$('#divLicenseDialog').dialog('close');" />
                    </div>

                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">

                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divLicenseDialog').dialog({
                height: 420,
                width: 680,
                autoOpen: false,
                draggable: true,
                title: "License",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
