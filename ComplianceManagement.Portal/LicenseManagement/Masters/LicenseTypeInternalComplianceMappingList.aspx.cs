﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using System.Drawing;
using static com.VirtuosoITech.ComplianceManagement.Business.License.LicenseTypeMasterManagement;
using static com.VirtuosoITech.ComplianceManagement.Business.License.InternalLicenseMgmt;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Masters
{
    public partial class LicenseTypeInternalComplianceMappingList : System.Web.UI.Page
    {
        protected static long customerID = 0;
        protected static string user_Roles;
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                BindCustomerFilter();          
                BindMapping();
                Session["CurrentRole"] = AuthenticationHelper.Role;
                Session["CurrentUserId"] = AuthenticationHelper.UserID;               
            }
        }
        private void BindCustomerFilter()
        {
            List<Customer> lstCustomers = CustomerManagement.GetAll(-1, null);

            if (lstCustomers.Count > 0)
                lstCustomers = lstCustomers.OrderBy(entry => entry.Name).ToList();

            ddlCustomer.DataTextField = "Name";
            ddlCustomer.DataValueField = "ID";

            ddlCustomer.DataSource = lstCustomers;
            ddlCustomer.DataBind();
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindLicenseTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindLicenseTypes()
        {
            try
            {
                user_Roles = AuthenticationHelper.Role;
                if (user_Roles.Contains("IMPT"))
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                        customerID = Convert.ToInt64(ddlCustomer.SelectedValue);
                }
                else if (user_Roles.Contains("CADMN"))
                {
                    customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }
                else if (user_Roles.Contains("MGMT"))
                {
                    customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }
                else if (user_Roles.Contains("EXCT"))
                {
                    customerID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                }
                ddlFilterLicenseType.DataSource = null;
                ddlFilterLicenseType.DataBind();                
                var data = LicenseTypeMasterManagement.GetInternalLicenseType(customerID);                
                ddlFilterLicenseType.DataTextField = "Name";
                ddlFilterLicenseType.DataValueField = "ID";
                ddlFilterLicenseType.DataSource = data;
                ddlFilterLicenseType.DataBind();
                ddlFilterLicenseType.Items.Insert(0, new ListItem("< All >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        

        #region Mapping Detail

        protected void grdMapping_RowCommand(object sender, GridViewCommandEventArgs e)
        {
           
        }

        public void BindMapping()
        {
            try
            {
                int licenseid = -1;
                if (ddlFilterLicenseType.SelectedValue != "-1")
                {
                    licenseid = Convert.ToInt32(ddlFilterLicenseType.SelectedValue);
                }
                else
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["LTMLID"]))
                    {                        
                        licenseid = Convert.ToInt32(Request.QueryString["LTMLID"]);
                        ddlFilterLicenseType.SelectedValue =Convert.ToString(licenseid);
                    }
                }
                var productmappinglist = InternalLicenseMgmt.GetAllLicenseMappingInternalCompliance(licenseid, tbxFilter.Text);                
                grdMapping.DataSource = productmappinglist;
                grdMapping.DataBind();
                Session["grdintDetailData"] = null;
                Session["grdintDetailData"] =(grdMapping.DataSource as List<LicenseInternalComplianceMap>).ToDataTable();
                upLicenseTypeComplianceMappingList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdMapping.PageIndex = e.NewPageIndex;
                BindMapping();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdMapping_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int licenseid = -1;
                if (ddlFilterLicenseType.SelectedValue != "-1")
                {
                    licenseid = Convert.ToInt32(ddlFilterLicenseType.SelectedValue);
                }                
                var productmappinglist = InternalLicenseMgmt.GetAllLicenseMappingInternalCompliance(licenseid, tbxFilter.Text);
                grdMapping.DataSource = productmappinglist;
                grdMapping.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdMapping_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
        }
        protected void grdMapping_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;
            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        #endregion
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdMapping.PageIndex = 0;
                BindMapping();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterLicenseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindMapping();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;
                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upLicenseTypeComplianceMappingList_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    if (Session["grdintDetailData"] != null)
                    {
                        String FileName = String.Empty;
                        FileName = "LicenseComplinaceReport";
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView((DataTable)Session["grdintDetailData"]);                        
                        ExcelData = view.ToTable("Selected", false, "ComplianceID", "ShortDescription", "LicenseTypeName", "Type");                        
                        if (ExcelData.Rows.Count > 0)
                        {
                            ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);
                            int rowCount = 0;
                            foreach (DataRow item in ExcelData.Rows)
                            {
                                item["SNo"] = ++rowCount;                             
                            }
                            exWorkSheet.Cells["A1:B1"].Merge = true;
                            exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["C1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                            exWorkSheet.Cells["C1"].Style.Font.Size = 12;

                            exWorkSheet.Cells["A2:C2"].Merge = true;
                            exWorkSheet.Cells["A2"].Value = "License Compliance Report";
                            exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                            //exWorkSheet.Cells["A3:C3"].Merge = true;
                            //exWorkSheet.Cells["A3"].Value =
                            //exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            //exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            //exWorkSheet.Cells["A3"].AutoFitColumns(15);

                            exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);

                            exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A4"].Value = "S.No.";
                            exWorkSheet.Cells["A4"].AutoFitColumns(5);
                            exWorkSheet.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A4"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["A4"].Style.WrapText = true;

                            exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B4"].Value = "ComplianceID";
                            exWorkSheet.Cells["B4"].AutoFitColumns(20);
                            exWorkSheet.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B4"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["B4"].Style.WrapText = true;

                            exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C4"].Value = "Short Description";
                            exWorkSheet.Cells["C4"].AutoFitColumns(100);
                            exWorkSheet.Cells["C4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C4"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["C4"].Style.WrapText = true;

                            exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D4"].Value = "License Type";
                            exWorkSheet.Cells["D4"].AutoFitColumns(40);
                            exWorkSheet.Cells["D4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D4"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["D4"].Style.WrapText = true;                           

                            exWorkSheet.Cells["E4"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E4"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E4"].Value = "Compliance Type";
                            exWorkSheet.Cells["E4"].AutoFitColumns(20);
                            exWorkSheet.Cells["E4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E4"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["E4"].Style.WrapText = true;

                            //Assign borders
                            using (ExcelRange col = exWorkSheet.Cells[4, 1, 4 + ExcelData.Rows.Count, 5])
                            {
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.WrapText = true;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }                            
                            Byte[] fileBytes = exportPackge.GetAsByteArray();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.ms-excel";
                            StringWriter sw = new StringWriter();
                            Response.BinaryWrite(fileBytes);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No data available to export for current selection(s)";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
