﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.LicenseManagement.Masters
{
    public partial class PageAuthorizationMasters : System.Web.UI.Page
    {
        protected bool flag = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUser();
            }
        }
        public void BindUser()
        {
            try
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var UserAllData = LicenseUserManagement.GetAllAuthorizeUsers_License(customerID);
                ddlUser.DataTextField = "Name";
                ddlUser.DataValueField = "ID";
                ddlUser.DataSource = UserAllData;
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem("Select User", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindLicenseTypeGrid()
        {
            long UserID = -1;
            grdDisplayUserdata.DataSource = null;
            grdDisplayUserdata.DataBind();
            Session["TotalRows"] = 0;
            btnSavePageAutorization.Visible = false;
            if (!string.IsNullOrEmpty((ddlUser.SelectedValue)))
            {
                if (ddlUser.SelectedValue != "-1")
                {
                    UserID = Convert.ToInt64(ddlUser.SelectedValue);

                    string isstatutoryinternal = "S";
                    if (ddlComplianceType.SelectedItem.Text == "Statutory")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Internal")
                    {
                        isstatutoryinternal = "I";
                    }

                    List<SP_LIC_GetUserLicenseTypeMappingData_Result> QueryResult = LicenseMgmt.GetAllCategoryMappingData(AuthenticationHelper.CustomerID, UserID, isstatutoryinternal);

                    if (QueryResult.Count > 0)
                        QueryResult = QueryResult.OrderBy(entry => entry.LicenseTypeName).ToList();

                    string SortExpr = string.Empty;
                    string CheckDirection = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                        {
                            CheckDirection = Convert.ToString(ViewState["Direction"]);

                            SortExpr = Convert.ToString(ViewState["SortExpression"]);
                            if (CheckDirection == "Ascending")
                            {
                                QueryResult = QueryResult.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                            }
                            else
                            {
                                CheckDirection = "Descending";
                                QueryResult = QueryResult.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                            }
                        }
                    }
                    flag = true;
                    Session["TotalRows"] = null;
                    if (QueryResult.Count > 0)
                    {
                        grdDisplayUserdata.DataSource = QueryResult;
                        Session["TotalRows"] = QueryResult.Count;
                        grdDisplayUserdata.DataBind();
                        btnSavePageAutorization.Visible = true;
                    }
                    else
                    {
                        grdDisplayUserdata.DataSource = QueryResult;
                        grdDisplayUserdata.DataBind();
                        Session["TotalRows"] = 0;
                        btnSavePageAutorization.Visible = false;
                    }
                }
            }
        }
        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                BindLicenseTypeGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSavePageAutorization_Click(object sender, EventArgs e)
        {
            try
            {
                #region Validation
                bool ValidationResult = false;
                if (!string.IsNullOrEmpty(ddlUser.SelectedValue))
                {
                    if (ddlUser.SelectedValue != "-1")
                    {
                        ValidationResult = true;
                    }
                    else
                    {
                        cvPage.IsValid = false;
                        cvPage.ErrorMessage = "Select User.";
                        vsSummary.CssClass = "alert alert-danger";
                        return;
                    }
                }
                #endregion

                #region Save/Update
                bool QueryResult = false;
                int UserID = 0;
                int TotalCheckedVal = 0;
                if (ValidationResult)
                {
                    string isstatutoryinternal = "S";
                    if (ddlComplianceType.SelectedItem.Text == "Statutory")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Internal")
                    {
                        isstatutoryinternal = "I";
                    }


                    UserID = Convert.ToInt32(ddlUser.SelectedValue);
                    LicenseMgmt.DeactiveAllCategoryOfUser(UserID, AuthenticationHelper.UserID, AuthenticationHelper.CustomerID, isstatutoryinternal);
                    for (int i = 0; i < grdDisplayUserdata.Rows.Count; i++)
                    {
                        Label LicenseTypeID = (Label)grdDisplayUserdata.Rows[i].Cells[3].FindControl("LicenseTypeID");                        
                        CheckBox chkADD = (CheckBox)grdDisplayUserdata.Rows[i].Cells[2].FindControl("chkADD");                        
                        if (chkADD.Checked == true)
                        {
                            TotalCheckedVal++;
                            Lic_tbl_UserAndLicenseTypeMapping newAssignment = new Lic_tbl_UserAndLicenseTypeMapping()
                            {
                                LicenseTypeID = Convert.ToInt32(LicenseTypeID.Text),
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdateBy = AuthenticationHelper.UserID,
                                UserID = UserID,
                                CustomerID = AuthenticationHelper.CustomerID,
                                Is_Statutory_NoStatutory=isstatutoryinternal,
                            };
                            QueryResult = LicenseMgmt.SaveUserCategoryManagement(newAssignment);
                        }
                    }
                    if (QueryResult)
                    {
                        BindLicenseTypeGrid(); bindPageNumber();
                        cvPage.IsValid = false;
                        cvPage.ErrorMessage = "LicenseType mapping created successfully.";
                        vsSummary.CssClass = "alert alert-success";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadPages", "ReloadPage();", true);
                    }
                    if (TotalCheckedVal <= 0)
                    {
                        cvPage.IsValid = false;
                        cvPage.ErrorMessage = "Please Select LicenseType.";
                        vsSummary.CssClass = "alert alert-danger";
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdDisplayUserdata_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
        {            
        }
        protected void grdDisplayUserdata_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
        }
        protected void grdDisplayUserdata_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdDisplayUserdata.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindLicenseTypeGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdDisplayUserdata.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdDisplayUserdata.PageIndex = chkSelectedPage - 1;
            grdDisplayUserdata.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindLicenseTypeGrid(); bindPageNumber();
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
        }
        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void grdDisplayUserdata_RowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void grdDisplayUserdata_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            try
            {
                long UserID = -1;
                if (!string.IsNullOrEmpty((ddlUser.SelectedValue)))
                {
                    if (ddlUser.SelectedValue != "-1")
                    {
                        UserID = Convert.ToInt64(ddlUser.SelectedValue);
                    }
                }
                string isstatutoryinternal = "S";
                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                List<SP_LIC_GetUserLicenseTypeMappingData_Result> QueryResult = LicenseMgmt.GetAllCategoryMappingData(AuthenticationHelper.CustomerID, UserID, isstatutoryinternal);
                if (QueryResult.Count > 0)
                    QueryResult = QueryResult.OrderBy(entry => entry.LicenseTypeName).ToList();
                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    QueryResult = QueryResult.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    QueryResult = QueryResult.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdDisplayUserdata.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdDisplayUserdata.Columns.IndexOf(field);
                    }
                }
                Session["TotalRows"] = null;
                flag = true;

                if (QueryResult.Count > 0)
                {
                    grdDisplayUserdata.DataSource = QueryResult;
                    Session["TotalRows"] = QueryResult.Count;
                    grdDisplayUserdata.DataBind();
                }
                else
                {
                    grdDisplayUserdata.DataSource = QueryResult;
                    grdDisplayUserdata.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            flag = false;
            BindLicenseTypeGrid();
            bindPageNumber();
        }
    }
}