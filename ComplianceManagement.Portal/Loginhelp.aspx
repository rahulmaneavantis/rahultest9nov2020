﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Loginhelp.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Loginhelp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="FortuneCookie - Development Team" />

    <title>Login Help</title>
    <!-- Bootstrap CSS -->
    <link href="NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="NewCSS/style.css" rel="stylesheet" />
    <link href="NewCSS/style-responsive.css" rel="stylesheet" />
    <style>
        .login-form1 {
            max-width: 350px;
            height: 450px;
            margin: 100px auto 0;
            background: white;
        }

        .login-form-head1 {
            /* max-width: 350px; */
            min-height: 80px;
            background: white;
            text-align: center;
        }

        .alert-success1 {
            background-color: white;
            border-color: #caf4ca;
            color: #4cd964;
        }
    </style>
</head>
<body>
    <div>
        <div class="container" id="dvLogin">
            <form runat="server" class="login-form" name="login" autocomplete="off" style="margin-left: 405px;">
                <asp:Panel ID="Panel" runat="server">
                    <div class="col-md-12 login-form-head1">
                        <p class="login-img">
                            <img src="Images/avantil-logo.png" />
                        </p>
                    </div>
                    <div class="login-wrap">
                        <div class="input-group;" style="border: none">
                            <div style="color: #555555; font-size: 13px;"><h1>How do I login to the product?</h1></div>
                            <div style="color:#555555;font-size: 13px;text-align: justify;">You can login to the product using your User ID and secure password. Your User ID is your registered email address with us.</div>
                            <div style="text-align:center">
                                <img src="Images/image-Login.png" style="height: 260PX; margin-top: 5%;" />
                            </div>

                        </div>
                   <%--     <div class="input-group" style="border: none">
                            <div style="color:#555555;font-size: 13px;text-align: justify;">
                                To ensure that you are not a computer program, we use reCaptcha technology, where you will be asked to identify certain patterns.
                            </div>
                            <div style="text-align:center">
                                <img src="Images/image-Captcha.png" />
                            </div>

                        </div>--%>
                        <div class="input-group" style="border: none">
                            <div style="color:#555555;font-size: 13px;text-align: justify;  margin-top: 5%;">OTP (One time Password) will be sent to your registered email ID and mobile number.</div>
                            <div style="color:#555555;font-size: 13px;text-align: justify;">
                              
                            </div>
                            <div style="text-align:center">
                                <img src="Images/image-OTP.png" style="height: 260PX; margin-top: 5%;" />
                            </div>
                        </div>
                        v>
                     <div class="input-group" style="border: none">
                            <div style="color:#555555;font-size: 13px;text-align: justify; ">
                                  If you login from a new Computer, you will be asked to answer your security questions. In case you continue to use the same computer, you may not be asked to answer your security questions. 
                            </div>
                            <div style="text-align:center">
                                <img src="Images/image-Security.png" style="height: 260PX; margin-top: 5%;" />
                            </div>

                        </div>
                        <div>
                            <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Login.aspx"  runat="server">Go Back</asp:HyperLink>
                        </div>
                    </div>
                </asp:Panel>
            </form>
        </div>
    </div>
</body>
</html>
