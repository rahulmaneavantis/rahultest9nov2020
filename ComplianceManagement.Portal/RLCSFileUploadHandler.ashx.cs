﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    /// <summary>
    /// Summary description for RLCSFileUploadHandler
    /// </summary>
    public class RLCSFileUploadHandler : IHttpHandler
    {
        Boolean sendmailresult = false;
        public void ProcessRequest(HttpContext context)
        {

            string Recepient = ConfigurationManager.AppSettings["RLCSFeedBackSenderEmailAddress"]; 
            //string Recepient = context.Request.Form["txttomail"];
            int sender = Convert.ToInt32(context.Request.Form["Sender"]);
            //string ListMail = System.Configuration.ConfigurationManager.AppSettings["ImpliMailList"];
            //string[] AllmailList2 = new string[] { };
            //if (!string.IsNullOrEmpty(Recepient))
            //{
            //    AllmailList2 = ListMail.Split(',');
            //}
            int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
            string ProfileId = Convert.ToString(AuthenticationHelper.ProfileID);
            var userDetail = GetByID(UserId, ProfileId);
            
            string Username = string.Empty;
            string RLCSUserID = string.Empty;
            string Corporatename = "Corporatename";
            if (userDetail != null)
            {
                Username = userDetail.FirstName + " " + userDetail.LastName;
                RLCSUserID = userDetail.UserID;
                var CorporateByIDDetail = GetCorporateByID(userDetail.CustomerID);
                if (CorporateByIDDetail != null)
                    Corporatename = CorporateByIDDetail.CO_Name;
            }
            string msg = context.Request.Form["txtmsgbody"];
            string MailDetail = "From<br>UserID : " + RLCSUserID + "<br>UserName : " + Username + "<br>Corporate : " + Corporatename + "<br>Message : " + msg;
            if (!string.IsNullOrEmpty(Recepient))
            {
                string[] arryval = Recepient.Split(',');//split values with ‘,’  

                if (arryval.Length > 0)
                {
                    int j = arryval.Length;

                    foreach (string v in arryval)
                    {
                        try
                        {
                            Email n = new Email();
                            n.Recepient = v;
                            n.Sender = sender;
                            n.Subjecttext = "Portal Feedback:" + context.Request.Form["txtsub"];
                            //n.MessageBody = context.Request.Form["txtmsgbody"];
                            n.MessageBody = MailDetail;
                            n.CreatedDate = DateTime.Now;
                            var custdetails = com.VirtuosoITech.ComplianceManagement.Business.UserManagement.GetByID(sender);
                            if (custdetails != null)
                            {
                                //int? customerID = custdetails.CustomerID;
                                //List<string> CustList = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMailList(Convert.ToInt32(customerID));
                                //CustList.Clear();
                                //if (CustList.Any(v.Contains))// || ListMail.Contains("," + v + ",")
                                //{
                                if (context.Request.Files.Count > 0)
                                {
                                    using (SmtpClient email = new SmtpClient())
                                    using (MailMessage mailMessage = new MailMessage())
                                    {
                                        email.DeliveryMethod = SmtpDeliveryMethod.Network;
                                        email.UseDefaultCredentials = false;
                                        NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                                        email.Credentials = credential;
                                        email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                                        email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                                        email.Timeout = 60000;
                                        email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                                        MailMessage oMessage = new MailMessage();
                                        string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                                        mailMessage.From = new MailAddress(FromEmailId);
                                        mailMessage.Subject = context.Request.Form["txtsub"];
                                        //mailMessage.Body = context.Request.Form["txtmsgbody"];
                                        mailMessage.Body = mailMessage.Body = MailDetail;
                                        mailMessage.IsBodyHtml = true;
                                        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                                        mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));

                                        HttpFileCollection files = context.Request.Files;
                                        for (int i = 0; i < files.Count; i++)
                                        {
                                            HttpPostedFile file = files[i];
                                            string fname;
                                            if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                                            {
                                                string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                                fname = testfiles[testfiles.Length - 1];
                                            }
                                            else
                                            {
                                                fname = file.FileName;
                                            }
                                            fname = Path.Combine(context.Server.MapPath("~/UploadedFile/"), fname);
                                            file.SaveAs(fname);
                                            Attachment data = new Attachment(fname);
                                            mailMessage.Attachments.Add(data);
                                            n.Attachment = fname;
                                        }
                                        mailMessage.To.Add(v);
                                        //mailMessage.To.Add(context.Request["txttomail"]);
                                        email.Send(mailMessage);
                                        sendmailresult = true;
                                    }
                                }
                                else
                                {

                                    using (SmtpClient email = new SmtpClient())
                                    using (MailMessage mailMessage = new MailMessage())
                                    {
                                        email.DeliveryMethod = SmtpDeliveryMethod.Network;
                                        email.UseDefaultCredentials = false;
                                        NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                                        email.Credentials = credential;
                                        email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                                        email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                                        email.Timeout = 60000;
                                        email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                                        MailMessage oMessage = new MailMessage();
                                        string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                                        mailMessage.From = new MailAddress(FromEmailId);
                                        mailMessage.Subject = "Portal Feedback:" + context.Request.Form["txtsub"];
                                        //mailMessage.Body = context.Request.Form["txtmsgbody"];
                                        mailMessage.Body = MailDetail;
                                        mailMessage.IsBodyHtml = true;
                                        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                                        mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));

                                        mailMessage.To.Add(v);
                                        //mailMessage.To.Add(context.Request["txttomail"]);
                                        email.Send(mailMessage);
                                        sendmailresult = true;
                                    }
                                }
                                com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.Addemaildata(n);
                                //}

                            }
                            context.Response.ContentType = "text/plain";
                        }
                        catch
                        {
                            sendmailresult = false;
                        }
                    }
                }
            }
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Write(sendmailresult.ToString());
            HttpContext.Current.Response.End();
        }

        public static RLCS_User_Mapping GetByID(int userID,string ProfileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.RLCS_User_Mapping
                            where row.AVACOM_UserID == userID && row.ProfileID == ProfileID
                            select row).FirstOrDefault();

                return user;
            }
        }
        public static RLCS_Customer_Corporate_Mapping GetCorporateByID(string CO_CorporateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.RLCS_Customer_Corporate_Mapping
                            where row.CO_CorporateID == CO_CorporateID
                            select row).FirstOrDefault();

                return user;
            }
        }
        
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}