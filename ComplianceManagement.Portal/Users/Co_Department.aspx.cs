﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class Co_Department : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    //if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        BindCustomers();                                    
                        if (AuthenticationHelper.Role == "CADMN")
                        {                         
                            divCustomerfilter.Visible = false;                                                      
                        }
                        else
                        {
                            divCustomerfilter.Visible = true;
                        }
                        if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                        {
                            btnAddUser.Visible = false;
                        }
                        BindDepartmentList(-1);
                    }
                    else
                    {                        
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {                    
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }            
        }
        private void BindCustomers()
        {
            try
            {
                int distributorID = -1;
                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";
               // var customerData = CustomerManagement.GetAllCustomers(customerID, AuthenticationHelper.Role, distributorID, tbxFilter.Text);
                ddlCustomerList.DataSource = CustomerManagement.GetAllCustomers(customerID, AuthenticationHelper.Role, distributorID, tbxFilter.Text);
                ddlCustomerList.DataBind();

                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    {
                        if (ddlCustomerList.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                        }
                    }
                }
                if (customerID != -1)
                {
                    Department objDeptcompliance = new Department()
                    {
                        Name = txtFName.Text.Trim(),
                        IsDeleted = false,
                        CustomerID = (int)customerID
                    };
                    mst_Department objDeptaudit = new mst_Department()
                    {
                        Name = txtFName.Text.Trim(),
                        IsDeleted = false,
                        CustomerID = (int)customerID
                    };
                    if ((int)ViewState["Mode"] == 1)
                    {
                        objDeptcompliance.ID = Convert.ToInt32(ViewState["DeptID"]);
                        objDeptaudit.ID = Convert.ToInt32(ViewState["DeptID"]);
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (CompDeptManagement.DepartmentExists(objDeptcompliance))
                        {
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "Department allready exists";
                        }
                        else
                        {
                            CompDeptManagement.CreateDepartmentMaster(objDeptcompliance);
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "Department saved successfully";
                            txtFName.Text = string.Empty;
                        }

                        if (CompDeptManagement.DepartmentExistsAudit(objDeptaudit))
                        {
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "Department already exists";
                        }
                        else
                        {
                            CompDeptManagement.CreateDepartmentMasterAudit(objDeptaudit);
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "Department saved successfully";
                            txtFName.Text = string.Empty;
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        if (CompDeptManagement.DepartmentExists(objDeptcompliance))
                        {
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "Department already exists";
                        }
                        else
                        {
                            CompDeptManagement.UpdateDepartmentMaster(objDeptcompliance);
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "Department updated successfully";
                        }
                        if (CompDeptManagement.DepartmentExistsAudit(objDeptaudit))
                        {
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "Department already exists";
                        }
                        else
                        {
                            CompDeptManagement.UpdateDepartmentMasterAudit(objDeptaudit);
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "Department updated successfully";
                        }
                    }
                    BindDepartmentList(customerID);
                    upDepeList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindDepartmentList(int CustomerID)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    customerID = CustomerID;
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    {
                        if (ddlCustomerList.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                        }
                    }
                }
                if (customerID != -1)
                {
                    var DepartmentMasterList = CompDeptManagement.GetAllDepartmentMasterList(customerID);
                    var filter = tbxFilter.Text;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        DepartmentMasterList = DepartmentMasterList.Where(entry => entry.Name.Contains(filter)).ToList();
                    }
                    grdAuditor.DataSource = DepartmentMasterList;
                    Session["TotalRows"] = DepartmentMasterList.Count;
                    grdAuditor.DataBind();
                    upModifyDepartment.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int DeptID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_Department"))
                {
                    int customerID = -1;
                    if (AuthenticationHelper.Role == "CADMN")
                    {
                        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                        {
                            if (ddlCustomerList.SelectedValue != "-1")
                            {
                                customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                            }
                        }
                    }
                    if (customerID != -1)
                    {
                        ViewState["Mode"] = 1;
                        ViewState["DeptID"] = DeptID;
                        mst_Department RPD = CompDeptManagement.DepartmentMasterGetByIDAudit(DeptID, customerID);
                        txtFName.Text = RPD.Name;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyDepartment\").dialog('open');", true);
                        upModifyDepartment.Update();
                    }
                }
                else if (e.CommandName.Equals("DELETE_Department"))
                {
                    int customerID = -1;
                    if (AuthenticationHelper.Role == "CADMN")
                    {
                        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                        {
                            if (ddlCustomerList.SelectedValue != "-1")
                            {
                                customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                            }
                        }
                    }
                    if (customerID != -1)
                    {
                        CompDeptManagement.DeleteDepartmentMaster(DeptID, customerID);
                        CompDeptManagement.DeleteDepartmentMasterAudit(DeptID, customerID);
                        BindDepartmentList(customerID);                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    if (ddlCustomerList.SelectedItem.Text == "< Select Customer >")
                    {
                        btnAddUser.Visible = false;
                    }
                    else
                    {
                        btnAddUser.Visible = true;
                    }
                }
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    {
                        if (ddlCustomerList.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                        }
                    }
                }
                if (customerID != -1)
                {
                    BindDepartmentList(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditor.PageIndex = 0;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    {
                        if (ddlCustomerList.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                        }
                    }
                }
                if (customerID != -1)
                {
                    BindDepartmentList(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upDepeList_Load(object sender, EventArgs e)
        {
        }
        protected void grdAuditor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAuditor.PageIndex = e.NewPageIndex;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    {
                        if (ddlCustomerList.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                        }
                    }
                }
                if (customerID != -1)
                {
                    BindDepartmentList(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upModifyAssignment_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComboboxForAssignmentsDialog", "initializeComboboxForAssignmentsDialog();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdAuditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                txtFName.Text = string.Empty;
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                {
                    if (ddlCustomerList.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                    }
                }
                if (customerID != -1)
                {
                    BindDepartmentList(customerID);
                }                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyDepartment\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}