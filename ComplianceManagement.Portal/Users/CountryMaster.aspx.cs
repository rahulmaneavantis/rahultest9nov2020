﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class CountryMaster : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "C")
                this.MasterPageFile = "~/NewCompliance.Master";
            else if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "L")
                this.MasterPageFile = "~/LitigationMaster.Master";
            else
                this.MasterPageFile = "~/AuditTool.Master";
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";

                BindCountryData();
                bindPageNumber();                
            }
        }

        private void BindCountryData()
        {
            try
            {
                var CountryList = CityStateCountryManagement.GetAllContryData();
                if (!string.IsNullOrEmpty(tbxSearchCountry.Text))
                {
                    CountryList = CountryList.Where(Entry => Entry.Name.Equals(tbxSearchCountry.Text)).ToList();
                }
                grdCountry.DataSource = CountryList;
                Session["TotalRows"] = CountryList.Count;
                grdCountry.DataBind();
                upPromotorList.Update();
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCountry.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCountryData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCountry.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxCountry.Text = string.Empty;
                upPromotor.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCountry_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdCountry_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_Country"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["CountryID"] = ID;
                    Country objCountry = CityStateCountryManagement.GetCountryDetailByID(ID);
                    tbxCountry.Text = objCountry.Name;
                    upPromotor.Update();
                }
                else if (e.CommandName.Equals("DELETE_Country"))
                {
                    CityStateCountryManagement.DeleteCountryDetail(ID);
                    CityStateContryManagementRisk.DeleteCountryDetail(ID);
                    BindCountryData();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdCountry.PageIndex = chkSelectedPage - 1;
            grdCountry.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindCountryData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Country objCountry = new Country()
                {
                    Name = tbxCountry.Text
                };
                mst_Country objAuditcontry = new mst_Country()
                {
                    Name = tbxCountry.Text
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    objCountry.ID = Convert.ToInt32(ViewState["CountryID"]);//Need to change DeptID
                    objAuditcontry.ID = Convert.ToInt32(ViewState["CountryID"]);//Need to change DeptID
                }

                if ((int) ViewState["Mode"] == 0)
                {
                    if (CityStateCountryManagement.CheckCountryExist(objCountry.Name))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Country Already Exists";
                    }
                    else
                    {
                        objCountry.IsDeleted = false;
                        CityStateCountryManagement.CreateContryDetails(objCountry);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Country Saved Successfully";
                        tbxCountry.Text = string.Empty;
                    }

                    if (CityStateContryManagementRisk.CheckCountryExist(objAuditcontry.Name))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Country Already Exists";
                    }
                    else
                    {
                        objAuditcontry.IsDeleted = false;
                        CityStateContryManagementRisk.CreateContryDetails(objAuditcontry);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Country Saved Successfully";
                        tbxCountry.Text = string.Empty;
                    }
                }

                else if ((int) ViewState["Mode"] == 1)
                {
                    CityStateCountryManagement.UpdateCountryDetails(objCountry);
                    CityStateContryManagementRisk.UpdateCountryDetails(objAuditcontry);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Country Updated Successfully";
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAuditorDialog\").dialog('close')", true);
                BindCountryData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCountry.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void tbxSearchCountry_TextChanged(object sender, EventArgs e)
        {
            BindCountryData();
        }
    }
}