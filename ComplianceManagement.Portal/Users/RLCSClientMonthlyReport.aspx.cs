﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class RLCSClientMonthlyReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime today = DateTime.Now;
                string Month = Convert.ToString(today.Month);
                string Year = Convert.ToString(today.Year);

                if (Convert.ToInt32(Month) < 10)
                    Month = "0" + Month;
                
                ddlMonth.SelectedValue = Month;
                ddlYear.SelectedValue = Year;
            }
        }

        public static int getDocumentCount_ClientWise_MonthYearWise(bool includeALL, List<SP_RLCS_GetLatestDocCountByMonthYear_Result> latestDoc,
            List<SP_RLCS_GetHistoricalDocCountByMonthYear_Result> historical,
            List<SP_RLCS_GetREGRepoDocCountByMonthYear_Result> regRepo,
            List<SP_RLCS_GetRENDocCountByMonthYear_Result> regRen,
            DateTime date, string clientID, string docType)
        {
            string Month = string.Empty;
            int totalDocCount = 0;

            if (!string.IsNullOrEmpty(docType))
            {
                if (Convert.ToInt32(date.Month) < 10)
                    Month = "0" + date.Month.ToString();
                else
                    Month = date.Month.ToString();


                if (latestDoc.Count > 0)
                {
                    if (docType.Trim().ToUpper().Equals("REGISTER"))
                    {
                        totalDocCount = totalDocCount + Convert.ToInt32(latestDoc.Where(x => x.LM_PayrollMonth.Trim() == Month
                        && x.LM_PayrollYear.Trim() == date.Year.ToString()
                        && x.RM_ScopeID == "SOW03"
                        && x.RM_ClientID == clientID).Count());

                        //totalDocCount = totalDocCount + Convert.ToInt32(latestDoc.Where(x => x.LM_PayrollMonth.Trim() == Month
                        //&& x.LM_PayrollYear.Trim() == date.Year.ToString()
                        //&& x.RM_ScopeID == "SOW03"
                        //&& x.RM_ClientID == clientID).Select(row => row.DocumentCount).Sum());
                    }
                    else if (docType.Trim().ToUpper().Equals("CHALLAN"))
                    {
                        totalDocCount = totalDocCount + Convert.ToInt32(latestDoc.Where(x => x.LM_PayrollMonth.Trim() == Month
                       && x.LM_PayrollYear.Trim() == date.Year.ToString()
                       && (x.RM_ScopeID == "SOW13" || x.RM_ScopeID == "SOW14" || x.RM_ScopeID == "SOW17")
                       && x.RM_ClientID == clientID).Count());

                        // totalDocCount = totalDocCount + Convert.ToInt32(latestDoc.Where(x => x.LM_PayrollMonth.Trim() == Month
                        //&& x.LM_PayrollYear.Trim() == date.Year.ToString()
                        //&& (x.RM_ScopeID == "SOW13" || x.RM_ScopeID == "SOW14" || x.RM_ScopeID == "SOW17")
                        //&& x.RM_ClientID == clientID).Select(row => row.DocumentCount).Sum());
                    }
                }

                if (historical.Count > 0)
                {
                    if (docType.Trim().ToUpper().Equals("RETURN"))
                    {
                        totalDocCount = totalDocCount + historical.Where(x => x.PayrollMonth.Trim() == Month
                                        && x.PayrollYear.Trim() == date.Year.ToString()
                                        && x.ScopeID == "SOW05"
                                        && x.LM_ClientID == clientID).Count();
                    }
                }

                if (includeALL)
                {
                    if (regRepo.Count > 0)
                    {
                        totalDocCount = totalDocCount + Convert.ToInt32(regRepo.Where(x => x.RR_ClientID == clientID).Count());
                    }

                    if (regRen.Count > 0)
                    {
                        totalDocCount = totalDocCount + Convert.ToInt32(regRen.Where(x => x.OA_ClientID == clientID).Count());
                    }
                }
            }

            return totalDocCount;
        }

        public static int getDocumentCount_ClientWise_FromMonthYearToMonthYear(bool includeALL, List<SP_RLCS_GetLatestDocCountByMonthYear_Report2_Result> latestDoc,
           List<SP_RLCS_GetHistoricalDocCountByMonthYear_Report2_Result> historical, List<SP_RLCS_GetREGRepoDocCountByMonthYear_Report2_Result> regRepo,
            List<SP_RLCS_GetRENDocCountByMonthYear_Report2_Result> regRen, DateTime date, string clientID, string docType)
        {
            int totalDocCount = 0;
            string Month = string.Empty;
            if (!string.IsNullOrEmpty(docType))
            {
                if (Convert.ToInt32(date.Month) < 10)
                    Month = "0" + date.Month.ToString();
                else
                    Month = date.Month.ToString();
                
                if (latestDoc.Count > 0)
                {
                    if (docType.Trim().ToUpper().Equals("REGISTER"))
                    {
                        totalDocCount = totalDocCount + Convert.ToInt32(latestDoc.Where(x => x.LM_PayrollMonth.Trim() == Month
                        && x.LM_PayrollYear.Trim() == date.Year.ToString()
                        && x.RM_ScopeID == "SOW03"
                        && x.RM_ClientID == clientID).Count());

                        //totalDocCount = totalDocCount + Convert.ToInt32(latestDoc.Where(x => x.LM_PayrollMonth.Trim() == Month
                        //&& x.LM_PayrollYear.Trim() == date.Year.ToString()
                        //&& x.RM_ScopeID == "SOW03"
                        //&& x.RM_ClientID == clientID).Select(row => row.DocumentCount).Sum());
                    }
                    else if (docType.Trim().ToUpper().Equals("CHALLAN"))
                    {
                        totalDocCount = totalDocCount + Convert.ToInt32(latestDoc.Where(x => x.LM_PayrollMonth.Trim() == Month
                          && x.LM_PayrollYear.Trim() == date.Year.ToString()
                          && (x.RM_ScopeID == "SOW13" || x.RM_ScopeID == "SOW14" || x.RM_ScopeID == "SOW17")
                          && x.RM_ClientID == clientID).Count());

                       // totalDocCount = totalDocCount + Convert.ToInt32(latestDoc.Where(x => x.LM_PayrollMonth.Trim() == Month
                       //&& x.LM_PayrollYear.Trim() == date.Year.ToString()
                       //&& (x.RM_ScopeID == "SOW13" || x.RM_ScopeID == "SOW14" || x.RM_ScopeID == "SOW17")
                       //&& x.RM_ClientID == clientID).Select(row => row.DocumentCount).Sum());
                    }
                }

                if (historical.Count > 0)
                {
                    if (docType.Trim().ToUpper().Equals("RETURN"))
                    {
                        totalDocCount = totalDocCount + historical.Where(x => x.PayrollMonth.Trim() == Month
                    && x.PayrollYear.Trim() == date.Year.ToString()
                    && x.LM_ClientID == clientID).Count();
                    }
                }

                if (includeALL)
                {
                    if (regRepo.Count > 0)
                    {
                        totalDocCount = totalDocCount + Convert.ToInt32(regRepo.Where(x => x.RR_ClientID == clientID).Count());
                    }

                    if (regRen.Count > 0)
                    {
                        totalDocCount = totalDocCount + Convert.ToInt32(regRen.Where(x => x.OA_ClientID == clientID).Count());
                    }
                }
            }

            return totalDocCount;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlMonth.SelectedValue) && !string.IsNullOrEmpty(ddlYear.SelectedValue))
                {
                    int Month = 0, Year = 0;
                    if (!string.IsNullOrEmpty(ddlMonth.SelectedValue))
                        Month = Convert.ToInt32(ddlMonth.SelectedValue);

                    if (!string.IsNullOrEmpty(ddlYear.SelectedValue))
                        Year = Convert.ToInt32(ddlYear.SelectedValue);

                    if (Month != 0 && Year != 0)
                    {
                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {
                            #region code                                                                               
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                entities.Database.CommandTimeout = 300;
                                var lstClientsMonthYear_Report1 = entities.Sp_RLCS_GetClientMonth(Month, Year).ToList();

                                if (lstClientsMonthYear_Report1.Count > 0)
                                {
                                    lstClientsMonthYear_Report1 = lstClientsMonthYear_Report1.OrderBy(x => x.LM_ClientID).ToList();
                                    var MonthYearList = lstClientsMonthYear_Report1.Select(x => x.LM_PayrollMonth + "-" + x.LM_PayrollYear).Distinct().ToList();

                                    var BranchCountData = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                           where row.BranchType.Equals("B") && row.CM_Status.Equals("A")
                                                           select row).ToList();

                                    lstClientsMonthYear_Report1 = lstClientsMonthYear_Report1.Distinct().ToList();

                                    var ClientList = lstClientsMonthYear_Report1.Select(x => x.LM_ClientID).Distinct().ToList();
                                    var MonthList = lstClientsMonthYear_Report1.Select(x => x.Date).Distinct().ToList();

                                    
                                    MonthList = MonthList.OrderBy(x => x.Value).ToList();

                                    DataTable Output = new DataTable();
                                    Output.Columns.Add("Sr_No", typeof(string));
                                    Output.Columns.Add("RM_ClientID", typeof(string));
                                    Output.Columns.Add("BranchName", typeof(string));
                                    Output.Columns.Add("BranchCount", typeof(int));

                                    List<string> lstDocTypes = new List<string>() { "REGISTER", "RETURN", "CHALLAN" };

                                    for (int i = 0; i < MonthList.Count; i++)
                                    {
                                        for (int j = 0; j < lstDocTypes.Count; j++)
                                        {
                                            Output.Columns.Add(MonthList[i].Value.ToString("MMMyyyy") + "-" + lstDocTypes[j].ToString(), typeof(string));                                                                               
                                        }
                                    }

                                    Output.Columns.Add("Empty", typeof(String));

                                    for (int i = 0; i < MonthList.Count; i++)
                                    {
                                        for (int j = 0; j < lstDocTypes.Count; j++)
                                        {
                                            Output.Columns.Add(MonthList[i].Value.ToString("MMM-yyyy") + "-" +lstDocTypes[j].ToString(), typeof(string));
                                        }
                                    }

                                    var LatestDocument = entities.SP_RLCS_GetLatestDocCountByMonthYear(Month, Year).ToList();
                                    var HistoricalDocument = entities.SP_RLCS_GetHistoricalDocCountByMonthYear(Month, Year).ToList();
                                    var REGRepoDocument = entities.SP_RLCS_GetREGRepoDocCountByMonthYear(Month, Year).ToList();
                                    var RENDocument = entities.SP_RLCS_GetRENDocCountByMonthYear(Month, Year).ToList();
                                    
                                    #region Get Month-Year for Report2

                                    int fromMonth = 4;
                                    int fromYear = 2019;

                                    int toMonth = 0;
                                    int toYear = 0;

                                    if (Month == 1)
                                    {
                                        toMonth = Month - 1;
                                        toYear = Year - 1;
                                    }
                                    else
                                    {
                                        toMonth = Month - 1;
                                        toYear = Year;
                                    }

                                    if (new DateTime(toYear, toMonth, 1) < new DateTime(fromYear, fromMonth, 1))
                                    {
                                        toMonth = fromMonth;
                                        toYear = fromYear;
                                    }

                                    var LatestDocument_Report2 = entities.SP_RLCS_GetLatestDocCountByMonthYear_Report2(fromMonth, fromYear, toMonth, toYear).ToList();
                                    var HistoricalDocument_Report2 = entities.SP_RLCS_GetHistoricalDocCountByMonthYear_Report2(fromMonth, fromYear, toMonth, toYear).ToList();
                                    var REGRepoDocument_Report2 = entities.SP_RLCS_GetREGRepoDocCountByMonthYear_Report2(fromMonth, fromYear, toMonth, toYear).ToList();
                                    var RENDocument_Report2 = entities.SP_RLCS_GetRENDocCountByMonthYear_Report2(fromMonth, fromYear, toMonth, toYear).ToList();

                                    #endregion

                                    int Cnt = 1;
                                    int RowCount1 = 0;

                                    DateTime selectedDate = new DateTime(Year, Month, 1);

                                    foreach (var item in ClientList)
                                    {
                                        string ClientID = item;
                                        string BranchName = lstClientsMonthYear_Report1.Where(x => x.LM_ClientID == item).Select(x => x.Entity).FirstOrDefault();
                                        int BranchCount = BranchCountData.Where(x => x.CM_ClientID.Equals(item)).Count();

                                        Output.Rows.Add(Cnt, ClientID, BranchName, BranchCount);

                                        int p = 0;
                                        int docCount = 0;
                                        int firstHalf = 4 + MonthList.Count;
                                        int columnCount = 4;

                                        for (int j = 4; j < firstHalf; j++)
                                        {
                                            for (int k = 0; k < lstDocTypes.Count; k++)
                                            {
                                                docCount = 0;
                                                if (MonthList[p].Value == selectedDate)
                                                {
                                                    docCount = getDocumentCount_ClientWise_MonthYearWise(false, LatestDocument, HistoricalDocument, REGRepoDocument, RENDocument, MonthList[p].Value, item, lstDocTypes[k]);
                                                    //docCount = getDocumentCount_ClientWise_MonthYearWise(true, LatestDocument, HistoricalDocument, REGRepoDocument, RENDocument, MonthList[p].Value, item);

                                                    if (docCount != 0)
                                                        Output.Rows[RowCount1][columnCount] = docCount;
                                                    else
                                                        Output.Rows[RowCount1][columnCount] = "";
                                                }
                                                else
                                                {
                                                    docCount = getDocumentCount_ClientWise_MonthYearWise(false, LatestDocument, HistoricalDocument, REGRepoDocument, RENDocument, MonthList[p].Value, item, lstDocTypes[k]);

                                                    if (docCount != 0)
                                                        Output.Rows[RowCount1][columnCount] = docCount;
                                                    else
                                                        Output.Rows[RowCount1][columnCount] = "";

                                                    //Output.Rows[RowCount1][j] = getCount_Includes_RegRetChallans(LatestDocument, HistoricalDocument, MonthList[p].Value, item);
                                                }
                                                columnCount++;
                                            }
                                            p++;
                                        }

                                        Output.Rows[RowCount1][firstHalf] = "";
                                        columnCount++;

                                        int q = 0;

                                        for (int j = firstHalf + 1; j < (firstHalf + 1 + MonthList.Count); j++)
                                        {
                                            for (int k = 0; k < lstDocTypes.Count; k++)
                                            {
                                                docCount = 0;
                                                docCount = getDocumentCount_ClientWise_FromMonthYearToMonthYear(false, LatestDocument_Report2, HistoricalDocument_Report2, REGRepoDocument_Report2, RENDocument_Report2, MonthList[q].Value, item, lstDocTypes[k]);

                                                if (docCount != 0)
                                                    Output.Rows[RowCount1][columnCount] = docCount;
                                                else
                                                    Output.Rows[RowCount1][columnCount] = "";

                                                //if (MonthList[q].Value == selectedDate)
                                                //{
                                                //    Output.Rows[RowCount1][j] = getDocumentCount_ClientWise_FromMonthYearToMonthYear(true, LatestDocument_Report2, HistoricalDocument_Report2, REGRepoDocument_Report2, RENDocument_Report2, MonthList[q].Value, item);
                                                //}
                                                //else
                                                //{
                                                //    Output.Rows[RowCount1][j] = getDocumentCount_ClientWise_FromMonthYearToMonthYear(true, LatestDocument_Report2, HistoricalDocument_Report2, REGRepoDocument_Report2, RENDocument_Report2, MonthList[q].Value, item);
                                                //}

                                                columnCount++;
                                            }
                                            q++;
                                        }

                                        RowCount1++;
                                        Cnt++;
                                    }

                                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Client Monthly Report");

                                    System.Data.DataTable ExcelData1 = null;

                                    DataView view1 = new System.Data.DataView(Output);

                                    //ExcelData1 = view1.ToTable("Selected", false, "Sr_No","RM_ClientID", "BranchCount", "M1", "M2", "M3", "M4", "M5", "M6");
                                    ExcelData1 = view1.ToTable();

                                    string MergeColumnName = string.Empty;
                                    int value = 3 * (MonthList.Count) + 3 * (MonthList.Count) + 5 + 64;
                                    if (value > 90)
                                    {
                                        MergeColumnName = GetMergeColumnName(value, "A", 1, 1);

                                        //int val = value - 26;
                                        //char sval = Convert.ToChar(val);
                                        //string ColName = "" + sval.ToString();
                                        //string ColID = "A" + ColName;
                                        //MergeColumnName = "A1:" + ColID + "1";
                                    }
                                    else
                                    {
                                        char LastColumnName = Convert.ToChar(value);
                                        MergeColumnName = "A1:" + LastColumnName + "1";
                                    }                                   

                                    exWorkSheet1.Cells["A1"].Value = "RLCS-Client(s) Monthly Usage Report-" + ddlMonth.SelectedItem + "-" + ddlYear.SelectedValue;
                                    exWorkSheet1.Cells[MergeColumnName].Merge = true;
                                    exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["A1"].Style.Font.Size = 14;
                                    exWorkSheet1.Cells[MergeColumnName].Style.Font.Italic = true;
                                    exWorkSheet1.Cells[MergeColumnName].Style.Font.UnderLine = true;
                                    exWorkSheet1.Cells[MergeColumnName].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells[MergeColumnName].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeColumnName].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeColumnName].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeColumnName].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;

                                    string MergeCoulmnName1 = string.Empty;
                                    int value1 = 3 * (MonthList.Count) + 4 + 64;
                                    if (value1 > 90)
                                    {
                                        MergeCoulmnName1 = GetMergeColumnName(value1, "E", 2, 2);

                                        //int val = value1 - 26;
                                        //char sval = Convert.ToChar(val);
                                        //string ColName = "" + sval.ToString();
                                        //string ColID = "A" + ColName;
                                        //MergeCoulmnName1 = "E2:" + ColID + "2";
                                    }
                                    else
                                    {
                                        char LastColumnName = Convert.ToChar(value1);
                                        MergeCoulmnName1 = "E2:" + LastColumnName + "2";
                                    }
                                    
                                    exWorkSheet1.Cells["E2"].Value = "Documents Migrated in-" + ddlMonth.SelectedItem.Text.ToString() + "-" + ddlYear.SelectedValue;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Merge = true;
                                    exWorkSheet1.Cells["E2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["E2"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["E2"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["E2"].Style.Font.Size = 14;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Font.Italic = true;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Font.UnderLine = true;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;

                                    #region header second chart
                                    value1 = value1 + 2;
                                    //char LastColumnNamecheck = Convert.ToChar(value1);
                                    string startcharacter = string.Empty;
                                    string startChar = string.Empty;
                                    if (value1 > 90)
                                    {
                                        startChar = GetColumnName(value1);
                                        startcharacter = startChar + "2";

                                        //int val = value1 - 26;
                                        //char sval = Convert.ToChar(val);
                                        //string ColName = "A" + sval.ToString();
                                        //startcharacter = ColName + "2";
                                        //startChar = ColName;
                                    }
                                    else
                                    {
                                        char sval = Convert.ToChar(value1);
                                        string ColName = "" + sval.ToString();
                                        startcharacter = ColName + "2";
                                        startChar = ColName;
                                    }

                                    string MergeCoulmnName12 = string.Empty;
                                    int value12 = 3 * (MonthList.Count) + 3 * (MonthList.Count) + 5 + 64;
                                    if (value12 > 90)
                                    {
                                        MergeCoulmnName12 = GetMergeColumnName(value12, startChar, 2, 2);

                                        //int val = value12 - 26;
                                        //char sval = Convert.ToChar(val);
                                        //string ColName = "" + sval.ToString();
                                        //string ColID = "A" + ColName;
                                        //MergeCoulmnName12 = startcharacter + ":" + ColID + "2";
                                    }
                                    else
                                    {
                                        char LastColumnName = Convert.ToChar(value12);
                                        MergeCoulmnName12 = startcharacter + ":" + LastColumnName + "2";
                                    }

                                    string ToMonthName = string.Empty;
                                    ToMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(toMonth);
                                    
                                    exWorkSheet1.Cells[startcharacter].Value = "Documents Migrated from April " + fromYear + " to " + ToMonthName + "-" + toYear;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Merge = true;
                                    exWorkSheet1.Cells[startcharacter].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells[startcharacter].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells[startcharacter].Style.Font.Bold = true;
                                    exWorkSheet1.Cells[startcharacter].Style.Font.Size = 14;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Font.Italic = true;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Font.UnderLine = true;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                                    
                                    #endregion

                                    exWorkSheet1.Cells["A4"].LoadFromDataTable(ExcelData1, true);

                                    exWorkSheet1.Cells["A3"].Value = "Sr No";
                                    exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["A3"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["A3"].AutoFitColumns(5);
                                    exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                                         
                                    exWorkSheet1.Cells["B3"].Value = "ClientID";
                                    exWorkSheet1.Cells["B3"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["B3"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["B3"].AutoFitColumns(15);
                                    exWorkSheet1.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["B3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["B3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["B3"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                                         
                                    exWorkSheet1.Cells["C3"].Value = "Client Name";
                                    exWorkSheet1.Cells["C3"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["C3"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["C3"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["C3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["C3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["C3"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                                         
                                    exWorkSheet1.Cells["D3"].Value = "Branch Count";
                                    exWorkSheet1.Cells["D3"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["D3"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["D3"].AutoFitColumns(15);
                                    exWorkSheet1.Cells["D3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["D3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    //exWorkSheet1.Cells["D2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    //exWorkSheet1.Cells["D2"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                    int StartFrom = 69;
                                    for (int i = 0; i < (MonthList.Count); i++)
                                    {
                                        for (int k = 0; k < lstDocTypes.Count; k++)
                                        {
                                            string ColID = string.Empty;
                                            if (StartFrom > 90)
                                            {
                                                 ColID = GetColumnName(StartFrom);

                                                //int val = StartFrom - 26;
                                                //char sval = Convert.ToChar(val);
                                                //string ColName = "" + sval.ToString();
                                                //string ColID = "A" + ColName;                                               
                                            }
                                            else
                                            {
                                                ColID = (Convert.ToChar(StartFrom)).ToString();                                               
                                            }

                                            exWorkSheet1.Cells[ColID + "3"].Value = MonthList[i].Value.ToString("MMM yyyy");
                                            exWorkSheet1.Cells[ColID + "3"].Style.Font.Bold = true;
                                            exWorkSheet1.Cells[ColID + "3"].Style.Font.Size = 12;
                                            exWorkSheet1.Cells[ColID + "3"].AutoFitColumns(10);
                                            exWorkSheet1.Cells[ColID + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells[ColID + "3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                            exWorkSheet1.Cells[ColID + "3"].Style.WrapText = true;

                                            StartFrom++;
                                        }
                                    }

                                    if (StartFrom > 90)
                                    {
                                        string ColID = GetColumnName(StartFrom);

                                        //int val = StartFrom - 26;
                                        //char sval = Convert.ToChar(val);
                                        //string ColName = "" + sval.ToString();
                                        //string ColID = "A" + ColName;
                                  
                                        exWorkSheet1.Cells[ColID + "3"].Value = "";
                                        exWorkSheet1.Cells[ColID + "3"].AutoFitColumns(5);
                                        StartFrom++;
                                    }
                                    else
                                    {
                                        char svalEmpty = Convert.ToChar(StartFrom);
                                        string ColID = "" + svalEmpty.ToString();
                                        exWorkSheet1.Cells[ColID + "3"].Value = "";
                                        exWorkSheet1.Cells[ColID + "3"].AutoFitColumns(5);
                                        StartFrom++;
                                    }

                                    for (int i = 0; i < (MonthList.Count); i++)
                                    {
                                        for (int k = 0; k < lstDocTypes.Count; k++)
                                        {
                                            if (StartFrom > 90)
                                            {
                                                string ColID = GetColumnName(StartFrom);

                                                //int val = StartFrom - 26;
                                                //char sval = Convert.ToChar(val);
                                                //string ColName = "" + sval.ToString();
                                                //string ColID = "A" + ColName;

                                                exWorkSheet1.Cells[ColID + "3"].Value = MonthList[i].Value.ToString("MMM yyyy");
                                                exWorkSheet1.Cells[ColID + "3"].Style.Font.Bold = true;
                                                exWorkSheet1.Cells[ColID + "3"].Style.Font.Size = 12;
                                                exWorkSheet1.Cells[ColID + "3"].AutoFitColumns(10);
                                                exWorkSheet1.Cells[ColID + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells[ColID + "3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                                exWorkSheet1.Cells[ColID + "3"].Style.WrapText = true;
                                                StartFrom++;
                                            }
                                            else
                                            {
                                                char sval = Convert.ToChar(StartFrom);

                                                exWorkSheet1.Cells["" + sval + "3"].Value = MonthList[i].Value.ToString("MMM yyyy");
                                                exWorkSheet1.Cells["" + sval + "3"].Style.Font.Bold = true;
                                                exWorkSheet1.Cells["" + sval + "3"].Style.Font.Size = 12;
                                                exWorkSheet1.Cells["" + sval + "3"].AutoFitColumns(10);
                                                exWorkSheet1.Cells["" + sval + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                exWorkSheet1.Cells["" + sval + "3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                                exWorkSheet1.Cells["" + sval + "3"].Style.WrapText = true;

                                                StartFrom++;
                                            }
                                        }                                 
                                    }

                                    #region Register, Return, Challan Heading

                                    exWorkSheet1.Cells["A4"].Value = "";
                                    exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["A4"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["A4"].AutoFitColumns(5);
                                    exWorkSheet1.Cells["A4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["A4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                    exWorkSheet1.Cells["B4"].Value = "";
                                    exWorkSheet1.Cells["B4"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["B4"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["B4"].AutoFitColumns(5);
                                    exWorkSheet1.Cells["B4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["B4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["B4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["B4"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                    exWorkSheet1.Cells["C4"].Value = "";
                                    exWorkSheet1.Cells["C4"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["C4"].Style.Font.Size = 12;
                                    //exWorkSheet1.Cells["C4"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["C4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["C4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["C4"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                    exWorkSheet1.Cells["D4"].Value = "";
                                    exWorkSheet1.Cells["D4"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["D4"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["D4"].AutoFitColumns(15);
                                    exWorkSheet1.Cells["D4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["D4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    //exWorkSheet1.Cells["D2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    //exWorkSheet1.Cells["D2"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                    StartFrom = 69;
                                    for (int i = 0; i < (MonthList.Count); i++)
                                    {
                                        for (int k = 0; k < lstDocTypes.Count; k++)
                                        {
                                            string ColID = string.Empty;
                                            if (StartFrom > 90)
                                            {
                                                ColID = GetColumnName(StartFrom);

                                                //int val = StartFrom - 26;
                                                //char sval = Convert.ToChar(val);
                                                //string ColName = "" + sval.ToString();
                                                //string ColID = "A" + ColName;                                               
                                            }
                                            else
                                            {
                                                ColID = (Convert.ToChar(StartFrom)).ToString();
                                            }

                                            exWorkSheet1.Cells[ColID + "4"].Value = lstDocTypes[k];
                                            exWorkSheet1.Cells[ColID + "4"].Style.Font.Bold = true;
                                            exWorkSheet1.Cells[ColID + "4"].Style.Font.Size = 12;
                                            exWorkSheet1.Cells[ColID + "4"].AutoFitColumns(10);
                                            exWorkSheet1.Cells[ColID + "4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells[ColID + "4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                            exWorkSheet1.Cells[ColID + "4"].Style.WrapText = true;

                                            StartFrom++;
                                        }
                                    }

                                    if (StartFrom > 90)
                                    {
                                        string ColID = GetColumnName(StartFrom);

                                        //int val = StartFrom - 26;
                                        //char sval = Convert.ToChar(val);
                                        //string ColName = "" + sval.ToString();
                                        //string ColID = "A" + ColName;

                                        exWorkSheet1.Cells[ColID + "4"].Value = "";
                                        exWorkSheet1.Cells[ColID + "4"].AutoFitColumns(5);
                                        StartFrom++;
                                    }
                                    else
                                    {
                                        char svalEmpty = Convert.ToChar(StartFrom);
                                        string ColID = "" + svalEmpty.ToString();
                                        exWorkSheet1.Cells[ColID + "4"].Value = "";
                                        exWorkSheet1.Cells[ColID + "4"].AutoFitColumns(5);
                                        StartFrom++;
                                    }

                                    for (int i = 0; i < (MonthList.Count); i++)
                                    {
                                        for (int k = 0; k < lstDocTypes.Count; k++)
                                        {
                                            string ColID = string.Empty;
                                            if (StartFrom > 90)
                                            {
                                                ColID = GetColumnName(StartFrom);

                                                //int val = StartFrom - 26;
                                                //char sval = Convert.ToChar(val);
                                                //string ColName = "" + sval.ToString();
                                                //string ColID = "A" + ColName;
                                            }
                                            else
                                            {
                                                char sval = Convert.ToChar(StartFrom);
                                                ColID = sval.ToString();
                                            }

                                            exWorkSheet1.Cells[ColID + "4"].Value = lstDocTypes[k];
                                            exWorkSheet1.Cells[ColID + "4"].Style.Font.Bold = true;
                                            exWorkSheet1.Cells[ColID + "4"].Style.Font.Size = 12;
                                            exWorkSheet1.Cells[ColID + "4"].AutoFitColumns(10);
                                            exWorkSheet1.Cells[ColID + "4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells[ColID + "4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                            exWorkSheet1.Cells[ColID + "4"].Style.WrapText = true;
                                            StartFrom++;
                                        }
                                    }

                                    #endregion

                                    using (ExcelRange col = exWorkSheet1.Cells[2, 1, 2 + ExcelData1.Rows.Count + 1, (3 * (MonthList.Count) + 3 * (MonthList.Count) + 5)])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }

                                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                                    Response.ClearContent();
                                    Response.Buffer = true;
                                    Response.AddHeader("content-disposition", "attachment;filename=ClientMonthlyReport-" + ddlMonth.SelectedItem + "-" + ddlYear.SelectedValue + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                                    Response.Charset = "";
                                    Response.ContentType = "application/vnd.ms-excel";
                                    StringWriter sw = new StringWriter();
                                    Response.BinaryWrite(fileBytes);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Available for Selected Month-Year";
                                }
                            }//Using End
                            #endregion
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Select Month and Year Before Proceed";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Select Month and Year Before Proceed";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnExportV2_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlMonth.SelectedValue) && !string.IsNullOrEmpty(ddlYear.SelectedValue))
                {
                    int selectedMonth = 0, selectedYear = 0;

                    if (!string.IsNullOrEmpty(ddlMonth.SelectedValue))
                        selectedMonth = Convert.ToInt32(ddlMonth.SelectedValue);

                    if (!string.IsNullOrEmpty(ddlYear.SelectedValue))
                        selectedYear = Convert.ToInt32(ddlYear.SelectedValue);

                    if (selectedMonth != 0 && selectedYear != 0)
                    {
                        #region Get Month-Year for Report2

                        int fromMonth = 4;
                        int fromYear = 2019;

                        int toMonth = 0;
                        int toYear = 0;

                        if (selectedMonth == 1)
                        {
                            toMonth = selectedMonth - 1;
                            toYear = selectedYear - 1;
                        }
                        else
                        {
                            toMonth = selectedMonth - 1;
                            toYear = selectedYear;
                        }

                        #endregion

                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {
                            #region code 
                                                                                                          
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var lstClientsMonthYear_Report1 = entities.Sp_RLCS_GetClientMonth(selectedMonth, selectedYear).ToList();
                                
                                if (lstClientsMonthYear_Report1.Count > 0)
                                {
                                    var BranchCountData = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                           where row.BranchType.Equals("B") && row.CM_Status.Equals("A")
                                                           select row).ToList();

                                    var lstClientsMonthYear_Report2 = entities.SP_RLCS_GetClientMonth_Report2(fromMonth, fromYear, toMonth, toYear).ToList();

                                    lstClientsMonthYear_Report1 = lstClientsMonthYear_Report1.OrderBy(x => x.LM_ClientID).ToList();

                                    lstClientsMonthYear_Report1 = lstClientsMonthYear_Report1.Distinct().ToList();

                                    var ClientList = lstClientsMonthYear_Report1.Select(x => x.LM_ClientID).Distinct().ToList();
                                    var MonthList = lstClientsMonthYear_Report1.Select(x => x.Date).Distinct().ToList();

                                    MonthList = MonthList.OrderBy(x => x.Value).ToList();

                                    DataTable Output = new DataTable();
                                    Output.Columns.Add("Sr_No", typeof(string));
                                    Output.Columns.Add("RM_ClientID", typeof(string));
                                    Output.Columns.Add("BranchName", typeof(string));
                                    Output.Columns.Add("BranchCount", typeof(int));

                                    for (int i = 0; i < MonthList.Count; i++)
                                    {
                                        Output.Columns.Add(MonthList[i].Value.ToString("MMM yyyy"), typeof(int));
                                    }

                                    Output.Columns.Add("Empty", typeof(String));

                                    for (int i = 0; i < MonthList.Count; i++)
                                    {
                                        Output.Columns.Add(MonthList[i].Value.ToString("MMM-yyyy"), typeof(int));
                                    }

                                    var LatestDocument = entities.SP_RLCS_GetLatestDocCountByMonthYear(selectedMonth, selectedYear).ToList();
                                    var HistoricalDocument = entities.SP_RLCS_GetHistoricalDocCountByMonthYear(selectedMonth, selectedYear).ToList();
                                    var REGRepoDocument = entities.SP_RLCS_GetREGRepoDocCountByMonthYear(selectedMonth, selectedYear).ToList();
                                    var RENDocument = entities.SP_RLCS_GetRENDocCountByMonthYear(selectedMonth, selectedYear).ToList();

                                    

                                    var LatestDocument_Report2 = entities.SP_RLCS_GetLatestDocCountByMonthYear_Report2(fromMonth, fromYear, toMonth, toYear).ToList();
                                    var HistoricalDocument_Report2 = entities.SP_RLCS_GetHistoricalDocCountByMonthYear_Report2(fromMonth, fromYear, toMonth, toYear).ToList();
                                    var REGRepoDocument_Report2 = entities.SP_RLCS_GetREGRepoDocCountByMonthYear_Report2(fromMonth, fromYear, toMonth, toYear).ToList();
                                    var RENDocument_Report2 = entities.SP_RLCS_GetRENDocCountByMonthYear_Report2(fromMonth, fromYear, toMonth, toYear).ToList();

                                    

                                    int Cnt = 1;
                                    int RowCount1 = 0;
                                    DateTime selectedDate = new DateTime(selectedYear, selectedMonth, 1);

                                    foreach (var item in ClientList)
                                    {
                                        string ClientID = item;
                                        string BranchName = lstClientsMonthYear_Report1.Where(x => x.LM_ClientID == item).Select(x => x.Entity).FirstOrDefault();
                                        int BranchCount = BranchCountData.Where(x => x.CM_ClientID.Equals(item)).Count();

                                        Output.Rows.Add(Cnt, ClientID, BranchName, BranchCount);

                                        int p = 0;
                                        int firstHalf = 4 + MonthList.Count;
                                        for (int j = 4; j < firstHalf; j++)
                                        {
                                            if (MonthList[p].Value == selectedDate)
                                            {
                                               // Output.Rows[RowCount1][j] = getDocumentCount_ClientWise_MonthYearWise(true, LatestDocument, HistoricalDocument, REGRepoDocument, RENDocument, MonthList[p].Value, item);
                                            }
                                            else
                                            {
                                               // Output.Rows[RowCount1][j] = getDocumentCount_ClientWise_MonthYearWise(false, LatestDocument, HistoricalDocument, REGRepoDocument, RENDocument, MonthList[p].Value, item);
                                                //Output.Rows[RowCount1][j] = getCount_Includes_RegRetChallans(LatestDocument, HistoricalDocument, MonthList[p].Value, item);
                                            }
                                            p++;
                                        }

                                        Output.Rows[RowCount1][firstHalf] = "";
                                        int q = 0;
                                        for (int j = firstHalf + 1; j < Output.Columns.Count; j++)
                                        {
                                            Output.Rows[RowCount1][j] = getDocumentCount_ClientWise_FromMonthYearToMonthYear(false, LatestDocument_Report2, HistoricalDocument_Report2, REGRepoDocument_Report2, RENDocument_Report2, MonthList[q].Value, item, "");

                                            //if (MonthList[q].Value == selectedDate)
                                            //{
                                            //    Output.Rows[RowCount1][j] = getDocumentCount_ClientWise_FromMonthYearToMonthYear(true, LatestDocument_Report2, HistoricalDocument_Report2, REGRepoDocument_Report2, RENDocument_Report2, MonthList[q].Value, item);
                                            //}
                                            //else
                                            //{
                                            //    Output.Rows[RowCount1][j] = getDocumentCount_ClientWise_FromMonthYearToMonthYear(true, LatestDocument_Report2, HistoricalDocument_Report2, REGRepoDocument_Report2, RENDocument_Report2, MonthList[q].Value, item);
                                            //}

                                            q++;
                                        }

                                        RowCount1++;
                                        Cnt++;
                                    }

                                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Client Monthly Report");

                                    System.Data.DataTable ExcelData1 = null;

                                    DataView view1 = new System.Data.DataView(Output);

                                    //ExcelData1 = view1.ToTable("Selected", false, "Sr_No","RM_ClientID", "BranchCount", "M1", "M2", "M3", "M4", "M5", "M6");
                                    ExcelData1 = view1.ToTable();

                                    string MergeCoulmnName = string.Empty;
                                    int value = MonthList.Count + MonthList.Count + 5 + 64;
                                    if (value > 90)
                                    {
                                        int val = value - 26;
                                        char sval = Convert.ToChar(val);
                                        string ColName = "" + sval.ToString();
                                        string ColID = "A" + ColName;
                                        MergeCoulmnName = "A1:" + ColID + "1";
                                    }
                                    else
                                    {
                                        char LastColumnName = Convert.ToChar(value);
                                        MergeCoulmnName = "A1:" + LastColumnName + "1";
                                    }


                                    exWorkSheet1.Cells["A1"].Value = "RLCS-Client(s) Monthly Usage Report-" + ddlMonth.SelectedItem + "-" + ddlYear.SelectedValue;
                                    exWorkSheet1.Cells[MergeCoulmnName].Merge = true;
                                    exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["A1"].Style.Font.Size = 14;
                                    exWorkSheet1.Cells[MergeCoulmnName].Style.Font.Italic = true;
                                    exWorkSheet1.Cells[MergeCoulmnName].Style.Font.UnderLine = true;
                                    exWorkSheet1.Cells[MergeCoulmnName].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells[MergeCoulmnName].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;


                                    string MergeCoulmnName1 = string.Empty;
                                    int value1 = MonthList.Count + 4 + 64;
                                    if (value1 > 90)
                                    {
                                        int val = value1 - 26;
                                        char sval = Convert.ToChar(val);
                                        string ColName = "" + sval.ToString();
                                        string ColID = "A" + ColName;
                                        MergeCoulmnName1 = "E2:" + ColID + "2";
                                    }
                                    else
                                    {
                                        char LastColumnName = Convert.ToChar(value1);
                                        MergeCoulmnName1 = "E2:" + LastColumnName + "2";
                                    }


                                    exWorkSheet1.Cells["E2"].Value = "Documents Migrated in-" + ddlMonth.SelectedItem.Text.ToString() + "-" + ddlYear.SelectedValue;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Merge = true;
                                    exWorkSheet1.Cells["E2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["E2"].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells["E2"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["E2"].Style.Font.Size = 14;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Font.Italic = true;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Font.UnderLine = true;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName1].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;

                                    #region header second chart
                                    value1 = value1 + 2;
                                    //char LastColumnNamecheck = Convert.ToChar(value1);
                                    string startcharacter = string.Empty;
                                    if (value1 > 90)
                                    {
                                        int val = value1 - 26;
                                        char sval = Convert.ToChar(val);
                                        string ColName = "" + sval.ToString();
                                        startcharacter = ColName + "2";
                                    }
                                    else
                                    {
                                        char sval = Convert.ToChar(value1);
                                        string ColName = "" + sval.ToString();
                                        startcharacter = ColName + "2";
                                    }
                                    string MergeCoulmnName12 = string.Empty;
                                    int value12 = MonthList.Count + MonthList.Count + 5 + 64;
                                    if (value12 > 90)
                                    {
                                        int val = value12 - 26;
                                        char sval = Convert.ToChar(val);
                                        string ColName = "" + sval.ToString();
                                        string ColID = "A" + ColName;
                                        MergeCoulmnName12 = startcharacter + ":" + ColID + "2";
                                    }
                                    else
                                    {
                                        char LastColumnName = Convert.ToChar(value12);
                                        MergeCoulmnName12 = startcharacter + ":" + LastColumnName + "2";
                                    }

                                    int yearid = Convert.ToInt32(ddlYear.SelectedValue);
                                    int monthid = Convert.ToInt32(ddlMonth.SelectedValue) - 1;
                                    string ToMonthName = string.Empty;
                                    if (monthid == 0)
                                    {
                                        ToMonthName = "Dec";
                                        yearid = yearid - 1;
                                    }
                                    else
                                        ToMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(monthid);

                                    exWorkSheet1.Cells[startcharacter].Value = "Documents Migrated from April " + yearid + " to " + ToMonthName + "-" + yearid;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Merge = true;
                                    exWorkSheet1.Cells[startcharacter].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells[startcharacter].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    exWorkSheet1.Cells[startcharacter].Style.Font.Bold = true;
                                    exWorkSheet1.Cells[startcharacter].Style.Font.Size = 14;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Font.Italic = true;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Font.UnderLine = true;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                                    exWorkSheet1.Cells[MergeCoulmnName12].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                                    #endregion

                                    exWorkSheet1.Cells["A3"].LoadFromDataTable(ExcelData1, true);

                                    exWorkSheet1.Cells["A3"].Value = "Sr No";
                                    exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["A3"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["A3"].AutoFitColumns(5);
                                    exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["A3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                    exWorkSheet1.Cells["B3"].Value = "ClientID";
                                    exWorkSheet1.Cells["B3"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["B3"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["B3"].AutoFitColumns(5);
                                    exWorkSheet1.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["B3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["B3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["B3"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                    exWorkSheet1.Cells["C3"].Value = "Client Name";
                                    exWorkSheet1.Cells["C3"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["C3"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["C3"].AutoFitColumns(40);
                                    exWorkSheet1.Cells["C3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["C3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["C3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    exWorkSheet1.Cells["C3"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                    exWorkSheet1.Cells["D3"].Value = "Branch Count";
                                    exWorkSheet1.Cells["D3"].Style.Font.Bold = true;
                                    exWorkSheet1.Cells["D3"].Style.Font.Size = 12;
                                    exWorkSheet1.Cells["D3"].AutoFitColumns(15);
                                    exWorkSheet1.Cells["D3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["D3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    //exWorkSheet1.Cells["D2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    //exWorkSheet1.Cells["D2"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                    int StartFrom = 69;
                                    for (int i = 0; i < (MonthList.Count); i++)
                                    {
                                        if (StartFrom > 90)
                                        {
                                            int val = StartFrom - 26;
                                            char sval = Convert.ToChar(val);
                                            string ColName = "" + sval.ToString();
                                            string ColID = "A" + ColName;
                                            exWorkSheet1.Cells[ColID + "3"].Value = MonthList[i].Value.ToString("MMM yyyy");
                                            exWorkSheet1.Cells[ColID + "3"].Style.Font.Bold = true;
                                            exWorkSheet1.Cells[ColID + "3"].Style.Font.Size = 12;
                                            exWorkSheet1.Cells[ColID + "3"].AutoFitColumns(15);
                                            exWorkSheet1.Cells[ColID + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells[ColID + "3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                            exWorkSheet1.Cells[ColID + "3"].Style.WrapText = true;
                                            StartFrom++;
                                        }
                                        else
                                        {
                                            char sval = Convert.ToChar(StartFrom);
                                            exWorkSheet1.Cells["" + sval + "3"].Value = MonthList[i].Value.ToString("MMM yyyy");
                                            exWorkSheet1.Cells["" + sval + "3"].Style.Font.Bold = true;
                                            exWorkSheet1.Cells["" + sval + "3"].Style.Font.Size = 12;
                                            exWorkSheet1.Cells["" + sval + "3"].AutoFitColumns(15);
                                            exWorkSheet1.Cells["" + sval + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells["" + sval + "3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                            exWorkSheet1.Cells["" + sval + "3"].Style.WrapText = true;
                                            StartFrom++;
                                        }
                                    }

                                    if (StartFrom > 90)
                                    {
                                        int val = StartFrom - 26;
                                        char sval = Convert.ToChar(val);
                                        string ColName = "" + sval.ToString();
                                        string ColID = "A" + ColName;
                                        exWorkSheet1.Cells[ColID + "3"].Value = "";
                                        exWorkSheet1.Cells[ColID + "3"].AutoFitColumns(15);
                                        StartFrom++;
                                    }
                                    else
                                    {
                                        char svalEmpty = Convert.ToChar(StartFrom);
                                        string ColID = "" + svalEmpty.ToString();
                                        exWorkSheet1.Cells[ColID + "3"].Value = "";
                                        exWorkSheet1.Cells[ColID + "3"].AutoFitColumns(15);
                                        StartFrom++;
                                    }

                                    for (int i = 0; i < (MonthList.Count); i++)
                                    {
                                        if (StartFrom > 90)
                                        {
                                            int val = StartFrom - 26;
                                            char sval = Convert.ToChar(val);
                                            string ColName = "" + sval.ToString();
                                            string ColID = "A" + ColName;

                                            exWorkSheet1.Cells[ColID + "3"].Value = MonthList[i].Value.ToString("MMM yyyy");
                                            exWorkSheet1.Cells[ColID + "3"].Style.Font.Bold = true;
                                            exWorkSheet1.Cells[ColID + "3"].Style.Font.Size = 12;
                                            exWorkSheet1.Cells[ColID + "3"].AutoFitColumns(15);
                                            exWorkSheet1.Cells[ColID + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells[ColID + "3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                            exWorkSheet1.Cells[ColID + "3"].Style.WrapText = true;
                                            StartFrom++;
                                        }
                                        else
                                        {
                                            char sval = Convert.ToChar(StartFrom);
                                            exWorkSheet1.Cells["" + sval + "3"].Value = MonthList[i].Value.ToString("MMM yyyy");
                                            exWorkSheet1.Cells["" + sval + "3"].Style.Font.Bold = true;
                                            exWorkSheet1.Cells["" + sval + "3"].Style.Font.Size = 12;
                                            exWorkSheet1.Cells["" + sval + "3"].AutoFitColumns(15);
                                            exWorkSheet1.Cells["" + sval + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells["" + sval + "3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                            exWorkSheet1.Cells["" + sval + "3"].Style.WrapText = true;
                                            StartFrom++;
                                        }
                                    }

                                    using (ExcelRange col = exWorkSheet1.Cells[2, 1, 2 + ExcelData1.Rows.Count + 1, (MonthList.Count) + (MonthList.Count) + 5])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }

                                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                                    Response.ClearContent();
                                    Response.Buffer = true;
                                    Response.AddHeader("content-disposition", "attachment;filename=ClientMonthlyReport-" + ddlMonth.SelectedItem + "-" + ddlYear.SelectedValue + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                                    Response.Charset = "";
                                    Response.ContentType = "application/vnd.ms-excel";
                                    StringWriter sw = new StringWriter();
                                    Response.BinaryWrite(fileBytes);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Available for Selected Month-Year";
                                }
                            }//Using End
                            #endregion
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Select Month and Year Before Proceed";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Select Month and Year Before Proceed";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void GetFromMonthToMonth(int month, int year)
        {
            try
            {
                if (month != 0 && year != 0)
                {
                    int fromMonth = 4;
                    int fromYear = 2019;

                    int toMonth = 0;
                    int toYear = 0;

                    if (month == 1)
                    {
                        toMonth = month - 1;
                        toYear = year - 1;
                    }
                    else
                    {
                        toMonth = month - 1;
                        toYear = year;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected string GetMergeColumnName(int value, string startColChar, int startRowIndex, int endRowIndex)
        {
            try
            {
                string MergeCoulmnName = string.Empty;

                if (value > 90)
                {
                    int val = value;
                    int iterateCount = 0;

                    do
                    {
                        val = val - 26;
                        iterateCount++;
                    } while (val > 90);

                    string[] alphabets = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

                    string tillColumnName = alphabets[iterateCount - 1];

                    char sval = Convert.ToChar(val);
                    string ColName = "" + sval.ToString();
                    string ColID = tillColumnName + ColName;
                    MergeCoulmnName = startColChar + startRowIndex + ":" + ColID + endRowIndex;
                }

                return MergeCoulmnName;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        protected string GetColumnName(int value)
        {
            try
            {
                string ColID = string.Empty;

                if (value > 90)
                {
                    int val = value;
                    int iterateCount = 0;

                    do
                    {
                        val = val - 26;
                        iterateCount++;
                    } while (val > 90);

                    string[] alphabets = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

                    string tillColumnName = alphabets[iterateCount - 1];

                    char sval = Convert.ToChar(val);
                    string ColName = "" + sval.ToString();
                    ColID = tillColumnName + ColName;

                }

                return ColID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        protected void upRLCSClientReport_Load(object sender, EventArgs e)
        {
            try
            {  
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeCombobox", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}