﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{

    public partial class VerticalMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerListFilter();
                BindCustomerList();
                if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
                {
                    if (ddlCustomerFilter.SelectedValue != "-1")
                    {
                        BindVerticalData(Convert.ToInt32(ddlCustomerFilter.SelectedValue));
                    }
                    else
                    {
                        grdAuditor.DataSource = null;
                        grdAuditor.DataBind();
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                    }
                }

                bindPageNumber();
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditor.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
            int customerID = -1;
            if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
            {
                if (ddlCustomerFilter.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
                }
            }
            BindVerticalData(customerID);
        }

        private void BindVerticalData(int customerID)
        {
            try
            {
                var Verti = UserManagementRisk.GetAllVertical(customerID);
                grdAuditor.DataSource = Verti;
                Session["TotalRows"] = Verti.Count;
                grdAuditor.DataBind();
                upPromotorList.Update();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
                {
                    if (ddlCustomerFilter.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
                    }
                }

                //Reload the Grid
                BindVerticalData(customerID);
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                        return;
                    }
                }

                mst_Vertical objVerti = new mst_Vertical()
                {
                    VerticalName = txtFName.Text,
                    CustomerID = customerID
                };

                if ((int)ViewState["Mode"] == 0)
                {
                    if (UserManagementRisk.VerticalNameExist(objVerti))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Vertical with same name already exists.";
                    }
                    else
                    {
                        UserManagementRisk.CreateVerticalName(objVerti);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Vertical Name Save Successfully.";
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    objVerti.ID = Convert.ToInt32(ViewState["VertiID"]);
                    UserManagementRisk.UpdateVerticalName(objVerti);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Vertical Name Updated Successfully.";
                }
                BindCustomerListFilter();
                if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
                {
                    if (ddlCustomerFilter.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
                    }
                }
                BindVerticalData(customerID);
                GetPageDisplaySummary();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }


                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReviewerScript", "fclosepopup();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                txtFName.Text = string.Empty;
                BindCustomerList();
                ddlCustomer.Enabled = true;
                upPromotor.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("Edit_Vertical"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["VertiID"] = ID;
                    BindVerticalData(Convert.ToInt32(ddlCustomerFilter.SelectedValue));
                    string value = ddlCustomerFilter.SelectedValue;
                    if (!string.IsNullOrEmpty(value))
                    {
                        ddlCustomer.SelectedValue = value;
                    }
                    ddlCustomer.Enabled = false;
                    mst_Vertical objVertical = UserManagementRisk.GetVerticalListByID(ID);

                    if (objVertical != null)
                        txtFName.Text = objVertical.VerticalName;

                    upPromotor.Update();
                }
                else if (e.CommandName.Equals("DELETE_Vertical"))
                {
                    UserManagementRisk.DeleteVertical(ID);

                    int customerID = -1;
                    if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
                    {
                        if (ddlCustomerFilter.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
                        }
                    }
                    BindVerticalData(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private void GetPageDisplaySummary()
        {
        }

        private void BindCustomerListFilter()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDL(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomerFilter.DataTextField = "CustomerName";
                ddlCustomerFilter.DataValueField = "CustomerId";
                ddlCustomerFilter.DataSource = customerList;
                ddlCustomerFilter.DataBind();
                ddlCustomerFilter.Items.Insert(0, new ListItem("Select Customer", "-1"));
                ddlCustomerFilter.SelectedIndex = 2;
            }
            else
            {
                ddlCustomerFilter.DataSource = null;
                ddlCustomerFilter.DataBind();
            }
        }

        protected void ddlCustomerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
            {
                if (ddlCustomerFilter.SelectedValue != "-1")
                {
                    BindVerticalData(Convert.ToInt32(ddlCustomerFilter.SelectedValue));
                }
                else
                {
                    grdAuditor.DataSource = null;
                    grdAuditor.DataBind();
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                }
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue == "-1")
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                }
            }
        }
        private void BindCustomerList()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDL(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
                // ddlCustomer.SelectedIndex = 2;
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();
            }
        }
    }
}