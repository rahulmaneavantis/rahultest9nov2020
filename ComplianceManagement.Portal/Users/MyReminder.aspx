﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="MyReminder.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.MyReminder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <style type="text/css">
            .k-grid-content
        {
            min-height:394px !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        /*.k-widget k-window {
            margin-left: 3%;
            margin-top: 2%;
        }*/
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

          .myKendoCustomClass {
            z-index:999 !important;
        }

          .btn-search 
          {
             background: #1fd9e1;
             /*float: left;*/
          }

          .k-grouping-header
        {
           color: #515967;
           font-style: italic;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.6em;
        }
       
        .k-grid td 
        {
            border-style: solid;
            border-width: 0 0 0 1px;
            padding: .7em .6em;
            overflow: hidden;
            line-height: 1.6em;
            vertical-align: middle;
            text-overflow: ellipsis;
       }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-grouping-header 
        {
           color: #515967;
           font-style: italic;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        /*.k-grid,
        .k-grid-header-wrap,
        .k-grid-header th,
        .k-grid-content > table > tbody > tr > td {
            border: none;
        }*/
        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        /*.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            margin-top: 9px;
        }*/
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }
        .k-grouping-header {
             border-right: solid 1px #ceced2;
               border-left: solid 1px #ceced2;
}
        table.k-selectable {
        /*border: solid 1px red;*/
        border-right: solid 1px #ceced2;
        }
        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
    </style>

    <script type="text/javascript">

        function FilterAll() {

            var Riskdetails = [];
            if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            }

            var Statusdetails = [];
            if ($("#dropdownlistStatus").data("kendoDropDownTree") != undefined) {
                Statusdetails = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
            }


            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values
 
            if (locationlist.length > 0 || Riskdetails.length > 0
                || Statusdetails.length > 0) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }
                if (Riskdetails.length > 0) {
                    var CategoryFilter = { logic: "or", filters: [] };

                    $.each(Riskdetails, function (i, v) {

                        CategoryFilter.filters.push({
                            field: "RiskType", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(CategoryFilter);
                }
                if (Statusdetails.length > 0) {
                    var StatusFilter = { logic: "or", filters: [] };

                    $.each(Statusdetails, function (i, v) {
                        StatusFilter.filters.push({
                            field: "Type", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(StatusFilter);
                }
                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }


        //$('#Searchfilter').on('input', function (e) {
        //    FilterGrid();
        //});


        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
            //$('#Searchfilter').val('');
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Compliance Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#ClearfilterMain').css('display', 'block');
            }


            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:7px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }

        //function OpenAdvanceSearchFilter(e) {
        //    $('#divAdvanceSearchFilterModel').modal('show');
        //    e.preventDefault();
        //    return false;
        //}

    </script>
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetUserReminder?UserID=<% =UId%>&Customerid=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>/Data/GetUserReminder?UserID=<% =UId%>&Customerid=<% =CustId%>'                       
                    },
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                toolbar: kendo.template($("#template").html()),
                //height: 523,
                sortable: true,
                groupable: true,
                filterable: true,
                editable: false,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                 
                    {
                        field: "ShortDescription", title: 'ShortDescription',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }
                    , {
                        field: "CustomerBranchName", title: 'Location',
                        width: "18%;",
                        attributes: {
                            style: 'white-space: nowrap;'                         
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Role", title: 'Role',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap; width:100px;'                          
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                        //, dataBound: function (e) {
                        //    e.sender.list.width("1000");
                        //}
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        width: "15%;",
                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                         filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "RemindOn", title: 'Reminder On',
                        width: "15%;",
                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                         filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Status", title: 'Status',
                        width: "15%;",
                        template: "#if(Status == '0') {#<span>Pending</span>#} else {#<span>clear</span>#}#",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }

                ]
            });


            $("#grid").kendoTooltip({
                filter: "td:nth-child(1)", //this filter selects the second column's cells
                position: "down",
                width: 250,
                content: function (e) {                 
                    var content = e.target.context.textContent;                   
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(2)", //this filter selects the second column's cells
                position: "down",
                width: 150,
                content: function (e) {                                      
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "down",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "down",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "down",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(6)", //this filter selects the second column's cells
                position: "down",
                width: 150,
                animation: {
                    open: {
                        effects: "fade:in"
                    }},
                content: function (e) {                                     
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(7)", //this filter selects the second column's cells
                position: "down",
                width: 150,
                content: function (e) {                                                     
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            function hrefTest(Status) {
                alert("Status is: " + Status);
            }

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                $('#divShowReminderDialog').modal('show');
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../aspxPages/AddEditReminder.aspx?AccessID=" + item.ReminderID)

            });
        }


        function onChangeSD() {
            //FilterGrid();
        }
        function onChangeLD() {
            //FilterGrid();
        }

        function BindGridApply(e) {
            BindGrid();
            FilterGrid();
            e.preventDefault();
        }

        
        $(document).ready(function () {
            BindGrid();

            $("#txtSearchfilter").on('input', function (e) {

                var grid = $('#grid').data('kendoGrid');
                var columns = grid.columns;

                var filter = { logic: 'or', filters: [] };

                columns.forEach(function (x) {
                    if (x.field == "ShortDescription" || x.field == "CustomerBranchName" || x.field == "Role" || x.field == "ScheduledOn" || x.field == "RemindOn" || x.field == "Status") {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        });
                    }
                });
                grid.dataSource.filter(filter);

            });


            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Compliance Type",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterAll();
                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');

                },
                dataSource: [
                    { text: "Statutory", value: "S" },
                    { text: "Internal", value: "I" }
                ]
            });


            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterAll();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');

                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" } 
                    
                ]
            });


            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {    
                    FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
            
        });

        function AddnewTest(e) {
            $('#divShowReminderDialog').modal('show');
            $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../users/ReminderNew.aspx?AccessID=0")
            e.preventDefault();
           }
    
        function CloseMyReminderPopup() {
            $('#divShowReminderDialog').modal('hide');
            Bindgrid();
        }

      function CloseClearPopup() {
          $('#APIOverView').attr('src', "../Common/blank.html");
      }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        
            <div class="row">
                <div class="row">
                    <input id="dropdownlistStatus" data-placeholder="Compliance Type" style="width: 242px;"/>
                    <input id="dropdownlistRisk" data-placeholder="Risk" style="width: 190px;"/>
                    <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 375px;"/>
                    <input id='txtSearchfilter' class='k-textbox' placeholder="Type to Filter" style="width: 175px;"/>
                    <%--<button id="Addnew" style="height: 35px; float:right; width:100px;" onclick="AddnewTest(event)" class="btn btn-search">Add New</button>--%>
                    <button id="Addnew" style="height: 29px; width: 102px;" onclick="AddnewTest(event)"><span class="k-icon k-i-plus" onclick="javascript:return false;"></span>Add New</button>
                    <%--<button id="Applyfilter" style="margin-left: 1%;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button> --%>
                    <button id="ClearfilterMain" style="float: right; margin-right: 5px; margin-left: 1%; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                </div>
            </div>

            <div class="clearfix" style="height: 10px;"></div>
            <div class="row" style="padding-bottom: 0px; font-size: 12px; display: none;color: #535b6a;font-weight:bold;" id="filterrisk">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;color: #535b6a;font-weight:bold;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;color: #535b6a;font-weight:bold;" id="filterstatus">&nbsp;</div>
            <div id="grid" style="border: none;"></div>
        </div>

    <div class="modal fade" id="divShowReminderDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="height: 390px;">
                <div class="modal-header" style="height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseMyReminderPopup();">&times;</button>
                </div>

                <div class="modal-body">
                    <iframe src="about:blank" id="showReminderDetail" scrolling="no" frameborder="0" runat="server" width="100%" height="300px" style="height: 320px;"></iframe>
                </div>
            </div>
        </div>
    </div>

    <script>

            $(document).ready(function () {
                fhead('My Reminder');
            });

    </script>

</asp:Content>
