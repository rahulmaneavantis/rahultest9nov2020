﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class Block_Ip : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    BindIPDetailsList(Convert.ToInt32(AuthenticationHelper.CustomerID)); 
                }
                else
                {                    
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }            
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (customerID != -1)
                {
                    Mst_BlockIP obj = new Mst_BlockIP()
                    {
                        IPName = txtIPName.Text.Trim(),
                        IPAddress = txtIPAddress.Text.Trim(),
                        IsActive = true,
                        CustomerID = (int)customerID,
                    };
                    if (txtremark.Text != "" && txtremark.Text != null)
                    {
                        obj.Remark = txtremark.Text.Trim();
                    }
                    if ((int)ViewState["Mode"] == 1)
                    {
                        obj.Id = Convert.ToInt32(ViewState["BlockIPID"]);
                    }
                    if ((int)ViewState["Mode"] == 0)
                    {
                        if ((IPAddBlock.ipExistNew(obj,"")))
                        {                           
                            CustomModify.IsValid = false;
                            CustomModify.ErrorMessage = "Name Or IP Address already exists";
                            vsLicenseListPage.CssClass = "alert alert-danger";
                        }
                        else
                        {
                            IPAddBlock.CreateIPMaster(obj);
                            
                            CustomModify.IsValid = false;
                            CustomModify.ErrorMessage = "IP Block Details saved successfully";
                            vsLicenseListPage.CssClass = "alert alert-success";
                            clearAll();
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        if (IPAddBlock.ipExists(obj, txtremark.Text))
                        {
                            CustomModify.IsValid = false;
                            CustomModify.ErrorMessage = "Name Or IP Address already exists";
                            vsLicenseListPage.CssClass = "alert alert-danger";
                        }
                        else
                        {
                            IPAddBlock.UpdateIPMaster(obj);
                            CustomModify.IsValid = false;
                            CustomModify.ErrorMessage = "IP Block Details updated successfully";
                            vsLicenseListPage.CssClass = "alert alert-success";
                            clearAll();
                        }
                    }
                    BindIPDetailsList(customerID);
                    UpIpAddress.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                vsLicenseListPage.CssClass = "alert alert-danger";
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            txtIPName.Text = string.Empty;
            txtIPAddress.Text = string.Empty;
            txtremark.Text = string.Empty;
        }
        protected void txtIPName_TextChanged(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if(customerID!=-1)
                    {
                    var result= IPAddBlock.GetAllMasterList(customerID);
                    var name = txtIPName.Text;
                   var  result1 = (result.Where(x => x.IPName == name)).FirstOrDefault();

                    if (result1!=null)
                    {
                        if (name != null)
                        {
                            txtIPAddress.Text = result1.IpAddress;
                        }
                        else
                        {
                            txtIPAddress.Text = "";
                        }
                    }
                    else
                    {
                        txtIPAddress.Text = "";
                    }
                }

                }
        }

        private void BindIPDetailsList(int CustomerID)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (customerID != -1)
                {
                    var IPMasterList = IPAddBlock.GetAllMasterList(customerID);
                    
                    var filter = tbxFilter.Text.Trim();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        IPMasterList = IPMasterList.Where(entry => entry.IpAddress.ToUpper().Trim().Contains(filter.ToUpper())
                        || entry.IPName.ToUpper().Trim().Contains(filter.ToUpper())
                        || (entry.Remark != null && entry.Remark.ToUpper().Contains(filter.ToUpper()))).ToList();
                    }
                    grdIPAddress.DataSource = IPMasterList;
                    Session["TotalRows"] = IPMasterList.Count;
                    grdIPAddress.DataBind();

                    if (IPMasterList.Count>0)
                    {
                        btnGridDelete.Visible = true;
                    }
                    upModifyDepartment.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdIPAddress_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {              
                int blockIpID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_IPBlock"))
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    if (customerID != -1)
                    {
                        ViewState["Mode"] = 1;
                        ViewState["BlockIPID"] = blockIpID;
                        Mst_BlockIP RPD = IPAddBlock.IPMasterGetByIDAudit(blockIpID, customerID);
                        txtIPName.Text = RPD.IPName;
                        txtIPAddress.Text = RPD.IPAddress;
                        txtremark.Text = RPD.Remark;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyDepartment\").dialog('open');", true);
                        upModifyDepartment.Update();
                    }
                }
                else if (e.CommandName.Equals("DELETE_IPBlock"))
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    if (customerID != -1)
                    {
                        bool checkdeletesucess = IPAddBlock.DeleteIPAddressMasterAudit(blockIpID, customerID);
                        if (!checkdeletesucess)
                        {
                            cvDuplicateEntry.IsValid = false;                            
                            cvDuplicateEntry.ErrorMessage = "IP Address already in use.";// "You Can't deleted because it is used in some groups.";
                            vsLicenseListPage.CssClass = "alert alert-danger";
                        }
                        BindIPDetailsList(customerID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdIPAddress.PageIndex = 0;
                int customerID = -1;

                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (customerID != -1)
                {
                    BindIPDetailsList(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upDepeList_Load(object sender, EventArgs e)
        {
        }
        protected void grdIPAddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdIPAddress.PageIndex = e.NewPageIndex;
                int customerID = -1;

                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (customerID != -1)
                {
                    BindIPDetailsList(customerID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upModifyAssignment_Load(object sender, EventArgs e)
        {
           
        }
      
        public void clearAll()
        {
            txtIPName.Text = string.Empty;
            txtIPAddress.Text = string.Empty;
            txtremark.Text = string.Empty;
        }
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                clearAll();
                upModifyDepartment.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyDepartment\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnGridDelete_Click(object sender, EventArgs e)
        {
            List<int> checkdata = new List<int>();
            foreach (GridViewRow grdrow in grdIPAddress.Rows)
            {
                CheckBox chk = (CheckBox)grdrow.FindControl("chkIPData");
                 if (chk != null & chk.Checked)
                {
                    string id = (grdrow.Cells[1].FindControl("lblID") as Label).Text;
                    checkdata.Add(int.Parse(id));
                }
            }
            int customerID = -1;
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            if (customerID != -1)
            {
                if(checkdata.Count>0)
                {
                    foreach (var item in checkdata)
                    {
                        bool checkdeletesucess = IPAddBlock.DeleteIPAddressMasterAudit(item, customerID);
                        if (!checkdeletesucess)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "You Can't deleted because it is used in some groups.";
                            vsLicenseListPage.CssClass = "alert alert-danger";
                        }                     
                    }
                    BindIPDetailsList(customerID);
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select at least one IP Address.";
                    vsLicenseListPage.CssClass = "alert alert-danger";
                }
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        int customerID = -1;
                        customerID =Convert.ToInt32( AuthenticationHelper.CustomerID);
                        var IPMasterList = IPAddBlock.GetAllMasterList(customerID);

                        var filter = tbxFilter.Text.Trim();
                        if (!string.IsNullOrEmpty(filter))
                        {
                            IPMasterList = IPMasterList.Where(entry => entry.IpAddress.ToUpper().Trim().Contains(filter.ToUpper())
                            || entry.IPName.ToUpper().Trim().Contains(filter.ToUpper())
                            || (entry.Remark != null && entry.Remark.ToUpper().Contains(filter.ToUpper()))).ToList();
                        }

                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("IPDetailReport");
                        DataTable ExcelData = null;
                        DateTime CurrentDate = DateTime.Today.Date;
                        DataView view = new System.Data.DataView((DataTable)(IPMasterList).ToDataTable());
                        ExcelData = view.ToTable("Selected", false, "IPName", "IPAddress", "Remark");
                        exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A1"].Value = "Location";
                        exWorkSheet.Cells["A1"].AutoFitColumns(30);

                        exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B1"].Value = "IP Address";
                        exWorkSheet.Cells["B1"].AutoFitColumns(30);

                        exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C1"].Value = "Remark";
                        exWorkSheet.Cells["C1"].AutoFitColumns(30);
                        
                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 3])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=IPDetailReport.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.SuppressContent = true;
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); 
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {

                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = IPAddressSheetsExitsts(xlWorkbook, "IP Address");
                            if (flag == true)
                            {
                                ProcessIPBlockData(xlWorkbook);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please correct the sheet name.";
                            }

                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
                }
            }
        }

        private void ProcessIPBlockData(ExcelPackage xlWorkbook)
        {
            try
            {

                List<Mst_BlockIP> mst_IP = new List<Mst_BlockIP>();
                bool suucessmsg = false;
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int datacount = 0;
                string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                Regex check = new Regex(pattern);
                bool valid = false;
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["IP Address"];
                if (xlWorksheet != null)
                {
                    List<string> errorMessage = new List<string>();
                    List<string> uploded_data = new List<string>();
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    string Location = string.Empty;
                    string IpAddress = string.Empty;
                    string Remarks = string.Empty;
                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        string Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                        count = count + 1;
                        Location = string.Empty;
                        IpAddress = string.Empty;
                        Remarks = string.Empty;

                        #region 1 Location
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            Location = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                        }
                        if (String.IsNullOrEmpty(Location))
                        {
                            valid = false;
                            errorMessage.Add("Required Location at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 2 Notice Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            IpAddress = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                            valid = check.IsMatch(IpAddress, 0);
                            if (valid)
                            {
                                valid = true;
                            }
                            else
                            {
                                valid = false;
                                errorMessage.Add("Required Valid IP Address at row number - " + (count + 1) + "");
                            }

                        }

                        if (String.IsNullOrEmpty(IpAddress))
                        {
                            errorMessage.Add("Required valid IP Address at row number - " + (count + 1) + "");
                        }
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            Remarks = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                        }
                        #endregion
                    }
                    #endregion
                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            Location = string.Empty;
                            IpAddress = string.Empty;
                            Remarks = string.Empty;
                            #region 1 location

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                            {
                                Location = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            }
                            #endregion
                            #region 2 IP Address
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))

                                IpAddress = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();

                            #endregion
                            #region 3  Remark
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            {
                                Remarks = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                            }
                            #endregion

                            if (customerID != -1)
                            {
                                Mst_BlockIP mst = new Mst_BlockIP();
                                {
                                    mst.IPName = Location;
                                    mst.IPAddress = Convert.ToString(IpAddress);
                                    mst.CustomerID = customerID;
                                    mst.IsActive = true;
                                }
                                if (!string.IsNullOrEmpty(Remarks))
                                {
                                    mst.Remark = Remarks;
                                }
                                if (IPAddBlock.ipExistNew(mst, ""))
                                {
                                    var getId = IPAddBlock.ipIDcheck(mst);
                                    mst.Id = getId;
                                    IPAddBlock.UpdateIPMaster(mst);
                                    datacount++;
                                    errorMessage.Add("IP Details Saved Successfully");
                                }
                                else
                                {
                                    IPAddBlock.CreateIPMaster(mst);
                                    datacount++;
                                }
                            }
                            suucessmsg = true;
                        }

                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                    }
                    if (suucessmsg)
                    {
                        if (datacount > 0)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = datacount + " IP Block Details saved successfully";
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Data Uploaded from Excel Document";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "No data found in excel document.";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again";
                }
                BindIPDetailsList(customerID);
                UpIpAddress.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again later";
            }
        }

        private bool IPAddressSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("IP Address"))
                    {
                        if (sheet.Name.Trim().Equals("IP Address"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
    }
}