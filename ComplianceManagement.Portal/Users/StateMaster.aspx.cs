﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class StateMaster : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "C")
                this.MasterPageFile = "~/NewCompliance.Master";
            else if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "L")
                this.MasterPageFile = "~/LitigationMaster.Master";
            else
                this.MasterPageFile = "~/AuditTool.Master";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";

                BindStateData();
                BindCountry();

                bindPageNumber();
            }
        }

        public void BindCountry()
        {
            var CountryList = CityStateCountryManagement.GetAllContryData();

            ddlCountry.DataTextField = "Name";
            ddlCountry.DataValueField = "ID";

            ddlCountry.DataSource = CountryList;
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select Country", "-1"));

            ddlCountryList.DataTextField = "Name";
            ddlCountryList.DataValueField = "ID";

            ddlCountryList.DataSource = CountryList;
            ddlCountryList.DataBind();
            ddlCountryList.Items.Insert(0, new ListItem("Select Country", "-1"));
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditor.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindStateData();
        }

        private void BindStateData()
        {
            try
            {
                var StateList = CityStateCountryManagement.GetAllState();
                if (!string.IsNullOrEmpty(ddlCountryList.SelectedValue))
                {
                    if (ddlCountryList.SelectedValue != "-1")
                    {
                        StateList = StateList.Where(Entry => Entry.CountryID == Convert.ToInt32(ddlCountryList.SelectedValue)).Distinct().ToList();
                    }
                }
                if (!string.IsNullOrEmpty(tbxSearchState.Text))
                {
                    StateList = StateList.Where(Entry => Entry.Name.Equals(tbxSearchState.Text)).Distinct().ToList();
                }
                grdAuditor.DataSource = StateList;
                Session["TotalRows"] = StateList.Count;
                grdAuditor.DataBind();

                //upPromotorList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                State objState = new State()
                {
                    Name = txtFName.Text,
                    CountryID = Convert.ToInt32(ddlCountry.SelectedValue)
                };

                mst_State ObjStateAudit = new mst_State()
                {
                    Name = txtFName.Text,
                    CountryID = Convert.ToInt32(ddlCountry.SelectedValue)
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    objState.ID = Convert.ToInt32(ViewState["StateID"]);//Need to change DeptID
                    ObjStateAudit.ID = Convert.ToInt32(ViewState["StateID"]);//Need to change DeptID
                }

                if ((int) ViewState["Mode"] == 0)
                {
                    if (CityStateContryManagementRisk.StateExist(ObjStateAudit.Name))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "State Already Exists";
                    }
                    else
                    {
                        ObjStateAudit.IsDeleted = false;
                        CityStateContryManagementRisk.CreateState(ObjStateAudit);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "State Saved Successfully";

                        txtFName.Text = string.Empty;
                        ddlCountry.SelectedIndex = -1;
                    }

                    //Compliace
                    if (CityStateCountryManagement.StateExist(objState.Name))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "State Already Exists";
                    }
                    else
                    {
                        objState.IsDeleted = false;
                        CityStateCountryManagement.CreateState(objState);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "State Saved Successfully";

                        txtFName.Text = string.Empty;
                        ddlCountry.SelectedIndex = -1;
                    }
                }

                else if ((int) ViewState["Mode"] == 1)
                {
                    CityStateContryManagementRisk.UpdateState(ObjStateAudit);
                    CityStateCountryManagement.UpdateState(objState);

                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "State Updated Successfully";
                }

                BindStateData();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindStateData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                txtFName.Text = string.Empty;
                ddlCountry.SelectedIndex = -1;
                upPromotor.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAuditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_State"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["StateID"] = ID;

                    State objstate = CityStateCountryManagement.StateGetByID(ID);

                    txtFName.Text = objstate.Name;
                    ddlCountry.SelectedValue = Convert.ToString(objstate.CountryID);

                    upPromotor.Update();
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorDialog\").dialog('open')", true);

                }
                else if (e.CommandName.Equals("DELETE_State"))
                {
                    CityStateCountryManagement.DeleteState(ID);
                    CityStateContryManagementRisk.DeleteState(ID);
                    BindStateData();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void ddlCountryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindStateData();
        }

        protected void tbxSearchState_TextChanged(object sender, EventArgs e)
        {
            BindStateData();
        }
    }
}