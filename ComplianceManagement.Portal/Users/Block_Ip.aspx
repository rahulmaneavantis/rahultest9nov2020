﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="Block_Ip.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.Block_Ip" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .custom-combobox-input {
            margin: 0;
            padding: 0.3em;
            width: 258px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#divModifyDepartment').dialog({
                height: 400,
                width: 580,
                top: 100,
                autoOpen: false,
                draggable: true,
                title: "IP BLOCK DETAILS",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
    <script type="text/javascript">
        function Text() {
            var txt1 = document.getElementById('<%= txtIPName.ClientID %>').value;
            document.getElementById('<%= txtIPAddress.ClientID %>').value = txt1;
        }
    </script>
    <script type="text/javascript">
        function checkAll(cb) {

            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIPData") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function UncheckHeader() {

            var rowCheckBox = $("#CheckBox input[id*='chkIPData']");
            var rowCheckBoxSelected = $("#CheckBox input[id*='chkIPData']:checked");
            var rowCheckBoxHeader = $("#CheckBox input[id*='chkIPDataHeader']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {
                rowCheckBoxHeader[0].checked = false;
            }
        }
        function showProgress() {
            var updateProgress = $get("<%# updateProgress.ClientID %>");
            updateProgress.style.display = "block";
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div style="float: right; margin-top: 10px; margin-right: 70px; margin-top: 5px;">
    </div>
    <asp:UpdatePanel ID="UpIpAddress" runat="server" UpdateMode="Conditional" OnLoad="upDepeList_Load">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" CssClass="vdsummary" runat="server" EnableClientScript="False" />
            </div>
            <table width="100%">                
                <tr>                    
                    <td align="left" style="width: 25%;">
                        <label style="font-size: 13px;">
                            Filter :</label>
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td>
                        <asp:FileUpload ID="MasterFileUpload" runat="server" Width="205px" />
                    </td>
                    <td style="float: right;">
                        <asp:Button ID="btnUploadFile" runat="server" Text="Upload" OnClick="btnUpload_Click" CssClass="button" />
                    </td>
                    <td align="right" style="width: 9%">
                        <u><a href="../../ExcelFormat/IPAddress.xlsx">Sample Format</a></u>
                    </td>
                    <td align="right" class="newlink">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddUser" OnClick="btnAddUser_Click" />
                    </td>
                    <td align="right">
                        <asp:LinkButton runat="server" ID="lbtnExportExcel" OnClick="lbtnExportExcel_Click" Style="margin-top: 15px; margin-top: -5px; margin-right: 20px"><img src="../Images/excel.png" alt="Export to Excel"
                         title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel2" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdIPAddress" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" Width="100%" Font-Size="12px"
                    OnPageIndexChanging="grdIPAddress_PageIndexChanging"
                    OnRowCommand="grdIPAddress_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" ItemStyle-Width="150" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("IPID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="left" ItemStyle-Width="8%">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkIPDataHeader" Text="Select All" ItemStyle-HorizontalAlign="Left" runat="server" onclick="checkAll(this)" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIPData" runat="server" Style="padding-left: 11px;" ItemStyle-HorizontalAlign="Left" onclick="UncheckHeader()" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="IPName" HeaderText="Location" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="25%" />
                        <asp:BoundField DataField="IPAddress" HeaderText="IP Address" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="25%" />
                        <asp:BoundField DataField="Remark" HeaderText="Remark" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="37%" />
                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_IPBlock" OnClientClick="fopenpopup()"
                                    CommandArgument='<%# Eval("IPID") %>'><img src="../../Images/edit_icon.png" alt="Edit IP Address Details" title="Edit Address Details" /></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_IPBlock" OnClientClick="return confirm('Are you certain you want to delete this IP Details?');"
                                    CommandArgument='<%# Eval("IPID") %>'><img src="../../Images/delete_icon.png" alt="Delete IP Address Details" title="Delete IP Details" /></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

                <div style="float: left;">
                    <asp:Button Text="Delete" runat="server" OnClick="btnGridDelete_Click"
                        Style="margin-top: 10px" ID="btnGridDelete" Visible="false"
                        CssClass="button" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divModifyDepartment">
        <asp:UpdatePanel ID="upModifyDepartment" runat="server" UpdateMode="Conditional" OnLoad="upModifyAssignment_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary  ID="vsLicenseListPage" runat="server"  Style="padding-left: 5%"  Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ModifyIPValidationGroup" />
                                    <asp:CustomValidator ID="CustomModify" runat="server" Style="padding-left: 5%" EnableClientScript="False"
                                        ValidationGroup="ModifyIPValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />


                                                  
                                </div>
                            </td>
                        </tr>
                        <tr>

                            <td style="width: 15%;">
                                <label style="width: 82px; display: block; float: left; font-size: 13px; color: red; text-align: right; margin-bottom: 15px; margin-left: -15px">*</label>
                                <label style="width: 143px; display: block; font-size: 13px; color: #333; text-align: right; margin-bottom: 15px">
                                    Location</label>
                            </td>
                            <td style="width: 35%; margin-bottom: 12px;">
                                <asp:TextBox runat="server" ID="txtIPName" CssClass="form-control" Style="width: 200px; margin-bottom: 12px" MaxLength="100" />
                                <%--OnTextChanged="txtIPName_TextChanged" AutoPostBack="True" --%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                    ErrorMessage="Location can not be empty." ControlToValidate="txtIPName"
                                    runat="server" ValidationGroup="ModifyIPValidationGroup" Display="None" />

                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None"
                                    runat="server" ValidationGroup="ModifyIPValidationGroup"
                                    ErrorMessage="Please enter valid Location" ControlToValidate="txtIPName"
                                    ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ &-:.]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                <label style="width: 70px; display: block; float: left; font-size: 13px; color: red; text-align: right; margin-bottom: 15px; margin-left: -5px">*</label>
                                <label style="width: 143px; display: block; font-size: 13px; color: #333; text-align: right; margin-bottom: 15px">
                                    IP Address</label>
                            </td>
                            <td style="width: 35%; margin-bottom: 12px;">
                                <asp:TextBox runat="server" ID="txtIPAddress" CssClass="form-control" Style="width: 200px; margin-bottom: 12px" MaxLength="100" Text='<%#Bind("IPAddress") %>' />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="IP Address can not be empty."
                                    ControlToValidate="txtIPAddress"
                                    runat="server" ValidationGroup="ModifyIPValidationGroup" Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None"
                                    runat="server" ValidationGroup="ModifyIPValidationGroup"
                                    ErrorMessage="Please enter  Valid IP Address" ControlToValidate="txtIPAddress"
                                    ValidationExpression="^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                <label style="width: 143px; display: block; font-size: 13px; color: #333; text-align: right">
                                    Remark</label>
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox runat="server" ID="txtremark" CssClass="form-control" Style="width: 200px;" MaxLength="100" TextMode="MultiLine" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="margin-bottom: 7px;">
                                <div style="margin-bottom: 7px; float: left; margin-left: 50px; margin-top: 10px;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>
                <div style="margin-bottom: 7px; margin-top: 10px; margin-left: 10%;">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click"
                        CssClass="button" ValidationGroup="ModifyIPValidationGroup" />

                    <asp:Button Text="Delete" runat="server" Style="margin-left: 10px" ID="delete" OnClick="btnDelete_Click"
                        CssClass="button" />
                    <asp:Button Text="Close" runat="server" ID="btnClose" Style="margin-left: 10px" CssClass="button" OnClientClick="$('#divModifyDepartment').dialog('close');" />
                </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnExportExcel" />
                <asp:PostBackTrigger ControlID="btnUploadFile" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

</asp:Content>
