﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class PerformerReviewerStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {

                string str = Request.QueryString["CID"].ToString();

                if (!string.IsNullOrEmpty(str))
                {
                    string[] s2 = str.Split(',');

                    long ComplianceInstanceID = Convert.ToInt64(s2[0]);
                    String flag = Convert.ToString(s2[1]);

                    if (flag == "I")
                    {

                        #region Internal                        
                        var GridInternalData = GetInternalCannedReportDataForPerformer((int)AuthenticationHelper.CustomerID, (int)ComplianceInstanceID);
                        GridInternalData = GridInternalData.Where(row => row.InternalComplianceInstanceID == ComplianceInstanceID).ToList();
                        grdComplianceTransactions.DataSource = GridInternalData;
                        grdComplianceTransactions.DataBind();
                        #endregion

                    }
                    else if (flag == "S")
                    {                        
                        var GridData = GetCannedReportDataForPerformer((int)AuthenticationHelper.CustomerID, (int)ComplianceInstanceID);
                        GridData = GridData.Where(row => row.ComplianceInstanceID == ComplianceInstanceID).ToList();
                        grdComplianceTransactions.DataSource = GridData;
                        grdComplianceTransactions.DataBind();
                    }
                }
                //if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                //{                    
                //    long ComplianceInstanceId = Convert.ToInt32(Request.QueryString["CID"].ToString());                   
                //    var GridData = GetCannedReportDataForPerformer((int)AuthenticationHelper.CustomerID, (int)ComplianceInstanceId);
                //    GridData = GridData.Where(row => row.ComplianceInstanceID == ComplianceInstanceId).ToList(); 
                //    grdComplianceTransactions.DataSource = GridData;
                //    grdComplianceTransactions.DataBind();
                //}
            }
        }
        public static List<SP_GetCompliancesStatusSummary_Result> GetCannedReportDataForPerformer(int Customerid, int ComplianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCompliancesStatusSummary_Result> transactionsQuery = new List<SP_GetCompliancesStatusSummary_Result>();
                transactionsQuery = (entities.SP_GetCompliancesStatusSummary(Customerid, ComplianceInstanceId)).ToList();
                return transactionsQuery.ToList();
            }
        }
        public static List<SP_GetInternalCompliancesStatusSummary_Result> GetInternalCannedReportDataForPerformer(int Customerid, int ComplianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetInternalCompliancesStatusSummary_Result> transactionsQuery = new List<SP_GetInternalCompliancesStatusSummary_Result>();
                transactionsQuery = (entities.SP_GetInternalCompliancesStatusSummary(Customerid, ComplianceInstanceId)).ToList();
                return transactionsQuery.ToList();
            }
        }
    }
}