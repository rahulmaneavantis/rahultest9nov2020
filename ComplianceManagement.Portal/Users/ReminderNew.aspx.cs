﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class ReminderNew : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AuthenticationHelper.CustomerID == 63)
                {

                    ddlComType.SelectedValue = "0";

                    ddlComType.Items.Remove(ddlComType.Items.FindByValue("-1"));

                }

                BindInputLists();

                tbxDate.Attributes.Add("readonly", "readonly");

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
        }
        protected void ddlComType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlComType.SelectedValue == "-1")
                {
                    BindInputLists();
                }
                else if (ddlComType.SelectedValue == "0")
                {
                    BindInputListsInternal();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindInputLists()
        {
            try
            {
                List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);

                //ViewState["AssignedCompliances"] = assignedCompliances;//commented by manisha

                //if ((assignedCompliances != null) && (assignedCompliances.Count > 0))
                //{
                //    ViewState["AssignedCompliances"] = assignedCompliances;//added by Manisha and commented by Manisha
                //}

                ddlBranch.DataSource = assignedCompliances.Select(entry => new { ID = entry.CustomerBranchID, Name = entry.Branch }).Distinct().OrderBy(entry => entry.Name).ToList();
                ddlBranch.DataBind();
                ddlBranch.Items.Insert(0, new ListItem("< Select >", "-1"));

                ddlBranch_SelectedIndexChanged(null, null);

                ////--------added by Manisha-------------------------------------------------------------------------
                //ddlCompliance.DataSource = assignedCompliances.Select(entry => new { ID = entry.ComplianceID, Name = entry.ShortDescription }).Distinct().OrderBy(entry => entry.Name).ToList();
                //ddlCompliance.DataBind();
                //ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));

                //ddlCompliance_SelectedIndexChanged(null, null);
                ////---------added by Manisha-----------------------------------------------------------
                //ddlRole.DataSource = assignedCompliances.Select(entry => new { ID = entry.RoleID, Name = entry.Role }).Distinct().OrderBy(entry => entry.Name).ToList();
                //ddlRole.DataBind();
                //ddlRole.Items.Insert(0, new ListItem("< Select >", "-1"));


                //----------------------------------------------------------------------------------

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindInputListsInternal()
        {
            try
            {
                List<InternalComplianceInstanceAssignmentView> assignedCompliances = Business.InternalComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);

                //ViewState["AssignedCompliances"] = assignedCompliances;//commented by manisha

                //if ((assignedCompliances != null) && (assignedCompliances.Count > 0))
                //{
                //    ViewState["AssignedCompliances"] = assignedCompliances;//added by Manisha and commented by Manisha
                //}

                ddlBranch.DataSource = assignedCompliances.Select(entry => new { ID = entry.CustomerBranchID, Name = entry.Branch }).Distinct().OrderBy(entry => entry.Name).ToList();
                ddlBranch.DataBind();
                ddlBranch.Items.Insert(0, new ListItem("< Select >", "-1"));

                ddlBranch_SelectedIndexChanged(null, null);

                ////--------added by Manisha-------------------------------------------------------------------------
                //ddlCompliance.DataSource = assignedCompliances.Select(entry => new { ID = entry.ComplianceID, Name = entry.ShortDescription }).Distinct().OrderBy(entry => entry.Name).ToList();
                //ddlCompliance.DataBind();
                //ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));

                //ddlCompliance_SelectedIndexChanged(null, null);
                ////---------added by Manisha-----------------------------------------------------------
                //ddlRole.DataSource = assignedCompliances.Select(entry => new { ID = entry.RoleID, Name = entry.Role }).Distinct().OrderBy(entry => entry.Name).ToList();
                //ddlRole.DataBind();
                //ddlRole.Items.Insert(0, new ListItem("< Select >", "-1"));


                //----------------------------------------------------------------------------------

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlComType.SelectedValue == "-1")
                {
                    List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);

                    ddlCompliance.ClearSelection();
                    ddlCompliance.DataSource = null;
                    ddlCompliance.DataBind();

                    ddlCompliance.DataSource = assignedCompliances.Where(entry => entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue)).Select(entry => new { ID = entry.ComplianceID, Name = entry.ShortDescription })
                        .OrderBy(entry => entry.Name).Distinct().ToList();
                    ddlCompliance.DataBind();

                    ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
                    ddlCompliance_SelectedIndexChanged(null, null);
                }

                else if (ddlComType.SelectedValue == "0")
                {
                    List<InternalComplianceInstanceAssignmentView> assignedCompliances = Business.InternalComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);

                    ddlCompliance.ClearSelection();
                    ddlCompliance.DataSource = null;
                    ddlCompliance.DataBind();

                    ddlCompliance.DataSource = assignedCompliances.Where(entry => entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue)).Select(entry => new { ID = entry.ComplianceID, Name = entry.IShortDescription })
                        .OrderBy(entry => entry.Name).Distinct().ToList();
                    ddlCompliance.DataBind();

                    ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
                    ddlCompliance_SelectedIndexChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlComType.SelectedValue == "-1")
                {
                    List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);
                    ddlRole.ClearSelection();
                    ddlRole.DataSource = null;
                    ddlRole.DataBind();

                    //if (ViewState["AssignedCompliances"] != null)//added by manisha
                    if (assignedCompliances != null)//added by manisha
                    {
                        if (assignedCompliances.Count > 0)//added by manisha
                        {
                            if ((ddlCompliance.SelectedValue != null) && (ddlCompliance.SelectedValue != string.Empty) && (ddlCompliance.SelectedValue != "-1")) //added by manisha
                            {
                                ddlRole.DataSource = assignedCompliances.Where(entry => entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue) && entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue)).Select(entry => new { ID = entry.RoleID, Name = entry.Role })
                                    .OrderBy(entry => entry.Name).Distinct().ToList();
                                ddlRole.DataBind();

                                ddlRole.Items.Insert(0, new ListItem("< Select >", "-1"));


                                DateTime scheduledOn = assignedCompliances.Where(entry => entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue) && entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue)).Select(entry => entry.ScheduledOn).FirstOrDefault();
                                if (scheduledOn != DateTime.MinValue)
                                {
                                    litScheduledOn.Text = scheduledOn == null ? string.Empty : scheduledOn.ToString("dd-MM-yyyy");
                                }
                            }
                        }
                    }
                }

                else if (ddlComType.SelectedValue == "0")
                {
                    List<InternalComplianceInstanceAssignmentView> assignedCompliances = Business.InternalComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);
                    ddlRole.ClearSelection();
                    ddlRole.DataSource = null;
                    ddlRole.DataBind();

                    //if (ViewState["AssignedCompliances"] != null)//added by manisha
                    if (assignedCompliances != null)//added by manisha
                    {
                        if (assignedCompliances.Count > 0)//added by manisha
                        {
                            if ((ddlCompliance.SelectedValue != null) && (ddlCompliance.SelectedValue != string.Empty) && (ddlCompliance.SelectedValue != "-1")) //added by manisha
                            {
                                ddlRole.DataSource = assignedCompliances.Where(entry => entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue) && entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue)).Select(entry => new { ID = entry.RoleID, Name = entry.Role })
                                    .OrderBy(entry => entry.Name).Distinct().ToList();
                                ddlRole.DataBind();

                                ddlRole.Items.Insert(0, new ListItem("< Select >", "-1"));


                                DateTime scheduledOn = assignedCompliances.Where(entry => entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue) && entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue)).Select(entry => entry.ScheduledOn).FirstOrDefault();
                                if (scheduledOn != DateTime.MinValue)
                                {
                                    litScheduledOn.Text = scheduledOn == null ? string.Empty : scheduledOn.ToString("dd-MM-yyyy");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlComType.SelectedValue == "-1")
                {
                    List<ComplianceInstanceAssignmentView> assignedCompliances = Business.ComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);
                    var assignment = assignedCompliances.Where(entry => entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue) && entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue) && entry.RoleID == Convert.ToInt32(ddlRole.SelectedValue)).FirstOrDefault();

                    ComplianceReminder reminder = new ComplianceReminder()
                    {
                        ComplianceDueDate = (DateTime)assignment.ScheduledOn,
                        RemindOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        ComplianceAssignmentID = assignment.ComplianceAssignmentID
                    };

                    //if ((int)ViewState["Mode"] == 1)
                    //{
                    //    reminder.ID = Convert.ToInt32(ViewState["UserReminderID"]);
                    //}

                    //if ((int)ViewState["Mode"] == 0)
                    //{
                        if (!Business.ComplianceManagement.ExistsComplianceReminder(reminder))
                        {
                            Business.ComplianceManagement.CreateReminder(reminder);
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Reminder Saved Sucessfully.";

                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Same Reminder Already Exists.";
                        }
                    //}
                    //else if ((int)ViewState["Mode"] == 1)
                    //{
                    //    Business.ComplianceManagement.UpdateReminder(reminder);
                    //}
                }

                else if (ddlComType.SelectedValue == "0")
                {
                    List<InternalComplianceInstanceAssignmentView> assignedCompliances = Business.InternalComplianceManagement.GetAllInstances(userID: AuthenticationHelper.UserID);
                    var assignment = assignedCompliances.Where(entry => entry.ComplianceID == Convert.ToInt32(ddlCompliance.SelectedValue) && entry.CustomerBranchID == Convert.ToInt32(ddlBranch.SelectedValue) && entry.RoleID == Convert.ToInt32(ddlRole.SelectedValue)).FirstOrDefault();

                    InternalComplianceReminder reminder = new InternalComplianceReminder()
                    {
                        ComplianceDueDate = (DateTime)assignment.ScheduledOn,
                        RemindOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        ComplianceAssignmentID = assignment.ComplianceAssignmentID
                    };

                    //if ((int)ViewState["Mode"] == 1)
                    //{
                    //    reminder.ID = Convert.ToInt32(ViewState["UserReminderID"]);
                    //}

                    //if ((int)ViewState["Mode"] == 0)
                    //{
                        if (!Business.ComplianceManagement.ExistsInternalComplianceReminder(reminder))
                        {
                            Business.InternalComplianceManagement.CreateReminder(reminder);

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Reminder Saved Sucessfully.";
                            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", " $('#Newaddremider').modal('hide')", true);

                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);
                            //FillUserReminders();
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Same Reminder Already Exists.";
                        }
                    //}
                    //else if ((int)ViewState["Mode"] == 1)
                    //{
                    //    Business.InternalComplianceManagement.UpdateReminder(reminder);
                    //}
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", " $('#Newaddremider').modal('hide')", true);
            //FillUserReminders();

        }
        protected void upUserReminders_Load(object sender, EventArgs e)
        {
            try
            {
                //if (ViewState["AssignedCompliances"] != null)//commented by Manisha
                //{
                List<ComplianceInstanceAssignmentView> assignedCompliances = (List<ComplianceInstanceAssignmentView>)ViewState["AssignedCompliances"];

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }

                // ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
    }
}