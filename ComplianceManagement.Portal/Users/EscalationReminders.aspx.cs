﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System.IO;
using System.Collections;
using System.Globalization;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ComplianceDocument
{
    public partial class EscalationReminders : System.Web.UI.Page
    {
        static bool ReviewerFlag;
        protected static List<Int32> roles;
        protected string Reviewername;
        protected string Performername;
        static int UserRoleID;
        public static string CompDocReviewPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {                   
                    ReviewerFlag = false;                                                                           
                    BindLocationFilter();
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }                                                         
                    btnSearch_Click(sender, e);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        protected void upDownloadList_Load(object sender, EventArgs e)
        {
            try
            {              
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                string isstatutoryinternal = "";
                if (ddlDocType.SelectedItem.Text == "Statutory" || ddlDocType.SelectedItem.Text == "Event Based")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlDocType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


       
        protected void grdReviewerComplianceDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    TextBox EscDays = (TextBox)e.Row.FindControl("txtEscDays");
                    TextBox IntreimDays = (TextBox) e.Row.FindControl("txtInterimDays");
                    Button btnAdd = (Button) e.Row.FindControl("btnAdd");

                    if (EscDays.Text != "" && IntreimDays.Text != "")
                    {
                        btnAdd.Text = "Update";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void grdReviewerComplianceDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {            
            FillComplianceDocuments();
        }                                       
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {               
                SelectedPageNo.Text = "1";
                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                {

                    grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdReviewerComplianceDocument.PageIndex = 0;
                }               
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

       
        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            ReviewerFlag = true;
            FillComplianceDocuments();
        }

       
        

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public void FillComplianceDocuments()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int type = Convert.ToInt32(ddlDocType.SelectedValue);
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                String location = tvFilterLocation.SelectedNode.Text;
                Session["TotalRows"] = 0;
                var ComplianceDocs = ComplianceManagement.Business.ComplianceManagement.GetComplianceEscalation(AuthenticationHelper.UserID, customerID, type, risk, location);
                grdReviewerComplianceDocument.DataSource = ComplianceDocs;
                grdReviewerComplianceDocument.DataBind();
                Session["TotalRows"] = ComplianceDocs.Count;                
                grdReviewerComplianceDocument.Visible = true;
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected string GetPerformer(long complianceID, int customerbranchID)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserNameForEscalation(complianceID, customerbranchID);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }

        protected long GetPerformerID(long complianceID, int customerbranchID)
        {
            try
            {
                long result;
                result = DashboardManagement.GetUserIDPerformerForEscalation(complianceID, customerbranchID);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return 0;
            }
        }            
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                {                    
                    SelectedPageNo.Text = "1";
                    int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                    if (currentPageNo < GetTotalPagesCount())
                    {
                        SelectedPageNo.Text = (currentPageNo).ToString();
                    }

                    if (ReviewerFlag)
                    {
                        grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }               
                //Reload the Grid
                FillComplianceDocuments();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {         
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
               
                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                {
                     if (ReviewerFlag)
                    {
                        grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }               
                //Reload the Grid
                FillComplianceDocuments();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
               
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                {
                   
                        grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    
                }               
                //Reload the Grid
                FillComplianceDocuments();               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }

       
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }              
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {              
                Button btn = (Button)sender;
                //Get the row that contains this button
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;

                TextBox EscDays = (TextBox)gvr.FindControl("txtEscDays");
                TextBox InterimDays = (TextBox)gvr.FindControl("txtInterimDays");
                
                Label Perofmerlbl = (Label)gvr.FindControl("lblComplianceId");
                long ComplianceIds = Convert.ToInt32(Perofmerlbl.Text);

                Label CustomerBranchID = (Label) gvr.FindControl("lblUstomerBranchID");
                int CustomerBranchIDs = Convert.ToInt32(CustomerBranchID.Text);


                Button btnAdd = (Button) gvr.FindControl("btnAdd");
                if (btnAdd.Text =="Save")
                {
                    ComplianceCustomEscalation cmpcustomescalation = new ComplianceCustomEscalation();
                    {
                        cmpcustomescalation.ComplianceId =(int) ComplianceIds;
                        cmpcustomescalation.days = Convert.ToInt32(EscDays.Text);
                        cmpcustomescalation.Interimdays = Convert.ToInt32(InterimDays.Text);
                        cmpcustomescalation.UserIdReviewer = AuthenticationHelper.UserID;
                        cmpcustomescalation.CustomerBranchID = CustomerBranchIDs;
                        cmpcustomescalation.UserIdPerformer =(int)GetPerformerID( ComplianceIds, CustomerBranchIDs);

                    };

                    Business.ComplianceManagement.CreateEscalationReminders(cmpcustomescalation);
                }
                else
                {
                    ComplianceCustomEscalation cmpcustomescalation = new ComplianceCustomEscalation();
                    {
                        cmpcustomescalation.ComplianceId = (int) ComplianceIds;
                        cmpcustomescalation.days = Convert.ToInt32(EscDays.Text);
                        cmpcustomescalation.Interimdays = Convert.ToInt32(InterimDays.Text);
                        cmpcustomescalation.CustomerBranchID = CustomerBranchIDs;
                        cmpcustomescalation.UserIdReviewer = AuthenticationHelper.UserID;
                    };
                    Business.ComplianceManagement.UpdateEscalation(cmpcustomescalation);
                }
                FillComplianceDocuments();
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Data Saved Successfully.";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
    }
}