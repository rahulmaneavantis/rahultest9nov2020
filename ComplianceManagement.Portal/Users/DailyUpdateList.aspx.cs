﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class DailyUpdateList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {               
                BindComplianceCategories();               
                BindStates();
                BindActList();
                BindGrid();
            }
        }

        private void BindGrid()
        {
            try
            {
                int actID = -1;
                int stateID = -1;
                int categoryID = -1;
                string filtertext = string.Empty;

                if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                    actID = Convert.ToInt32(ddlAct.SelectedValue);

                if (!string.IsNullOrEmpty(ddlState.SelectedValue))
                    stateID = Convert.ToInt32(ddlState.SelectedValue);

                if (!string.IsNullOrEmpty(ddlComplianceCatagory.SelectedValue))
                    categoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);

                if (!string.IsNullOrEmpty(txtSearchType.Text))
                    filtertext = Convert.ToString(txtSearchType.Text).Trim().ToUpper();

                var AllDailyUpdates = DailyUpdateManagment.GetAllDailyUpdateList(categoryID, stateID, actID, filtertext);
                
                if (AllDailyUpdates != null)
                {
                    grdDailyUpdateList.DataSource = AllDailyUpdates;
                    Session["TotalRows"] = AllDailyUpdates.Count;
                    grdDailyUpdateList.DataBind();

                    GetPageDisplaySummary();                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    grdDailyUpdateList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdDailyUpdateList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }                

                //Reload the Grid
                BindGrid();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                    grdDailyUpdateList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdDailyUpdateList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }

                //Reload the Grid
                BindGrid();
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }

        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                    grdDailyUpdateList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdDailyUpdateList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }

                //Reload the Grid
                BindGrid();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }

        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        protected void grdDailyUpdateList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();
        }

        //Added by Amita as on 9 Jan 2019
        private void BindActList()
        {
            try
            {
                int catagoryID = -1;
                int stateID = -1;

                ddlAct.Items.Clear();

                if (!string.IsNullOrEmpty(ddlComplianceCatagory.SelectedValue))
                    catagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);

                if (!string.IsNullOrEmpty(ddlState.SelectedValue))
                    stateID = Convert.ToInt32(ddlState.SelectedValue);
                
                List<ActView> ActList = DailyUpdateManagment.GetAllActs(catagoryID, stateID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("Act", "-1")); 
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //Added by Amita as on 9 Jan 2019
        private void BindComplianceCategories()
        {
            try
            {
                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";
                ddlComplianceCatagory.DataSource = DailyUpdateManagment.GetAllComplianceCategories();
                ddlComplianceCatagory.DataBind();
                ddlComplianceCatagory.Items.Insert(0, new ListItem("Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //Added by Amita as on 9 Jan 2019
        private void BindStates()
        {
            try
            {               
                int categoryID = -1;

                ddlState.Items.Clear();

                if (!string.IsNullOrEmpty(ddlComplianceCatagory.SelectedValue))
                    categoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);

                List<State> stateList = DailyUpdateManagment.GetAllStates(categoryID);

                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";
                ddlState.Items.Insert(0, new ListItem("Compliance Type", "-1"));
                ddlState.Items.Insert(1, new ListItem("Central", "-2"));
                int count = ddlState.Items.Count;
                foreach (State item in stateList)
                {
                    ddlState.Items.Insert(count, new ListItem(item.Name, Convert.ToString(item.ID)));
                    count++;
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       

        //Added by Amita as on 9 jan 2019
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SelectedPageNo.Text = "1";
            int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

            if (currentPageNo <= GetTotalPagesCount())
            {
                SelectedPageNo.Text = (currentPageNo).ToString();
                grdDailyUpdateList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdDailyUpdateList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
            }

            BindGrid();
        }

        //Added by Amita as on 11 jan 2019
        protected void ddlComplianceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtSearchType.Text = string.Empty;
           
            BindStates();
            BindActList();
            btnSearch_Click(null,null);
        }

        //Added by Amita as on 9 jan 2019
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindActList();
            btnSearch_Click(null, null);
        }

        //Aded by Amita as on 18 Jan 2019
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }
    }
}