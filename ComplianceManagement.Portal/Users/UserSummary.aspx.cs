﻿using System;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.Controls;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class UserSummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (AuthenticationHelper.Role == "SADMN")
                    {
                        phSummaryControl.Controls.Add(Page.LoadControl("~/Controls/UserSummarySuperAdmin.ascx"));
                    }
                    else if (AuthenticationHelper.Role == "CADMN")
                    {
                        phSummaryControl.Controls.Add(Page.LoadControl("~/Controls/UsersSummaryCompanyAdmin.ascx"));
                    }
                }

                udcUserInputForm.OnSaved += (inputForm, args) => { phSummaryControl.Controls.Add(Page.LoadControl("~/Controls/UserSummarySuperAdmin.ascx")); };
                udcCustomerInputForm.OnSaved += (inputForm, args) => { phSummaryControl.Controls.Add(Page.LoadControl("~/Controls/UserSummarySuperAdmin.ascx")); };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbNewUser_Click(object sender, EventArgs e)
        {
            try
            {
                udcUserInputForm.AddNewUser();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbNewCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                udcCustomerInputForm.AddCustomer();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "initializeDatePicker", "initializeDatePicker(null)", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}