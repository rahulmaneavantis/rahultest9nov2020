﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Users
{
    public partial class ModifyAssignments : System.Web.UI.Page
    {
        public static List<long> locationList = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLocationFilter();
                BindUserList();
                tbxFilterLocation.Text = "< Select >";
                User user = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                BindNewUserList(user.CustomerID ?? -1, ddlNewUsers);

                int userID = Convert.ToInt32(ddlUserList.SelectedValue);
                if (rbtSelectionType.SelectedValue == "0")
                {
                    BindComplianceList();
                    BindComplianceInstances(userID);
                }
                else
                {
                    BindInternalComplianceList();
                    BindInternalComplianceInstances(userID);
                }
            }
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

        }
        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());           
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();

                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
           // tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            int userID = Convert.ToInt32(ddlUserList.SelectedValue);
            if (rbtSelectionType.SelectedValue == "0")
            {
                BindComplianceList();
                BindComplianceInstances(userID);
            }
            else
            {
                BindInternalComplianceList();
                BindInternalComplianceInstances(userID);
            }
        }
        public void BindUserList()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    if (AuthenticationHelper.Role == "CADMN")
                    {
                        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    }
                    var uselist = UserManagement.GetAllUser(customerID, string.Empty);

                    var user = (from row in uselist
                                select new UserName()
                                {
                                    ID = row.ID,
                                    Name = row.FirstName + " " + row.LastName
                                }).ToList();

                    user = user.OrderBy(x => x.Name).ToList();
                    ddlUserList.DataTextField = "Name";
                    ddlUserList.DataValueField = "ID";
                    ddlUserList.Items.Clear();
                    ddlUserList.DataSource = user;
                    ddlUserList.DataBind();
                    ddlUserList.Items.Insert(0, new ListItem("Select User", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public class UserName
        {
            public long ID { get; set; }
            public string Name { get; set; }
        }

        public class ComplianceDeatil
        {
            public long ID { get; set; }
            public string Name { get; set; }
        }
     
        //protected void RetrieveNodes(TreeNode node)
        //{
        //    if (node.Checked && node.ChildNodes.Count == 0) // && node.ChildNodes.Count == 0 if (node.Checked)
        //    {
        //        if (!locationList.Contains(Convert.ToInt32(node.Value)))
        //            locationList.Add(Convert.ToInt32(node.Value));
        //    }

        //    foreach (TreeNode tn in node.ChildNodes)
        //    {
        //        if (tn.Checked && tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)
        //        {
        //            if (!locationList.Contains(Convert.ToInt32(tn.Value)))
        //                locationList.Add(Convert.ToInt32(tn.Value));
        //        }

        //        if (tn.ChildNodes.Count != 0)
        //        {
        //            for (int i = 0; i < tn.ChildNodes.Count; i++)
        //            {
        //                RetrieveNodes(tn.ChildNodes[i]);
        //            }
        //        }
        //    }
        //}
        protected void RetrieveNodes(TreeNode node)
        {            
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }


                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != customerID)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != customerID)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {                
            }
        }
        public static bool FindNodeExists1(NameValueHierarchy item, List<int> ListIDs)
        {
            bool result = false;
            try
            {
                if (ListIDs.Contains(item.ID))
                {
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                        }
                        else
                        {
                            result = FindNodeExists1(childNode, ListIDs);
                        }

                        if (result)
                        {
                            break;
                        }
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        private void BindComplianceInstances(int userID)
        {
            try
            {                                
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                int CompID = -1;
                if (ddlCompliance.SelectedValue != "-1")
                {
                    CompID = Convert.ToInt32(ddlCompliance.SelectedValue);
                }
                int branchID = -1;
                locationList.Clear();

                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {                    
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }

                if (!(tbxFilterLocation.Text.Trim().Equals("< Select >")) && !(tbxFilterLocation.Text.Trim().Equals("< All >")))
                {
                   // branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                }
                //if (ddlCustomerList.SelectedValue != "-1")
                //{
                //    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                //}
                if (customerID != -1)
                {
                    grdTask.DataSource = null;
                    grdTask.DataBind();
                    if (rbtSelectType.SelectedValue == "0")
                    {
                        List<Sp_ComplianceAssignedInstanceNew_Result> datasource = new List<Sp_ComplianceAssignedInstanceNew_Result>();
                        if (chkEvent.Checked == true)
                        {
                            chkCheckList.Checked = false;
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();
                            datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("Y", customerID, userID, "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            //datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExclude("Y", customerID, userID, "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            //if (branchID > 0)
                            //{
                            //    datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();

                            //}
                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();
                            }
                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.ComplianceID == CompID).ToList();
                            }
                            grdComplianceInstances.DataSource = datasource;
                            grdComplianceInstances.DataBind();
                            
                        }
                        else if (chkCheckList.Checked == true)
                        {
                            chkEvent.Checked = false;
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();
                            datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("NA", customerID, userID, "Yes", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            //datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExclude("NA", customerID, userID, "Yes", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            //if (branchID > 0)
                            //{
                            //    datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            //}
                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();

                            }
                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.ComplianceID == CompID).ToList();
                            }
                            grdComplianceInstances.DataSource = datasource;
                            grdComplianceInstances.DataBind();
                            
                        }
                        else
                        {
                            chkEvent.Checked = false;
                            chkCheckList.Checked = false;
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();
                            datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("N", customerID, userID, "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            //datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExclude("N", customerID, userID, "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            //if (branchID > 0)
                            //{
                            //    datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            //}
                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();

                            }
                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.ComplianceID == CompID).ToList();
                            }
                            grdComplianceInstances.DataSource = datasource;
                            grdComplianceInstances.DataBind();
                            
                        }
                        grdInternalComplianceInstances.DataSource = null;
                        grdInternalComplianceInstances.DataBind();
                    }
                    else if (rbtSelectType.SelectedValue == "1")
                    {
                        grdComplianceInstances.DataSource = null;
                        grdComplianceInstances.DataBind();
                        grdInternalComplianceInstances.DataSource = null;
                        grdInternalComplianceInstances.DataBind();

                        if (rbtSelectionType.SelectedValue == "0")
                        {
                            grdTask.DataSource = null;
                            grdTask.DataBind();
                            List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result> datasource = new List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result>();
                            datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_InternalNew(customerID, userID, "S", txtAssigmnetFilter.Text).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            //datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_Internal(customerID, userID, "S", txtAssigmnetFilter.Text).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            //if (branchID > 0)
                            //{
                            //    datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            //}

                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();

                            }

                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.ComplianceID == CompID).ToList();
                            }
                            grdTask.DataSource = datasource;
                            grdTask.DataBind();
                            
                        }
                        if (rbtSelectionType.SelectedValue == "1")
                        {
                            grdTask.DataSource = null;
                            grdTask.DataBind();
                            List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result> datasource = new List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result>();
                            datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_InternalNew(customerID, userID, "I", txtAssigmnetFilter.Text).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            //datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_Internal(customerID, userID, "I", txtAssigmnetFilter.Text).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                            //if (branchID > 0)
                            //{
                            //    datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            //}

                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();

                            }

                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.ComplianceID == CompID).ToList();
                            }
                            grdTask.DataSource = datasource;
                            grdTask.DataBind();
                            
                        }
                    }
                    else
                    {
                        BindInternalComplianceInstances(userID);                        
                    }
                    //upModifyAssignment.Update();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //CustomModifyAsignment.IsValid = false;
                //CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindComplianceList()
        {
            try
            {
                 
                bool FlagIsStatutory = true;                
                int userID = Convert.ToInt32(ddlUserList.SelectedValue);
                List<ComplianceDeatil> objCompliance = new List<ComplianceDeatil>();
                try
                {
                    int customerID = -1;
                    if (AuthenticationHelper.Role == "CADMN")
                    {
                        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    }                 
                    if (customerID != -1)
                    {
                        if (rbtSelectType.SelectedValue == "0")
                        {
                            List<Sp_ComplianceAssignedInstanceNew_Result> datasource = new List<Sp_ComplianceAssignedInstanceNew_Result>();
                            if (chkEvent.Checked == true)
                            {
                                chkCheckList.Checked = false;
                                datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("Y", customerID, userID, "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                                objCompliance.Clear();
                                foreach (var item in datasource)
                                {
                                    objCompliance.Add(new ComplianceDeatil { ID = item.ComplianceID, Name = item.ShortDescription });
                                }
                            }
                            else if (chkCheckList.Checked == true)
                            {
                                chkEvent.Checked = false;
                                datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("NA", customerID, userID, "Yes", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                                objCompliance.Clear();
                                foreach (var item in datasource)
                                {
                                    objCompliance.Add(new ComplianceDeatil { ID = item.ComplianceID, Name = item.ShortDescription });
                                }
                            }
                            else
                            {
                                chkEvent.Checked = false;
                                chkCheckList.Checked = false;
                                datasource = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("N", customerID, userID, "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                                objCompliance.Clear();
                                foreach (var item in datasource)
                                {
                                    objCompliance.Add(new ComplianceDeatil { ID = item.ComplianceID, Name = item.ShortDescription });
                                }

                            }
                        }
                        else if (rbtSelectType.SelectedValue == "1")
                        {
                            if (rbtSelectionType.SelectedValue == "0")
                            {
                                List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result> datasource = new List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result>();
                                datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_InternalNew(customerID, userID, "S", txtAssigmnetFilter.Text).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                                objCompliance.Clear();
                                foreach (var item in datasource)
                                {
                                    objCompliance.Add(new ComplianceDeatil { ID = item.ComplianceID, Name = item.ShortDescription });
                                }

                            }
                            if (rbtSelectionType.SelectedValue == "1")
                            {
                                List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result> datasource = new List<Sp_TASK_Statutory_InternalComplianceAssignedInstanceNew_Result>();
                                datasource = ComplianceExclude_Reassign.GetAllTaskAssignedInstancesReassginExcludeStatutory_InternalNew(customerID, userID, "I", txtAssigmnetFilter.Text).GroupBy(entry => entry.TaskInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                                objCompliance.Clear();
                                foreach (var item in datasource)
                                {
                                    objCompliance.Add(new ComplianceDeatil { ID = item.ComplianceID, Name = item.ShortDescription });
                                }
                            }
                        }
                        else
                        {
                            FlagIsStatutory = false;
                            BindInternalComplianceList();
                        }
                    }

                    if (FlagIsStatutory)
                    {                       
                        var data= objCompliance.GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                        ddlCompliance.DataTextField = "Name";
                        ddlCompliance.DataValueField = "ID";
                        ddlCompliance.Items.Clear();
                        ddlCompliance.DataSource = data;
                        ddlCompliance.DataBind();
                        ddlCompliance.Items.Insert(0, new ListItem("Select Compliance", "-1"));
                    }

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //CustomModifyAsignment.IsValid = false;
                    //CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
                }                         
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindInternalComplianceList()
        {
            try
            {
                bool FlagIsInternal = true;
                List<ComplianceDeatil> objCompliance = new List<ComplianceDeatil>();
                int userID = Convert.ToInt32(ddlUserList.SelectedValue);
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }             
                if (customerID != -1)
                {
                    if (rbtSelectType.SelectedValue == "0")
                    {
                        if (chkCheckList.Checked == true)
                        {
                            var datasource = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("Y", customerID, userID, filter: txtAssigmnetFilter.Text).ToList();
                            objCompliance.Clear();
                            foreach (var item in datasource)
                            {
                                objCompliance.Add(new ComplianceDeatil { ID = item.InternalComplianceID, Name = item.IShortDescription });
                            }
                        }
                        else if (chkEvent.Checked == true)
                        {
                            objCompliance.Clear();                            
                        }
                        else
                        {
                            var datasource = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("N", customerID, userID, filter: txtAssigmnetFilter.Text).ToList();
                            objCompliance.Clear();
                            foreach (var item in datasource)
                            {
                                objCompliance.Add(new ComplianceDeatil { ID = item.InternalComplianceID, Name = item.IShortDescription });
                            }
                        }
                    }
                    else
                    {
                        FlagIsInternal = false;
                        BindComplianceList();
                    }
                }
                if (FlagIsInternal)
                {
                    var data = objCompliance.GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                    ddlCompliance.DataTextField = "Name";
                    ddlCompliance.DataValueField = "ID";
                    ddlCompliance.Items.Clear();
                    ddlCompliance.DataSource = data;
                    ddlCompliance.DataBind();
                    ddlCompliance.Items.Insert(0, new ListItem("Select Compliance", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //CustomModifyAsignment.IsValid = false;
                //CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindInternalComplianceInstances(int userID)
        {
            try
            {                
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                int branchID = -1;
                if (!(tbxFilterLocation.Text.Trim().Equals("< Select >")) && !(tbxFilterLocation.Text.Trim().Equals("< All >")))
                {
                   // branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                }

                locationList.Clear();

                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {                    
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }

                int CompID = -1;
                if (ddlCompliance.SelectedValue != "-1")
                {
                    CompID = Convert.ToInt32(ddlCompliance.SelectedValue);
                }
                if (customerID != -1)
                {
                    if (rbtSelectType.SelectedValue == "0")
                    {
                        if (chkCheckList.Checked == true)
                        {
                            var datasource = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("Y", customerID, userID, filter: txtAssigmnetFilter.Text).ToList();
                            //if (branchID > 0)
                            //{
                            //    datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            //}

                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();
                            }
                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.InternalComplianceID == CompID).ToList();
                            }
                            grdInternalComplianceInstances.DataSource = datasource;
                            grdInternalComplianceInstances.DataBind();

                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();

                        }
                        else if (chkEvent.Checked == true)
                        {
                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();

                            grdInternalComplianceInstances.DataSource = null;
                            grdInternalComplianceInstances.DataBind();
                        }
                        else
                        {
                            var datasource = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("N", customerID, userID, filter: txtAssigmnetFilter.Text).ToList();
                            //if (branchID > 0)
                            //{
                            //    datasource = datasource.Where(x => x.CustomerBranchID == branchID).ToList();
                            //}

                            if (locationList.Count > 0)
                            {
                                datasource = datasource.Where(x => locationList.Contains(x.CustomerBranchID)).ToList();
                            }

                            if (CompID > 0)
                            {
                                datasource = datasource.Where(x => x.InternalComplianceID == CompID).ToList();
                            }
                            grdInternalComplianceInstances.DataSource = datasource;
                            grdInternalComplianceInstances.DataBind();

                            grdComplianceInstances.DataSource = null;
                            grdComplianceInstances.DataBind();

                        }
                    }
                    else
                    {
                        BindComplianceInstances(userID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //CustomModifyAsignment.IsValid = false;
                //CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindNewUserList(int customerID, DropDownList ddllist)
        {
            try
            {
                var data = UserManagement.GetAllUserCustomerID(customerID, null);

                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";

                ddlNewReviewerUsers.DataTextField = "Name";
                ddlNewReviewerUsers.DataValueField = "ID";

                ddlNewApproverUsers.DataTextField = "Name";
                ddlNewApproverUsers.DataValueField = "ID";
                if (data != null)
                {
                    ddllist.DataSource = data;
                    ddllist.DataBind();

                    ddlNewReviewerUsers.DataSource = data;
                    ddlNewReviewerUsers.DataBind();

                    ddlNewApproverUsers.DataSource = data;
                    ddlNewApproverUsers.DataBind();
                }

                ddllist.Items.Insert(0, new ListItem("< Select user >", "-1"));
                ddlNewReviewerUsers.Items.Insert(0, new ListItem("< Select user >", "-1"));
                ddlNewApproverUsers.Items.Insert(0, new ListItem("< Select user >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //CustomModifyAsignment.IsValid = false;
                //CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbtSelectType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(ddlUserList.SelectedValue);
            if (rbtSelectionType.SelectedValue == "0")
            {
                if (rbtSelectType.SelectedValue == "0")
                {
                    divEventBase.Visible = true;
                    BindComplianceList();
                    BindComplianceInstances(userID);
                }
                else
                {
                    divEventBase.Visible = false;
                    BindComplianceList();
                    BindComplianceInstances(userID);
                }
            }
            else
            {
                BindInternalComplianceList();
                BindInternalComplianceInstances(userID);
            }            
        }

        protected void rbtSelectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(ddlUserList.SelectedValue);
            if (rbtSelectionType.SelectedValue == "0")
            {
                BindComplianceList();
                BindComplianceInstances(userID);
            }
            else
            {
                BindInternalComplianceList();
                BindInternalComplianceInstances(userID);
            }
        }

        protected void rbtModifyAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string action = Convert.ToString(rbtModifyAction.SelectedValue);
                if (action.Equals("Delete") || action.Equals("ApproverDelete"))
                {
                    divNewUser.Visible = false;
                }
                else
                {
                    divNewUser.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {
            chkCheckList.Checked = false;
            if (!string.IsNullOrEmpty(Convert.ToString(ddlUserList.SelectedValue)))
            {
                if (rbtSelectionType.SelectedValue == "0")
                {
                    BindComplianceList();
                    BindComplianceInstances(Convert.ToInt32(ddlUserList.SelectedValue));
                }
                else
                {
                    BindInternalComplianceList();
                    BindInternalComplianceInstances(Convert.ToInt32(ddlUserList.SelectedValue));
                }
            }

        }
        protected void chkCheckList_CheckedChanged(object sender, EventArgs e)
        {
            chkEvent.Checked = false;
            if (!string.IsNullOrEmpty(Convert.ToString(ddlUserList.SelectedValue)))
            {
                if (rbtSelectionType.SelectedValue == "0")
                {
                    BindComplianceList();
                    BindComplianceInstances(Convert.ToInt32(ddlUserList.SelectedValue));
                }
                else
                {
                    BindInternalComplianceList();
                    BindInternalComplianceInstances(Convert.ToInt32(ddlUserList.SelectedValue));
                }
            }
        }

        protected void txtAssigmnetFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int userID = Convert.ToInt32(ddlUserList.SelectedValue);
                if (rbtSelectionType.SelectedValue == "0")
                {
                    BindComplianceInstances(userID);
                }
                else
                {
                    BindInternalComplianceInstances(userID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
                     
        protected void grdComplianceInstances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["InstancesSortIndex"]);
                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CheckBoxValueSaved();
                grdComplianceInstances.PageIndex = e.NewPageIndex;
                if (!string.IsNullOrEmpty(Convert.ToString(ddlUserList.SelectedValue)))
                {
                    BindComplianceInstances(Convert.ToInt32(ddlUserList.SelectedValue));
                }
                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void AssignCheckBoxexValue()
        {

            List<Tuple<long, string>> chkList = (List<Tuple<long, string>>)ViewState["AssignedCompliancesID"];
            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
                {
                    int index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);                    
                    string role = "Performer";
                    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                    if (chkList.Contains(checkedData))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        protected void grdComplianceInstances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                if (customerID != -1)
                {
                    List<Sp_ComplianceAssignedInstanceNew_Result> assignmentList = new List<Sp_ComplianceAssignedInstanceNew_Result>();
                    if (chkEvent.Checked == true)
                    {
                        chkCheckList.Checked = false;
                        assignmentList = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("Y", customerID, Convert.ToInt32(ddlUserList.SelectedValue), "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();                        
                    }
                    else if (chkCheckList.Checked == true)
                    {
                        chkEvent.Checked = false;
                        assignmentList = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("NA", customerID, Convert.ToInt32(ddlUserList.SelectedValue), "Yes", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        chkEvent.Checked = false;
                        chkCheckList.Checked = false;
                        assignmentList = ComplianceExclude_Reassign.Statutory_GetAllAssignedInstancesReassginExcludeNew("N", customerID, Convert.ToInt32(ddlUserList.SelectedValue), "No", filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();                        
                    }
                    if (direction == SortDirection.Ascending)
                    {
                        assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Ascending;
                    }
                    foreach (DataControlField field in grdComplianceInstances.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["InstancesSortIndex"] = grdComplianceInstances.Columns.IndexOf(field);
                        }
                    }
                    grdComplianceInstances.DataSource = assignmentList;
                    grdComplianceInstances.DataBind();
                }
                else
                {
                    CustomModifyAsignment.IsValid = false;
                    CustomModifyAsignment.ErrorMessage = "Something went wrong, Please check customer selection & try again.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdInternalComplianceInstances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["InternalInstancesSortIndex"]);
                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                InternalCheckBoxValueSaved();
                grdInternalComplianceInstances.PageIndex = e.NewPageIndex;
                BindInternalComplianceInstances(Convert.ToInt32(ddlUserList.SelectedValue));
                InternalAssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void InternalCheckBoxValueSaved()
        {
            List<Tuple<long, string>> chkIList = new List<Tuple<long, string>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdInternalComplianceInstances.Rows)
            {
                index = Convert.ToInt32(grdInternalComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkICompliances")).Checked;
                string role = "Performer";

                if (ViewState["AssignedInternalCompliancesID"] != null)
                    chkIList = (List<Tuple<long, string>>)ViewState["AssignedInternalCompliancesID"];

                var checkedData = chkIList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkIList.Add(new Tuple<long, string>(index, role));
                }
                else
                    chkIList.Remove(checkedData);
            }
            if (chkIList != null && chkIList.Count > 0)
                ViewState["AssignedInternalCompliancesID"] = chkIList;
        }
        private void InternalAssignCheckBoxexValue()
        {

            List<Tuple<long, string>> chkIList = (List<Tuple<long, string>>)ViewState["AssignedInternalCompliancesID"];
            if (chkIList != null && chkIList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdInternalComplianceInstances.Rows)
                {
                    int index = Convert.ToInt32(grdInternalComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                    string role = "Performer";
                    var checkedIData = chkIList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                    if (chkIList.Contains(checkedIData))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkICompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }
        protected void grdInternalComplianceInstances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                if (customerID != -1)
                {
                    if (chkCheckList.Checked == true)
                    {
                        var InternalassignmentList = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("Y", customerID, Convert.ToInt32(ddlUserList.SelectedValue), filter: txtAssigmnetFilter.Text).OrderBy(entry => entry.Branch).ToList();

                        if (direction == SortDirection.Ascending)
                        {
                            InternalassignmentList = InternalassignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            InternalassignmentList = InternalassignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Ascending;
                        }
                        foreach (DataControlField field in grdInternalComplianceInstances.Columns)
                        {
                            if (field.SortExpression == e.SortExpression)
                            {
                                ViewState["InternalInstancesSortIndex"] = grdInternalComplianceInstances.Columns.IndexOf(field);
                            }
                        }

                        grdInternalComplianceInstances.DataSource = InternalassignmentList;
                        grdInternalComplianceInstances.DataBind();
                    }
                    else
                    {
                        var InternalassignmentList = ComplianceExclude_Reassign.Internal_GetAllAssignedInstancesNew("N", customerID, Convert.ToInt32(ddlUserList.SelectedValue), filter: txtAssigmnetFilter.Text).OrderBy(entry => entry.Branch).ToList();
                        if (direction == SortDirection.Ascending)
                        {
                            InternalassignmentList = InternalassignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            InternalassignmentList = InternalassignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            direction = SortDirection.Ascending;
                        }
                        foreach (DataControlField field in grdInternalComplianceInstances.Columns)
                        {
                            if (field.SortExpression == e.SortExpression)
                            {
                                ViewState["InternalInstancesSortIndex"] = grdInternalComplianceInstances.Columns.IndexOf(field);
                            }
                        }
                        grdInternalComplianceInstances.DataSource = InternalassignmentList;
                        grdInternalComplianceInstances.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdTask_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdTask_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Task_CheckBoxValueSaved();
                grdTask.PageIndex = e.NewPageIndex;
                if (!string.IsNullOrEmpty(Convert.ToString(ddlUserList.SelectedValue)))
                {
                    BindComplianceInstances(Convert.ToInt32(ddlUserList.SelectedValue));
                }
                Task_AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void Task_CheckBoxValueSaved()
        {
            List<Tuple<long, int>> chkList = new List<Tuple<long, int>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdTask.Rows)
            {
                index = Convert.ToInt32(grdTask.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkTask")).Checked;
                int role = 3;
                if (ViewState["AssignedTaskID"] != null)
                    chkList = (List<Tuple<long, int>>)ViewState["AssignedTaskID"];

                var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<long, int>(index, Convert.ToInt32(role)));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["AssignedTaskID"] = chkList;
        }
        private void Task_AssignCheckBoxexValue()
        {

            List<Tuple<long, string>> chkList = (List<Tuple<long, string>>)ViewState["AssignedTaskID"];
            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdTask.Rows)
                {
                    int index = Convert.ToInt32(grdTask.DataKeys[gvrow.RowIndex].Value);
                    int role = 3;
                    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                    if (chkList.Contains(checkedData))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkTask");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }
        protected void grdTask_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void CheckBoxValueSaved()
        {
            List<Tuple<long, string>> chkList = new List<Tuple<long, string>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
            {
                index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;
                string role = "Performer";
                if (ViewState["AssignedCompliancesID"] != null)
                    chkList = (List<Tuple<long, string>>)ViewState["AssignedCompliancesID"];

                var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<long, string>(index, role));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["AssignedCompliancesID"] = chkList;
        }

        protected void ddlUserList_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlUserList.SelectedValue != "-1")
            {
                int userID = Convert.ToInt32(ddlUserList.SelectedValue);
                chkEvent.Checked = false;
                chkCheckList.Checked = false;
                txtAssigmnetFilter.Text = string.Empty;
                ViewState["UserID"] = userID;
                User user = UserManagement.GetByID(userID);
                BindNewUserList(user.CustomerID ?? -1, ddlNewUsers);
                rbtSelectionType.SelectedValue = "0";

                BindComplianceList();
                BindComplianceInstances(userID);
                int IsIComplianceApp = 0;
                Customer customer = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                if (customer.IComplianceApplicable != null)
                {
                    IsIComplianceApp = Convert.ToInt32(customer.IComplianceApplicable);
                }
                if (IsIComplianceApp.ToString() == "1")
                {
                    divSelectionType.Visible = true;
                }
                else
                {
                    divSelectionType.Visible = false;
                }                
            }
            else
            {
                BindComplianceList();
                BindComplianceInstances(Convert.ToInt32(ddlUserList.SelectedValue));
            }
        }
        protected void btnSaveAssignment_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> c = new List<string>();
                c.Add("rahul@avantis.info");
                c.Add("Narendra@avantis.info");
                c.Add("deepali@avantis.info");
                c.Add("bhagyesh@avantis.net.in");
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string[] confirmValue = Request.Form["confirm_value"].Split(',');

                    if (confirmValue[confirmValue.Length - 1] == "Yes")
                    {
                        if (rbtSelectType.SelectedValue == "0")
                        {
                            if (rbtSelectionType.SelectedValue == "0")
                            {
                                #region Statutory
                                CheckBoxValueSaved();
                                List<Tuple<long, string>> chkList = (List<Tuple<long, string>>)ViewState["AssignedCompliancesID"];
                                if (chkList != null)
                                {
                                    string action = Convert.ToString(rbtModifyAction.SelectedValue);
                                    if (action.Equals("ApproverDelete"))
                                    {
                                        #region Delete Approver Compliance Assignment
                                        int customerID = -1;
                                        if (AuthenticationHelper.Role == "CADMN")
                                        {
                                            customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                                        }
                                        if (customerID != -1)
                                        {
                                            try
                                            {
                                                if (chkList.Count > 0)
                                                {
                                                    ComplianceExclude_Reassign.StatutoryApprover_DeleteComplianceAssignment(chkList, customerID, AuthenticationHelper.UserID);                                                    
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            }                                           
                                        }
                                        #endregion
                                    }
                                    else if (action.Equals("Delete"))
                                    {
                                        #region Delete Assignment
                                        int customerID = -1;
                                        if (AuthenticationHelper.Role == "CADMN")
                                        {
                                            customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                                        }
                                        if (customerID != -1)
                                        {
                                            try
                                            {
                                                if (!string.IsNullOrEmpty(Convert.ToString(ddlUserList.SelectedValue)))
                                                {
                                                    if (chkList.Count > 0)
                                                    {
                                                        ComplianceExclude_Reassign.Statutory_DeleteComplianceAssignment(Convert.ToInt32(ddlUserList.SelectedValue), chkList, customerID, AuthenticationHelper.UserID);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Ressign Performer /Reviewer
                                        long pID = -1;
                                        long rID = -1;
                                        long aID = -1;
                                        if (!String.IsNullOrEmpty(ddlNewUsers.SelectedValue))
                                        {
                                            if (ddlNewUsers.SelectedValue != "-1")
                                            {
                                                pID = Convert.ToInt32(ddlNewUsers.SelectedValue);
                                            }
                                        }
                                        if (!String.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                                        {
                                            if (ddlNewReviewerUsers.SelectedValue != "-1")
                                            {
                                                rID = Convert.ToInt32(ddlNewReviewerUsers.SelectedValue);
                                            }
                                        }
                                        if (!String.IsNullOrEmpty(ddlNewApproverUsers.SelectedValue))
                                        {
                                            if (ddlNewApproverUsers.SelectedValue != "-1")
                                            {
                                                aID = Convert.ToInt32(ddlNewApproverUsers.SelectedValue);
                                            }
                                        }
                                        if (pID == -1 && rID == -1 && aID == -1)
                                        {
                                            CustomModifyAsignment.IsValid = false;
                                            CustomModifyAsignment.ErrorMessage = "Please select at list one user for proceed.";
                                            return;
                                        }
                                        else
                                        {
                                            ComplianceExclude_Reassign.Statutory_ReplaceUserForComplianceAssignmentNew(pID, rID, aID, AuthenticationHelper.UserID, chkList);                                                                                       
                                        }
                                        #endregion
                                    }
                                    int userID = Convert.ToInt32(ddlUserList.SelectedValue);
                                    if (rbtSelectionType.SelectedValue == "0")
                                    {
                                        BindComplianceList();
                                        BindComplianceInstances(userID);
                                    }
                                    else
                                    {
                                        BindInternalComplianceList();
                                        BindInternalComplianceInstances(userID);
                                    }
                                    ViewState["AssignedCompliancesID"] = null;
                                }
                                else
                                {
                                    ViewState["AssignedCompliancesID"] = null;
                                    CustomModifyAsignment.IsValid = false;
                                    CustomModifyAsignment.ErrorMessage = "Please select at list one compliance for proceed.";
                                }
                                #endregion
                            }
                            else//internal
                            {
                                #region internal
                                InternalCheckBoxValueSaved();
                                List<Tuple<long, string>> chkIList = (List<Tuple<long, string>>)ViewState["AssignedInternalCompliancesID"];
                                if (chkIList != null)
                                {
                                    string action = Convert.ToString(rbtModifyAction.SelectedValue);
                                    if (action.Equals("ApproverDelete"))
                                    {
                                        #region Delete Approver Internal Assignment
                                        int customerID = -1;
                                        if (AuthenticationHelper.Role == "CADMN")
                                        {
                                            customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                                        }
                                        if (customerID != -1)
                                        {
                                            try
                                            {
                                                if (chkIList.Count > 0)
                                                {
                                                    ComplianceExclude_Reassign.InternalApprover_DeleteComplianceAssignment(chkIList, customerID, AuthenticationHelper.UserID);                                                   
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            }
                                        }
                                        #endregion
                                    }
                                    else if (action.Equals("Delete"))
                                    {
                                        #region Delete Assignment
                                        int customerID = -1;
                                        if (AuthenticationHelper.Role == "CADMN")
                                        {
                                            customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                                        }
                                        if (customerID != -1)
                                        {
                                            try
                                            {
                                                if (!string.IsNullOrEmpty(Convert.ToString(ddlUserList.SelectedValue)))
                                                {
                                                    if (chkIList.Count > 0)
                                                    {
                                                        ComplianceExclude_Reassign.Internal_DeleteComplianceAssignment(Convert.ToInt32(ddlUserList.SelectedValue), chkIList, customerID, AuthenticationHelper.UserID);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            }


                                            //var oldUser = UserManagement.GetByID(Convert.ToInt32(ViewState["UserID"]));
                                            //if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                            //{
                                            //    if (chkIList.Count > 0)
                                            //    {
                                            //        var instancsids = chkIList.Select(a => a.Item1).ToList();

                                            //        var compilancelist = (from ICI in entities.InternalComplianceInstances
                                            //                              join IC in entities.InternalCompliances on ICI.InternalComplianceID equals IC.ID                                                                          
                                            //                              join CB in entities.CustomerBranches on ICI.CustomerBranchID equals CB.ID
                                            //                              where instancsids.Contains(ICI.ID)
                                            //                              select new
                                            //                              {
                                            //                                  Branch = CB.Name,
                                            //                                  ComplianceID = IC.ID,                                                                              
                                            //                                  ShortDescription = IC.IShortDescription
                                            //                              }).ToList();


                                            //        string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                            //        var EscalationEmail = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ExcludeInternalComplinces
                                            //                                   .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                            //                                   .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                            //                                   .Replace("@From", ReplyEmailAddressName);
                                            //        StringBuilder details = new StringBuilder();
                                            //        var remindersummary = compilancelist.Distinct().ToList();

                                            //        if (remindersummary.Count > 0)
                                            //        {
                                            //            foreach (var row in remindersummary)
                                            //            {
                                            //                details.AppendLine(com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ExcludeInternalTableRows
                                            //                    .Replace("@Branch", row.Branch)
                                            //                    .Replace("@ComplianceID", Convert.ToString(row.ComplianceID))                                                                
                                            //                    .Replace("@Compliance", row.ShortDescription));

                                            //            }
                                            //            string message = EscalationEmail.Replace("@User", "User").Replace("@Details", details.ToString());
                                            //            // new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for excluding compliances.", message); }).Start();                                                       
                                            //            new Thread(() => { EmailManager.SendMailRahul(ConfigurationManager.AppSettings["SenderEmailAddress"], c, null, null, "AVACOM Notification for excluding compliances.", message); }).Start();
                                            //        }//remindersummary.Count
                                            //    }//chkList.Count end                                               
                                            //}
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Ressign Performer /Reviewer
                                        long pID = -1;
                                        long rID = -1;
                                        long aID = -1;
                                        if (!String.IsNullOrEmpty(ddlNewUsers.SelectedValue))
                                        {
                                            if (ddlNewUsers.SelectedValue != "-1")
                                            {
                                                pID = Convert.ToInt32(ddlNewUsers.SelectedValue);
                                            }
                                        }
                                        if (!String.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                                        {
                                            if (ddlNewReviewerUsers.SelectedValue != "-1")
                                            {
                                                rID = Convert.ToInt32(ddlNewReviewerUsers.SelectedValue);
                                            }
                                        }
                                        if (!String.IsNullOrEmpty(ddlNewApproverUsers.SelectedValue))
                                        {
                                            if (ddlNewApproverUsers.SelectedValue != "-1")
                                            {
                                                aID = Convert.ToInt32(ddlNewApproverUsers.SelectedValue);
                                            }
                                        }
                                        if (pID == -1 && rID == -1 && aID == -1)
                                        {
                                            CustomModifyAsignment.IsValid = false;
                                            CustomModifyAsignment.ErrorMessage = "Please select at list one user for proceed.";
                                            return;
                                        }
                                        else
                                        {
                                            ComplianceExclude_Reassign.Internal_ReplaceUserForComplianceAssignmentNew(pID, rID, aID, AuthenticationHelper.UserID, chkIList);                                                                                     
                                        }
                                        #endregion
                                    }
                                    int userID = Convert.ToInt32(ddlUserList.SelectedValue);
                                    if (rbtSelectionType.SelectedValue == "0")
                                    {
                                        BindComplianceList();
                                        BindComplianceInstances(userID);
                                    }
                                    else
                                    {
                                        BindInternalComplianceList();
                                        BindInternalComplianceInstances(userID);
                                    }
                                    ViewState["AssignedInternalCompliancesID"] = null;
                                }
                                else
                                {
                                    ViewState["AssignedInternalCompliancesID"] = null;
                                    CustomModifyAsignment.IsValid = false;
                                    CustomModifyAsignment.ErrorMessage = "Please select at list one compliance for proceed.";
                                }
                                #endregion
                            }
                        }//Task
                        else
                        {
                            #region Task
                            Task_CheckBoxValueSaved();
                            List<Tuple<long, int>> chkList = (List<Tuple<long, int>>)ViewState["AssignedTaskID"];
                            if (chkList != null)
                            {
                                string action = Convert.ToString(rbtModifyAction.SelectedValue);
                                if (action.Equals("ApproverDelete"))
                                {
                                    #region Delete Approver Internal Assignment
                                    int customerID = -1;
                                    if (AuthenticationHelper.Role == "CADMN")
                                    {
                                        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                                    }
                                    if (customerID != -1)
                                    {
                                        try
                                        {
                                            if (chkList.Count > 0)
                                            {
                                                ComplianceExclude_Reassign.TaskApprover_DeleteComplianceAssignment(chkList, customerID, AuthenticationHelper.UserID);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }
                                    }
                                    #endregion
                                }
                                else if (action.Equals("Delete"))
                                {
                                    #region Delete Assignment
                                    int customerID = -1;
                                    if (AuthenticationHelper.Role == "CADMN")
                                    {
                                        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                                    }
                                    if (customerID != -1)
                                    {
                                        try
                                        {
                                            if (!string.IsNullOrEmpty(Convert.ToString(ddlUserList.SelectedValue)))
                                            {
                                                if (chkList.Count > 0)
                                                {
                                                    bool taskresult = ComplianceExclude_Reassign.DeleteTaskAssignmentTask(Convert.ToInt32(ddlUserList.SelectedValue), chkList, customerID, AuthenticationHelper.UserID);
                                                    if (taskresult)
                                                    {
                                                        int userID = Convert.ToInt32(ddlUserList.SelectedValue);
                                                        if (rbtSelectionType.SelectedValue == "0")
                                                        {
                                                            BindComplianceList();
                                                            BindComplianceInstances(userID);
                                                        }
                                                        else
                                                        {
                                                            BindInternalComplianceList();
                                                            BindInternalComplianceInstances(userID);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ViewState["AssignedTaskID"] = null;
                                                        CustomModifyAsignment.IsValid = false;
                                                        CustomModifyAsignment.ErrorMessage = "Subtask is exist.";
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Ressign Performer /Reviewer
                                    long pID = -1;
                                    long rID = -1;
                                    long aID = -1;
                                    if (!String.IsNullOrEmpty(ddlNewUsers.SelectedValue))
                                    {
                                        if (ddlNewUsers.SelectedValue != "-1")
                                        {
                                            pID = Convert.ToInt32(ddlNewUsers.SelectedValue);
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(ddlNewReviewerUsers.SelectedValue))
                                    {
                                        if (ddlNewReviewerUsers.SelectedValue != "-1")
                                        {
                                            rID = Convert.ToInt32(ddlNewReviewerUsers.SelectedValue);
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(ddlNewApproverUsers.SelectedValue))
                                    {
                                        if (ddlNewApproverUsers.SelectedValue != "-1")
                                        {
                                            aID = Convert.ToInt32(ddlNewApproverUsers.SelectedValue);
                                        }
                                    }
                                    if (pID == -1 && rID == -1 && aID == -1)
                                    {
                                        CustomModifyAsignment.IsValid = false;
                                        CustomModifyAsignment.ErrorMessage = "Please select at list one user for proceed.";
                                        return;
                                    }
                                    else
                                    {
                                        ComplianceExclude_Reassign.ReassignTaskNew(pID, rID, aID, AuthenticationHelper.UserID, chkList);
                                    }
                                    int userID = Convert.ToInt32(ddlUserList.SelectedValue);
                                    if (rbtSelectionType.SelectedValue == "0")
                                    {
                                        BindComplianceList();
                                        BindComplianceInstances(userID);
                                    }
                                    else
                                    {
                                        BindInternalComplianceList();
                                        BindInternalComplianceInstances(userID);
                                    }
                                    #endregion
                                }
                                if (rbtSelectionType.SelectedValue == "0")
                                {
                                    BindComplianceList();
                                    BindComplianceInstances(Convert.ToInt32(ddlUserList.SelectedValue));
                                }
                                else
                                {
                                    BindInternalComplianceList();
                                    BindInternalComplianceInstances(Convert.ToInt32(ddlUserList.SelectedValue));
                                }
                                ViewState["AssignedTaskID"] = null;
                            }
                            else
                            {
                                ViewState["AssignedTaskID"] = null;
                                CustomModifyAsignment.IsValid = false;
                                CustomModifyAsignment.ErrorMessage = "Please select at list one compliance for proceed.";
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        CustomModifyAsignment.IsValid = false;
                        CustomModifyAsignment.ErrorMessage = "Please select user and compliance for proceed.";
                        //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divModifyAssignment\").dialog('close')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomModifyAsignment.IsValid = false;
                CustomModifyAsignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(ddlUserList.SelectedValue);
            if (rbtSelectionType.SelectedValue == "0")
            {
                if (rbtSelectType.SelectedValue == "0")
                {
                    divEventBase.Visible = true;                    
                    BindComplianceInstances(userID);
                }
                else
                {
                    divEventBase.Visible = false;                    
                    BindComplianceInstances(userID);
                }
            }
            else
            {
                BindInternalComplianceInstances(userID);
            }
        }
    }
}