﻿<%@ Page Title="Check List" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="InternalCheckList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.InternalCheckList" EnableEventValidation="false" %>

<%@ Register Src="~/Controls/ComplianceStatusTransactionInternalChkList.ascx" TagName="ComplianceStatusTransactionInternalChkList" TagPrefix="vit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function fComplianceOverviewInternal(obj) {

            OpenOverViewpupInternal($(obj).attr('scheduledonid'), $(obj).attr('instanceid'));
        }

        function ViewInternalDocument() {
            $('#divViewInternalDocument').modal('show');
        };

        function fopendocfileReviewInternal(file) {
            $('#divViewInternalDocument').modal('show');
            $('#ContentPlaceHolder1_docViewerInternal').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        function ShowInternalDownloadDocument() {
            $('#divInternalDownloadDocument').modal('show');
            return true;
        };

        function hidediv() {

            var div = document.getElementById('AdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }

        function openModal() {
            if (Displays() == true) {
                $('#ComplaincePerformer').modal('show');
            }
            return true;
        }


        function initializeDatePicker11(date2) {
            var startDate = new Date();
            $('#<%= txtStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1
            });

            if (date2 != null) {
                $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date2);
            }
        }

        function initializeDatePicker12(date1) {
            var startDate = new Date();
            $('#<%= txtEndDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1
            });

            if (date1 != null) {
                $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date1);
            }
        }

        function myFunction() {
            $('#ContentPlaceHolder1_divFilterLocationPerformer').show();
        }

    </script>

    <style type="text/css">
        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .clspenaltysave {
            font-weight: bold;
            margin-left: 15px;
        }

        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>

    <div onselectstart="return false;" style="-moz-user-select: none;">
        <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
            <ContentTemplate>
                <div class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12 ">
                            <section class="panel">
                                <header class="panel-heading tab-bg-primary ">
                                          <ul id="rblRole1" class="nav nav-tabs">
                                                   <li class="active">                                                     
                                                        <asp:LinkButton ID="liPerformer"  runat="server">Performer</asp:LinkButton>
                                                    </li>
                                        </ul>
                                </header>
                                       <div class="clearfix"></div>
                                      <div class="clearfix"></div>
                                <div class="panel-body">
                                     
                                    <div class="col-md-12 colpadding0">

                                        <div class="col-md-2 colpadding0 entrycount">
                                            <div class="col-md-3 colpadding0">
                                                <p style="color:#999; margin-top:5px"> Show </p>
                                            </div>
                                            <asp:DropDownList runat="server" ID="ddlpageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlpageSize_SelectedIndexChanged" class="form-control m-bot15" Style="width: 70px; float: left">
                                                <asp:ListItem Text="5" Selected="True"/>
                                                <asp:ListItem Text="10" />
                                                <asp:ListItem Text="20" />
                                                <asp:ListItem Text="50" />
                                            </asp:DropDownList>
                                            <div class="col-md-3 colpadding0">
                                            <p style="color:#999; margin-top:5px; margin-left:5px">Entries</p></div> 
                                        </div>                                        

                                        <div class="col-md-9 colpadding0" style="text-align: right; float: right">
                                            <div class="col-md-8 colpadding0">
                                                <asp:DropDownList runat="server" ID="ddlComplianceType" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" class="form-control m-bot15 search-select">
                                                    <%--<asp:ListItem Text="Statutory" Value="-1" />--%>
                                                    <asp:ListItem Text="Internal" Value="0" />
                                                </asp:DropDownList>

                                                <asp:DropDownList runat="server" ID="ddlRiskType" class="form-control m-bot15 search-select">
                                                    <asp:ListItem Text="Risk" Value="-1" />
                                                    <asp:ListItem Text="High" Value="0" />
                                                    <asp:ListItem Text="Medium" Value="1" />
                                                    <asp:ListItem Text="Low" Value="2" />
                                                    
                                                </asp:DropDownList>

                                                <asp:DropDownList runat="server" ID="ddlStatus" class="form-control m-bot15 search-select"  Visible ="false">
                                                </asp:DropDownList>

                                                  <div style="float: left; margin-right: 2%;" id="divloc">
                                                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocationPerformer" onfocus="myFunction()" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 180px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                                        CssClass="txtbox" />
                                                    <div style="margin-left: 1px; position: absolute; z-index: 10;" id="divFilterLocationPerformer" runat="server">
                                                        <asp:TreeView runat="server" ID="tvFilterLocationPerformer" SelectedNodeStyle-Font-Bold="true" Width="325px" NodeStyle-ForeColor="#8e8e93"
                                                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                            OnSelectedNodeChanged="tvFilterLocationPerformer_SelectedNodeChanged" >
                                                        </asp:TreeView>
                                                    </div>
                                                </div>

                                             <%--   <asp:DropDownList runat="server" ID="ddlLocation" class="form-control m-bot15 select_location" Style="width: 200px;">
                                                </asp:DropDownList>--%>
                                            </div>
                                            <div class="col-md-4 colpadding0">
                                                <div class="col-md-6 colpadding0">                                                    
                                                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" OnClick="btnSearch_Click" runat="server" Text="Apply" />
                                                </div>
                                                <div class="col-md-6">
                                                   <asp:Label ID="lblRole" runat="server" Text="" Visible="false"></asp:Label>
                                                   <%-- <a id="btnCheck_List" runat="server" class="btn btn-search" href="../Compliances/Check_List_Reports_Performer.aspx" style="margin-top: -40px; position: absolute; width: 134px;">Export&nbsp;to&nbsp;Excel</a>                                                    --%>
<%--                                                   <asp:Button id="btnCheck_List" runat="server" class="btn btn-search" href="../Compliances/Check_List_Reports_Performer.aspx?role=" + role1 +" style="margin-top: -40px; position: absolute; width: 134px;" Text="Export to Excel"></asp:Button> --%>
                                                      <asp:Button ID="btnCheck_List" CausesValidation="false" class="btn btn-search" OnClick="btnCheck_List_Click" runat="server" Text="Checklist Report" style="margin-top: -40px; position: absolute; width: 134px;" />
                                                    <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch" title="Search">Advanced Search</a>
                                                </div>
                                            </div>               
                                        </div>

                                        </div>
                                        <!--advance search starts-->
                                   
                                    <div class="col-md-12 AdvanceSearchScrum">
                                        <div id="divAdvSearch" runat="server" visible="false">
                                        <p>
                                            <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label></p>
                                        <p>
                                         <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Advanced Search</asp:LinkButton>
                                        </p>
                                       </div>

                                         <div runat="server" id="DivRecordsScrum" style="float: right;">               
                                        <p>
                                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>

                                        </div>

                                    <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" style="width: 1000px">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <p></p>
                                </div>
                                <div class="modal-body" style="margin-left:50px">
                                    <h2 style="text-align: center">Advanced Search</h2>
                                    <br />
                                    <div class="col-md-12 colpadding0">
                                        <div class="table-advanceSearch-selectOpt">
                                            <asp:DropDownList runat="server" ID="ddlType" class="form-control m-bot15">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="table-advanceSearch-selectOpt">
                                            <asp:DropDownList runat="server" ID="ddlCategory" class="form-control m-bot15">
                                            </asp:DropDownList>
                                        </div>
                                        
                                        <asp:Panel ID="Panel1" runat="server">
                                                                <div id="Div3" runat="server" class="table-advanceSearch-selectOpt">
                                                                 <asp:TextBox runat="server" Height="35px" Width="200px" style="padding-left:7px; border-radius: 5px;" placeholder="From Date" class="form-group form-control" ID="txtStartDate" CssClass="StartDate"/>
                                                              </div>  </asp:Panel>

                                                            <asp:Panel ID="Panel2" runat="server">
                                                                <div id="Div4" runat="server" class="table-advanceSearch-selectOpt">
                                                                <asp:TextBox runat="server" Height="35px" Width="200px" style="padding-left:7px; border-radius: 5px;" placeholder="To Date" class="form-group form-control" ID="txtEndDate" CssClass="StartDate"/>
                                                               </div> </asp:Panel>
                                         <div class="clearfix"></div>
                                        <div class="table-advanceSearch-buttons">
                                            <table id="tblSearch">
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnAdvSearch" Text="Search" class="btn btn-search" OnClick="btnAdvSearch_Click" runat="server" OnClientClick="return hidediv();" />
                                                    </td>
                                                    <td>
                                                        <button id="btnAdvClose" type="button" class="btn btn-search" data-dismiss="modal">Close</button>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                        <!--advance search ends-->
                                    </div>
                        </div>
                        <div class="tab-content ">
                            <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" AllowSorting="true"
                                GridLines="None" CssClass="table" AllowPaging="True" PageSize="5" OnSorting="grdComplianceTransactions_Sorting"
                                DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging"
                                OnRowDataBound="grdComplianceTransactions_RowDataBound" OnRowCommand="grdComplianceTransactions_RowCommand"  OnRowCreated="grdComplianceTransactions_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                                <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("IShortDescription") %>' ToolTip='<%# Eval("IShortDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lbllocation" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Risk Category" Visible="false">
                                        <ItemTemplate>
                                            <asp:Image runat="server" ID="imtemplat" />
                                            <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("ComplianceID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblInstanceID" runat="server" Text='<%# Eval("InternalComplianceInstanceID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblScheduleOnID" runat="server" Text='<%# Eval("ScheduledOnID") %>' Visible="false"></asp:Label>
                                            <%--   <asp:Label ID="lblCheckListTypeID" runat="server" Text='<%# Eval("CheckListTypeID") %>' Visible="false"></asp:Label>--%>
                                            <asp:Label ID="lblUploadedFileName" runat="server" Text='<%# Eval("UploadedFileName") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblUploadedFilePath" runat="server" Text='<%# Eval("UploadedFilePath") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblRoleID" runat="server" Text='<%# Eval("RoleID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Month">
                                        <ItemTemplate>
                                            <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Scheduled On">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScheduledOn" runat="server" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblChecklistStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Details">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Button runat="server" OnClientClick="return openModal()" ID="btnChangeStatus" OnClick="btnChangeStatus_Click" CssClass="btnss"
                                                    CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("InternalComplianceInstanceID") + "," + Eval("ScheduledOn")  %>' />
                                                <asp:UpdatePanel ID="upDownloadInternal" runat="server">
                                                    <ContentTemplate>
                                                        <asp:ImageButton ID="lblDownLoadfileInternal" runat="server" ImageUrl="~/img/icon-download.png" CommandName="Download"
                                                            CommandArgument='<%# Eval("ScheduledOnID") + " , " + Eval("ComplianceTransactionID") +" , "+Eval("UploadedFileID") %>' ToolTip="Download"></asp:ImageButton>
                                                        <asp:ImageButton ID="lblViewfileInternal" runat="server" ImageUrl="~/Images/View-doc.png" CommandName="View"
                                                            CommandArgument='<%# Eval("ScheduledOnID") + " , " + Eval("ComplianceTransactionID")+" , "+Eval("UploadedFileID")  %>' ToolTip="View"></asp:ImageButton>
                                                        <asp:ImageButton ID="lblOverView2" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("ScheduledOnID")%>' instanceId='<%#Eval("InternalComplianceInstanceID")  %>'
                                                            OnClientClick='fComplianceOverviewInternal(this)' ToolTip="Click to OverView"></asp:ImageButton>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lblDownLoadfileInternal" />
                                                        <asp:PostBackTrigger ControlID="lblViewfileInternal" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField><%--Visible='<%# visiblebutton(Convert.ToInt32(Eval("RoleID"))) %>'--%>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkCompletedSelectAll" Text="Completed" runat="server" AutoPostBack="true" OnCheckedChanged="chkCompletedSelectAll_CheckedChanged" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkCompleted" runat="server" AutoPostBack="true" OnCheckedChanged="chkCompleted_CheckedChanged" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkNotCompletedSelectAll" Text="Not Completed" runat="server" AutoPostBack="true" OnCheckedChanged="chkNotCompletedSelectAll_CheckedChanged" />
                                            onclick="javascript:SelectheaderCheckboxes(this)"
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkNotCompleted" runat="server" AutoPostBack="true" OnCheckedChanged="chkNotCompleted_CheckedChanged" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" />
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <PagerTemplate>
                                    <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Records Found
                                </EmptyDataTemplate>
                            </asp:GridView>

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                </div>
                                <div class="col-md-6 colpadding0">
                                    <div class="table-paging" style="margin-bottom: 20px">
                                        <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                        <div class="table-paging-text">
                                            <p>
                                                <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                        <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: center; margin-left: 472px;">
                            <asp:Button ID="btnSave" runat="server" class="btn btn-search" Style="margin-bottom: 15px;" Text="Submit" OnClick="btnSave_Click" />
                        </div>
                        </section>
                    </div>
                </div>
                </div>
            <tr id="trErrorMessage" runat="server" visible="true">
                <td colspan="3" style="background-color: #e9e1e1;">
                    <asp:Label ID="GridViewPagingError" runat="server" Font-Names="Verdana" Font-Size="9pt"
                        ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </td>
            </tr>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hdnCheckPageNo" runat="server" />

    <div>
        <div class="modal fade" id="ComplaincePerformer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 85%">

                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 34px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    </div>
                    <div class="modal-body" style="background-color: #f7f7f7; width: 100%;">
                        <vit:ComplianceStatusTransactionInternalChkList runat="server" ID="udcComplianceStatusTransactionInternalChkList" />
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div>
        <div class="modal fade" id="divInternalDownloadDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 500px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body">
                        <table width="100%" style="text-align: left; margin-left: 25%;">
                            <thead>
                                <tr>
                                    <td valign="top">
                                        <asp:Repeater ID="rptIComplianceVersion" runat="server" OnItemCommand="rptIComplianceVersion_ItemCommand"
                                            OnItemDataBound="rptIComplianceVersion_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="tblIComplianceDocumnets">
                                                    <thead>
                                                        <th>Versions</th>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") %>' ID="lblIDocumentVersion"
                                                            runat="server" Text='<%# Eval("Version")%>'></asp:LinkButton></td>
                                                    <td>
                                                        <asp:LinkButton class="btn btn-search" CommandName="Download" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") %>' OnClientClick='javascript:enableControls()'
                                                            ID="btnIComplinceVersionDoc" runat="server" Text="Download" Style="margin-top: 10%;">
                                                        </asp:LinkButton></td>

                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                    <td valign="top">
                                        <asp:Repeater ID="rptIComplianceDocumnets" runat="server" OnItemCommand="rptIComplianceVersion_ItemCommand"
                                            OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="tblIComplianceDocumnets">
                                                    <thead>
                                                        <th>Compliance Related Documents</th>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton
                                                            CommandArgument='<%# Eval("FileID")%>'
                                                            OnClientClick='javascript:enableControls()'
                                                            ID="btnIComplianceDocumnets" runat="server"
                                                            Text='<%# Eval("FileName") %>'>
                                                        </asp:LinkButton></td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                            OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="tblWorkingFiles">
                                                    <thead>
                                                        <th>Compliance Working Files</th>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton
                                                            CommandArgument='<%# Eval("FileID")%>'
                                                            ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                        </asp:LinkButton></td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div>
        <div class="modal fade" id="divViewInternalDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: left; width: 10%">
                                <table width="100%" style="text-align: left; margin-left: 25%;">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <asp:Repeater ID="rptIComplianceVersionView" runat="server" OnItemCommand="rptIComplianceVersionView_ItemCommand"
                                                    OnItemDataBound="rptIComplianceVersionView_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="tblComplianceDocumnets">
                                                            <thead>
                                                                <th>Versions</th>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version")+ ","+ Eval("FileID") %>' ID="lblIDocumentVersionView"
                                                                    runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version")%>'></asp:LinkButton></td>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: left; width: 90%">
                                <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdatleMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label runat="server" ID="lblMessageInternal" Style="color: red;"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 530px; width: 100%;">
                                    <iframe src="about:blank" id="docViewerInternal" runat="server" width="100%" height="510px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1150px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body">

                        <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function OpenOverViewpupInternal(scheduledonid, instanceid) {
            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '1050px');
            $('#OverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '1100px');
            $('#OverViews').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
        }

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('My Workspace/InternalCheckList');
        });
    </script>
</asp:Content>
