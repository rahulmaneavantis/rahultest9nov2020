﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="Assign_Compliance_Activate_Internal.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.Assign_Compliance_Activate_Internal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });

            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });
        }
        function fCheckTree(obj) {
            var id = $(obj).attr('data-attr');
            var elm = $("#" + id);
            $(elm).trigger('click');
        }
        function FnSearch() {

            var tree = document.getElementById('BodyContent_tvFilterLocation');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tbxFilterLocation').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');
                            // var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  name="' + getId + 'CheckBox" id="' + getId + 'CheckBox"><a id="' + getId + '" href="' + $(anch).attr('href') + '" onclick="' + $(anch).attr('onclick') + '" >' + anch.html() + '</a></br>';
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement').show();
            } else {
                $('#bindelement').html('');
                $('#bindelement').hide();
                $(tree).show();
            }

        }

        function initializeDatePicker(date) {

            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1
            });

        }
         function Confirm() {
            var a=document.getElementById("BodyContent_tbxStartDate").value;
            if ( a!= "") {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                var Date = document.getElementById("<%=tbxStartDate.ClientID %>").value;
                confirm_value.value = "";
                confirm_value
                if (confirm("Start date is " + Date + ", to continue saving click OK!!")) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert("Start date should not be blank..!");
            }
        };
      function OnTreeClick(evt) {            
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if (isChkBoxClick) {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
            {
                if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                {
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);
        }
        }
             
    function CheckUncheckChildren(childContainer, check) {
        var childChkBoxes = childContainer.getElementsByTagName("input");
        var childChkBoxCount = childChkBoxes.length;
        for (var i = 0; i < childChkBoxCount; i++) {
            childChkBoxes[i].checked = check;
        }
    }

    function CheckUncheckParents(srcChild, check) {
        var parentDiv = GetParentByTagName("div", srcChild);
        var parentNodeTable = parentDiv.previousSibling;

        if (parentNodeTable) {
            var checkUncheckSwitch;

            if (check) //checkbox checked
            {
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                if (isAllSiblingsChecked)
                    checkUncheckSwitch = true;
                else
                    return; //do not need to check parent if any(one or more) child not checked
            }
            else //checkbox unchecked
            {
                checkUncheckSwitch = false;
            }

            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
            if (inpElemsInParentTable.length > 0) {
                var parentNodeChkBox = inpElemsInParentTable[0];
                parentNodeChkBox.checked = checkUncheckSwitch;
                //do the same recursively
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
            }
        }
    }

    function AreAllSiblingsChecked(chkBox) {
        var parentDiv = GetParentByTagName("div", chkBox);
        var childCount = parentDiv.childNodes.length;
        for (var i = 0; i < childCount; i++) {
            if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
            {
                if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                    var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                    //if any of sibling nodes are not checked, return false
                    if (!prevChkBox.checked) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //utility function to get the container of an element by tagname
    function GetParentByTagName(parentTagName, childElementObj) {
        var parent = childElementObj.parentNode;
        while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
            parent = parent.parentNode;
        }
        return parent;
    }
    </script>

    <style type="text/css">
        .td1 {
            width: 15%;
        }

        .td2 {
            width: 25%;
        }

        .td3 {
            width: 15%;
        }

        .td4 {
            width: 25%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
     <div style="float: right; margin-top: 10px; margin-right: 70px; margin-top: 5px;">
        <asp:LinkButton runat="server" ID="lbtnExportExcel" Style="margin-top: 15px; margin-top: -5px;" OnClick="lbtnExportExcel_Click"><img src="../Images/excel.png" alt="Export to Excel"
        title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
    </div>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>

            <center>
                <table runat="server" width="80%">
                    <tr>
                        <td class="td3" colspan="2" align="left">
                            <asp:CompareValidator ControlToValidate="tbxFilterLocation" ErrorMessage="Please select Location."
                                runat="server" ValueToCompare="< Select Location >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </td>
                        <td class="td4" align="left"></td>
                    </tr>
                    <tr>

                        <td class="td1" align="right">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Select Location:
                            </label>
                        </td>
                        <td class="td2" align="left">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 290px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="300px"
                                    Style="overflow: auto" ShowLines="true" ShowCheckBoxes="All" onclick="OnTreeClick(event)" > <%--OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged"--%>
                                </asp:TreeView>
                                <div id="bindelement" style="background: white;height: 292px;display:none; width: 390px;border: 1px solid;overflow: auto;"></div>
    
                                <asp:Button ID="btnlocation" runat="server"  OnClick="btnlocation_Click" Text="select"/> 
                                    <asp:Button ID="btnClear1" Visible="true" runat="server" OnClick="btnClear1_Click" Text="Clear"  /> 

                            </div>
                        </td>
                        <td class="td3" align="right">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Start Date:
                            </label>
                        </td>
                        <td class="td4" align="left">
                            <asp:TextBox runat="server" ID="tbxStartDate" Style="height: 16px; width: 290px;" AutoPostBack="true" OnTextChanged="tbxStartDate_TextChanged" />
                             <asp:RequiredFieldValidator ID="reqfldfordate" runat="server" ControlToValidate="tbxStartDate" ValidationGroup="ComplianceInstanceValidationGroup"></asp:RequiredFieldValidator>
                         </td>

                    </tr>
                    <tr>
                        <td class="td1" align="right">
                            <asp:CheckBox ID="chkToActiveInstance" runat="server" AutoPostBack="true" OnCheckedChanged="chkToActiveInstance_CheckedChanged" Checked="true" />

                        </td>
                        <td class="td2" align="left">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                To Activate
                            </label>
                        </td>
                        <td class="td3" align="right">
                            <asp:CheckBox ID="chkToDeActiveInstance" runat="server" Visible="false" AutoPostBack="true" OnCheckedChanged="chkToDeActiveInstance_CheckedChanged" />
                        </td>
                        <td class="td4" align="left">
                           <%-- <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                To DeActivate
                            </label>--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" runat="server">
                            <asp:Panel ID="Panel1" Width="100%" Height="430px" ScrollBars="Auto" runat="server">
                                <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdComplianceRoleMatrix_RowCreated"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdComplianceRoleMatrix_Sorting"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50" OnRowDataBound="grdComplianceRoleMatrix__RowDataBound"
                                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Departmentid"  Visible="false">
                                            <ItemTemplate>                                                                                                
                                                    <asp:Label ID="lblDepartmentID" runat="server" Text='<%# Eval("DepartmentID")%>'></asp:Label>                                                
                                                    
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ComplianceID" SortExpression="Sections">
                                            <ItemTemplate>
                                                <%--width: 150px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("ComplianceID")%>'></asp:Label>
                                                     <asp:Label ID="lblCustomerBranchId" runat="server" Text='<%# Eval("CustomerBranchID")%>' style="display:none;"></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" SortExpression="Description">
                                            <ItemTemplate>

                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 530px;">
                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Users" SortExpression="Users">
                                            <ItemTemplate>
                                                <%--width: 550px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblUsers" runat="server" Text='<%# Eval("Users") %>'></asp:Label>
                                                    <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("userID") %>' Visible="false"></asp:Label>

                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                            <ItemTemplate>
                                                <%-- width: 550px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Label">
                                            <ItemTemplate>                                                                                                                                                                                                    
                                                    <asp:Label ID="lblSequenceID" runat="server" Text='<%# Eval("SequenceID")%>'></asp:Label>                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:UpdatePanel ID="UpStartDate" runat="server">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtStartDate" CssClass="StartDate" runat="server" ReadOnly="true"></asp:TextBox>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="txtStartDate" />
                                                    </Triggers>
                                                </asp:UpdatePanel>

                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>

                                                <asp:CheckBox ID="chkActivateSelectAll" Text="Activate" runat="server" AutoPostBack="true" OnCheckedChanged="chkActivateSelectAll_CheckedChanged" />

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkActivate" runat="server" AutoPostBack="true" OnCheckedChanged="chkActivate_CheckedChanged" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>

                                <asp:GridView runat="server" ID="grdToDeactivate" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdToDeactivate_RowCreated"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdToDeactivate_Sorting"
                                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50" OnRowDataBound="grdToDeactivate__RowDataBound"
                                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdToDeactivate_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ComplianceID" SortExpression="Sections">
                                            <ItemTemplate>
                                                <%--width: 150px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("InternalComplianceID")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" SortExpression="Description">
                                            <ItemTemplate>

                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Users" SortExpression="Users">
                                            <ItemTemplate>
                                                <%--width: 550px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblUsers" runat="server" Text='<%# Eval("Users") %>'></asp:Label>
                                                    <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("userID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblComplianceInstanceID" runat="server" Text='<%# Eval("InternalComplianceInstanceID") %>' Visible="false"></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                            <ItemTemplate>
                                                <%-- width: 550px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CreatedOn" SortExpression="CreatedOn">
                                            <ItemTemplate>
                                                <%-- width: 550px;--%>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <%-- <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn") %>'  ></asp:Label>--%>
                                                    <%# Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MMM-yyyy") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("StartDate") %>'  ></asp:Label>--%>
                                                <%# Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>

                                                <asp:CheckBox ID="chkDeActivateSelectAll" Text="Deactivate" runat="server" AutoPostBack="true" OnCheckedChanged="chkDeActivateSelectAll_CheckedChanged" />

                                            </HeaderTemplate>
                                            <ItemTemplate>

                                                <asp:CheckBox ID="chkDeActivate" runat="server" AutoPostBack="true" OnCheckedChanged="chkDeActivate_CheckedChanged" />


                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                            <asp:Button Text="Save" runat="server" ID="Button1" OnClick="btnSave_Click" OnClientClick="return Confirm();" CssClass="button"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                        </td>
                    </tr>
                </table>
            </center>
        </ContentTemplate>
           <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
