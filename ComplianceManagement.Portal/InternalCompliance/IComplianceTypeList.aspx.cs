﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.License;

namespace com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance
{
    public partial class IComplianceTypeList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["CustomerID"] = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                BindComplianceTypes();
            }
        }

        private void BindComplianceTypes()
        {
            try
            {
                if (ViewState["CustomerID"] != null)
                {
                    grdIComplianceType.DataSource = ComplianceTypeManagement.GetAllInternalCompliances(Convert.ToInt32(ViewState["CustomerID"]), tbxFilter.Text);
                    grdIComplianceType.DataBind();
                }
                upIComplianceTypeList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdIComplianceType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int typeID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_COMPLIANCE_TYPE"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["ComplianceTypeID"] = typeID;
                    var userParameter = ComplianceTypeManagement.GetByIDInternalCompliance(typeID);
                    if (userParameter.Is_License == true)
                    {
                        chkIsLicense.Checked = true;
                    }
                    else
                    {
                        chkIsLicense.Checked = false;
                    }
                    //
                    ViewState["CustomerID"] = userParameter.CustomerID;
                    tbxName.Text = userParameter.Name;
                    tbxDescription.Text = userParameter.Description;
                    tbxDescription.ToolTip = userParameter.Description;

                    upIComplianceType.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divIComplianceTypeDialog\").dialog('open')", true);

                }
                else if (e.CommandName.Equals("DELETE_COMPLIANCE_TYPE"))
                {
                    if (ComplianceTypeManagement.InternalComplianceTypeUsed(typeID))
                    {
                        ComplianceTypeManagement.DeleteInternalCompliance(typeID);
                        BindComplianceTypes();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('Compliance type is associated with one or more compliances, can not be deleted.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdIComplianceType_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdIComplianceType.PageIndex = e.NewPageIndex;
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddIComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                chkIsLicense.Checked = false;
                ViewState["Mode"] = 0;
                ViewState["CustomerID"] = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                tbxName.Text = string.Empty;
                tbxDescription.Text = string.Empty;

                upIComplianceType.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divIComplianceTypeDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdIComplianceType.PageIndex = 0;
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                InternalComplianceType type = new InternalComplianceType()
                {
                    Name = tbxName.Text,
                    Description = tbxDescription.Text,
                    CustomerID = Convert.ToInt32(ViewState["CustomerID"])
                };
                if (chkIsLicense.Checked==true)
                {
                    type.Is_License = true;                                                     
                }
                else
                {
                    type.Is_License = false;
                }

                Lic_tbl_InternalLicenseType_Master ltlm = new Lic_tbl_InternalLicenseType_Master()
                {
                    Name = tbxName.Text.Trim(),
                    IsDeleted = false,
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,
                    Is_Statutory_NoStatutory = "I",
                    CustomerID = Convert.ToInt32(ViewState["CustomerID"]),
                };
                if ((int)ViewState["Mode"] == 1)
                {
                    type.ID = Convert.ToInt32(ViewState["ComplianceTypeID"]);
                }

                if (!LicenseTypeMasterManagement.Exists(ltlm))
                {
                    LicenseTypeMasterManagement.CreateInternal(ltlm);
                }
                else
                {
                    LicenseTypeMasterManagement.Update(ltlm);
                }
                
                if (ComplianceTypeManagement.ExistsInternalCompliances(type, Convert.ToInt32(ViewState["CustomerID"])))
                {
                    cvDuplicateEntry.ErrorMessage = "Type name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    ComplianceTypeManagement.Create(type);
                }
                else if ((int)ViewState["Mode"] == 1)
                {

                    ComplianceTypeManagement.Update(type);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divIComplianceTypeDialog\").dialog('close')", true);
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdIComplianceType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceType = ComplianceTypeManagement.GetAllInternalCompliances(Convert.ToInt32(ViewState["CustomerID"]), tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    complianceType = complianceType.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceType = complianceType.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdIComplianceType.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdIComplianceType.Columns.IndexOf(field);
                    }
                }

                grdIComplianceType.DataSource = complianceType;
                grdIComplianceType.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdIComplianceType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }


    }
}