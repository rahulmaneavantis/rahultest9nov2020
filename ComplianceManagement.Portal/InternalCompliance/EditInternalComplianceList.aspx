﻿<%@ Page Title="Internal Compliances" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="EditInternalComplianceList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InternalCompliance.EditInternalComplianceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlComplinceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplinceCatagory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterFrequencies" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <%--  <td>
                      <asp:RadioButton id="rdFunctionBased" Text="Function Based" AutoPostBack="true" Width="110px" GroupName="ComplianceType" runat="server"
                       OnCheckedChanged="rdFunctionBased_CheckedChanged"/>
                    </td>
                    <td>
                      <asp:RadioButton id="rdChecklist" Text="Checklist" AutoPostBack="true" Width="100px" GroupName="ComplianceType" runat="server"
                      OnCheckedChanged="rdChecklist_CheckedChanged"/>
                    </td>--%>

                    <td style="width: 25%; padding-right: 20px;" align="right">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right" style="width: 70px">
                        <asp:LinkButton Text="Add New" runat="server" Visible="false" ID="btnAddCompliance" OnClick="btnAddCompliance_Click" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCompliances_RowCreated"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" Width="100%" OnSorting="grdCompliances_Sorting"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderText="Type Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="60px" SortExpression="IComplianceTypeName">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("IComplianceTypeName") %>' ToolTip='<%# Eval("IComplianceTypeName") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="60px" SortExpression="IComplianceCategoryName">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">
                                <asp:Label ID="Label11" runat="server" Text='<%# Eval("IComplianceCategoryName") %>' ToolTip='<%# Eval("IComplianceCategoryName") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="IShortDescription">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("IShortDescription") %>' ToolTip='<%# Eval("IShortDescription") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Upload Document" ItemStyle-Width="140px" SortExpression="IUploadDocument">
                        <ItemTemplate>
                            <%# Convert.ToBoolean(Eval("IUploadDocument")) ? "Yes" : "No"%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Risk" HeaderText="Risk Type" ItemStyle-Width="150px" SortExpression="Risk" />
                    <asp:BoundField DataField="Frequency" HeaderText="Frequency" ItemStyle-Width="180px" SortExpression="Frequency" />
                    <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Compliance" title="Edit Compliance" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this compliance?');"><img src="../Images/delete_icon.png" alt="Delete Compliance" title="Delete Compliance" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_SCHEDULE" CommandArgument='<%# Eval("ID") %>'
                                Visible='<%# ViewSchedule(Eval("Frequency"), Eval("IComplianceType"),Eval("ISubComplianceType"),Eval("CheckListTypeID")) %>'><img src="../Images/package_icon.png" alt="Display Schedule Information" title="Display Schedule Information" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divInternalComplianceDetailsDialog">
        <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Type Name</label>
                        <asp:DropDownList runat="server" ID="ddlIComplinceType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="cvIComplinceType" ErrorMessage="Please select internal compliance type name."
                            ControlToValidate="ddlIComplinceType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Category Name</label>
                        <asp:DropDownList runat="server" ID="ddlICategory" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="cvIComplinceCategory" ErrorMessage="Please select internal compliance category name."
                            ControlToValidate="ddlICategory" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Short Description</label>
                        <asp:TextBox runat="server" ID="txtShortDescription" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="rfvIShortDescription" ErrorMessage="Please enter short description."
                            ControlToValidate="txtShortDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                     <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Effective Date</label>
                        <asp:TextBox runat="server" ID="txtEffectiveDate" Style="height: 16px; width: 390px;" AutoPostBack="true" />
                     <%--   <asp:RequiredFieldValidator ID="rfvEffectiveDate" ErrorMessage="Please enter Effective Date."
                            ControlToValidate="txtEffectiveDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />--%>
                    </div>
                    <div style="margin-bottom: 7px" id="div2" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Occourrence</label>
                        <asp:RadioButtonList ID="rbtnlstCompOcc" runat="server" Enabled="false" RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="rbtnlstCompOcc_SelectedIndexChanged">
                            <asp:ListItem Text="One Time" Value="0" />
                            <asp:ListItem Text="Recurring" Value="1" Selected="True" />
                        </asp:RadioButtonList>
                    </div>

                    <div style="margin-bottom: 7px" id="divCompType" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Type</label>
                        <asp:DropDownList runat="server" ID="ddlComplianceType" Enabled="false" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged">
                            <asp:ListItem Text="Function based" Value="0" />
                            <asp:ListItem Text="Checklist" Value="1" />
                            <asp:ListItem Text="Time Based" Value="2" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="Please Select Compliance Type."
                            ControlToValidate="ddlComplianceType" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px" id="divChecklist" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Checklist Type</label>
                        <asp:DropDownList runat="server" ID="ddlChklstType" Enabled="false" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlChklstType_SelectedIndexChanged">
                            <asp:ListItem Text="One Time based Checklist" Value="0" />
                            <asp:ListItem Text="Function based Checklist" Value="1" />
                            <asp:ListItem Text="Time Based Checklist" Value="2" />
                            <%--  <asp:ListItem Text="Reference Checklist" Value="3" />--%>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="Please Select Compliance Type."
                            ControlToValidate="ddlComplianceType" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                     <div style="margin-bottom: 7px; clear: both" id="divOnetimeDate" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            One Time Date</label>
                        <asp:TextBox runat="server" ID="tbxOnetimeduedate" Style="height: 16px; width: 390px;" AutoPostBack="true" />
                        <asp:RequiredFieldValidator ID="rfvOnetimeduedate" ErrorMessage="Please enter One time Due Date."
                            ControlToValidate="tbxOnetimeduedate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" id="divTimebasedTypes" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Timely Based Type</label>
                        <asp:RadioButtonList ID="rbTimebasedTypes" runat="server" Enabled="false" RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="rbTimebasedTypes_SelectedIndexChanged">
                            <asp:ListItem Text="Fixed Gap" Value="0" Selected="True" />
                            <asp:ListItem Text="Periodically Based" Value="1" />
                        </asp:RadioButtonList>
                    </div>
            
                    <div style="margin-bottom: 7px;" id="divComplianceDueDays" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Due(Day)</label>
                        <asp:TextBox runat="server" ID="txtEventDueDate" MaxLength="4" Width="80px" />
                        <asp:RegularExpressionValidator ID="rgexEventDueDate" ControlToValidate="txtEventDueDate"
                            runat="server" ErrorMessage="Only Numbers allowed" Display="None" ValidationExpression="^-?[0-9]\d*(\.\d+)?$" ValidationGroup="ComplianceValidationGroup" />
                        <asp:RequiredFieldValidator ID="rfvEventDue" ErrorMessage="Please enter compliance after due."
                            ControlToValidate="txtEventDueDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" Enabled="false" />
                    </div>


                    <div runat="server" id="divFunctionBased">
                        <div id="divNonEvents" runat="server">
                            <div style="margin-bottom: 7px" id="divFrequency" runat="server">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Frequency</label>
                                <asp:DropDownList runat="server" ID="ddlFrequency" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged"  Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox">
                                </asp:DropDownList>
                                <asp:CompareValidator ErrorMessage="Please select frequency." ControlToValidate="ddlFrequency" ID="cvfrequency"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                    Display="None" />
                            </div>
                            <div style="margin-bottom: 7px" id="vivDueDate" runat="server">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Due Date</label>
                                <asp:DropDownList runat="server" ID="ddlDueDate" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="vaDueDate" ErrorMessage="Please Select Due Date."
                                    ControlToValidate="ddlDueDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                                    Display="None" Enabled="false" />
                            </div>
                            <div style="margin-bottom: 7px" id="vivWeekDueDays" runat="server" visible="false">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                   Week Due Day</label>
                                <asp:DropDownList runat="server" ID="ddlWeekDueDay" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox">
                                </asp:DropDownList>
                                 <asp:CompareValidator ErrorMessage="Please Select Week Due Day." ControlToValidate="ddlWeekDueDay" ID="vaDueWeekDay"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                            </div>
                        </div>

                    </div>
                    <div style="margin-bottom: 7px" id="divReminderType">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Reminder Type</label>
                        <asp:RadioButtonList ID="rbReminderType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="rbReminderType_SelectedIndexChanged">
                            <asp:ListItem Text="Standard" Value="0" Selected="True" />
                            <asp:ListItem Text="Custom" Value="1" />
                        </asp:RadioButtonList>
                    </div>
                    <div style="margin-bottom: 7px;" id="divForCustome" runat="server" visible="false">
                        <div style="float: left; width: 30%; overflow: hidden; margin-left: 200px;">
                            <label>
                                Before(In Days)
                            </label>
                            <asp:TextBox runat="server" ID="txtReminderBefore" MaxLength="4" Width="80px" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtReminderBefore"
                                runat="server" ErrorMessage="Only Numbers allowed" Display="None" ValidationExpression="^-?[0-9]\d*(\.\d+)?$" ValidationGroup="ComplianceValidationGroup" />
                            <asp:RequiredFieldValidator ID="rfvforcustomeReminder" ErrorMessage="Please enter reminder before value."
                                ControlToValidate="txtReminderBefore" runat="server" ValidationGroup="ComplianceValidationGroup"
                                Display="None" Enabled="false" />
                        </div>
                        <div style="overflow: hidden;">
                            <label>
                                Gap(In Days)
                            </label>
                            <asp:TextBox runat="server" ID="txtReminderGap" MaxLength="4" Width="80px" />
                        </div>

                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Priority Type</label>
                        <asp:DropDownList runat="server" ID="ddlRiskType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox">
                            <asp:ListItem Text="< Select >" Value="-1" />
                            <asp:ListItem Text="High" Value="0" />
                            <asp:ListItem Text="Low" Value="2" />
                            <asp:ListItem Text="Medium" Value="1" />
                            <asp:ListItem Text="Critical" Value="3" />

                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator8" ErrorMessage="Please select Priority type."
                            ControlToValidate="ddlRiskType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                            <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Upload Document</label>
                                <asp:CheckBox runat="server" ID="chkDocument" CssClass="txtbox" OnCheckedChanged="chkDocument_CheckedChanged" AutoPostBack="true" />
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Required Forms</label>
                                <asp:TextBox runat="server" ID="tbxRequiredForms" Style="height: 16px; width: 390px;" />
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Sample Form</label>
                                <asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" />
                                <asp:FileUpload runat="server" ID="fuSampleFile" />
                                <%-- <asp:RequiredFieldValidator ErrorMessage="Please select document to upload." ControlToValidate="fuSampleFile"
                            runat="server" ID="rfvFile" ValidationGroup="ComplianceValidationGroup" Display="None" Enabled="false" />--%>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div style="margin-bottom: 7px; margin-left: 210px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divInternalComplianceDetailsDialog').dialog('close');" />
                    </div>
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 0px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div id="divComplianceScheduleDialog">
        <asp:UpdatePanel ID="upComplianceScheduleDialog" runat="server" UpdateMode="Conditional" OnLoad="upComplianceScheduleDialog_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" Style="border: solid 1px red; background-color: #ffe8eb;"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div runat="server" id="divStartMonth" style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Select Year</label>
                        <asp:DropDownList runat="server" ID="ddlStartMonth" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" DataTextField="Name" DataValueField="ID"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlStartMonth_SelectedIndexChanged">
                            <asp:ListItem Value="1" Text="Calender Year"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Financial Year"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                     <div style="margin-bottom: 10px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Effective Date</label>
                        <%--<asp:Label ID="lblEffectivedate" runat="server" Text=""></asp:Label>--%>
                    </div>
                    <asp:UpdatePanel runat="server" ID="upSchedulerRepeter" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="Div1" runat="server" style="margin-bottom: 7px; padding-left: 50px">
                                <asp:Repeater runat="server" ID="repComplianceSchedule" OnItemDataBound="repComplianceSchedule_ItemDataBound">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <th style="width: 150px; border: 1px solid gray">For Period
                                                </th>
                                                <th style="width: 200px; border: 1px solid gray">Day
                                                </th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="margin-bottom: 7px">
                                            <tr>
                                                <td align="center" style="border: 1px solid gray">
                                                    <%# Eval("ForMonthName")%>
                                                    <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnForMonth" Value='<%# Eval("ForMonth") %>' />
                                                </td>
                                                <td align="center" style="border: 1px solid gray">
                                                    <asp:DropDownList runat="server" ID="ddlMonths" Style="padding: 0px; margin: 0px; height: 22px; width: 100px;"
                                                        CssClass="txtbox" DataTextField="Name" DataValueField="ID"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlMonths_SelectedIndexChanged" />
                                                    <asp:DropDownList runat="server" ID="ddlDays" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                                        CssClass="txtbox" DataTextField="Name" DataValueField="ID" />
                                                </td>
                                            </tr>
                                        </div>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 142px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSaveSchedule" OnClick="btnSaveSchedule_Click"
                            CssClass="button" />
                        <asp:Button Text="Reset" runat="server" ID="btnReset" OnClick="btnReset_Click"
                            CssClass="button" />
                        <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divComplianceScheduleDialog').dialog('close');" />
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divInternalComplianceDetailsDialog').dialog({
                height: 670,
                width: 800,
                autoOpen: false,
                draggable: true,
                title: "Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divComplianceScheduleDialog').dialog({
                height: 600,
                width: 600,
                autoOpen: false,
                draggable: true,
                title: "Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

       <%-- function initializeCombobox() {
            $("#<%= ddlIComplinceType.ClientID %>").combobox();
            $("#<%= ddlComplianceType.ClientID %>").combobox();
            $("#<%= ddlFrequency.ClientID %>").combobox();
            $("#<%= ddlRiskType.ClientID %>").combobox();
            $("#<%= ddlDueDate.ClientID %>").combobox();
            $("#<%= ddlICategory.ClientID %>").combobox();
            $("#<%= ddlChklstType.ClientID %>").combobox();
        }--%>

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeDatePicker(date) {

            var startDate = new Date();
            $("#<%= tbxOnetimeduedate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: startDate,
            numberOfMonths: 1,
            minDate: startDate,
            onClose: function (startDate) {
                $("#<%= tbxOnetimeduedate.ClientID %>").datepicker("option", "minDate", startDate);
            }
        });
            if (date != null) {
                $("#<%= tbxOnetimeduedate.ClientID %>").datepicker("option", "defaultDate", date);

            }
        }


        function initializeDatePickerEffectiveDate(date) {

                var startDate = new Date();
                $("#<%= txtEffectiveDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,
                onClose: function (startDate) {
                    $("#<%= txtEffectiveDate.ClientID %>").datepicker("option", "minDate", startDate);
                }
            });




                if (date != null) {
                    $("#<%= txtEffectiveDate.ClientID %>").datepicker("option", "defaultDate", date);

            }
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
