﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCSAuditChecklistSchedule : System.Web.UI.Page
    {
        protected static string user_Roles;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    user_Roles = AuthenticationHelper.Role;

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        [WebMethod]
        public static string BindScheduleAuditList(AuditListFilter AuditListFilterObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (AuditListFilterObj.VendorIDs != null && AuditListFilterObj.Branches != null)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<int> AuditIDList = new List<int>();
                        AuditListFilterObj.VendorIDs = AuditListFilterObj.VendorIDs.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        AuditIDList = AuditListFilterObj.VendorIDs.ConvertAll(int.Parse);
                        List<int> VendorList = new List<int>();
                        foreach (var AuditID in AuditIDList)
                        {
                            long Auditid = Convert.ToInt64(AuditID);
                            var AuditObj = (from aduit in entities.RLCS_VendorAuditInstance
                                            where aduit.ID == Auditid
                                            select aduit).FirstOrDefault();
                            int VendorID = Convert.ToInt32(AuditObj.AVACOM_VendorID);
                            VendorList.Add(VendorID);
                        }

                        //List<int> VendorList = new List<int>();
                        //AuditListFilterObj.VendorIDs = AuditListFilterObj.VendorIDs.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        //VendorList = AuditListFilterObj.VendorIDs.ConvertAll(int.Parse);

                        List<int> BranchesList = new List<int>();
                        AuditListFilterObj.Branches = AuditListFilterObj.Branches.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                        BranchesList = AuditListFilterObj.Branches.ConvertAll(int.Parse);

                        List<SP_RLCS_GetOpenAuditListDetails_Result> lstVendorScheduleAuditDetails = new List<SP_RLCS_GetOpenAuditListDetails_Result>();
                        lstVendorScheduleAuditDetails = entities.SP_RLCS_GetOpenAuditListDetails().ToList();
                        if (VendorList.Count > 0)
                        {

                            if (lstVendorScheduleAuditDetails.Count > 0)
                            {
                                if (VendorList.Count > 0)
                                    lstVendorScheduleAuditDetails = lstVendorScheduleAuditDetails.Where(x => VendorList.Contains(Convert.ToInt32(x.AVACOM_VendorID))).ToList();

                                List<int> BranchList = new List<int>();
                                foreach (var branchid in BranchesList)
                                {
                                    int branchId = Convert.ToInt32(branchid);
                                    BranchList.Add(branchId);
                                }
                                if (BranchList.Count > 0)
                                    lstVendorScheduleAuditDetails = lstVendorScheduleAuditDetails.Where(x => BranchList.Contains(Convert.ToInt32(x.BranchID))).ToList();
                            }

                        }
                        return serializer.Serialize(lstVendorScheduleAuditDetails);
                    }

                }
                else
                {
                    return serializer.Serialize("");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string AssignScheduleAudit(AuditListFilter AssignScheduleDetailsObj)
        {
            try
            {
                bool Success = false;

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    if (AssignScheduleDetailsObj.AssignScheduleDetails.Count > 0)
                    {
                        foreach (var Details in AssignScheduleDetailsObj.AssignScheduleDetails)
                        {
                            long SchedulOnID = Convert.ToInt64(Details.ScheduleOnID);
                            long AuditID = Convert.ToInt64(Details.AuditID);
                            long AuditorID = Convert.ToInt64(Details.AuditorID);
                            long VendorID = Convert.ToInt64(Details.VendorID);
                            RLCS_VendorAuditAssignment objActAssAssgmentVendor = (from row in entities.RLCS_VendorAuditAssignment
                                                                                  where row.AuditId == AuditID && row.UserID == VendorID && row.RoleID == 3
                                                                                  select row).FirstOrDefault();
                            if (objActAssAssgmentVendor == null)
                            {
                                RLCS_VendorAuditAssignment TempAssP = new RLCS_VendorAuditAssignment();
                                TempAssP.AuditId = Convert.ToInt64(Details.AuditID);
                                TempAssP.RoleID = 3;
                                TempAssP.UserID = Convert.ToInt32(Details.VendorID);
                                TempAssP.CreatedOn = DateTime.Now;
                                TempAssP.CustomerBranchID = Convert.ToInt32(Details.BranchID);
                                entities.RLCS_VendorAuditAssignment.Add(TempAssP);
                                entities.SaveChanges();
                            }
                            RLCS_VendorAuditAssignment objActAssAssgmentAuditor = (from row in entities.RLCS_VendorAuditAssignment
                                                                                   where row.AuditId == AuditID && row.UserID == AuditorID && row.RoleID == 4
                                                                                   select row).FirstOrDefault();
                            if (objActAssAssgmentAuditor == null)
                            {
                                RLCS_VendorAuditAssignment TempAssP1 = new RLCS_VendorAuditAssignment();
                                TempAssP1.AuditId = Convert.ToInt64(Details.AuditID);
                                TempAssP1.RoleID = 4;
                                TempAssP1.UserID = Convert.ToInt32(Details.AuditorID);
                                TempAssP1.CreatedOn = DateTime.Now;
                                TempAssP1.CustomerBranchID = Convert.ToInt32(Details.BranchID);
                                entities.RLCS_VendorAuditAssignment.Add(TempAssP1);
                                entities.SaveChanges();
                            }

                            RLCS_VendorAuditScheduleOn objUpdateScheduleOn = (from row in entities.RLCS_VendorAuditScheduleOn
                                                                              where row.ID == SchedulOnID
                                                                              select row).FirstOrDefault();
                            if (objUpdateScheduleOn != null)
                            {
                                if (Details.ScheduleOn != "")
                                {
                                    objUpdateScheduleOn.ScheduleOn = GetDate(Details.ScheduleOn);
                                    objUpdateScheduleOn.PONO = Details.PONO;
                                    objUpdateScheduleOn.InVoiceNo = Details.InvoiceNo;
                                    entities.SaveChanges();
                                    Success = true;
                                }
                            }
                        }
                    }
                    return serializer.Serialize(Success);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string ReCreateAuditSave(ReCreateAudit ReCreateAuditDetailsObj)
        {
            try
            {
                bool Success = false;
                long ScheduledID = Convert.ToInt64(ReCreateAuditDetailsObj.ScheduleID);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (CheckDuplicateInvoiceNo(ReCreateAuditDetailsObj))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        RLCS_VendorAuditScheduleOn Scheduledobj = (from row in entities.RLCS_VendorAuditScheduleOn
                                                                   where row.ID == ScheduledID
                                                                   select row).FirstOrDefault();

                        if (Scheduledobj != null)
                        {

                            RLCS_VendorAuditScheduleOn Scheduled = new RLCS_VendorAuditScheduleOn()
                            {
                                AuditID = Scheduledobj.AuditID,
                                ForMonth = Scheduledobj.ForMonth,
                                ForPeriod = Scheduledobj.ForPeriod,
                                PONO= Scheduledobj.PONO,
                                CustomerBranchID = Scheduledobj.CustomerBranchID,
                                IsActive = true,
                                AuditStatusID = 1,
                                InVoiceNo = ReCreateAuditDetailsObj.InVoiceNo
                            };
                            entities.RLCS_VendorAuditScheduleOn.Add(Scheduled);
                            entities.SaveChanges();

                            RLCS_VendorAuditTransaction transaction = new RLCS_VendorAuditTransaction()
                            {
                                AuditID = Scheduledobj.AuditID,
                                VendorAuditScheduleOnID = Scheduled.ID,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                CustomerBranchId = Scheduledobj.CustomerBranchID,
                                StatusId = 1,
                                Dated = DateTime.Now,
                                Remarks = "New compliance assigned."
                            };
                            entities.RLCS_VendorAuditTransaction.Add(transaction);
                            entities.SaveChanges();
                            List<long> CheckListID = new List<long>();

                            CheckListID = (from row in entities.RLCS_VendorAuditStepMapping
                                           where row.AuditID == Scheduledobj.AuditID && row.VendorAuditScheduleOnID == ScheduledID
                                           select
                               row.CheckListID).ToList();
                            foreach (var id in CheckListID)
                            {
                                RLCS_VendorAuditStepMapping StepMapping = new RLCS_VendorAuditStepMapping()
                                {
                                    AuditID = Scheduledobj.AuditID,
                                    VendorAuditScheduleOnID = Scheduled.ID,
                                    CheckListID = id,
                                    IsActive = true
                                };
                                entities.RLCS_VendorAuditStepMapping.Add(StepMapping);
                                entities.SaveChanges();
                            }
                            Success = true;
                        }
                        return serializer.Serialize(Success);
                    }
                }
                else
                {

                    return serializer.Serialize("InvoiceExist");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }



        public static bool CheckDuplicateInvoiceNo(ReCreateAudit ReCreateAuditDetailsObj)
        {
            try
            {
                bool Success = true;
                long ScheduledID = Convert.ToInt64(ReCreateAuditDetailsObj.ScheduleID);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    RLCS_VendorAuditScheduleOn Scheduledobj = (from row in entities.RLCS_VendorAuditScheduleOn
                                                               where row.ID == ScheduledID
                                                               select row).FirstOrDefault();

                    if (Scheduledobj != null)
                    {
                        List<string> ListInvoiceNo = new List<string>();

                        ListInvoiceNo = (from row in entities.RLCS_VendorAuditScheduleOn
                                         where row.AuditID == Scheduledobj.AuditID
                                         select
                             row.InVoiceNo).ToList();
                        if (ListInvoiceNo.Contains(ReCreateAuditDetailsObj.InVoiceNo))
                        {
                            Success = false;
                        }

                    }
                    return Success;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        [WebMethod]
        public static string BindAuditCustomerList()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var role = string.Empty;
                    if (user_Roles.Contains("HVADM"))
                    {
                        role = user_Roles.Trim();
                    }
                    else
                    {
                        var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                        role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                    }
                    var lstCustomers = RLCSVendorMastersManagement.GetAll(AuthenticationHelper.UserID, role);
                    return serializer.Serialize(lstCustomers);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindAuditEntityList(AuditListFilter AssignmentObj)
        {
            try
            {
                long CustomerID = Convert.ToInt64(AssignmentObj.CustomerID);
                List<object> Entities = new List<object>();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var role = string.Empty;
                if (user_Roles.Contains("HVADM"))
                {
                    role = user_Roles.Trim();
                }
                else
                {
                    var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                    role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                }
                if (CustomerID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var userAssignedBranchList = (entities.SP_RLCS_GetVendorAuditAssignedLocationBranches((int)CustomerID, AuthenticationHelper.UserID, role)).ToList();
                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                Entities = RLCSVendorMastersManagement.GetEntitiesCustomerWise((int)CustomerID);
                            }
                        }
                    }
                }
                return serializer.Serialize(Entities);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindAuditEntityStateList(AuditListFilter AssignmentObj)
        {
            try
            {
                long EntityID = Convert.ToInt64(AssignmentObj.EntityID);
                long CustomerID = Convert.ToInt64(AssignmentObj.CustomerID);
                List<object> EntityState = new List<object>();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var role = string.Empty;
                if (user_Roles.Contains("HVADM"))
                {
                    role = user_Roles.Trim();
                }
                else
                {
                    var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                    role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                }
                if (EntityID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        EntityState = RLCSVendorMastersManagement.GetStateEntityWise((int)CustomerID);
                    }
                }
                return serializer.Serialize(EntityState);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindAuditEntityLocationList(AuditListFilter AssignmentObj)
        {
            try
            {
                long StateID = Convert.ToInt64(AssignmentObj.StateID);
                long CustomerID = Convert.ToInt64(AssignmentObj.CustomerID);
                List<object> EntityLocation = new List<object>();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var role = string.Empty;
                if (user_Roles.Contains("HVADM"))
                {
                    role = user_Roles.Trim();
                }
                else
                {
                    var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                    role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                }
                if (StateID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        EntityLocation = RLCSVendorMastersManagement.GetLocationStateWise((int)StateID, (int)CustomerID);
                    }
                }
                return serializer.Serialize(EntityLocation);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindEntityBranchList(AuditListFilter AssignmentObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (AssignmentObj.LocationID != null)
                {
                    List<int> LocationList = new List<int>();
                    AssignmentObj.LocationID = AssignmentObj.LocationID.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                    LocationList = AssignmentObj.LocationID.ConvertAll(int.Parse);
                    long CustomerID = Convert.ToInt64(AssignmentObj.CustomerID);
                    List<BranchesLocationWise> EntityBranch = new List<BranchesLocationWise>();
                    var role = string.Empty;
                    if (user_Roles.Contains("HVADM"))
                    {
                        role = user_Roles.Trim();
                    }
                    else
                    {
                        var user = UserManagement.GetByID(AuthenticationHelper.UserID);
                        role = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                    }
                    foreach (var LocationID in LocationList)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var branches = RLCSVendorMastersManagement.GetBranchLocationWise((int)CustomerID, (int)LocationID);
                            foreach (var item in branches)
                            {
                                BranchesLocationWise branchobj = new BranchesLocationWise();
                                branchobj.ID = item.ID;
                                branchobj.Name = item.Name;
                                EntityBranch.Add(branchobj);
                            }
                        }
                    }
                    return serializer.Serialize(EntityBranch);
                }
                return serializer.Serialize(0);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }
        [WebMethod]
        public static string BindAuditVendorList(AuditListFilter AssignmentObj)
        {
            try
            {

                int branchID = -1;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (AssignmentObj.Branches != null && AssignmentObj.CustomerID != null)
                {
                    //  branchID = Convert.ToInt32(AssignmentObj.Location);
                    List<int> BranchList = new List<int>();
                    AssignmentObj.Branches = AssignmentObj.Branches.Where(s => !string.IsNullOrEmpty(s)).Distinct().ToList();
                    BranchList = AssignmentObj.Branches.ConvertAll(int.Parse);
                    List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                    Dictionary<string, object> childRow;

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<CustomerBranch> Obj = new List<CustomerBranch>();
                        DataTable VendorTable = new DataTable("Vendor");
                        DataColumn dtColumn;
                        DataRow myDataRow;
                        // Create id column  
                        dtColumn = new DataColumn();
                        dtColumn.DataType = typeof(Int32);
                        dtColumn.ColumnName = "ID";
                        dtColumn.Caption = "Vendor ID";
                        dtColumn.ReadOnly = false;
                        dtColumn.Unique = false;
                        // Add column to the DataColumnCollection.  
                        VendorTable.Columns.Add(dtColumn);

                        // Create Name column.    
                        dtColumn = new DataColumn();
                        dtColumn.DataType = typeof(String);
                        dtColumn.ColumnName = "Name";
                        dtColumn.Caption = "Vendor Name";
                        dtColumn.AutoIncrement = false;
                        dtColumn.ReadOnly = false;
                        dtColumn.Unique = false;
                        /// Add column to the DataColumnCollection.  
                        VendorTable.Columns.Add(dtColumn);

                        // Create Name column.    
                        dtColumn = new DataColumn();
                        dtColumn.DataType = typeof(String);
                        dtColumn.ColumnName = "ContractorName";
                        dtColumn.Caption = "Contractor Name";
                        dtColumn.AutoIncrement = false;
                        dtColumn.ReadOnly = false;
                        dtColumn.Unique = false;
                        /// Add column to the DataColumnCollection.  
                        VendorTable.Columns.Add(dtColumn);
                        // Create Name column.    
                        dtColumn = new DataColumn();
                        dtColumn.DataType = typeof(int);
                        dtColumn.ColumnName = "StartYear";
                        dtColumn.Caption = "Contractor StartYear";
                        dtColumn.AutoIncrement = false;
                        dtColumn.ReadOnly = false;
                        dtColumn.Unique = false;
                        /// Add column to the DataColumnCollection.  
                        VendorTable.Columns.Add(dtColumn);
                        // Create Name column.    
                        dtColumn = new DataColumn();
                        dtColumn.DataType = typeof(int);
                        dtColumn.ColumnName = "EndYear";
                        dtColumn.Caption = "Contractor EndYear";
                        dtColumn.AutoIncrement = false;
                        dtColumn.ReadOnly = false;
                        dtColumn.Unique = false;
                        /// Add column to the DataColumnCollection.  
                        VendorTable.Columns.Add(dtColumn);
                        foreach (var BranchId in BranchList)
                        {
                            branchID = Convert.ToInt32(BranchId);
                            int branchid = branchID;
                            int Customerid = Convert.ToInt32(AssignmentObj.CustomerID);
                            var data = entities.SP_RLCS_Vendor_GetContractorsList(branchid, Customerid).ToList();
                            foreach (var row in data)
                            {
                                myDataRow = VendorTable.NewRow();
                                myDataRow["ID"] = row.AuditID;
                                myDataRow["Name"] = row.Name;
                                myDataRow["ContractorName"] = row.CC_ContractorName;
                                myDataRow["StartYear"] = row.ContractStartYear;
                                myDataRow["EndYear"] = row.ContractEndYear;
                                VendorTable.Rows.Add(myDataRow);
                            }
                        }
                        foreach (DataRow row in VendorTable.Rows)
                        {
                            childRow = new Dictionary<string, object>();
                            foreach (DataColumn col in VendorTable.Columns)
                            {
                                childRow.Add(col.ColumnName, row[col]);
                            }
                            parentRow.Add(childRow);
                        }
                        if (VendorTable.Rows.Count > 0)
                        {
                            return serializer.Serialize(parentRow);
                        }
                        else
                        {
                            return serializer.Serialize("");
                        }
                    }
                }
                else
                {
                    return serializer.Serialize("");
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }


        public static List<BranchesLocationWise> GetBranchLocationWise(int LocationID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objchechklist = (from CB in entities.CustomerBranches
                                     where CB.ParentID == LocationID
                                     select new BranchesLocationWise
                                     {
                                         Name = CB.Name,
                                         ID = CB.ID
                                     }).Distinct().ToList();

                return objchechklist;
            }
        }

        public static DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public class AuditListFilter
        {
            public string CustomerID { get; set; }
            public string EntityID { get; set; }
            public string StateID { get; set; }
            public List<string> LocationID { get; set; }
            public List<string> VendorIDs { get; set; }
            public List<string> Branches { get; set; }

            public List<AssignScheduleDetails> AssignScheduleDetails { get; set; }

        }

        public class AssignScheduleDetails
        {
            public string ScheduleOnID { get; set; }
            public string AuditID { get; set; }
            public string BranchID { get; set; }
            public string AuditorID { get; set; }
            public string VendorID { get; set; }
            public string ScheduleOn { get; set; }
            public string PONO { get; set; }
            public string InvoiceNo { get; set; }

            public List<string> VendorIDs { get; set; }

            public List<string> AuditorIDs { get; set; }

        }

        public class BranchesLocationWise
        {
            public int? ID { get; set; }
            public string Name { get; set; }
        }

        public class ReCreateAudit
        {
            public string ScheduleID { get; set; }
            public string InVoiceNo { get; set; }

        }

        public class VendorAuditStepMappingDetails
        {
            public long CheckListID { get; set; }
        }

    }
}