﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Collections.Generic;
using Saplin.Controls;
using System.Web.Services;
using System.Web.Script.Serialization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCSVendorAuditReassignAudit : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }
        [WebMethod]
        public static string BindAuditDetailsTable(AuditDetails auditdetails)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            int ScheduleId = Convert.ToInt32(auditdetails.ScheduledOnID);
            List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result> AuditDetail = new List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result>();

            AuditDetail = fetchDataManagement(0, Convert.ToInt32(auditdetails.UserID), auditdetails.Role, null);
            if (ScheduleId > 0)
            {
                AuditDetail = AuditDetail.Where(x => x.AuditScheduleOnID == ScheduleId).ToList();
            }
            AuditTable tblObj = new AuditTable();
            tblObj.Location = AuditDetail[0].Location;
            tblObj.Vendor = AuditDetail[0].VendorName;
            tblObj.SPOC = AuditDetail[0].SPOCName;
            tblObj.StartDate = Convert.ToDateTime(AuditDetail[0].StartDate).ToString("dd-MMM-yyyy");
            tblObj.EndDate =Convert.ToDateTime(AuditDetail[0].EndDate).ToString("dd-MMM-yyyy");
            if (tblObj.EndDate == "01-Jan-0001")
                tblObj.EndDate = "";

            if (tblObj.StartDate == "01-Jan-0001")
                tblObj.StartDate = "";


            tblObj.Period = AuditDetail[0].ForMonth;

            return serializer.Serialize(tblObj);
        }

        public static List<SP_RLCS_GetOpenAndClosedAuditCountDetails_Result> fetchDataManagement(int customerid, int userid, string role, string Period = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                customerid = (int)AuthenticationHelper.CustomerID;
                var mgmtQueryStatutory = (from row in entities.SP_RLCS_GetOpenAndClosedAuditCountDetails(customerid, userid, "HVAUD")
                                          select row).ToList();
                mgmtQueryStatutory = mgmtQueryStatutory.Where(x => x.AuditStatusID == 1 || x.AuditStatusID == 4 || x.AuditStatusID == 2 || x.AuditStatusID == 5).ToList();                //var result =  from m in mgmtQueryStatutory join CB in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                if (mgmtQueryStatutory.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Period))
                        mgmtQueryStatutory = mgmtQueryStatutory.Where(x => x.ForMonth.Trim().ToUpper() == Period.Trim().ToUpper()).ToList();

                }
                return mgmtQueryStatutory;
            }
        }

        [WebMethod]
        public static string BindAuditorList()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = entities.SP_RLCS_Auditor_GetChildBranchesUsersFromParentBranch(0).ToList();
                    return serializer.Serialize(data);

                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string ReassignAudit(AuditDetails auditdetails)
        {
            string Success = "0";
            int ID = Convert.ToInt32(auditdetails.ScheduledOnID);
            int Auditorid= Convert.ToInt32(auditdetails.AuditorID);
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                   
                        var Audit = (from row in entities.RLCS_VendorAuditScheduleOn
                                     where row.ID == ID
                                     select row).FirstOrDefault();

                    Audit.AuditorID = Auditorid;
                    entities.SaveChanges();
                    Success = "1";

                }
                return Success;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }

    public class AuditDetails
    {
        public string CustomerID { get; set; }
        public string UserID { get; set; }
        public string Role { get; set; }
        public string Period { get; set; }
        public string AuditID { get; set; }
        public string ScheduledOnID { get; set; }
        public string AuditorID { get; set; }
    }
    public class AuditTable
    {
        public string Location { get; set; }
        public string Vendor { get; set; }
        public string SPOC { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Period { get; set; }

    }
}