﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class AuditDisplayScheduleInformation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                {
                    OpenScheduleInformation(Convert.ToInt32(Request.QueryString["AOID"].ToString()));
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static bool ExistsComplianceScheduleAssignment(long auditID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.RLCS_VendorAuditInstance
                             join row1 in entities.RLCS_VendorAuditAssignment
                             on row.ID equals row1.AuditId
                             where row.ID== auditID && row.IsActive == false && row.CC_Status=="A"
                             select row1.ID).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static List<RLCS_VendorFrquencySchedule> GetScheduleByComplianceID(long auditid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.RLCS_VendorFrquencySchedule
                                    where row.AuditID == auditid
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }
        public static void GenerateDefaultScheduleForComplianceID(long auditID, long duedate, int FrequencyID, bool deleteOldData = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (FrequencyID != -1)
                {
                    if (deleteOldData)
                    {
                        var ids = (from row in entities.RLCS_VendorFrquencySchedule
                                   where row.AuditID == auditID
                                   select row.ID).ToList();
                        ids.ForEach(entry =>
                        {
                            RLCS_VendorFrquencySchedule schedule = (from row in entities.RLCS_VendorFrquencySchedule
                                                                    where row.ID == entry
                                                                    select row).FirstOrDefault();

                            entities.RLCS_VendorFrquencySchedule.Remove(schedule);
                        });
                        entities.SaveChanges();
                    }
                    int step = 1;
                    switch ((VFrequency)FrequencyID)
                    {
                        case VFrequency.Quarterly:
                            step = 3;
                            break;
                        case VFrequency.HalfYearly:
                            step = 6;
                            break;
                        case VFrequency.Annual:
                            step = 12;
                            break;
                    }

                    if (FrequencyID == 1 || FrequencyID == 4)
                    {
                        //Monthly and Quarterly
                        int SpecialMonth;
                        for (int month = 1; month <= 12; month += step)
                        {
                            RLCS_VendorFrquencySchedule complianceShedule = new RLCS_VendorFrquencySchedule();
                            complianceShedule.AuditID = auditID;
                            complianceShedule.ForMonth = month;
                            complianceShedule.FrequnecyId = FrequencyID;
                            SpecialMonth = month;
                            if (FrequencyID == 1 && SpecialMonth == 12)
                            {
                                SpecialMonth = 1;
                            }
                            else if (FrequencyID == 4 && SpecialMonth == 10)
                            {
                                SpecialMonth = 1;
                            }
                            else if (FrequencyID == 2 && SpecialMonth == 7)
                            {
                                SpecialMonth = 1;
                            }
                            else if (FrequencyID == 3 && SpecialMonth == 1)
                            {
                                SpecialMonth = 1;
                            }
                            else
                            {
                                SpecialMonth = SpecialMonth + step;
                            }
                            int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                            if (Convert.ToInt32(duedate.ToString("D2")) > lastdate)
                            {
                                complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                            }
                            else
                            {
                                complianceShedule.SpecialDate = duedate.ToString("D2") + SpecialMonth.ToString("D2");
                            }
                            entities.RLCS_VendorFrquencySchedule.Add(complianceShedule);
                        }
                        entities.SaveChanges();
                    }
                    else
                    {
                        int SpecialMonth;
                        for (int month = 4; month <= 12; month += step)
                        {
                            RLCS_VendorFrquencySchedule complianceShedule = new RLCS_VendorFrquencySchedule();
                            complianceShedule.AuditID = auditID;
                            complianceShedule.ForMonth = month;
                            complianceShedule.FrequnecyId = FrequencyID;
                            SpecialMonth = month;
                            if (FrequencyID == 1 && SpecialMonth == 12)
                            {
                                SpecialMonth = 1;
                            }
                            else if (FrequencyID == 4 && SpecialMonth == 10)
                            {
                                SpecialMonth = 1;
                            }
                            else if (FrequencyID == 2 && SpecialMonth == 10)
                            {
                                SpecialMonth = 4;
                            }
                            else if (FrequencyID == 3 && SpecialMonth == 4)
                            {
                                SpecialMonth = 4;
                            }
                            else
                            {
                                SpecialMonth = SpecialMonth + step;
                            }
                            int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                            if (Convert.ToInt32(duedate.ToString("D2")) > lastdate)
                            {
                                complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                            }
                            else
                            {
                                complianceShedule.SpecialDate = duedate.ToString("D2") + SpecialMonth.ToString("D2");
                            }
                            entities.RLCS_VendorFrquencySchedule.Add(complianceShedule);
                        }
                        entities.SaveChanges();
                    }
                }//frequency end                
            }
        }
        private void OpenScheduleInformation(long auditID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var instance = (from row in entities.RLCS_VendorAuditInstance
                                    where row.ID == auditID && row.CC_Status=="A"
                                    && row.IsActive==false
                                    select row).First();
                    if (instance != null)
                    {
                        int frequencyid = -1;
                        if (instance.CC_Frequency.ToUpper().Trim() == "M")
                        {
                            frequencyid = 1;
                        }
                        else if (instance.CC_Frequency.ToUpper().Trim() == "H")
                        {
                            frequencyid = 2;
                        }
                        else if (instance.CC_Frequency.ToUpper().Trim() == "A" || instance.CC_Frequency.ToUpper().Trim() == "Y")
                        {
                            frequencyid = 3;
                        }
                        else if (instance.CC_Frequency.ToUpper().Trim() == "Q")
                        {
                            frequencyid = 4;
                        }
                        if (frequencyid != -1)
                        {
                            //DueDate
                            ViewState["AuditID"] = auditID;
                            ViewState["Frequency"] = frequencyid;
                            if (instance.DueDate != null)
                            {
                                ViewState["Day"] = instance.DueDate;
                            }
                            if ((frequencyid == 1 || frequencyid == 4))
                                divStartMonth.Visible = false;
                            else
                                divStartMonth.Visible = true;
                           
                            var scheduleList = GetScheduleByComplianceID(auditID);
                            if (scheduleList.Count == 0)
                            {
                                GenerateDefaultScheduleForComplianceID(auditID, instance.DueDate, frequencyid);
                                scheduleList= GetScheduleByComplianceID(auditID);
                            }

                            int step = 0;
                            if (frequencyid == 1)
                                step = 0;
                            else if (frequencyid == 4)
                                step = 2;
                            else if (frequencyid == 2)
                                step = 5;                            
                            else
                                step = 11;

                            var dataSource = scheduleList.Select(entry => new
                            {
                                ID = entry.ID,
                                ForMonth = entry.ForMonth,
                                ForMonthName = frequencyid == 1 ? ((Month)entry.ForMonth).ToString() : ((Month)entry.ForMonth).ToString() + " - " + ((Month)((entry.ForMonth + step) > 12 ? (entry.ForMonth + step) - 12 : (entry.ForMonth + step))).ToString(),
                                SpecialDay = Convert.ToByte(entry.SpecialDate.Substring(0, 2)),
                                SpecialMonth = Convert.ToByte(entry.SpecialDate.Substring(2, 2))
                            }).ToList();

                            if (divStartMonth.Visible)
                            {
                                ddlStartMonth.SelectedValue = dataSource.First().ForMonth.ToString();
                            }
                            repComplianceSchedule.DataSource = dataSource;
                            repComplianceSchedule.DataBind();

                            //upSchedulerRepeter.Update();
                            //upComplianceScheduleDialog.Update();                            
                        }// frequencyid end
                    }//instance end
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cucustomentry.IsValid = false;
                cucustomentry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void UpdateComplianceUpdatedOn(long auditid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_VendorAuditInstance compliance = (from row in entities.RLCS_VendorAuditInstance
                                                       where row.ID == auditid
                                                       select row).FirstOrDefault();

                compliance.UpdatedOn = DateTime.Now;             
                entities.SaveChanges();
            }
        }
        public static void UpdateScheduleInformation(List<RLCS_VendorFrquencySchedule> scheduleList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                scheduleList.ForEach(schedule =>
                {
                    var complianceScheduleToUpdate = (from row in entities.RLCS_VendorFrquencySchedule
                                                      where row.ID == schedule.ID
                                                      select row).FirstOrDefault();

                    complianceScheduleToUpdate.AuditID = schedule.AuditID;
                    complianceScheduleToUpdate.ForMonth = schedule.ForMonth;
                    complianceScheduleToUpdate.SpecialDate = schedule.SpecialDate;
                });
                entities.SaveChanges();
            }
        }
        public static void ResetComplianceSchedule(int auditid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceSchedule = (from row in entities.RLCS_VendorFrquencySchedule
                                          where row.AuditID == auditid
                                          select row).ToList();

                complianceSchedule.ForEach(entry =>
                {
                    entities.RLCS_VendorFrquencySchedule.Attach(entry);
                    entities.RLCS_VendorFrquencySchedule.Remove(entry);
                });

                entities.SaveChanges();
            }
        }
        protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                var dataSource = new List<object>();
                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");


                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = Convert.ToByte(hdnForMonth.Value),
                        ForMonthName = ((Month)Convert.ToByte(hdnForMonth.Value)).ToString(),
                        SpecialDay = ddlDays.SelectedValue,
                        SpecialMonth = ddlMonths.SelectedValue
                    });


                    Month month = (Month)Convert.ToInt32(ddlMonths.SelectedValue);
                    int totalDays = 0;
                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var daysdataSource = new List<object>();

                    for (int j = 1; j <= totalDays; j++)
                    {
                        daysdataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = daysdataSource;
                    ddlDays.DataBind();
                    //upSchedulerRepeter.Update();
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cucustomentry.IsValid = false;
                cucustomentry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlStartMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                VFrequency frequency = (VFrequency)Convert.ToByte(ViewState["Frequency"]);
                byte day = Convert.ToByte(ViewState["Day"]);
                byte startMonth = Convert.ToByte(ddlStartMonth.SelectedValue);
                byte step = 1;
                switch (frequency)
                {
                    case VFrequency.Quarterly:
                        step = 3;
                        break;                    
                    case VFrequency.HalfYearly:
                        step = 6;
                        break;
                    case VFrequency.Annual:
                        step = 12;
                        break;
                    
                }

                var dataSource = new List<object>();

                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");

                    int month = startMonth + (step * i) <= 12 ? startMonth + (step * i) : 1;
                    int specialMonth;
                    if (step == 12)
                    {
                        specialMonth = startMonth + (step * i);
                    }
                    else
                    {
                        if (month < 10)
                        {
                            if (month > 12)
                            {
                                specialMonth = 1;
                            }
                            else
                            {
                                specialMonth = startMonth + (step * i) + 6;

                            }
                        }
                        else
                        {

                            specialMonth = startMonth + (step * i) - 6;
                        }
                    }

                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = month,
                        ForMonthName = frequency == VFrequency.Monthly ? ((Month)month).ToString() : ((Month)month).ToString() + " - " + ((Month)((month + (step - 1)) > 12 ? (month + (step - 1)) - 12 : (month + (step - 1)))).ToString(),
                        SpecialDay = day,
                        SpecialMonth = specialMonth
                    });
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cucustomentry.IsValid = false;
                cucustomentry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void repComplianceSchedule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    DropDownList ddlDays = (DropDownList)e.Item.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)e.Item.FindControl("ddlMonths");


                    ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterAsyncPostBackControl(ddlMonths);
                    }

                    ddlMonths.DataSource = Enumerations.GetAll<Month>();
                    ddlMonths.DataBind();

                    int totalDays = 28;
                    int day = Convert.ToInt16(Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialDay").GetValue(e.Item.DataItem, null).ToString()));
                    Month month = (Month)Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialMonth").GetValue(e.Item.DataItem, null).ToString());
                    ddlMonths.SelectedValue = ((byte)month).ToString();

                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var dataSource = new List<object>();

                    for (int i = 1; i <= totalDays; i++)
                    {
                        dataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = dataSource;
                    ddlDays.DataBind();

                    if (day > totalDays)
                    {
                        day = totalDays;
                    }

                    ddlDays.SelectedValue = day.ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cucustomentry.IsValid = false;
                cucustomentry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSaveSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["AuditID"].ToString() != null && ViewState["AuditID"].ToString() != "")
                {
                    bool chkexistsComplianceschedule = ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["AuditID"]));
                    if (chkexistsComplianceschedule == true)
                    {
                        cucustomentry.IsValid = false;
                        cucustomentry.ErrorMessage = "Audit assigned cannot be change Calender/Financial Year.";
                    }
                    else
                    {
                        List<RLCS_VendorFrquencySchedule> scheduleList = new List<RLCS_VendorFrquencySchedule>();

                        for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                        {
                            RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                            HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                            HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                            DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                            DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");

                            scheduleList.Add(new RLCS_VendorFrquencySchedule()
                            {
                                ID = Convert.ToInt32(hdnID.Value),
                                AuditID = Convert.ToInt64(ViewState["AuditID"]),
                                FrequnecyId = Convert.ToInt32(ViewState["Frequency"]),
                                ForMonth = Convert.ToInt32(hdnForMonth.Value),
                                SpecialDate = string.Format("{0}{1}", Convert.ToByte(ddlDays.SelectedValue).ToString("D2"), Convert.ToByte(ddlMonths.SelectedValue).ToString("D2"))
                            });
                        }
                        UpdateComplianceUpdatedOn(Convert.ToInt64(ViewState["AuditID"]));
                        UpdateScheduleInformation(scheduleList);
                        cucustomentry.IsValid = false;
                        cucustomentry.ErrorMessage = "Schedule Save Successfully.";
                        //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                        //upSchedulerRepeter.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cucustomentry.IsValid = false;
                cucustomentry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (ViewState["AuditID"].ToString() != null && ViewState["AuditID"].ToString() != "")
                {
                    bool chkexistsComplianceschedule =ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["AuditID"]));
                    if (chkexistsComplianceschedule == true)
                    {
                        cucustomentry.IsValid = false;
                        cucustomentry.ErrorMessage = "Audit assigned cannot be change Calender/Financial Year.";
                    }
                    else
                    {
                        ResetComplianceSchedule(Convert.ToInt32(ViewState["AuditID"]));
                        //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                        //upSchedulerRepeter.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cucustomentry.IsValid = false;
                cucustomentry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}