﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Collections.Generic;
using Saplin.Controls;
using System.Web;
using System.IO;
using System.Configuration;
using Ionic.Zip;
using System.Net;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCSVendorAuditChecklist : System.Web.UI.Page
    {
        public static List<long> NewStepID = new List<long>();
        public static int TotalChecklistCount = 0;
        public static int TotalChecklistCloseCount = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["AID"]))
                    && !string.IsNullOrEmpty(Convert.ToString(Request.QueryString["SOID"])))
                {
                    ViewState["AID"] = Request.QueryString["AID"];
                    ViewState["SOID"] = Request.QueryString["SOID"];
                    GetAuditName();
                    string Flag = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["Flag"])))
                    {
                        Flag = Convert.ToString(Request.QueryString["Flag"]);
                    }
                    if (Flag.Equals("byOtp"))
                    {
                        btnBack.Visible = false;
                    }

                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = "ID";

                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                        ViewState["vrole"] = Convert.ToString(vrole);
                        if(vrole== "HVAUD")
                        {
                            divUploadDoc.Visible = false;
                        }
                        else
                        {
                            divUploadDoc.Visible = true;
                        }
                        buttonvisible(vrole);
                    }
                    StepBind(ddlNewStepCheck);

                    BindAuditCheckList();
                    bindActGroupList();
                    bindChecklistStatus();
                    BindGrid();
                }
            }
        }
        private void GetAuditName()
        {
            try
            {
                int AuditId = Convert.ToInt32(ViewState["AID"]);
                int ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var AuditDetail = (from row in entities.RLCS_VendorAuditScheduleOn
                                       join row1 in entities.CustomerBranches
                                       on row.CustomerBranchID equals row1.ID
                                       where row1.IsDeleted == false
                                       && row.ID == ScheduleOnID
                                       && row.AuditID == AuditId
                                       //row.IsActive == true
                                       select new {row, row1 }).FirstOrDefault();
                    if (AuditDetail != null)
                    {
                        lblAuditName.Text = AuditDetail.row1.Name +" > "+ Convert.ToString(Request.QueryString["VendorName"]) + " > "+ AuditDetail.row.ForMonth;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindChecklistStatus()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = RLCSVendorMastersManagement.getRLCSChecklistStatus();
                    ddlChecklistStatus.DataTextField = "Name";
                    ddlChecklistStatus.DataValueField = "ID";
                    ddlChecklistStatus.DataSource = data;
                    ddlChecklistStatus.DataBind();
                    ddlChecklistStatus.Items.Insert(0, new ListItem("Select Status ", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindActGroupList()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var data = RLCSVendorMastersManagement.getRLCSActGrouplistData();
                    ddlAct.DataTextField = "AM_ActName";
                    ddlAct.DataValueField = "AM_ActID";
                    ddlAct.DataSource = data;
                    ddlAct.DataBind();
                    ddlAct.Items.Insert(0, new ListItem("Select Act ", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditCheckList();
        }
        protected void ddlChecklistStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditCheckList();
        }

        private void BindAuditCheckList()
        {
            try
            {
                grdChecklist.DataSource = null;
                grdChecklist.DataBind();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int AuditId = Convert.ToInt32(ViewState["AID"]);
                    int ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);

                    if (AuditId > 0)
                    {
                        User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                        var role = "";
                        if (LoggedUser != null)
                        {
                            role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                        }
                            var lstChecklistDetails = entities.SP_RLCS_VendorCheckListStatusDetails(AuditId, ScheduleOnID, Convert.ToInt64(AuthenticationHelper.UserID), role).ToList();
                        lstChecklistDetails = lstChecklistDetails.GroupBy(test => test.ID).Select(grp => grp.First()).ToList();
                        if (lstChecklistDetails.Count > 0)
                        {
                            string actid = string.Empty;
                            string status = string.Empty;
                            if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                            {
                                if (Convert.ToString(ddlAct.SelectedValue) != "-1")
                                {
                                    actid = Convert.ToString(ddlAct.SelectedValue);
                                    lstChecklistDetails = lstChecklistDetails.Where(entry => entry.ActID == actid).ToList();
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlChecklistStatus.SelectedValue))
                            {
                                if (Convert.ToString(ddlChecklistStatus.SelectedValue) != "-1")
                                {
                                    status = Convert.ToString(ddlChecklistStatus.SelectedItem.Text);
                                    lstChecklistDetails = lstChecklistDetails.Where(entry => entry.Status == status).ToList();
                                }
                            }
                            TotalChecklistCount = lstChecklistDetails.Count;
                            string statusid = "Closed";
                            TotalChecklistCloseCount = lstChecklistDetails.Where(entry => entry.Status == statusid).ToList().Count;
                            grdChecklist.DataSource = lstChecklistDetails;
                            grdChecklist.DataBind();
                            Session["TotalRows"] = lstChecklistDetails.Count;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdChecklist_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EditChecklist"))
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        var vrole = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;

                        if (vrole.Equals("HVEND"))
                        {
                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                            long CheckListID = Convert.ToInt64(commandArgs[0]);
                            string CheckListStatus = Convert.ToString(commandArgs[1]);
                            long CustomerBranchID = Convert.ToInt64(commandArgs[2]);

                            long StatusId = -1;
                            if (Convert.ToString(commandArgs[1]) == "Open")
                                StatusId = 1;

                            if (Convert.ToString(commandArgs[1]) == "Submitted")
                                StatusId = 2;

                            if (Convert.ToString(commandArgs[1]) == "Team Review")
                                StatusId = 3;

                            if (Convert.ToString(commandArgs[1]) == "Closed")
                                StatusId = 4;

                            long AuditId = Convert.ToInt32(ViewState["AID"]);
                            long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);

                            ScriptManager.RegisterStartupScript(this, GetType(), "script", "openperformerpopup(" + CheckListID + "," + AuditId + "," + ScheduleOnID + "," + StatusId + "," + CustomerBranchID + ");", true);
                        }
                        else if (vrole.Equals("HVAUD"))
                        {
                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                            long CheckListID = Convert.ToInt64(commandArgs[0]);
                            string CheckListStatus = Convert.ToString(commandArgs[1]);
                            long CustomerBranchID = Convert.ToInt64(commandArgs[2]);
                            long StatusId = -1;
                            if (Convert.ToString(commandArgs[1]) == "Open")
                                StatusId = 1;

                            if (Convert.ToString(commandArgs[1]) == "Submitted")
                                StatusId = 2;

                            if (Convert.ToString(commandArgs[1]) == "Team Review")
                                StatusId = 3;

                            if (Convert.ToString(commandArgs[1]) == "Closed")
                                StatusId = 4;

                            long AuditId = Convert.ToInt32(ViewState["AID"]);
                            long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);
                            ScriptManager.RegisterStartupScript(this, GetType(), "script", "openreviwerpopup(" + CheckListID + "," + AuditId + "," + ScheduleOnID + "," + StatusId + "," + CustomerBranchID + ");", true);
                        }
                    }
                }
                else if (e.CommandName.Equals("ClosedChecklist"))
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {

                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        long CheckListID = Convert.ToInt64(commandArgs[0]);
                        string CheckListStatus = Convert.ToString(commandArgs[1]);
                        long CustomerBranchID = Convert.ToInt64(commandArgs[2]);

                        long StatusId = -1;
                        if (Convert.ToString(commandArgs[1]) == "Open")
                            StatusId = 1;

                        if (Convert.ToString(commandArgs[1]) == "Submitted")
                            StatusId = 2;

                        if (Convert.ToString(commandArgs[1]) == "Team Review")
                            StatusId = 3;

                        if (Convert.ToString(commandArgs[1]) == "Closed")
                            StatusId = 4;

                        long AuditId = Convert.ToInt32(ViewState["AID"]);
                        long ScheduleOnID = Convert.ToInt32(ViewState["SOID"]);

                        ScriptManager.RegisterStartupScript(this, GetType(), "script", "openClosedpopup(" + CheckListID + "," + AuditId + "," + ScheduleOnID + "," + StatusId + "," + CustomerBranchID + ");", true);                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        public static List<int> GetAssignedroleid(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> roles = new List<int>();
                var rolesfromsp = (entities.RLCS_Sp_GetAllAssignedRoles(Userid)).ToList();
                roles = rolesfromsp.Where(x => x != null).Cast<int>().ToList();
                return roles;
            }
        }
        protected bool CanChangeStatus(string Status,int astustid)
        {
            try
            {
                bool result = false;
                if (astustid == 5)
                {
                    result = false;
                }
                else
                {
                    int statusID = -1;
                    if (Status == "Open")
                    {
                        statusID = 1;
                    }
                    else if (Status == "Submitted")
                    {
                        statusID = 2;
                    }
                    else if (Status == "Team Review")
                    {
                        statusID = 3;
                    }
                    else
                    {
                        statusID = 4;
                    }
                    List<int> roles = GetAssignedroleid(AuthenticationHelper.UserID);
                    if (roles.Contains(3))
                    {
                        result = CanChangePerformerStatus(AuthenticationHelper.UserID, 3, statusID);
                    }
                    else if (roles.Contains(4))
                    {
                        result = CanChangeReviewerStatus(AuthenticationHelper.UserID, 4, statusID);
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected bool CanChangeStatusClosed(string Status)
        {
            try
            {
                bool result = false;
                if (!string.IsNullOrEmpty(Status))
                {
                    if (Status.Equals("Closed"))
                    {
                        result = true;
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        
        protected bool CanChangePerformerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 3;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CanChangeReviewerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }

                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindAuditCheckList();                
                string  role = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["vrole"])))
                {
                    role = Convert.ToString(ViewState["vrole"]);
                }
                else
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (LoggedUser != null)
                    {
                        role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                        ViewState["vrole"] = Convert.ToString(role);                                              
                    }
                }
                buttonvisible(role);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public void buttonvisible(string Role)
        {
            try
            {
                if (Role == "HVAUD")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        long scheduleonID = -1;
                        long auditID = -1;
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AID"])))
                        {
                            auditID = Convert.ToInt64(ViewState["AID"]);
                        }
                        else
                        {
                            auditID = Convert.ToInt64(Request.QueryString["AID"]);
                            ViewState["AID"] = auditID;
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SOID"])))
                        {
                            scheduleonID = Convert.ToInt64(ViewState["SOID"]);
                        }
                        else
                        {
                            scheduleonID = Convert.ToInt64(Request.QueryString["SOID"]);
                            ViewState["SOID"] = scheduleonID;
                        }
                        if (auditID != -1 && scheduleonID != -1)
                        {

                            var checkExist = (from row in entities.RLCS_VendorAuditScheduleOn
                                              where row.AuditID == auditID
                                              && row.ID == scheduleonID
                                              && row.IsActive == true
                                              select row).FirstOrDefault();
                            if (checkExist != null)
                            {
                                if (checkExist.AuditStatusID == 4)
                                {
                                    btnAddNewStep.Visible = false;
                                    btnSave.Visible = false;
                                    txtRecomandation.Visible = false;
                                    txtObservation.Visible = false;
                                    //  btnSaveClose.Visible = true;
                                }
                                else
                                {
                                    btnAddNewStep.Visible = true;
                                }
                                if (checkExist.AuditStatusID != 4)
                                {
                                    var totalchecklist = (from row in entities.RLCS_VendorAuditStepMapping
                                                          where row.AuditID == auditID
                                                          && row.IsActive == true
                                                          && row.VendorAuditScheduleOnID == scheduleonID
                                                          select row).ToList();

                                    totalchecklist = totalchecklist.GroupBy(A => A.CheckListID).Select(aa => aa.FirstOrDefault()).ToList();


                                    var closedchecklist = (from row in entities.RLCS_VendorAuditTransaction
                                                           where row.AuditID == auditID
                                                           && row.VendorAuditScheduleOnID == scheduleonID
                                                           && row.StatusId == 4
                                                           select row).ToList();

                                    closedchecklist = closedchecklist.GroupBy(A => A.CheckListId).Select(aa => aa.FirstOrDefault()).ToList();
                                    if (totalchecklist.Count == closedchecklist.Count)
                                    {
                                        btnSaveClose.Visible = true;
                                        txtRecomandation.Visible = true;
                                        txtObservation.Visible = true;
                                        btnSave.Visible = false;
                                    }
                                    else
                                    {
                                        btnSave.Visible = true;
                                        btnSaveClose.Visible = false;
                                        txtRecomandation.Visible = true;
                                        txtObservation.Visible = true;
                                    }

                                    if (TotalChecklistCount == TotalChecklistCloseCount)
                                    {
                                        btnSaveClose.Visible = true;
                                        txtRecomandation.Visible = true;
                                        txtObservation.Visible = true;
                                        btnSave.Visible = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    btnSave.Visible = false;
                    btnSaveClose.Visible = false;
                    txtRecomandation.Visible = false;
                    txtObservation.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long scheduleonID = -1;
                    long auditID = -1;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AID"])))
                    {
                        auditID = Convert.ToInt64(ViewState["AID"]);
                    }
                    else
                    {
                        auditID = Convert.ToInt64(Request.QueryString["AID"]);
                        ViewState["AID"] = auditID;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SOID"])))
                    {
                        scheduleonID = Convert.ToInt64(ViewState["SOID"]);
                    }
                    else
                    {
                        scheduleonID = Convert.ToInt64(Request.QueryString["SOID"]);
                        ViewState["SOID"] = scheduleonID;
                    }
                    if (auditID != -1 && scheduleonID != -1)
                    {
                        var totalchecklist = (from row in entities.RLCS_VendorAuditStepMapping
                                              where row.AuditID == auditID
                                              && row.IsActive == true
                                              && row.VendorAuditScheduleOnID == scheduleonID
                                              select row).ToList();

                        totalchecklist = totalchecklist.GroupBy(A => A.CheckListID).Select(aa => aa.FirstOrDefault()).ToList();


                        var closedchecklist = (from row in entities.RLCS_VendorAuditTransaction
                                               where row.AuditID == auditID
                                               && row.VendorAuditScheduleOnID == scheduleonID
                                               && row.StatusId == 4
                                               select row).ToList();

                        closedchecklist = closedchecklist.GroupBy(A => A.CheckListId).Select(aa => aa.FirstOrDefault()).ToList();

                        if (totalchecklist.Count == closedchecklist.Count)
                        {
                            var checkExist = (from row in entities.RLCS_VendorAuditScheduleOn
                                              where row.ID == scheduleonID
                                              && row.IsActive == true
                                              select row).FirstOrDefault();
                            if (checkExist != null)
                            {
                                checkExist.AuditStatusID = 4;
                                checkExist.Recommendation = txtRecomandation.Text;
                                checkExist.Observation = txtObservation.Text;
                                entities.SaveChanges();

                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Audit Closed Sucessfully.";
                                btnSaveClose.Visible = false;
                                btnAddNewStep.Visible = false;
                                txtRecomandation.Visible = true;
                                txtObservation.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Ohh..! something went wrong. Please try after some time.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }//

       
        private void StepBind(Saplin.Controls.DropDownCheckBoxes DrpNewStepCheck)
        {
            try
            {
                long scheduleonID = -1;
                long auditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AID"])))
                {
                    auditID = Convert.ToInt64(ViewState["AID"]);
                }
                else
                {
                    auditID = Convert.ToInt64(Request.QueryString["AID"]);
                    ViewState["AID"] = auditID;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SOID"])))
                {
                    scheduleonID = Convert.ToInt64(ViewState["SOID"]);
                }
                else
                {
                    scheduleonID = Convert.ToInt64(Request.QueryString["SOID"]);
                    ViewState["SOID"] = scheduleonID;
                }
                if (auditID != -1 && scheduleonID != -1)
                {
                    DrpNewStepCheck.DataTextField = "Name";
                    DrpNewStepCheck.DataValueField = "ID";
                    DrpNewStepCheck.DataSource = FillNewSteps(auditID, scheduleonID);
                    DrpNewStepCheck.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        public static object FillNewSteps(long AID, long SOID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var getvendorid = (from row in entities.RLCS_VendorAuditAssignment
                                   where row.AuditId == AID && row.RoleID==3
                                   select row.UserID).FirstOrDefault();

                var query = (from row in entities.Sp_RLCS_VendorNewStep(AID, SOID, getvendorid)                           
                             select row);
                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }        
        protected void ddlNewStepCheck_SelcetedIndexChanged(object sender, EventArgs e)
        {
            Saplin.Controls.DropDownCheckBoxes DrpNewStepCheck = (DropDownCheckBoxes)sender;
            NewStepID.Clear();
            for (int i = 0; i < DrpNewStepCheck.Items.Count; i++)
            {
                if (DrpNewStepCheck.Items[i].Selected)
                {
                    NewStepID.Add(Convert.ToInt64(DrpNewStepCheck.Items[i].Value));
                }
            }
        }
        public static bool CreateVendorAuditStepMapping(RLCS_VendorAuditStepMapping objRVASM)
        {           
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.RLCS_VendorAuditStepMapping.Add(objRVASM);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
               return  false;
            }
        }
        protected void btnPopupSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool savesucess = false;
                long scheduleonID = -1;
                long auditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AID"])))
                {
                    auditID = Convert.ToInt64(ViewState["AID"]);
                }
                else
                {
                    auditID = Convert.ToInt64(Request.QueryString["AID"]);
                    ViewState["AID"] = auditID;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SOID"])))
                {
                    scheduleonID = Convert.ToInt64(ViewState["SOID"]);
                }
                else
                {
                    scheduleonID = Convert.ToInt64(Request.QueryString["SOID"]);
                    ViewState["SOID"] = scheduleonID;
                }
                if (auditID != -1 && scheduleonID != -1)
                {
                    NewStepID.Clear();
                    for (int i = 0; i < ddlNewStepCheck.Items.Count; i++)
                    {
                        if (ddlNewStepCheck.Items[i].Selected)
                        {
                            NewStepID.Add(Convert.ToInt64(ddlNewStepCheck.Items[i].Value));
                        }
                    }
                    if (NewStepID.Count > 0)
                    {
                        foreach (var aItem in NewStepID)
                        {
                            RLCS_VendorAuditStepMapping RVASM = new RLCS_VendorAuditStepMapping()
                            {
                                AuditID = auditID,
                                VendorAuditScheduleOnID = scheduleonID,
                                CheckListID = aItem,
                                IsActive = true,
                            };
                            savesucess= CreateVendorAuditStepMapping(RVASM);
                        }
                        NewStepID.Clear();
                    }
                    if (savesucess)
                    {
                        cvpopup.IsValid = false;
                        cvpopup.ErrorMessage = "Checklist added to audit sucessfully";
                        BindAuditCheckList();
                        StepBind(ddlNewStepCheck);
                        Upvendorchecklist.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnMusterRoll_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AID"])
                          && !string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    int UserID = AuthenticationHelper.UserID;
                    int AuditID = Convert.ToInt32(Request.QueryString["AID"].ToString());
                    int ScheduleOnID = Convert.ToInt32(Request.QueryString["SOID"].ToString());
                    if (MusterRollFileUpload != null)
                    {
                        if (MusterRollFileUpload.HasFile)
                        {
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                            List<RLCSVendorFileData> files = new List<RLCSVendorFileData>();
                            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                            HttpFileCollection fileCollection = Request.Files;
                            bool blankfileCount = true;
                            if (fileCollection.Count > 0)
                            {
                                string directoryPath = null;
                                string version = null;
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\RLCSVendorDocuments\\" + UserID + "\\" + AuditID.ToString() + "\\" + ScheduleOnID + "\\" + version;
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/RLCSVendorDocuments/" + UserID + "/" + AuditID.ToString() + "/" + ScheduleOnID + "/" + version);
                                }
                                DocumentManagement.CreateDirectory(directoryPath);
                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = fileCollection[i];

                                    string[] keys = fileCollection.Keys[i].Split('$');
                                    String fileName = "";

                                    fileName = uploadfile.FileName;
                                    list.Add(new KeyValuePair<string, int>(fileName, 1));

                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                    if (uploadfile.ContentLength > 0)
                                    {
                                        string filepathvalue = string.Empty;
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale.Replace(@"\", "/");
                                        }
                                        else
                                        {
                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        }
                                        RLCSVendorFileData file = new RLCSVendorFileData()
                                        {
                                            Name =  fileName ,// ddlDocumentType.SelectedItem.ToString(),
                                            FilePath = filepathvalue,
                                            FileKey = fileKey.ToString(),
                                            Version = version,
                                            CreatedOn = DateTime.Now,
                                            FileSize = uploadfile.ContentLength.ToString()
                                        };
                                        files.Add(file);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(uploadfile.FileName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            bool flag = false;
                            if (blankfileCount)
                            {
                                flag = RLCSVendorMastersManagement.MusterRollFileUpload(Convert.ToInt64(AuditID), Convert.ToInt64(ScheduleOnID), files, list, Filelist);

                                if (flag)
                                {
                                   // cvDuplicateEntry.IsValid = false;
                                   // cvDuplicateEntry.ErrorMessage = "File Upload Successfully.";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "SuccessAlert", "alert('File Upload Successfully.')", true);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showUploadDocumentModal();", true);
                                }
                                else
                                {
                                   // cvDuplicateEntry.IsValid = false;
                                  //  cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "SuccessAlert", "alert('Please do not upload virus file or blank files.')", true);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showUploadDocumentModal();", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showUploadDocumentModal();", true);
                                //cvDuplicateEntry.IsValid = false;
                                //cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            }
                        }
                        else
                        {
                            //cvDuplicateEntry.IsValid = false;
                            //cvDuplicateEntry.ErrorMessage = "Please attached at least one file.";
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "SuccessAlert", "alert('Please attached at least one file.')", true);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showUploadDocumentModal();", true);
                        }
                    }
                   BindGrid();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (RLCSVendorMastersManagement.DeleteRLCSVendorFile(FileID, Convert.ToInt32(AuthenticationHelper.UserID)))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);
                            BindGrid();
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            RLCSVendorFileData file = RLCSVendorMastersManagement.GetRLCSVendorFile(FileID);
                            if (file != null)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(file.Name, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=VendorAuditDocumnet.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                        }
                        BindGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }

        public void BindGrid()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    int AuditID = Convert.ToInt32(Request.QueryString["AID"].ToString());
                    long ScheduleOnID = Convert.ToInt64(Request.QueryString["SOID"].ToString());
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstChecklistDetails = RLCSVendorMastersManagement.GetMustorDocumentData(Convert.ToInt64(AuditID), Convert.ToInt64(ScheduleOnID));
                        if (lstChecklistDetails.Count > 0)
                        {
                          
                            ViewState["DocumentListCount"] = lstChecklistDetails.Count;
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                        }
                        else
                        {
                            ViewState["DocumentListCount"] = 0;
                            grdDocument.DataSource = lstChecklistDetails;
                            grdDocument.DataBind();
                        }
                        lstChecklistDetails.Clear();
                        lstChecklistDetails = null;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long scheduleonID = -1;
                    long auditID = -1;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AID"])))
                    {
                        auditID = Convert.ToInt64(ViewState["AID"]);
                    }
                    else
                    {
                        auditID = Convert.ToInt64(Request.QueryString["AID"]);
                        ViewState["AID"] = auditID;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SOID"])))
                    {
                        scheduleonID = Convert.ToInt64(ViewState["SOID"]);
                    }
                    else
                    {
                        scheduleonID = Convert.ToInt64(Request.QueryString["SOID"]);
                        ViewState["SOID"] = scheduleonID;
                    }
                    if (auditID != -1 && scheduleonID != -1)
                    {
                            var checkExist = (from row in entities.RLCS_VendorAuditScheduleOn
                                              where row.ID == scheduleonID
                                              && row.IsActive == true
                                              select row).FirstOrDefault();
                            if (checkExist != null)
                            {
                                checkExist.Recommendation = txtRecomandation.Text;
                                checkExist.Observation = txtObservation.Text;
                                entities.SaveChanges();

                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Recommendation and Observation Save Sucessfully.";
                            }
                        
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Ohh..! something went wrong. Please try after some time.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}