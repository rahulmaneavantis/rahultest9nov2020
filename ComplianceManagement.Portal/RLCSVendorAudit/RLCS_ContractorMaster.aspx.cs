﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit
{
    public partial class RLCS_ContractorMaster : System.Web.UI.Page
    {
        protected static string user_Roles;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                user_Roles = AuthenticationHelper.Role;

            }
        }

        [WebMethod]
        public static string BindContractorList(ContractDetails DetailsObj)
        {
            try
            {

               
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                   
                        var ContractorObj = (from Contractor in entities.RLCS_VendorContractorMaster
                                             where Contractor.ClientID == DetailsObj.ClientID
                                             select Contractor).ToList();
                        foreach (var row in ContractorObj)
                        {
                            ContractDetails Details = new ContractDetails();
                            Details.ID = row.ID;
                            Details.ContractorID = row.ContractorID;
                            Details.ContractorName = row.ContractorName;
                            Details.SPOCName = row.SPOCName;
                            Details.SPOCEmailID = row.SPOCEmailID;
                            Details.SPOCMobileNo = row.SPOCMobileNo;
                            ContractorDetailsList.Add(Details);

                        }
                   

                    return serializer.Serialize(ContractorDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string SaveContractDetails(ContractDetails DetailsObj)
        {
            try
            {
                bool Success = false;
                ReturnDetails RtrnMsg = new ReturnDetails();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    int id = Convert.ToInt32(DetailsObj.ID);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        RLCS_VendorContractorMaster updateDetails = (from row in entities.RLCS_VendorContractorMaster
                                                                     where row.ID == id
                                                                     select row).FirstOrDefault();

                        if (updateDetails != null)
                        {
                            #region Update Details
                            if (CheckDuplicateContractor(DetailsObj.ClientID, DetailsObj.SPOCEmailID, DetailsObj.ContractorName))
                            {
                                updateDetails.ContractorID = DetailsObj.ContractorID;
                                updateDetails.ContractorName = DetailsObj.ContractorName;
                                updateDetails.SPOCName = DetailsObj.SPOCName;
                                updateDetails.SPOCEmailID = DetailsObj.SPOCEmailID;
                                updateDetails.SPOCMobileNo = DetailsObj.SPOCMobileNo;

                                entities.SaveChanges();

                                if (CheckDuplicateMailID(DetailsObj.SPOCEmailID))
                                {
                                    User UserMaster = new User()
                                    {
                                        FirstName = DetailsObj.SPOCName,
                                        LastName = "",
                                        Email = DetailsObj.SPOCEmailID,
                                        Password = "Bqsw1r6xd7UB3NUACDGDVw==",
                                        ContactNumber = "0000000000",
                                        IsDeleted = false,
                                        CreatedBy = 1,
                                        CreatedByText = "Admin",
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        RoleID = 23,
                                        LastLoginTime = DateTime.Now,
                                        VendorRoleID = 23,
                                        EnType = "A"
                                    };
                                    entities.Users.Add(UserMaster);
                                    entities.SaveChanges();
                                    RtrnMsg.Message = "Contractor Update Successfully.";
                                    RtrnMsg.Success = true;
                                    Success = true;
                                }
                                else
                                {
                                    User UserObj = (from row in entities.Users
                                                    where row.Email == DetailsObj.SPOCEmailID
                                                    select row).FirstOrDefault();
                                    if (UserObj != null)
                                    {
                                        UserObj.FirstName = DetailsObj.SPOCName;
                                        entities.SaveChanges();
                                        Success = true;
                                        RtrnMsg.Message = "Contractor Update Successfully.";
                                        RtrnMsg.Success = true;
                                    }
                                }
                            }
                            else
                            {
                                updateDetails.SPOCName = DetailsObj.SPOCName;
                                updateDetails.SPOCMobileNo = DetailsObj.SPOCMobileNo;
                                entities.SaveChanges();
                                RtrnMsg.Message = "Contractor Update Successfully.";
                                RtrnMsg.Success = true;

                            }
                            #endregion


                        }
                        else
                        {
                            #region add new detail
                            if (CheckDuplicateContractor(DetailsObj.ClientID,DetailsObj.SPOCEmailID,DetailsObj.ContractorName))
                            {
                                RLCS_VendorContractorMaster ContractorMaster = new RLCS_VendorContractorMaster()
                                {
                                    ContractorID = DetailsObj.ContractorID,
                                    ContractorName = DetailsObj.ContractorName,
                                    SPOCName = DetailsObj.SPOCName,
                                    SPOCMobileNo = DetailsObj.SPOCMobileNo,
                                    SPOCEmailID = DetailsObj.SPOCEmailID,
                                    ClientID = DetailsObj.ClientID

                                };
                                entities.RLCS_VendorContractorMaster.Add(ContractorMaster);
                                entities.SaveChanges();
                                if (CheckDuplicateMailID(DetailsObj.SPOCEmailID))
                                {
                                    User UserMaster = new User()
                                    {
                                        FirstName = DetailsObj.SPOCName,
                                        LastName = "",
                                        Email = DetailsObj.SPOCEmailID,
                                        Password = "Bqsw1r6xd7UB3NUACDGDVw==",
                                        ContactNumber = "0000000000",
                                        IsDeleted = false,
                                        CreatedBy = 1,
                                        CreatedByText = "Admin",
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        RoleID = 23,
                                        LastLoginTime = DateTime.Now,
                                        VendorRoleID = 23,
                                        EnType = "A"
                                    };
                                    entities.Users.Add(UserMaster);
                                    entities.SaveChanges();
                                    Success = true;
                                    RtrnMsg.Message = "Contractor Save Successfully.";
                                    RtrnMsg.Success = true ;
                                }
                                else
                                {
                                    User UserObj = (from row in entities.Users
                                                    where row.Email == DetailsObj.SPOCEmailID
                                                    select row).FirstOrDefault();
                                    if (UserObj != null)
                                    {
                                        UserObj.FirstName = DetailsObj.SPOCName;
                                        entities.SaveChanges();
                                        Success = true;
                                        RtrnMsg.Message = "Contractor Save Successfully.";
                                        RtrnMsg.Success = true;
                                    }
                                }
                            }
                            else
                            {
                                RtrnMsg.Message = "Contractor Name or Mailid Already Exist";
                                RtrnMsg.Success = false;
                            }
                           
                            #endregion
                        }

                        return serializer.Serialize(RtrnMsg);
                    }
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindContractor(ContractDetails DetailsObj)
        {
            try
            {
                bool Success = false;
                int id = Convert.ToInt32(DetailsObj.ID);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (id > 0)
                    {
                        var ContractorObj = (from Contractor in entities.RLCS_VendorContractorMaster
                                             where Contractor.ID == id && Contractor.ClientID== DetailsObj.ClientID
                                             select Contractor).ToList();
                        foreach (var row in ContractorObj)
                        {
                            ContractDetails Details = new ContractDetails();
                            Details.ID = row.ID;
                            Details.ContractorID = row.ContractorID;
                            Details.ContractorName = row.ContractorName;
                            Details.SPOCName = row.SPOCName;
                            Details.SPOCEmailID = row.SPOCEmailID;
                            Details.SPOCMobileNo = row.SPOCMobileNo;

                            ContractorDetailsList.Add(Details);
                        }
                    }
                    else
                    {
                        var ContractorObj = (from Contractor in entities.RLCS_VendorContractorMaster
                                             where  Contractor.ClientID == DetailsObj.ClientID
                                             select Contractor).ToList();
                        foreach (var row in ContractorObj)
                        {
                            ContractDetails Details = new ContractDetails();
                            Details.ID = row.ID;
                            Details.ContractorID = row.ContractorID;
                            Details.ContractorName = row.ContractorName;
                            Details.SPOCName = row.SPOCName;
                            Details.SPOCEmailID = row.SPOCEmailID;
                            Details.SPOCMobileNo = row.SPOCMobileNo;

                            ContractorDetailsList.Add(Details);
                        }
                    }



                    return serializer.Serialize(ContractorDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string GetContractorDetails(ContractDetails DetailsObj)
        {
            try
            {
                bool Success = false;

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ContractDetails> ContractorDetailsList = new List<ContractDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var ContractorObj = (from Contractor in entities.RLCS_VendorContractorMaster
                                         where Contractor.ID == DetailsObj.ID
                                         select Contractor).ToList();

                    foreach (var row in ContractorObj)
                    {
                        ContractDetails Details = new ContractDetails();
                        Details.ID = row.ID;
                        Details.ContractorID = row.ContractorID;
                        Details.ContractorName = row.ContractorName;
                        Details.SPOCName = row.SPOCName;
                        Details.SPOCEmailID = row.SPOCEmailID;
                        Details.SPOCMobileNo = row.SPOCMobileNo;

                        ContractorDetailsList.Add(Details);
                    }

                    return serializer.Serialize(ContractorDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindClientList()
        {
            try
            {
                
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ClientDetails> ClientDetailsList = new List<ClientDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var ClientObj = (from Client in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                         where Client.BranchType == "E"
                                         select Client).Distinct().ToList();
                    foreach (var row in ClientObj)
                    {
                        ClientDetails Details = new ClientDetails();
                        Details.ClientID = row.CM_ClientID;
                        Details.ClientName = row.CM_ClientName;
                        Details.CustomerID = row.AVACOM_CustomerID.ToString();
                        ClientDetailsList.Add(Details);
                    }

                    return serializer.Serialize(ClientDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        public static bool CheckDuplicateMailID(string MailId)
        {
            try
            {
                bool Success = true;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    User UserObj = (from row in entities.Users
                                    where row.Email == MailId
                                    select row).FirstOrDefault();

                    if (UserObj != null)
                    {
                        Success = false;
                    }
                    return Success;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CheckDuplicateContractor(string ClientID,string EmailID,string ContractorName)
        {
            try
            {
                bool Success = true;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    RLCS_VendorContractorMaster contractorObj = (from row in entities.RLCS_VendorContractorMaster
                                    where row.ClientID == ClientID && row.SPOCEmailID== EmailID && row.ContractorName == ContractorName
                                    select row).FirstOrDefault();

                    if (contractorObj != null)
                    {
                        Success = false;
                    }
                    return Success;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

    

        public class ContractDetails
        {
            public int ID { get; set; }
            public string ClientID { get; set; }
            public string ContractorID { get; set; }
            public string ContractorName { get; set; }
            public string SPOCName { get; set; }
            public string SPOCEmailID { get; set; }
            public string SPOCMobileNo { get; set; }

        }

        public class ClientDetails
        {
            public string ClientID { get; set; }
            public string CustomerID { get; set; }
            public string ClientName { get; set; }

        }

        public class ReturnDetails
        {
            public bool Success { get; set; }
            public string Message { get; set; }
        }

    }
}