﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSVendor.Master" AutoEventWireup="true" CodeBehind="RLCS_CreateAuditContract.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCS_CreateAuditContract" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../assets/select2.min.css" rel="stylesheet" />
    <script src="../assets/select2.min.js"></script>
    <link href="../NewCSS/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="../Newjs/bootstrap-datepicker.min.js"></script>
    <script src="../Newjs/parsley.js"></script>
    <script src="../Newjs/parsley.min.js"></script>

    <script type="text/javascript">
        var ContractorID = "";
        $(document).ready(function () {
            setactivemenu('leftnoticesmenu');
            fhead('Location Mapping');

            var ContractorDetails = getUrlVars();
            var ContractorName = ContractorDetails.ContractorName.replace(/%20/g, " ");
            var ClientName = ContractorDetails.ClientName.replace(/%20/g, " ");
            $("#txtRedirectContractorName").val(ContractorName);
            $("#txtContractorName").val(ContractorName);
            $("#txtRedirectClientName,#txtClientName").val(ClientName).attr("ClientID", ContractorDetails.ClientID);
            ContractorID = ContractorDetails.ContractorID;
            BindClientWiseContractor(ContractorDetails.ClientID);

            $('#NewContractorModal').on('shown.bs.modal', function (e) {
                BindStateList();
                BindPTStateList();
                BindAuditorList();
                SaveValidation();
                BindLocationList(0);
                BindBranchList(0);
                $("#lblAuditID").text("0");
            })
            $("#btnNew").on("click", function (e) {
                Clear();
                $("#NewContractorModal").modal("show");

            });

            $("#ddlState").on("change", function (e) {
                BindLocationList($(this).val());
            });

            $("#ddlLocation").on("change", function (e) {
                BindBranchList($("#ddlState").val());
            });

            $("#btnSave").click(function (e) {
                if (Validation()) {
                    debugger;
                    var DetailsObj = {
                        ID: $("#lblAuditID").text(),
                        CC_ClientID: $("#txtRedirectClientName").attr("ClientID"),
                        ContractorMasterID: ContractorID,
                        AVACOM_CustomerID: 0,
                        CC_ContractorName: $("#txtContractorName").val(),
                        CC_Frequency: $("#ddlFrequency").val(),
                        DueDate: $("#ddlDueDate").val(),
                        CC_StateID: $("#ddlState").val(),
                        CC_BranchID: $("#ddlBranch option:selected").text(),
                        AVACOM_BranchID: $("#ddlBranch").val(),
                        CC_ContractorAddress: $("#txtAddress").val(),
                        CC_ContractFrom: $("#data-date").val(),
                        CC_ContractTo: $("#data-date1").val(),
                        CC_Auditor: $("#ddlAuditor").val(),
                        CC_NatureOfBusiness: $("#txtNatureOfBusiness").val(),
                        CC_MaleHeadCount: $("#txtMaleHeadCount").val(),
                        CC_FeMaleHeadCount: $("#txtFemaleHeadCount").val(),
                        CC_Status: $("#ddlStatus").val(),
                        CC_ESIC_Number: $("#txtESICNumber").val(),
                        CC_PF_Code: $("#txtPFCode").val(),
                        CC_PT_State: $("#ddlPTState").val(),
                        CC_Location: $("#ddlLocation").val(),
                        DepartmentType: $("#ddlDepartment").val()
                    }

                    $.ajax({
                        type: "POST",
                        url: "./RLCS_CreateAuditContract.aspx/SaveContractDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var Success = JSON.parse(data.d);
                            debugger;
                            if (Success) {
                                alert("Contract Save Successfully.");
                                Clear();
                                $("#NewContractorModal").modal("hide");
                                BindClientWiseContractor($("#txtRedirectClientName").attr("ClientID"));
                            }

                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }

            });

            $("#btnUpload").click(function (e) {
                $("#UploadModal").modal("show");
            });

            $(document).on("click", "#tblClientWiseContract tbody tr .Update", function (e) {
                Clear();
                var item = $("#tblClientWiseContract").data("kendoGrid").dataItem($(this).closest("tr"));
                var DetailsObj = {
                    ID: item.ID
                }
                $("#NewContractorModal").modal("show");
                $.ajax({
                    type: "POST",
                    url: "./RLCS_CreateAuditContract.aspx/GetContractorAuditDetails",
                    data: JSON.stringify({ DetailsObj }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (data) {
                        var ContractorDetails = JSON.parse(data.d);

                        $("#lblAuditID").text(ContractorDetails.ID)
                        $("#txtClientName").val(ContractorDetails.CustomerName);
                        $("#txtContractorName").val(ContractorDetails.CC_ContractorName);
                        $("#ddlFrequency").val(ContractorDetails.CC_Frequency);
                        $("#ddlDueDate").val(ContractorDetails.DueDate);
                        $("#ddlState").val(ContractorDetails.CC_StateID).trigger('change');
                        debugger;
                        $("#ddlLocation").val(ContractorDetails.CC_Location).trigger('change');
                        $("#ddlBranch").val(ContractorDetails.AVACOM_BranchID).trigger('change');
                        $("#txtAddress").val(ContractorDetails.CC_ContractorAddress);
                        var StartdateTime = ContractorDetails.CC_ContractFrom;
                        var parts = StartdateTime.split(/[- : /]/);
                        var startday = (parts[1].length == 1 ? "0" + parts[1] : parts[1]);
                        var startmonth = (parts[0].length == 1 ? "0" + parts[0] : parts[0]);
                        var FromDate = `${startday}/${startmonth}/${parts[2]}`;
                        $("#data-date").val(FromDate);
                        var EnddateTime = ContractorDetails.CC_ContractTo;
                        var parts1 = EnddateTime.split(/[- : /]/);
                        var Endday = (parts1[1].length == 1 ? "0" + parts1[1] : parts1[1]);
                        var Endmonth = (parts1[0].length == 1 ? "0" + parts1[0] : parts1[0]);
                        var ToDate = `${Endday}/${Endmonth}/${parts1[2]}`;
                        $("#data-date1").val(ToDate);
                        $("#ddlAuditor").val(ContractorDetails.CC_Auditor).trigger('change');
                        $("#txtNatureOfBusiness").val(ContractorDetails.CC_NatureOfBusiness);
                        $("#txtMaleHeadCount").val(ContractorDetails.CC_MaleHeadCount);
                        $("#txtFemaleHeadCount").val(ContractorDetails.CC_FeMaleHeadCount);
                        $("#ddlStatus").val(ContractorDetails.CC_Status);
                        $("#txtESICNumber").val(ContractorDetails.CC_ESIC_Number);
                        $("#txtPFCode").val(ContractorDetails.CC_PF_Code);
                        $("#ddlPTState").val(ContractorDetails.CC_PT_State).trigger('change');
                        $("#ddlDepartment").val(ContractorDetails.DepartmentType);

                    },
                    failure: function (data) {
                        alert(data);
                    }
                });
            });

            $("#txtMaleHeadCount,#txtFemaleHeadCount").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    alert("Please Enter Only Number");
                    return false;
                }
            });
        });


        function BindStateList() {
            $.ajax({
                type: "POST",
                url: "./RLCS_CreateAuditContract.aspx/BindStateList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $("#ddlState").empty();
                        $("#ddlState").append($("<option></option>").val("").html("Select State"));
                        $.each(State, function (data, value) {
                            $("#ddlState").append($("<option></option>").val(value.Code).html(value.Name));
                        })
                        $("#ddlState").select2();
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindPTStateList() {
            $.ajax({
                type: "POST",
                url: "./RLCS_CreateAuditContract.aspx/BindStateList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var State = JSON.parse(data.d);
                    if (State.length > 0) {
                        $("#ddlPTState").empty();
                        $("#ddlPTState").append($("<option></option>").val("").html("Select PT State"));
                        $.each(State, function (data, value) {
                            $("#ddlPTState").append($("<option></option>").val(value.Code).html(value.Name));
                        })
                        $("#ddlPTState").select2();
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindLocationList(stateid) {
            var StateObj = {
                Code: stateid
            }
            $.ajax({
                type: "POST",
                url: "./RLCS_CreateAuditContract.aspx/BindLocationList",
                data: JSON.stringify({ StateObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var Location = JSON.parse(data.d);
                    $("#ddlLocation").empty();
                    $("#ddlLocation").append($("<option></option>").val("-1").html("Select Location"));
                    $.each(Location, function (data, value) {
                        $("#ddlLocation").append($("<option></option>").val(value.Code).html(value.Name));
                    })
                    $("#ddlLocation").select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindBranchList(StateID) {
            var BranchObj = {
                ClientID: $("#txtRedirectClientName").attr("ClientID"),
                Code: StateID,
                LocationCode: $("#ddlLocation").val()
            }
            $.ajax({
                type: "POST",
                url: "./RLCS_CreateAuditContract.aspx/BindBranchList",
                data: JSON.stringify({ BranchObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var Location = JSON.parse(data.d);
                    $("#ddlBranch").empty();
                    $("#ddlBranch").append($("<option></option>").val("-1").html("Select Branch"));
                    $.each(Location, function (data, value) {
                        $("#ddlBranch").append($("<option></option>").val(value.ID).html(value.Name));
                    })
                    $("#ddlBranch").select2();
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindAuditorList() {
            $.ajax({
                type: "POST",
                url: "./RLCS_CreateAuditContract.aspx/BindAuditorList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var Customer = JSON.parse(data.d);
                    if (Customer.length > 0) {
                        $("#ddlAuditor").empty();
                        $("#ddlAuditor").append($("<option></option>").val("-1").html("Select Auditor"));
                        $.each(Customer, function (data, value) {
                            $("#ddlAuditor").append($("<option></option>").val(value.ID).html(value.Name));
                        })
                        $("#ddlAuditor").select2();
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindClientWiseContractor(id) {
            var DetailsObj = {
                CC_ClientID: id,
                ContractorMasterID: ContractorID
            }
            $.ajax({
                type: "POST",
                url: "./RLCS_CreateAuditContract.aspx/BindClientWiseContractor",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var AssignCheckList = JSON.parse(data.d);
                    //if (AssignCheckList.length > 0) {
                    $("#tblClientWiseContract").kendoGrid({
                        dataSource: {
                            data: AssignCheckList
                        },
                        height: 430,
                        sortable: true,
                        filterable: false,
                        columnMenu: true,
                        reorderable: true,
                        resizable: true,
                        columns: [
                            { hidden: true, field: "ID" },
                            { field: "StateName", title: "State Name", width: "15%" },
                            { field: "LocationName", title: "Location Name", width: "30%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "BranchName", title: "Branch Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "AuditorName", title: "Auditor Name", width: "20%" },
                            { field: "NatureOfService", title: "Nature Of Service", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                             { field: "Status", title: "Status", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                             {
                                 title: "Action", lock: true, width: "20%;",
                                 command: [{ name: "UpdateContractor", text: "Update", className: "Update" }
                                 ],
                             }
                        ]
                    });
                    $("#tblClientWiseContract").kendoTooltip({
                        filter: ".k-ScheduleAudit",
                        content: function (e) {
                            return "";
                        }
                    });
                    //}


                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        function SaveValidation() {
            if ($('#form1').parsley().validate())
                return true;
            else
                return false;
        }

        function Validation() {
            debugger;
            if ($("#ddlFrequency").val() == "-1") {
                alert("Please select Frequency");
                return false;
            }
            else if ($("#ddlState").val() == "") {
                alert("Please select State");
                return false;
            }
            else if ($("#ddlLocation").val() == "-1") {
                alert("Please select Location");
                return false;
            }
            else if ($("#ddlBranch").val() == "-1") {
                alert("Please select Branch");
                return false;
            }
            else if ($.trim($("#txtAddress").val()) == "") {
                alert("Please Enter Address");
                return false;
            }
            else if ($("#data-date").val() == "") {
                alert("Please select From Date");
                return false;
            }
            else if ($("#data-date1").val() == "") {
                alert("Please select To Date");
                return false;
            }
            else if ($("#ddlAuditor").val() == "-1") {
                alert("Please select Auditor");
                return false;
            }
            else if ($.trim($("#txtNatureOfBusiness").val()) == "") {
                alert("Please Enter Nature Of Business");
                return false;
            }
            else if ($.trim($("#txtMaleHeadCount").val()) == "") {
                alert("Please Enter Male Count");
                return false;
            }
            else if ($.trim($("#txtFemaleHeadCount").val()) == "") {
                alert("Please Enter Female Count");
                return false;
            }
            else {
                return true;
            }
        }

        function Clear() {
            $("#ddlFrequency").val("-1");
            $("#ddlDueDate").val("1");
            $("#ddlState").val("-1");
            $("#ddlBranch").val("-1");
            $("#txtAddress").val("");
            $("#data-date").val("");
            $("#data-date1").val("");
            $("#ddlAuditor").val("-1");
            $("#txtNatureOfBusiness").val("");
            $("#txtMaleHeadCount").val("");
            $("#txtFemaleHeadCount").val("");
            $("#ddlStatus").val("A");
            $("#txtESICNumber").val("");
            $("#txtPFCode").val("");
            $("#ddlPTState").val("-1");
            $("#ddlDepartment").val("-1");
            $("#lblAuditID").text("0");
        }

        function ExcelPopupShow() {
            $("#UploadModal").modal("show");
        }

    </script>
    <style>
        .select2-dropdown {
            background-color: #ffffff;
            color: #151010;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }


        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #c7c7cc;
        }

        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 34px;
            user-select: none;
            -webkit-user-select: none;
        }

        .parsley-errors-list {
            display: none;
        }

        #parsley-id-13 {
            margin-bottom: -40px;
        }

        .form-validate .form-group input.error, .form-validate .form-group textarea.error, input:focus:invalid:focus, select:focus:invalid:focus, textarea:focus:invalid:focus {
            border-color: red !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                    <input id="txtRedirectClientName" type="text" class="form-control" readonly />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                    <input id="txtRedirectContractorName" type="text" class="form-control" readonly />
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                    <button id="btnNew" type="button" class="btn btn-primary">Add New Contract</button>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                    <button id="btnUpload" type="button" class="btn btn-primary">Upload Contract</button>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                    <div id="tblClientWiseContract"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal  modal-fullscreen" id="NewContractorModal" role="dialog">
        <div class="modal-dialog modal-full" style="width: 100%;">
            <!-- Modal content-->
            <div class="modal-content" id="form1">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Contractor</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Client<span style="color: red">*</span></label>
                                <input id="txtClientName" type="text" class="form-control" readonly />
                                <label class="hidden" id="lblAuditID"></label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Contractor Name<span style="color: red">*</span></label>
                                <input type="text" id="txtContractorName" class="form-control" readonly />
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 form-group">
                                <label style="color: black;" errormessage="Please Select Frequency">Frequency<span style="color: red">*</span></label>
                                <select id="ddlFrequency" class="form-control">
                                    <option value="-1">Select Frequency</option>
                                    <option value="M">Monthly</option>
                                    <option value="Q">Quarterly</option>
                                    <option value="H">Half Yearly</option>
                                    <option value="A">Yearly</option>
                                    <option value="O">On Occurance</option>
                                </select>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 form-group">
                                <label style="color: black;">Due Date<span style="color: red">*</span></label>
                                <select id="ddlDueDate" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Select State<span style="color: red">*</span></label>
                                <select id="ddlState" class="form-control" errormessage="Please Select State"></select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Select Location<span style="color: red">*</span></label>
                                <select id="ddlLocation" class="form-control"></select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Select Branch<span style="color: red">*</span></label>
                                <select id="ddlBranch" class="form-control"></select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;">Address<span style="color: red">*</span></label>
                                <textarea id="txtAddress" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Contract From<span style="color: red">*</span></label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="data-date">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Contract To<span style="color: red">*</span></label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="data-date1">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <label style="color: black;">Select Auditor<span style="color: red">*</span></label>
                                <select id="ddlAuditor" class="form-control"></select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Nature Of Business<span style="color: red">*</span></label>
                                <input id="txtNatureOfBusiness" type="text" class="form-control" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Male Head Count<span style="color: red">*</span></label>
                                <input id="txtMaleHeadCount" type="text" class="form-control" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Female Head Count<span style="color: red">*</span></label>
                                <input id="txtFemaleHeadCount" type="text" class="form-control" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Status</label>
                                <select id="ddlStatus" class="form-control">
                                    <option value="A">Active</option>
                                    <option value="I">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">ESIC Number</label>
                                <input id="txtESICNumber" type="text" class="form-control" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">PF Code</label>
                                <input id="txtPFCode" type="text" class="form-control" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Select PT State</label>
                                <select id="ddlPTState" class="form-control"></select>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Select Department</label>
                                <select id="ddlDepartment" class="form-control">
                                    <option value="-1">Select Department</option>
                                    <option value="1">Admin</option>
                                    <option value="2">HR</option>
                                    <option value="3">Support</option>
                                    <option value="4">Finance</option>
                                    <option value="5">IT</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnSave" type="button" value="Submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal  modal-fullscreen" id="UploadModal" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload Excel</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                                    class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" />
                                <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                    ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <asp:FileUpload ID="ContractFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <asp:Button runat="server" ID="btnUploadExcel" CssClass="btn btn-primary" Text="Upload" OnClick="btnUploadExcel_Click" />
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 form-group">
                                <a href="SampleFormat/ContractBulkUpload.xlsx">Download Sample Format</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
