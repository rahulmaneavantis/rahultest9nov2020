﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   

  <%-- <link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
      <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>
     <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;

      
    }
     .img1{
        border:ridge;
        border-color:lightgrey;
    }
     .MainContainer
    {
       
        /*height:1000px;*/
    }
    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
  width:1200px;


  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
  /*background-color:#1fd9e1;*/
}

* {
  box-sizing: border-box;
}

    </style>
 
      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
      
       <div class="row content"> 

           <div class="col-sm-3 sidenav">
                     
                    

                 <ul class="nav nav-pills nav-stacked" >
                    <li ><a href="../HelpManagement/dashboard.aspx" style="color:#333;font-size:16px"><b>Management Dashboard</b></a></li>
                        <li ><a href="../NewMgmtHelp.aspx" style="color:#333"><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                        
                                <li ><a href="../HelpManagement/Entities.aspx" style="color:#333" > Entities</a></li>
                                <li><a href="../HelpManagement/Location.aspx" style="color:#333">Location</a></li>
                                <li><a href="../HelpManagement/Categories.aspx" style="color:#333">Categories</a></li>
                                <li> <a href="../HelpManagement/Compliances.aspx" style="color:#333">Compliances</a></li>
                                <li> <a href="../HelpManagement/Users.aspx" style="color:#333">Users</a></li>
                                <li> <a href="../HelpManagement/Penalty.aspx" style="color:#333">Penalty</a></li>
                                <li><a href="../HelpManagement/OverdueSummary.aspx" style="color:#333">Summary of Overdue Compliances</a></li>
                                <li> <a href="../HelpManagement/PerformanceSummary.aspx" style="color:#333">Performance Summary</a></li>
                                <li> <a href="../HelpManagement/RiskSummary.aspx" style="color:#333">Risk Summary</a></li>
                                <li> <a href="../HelpManagement/PenaltySummary.aspx" style="color:#333">Penalty Summary</a></li>
                                <li> <a href="../HelpManagement/GradingReport.aspx" style="color:#333">Grading Reports</a></li>
                                <li><a href="../HelpManagement/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpManagement/DailyUpdates.aspx"style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpManagement/Newsletter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li ><a href="../HelpManagement/Reports.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li ><a href="../HelpManagement/Documents.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li ><a href="../HelpManagement/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>
                       
                      
                     </ul>
            <br/>
                
           </div>

           <div class="col-sm-9 MainContainer" style="line-height:25px;border:groove;border-color:#f1f3f4">
            <h3>My Reports</h3>
              <b style="font-size:16px;">Standard Report</b>
              <p>This section features six types of Standard reports. This standard report is generated on weekly basis. Following reports can be downloaded </p>
                 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

	             	<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Overall Summary Report</b>
					</a>
				</h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
					 
                <p>This dynamic report shows the overall summary of  compliances completion status.  The completion status can be viewed based on locations, categories, users, risk profiles, along with details of overdue compliances.</p>
               <ul>
                   <li style="list-style-type:decimal">To download you need to select Standard Report in My Report. </li>
                    <li style="list-style-type:decimal">You will be redirected to page that holds all standard report, here in Overall Summary Report section click on download icon for the week you want report. These reports can be bifurcated with compliance type (Statutory and Internal).  </li>
                  <li style="list-style-type:decimal">Below window will appear, you can set filter for specific results</li>
            
                    <img  class="img1" style="width: 600px;margin-left:70px; height: 250px" src="../perfreviewerUpdatedscreens/My%20Report_Overall%20Summary%20Report_Screenshot_1.png" /><br />
                    <li style="list-style-type:decimal">The show more link populates the report of older weeks.</li><br />
                   <img  class="img1" style="width: 600px;margin-left:70px; height: 250px" src="../perfreviewerUpdatedscreens/My%20Report_Overall%20Summary%20Report_Screenshot._2.png" /><br />
                      <li style="list-style-type:decimal">To View overall report of last 18 months till current date Click on following icon.</li><br />    
                   <img  class="img1" style="width: 600px;margin-left:70px; height: 250px" src="../perfreviewerUpdatedscreens/My%20Report_Overall%20Summary%20Report_Screenshot._3.png" />
                  

                 <%-- <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_Overall%20Summary%20Report_Screenshot.png" />
              --%> </ul>
                </div>
                
			</div>

	</div>

                    <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTwo">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Location Summary Report</b>
					</a>
				</h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				<div class="panel-body">
					<p>View the status of your compliances segregated for each of your locations across categories, risk profiles, and users, along with details of ageing of overdue compliances by location.</p> 
                    <ul>
                         <li style="list-style-type:decimal">To download you need to select Standard Report in My Report. </li>
                        <img  class="img1" style="width: 600px;margin-left:70px; height: 250px"src="../perfreviewerUpdatedscreens/My%20Report_Location%20Summary%20Report%20_Screenshot_1.png" />
                         <li style="list-style-type:decimal">You will be redirected to page that holds all standard report, here in Location Summary Report section click on download icon for the week you want report. </li>
                        <img  class="img1" style="width: 600px;margin-left:70px; height: 250px"  src="../perfreviewerUpdatedscreens/My%20Report_Location%20Summary%20Report%20_Screenshot_2.png" />
                         <li style="list-style-type:decimal">The show more link populates the report of older weeks.</li><br />
                        <img  class="img1" style="width: 600px;margin-left:70px; height: 250px"  src="../perfreviewerUpdatedscreens/My%20Report_Location%20Summary%20Report%20_Screenshot_3.png" />
                         <%-- <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_Location%20Summary%20Report%20_Screenshot.png" />
                       --%>
                    </ul>   
               </div>

			</div>

	</div>

                    <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingThree">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>User Summary Report</b>
					</a>
				</h4>
			</div>
			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				<div class="panel-body">
				<p>This report shows the status of your compliances managed by each one of your performers and reviewers across categories, risk profiles, and locations, along with details of ageing of overdue compliances.</p>	 
                <ul>
                    <li style="list-style-type:decimal">To download you need to select Standard Report in My Report. </li>
                     <li style="list-style-type:decimal">You will be redirected to page that holds all standard report, here in Location Summary Report section click on download icon for the week you want report. These reports can be bifurcated with compliance type (Statutory and Internal).</li>
                       <li style="list-style-type:decimal">The show more link populates the report of older weeks.</li><br /><br />
                    <img  class="img1" style="width: 600px;margin-left:70px; height: 250px"   src="../perfreviewerUpdatedscreens/My%20Report_User%20Summary%20Report_Screenshot_1.png" /><br /><br />
                        <li style="list-style-type:decimal">To View overall report of last 18 months till current date Click on following icon. </li>            
                     <img  class="img1" style="width: 600px;margin-left:70px; height: 250px"   src="../perfreviewerUpdatedscreens/My%20Report_User%20Summary%20Report_Screenshot_2.png" /><br /><br />
                    <li style="list-style-type:decimal">Below window will appear, you can set filter for specific results</li>
                    <img  class="img1" style="width: 600px;margin-left:70px; height: 250px"   src="../perfreviewerUpdatedscreens/My%20Report_User%20Summary%20Report_Screenshot_3.png" />
                      <%-- <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_User%20Summary%20Report_Screenshot.png" />
                  --%>  
                </ul>
                
                </div>

			</div>

	</div>

                    <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingFive">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Category Summary Report </b>
					</a>
				</h4>
			</div>
			<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
				<div class="panel-body">
					<p>View the status of your compliances segregated by category across locations, risk profiles, and users, along with details of ageing of overdue compliances by category.</p> 
                    <ul>
                        <li style="list-style-type:decimal">To download you need to select Standard Report in My Report. </li>
                        <li style="list-style-type:decimal">You will be redirected to page that holds all standard report, here in Category Summary Report section click on download icon for the week you want report. These reports can be bifurcated with compliance type (Statutory and Internal).</li>
                        <li style="list-style-type:decimal">The show more link populates the report of older weeks.</li><br />
                          <img  class="img1" style="width: 600px;margin-left:70px; height: 250px"  src="../perfreviewerUpdatedscreens/My%20Report_Category%20Summary%20Report%20%20_Screenshot_1.png" /><br />
                       <li style="list-style-type:decimal">To View overall report of last 18 months till current date Click on following icon</li><br />
                        <img  class="img1" style="width: 600px;margin-left:70px; height: 250px"  src="../perfreviewerUpdatedscreens/My%20Report_Category%20Summary%20Report%20%20_Screenshot_2.png" />
                       <li style="list-style-type:decimal">Below window will appear, you can set filter for specific results</li>
                        <img  class="img1" style="width: 700px;margin-left:70px; height: 250px"  src="../perfreviewerUpdatedscreens/My%20Report_Category%20Summary%20Report%20%20_Screenshot_3.png" />
                        <br />
                        <%--<img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_Category%20Summary%20Report%20%20_Screenshot.png" />--%>

                    </ul>
           
               </div>

			</div>

	</div>

                <div class="panel panel-default">
		           	<div class="panel-heading" role="tab" id="headingSix">
				       <h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Risk Summary Report</b>
					</a>
				      </h4>
		         	</div>
	           		<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
			        	<div class="panel-body">
					<p>This dynamic report shows the status of your compliances segregated by risk profiles across locations, categories, and users, along with details of ageing of overdue compliances by risk.</p> 
                    <ul>
                        <li style="list-style-type:decimal">To download you need to select Standard Report in My Report. </li>
                         <li style="list-style-type:decimal">You will be redirected to page that holds all standard report, here in Risk Summary Report section click on download icon for the week you want report. These reports can be bifurcated with compliance type (Statutory and Internal). </li><br />
                         <li style="list-style-type:decimal">The show more link populates the report of older weeks.</li><br />
                        <img  class="img1" style="width: 700px;margin-left:70px; height: 250px" src="../perfreviewerUpdatedscreens/My%20Report_Risk%20Summary%20Report%20_Screenshot_1.png" />
                        <li style="list-style-type:decimal">To View overall report of last 18 months till current date Click on following icon.</li>
                          <img  class="img1" style="width: 700px;margin-left:70px; height: 250px" src="../perfreviewerUpdatedscreens/My%20Report_Risk%20Summary%20Report%20_Screenshot_2.png" />
                        <li style="list-style-type:decimal">Below window will appear, you can set filter for specific results</li>
                        <img class="img1"  style="height:300px;margin-left:70px;width:700px" src="../perfreviewerUpdatedscreens/My%20Report_Risk%20Summary%20Report%20_Screenshot_3.png" />
                         <br />
                        <%--<img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_Risk%20Summary%20Report%20_Screenshot.png" />
                    --%></ul>
           
                     </div>

			  </div>

          

	   </div>

                    <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingSeven">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Detailed Summary Report</b>
					</a>
				</h4>
			</div>
			<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
				<div class="panel-body">
					<p>Store a contract forever with keyword tagging for easy search and retrieval with role-based access.</p> 
                    <ul>
                        <li style="list-style-type:decimal">To download you need to select Standard Report in My Report. </li>
                        <li style="list-style-type:decimal">2.	You will be redirected to page that holds all standard report, here in Detailed Summary Report section click on download icon for the week you want report. These reports can be bifurcated with compliance type (Statutory and Internal).</li>
                        <li style="list-style-type:decimal">The show more link populates the report of older weeks.</li><br />
                        <img class="img1" style="height:300px;width:700px" src="../perfreviewerUpdatedscreens/My%20Report_Detailed%20Summary%20Report_Screenshot.png" />
                        <%--<img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_Detailed%20Report_1_Screenshot.png" />
                        --%>
                    </ul>
           
               </div>

			</div>

	</div>

                    <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingEight">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Detailed Report</b>
					</a>
				</h4>
			</div>
			<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
				<div class="panel-body">
					<p>Based on compliance type you can view detailed report and also download them. To view and download compliance report you need to follow below steps,</p> 
                    <ul>
                        <li style="list-style-type:decimal">Select Detailed Report option from My Report, you will be redirected to page that features all list of compliance.</li>
                        <li style="list-style-type:decimal">On this page you can view the list of compliances based on compliance type (Statutory and Internal), you can select the type to view list. Also, you can set filter and generate desired list.</li><br />
                        <img class="img1" style="height:300px;width:700px"  src="../ManagementHelpcenterScreenshot/My%20Report_Detailed%20Report_1_Screenshot.png" /><br /><br />
                        <li style="list-style-type:decimal">Advance Search- To generate more specific list this option can be used.</li><br />
                        <img class="img1" style="height:300px;width:700px"  src="../ManagementHelpcenterScreenshot/My%20Report_Detailed%20Report_2_Screenshot.png" /><br /><br />
                        <li style="list-style-type:decimal">To view particular compliance detailed report, click on Action icon and a window will appear, holding the details of compliance, you will see different tabs Summary, Details, Historical Documents, Updates, Audit logs. And two sections - Current Documents and Comments, following are the use,  
                        <ul>
                            <li style="list-style-type:lower-roman" >Summary - This will feature basic details and historical completion status. </li>
                            <li style="list-style-type:lower-roman" >Details - Compliance complete details will be featured.  </li>
                            <li style="list-style-type:lower-roman" >Historical Documents- All the documents related to compliance will be featured, you can view the list in ascending and descending order, adjust no. of columns and set filter. </li>
                            <li style="list-style-type:lower-roman" >Updates - The updates related to compliance will be featured here, you can view the list in ascending and descending order, adjust no. of columns and set filter.   </li>
                            <li style="list-style-type:lower-roman" >Current documents- You can view the related documents in this section.     </li>
                            <li style="list-style-type:lower-roman" >Comment - To comment this section will be used, you can address particular user and comment. To do this, type "<@username> <your comment>" post. </li>
                    
                        </ul>
                            <br /><br />
                            <img class="img1" style="height:300px;width:700px"  src="../ManagementHelpcenterScreenshot/My%20Report_Detailed%20Report_3_Screenshot.png" /> <br />
                        </li>
                        <br />
                        <li style="list-style-type:decimal">You can also export the detailed report of compliance list. And this can be done by setting filter and click on Excel icon to download. <br />
                       </li><br />
                        <img class="img1" style="height:250px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_Detailed%20Report_4_Screenshot.png" />
                    </ul>
           
               </div>

			</div>

	</div>

                    <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingNine">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="true" aria-controls="collapseNine">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Assignment report</b>
					</a>
				</h4>
			</div>
			<div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
				<div class="panel-body">
					<p>You can download the report of compliance assigned location wise, following is the process to download, </p> 
                    <ul>
                        <li style="list-style-type:decimal">Select Assignment Report option in My Report. </li>
                        <li style="list-style-type:decimal">You will be redirected to page, on this page select compliance type and location.</li><br />
                        <li style="list-style-type:decimal">And to download the report click on Export to Excel button. </li><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_Assignment%20Report_Screenshot.png" />  <br />
                      
                        
                    </ul>
           
               </div>

			</div>

	</div>
         
		
                    <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTen">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="true" aria-controls="collapseTen">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Usage Report</b>
					</a>
				</h4>
			</div>
			<div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
				<div class="panel-body">
					<p>This report gives details about user (Performer and Reviewer) AVACOM usage only for the assigned Entities. And this report is only accessible to Management and Company Admin. To access this report, follow below steps,</p> 
                    <ul>
                        <li style="list-style-type:decimal">Select Usage Report in My Report and you will be redirected to Usage Report page.</li>
                        <li style="list-style-type:decimal">On this page select Entity/Sub-Entity/Location, From date and To date.</li>
                        <li style="list-style-type:decimal">Click on Export to Excel button and report will be downloaded to access.</li><br /><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_Usage%20Report_Screenshot.png" />    <br /><br />
                        <li style="list-style-type:decimal">The sheet will hold different tabs giving details of following,
                               <ul>
                                    <li style="list-style-type:lower-roman">Number of User logins in Month</li>
                                    <li style="list-style-type:lower-roman">Last Login Date Report</li>
                                    <li style="list-style-type:lower-roman">Performance Report in Time</li>
                                    <li style="list-style-type:lower-roman">Ageing of System Updation</li>
                                    <li style="list-style-type:lower-roman">Performance Report- Overdue (P)</li>
                                    <li style="list-style-type:lower-roman">Performance Report (Reviewer)</li>
                               </ul>
   
                        </li>
                   </ul>
           
               </div>

			</div>

	</div>
     
                    
                    <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingEleven">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="true" aria-controls="collapseEleven">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Task Report</b>
					</a>
				</h4>
			</div>
			<div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
				<div class="panel-body">
					<p>The Task report gives details of task assigned to you, and to view and download this report, follow the below process,</p> 
                    <ul>
                        <li style="list-style-type:decimal">Select Task Report option in My Report, you will be redirected to page that features list of tasks. </li>
                        <li style="list-style-type:decimal">You can generate list of tasks by setting filter and for more specific list of tasks use Advanced Search.</li><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_Task%20Report_1_Screenshot.png" />
                        <br /><br />
                        <li style="list-style-type:decimal">To download the report, click on Excel icon.</li><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_Task%20Report_2_Screenshot.png" /> <br />
                  </ul>
           
               </div>

			</div>

	</div>

                    <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTwelve">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="true" aria-controls="collapseTwelve">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Audit Report</b>
					</a>
				</h4>
			</div>
			<div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
				<div class="panel-body">
					<p>This report gives details about Audit conducted for assigned Entities. To view/export the report follow below steps, </p> 
                    <ul>
                        <li style="list-style-type:decimal">Select Audit Report in My Report, you will be redirected to page Audit report page that holds list of Audit conducted . </li>
                        <li style="list-style-type:decimal">To view specific Entity/Sub-Entity/Location list set filter and to download the report click on Export Report button. </li><br /><br />
                        <img class="img1" style="height:300px;width:700px" src="../ManagementHelpcenterScreenshot/My%20Report_Audit%20Report_Screenshot.png" />
                    </ul>
           
               </div>

			</div>

	</div>
     

	        </div>
	
	
          </div>
     </div>

 </div><!-- container -->
    </form>
 

  <script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>
