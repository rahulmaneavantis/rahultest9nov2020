﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ProductMappingDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping.ProductMappingDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $('#divUsersDialog').dialog({
                height: 380,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "Product Mapping",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeCombobox(flag) {

            if (flag == 1) {            
                $("#<%= ddlCustomer.ClientID %>").combobox();
                $("#<%= ddlProduct.ClientID %>").combobox();
            }
        }

        function disableCombobox() {

            $(".custom-combobox").attr('disabled', 'disabled');
        }

       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td style="vertical-align: bottom">
                        <div id="divCustomerfilter" runat="server" style="margin-left: 300px">
                            <label style="width: 110px; display: block; float: left; font-size: 13px; color: White; margin-bottom: -5px;">
                                Select Customer :</label>
                            <div style="width: 150px; float: left; margin-top: -15px; margin-left: 109px;">
                                <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged" />
                            </div>
                        </div>
                    </td>
                    <td align="right" style="width: 65%; padding-right: 60px;">
                        <label style="font-size: 13px;">
                            Filter :</label>
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td align="right" class="newlink">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddUser" OnClick="btnAddUser_Click" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdUser" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCreated="grdUser_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdUser_Sorting"
                    Font-Size="12px" DataKeyNames="ProductID,CustomerId" OnRowCommand="grdUser_RowCommand" OnPageIndexChanging="grdUser_PageIndexChanging" 
                    OnRowDataBound="grdUser_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="ProductName" HeaderText="Product Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="ProductName" />
                        <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" SortExpression="CustomerName" />      
                        
                         <asp:TemplateField ItemStyle-Width="110px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>                            
                            <%--<asp:LinkButton runat="server" CommandName="EDIT_USER" ID="lbtnEdit" CommandArgument='<%# Eval("CustomerId") +";"+ Eval("ProductID") %>'><img src="../Images/edit_icon.png" 
                                alt="Edit Product" title="Edit Product" /></asp:LinkButton>--%>
                            <asp:LinkButton runat="server" CommandName="DELETE_USER" CommandArgument='<%# Eval("CustomerId") +";"+ Eval("ProductID") %>' ID="lbtnDelete"
                                OnClientClick="return confirm('Are you certain you want to delete this Product?');"><img src="../Images/delete_icon.png"
                                     alt="Delete Product" title="Delete Product" /></asp:LinkButton>
                            
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                    </asp:TemplateField>
                                                                
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divUsersDialog">
        <asp:UpdatePanel ID="upUsers" runat="server" UpdateMode="Conditional" OnLoad="upUsers_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="UserValidationGroup" />

                        <asp:CustomValidator ID="cvEmailError" runat="server" EnableClientScript="False"
                            ErrorMessage="Email already exists." ValidationGroup="UserValidationGroup" Display="None" />
                    </div>                                      
                    <div runat="server" id="divCustomer" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer</label>
                        <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select customer."
                            ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="UserValidationGroup" Display="None" />
                    </div>
                     <div runat="server" id="divProduct" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Product</label>
                        <asp:DropDownList runat="server" ID="ddlProduct" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 257px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="UserValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divUsersDialog').dialog('close');" />
                    </div>
                </div>

                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
