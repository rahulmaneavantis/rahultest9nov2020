﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping
{
    public partial class CustomerMapping : System.Web.UI.Page
    {
        protected string Approveruser_Roles;
        protected List<Int32> roles;
        protected static int CID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                    {
                        CID = Convert.ToInt32(Request.QueryString["CID"]);
                        BindGrid(CID);
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                
            }
        }
        protected void  BindGrid(int productid)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Customer = (from row in entities.UserCustomerMappings
                                join row1 in entities.Customers on row.CustomerID equals row1.ID
                                where row.UserID == AuthenticationHelper.UserID
                                && row.ProductID== productid
                                select new
                                {
                                    ID = row.UserID,
                                    Name = row1.Name,
                                    ProductID=row.ProductID,
                                    customerid=row1.ID,
                                    Logopath=row1.LogoPath
                                }).Distinct().ToList();
                grdUser.DataSource = Customer;
                grdUser.DataBind();
            }
        }
        protected void grdUser_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
            
                if (e.CommandName.Equals("CHANGE_STATUS"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int cid = Convert.ToInt32(commandArgs[0]);
                    string company = Convert.ToString(commandArgs[1]);
                    int pid = Convert.ToInt32(commandArgs[2]);
                    int userID = Common.AuthenticationHelper.UserID;
                    int custID = Convert.ToInt32(commandArgs[3]);
                    Session["companyDFM"] = company;
                    if (pid==1)
                    {
                        ProcessAuthenticationInformation_Compliance(UserManagement.GetByID(userID), custID);
                    }
                    else if (pid==2)
                    {
                        ProcessAuthenticationInformation_Litigation(UserManagement.GetByID(userID), custID);
                    }
                    else if (pid==3)
                    {
                        ProcessAuthenticationInformation_Audit(UserManagement.GetByID(userID), custID);
                    }
                    else if (pid==4)
                    {
                        ProcessAuthenticationInformation_InternalControl(UserManagement.GetByID(userID), custID);
                    }
                    else if (pid==5)
                    {
                        ProcessAuthenticationInformation_Contract(UserManagement.GetByID(userID), custID);
                    }
                    else if (pid==6)
                    {
                        ProcessAuthenticationInformation_License(UserManagement.GetByID(userID), custID);
                    }
                    else if (pid==7)
                    {
                        ProcessAuthenticationInformation_Vendor(UserManagement.GetByID(userID), custID);
                    }
                    else if (pid==8)
                    {
                        ProcessAuthenticationInformation_Secretarial(UserManagement.GetByID(userID), custID);
                    }
                    else if (pid == 9)
                    {
                        ProcessAuthenticationInformation_HRProduct(UserManagement.GetByID(userID), custID);
                    }
                }

            }
            catch (Exception ex)
            {

            }
        }
        public bool CheckIsServiseProviderId(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ServiceproviderId = false;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select (bool)cust.IsDistributor).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }

        
        public void FormsAuthenticationRedirect_Compliance(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType,int customerid)
        {
            roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(user.ID));
            Session["User_comp_Roles"] = roles;
            if (roles.Contains(6))
            {
                Approveruser_Roles = "APPR";
            }
            else
            {
                Approveruser_Roles = "";
            }
            int ServiceproviderID = 0;            
            bool chkserviceProviderId = CheckIsServiseProviderId(Convert.ToInt32(user.CustomerID));
            if (chkserviceProviderId)
            {
                ServiceproviderID = (int)user.CustomerID;
            }
            else
            {
                ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            }

            //ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            string userProfileID = string.Empty;
            string userProfileID_Encrypted = string.Empty;
            string authkey = string.Empty;

            //if (user.RoleID >= 14 && user.RoleID <= 18 && complianceProdType < 2)
            if (complianceProdType == 1 || complianceProdType == 3)
            {
                userProfileID = RLCSManagement.GetProfileIDByUserID(user.ID);

                string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                if (!string.IsNullOrEmpty(TLConnectAPIUrl) && !string.IsNullOrEmpty(userProfileID))
                {
                    try
                    {
                        authkey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, userProfileID, TLConnectKey);
                    }
                    catch (Exception ex)
                    {
                        userProfileID = string.Empty;
                        authkey = string.Empty;
                    }
                }
            }
            else
                userProfileID = user.ID.ToString();

            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "C", customerid, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);

            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);

            if (complianceProdType == 1)// TLConnect Customer
            {
                if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                }
                else if (role.Equals("HVADM") || role.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (role.Equals("VAUDT"))
                {
                    HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
                }
            }
            else if (complianceProdType == 2) //HRPlus Compliance Product User
            {
                if (role.Equals("HMGMT") || role.Equals("HMGR") || role.Equals("HEXCT"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                }
                else if (role.Equals("SPADM") || role.Equals("DADMN"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                }
                else if (role.Equals("CADMN"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
                }
            }
            else
            {
                if (role.Equals("MGMT") || role.Equals("AUDT"))
                {
                    HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                }
                else if (role.Equals("IMPT"))
                {
                    HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
                else if (role.Equals("CADMN"))
                {
                    if (roles.Count == 0)
                        HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
                    else
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                }
                else if (role.Equals("HVADM") || role.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (role.Equals("VAUDT"))
                {
                    HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
                }
                else if (role.Equals("EXCT"))
                {
                    if (user.IsHead == true)
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                    }
                    else if (Approveruser_Roles.Contains("APPR") && (roles.Contains(3) || roles.Contains(4)))
                    {
                        if (user.IsHead == true)
                        {
                            HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                        }
                        else
                        {
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                        }
                    }
                    else if (Approveruser_Roles.Contains("APPR"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/ManagementDashboardNew.aspx", false);
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                }
                else if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT")) //AVACOM+TL User
                {
                    //HMGMT
                    if (role.Equals("HMGMT"))
                    {
                        var lstMgrAssignedBranch = CustomerBranchManagement.GetMGMTAssignedBranch(Convert.ToInt32(user.ID));

                        if (lstMgrAssignedBranch.Count > 0)
                            HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                        else if (roles.Count > 0)
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                        else
                            HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                    else if (role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                    {
                        if (roles.Count > 0)
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                        else
                            HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                }
                else
                {
                    if (user.IsHead == true)
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                }
            }
        }
        public void FormsAuthenticationRedirect_Litigation(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType,int customerid)
        {
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "L", customerid, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/Litigation/Dashboard/LitigationManagementDashboard.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/Litigation/Dashboard/LitigationManagementDashboard.aspx", false);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Litigation/Dashboard/LitigationDashboard.aspx", false);
            }
        }
        public void FormsAuthenticationRedirect_IFC(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType,int customerid)
        {
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "A", customerid, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/Management/ICFRManagementDashboard.aspx", false);
            }
            else if (role.Equals("HVADM") || role.Equals("HVAUD"))
            {
                HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
            }
            else if (role.Equals("IMPT"))
            {
                HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
            }
            else
            {
                string AuditHeadOrManager = "";
                AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(user.ID));
                if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/IFCAuditManagerDashboard.aspx", false);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/AuditDashboard.aspx", false);
                }
            }
        }
        public void FormsAuthenticationRedirect_ARS(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType,int customerid)
        {
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "I", customerid, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/Management/AuditManagementDashboard.aspx", false);
            }
            else if (role.Equals("IMPT"))
            {
                HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
            }
            else if (role.Equals("HVADM") || role.Equals("HVAUD"))
            {
                HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Common/admindashboard.aspx", false);
            }
            else
            {
                string AuditHeadOrManager = "";
                var PersonResp = CustomerManagementRisk.GetInternalPersonResponsibleid(Convert.ToInt32(user.ID));
                AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(user.ID));
                if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/AuditManagerDashboard.aspx", false);
                }
                else if (PersonResp != 0)  // Person Responsible
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/PersonResponsibleDashboard.aspx", false);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/InternalControlDashboard.aspx", false);
                }
            }
        }
        public void FormsAuthenticationRedirect_Contract(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType,int customerid)
        {
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, 'T', customerid, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/ContractProduct/Dashboard/ContractMangDashboard.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/ContractProduct/Dashboard/ContractMangDashboard.aspx", false);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/ContractProduct/Dashboard/ContractDashboard.aspx", false);
            }
        }
        public void FormsAuthenticationRedirect_License(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType, int customerid)
        {
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, 'S', customerid, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);

            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/LicenseManagement/Dashboard/LicenseMangDashboard.aspx", false);
            }
            else if (role.Equals("HVADM") || role.Equals("HVAUD"))
            {
                HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/LicenseManagement/Dashboard/LicenseMangDashboard.aspx", false);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/LicenseManagement/Dashboard/LicenseDashboard.aspx", false);
            }
        }
        public void FormsAuthenticationRedirect_RLCSVendor(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType, int customerid)
        {
            if (user.VendorRoleID != null && user.VendorRoleID != -1)
            {
                var vrole = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                int ServiceproviderID = 0;
                ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
                FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, vrole, name, checkInternalapplicable, 'S', customerid, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                if (vrole.Equals("HVADM"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (vrole.Equals("HVEND") || vrole.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSVendorAuditDashboard.aspx", false);
                }
            }
            else
            {
                int ServiceproviderID = 0;
                ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
                FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, 'S', customerid, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                if (role.Equals("HVADM"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (role.Equals("HVEND") || role.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSVendorAuditDashboard.aspx", false);
                }
            }
        }
        public void FormsAuthenticationRedirect_Secretarial(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType,int customerid)
        {
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "S", customerid, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);            
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);          
            if (role.Equals("DRCTR"))
            {
                HttpContext.Current.Response.Redirect("~/BM_Management/Dashboard/Dashboard", false);
            }
            else if (role.Equals("HDCS") || role.Equals("CS"))
            {
                HttpContext.Current.Response.Redirect("~/BM_Management/Dashboard/", false);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/BM_Management/Dashboard/", false);
            }

        }
        private void ProcessAuthenticationInformation_Compliance(User user,int custid)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;

                int complianceProdType = 0;

                if (custid == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valCompProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valCompProdType);
                }
                FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, custid);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Audit(User user,int custid) //IFC
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagementRisk.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (custid == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_IFC(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, custid);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_InternalControl(User user,int custid)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagementRisk.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (custid == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_ARS(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, custid);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Litigation(User user,int custid)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.LitigationRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.LitigationRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (custid == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, custid);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Contract(User user,int custid)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.ContractRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.ContractRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (custid == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, custid);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_License(User user,int custid)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.LicenseRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.LicenseRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;

                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (custid == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, custid);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Vendor(User user,int custid)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.VendorRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.VendorRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;

                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (custid == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, custid);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Secretarial(User user,int custid)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;

                if (user.SecretarialRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.SecretarialRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;

                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (custid == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }

                FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, custid);

                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton lbtimg = (ImageButton)e.Row.FindControl("lbtnChangeStatus");
                    Label lblId = (Label)e.Row.FindControl("lblId");
                    //lbtimg.ImageUrl = "../images/" + lblId.Text + "_1.png";
                    lbtimg.ImageUrl = lblId.Text;
                }
            }
            catch(Exception ex) {
                string s = ex.ToString();
            }
        }
        private void ProcessAuthenticationInformation_HRProduct(User user, int custid)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID((int)user.HRRoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;

                int complianceProdType = 0;

                if (custid == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valCompProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valCompProdType);
                }
                FormsAuthenticationRedirect_HRCompliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, custid);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void FormsAuthenticationRedirect_HRCompliance(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType, int customerid)
        {
            roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(user.ID));
            Session["User_comp_Roles"] = roles;
            if (roles.Contains(6))
            {
                Approveruser_Roles = "APPR";
            }
            else
            {
                Approveruser_Roles = "";
            }
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            string userProfileID = string.Empty;
            string userProfileID_Encrypted = string.Empty;
            string authkey = string.Empty;

            //if (user.RoleID >= 14 && user.RoleID <= 18 && complianceProdType < 2)
            if (complianceProdType == 1 || complianceProdType == 3)
            {
                userProfileID = RLCSManagement.GetProfileIDByUserID(user.ID);

                string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                if (!string.IsNullOrEmpty(TLConnectAPIUrl) && !string.IsNullOrEmpty(userProfileID))
                {
                    try
                    {
                        authkey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, userProfileID, TLConnectKey);
                    }
                    catch (Exception ex)
                    {
                        userProfileID = string.Empty;
                        authkey = string.Empty;
                    }
                }
            }
            else
                userProfileID = user.ID.ToString();

            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "C", customerid, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);

            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);

            if (role.Equals("HMGMT") || role.Equals("HMGR") || role.Equals("HEXCT"))
            {
                HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
            }
            else if (role.Equals("SPADM") || role.Equals("DADMN"))
            {
                HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
            }
        }
    }
}