﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.Users;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping
{
    public partial class ProductMappingStructure : System.Web.UI.Page
    {
        protected string Approveruser_Roles;
        protected List<Int32> roles;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    long customerID = -1;
                    //customerID = Common.AuthenticationHelper.CustomerID;
                    int userID = Common.AuthenticationHelper.UserID;
                    User loggedInUser = UserManagement.GetByID(userID);

                    if (loggedInUser.CustomerID != null)
                        customerID = Convert.ToInt32(loggedInUser.CustomerID);
                    else
                        customerID = Common.AuthenticationHelper.CustomerID;

                    List<long> ProductMappingDetails = new List<long>();
                    //var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
                    //if (pPdetails.Count > 0)
                    //{
                    //    Session["IsUserProduct"] = true;
                    //    ProductMappingDetails = pPdetails;
                    //}
                    //else
                    //{
                    ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                    //}

                    //var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                    if (ProductMappingDetails.Count > 3)
                    {
                        dvbtnCompliance.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnLitigation.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnContract.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnLicense.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnVendor.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";

                        dvbtnSecretarial.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnHRProduct.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnInternalControl.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnAudit.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";

                        dvbtnPracticeMgmt.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        dvbtnPayrollMgmt.Attributes["style"] = "width: 30%; margin-top:3%; margin-left:3%;";
                        
                    }


                    bool showComplianceProduct = false;
                    if (ProductMappingDetails.Contains(1))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.RoleID != null && loggedInUser.RoleID != -1)
                            {
                                showComplianceProduct = true;
                            }
                        }
                    }
                    if (showComplianceProduct)
                    {
                        dvbtnCompliance.Visible = true;
                        btnCompliance.Visible = true;
                    }
                    else
                    {
                        dvbtnCompliance.Attributes["style"] = "display:none;";
                        btnCompliance.Visible = false;
                    }


                    bool showLitigationProduct = false;
                    if (ProductMappingDetails.Contains(2))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.LitigationRoleID != null && loggedInUser.LitigationRoleID != -1)
                            {
                                showLitigationProduct = true;
                            }
                        }
                    }
                    if (showLitigationProduct)
                    {
                        dvbtnLitigation.Visible = true;
                        btnLitigation.Visible = true;
                    }
                    else
                    {
                        dvbtnLitigation.Attributes["style"] = "display:none;";
                        btnLitigation.Visible = false;
                    }

                    //---------------Audit

                    bool showIFCProduct = false;
                    if (ProductMappingDetails.Contains(3))
                    {
                        if (loggedInUser != null)
                        {
                            if (!string.IsNullOrEmpty(loggedInUser.PrimaryRoleAudit))
                            {
                                showIFCProduct = true;
                            }
                        }
                    }
                    if (showIFCProduct)
                    {
                        dvbtnInternalControl.Visible = true;
                        btnInternalControl.Visible = true;
                    }
                    else
                    {
                        dvbtnInternalControl.Attributes["style"] = "display:none;";
                        btnInternalControl.Visible = false;
                    }


                    bool showARSProduct = false;
                    if (ProductMappingDetails.Contains(4))
                    {
                        if (loggedInUser != null)
                        {
                            //if (!string.IsNullOrEmpty(loggedInUser.PrimaryRoleAudit))
                            //{
                            showARSProduct = true;
                            //}
                        }
                    }
                    if (showARSProduct)
                    {
                        dvbtnAudit.Visible = true;
                        btnAudit.Visible = true;
                    }
                    else
                    {
                        dvbtnAudit.Attributes["style"] = "display:none;";
                        btnAudit.Visible = false;
                    }



                    bool showContractProduct = false;
                    if (ProductMappingDetails.Contains(5))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.ContractRoleID != null && loggedInUser.ContractRoleID != -1)
                            {
                                showContractProduct = true;
                            }
                        }
                    }
                    if (showContractProduct)
                    {
                        dvbtnContract.Visible = true;
                        btnContract.Visible = true;
                    }
                    else
                    {
                        dvbtnContract.Attributes["style"] = "display:none;";
                        btnContract.Visible = false;
                    }

                    bool showLicenseManagement = false;
                    if (ProductMappingDetails.Contains(6))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.LicenseRoleID != null && loggedInUser.LicenseRoleID != -1)
                            {
                                showLicenseManagement = true;
                            }
                        }
                    }
                    if (showLicenseManagement)
                    {
                        dvbtnLicense.Visible = true;
                        btnLicense.Visible = true;
                    }
                    else
                    {
                        dvbtnLicense.Attributes["style"] = "display:none;";
                        btnLicense.Visible = false;
                    }


                    bool showVendorManagement = false;
                    if (ProductMappingDetails.Contains(7))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.VendorRoleID != null && loggedInUser.VendorRoleID != -1)
                            {
                                showVendorManagement = true;
                            }
                        }
                    }
                    if (showVendorManagement)
                    {
                        dvbtnVendor.Visible = true;
                        btnVendor.Visible = true;
                    }
                    else
                    {
                        dvbtnVendor.Attributes["style"] = "display:none;";
                        btnVendor.Visible = false;
                    }

                    //---------------Secretarial
                    bool showSecretarialProduct = false;
                    if (ProductMappingDetails.Contains(8))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.SecretarialRoleID != null && loggedInUser.SecretarialRoleID != -1)
                            {
                                showSecretarialProduct = true;
                            }
                        }
                    }
                    if (showSecretarialProduct)
                    {
                        dvbtnSecretarial.Visible = true;
                        btnSecretarial.Visible = true;
                    }
                    else
                    {
                        dvbtnSecretarial.Attributes["style"] = "display:none;";
                        btnSecretarial.Visible = false;
                    }




                    //---------------HR
                    bool showHRProduct = false;
                    if (ProductMappingDetails.Contains(9))
                    {
                        if (loggedInUser != null)
                        {
                            if (loggedInUser.HRRoleID != null && loggedInUser.HRRoleID != -1)
                            {
                                showHRProduct = true;
                            }
                        }
                    }
                    if (showHRProduct)
                    {
                        dvbtnHRProduct.Visible = true;
                        btnHRProduct.Visible = true;
                    }
                    else
                    {
                        dvbtnHRProduct.Attributes["style"] = "display:none;";
                        btnHRProduct.Visible = false;
                    }

                    //--------------------Practice Management

                    bool showPracticeMgmtProduct = false;
                    if (ProductMappingDetails.Contains(10))
                    {
                        if (loggedInUser != null)
                        {
                            showPracticeMgmtProduct = true;
                        }
                    }
                    if (showPracticeMgmtProduct)
                    {
                        dvbtnPracticeMgmt.Visible = true;
                        btnPracticeMgmt.Visible = true;
                    }
                    else
                    {
                        dvbtnPracticeMgmt.Attributes["style"] = "display:none;";
                        btnPracticeMgmt.Visible = false;
                    }

                    //--------------------Payroll Management

                    bool showPayrollMgmtProduct = false;
                    if (ProductMappingDetails.Contains(11))
                    {
                        if (loggedInUser != null)
                        {
                            showPayrollMgmtProduct = true;
                        }
                    }
                    if (showPayrollMgmtProduct)
                    {
                        dvbtnPayrollMgmt.Visible = true;
                        btnPayrollMgmt.Visible = true;
                    }
                    else
                    {
                        dvbtnPayrollMgmt.Attributes["style"] = "display:none;";
                        btnPayrollMgmt.Visible = false;
                    }
                  
                    cid.Value = Convert.ToString(Common.AuthenticationHelper.CustomerID);

                    #region added by sagar on 15-09-2020
                    bool checkLicense = UserManagementRisk.CheckEndUserAcceptLicenseAgreementOrNot(userID);
                    bool checkLicense1 = UserManagement.CheckEndUserAcceptLicenseAgreementOrNot(userID);

                    if (checkLicense == false && checkLicense1 == false)
                    {
                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenPopup();", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenPopup();", true);
                        //Response.Redirect("~/RiskManagement/AuditTool/EndUserLicenseAgreement.html");
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Compliance(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;

                int complianceProdType = 0;

                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valCompProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valCompProdType);
                }
                FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Audit(mst_User user) //IFC
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagementRisk.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_IFC(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_InternalControl(mst_User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagementRisk.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_ARS(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Litigation(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.LitigationRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.LitigationRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Contract(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.ContractRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.ContractRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_License(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.LicenseRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.LicenseRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;

                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_Vendor(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;
                if (user.VendorRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.VendorRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;

                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }
                FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void ProcessAuthenticationInformation_HRProduct(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID((int)user.HRRoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;

                int complianceProdType = 0;

                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valCompProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valCompProdType);
                }
                FormsAuthenticationRedirect_HRCompliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ProcessAuthenticationInformation_Secretarial(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = string.Empty;

                if (user.SecretarialRoleID != null)
                    role = RoleManagement.GetByID(Convert.ToInt32(user.SecretarialRoleID)).Code;
                else
                    role = RoleManagement.GetByID(user.RoleID).Code;

                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                    complianceProdType = 0;
                }
                else
                {
                    var IsInternalComplianceApplicable = Common.AuthenticationHelper.IComplilanceApplicable;
                    var IsTaskApplicable = Common.AuthenticationHelper.TaskApplicable;
                    var Isverticlapplicable = Common.AuthenticationHelper.IsVerticalApplicable;
                    var IsLabelApplicable = Common.AuthenticationHelper.IsLabelApplicable;
                    var valComProdType = Common.AuthenticationHelper.ComplianceProductType;

                    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    checkVerticalapplicable = Convert.ToInt32(Isverticlapplicable);
                    checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    complianceProdType = Convert.ToInt32(valComProdType);
                }

                FormsAuthenticationRedirect_Secretarial(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);

                if (user.ChangPasswordDate != null)
                {
                    DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
                else
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnCompliance_Click(object sender, EventArgs e)
        {
            //long customerID = -1;
            //customerID = Common.AuthenticationHelper.CustomerID;
            //int userID = Common.AuthenticationHelper.UserID;
            //List<long> ProductMappingDetails = new List<long>();
            //var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
            //if (pPdetails.Count > 0)
            //{
            //    ProductMappingDetails = pPdetails;
            //    Response.Redirect("~/ProductMapping/CustomerMapping.aspx?CID=1", false);
            //}
            //else
            //{
            //    ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));

            //    if (ProductMappingDetails.Contains(1))
            //    {
            //        ProcessAuthenticationInformation_Compliance(UserManagement.GetByID(userID));
            //    }
            //    else
            //    {
            //        lblErrMsg.Text = "You don't have permission to Access. ";
            //    }
            //}

            long customerID = -1;
            int userID = Common.AuthenticationHelper.UserID;
            User loggedInUser = UserManagement.GetByID(userID);

            if (loggedInUser.CustomerID != null)
                customerID = Convert.ToInt32(loggedInUser.CustomerID);
            else
                customerID = Common.AuthenticationHelper.CustomerID;

            List<long> ProductMappingDetails = new List<long>();
            ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(1))
            {
                ProcessAuthenticationInformation_Compliance(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }

        protected void btnHRProduct_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            int userID = Common.AuthenticationHelper.UserID;
            List<long> ProductMappingDetails = new List<long>();
            var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
            //if (pPdetails.Count > 0)
            //{
            //    ProductMappingDetails = pPdetails;
            //    Response.Redirect("~/ProductMapping/CustomerMapping.aspx?CID=9", false);
            //}
            //else
            //{
            ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));

            if (ProductMappingDetails.Contains(9))
            {
                ProcessAuthenticationInformation_HRProduct(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
            //}
        }

        protected void btnPayrollMgmt_Click(object sender, EventArgs e)
        {
        }
        protected void btnPracticeMgmt_Click(object sender, EventArgs e)
        {
            
            Response.Redirect("~/Common/PracticeDashboard.aspx", false);
        }
        protected void btnLitigation_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            int userID = Common.AuthenticationHelper.UserID;
            List<long> ProductMappingDetails = new List<long>();
            var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
            if (pPdetails.Count > 0)
            {
                ProductMappingDetails = pPdetails;
                Response.Redirect("~/ProductMapping/CustomerMapping.aspx?CID=2", false);
            }
            else
            {
                ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                if (ProductMappingDetails.Contains(2))
                {
                    ProcessAuthenticationInformation_Litigation(UserManagement.GetByID(userID));
                }
                else
                {
                    lblErrMsg.Text = "You don't have permission to Access. ";
                }
            }
        }
        protected void btnContract_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            int userID = Common.AuthenticationHelper.UserID;
            List<long> ProductMappingDetails = new List<long>();
            var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
            if (pPdetails.Count > 0)
            {
                ProductMappingDetails = pPdetails;
                Response.Redirect("~/ProductMapping/CustomerMapping.aspx?CID=5", false);
            }
            else
            {
                ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                if (ProductMappingDetails.Contains(5))
                {
                    ProcessAuthenticationInformation_Contract(UserManagement.GetByID(userID));
                }
                else
                {
                    lblErrMsg.Text = "You don't have permission to Access. ";
                }
            }
        }
        protected void btnLicense_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            int userID = Common.AuthenticationHelper.UserID;
            List<long> ProductMappingDetails = new List<long>();
            var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
            if (pPdetails.Count > 0)
            {
                ProductMappingDetails = pPdetails;
                Response.Redirect("~/ProductMapping/CustomerMapping.aspx?CID=6", false);
            }
            else
            {
                ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                if (ProductMappingDetails.Contains(6))
                {
                    ProcessAuthenticationInformation_License(UserManagement.GetByID(userID));
                }
                else
                {
                    lblErrMsg.Text = "You don't have permission to Access. ";
                }
            }
        }
        protected void btnVendor_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            int userID = Common.AuthenticationHelper.UserID;
            List<long> ProductMappingDetails = new List<long>();
            var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
            if (pPdetails.Count > 0)
            {
                ProductMappingDetails = pPdetails;
                Response.Redirect("~/ProductMapping/CustomerMapping.aspx?CID=7", false);
            }
            else
            {
                ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                if (ProductMappingDetails.Contains(7))
                {
                    ProcessAuthenticationInformation_Vendor(UserManagement.GetByID(userID));
                }
                else
                {
                    lblErrMsg.Text = "You don't have permission to Access. ";
                }
            }
        }
        protected void btnInternalControl_Click(object sender, EventArgs e)
        {
            //long customerID = -1;
            //customerID = Common.AuthenticationHelper.CustomerID;
            //int userID = Common.AuthenticationHelper.UserID;
            //List<long> ProductMappingDetails = new List<long>();
            //var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
            //if (pPdetails.Count > 0)
            //{
            //    ProductMappingDetails = pPdetails;
            //    Response.Redirect("~/ProductMapping/CustomerMapping.aspx?CID=3", false);
            //}
            //else
            //{
            //    ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            //    if (ProductMappingDetails.Contains(3))
            //    {
            //        ProcessAuthenticationInformation_Audit(UserManagement.GetByID(Convert.ToInt32(userID)));
            //    }
            //    else
            //    {
            //        lblErrMsg.Text = "You don't have permission to Access. ";
            //    }
            //}


            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            int userID = Common.AuthenticationHelper.UserID;
            List<long> ProductMappingDetails = new List<long>();
            //var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
            //if (pPdetails.Count > 0)
            //{
            //    ProductMappingDetails = pPdetails;
            //    if (ProductMappingDetails.Contains(3))
            //    {
            //        ProcessAuthenticationInformation_Audit(UserManagementRisk.GetByID(Convert.ToInt32(userID)));
            //    }
            //    else
            //    {
            //        lblErrMsg.Text = "You don't have permission to Access. ";
            //    }
            //}
            //else
            //{
            ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(3))
            {
                ProcessAuthenticationInformation_Audit(UserManagementRisk.GetByID(Convert.ToInt32(userID)));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
            //}
        }
        protected void btnAudit_Click(object sender, EventArgs e)
        {
            //long customerID = -1;
            //customerID = Common.AuthenticationHelper.CustomerID;
            //int userID = Common.AuthenticationHelper.UserID;
            //List<long> ProductMappingDetails = new List<long>();
            //var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
            //if (pPdetails.Count > 0)
            //{
            //    ProductMappingDetails = pPdetails;
            //    Response.Redirect("~/ProductMapping/CustomerMapping.aspx?CID=4", false);
            //}
            //else
            //{
            //    ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            //    if (ProductMappingDetails.Contains(4))
            //    {                    
            //        ProcessAuthenticationInformation_InternalControl(UserManagement.GetByID(Convert.ToInt32(userID)));
            //    }
            //    else
            //    {
            //        lblErrMsg.Text = "You don't have permission to Access. ";
            //    }
            //}
            long customerID = -1;
            customerID = Common.AuthenticationHelper.CustomerID;
            int userID = Common.AuthenticationHelper.UserID;
            List<long> ProductMappingDetails = new List<long>();
            //var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
            //if (pPdetails.Count > 0)
            //{
            //    ProductMappingDetails = pPdetails;
            //    if (ProductMappingDetails.Contains(4))
            //    {
            //        ProcessAuthenticationInformation_InternalControl(UserManagementRisk.GetByID(Convert.ToInt32(userID)));
            //    }
            //    else
            //    {
            //        lblErrMsg.Text = "You don't have permission to Access. ";
            //    }
            //}
            //else
            //{
            ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(4))
            {
                ProcessAuthenticationInformation_InternalControl(UserManagementRisk.GetByID(Convert.ToInt32(userID)));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
            //}
        }

        protected void btnSecretarial_Click(object sender, EventArgs e)
        {
            long customerID = -1;
            //customerID = Common.AuthenticationHelper.CustomerID;
            int userID = Common.AuthenticationHelper.UserID;
            User loggedInUser = UserManagement.GetByID(userID);

            if (loggedInUser.CustomerID != null)
                customerID = Convert.ToInt32(loggedInUser.CustomerID);
            else
                customerID = Common.AuthenticationHelper.CustomerID;

            List<long> ProductMappingDetails = new List<long>();
            var pPdetails = UserManagement.GetProductIDListByUser(Convert.ToInt32(userID));
            ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
            if (ProductMappingDetails.Contains(8))
            {
                ProcessAuthenticationInformation_Secretarial(UserManagement.GetByID(userID));
            }
            else
            {
                lblErrMsg.Text = "You don't have permission to Access. ";
            }
        }
        public void FormsAuthenticationRedirect_Compliance(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType)
        {
            roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(user.ID));

            int ServiceproviderID = 0;
            bool chkserviceProviderId = CheckIsServiseProviderId(Convert.ToInt32(user.CustomerID));
            if (chkserviceProviderId)
            {
                ServiceproviderID = (int)user.CustomerID;
            }
            else
            {
                ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            }
            Session["User_comp_Roles"] = roles;
            if (roles.Contains(6))
            {
                Approveruser_Roles = "APPR";
            }
            else
            {
                Approveruser_Roles = "";
            }

            string userProfileID = string.Empty;
            string userProfileID_Encrypted = string.Empty;
            string authkey = string.Empty;
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("DADMN"))
            {
                HttpContext.Current.Response.Redirect("~/Management/DistributorAdminDashboard.aspx", false);
            }
            else if (role.Equals("MGMT") || role.Equals("AUDT"))
            {
                HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
            }
            else if (role.Equals("IMPT"))
            {
                HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                if (roles.Count == 0)
                    HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
                else
                    HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
            }
            else if (role.Equals("EXCT"))
            {
                HttpContext.Current.Response.Redirect("~/Management/DistributorAdminDashboard.aspx", false);
                //if (user.IsHead == true)
                //{
                //    HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                //}
                //else if (Approveruser_Roles.Contains("APPR") && (roles.Contains(3) || roles.Contains(4)))
                //{
                //    if (user.IsHead == true)
                //    {
                //        HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                //    }
                //    else
                //    {
                //        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                //    }
                //}
                //else if (Approveruser_Roles.Contains("APPR"))
                //{
                //    HttpContext.Current.Response.Redirect("~/Management/ManagementDashboardNew.aspx", false);
                //}
                //else
                //{
                //    HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                //}
            }
            else
            {
                if (user.IsHead == true)
                {
                    HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                }
            }
        }
        //public void FormsAuthenticationRedirect_Compliance(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType)
        //{
        //    roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(user.ID));
        //    //int ServiceproviderID = 0;
        //    //ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);

        //    int ServiceproviderID = 0;
        //    bool chkserviceProviderId = CheckIsServiseProviderId(Convert.ToInt32(user.CustomerID));
        //    if (chkserviceProviderId)
        //    {
        //        ServiceproviderID = (int)user.CustomerID;
        //    }
        //    else
        //    {
        //        ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
        //    }
        //    Session["User_comp_Roles"] = roles;
        //    if (roles.Contains(6))
        //    {
        //        Approveruser_Roles = "APPR";
        //    }
        //    else
        //    {
        //        Approveruser_Roles = "";
        //    }

        //    string userProfileID = string.Empty;
        //    string userProfileID_Encrypted = string.Empty;
        //    string authkey = string.Empty;

        //    //if (user.RoleID >= 14 && user.RoleID <= 18 && complianceProdType < 2)
        //    if (complianceProdType == 1 || complianceProdType == 3)
        //    {
        //        userProfileID = RLCSManagement.GetProfileIDByUserID(user.ID);

        //        string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
        //        string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

        //        if (!string.IsNullOrEmpty(TLConnectAPIUrl) && !string.IsNullOrEmpty(userProfileID))
        //        {
        //            try
        //            {
        //                authkey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, userProfileID, TLConnectKey);
        //            }
        //            catch (Exception ex)
        //            {
        //                userProfileID = string.Empty;
        //                authkey = string.Empty;
        //            }
        //        }
        //    }
        //    else
        //        userProfileID = user.ID.ToString();


        //    if (complianceProdType == 3 && role.StartsWith("H"))
        //    {
        //        var lstMgrAssignedBranch = CustomerBranchManagement.GetMGMTAssignedBranch(Convert.ToInt32(user.ID));
        //        if (lstMgrAssignedBranch.Count > 0)
        //            role = "MGMT";
        //        else if (roles.Count > 0)
        //            role = "EXCT";
        //    }

        //    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
        //    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);

        //    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
        //    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);

        //    if (complianceProdType == 1)// TLConnect Customer
        //    {
        //        if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
        //        {
        //            HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
        //        }
        //        else if (role.Equals("HVADM") || role.Equals("HVAUD"))
        //        {
        //            HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
        //        }
        //        else if (role.Equals("VAUDT"))
        //        {
        //            HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
        //        }
        //    }
        //    else if (complianceProdType == 2) //HRPlus Compliance Product User
        //    {
        //        if (role.Equals("HMGMT") || role.Equals("HMGR") || role.Equals("HEXCT"))
        //        {
        //            HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
        //        }
        //        else if (role.Equals("SPADM") || role.Equals("DADMN"))
        //        {
        //            HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
        //        }
        //        else if (role.Equals("CADMN"))
        //        {
        //            HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
        //        }
        //    }
        //    else
        //    {
        //        if (role.Equals("MGMT") || role.Equals("AUDT"))
        //        {
        //            HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
        //        }
        //        else if (role.Equals("IMPT"))
        //        {
        //            HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
        //        }
        //        else if (role.Equals("CADMN"))
        //        {
        //            if (roles.Count == 0)
        //                HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
        //            else
        //                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
        //        }
        //        else if (role.Equals("HVADM") || role.Equals("HVAUD"))
        //        {
        //            HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
        //        }
        //        else if (role.Equals("VAUDT"))
        //        {
        //            HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
        //        }
        //        else if (role.Equals("EXCT"))
        //        {
        //            if (user.IsHead == true)
        //            {
        //                HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
        //            }
        //            else if (Approveruser_Roles.Contains("APPR") && (roles.Contains(3) || roles.Contains(4)))
        //            {
        //                if (user.IsHead == true)
        //                {
        //                    HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
        //                }
        //                else
        //                {
        //                    HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
        //                }
        //            }
        //            else if (Approveruser_Roles.Contains("APPR"))
        //            {
        //                HttpContext.Current.Response.Redirect("~/Management/ManagementDashboardNew.aspx", false);
        //            }
        //            else
        //            {
        //                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
        //            }
        //        }
        //        else if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT")) //AVACOM+TL User
        //        {
        //            //HMGMT
        //            if (role.Equals("HMGMT"))
        //            {
        //                var lstMgrAssignedBranch = CustomerBranchManagement.GetMGMTAssignedBranch(Convert.ToInt32(user.ID));

        //                if (lstMgrAssignedBranch.Count > 0)
        //                    HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
        //                else if (roles.Count > 0)
        //                    HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
        //                else
        //                    HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
        //            }
        //            else if (role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
        //            {
        //                if (roles.Count > 0)
        //                    HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
        //                else
        //                    HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
        //            }
        //        }
        //        else
        //        {
        //            if (user.IsHead == true)
        //            {
        //                HttpContext.Current.Response.Redirect("~/Management/MgmtDashboardDeptHead.aspx", false);
        //            }
        //            else
        //            {
        //                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
        //            }
        //        }
        //    }
        //}
        public void FormsAuthenticationRedirect_Litigation(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType)
        {
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "L", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/Litigation/Dashboard/LitigationManagementDashboard.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/Litigation/Dashboard/LitigationManagementDashboard.aspx", false);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Litigation/Dashboard/LitigationDashboard.aspx", false);
            }
        }
        public void FormsAuthenticationRedirect_IFC(mst_User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "A", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/Management/ICFRManagementDashboard.aspx", false);
            }
            else if (role.Equals("HVADM") || role.Equals("HVAUD"))
            {
                HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
            }
            else if (role.Equals("IMPT"))
            {
                HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
            }
            else
            {
                string AuditHeadOrManager = "";
                AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(user.ID));
                if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/IFCAuditManagerDashboard.aspx", false);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/AuditDashboard.aspx", false);
                }
            }
        }
        public bool CheckIsServiseProviderId(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ServiceproviderId = false;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select (bool)cust.IsDistributor).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }
        public void FormsAuthenticationRedirect_ARS(mst_User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            int ServiceproviderID = 0;
            bool chkserviceProviderId = CheckIsServiseProviderId(Convert.ToInt32(user.CustomerID));
            if (chkserviceProviderId)
            {
                ServiceproviderID = (int)user.CustomerID;
            }
            else
            {
                ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            }
            //ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "I", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/Management/AuditManagementDashboard.aspx", false);
            }
            else if (role.Equals("IMPT"))
            {
                HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
            }
            else if (role.Equals("HVADM") || role.Equals("HVAUD"))
            {
                HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(user.ID));

                if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/AuditManagerDashboard.aspx", false);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/admindashboard.aspx", false);
                }
            }
            else
            {
                string AuditHeadOrManager = "";
                var PersonResp = CustomerManagementRisk.GetInternalPersonResponsibleid(Convert.ToInt32(user.ID));
                AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(user.ID));
                if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/AuditManagerDashboard.aspx", false);
                }
                else if (PersonResp != 0)  // Person Responsible
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/PersonResponsibleDashboard.aspx", false);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/RiskManagement/Common/InternalControlDashboard.aspx", false);
                }
            }
        }
        public void FormsAuthenticationRedirect_Contract(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, 'T', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/ContractProduct/Dashboard/ContractMangDashboard.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/ContractProduct/Dashboard/ContractMangDashboard.aspx", false);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/ContractProduct/Dashboard/ContractDashboard.aspx", false);
            }
        }
        public void FormsAuthenticationRedirect_License(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, 'S', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);

            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
            if (role.Equals("MGMT"))
            {
                HttpContext.Current.Response.Redirect("~/LicenseManagement/Dashboard/LicenseMangDashboard.aspx", false);
            }
            else if (role.Equals("HVADM") || role.Equals("HVAUD"))
            {
                HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/LicenseManagement/Dashboard/LicenseMangDashboard.aspx", false);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/LicenseManagement/Dashboard/LicenseDashboard.aspx", false);
            }
        }
        public void FormsAuthenticationRedirect_RLCSVendor(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            if (user.VendorRoleID != null && user.VendorRoleID != -1)
            {
                var vrole = RoleManagement.GetByID((int)user.VendorRoleID).Code;
                int ServiceproviderID = 0;
                ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
                FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, vrole, name, checkInternalapplicable, 'S', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                if (vrole.Equals("HVADM"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (vrole.Equals("HVEND") || vrole.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSVendorAuditDashboard.aspx", false);
                }
            }
            else
            {
                int ServiceproviderID = 0;
                ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
                FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, 'S', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                if (role.Equals("HVADM"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else if (role.Equals("HVEND") || role.Equals("HVAUD"))
                {
                    HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSVendorAuditDashboard.aspx", false);
                }
            }
        }
        public void FormsAuthenticationRedirect_RLCSCompliance(int customerID, RLCS_User_Mapping user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType)
        {
            if (customerID != -1)
            {
                int roleidfromrole = UserManagement.GetRLCSRoleID(user.AVACOM_UserRole);

                roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(user.AVACOM_UserID));

                int ServiceproviderID = 0;
                ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.AVACOM_UserID);

                var lstMgrAssignedBranch = CustomerBranchManagement.GetMGMTAssignedBranch(Convert.ToInt32(user.AVACOM_UserID));

                Session["User_comp_Roles"] = roles;
                if (roles.Contains(6))
                {
                    Approveruser_Roles = "APPR";
                }
                else
                {
                    Approveruser_Roles = "";
                }


                string userProfileID = string.Empty;
                string userProfileID_Encrypted = string.Empty;
                string authkey = string.Empty;

                if (roleidfromrole >= 14 && (complianceProdType == 1 || complianceProdType == 3))
                {
                    string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                    string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                    userProfileID = RLCSManagement.GetProfileIDByUserID(user.UserID, user.CustomerID);

                    if (!string.IsNullOrEmpty(TLConnectAPIUrl) && !string.IsNullOrEmpty(userProfileID))
                    {
                        try
                        {
                            authkey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, userProfileID, TLConnectKey);
                        }
                        catch (Exception ex)
                        {
                            //userProfileID = string.Empty;
                            authkey = string.Empty;
                        }
                    }
                }
                else
                {
                    if (user.AVACOM_UserID != null)
                        userProfileID = user.AVACOM_UserID.ToString();
                }

                if (complianceProdType == 3 && role.StartsWith("H"))
                {
                    if (lstMgrAssignedBranch.Count > 0)
                        role = "MGMT";
                    else if (roles.Count > 0)
                        role = "EXCT";
                }

                FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.AVACOM_UserID, role, name, checkInternalapplicable, "C", customerID, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);

                if (complianceProdType == 1)// TLConnect Customer
                {
                    if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                    else if (role.Equals("HVADM") || role.Equals("HVAUD"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                    }
                    else if (role.Equals("VAUDT"))
                    {
                        HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
                    }
                }
                else if (complianceProdType == 2)//HRPlus Compliance Product User
                {
                    if (role.Equals("HMGMT") || role.Equals("HMGR") || role.Equals("HEXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                    }
                    else if (role.Equals("SPADM") || role.Equals("DADMN"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                    }
                    else if (role.Equals("CADMN"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
                    }
                }
                else
                {
                    if (role.Equals("MGMT") || role.Equals("AUDT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                    }
                    else if (role.Equals("IMPT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
                    }
                    else if (role.Equals("CADMN"))
                    {
                        if (roles.Count == 0)
                            HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
                        else
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (role.Equals("EXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (Approveruser_Roles.Contains("APPR") && (roles.Contains(3) || roles.Contains(4)))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (Approveruser_Roles.Contains("APPR"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                    }
                    else if (role.Equals("HVADM"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                    }
                    else if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT")) //AVACOM+TL User
                    {
                        //HMGMT
                        if (role.Equals("HMGMT"))
                        {
                            if (lstMgrAssignedBranch.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                            else if (roles.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                            else
                                HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                        }
                        else if (role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                        {
                            if (roles.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                            else
                                HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                        }
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                }
            }
        }
        public void FormsAuthenticationRedirect_RLCSCompliance(int customerID, RLCS_User_Mapping user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, string profileID, string authKey, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType)
        {
            if (customerID != -1)
            {
                int roleidfromrole = UserManagement.GetRLCSRoleID(user.AVACOM_UserRole);
                roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(user.AVACOM_UserID));
                int ServiceproviderID = 0;
                ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.AVACOM_UserID);

                var lstMgrAssignedBranch = CustomerBranchManagement.GetMGMTAssignedBranch(Convert.ToInt32(user.AVACOM_UserID));
                Session["User_comp_Roles"] = roles;
                if (roles.Contains(6))
                {
                    Approveruser_Roles = "APPR";
                }
                else
                {
                    Approveruser_Roles = "";
                }
                string userProfileID = profileID;
                string authkey = authKey;

                if (String.IsNullOrEmpty(userProfileID) || string.IsNullOrEmpty(authKey))
                {
                    if (roleidfromrole >= 14 && complianceProdType < 2)
                    {
                        string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                        string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                        //userProfileID = RLCSManagement.GetProfileIDByUserID((long)user.AVACOM_UserID);
                        userProfileID = RLCSManagement.GetProfileIDByUserID(user.UserID, user.CustomerID);

                        if (!string.IsNullOrEmpty(TLConnectAPIUrl) && !string.IsNullOrEmpty(userProfileID))
                        {
                            try
                            {
                                authkey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, userProfileID, TLConnectKey);
                            }
                            catch (Exception ex)
                            {
                                //userProfileID = string.Empty;
                                authkey = string.Empty;
                            }
                        }
                    }
                }

                FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.AVACOM_UserID, role, name, checkInternalapplicable, "C", customerID, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);

                if (complianceProdType == 1)// TLConnect Customer
                {

                    if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                    else if (role.Equals("HVADM") || role.Equals("HVAUD"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                    }
                    else if (role.Equals("VAUDT"))
                    {
                        HttpContext.Current.Response.Redirect("~/VenderAudit/aspxpages/VendorAuditDashboard.aspx", false);
                    }
                }
                else if (complianceProdType == 2)//HRPlus Compliance Product User
                {
                    if (role.Equals("HMGMT") || role.Equals("HMGR") || role.Equals("HEXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                    }
                    else if (role.Equals("SPADM") || role.Equals("DADMN"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
                    }
                    else if (role.Equals("CADMN"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
                    }
                }
                else
                {
                    if (role.Equals("MGMT") || role.Equals("AUDT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                    }
                    else if (role.Equals("IMPT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
                    }
                    else if (role.Equals("CADMN"))
                    {
                        if (roles.Count == 0)
                            HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
                        else
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (role.Equals("EXCT"))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (Approveruser_Roles.Contains("APPR") && (roles.Contains(3) || roles.Contains(4)))
                    {
                        HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                    }
                    else if (Approveruser_Roles.Contains("APPR"))
                    {
                        HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                    }
                    else if (role.Equals("HVADM"))
                    {
                        HttpContext.Current.Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                    }
                    else if (role.Equals("HMGMT") || role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT")) //AVACOM+TL User
                    {
                        //HMGMT
                        if (role.Equals("HMGMT"))
                        {
                            if (lstMgrAssignedBranch.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                            else if (roles.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                            else
                                HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                        }
                        else if (role.Equals("LSPOC") || role.Equals("HMGR") || role.Equals("HAPPR") || role.Equals("HEXCT"))
                        {
                            if (roles.Count > 0)
                                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                            else
                                HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                        }
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", false);
                    }
                }
            }
        }
        public void FormsAuthenticationRedirect_HRCompliance(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProdType)
        {
            roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(user.ID));
            //int ServiceproviderID = 0;
            //ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);

            int ServiceproviderID = 0;
            bool chkserviceProviderId = CheckIsServiseProviderId(Convert.ToInt32(user.CustomerID));
            if (chkserviceProviderId)
            {
                ServiceproviderID = (int)user.CustomerID;
            }
            else
            {
                ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            }
            Session["User_comp_Roles"] = roles;
            if (roles.Contains(6))
            {
                Approveruser_Roles = "APPR";
            }
            else
            {
                Approveruser_Roles = "";
            }

            string userProfileID = string.Empty;
            string userProfileID_Encrypted = string.Empty;
            string authkey = string.Empty;

            //if (user.RoleID >= 14 && user.RoleID <= 18 && complianceProdType < 2)
            if (complianceProdType == 1 || complianceProdType == 3)
            {
                userProfileID = RLCSManagement.GetProfileIDByUserID(user.ID);

                string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                string TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                if (!string.IsNullOrEmpty(TLConnectAPIUrl) && !string.IsNullOrEmpty(userProfileID))
                {
                    try
                    {
                        authkey = RLCSManagement.GetAuthKeyByProfileID(TLConnectAPIUrl, userProfileID, TLConnectKey);
                    }
                    catch (Exception ex)
                    {
                        userProfileID = string.Empty;
                        authkey = string.Empty;
                    }
                }
            }
            else
                userProfileID = user.ID.ToString();

            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, userProfileID, authkey, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);

            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);

            if (role.Equals("HMGMT") || role.Equals("HMGR") || role.Equals("HEXCT"))
            {
                HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
            }
            else if (role.Equals("SPADM") || role.Equals("DADMN"))
            {
                HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMSPOCDashboard.aspx", false);
            }
            else if (role.Equals("CADMN"))
            {
                HttpContext.Current.Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
            }
        }

        //private void FormsAuthenticationRedirect_Secreterial(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable)
        public void FormsAuthenticationRedirect_Secretarial(User user, string role, string name, int checkInternalapplicable, int checkTaskapplicable, int IsVerticlApplicable, bool IsPaymentCustomer, int checkLabelApplicable, int complianceProductType)
        {
            int ServiceproviderID = 0;
            ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "S", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, IsVerticlApplicable, IsPaymentCustomer, checkLabelApplicable, complianceProductType, ServiceproviderID), false);
            //FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", user.ID, role, name, checkInternalapplicable, "L", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty), false);
            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);

            if (role.Equals("DRCTR"))
            {
                HttpContext.Current.Response.Redirect("~/BM_Management/Dashboard/Dashboard", false);
            }
            else if (role.Equals("HDCS") || role.Equals("CS"))
            {
                HttpContext.Current.Response.Redirect("~/BM_Management/Dashboard/", false);
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/BM_Management/Dashboard/", false);
            }

        }
        public bool ReAuthenticate_User(int customerID, string role = "", string prodApplLogin = "")
        {
            bool authSuccess = false;
            try
            {
                if (customerID != 0)
                {
                    string[] userDetails = HttpContext.Current.User.Identity.Name.Split(';');

                    if (userDetails.Length >= 9)
                    {
                        string UserID = userDetails[0];
                        string Role = userDetails[1];

                        if (!role.Equals(""))
                            Role = role;

                        string User = userDetails[2];
                        string IComplilanceApplicable = userDetails[3];
                        string ProductApplicableLogin = userDetails[4];

                        if (!prodApplLogin.Equals(""))
                            ProductApplicableLogin = prodApplLogin;

                        string CustomerID = customerID.ToString();
                        string TaskApplicable = userDetails[6];
                        string ProfileID = userDetails[7];
                        string AuthKey = userDetails[8];
                        string IsVerticalApplicable = userDetails[9];
                        string IsPaymentCustomer = userDetails[10];
                        string IsLabelApplicable = userDetails[11];
                        string ComplianceProductType = userDetails[12];
                        string ServiceproviderID = userDetails[13];

                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}",
                          UserID, Role, User, IComplilanceApplicable, ProductApplicableLogin, CustomerID, TaskApplicable, ProfileID, AuthKey, IsVerticalApplicable, IsPaymentCustomer, IsLabelApplicable, ComplianceProductType, ServiceproviderID), false);

                        authSuccess = true;
                    }
                }

                return authSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return authSuccess;
            }
        }

        public bool ReAuthenticate_UserNew(int customerID, string role = "", string prodApplLogin = "")
        {
            bool authSuccess = false;
            try
            {
                if (customerID != 0)
                {
                    string[] userDetails = HttpContext.Current.User.Identity.Name.Split(';');

                    if (userDetails.Length >= 9)
                    {
                        string UserID = userDetails[0];
                        string Role = userDetails[1];

                        if (!role.Equals(""))
                            Role = role;

                        string User = userDetails[2];
                        string IComplilanceApplicable = userDetails[3];
                        string ProductApplicableLogin = userDetails[4];

                        if (!prodApplLogin.Equals(""))
                            ProductApplicableLogin = prodApplLogin;

                        string CustomerID = customerID.ToString();
                        string TaskApplicable = userDetails[6];
                        string ProfileID = userDetails[7];
                        string AuthKey = userDetails[8];
                        string IsVerticalApplicable = userDetails[9];
                        string IsPaymentCustomer = userDetails[10];
                        string IsLabelApplicable = userDetails[11];
                        string ComplianceProductType = userDetails[12];
                        string ServiceproviderID = userDetails[13];

                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}",
                          UserID, Role, User, IComplilanceApplicable, ProductApplicableLogin, CustomerID, TaskApplicable, ProfileID, AuthKey, IsVerticalApplicable, IsPaymentCustomer, IsLabelApplicable, ComplianceProductType, ServiceproviderID), false);
                        //TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                        //Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                        if (Role.Equals("DADMN"))
                        {
                            HttpContext.Current.Response.Redirect("~/Management/DistributorAdminDashboard.aspx", false);
                        }
                        else if (Role.Equals("MGMT") || role.Equals("AUDT"))
                        {
                            HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
                        }
                        else if (Role.Equals("IMPT"))
                        {
                            HttpContext.Current.Response.Redirect("~/Common/CompanyStructure.aspx", false);
                        }
                        else if (Role.Equals("CADMN"))
                        {
                            if (roles.Count == 0)
                                HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
                            else
                                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                        }
                        else if (Role.Equals("EXCT"))
                        {
                            roles = CustomerBranchManagement.GetAssignedroleid(Convert.ToInt32(UserID));                                                    
                            if (roles.Contains(6))
                            {
                                Approveruser_Roles = "APPR";
                            }
                            else
                            {
                                Approveruser_Roles = "";
                            }
                            if (Approveruser_Roles.Contains("APPR"))
                            {
                                HttpContext.Current.Response.Redirect("~/Management/ManagementDashboardNew.aspx", false);
                            }
                            else
                            {
                                HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                            }
                        }
                        else
                        {
                            HttpContext.Current.Response.Redirect("~/Common/Dashboard.aspx", false);
                        }
                        authSuccess = true;
                    }
                }

                return authSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return authSuccess;
            }
        }
    }
}