﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping
{
    public partial class ProductMappingDetails : System.Web.UI.Page
    {

        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindCustomers();
                BindCustomersList();
                BindProduct();
                BindUsers();
                Session["CurrentRole"] = AuthenticationHelper.Role;
                Session["CurrentUserId"] = AuthenticationHelper.UserID;
            }
        }
        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                ddlCustomer.DataSource = CustomerManagement.GetAll(customerID);
                ddlCustomer.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomersList()
        {
            try
            {
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                ddlCustomerList.DataSource = CustomerManagement.GetAll(customerID);
                ddlCustomerList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindProduct()
        {
            try
            {
                if (ddlCustomer.SelectedValue != "" && ddlCustomer.SelectedValue != null)
                {
                    ddlProduct.DataTextField = "Name";
                    ddlProduct.DataValueField = "ID";
                    ddlProduct.DataSource = CommanClass.FillProduct();
                    ddlProduct.DataBind();
                    ddlProduct.Items.Insert(0, new ListItem("< Select Product >", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                var productmappinglist = CommanClass.GetAllProductMapping(customerID, tbxFilter.Text);
                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "ProductName")
                    {
                        productmappinglist = productmappinglist.OrderBy(entry => entry.ProductName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "CustomerName")
                    {
                        productmappinglist = productmappinglist.OrderBy(entry => entry.CustomerName).ToList();
                    }

                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ProductName")
                    {
                        productmappinglist = productmappinglist.OrderByDescending(entry => entry.ProductName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "CustomerName")
                    {
                        productmappinglist = productmappinglist.OrderByDescending(entry => entry.CustomerName).ToList();
                    }

                    direction = SortDirection.Ascending;
                }
                grdUser.DataSource = productmappinglist;
                grdUser.DataBind();
                upUserList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            AddNewUser();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open')", true);
        }
        public void AddNewUser()
        {
            try
            {
                ViewState["Mode"] = 0;
                ddlCustomer.ClearSelection();
                ddlProduct.ClearSelection();
                ddlCustomer.Enabled = true;
                upUsers.Update();
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #region user Detail

        protected void grdUser_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(Convert.ToString(e.CommandArgument).Split(';')[0]);
                int productid = Convert.ToInt32(Convert.ToString(e.CommandArgument).Split(';')[1]);
                if (e.CommandName.Equals("EDIT_USER"))
                {
                    EditUserInformation(customerid, productid);
                }
                else if (e.CommandName.Equals("DELETE_USER"))
                {
                    CommanClass.Delete(customerid, productid);
                    CommanClass.ProductMappingRiskDelete(customerid, productid);
                    BindUsers();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUser.PageIndex = e.NewPageIndex;
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdUser_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                var productmappinglist = CommanClass.GetAllProductMapping(customerID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    productmappinglist = productmappinglist.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    productmappinglist = productmappinglist.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdUser.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdUser.Columns.IndexOf(field);
                    }
                }
                grdUser.DataSource = productmappinglist;
                grdUser.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdUser_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        #endregion
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;
                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upUsers_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox(1);", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindProduct();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void EditUserInformation(int customerId, long ProductId)
        {
            try
            {

                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox(0);", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "disableCombobox", "disableCombobox();", true);
                ViewState["Mode"] = 1;
                //ViewState["customerId"] = customerId;
                com.VirtuosoITech.ComplianceManagement.Business.Data.ProductMapping user = CommanClass.GetByProductMappingID(customerId, ProductId);
                ddlProduct.SelectedValue = user.ProductID.ToString();
                if (divCustomer.Visible && user.CustomerID.HasValue)
                {
                    ddlCustomer.SelectedValue = user.CustomerID.Value.ToString();
                    ddlCustomer_SelectedIndexChanged(null, null);
                }
                upUsers.Update();
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Business.DataRisk.ProductMapping_Risk productmappingrisk = new Business.DataRisk.ProductMapping_Risk()
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue),
                    ProductID = Convert.ToInt32(ddlProduct.SelectedValue),
                    IsActive = false,
                    CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now
                };
                Business.Data.ProductMapping productmapping = new Business.Data.ProductMapping()
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue),
                    ProductID = Convert.ToInt32(ddlProduct.SelectedValue),
                    IsActive = false,
                    CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now
                };

                bool result = false;
                if ((int)ViewState["Mode"] == 0)
                {
                    if (CommanClass.Exists(ddlProduct.SelectedItem.Text, ddlCustomer.SelectedItem.Text))
                    {
                        cvEmailError.ErrorMessage = "Product & Customer name already exists.";
                        cvEmailError.IsValid = false;
                        return;
                    }
                    var cadminuser = CommanClass.GetAllCompanyAdminUsers(Convert.ToInt32(ddlCustomer.SelectedValue));
                    if (cadminuser != null)
                    {
                        //Litigation Management
                        if (Convert.ToInt32(ddlProduct.SelectedValue) == 2)
                        {
                            CommanClass.UpdateUserRole(cadminuser.ToList(), "LIT");
                            CommanClass.UpdateUserRoleRisk(cadminuser.ToList(), "LIT");
                        }
                        //Contract Management
                        if (Convert.ToInt32(ddlProduct.SelectedValue) == 5)
                        {
                            CommanClass.UpdateUserRole(cadminuser.ToList(), "CONT");
                            CommanClass.UpdateUserRoleRisk(cadminuser.ToList(), "CONT");
                        }
                        //License Management
                        if (Convert.ToInt32(ddlProduct.SelectedValue) == 6)
                        {
                            CommanClass.UpdateUserRole(cadminuser.ToList(), "LIC");
                            CommanClass.UpdateUserRoleRisk(cadminuser.ToList(), "LIC");
                        }
                    }
                    if (CommanClass.UpdateNew(productmapping)==false)
                    {
                        result = CommanClass.Create(productmapping);
                    }
                    else
                    {
                        result = true;
                    }                  
                    if (CommanClass.UpdateNewRisk(productmappingrisk) == false)
                    {
                        result = CommanClass.Create(productmappingrisk);
                    }
                    else
                    {
                        result = true;
                    }
                }

                if (result)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Your details have been saved successfully.";
                }
                //if (result)
                //{
                //    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "CloseDialog", "$(\"#divUsersDialog\").dialog('close')", true);
                //}
                //else
                //{
                //    cvEmailError.IsValid = false;
                //    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                //}
                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public event EventHandler OnSaved;

    }
}