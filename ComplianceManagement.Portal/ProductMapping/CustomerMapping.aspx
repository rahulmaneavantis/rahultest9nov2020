﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerMapping.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping.CustomerMapping" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        #grdUser > tbody > tr {
            display: inline-block;
            width: 13%;
            margin-left: 40px;
        }

            #grdUser > tbody > tr > td {
                border: none;
            }

        #grdUser {
            border: none !important;
            width: 85%;
        }
    </style>
    <style>
        input#dvbtnInternalControl:hover {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            cursor: pointer;
        }

        input#dvbtnAudit:hover {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            cursor: pointer;
        }

        input#dvbtnCompliance:hover {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            cursor: pointer;
        }

        input#dvbtnLicense:hover {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            cursor: pointer;
        }
    </style>

    <style>
        body {
            font-family: arial;
        }

        .clsMain {
        }

        #head1 {
            font-size: 43px;
            font-family: arial;
            color: #666666;
        }

        #head2 {
            font-size: 54px;
            font-weight: bold;
            font-family: arial;
            color: #666666;
        }

        .clsSqMain {
        }

        #squre1 {
            background: #1FD9E1;
        }

        #sqTxt1 {
            color: #ffffff;
            text-align: center;
        }

        #dvbtnCompliance1, #squre2, #squre3 {
            float: left;
            width: 200px;
            height: 80px;
            border: 2px solid #1FD9E1;
            background: #1FD9E1;
            margin-right: 10px;
            Color: white;
            font-weight: bold;
        }

        #squre2 {
            left: 20%;
        }

        #squre3 {
            left: 40%;
        }

        #sqTxt2, #sqTxt3 {
            text-align: center;
        }

        #sqTxt1, #sqTxt2 {
            padding-top: 16%;
        }

        #sqTxt3 {
            padding-top: 15%;
        }

        #inner {
            display: table;
            margin: 0 auto;
            width: 75%;
            text-align: center;
        }

        #innerHead {
            display: table;
            margin: 0 auto;
            width: 75%;
            text-align: center;
        }

        .btn {
            padding: 12px 12px 12px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="clsMain" style="padding-top: 5%;">
            <div id="innerHead">
                <%--<center>--%>
                <img src="../Images/avantisLogo.png" align="BOTTOM" />
                <div id="head1">Welcome to AVANTIS</div>

                <%--</center>--%>
            </div>
        </div>
        <div style="margin-left: 185px;">
            <div></div>
            <asp:GridView runat="server" ID="grdUser" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                BackColor="White"
                CellPadding="4" AllowPaging="True" PageSize="12" Width="100%"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdUser_RowCommand" OnRowDataBound="grdUser_RowDataBound">
                <Columns>
                    <asp:TemplateField ItemStyle-Width="60px" Visible="false" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblId" Text='<%# Eval("LogoPath") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <div style="border: 1px solid #DEDFDE; margin: 0px; padding: 5px; width: 200px;">
                                <div style="height: 110px;">
                                    <asp:ImageButton runat="server" CommandName="CHANGE_STATUS"
                                        CommandArgument='<%# Eval("ID") +","+ Eval("Name") +","+ Eval("ProductID") +","+ Eval("customerid") %>' ID="lbtnChangeStatus" Width="200px"
                                        ToolTip="Click to toggle the status..."></asp:ImageButton>
                                </div>
                                <div style="clear: both; height: 5px"></div>
                                <div>
                                    <asp:Label runat="server" Text='<%# Eval("Name") %>' ID="lbtnChangeStatus12"></asp:Label>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" Visible="false" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ID="lbtnChangeStatus1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>

                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                   
                </EmptyDataTemplate>
            </asp:GridView>
            <div></div>
        </div>
    </form>
</body>
</html>
