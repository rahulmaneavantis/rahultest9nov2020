﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Threading;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContactUs
{
    public partial class ContactUs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //LblMessage.Text = string.Empty;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //Response.Headers.Add("X-Content-Type-Options", "nosniff");
            //Response.Headers.Add("X-XSS-Protection", "1");
            //base.OnPreRender(e);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    if ((!(string.IsNullOrEmpty(txtName.Text)) && !(string.IsNullOrEmpty(txtEmail.Text)) && !(string.IsNullOrEmpty(txtPhone.Text)) && !(string.IsNullOrEmpty(txtMsg.Text))))
                    {
                        string message = "<html><head><title>New enquiry arrived.</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                             + "Dear Sandeep,<br /><br /> New enquiry has been arrived for AVACOM. The details are as follows, <br /><br />Name:" + txtName.Text
                                             + "<br />Email : " + txtEmail.Text
                                             + "<br />Mobile : " + txtPhone.Text
                                             + "<br />Message Posted : " + txtMsg.Text.Replace("\n", "<br />")
                                             + "<br /><br />Please do the follow up."
                                             + "<br /><br />Thanks and Regards,<br />Team AVACOM.</body></html>";

                        new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { ConfigurationManager.AppSettings["ContactUsEmail"].ToString() }), null, null, "New enquiry arrived for AVACOM.", message); }).Start();

                        txtName.Text = string.Empty;
                        txtEmail.Text = string.Empty;
                        txtPhone.Text = string.Empty;
                        txtMsg.Text = string.Empty;
                        LblMessage.Text = "Thank you for enquiring about AVACOM. Our team will contact you shortly."; 
                    }
                    else
                    {
                        LblMessage.Text = "Please enter the following value."; 
                    }

                   
                   
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}