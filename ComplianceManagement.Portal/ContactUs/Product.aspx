﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContactUs.Product" %>

<!DOCTYPE html>
<!--[if IE 8 ]><html lang="en" class="isie ie8 oldie no-js"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="isie ie9 no-js"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<!-- Meta Tags -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Avantis Main 1">
<meta name="keywords" content="Homepage, Avantis">
<!-- Title -->
<title>Product</title>
<!-- Favicon -->

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,800' rel='stylesheet' type='text/css'>
<!-- Stylesheets -->
<link rel='stylesheet' id='twitter-bootstrap-css' href='../Style/NewCss/bootstrap.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='fontello-css' href='../Style/NewCss/fontello.css' type='text/css' media='all'/>
<link rel='stylesheet' id='prettyphoto-css-css' href='../Scripts/New/prettyphoto/css/prettyPhoto.css' type='text/css' media='all'/>
<link rel='stylesheet' id='animation-css' href='../Style/NewCss/animation.css' type='text/css' media='all'/>
<link rel='stylesheet' id='flexSlider-css' href='../Style/NewCss/flexslider.css' type='text/css' media='all'/>
<link rel='stylesheet' id='perfectscrollbar-css' href='../Style/NewCss/perfect-scrollbar-0.4.10.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-validity-css' href='../Style/NewCss/jquery.validity.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-ui-css' href='../Style/NewCss/jquery-ui.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='style-css' href='../Style/NewCss/style.css' type='text/css' media='all'/>
<link rel='stylesheet' id='mobilenav-css' href='../Style/NewCss/mobilenav.css' type='text/css' media="screen and (max-width: 838px)"/>
<!-- jQuery -->
<script src="../Scripts/New/jquery-1.11.1.min.js"></script>
<!-- <script src="js/hightlight.js"></script> -->
<!-- Google Maps -->
<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;ver=4.0'></script>
<!--[if lt IE 9]>
            <script>
                document.createElement("header");
                document.createElement("nav");
                document.createElement("section");
                document.createElement("article");
                document.createElement("aside");
                document.createElement("footer");
                document.createElement("hgroup");
            </script>
        <![endif]-->
<!--[if lt IE 9]>
            <script src="js/html5.js"></script>
        <![endif]-->
<!--[if lt IE 7]>
            <script src="js/icomoon.js"></script>
        <![endif]-->
<!--[if lt IE 9]>
            <link href="css/ie.css" rel="stylesheet">
        <![endif]-->
<!--[if lt IE 9]>
            <script src="js/jquery.placeholder.js"></script>
            <script src="js/script_ie.js"></script>
        <![endif]-->
</head>
<body class="w1170 headerstyle1 preheader-on">
<!-- Content Wrapper -->
<div id="Avantis-content-wrapper">
	<header id="header" class="style1">
<!--div id="upper-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="item left hidden-separator">
					<ul id="menu-shop-header" class="menu">
						
						<li class="menu-item"><a href="#">Wishlist</a></li>
						<li class="menu-item"><a href="#">My Account</a></li>
					</ul>
				</div>
				<div class="item right hidden-separator">
					<div class="cart-menu-item ">
						<a href="#">0 ITEMS(S) - <span class="amount">&pound;0.00</span></a>
						<div class="shopping-cart-dropdown">
							<div class="sc-header">
								<h4>Cart is empty</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Main Header -->
<div id="main-header">
	<div class="container">
		<div class="row">
			<!-- Logo -->
			<div class="col-lg-4 col-md-4 col-sm-4 logo">
				<a href="Index.aspx" title="Avantis" rel="home"><img class="logo" src="../Images/img/logo.png" alt="Avantis"></a>
				<div id="main-nav-button">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="col-sm-8 align-right">
				<!-- Text List -->
				<!--<ul class="text-list">
					<li>Call Us: +91 98234 92350</li>
				</ul>-->
				<!-- Social Media -->
				<!--<ul class="social-media">
					<li><a target="_blank" href="#"><i class="icon-facebook"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-twitter"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-google"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-linkedin"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-instagram"></i></a>
					</li>
				</ul>-->
                <ul  class="menu top_menu" style="border-right:none;margin: 25px 0px 0px;">
					<li class="menu-item"><a href="Index.aspx">JOIN US</a></li>
                    <li class="menu-item"><a href="../Login.aspx">CUSTOMER LOGIN</a></li>
					<li class="menu-item"><a href="ContactUs.aspx">CONTACT US</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- /Main Header -->
<!-- Lower Header -->
<div id="lower-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="lower-logo">
					<a href="Index.aspx" title="Avantis" rel="home"><img class="logo" src="../Images/img/logo.png" alt="Avantis"></a>
				</div>
				<!-- Main Navigation -->
				<ul id="main-nav" class="menu">
					<li class="menu-item"><a href="Index.aspx" >Home</a></li>
                    <li class="menu-item"><a href="Product.aspx" style="color: #0294D3;">Product</a></li>
                    <li class="menu-item"><a href="AboutUs.aspx">About</a></li>
                    <li class="menu-item"><a href="#">Case Studies</a></li>
					<li class="menu-item"><a href="#">Blog</a></li>
				</ul>
				<!-- /Main Navigation -->
				<!-- Search Box -->
				<div id="search-box" class="align-right">
					<i class="icons icon-search"></i>
					<form role="search" method="get" id="searchform" action="#">
						<input type="text" name="s" placeholder="Search here..">
						<div class="iconic-submit">
							<div class="icon">
								<i class="icons icon-search"></i>
							</div>
							<input type="submit" value="">
						</div>
					</form>
				</div>
				<!-- /Search Box -->
			</div>
		</div>
	</div>
</div>
<!-- /Lower Header -->
</header>
	<!-- /Header -->
	
	<div id="Avantis-content-inner">
	<!-- Main Content -->
    
	<section id="main-content" style="min-height:450px;">
	<!-- Container -->
	
		<!-- Google Map -->
		
			
				
					
				
		<div class="container">	
		<!-- /Google Map -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 small-padding" style="padding-top:0px !important">
				<!--<div class="clearfix clearfix-height20">
				</div>-->
				<!--<div class="page-heading style3 wrapper border-bottom ">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h1>Product</h1>
				</div>
				
			</div>
		</div>-->
				
				<h2 style="padding:30px 0px 0px;text-align: center; font-weight: bold;">AVACOM – Statutory Compliance Tool</h2>

<p>Avacom provides highly automated and flexible compliance solution to organisations in diverse industries like Avantis, manufacturing, IT, media and financial institutions. It allows access to compliance monitoring dashboards from anywhere on the planet. If you can launch a browser you can access avacom. </p>
<p>Avacom is a highly adaptable software product designed to improve compliance management that replaces people dependent manual processes. Avacom manages your compliances, stores your documents, and enables audits and seamless monitoring across the globe.</p>
<p>Avacom provides a comprehensive solution to organization’s complete

compliance requirements. It can scale across multiple geographies, 

supporting compliance tasks at various organizational levels. With over 

200 Acts preconfigured, it creates a robust compliance framework. It 

generates comprehensive “Group level Compliance Dashboards” for 

consistent reporting across all locations.”</p>
	</div>
			</div>
		</div>
<div class="row">
<div class="col-md-6" style="
    background-color: #5F5E5E;
    color: #FFF;
        padding: 1% 2%;height:465px;
">
<h4 style="
    color: #FFF;
">Product Features</h4>
<ul class="list wow animated animated animated" style="visibility: visible;">
						<li class="icon-arrows-cw">Web-based</li>
<li class="icon-arrows-cw">Maps to your organisation’s structure or organogram</li>
<li class="icon-arrows-cw">Deploy in multiple geographies, different business units</li>
<li class="icon-arrows-cw">Single repository for all documents. Nothing is ever deleted. Upgraded documents are saved as new version</li>
<li class="icon-arrows-cw">Every compliance is assigned to a Performer. It also has a Reviewer and Approver. Specific dashboards for each role help oversee and perform tasks</li>
<li class="icon-arrows-cw">Reminders and escalations are built in </li>
<li class="icon-arrows-cw">Compliances are colour coded for severity of risk. Penalties can be understood at a glance </li>
<li class="icon-arrows-cw">Delayed compliances are highlighted according to their risk in Management dashboards</li>
<li class="icon-arrows-cw">Reports easily export to excel</li>
</ul>
</div>
<div class="col-md-6" style="
    
    padding: 1% 2% 1% 2%;
    background-color: #C3C3C3;
    color: #000;
    display: block; height:465px;
   
">
<h4>The Avacom Advantage</h4>
<ul class="list wow animated animated animated" style="visibility: visible;">
<li class="icon-arrows-cw">Accessible -Access anywhere</li>
<li class="icon-arrows-cw">Configurable -Fits perfectly to the org structure</li>
<li>Scalable –Easily add Acts/ compliances/ business units</li>
<li class="icon-arrows-cw">Flexible –Admin privilege in-house to quickly adapt when employee role changes</li>
<li class="icon-arrows-cw">Comprehensive –Most complete database of Acts and compliances, updated continuously</li>
<li class="icon-arrows-cw">Audit-friendly –Never hunt for a file again! All documents in one place</li>
<li class="icon-arrows-cw">Secure –Prevents breach and maintains full disaster recovery</li>
<li class="icon-arrows-cw" style="margin-bottom:29px;">Easy reporting –Click a button to create reports or print<br>
<br><br>
</li>

</ul>
</div>
</div>


			
	</div>
	<!-- /Container -->
	</section>
	<!-- /Main Content -->
</div>
	<!-- /Avantis Conten Inner -->
	<!-- Footer -->
<footer id="footer">
<!-- Upper Footer -->

<!-- /Upper Footer -->
<!-- Main Footer -->
<div id="main-footer" class="smallest-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div id="text-2" class="widget widget_text">
					<div class="textwidget">
						<img src="../Images/img/logo.png" alt="logo">
						<p>
							Avantis’ Compliance Product provides a comprehensive Solution to Organization’s complete compliance requirements. It is a secured web based product which can be accessed by all business and management users over the internet
						</p>
					</div>
				</div>
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div id="text-4" class="widget widget_text">
					<h4>Contact</h4>
					<div class="textwidget">
						<ul class="iconic-list">
							<li>
							<i class="icons icon-location-7"></i>
							Pune, Maharashtra, India
							<li>
							<i class="icons icon-mobile-6"></i>
							Phone: +91 98234 92350 <br>
							 Fax: +91 98234 92350 </li>
							<li>
							<i class="icons icon-mail-7"></i>
							sandeep@avantis.co.in </li>
						</ul>
					</div>
				</div>
			</div>
			<%--<div class="col-lg-4 col-md-4 col-sm-4">
				<h4>Login</h4>
				<form class="get-in-touch contact-form wow animated fadeInUp align-center animated" method="post" style="visibility: visible; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
						<input type="hidden" id="contact_nonce" name="contact_nonce" value="68592e213c"><input type="hidden" name="" value="/">
						<input type="hidden" name="contact-form-value" value="1">
						<div class="iconic-input">
							<input type="text" name="name" placeholder="Name*">
							<i class="icons icon-user-1"></i>
						</div>
						<div class="iconic-input">
							<input type="password" name="passowrd" placeholder="passowrd*">
							<i class="icons icon-email"></i>
						</div>
						<input type="submit" value="Login" style="float:right;">
						
					</form>
			</div>--%>
		</div>
	</div>
</div>
<!-- /Main Footer -->
<!-- Lower Footer -->
<div id="lower-footer">
	<div class="container">
		<span class="copyright">© 2014 Avantis. All Rights Reserved</span>
	</div>
</div>
<!-- /Lower Footer -->
</footer>
	<!-- /Footer -->
</div>
<!-- /Content Wrapper -->
<!-- JavaScript -->
<script type='text/javascript' src='../Scripts/New/bootstrap.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery-ui.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.mousewheel.min.js'></script>
<script type='text/javascript' src='../Scripts/New/SmoothScroll.min.js'></script>
<script type='text/javascript' src='../Scripts/New/prettyphoto/js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='../Scripts/New/modernizr.js'></script>
<script type='text/javascript' src='../Scripts/New/wow.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.sharre.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.flexslider-min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.knob.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.mixitup.min.js'></script>
<script type='text/javascript' src='../Scripts/New/masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='../Scripts/New/jquery.masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='../Scripts/New/jquery.fitvids.js'></script>
<script type='text/javascript' src='../Scripts/New/perfect-scrollbar-0.4.10.with-mousewheel.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.nouislider.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.validity.min.js'></script>
<script type='text/javascript' src='../Scripts/New/tweetie.min.js'></script>
<script type='text/javascript' src='../Scripts/New/script.js'></script>
</body>
</html>
