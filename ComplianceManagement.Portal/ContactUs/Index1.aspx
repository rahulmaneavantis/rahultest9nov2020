﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index1.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContactUs.Index1" %>

<!DOCTYPE html>
<!--[if IE 8 ]><html lang="en" class="isie ie8 oldie no-js"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="isie ie9 no-js"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<!-- Meta Tags -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Avantis Main 1">
<meta name="keywords" content="Homepage, Avantis">
<!-- Title -->
<title>Avantis</title>
<!-- Favicon -->

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,800,900' rel='stylesheet' type='text/css'>
<!-- Stylesheets -->
<link rel='stylesheet' id='twitter-bootstrap-css' href='../Style/NewCss/bootstrap.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='fontello-css' href='../Style/NewCss/fontello.css' type='text/css' media='all'/>
<link rel='stylesheet' id='prettyphoto-css-css' href='../Scripts/New/prettyphoto/css/prettyPhoto.css' type='text/css' media='all'/>
<link rel='stylesheet' id='animation-css' href='../Style/NewCss/animation.css' type='text/css' media='all'/>
<link rel='stylesheet' id='flexSlider-css' href='../Style/NewCss/flexslider.css' type='text/css' media='all'/>
<link rel='stylesheet' id='perfectscrollbar-css' href='../Style/NewCss/perfect-scrollbar-0.4.10.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-validity-css' href='../Style/NewCss/jquery.validity.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-ui-css' href='../Style/NewCss/jquery-ui.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='style-css' href='../Style/NewCss/style.css' type='text/css' media='all'/>
<link rel='stylesheet' id='mobilenav-css' href='../Style/NewCss/mobilenav.css' type='text/css' media="screen and (max-width: 838px)"/>
<link rel="stylesheet" type="text/css" href="../Style/NewCss/style.revslider.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="../Scripts/New/rs-plugin/css/settings.css" media="screen"/>
<!-- jQuery -->
<script src="../Scripts/New/jquery-1.11.1.min.js" type="text/javascript"></script>

<!--[if lt IE 9]>
            <script>
                document.createElement("header");
                document.createElement("nav");
                document.createElement("section");
                document.createElement("article");
                document.createElement("aside");
                document.createElement("footer");
                document.createElement("hgroup");
            </script>
        <![endif]-->
<!--[if lt IE 9]>
            <script src="js/html5.js"></script>
        <![endif]-->
<!--[if lt IE 7]>
            <script src="js/icomoon.js"></script>
        <![endif]-->
<!--[if lt IE 9]>
            <link href="css/ie.css" rel="stylesheet">
        <![endif]-->
<!--[if lt IE 9]>
            <script src="js/jquery.placeholder.js"></script>
            <script src="js/script_ie.js"></script>
        <![endif]-->
</head>
<body class="w1170 headerstyle1">
<div id="Avantis-content-wrapper">
    <!-- Header -->
<header id="header" class="style1">
<!-- Main Header -->
<div id="main-header">
	<div class="container">
		<div class="row">
			<!-- Logo -->
			<div class="col-lg-4 col-md-4 col-sm-4 logo">
				<a href="Index.aspx" title="Avantis" rel="home"><img class="logo" src="../Images/img/logo.png" alt="Avantis"></a>
				<div id="main-nav-button">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="col-sm-8 align-right">
            <ul  class="menu top_menu" style="border-right:none;margin: 25px 0px 0px;">
					<li class="menu-item"><a href="Index.aspx">JOIN US</a></li>
                    <li class="menu-item"><a href="../Login.aspx">CUSTOMER LOGIN</a></li>
					<li class="menu-item"><a href="ContactUs.aspx">CONTACT US</a></li>
				</ul>
				<!-- Text List -->
				<!--<ul class="text-list">
					<li>Call Us: +91 98234 92350</li>
				</ul>-->
				<!-- Social Media -->
				<!--<ul class="social-media">
					<li><a target="_blank" href="#"><i class="icon-facebook"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-twitter"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-google"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-linkedin"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-instagram"></i></a>
					</li>
				</ul>-->
			</div>
		</div>
	</div>
</div>
<!-- /Main Header -->
<!-- Lower Header -->
<div id="lower-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="lower-logo">
					<a href="Index.aspx" title="Avantis" rel="home"><img class="logo" src="../Images/img/logo.png" alt="Avantis"></a>
				</div>
				<!-- Main Navigation -->
				<ul id="main-nav" class="menu">
					<li class="menu-item"><a href="Index.aspx" style="color: #0294D3;">Home</a></li>
                    <li class="menu-item"><a href="Product.aspx">Product</a></li>
                    <li class="menu-item"><a href="AboutUs.aspx">About</a></li>
                    <li class="menu-item"><a href="#">Case Studies</a></li>
					<li class="menu-item"><a href="#">Blog</a></li>
				</ul>
				<!-- /Main Navigation -->
				<!-- Search Box -->
				<div id="search-box" class="align-right">
					<i class="icons icon-search"></i>
					<form role="search" method="get" id="searchform" action="#">
						<input type="text" name="s" placeholder="Search here..">
						<div class="iconic-submit">
							<div class="icon">
								<i class="icons icon-search"></i>
							</div>
							<input type="submit" value="">
						</div>
					</form>
				</div>
				<!-- /Search Box -->
			</div>
		</div>
	</div>
</div>
<!-- /Lower Header -->
</header>
    <!-- /Header -->
    
<div id="Avantis-content-inner">
	<!-- Main Content -->
	<section id="main-content">
	<section id="slider">
		<div class="container">
			<!-- START REVOLUTION SLIDER 4.6.0 fullwidth mode -->
			<div id="rev_slider_22_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#eee;padding:0px;margin-top:0px;margin-bottom:0px;max-height:810px;">
				<div id="rev_slider_22_1" class="rev_slider fullwidthabanner" style="display:none;max-height:810px;height:810px;">
					<ul>	<!-- SLIDE  -->
						<li data-transition="fade" data-slotamount="7" data-masterspeed="700" data-delay="10400"> 
							<!-- MAIN IMAGE -->
							<img src="../Images/img/rs-images/dummy.png"  alt="slide1-2" data-lazyload="../Images/img/new/slide1.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
							<!-- LAYERS -->
							<!-- LAYER NR. 1 -->
							<div class="tp-caption tp-fade rs-parallaxlevel-3" data-x="center" data-hoffset="1" data-y="45"  data-speed="1000" data-start="1100" data-easing="Power4.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500"
							style="z-index: 2;"><img src="../Images/img/rs-images/dummy.png" alt="" data-lazyload="../Images/img/logo-avacom.png">
							</div>
							<!-- LAYER NR. 2 -->
							<div class="tp-caption big_extraheavy_60 skewfromrightshort tp-resizeme rs-parallaxlevel-0 customin customout" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:-100;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-x="center" data-hoffset="0" data-y="305" data-speed="700" data-start="500" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap; font-size:54px;">Never miss a compliance again</div>
							<!-- LAYER NR. 3 -->
							<!--<div class="tp-caption extrabold_littlesub_open customin tp-resizeme rs-parallaxlevel-0" data-x="center" data-hoffset="0" data-y="385" 
								data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-speed="300" data-start="2250" data-easing="Power3.easeInOut" data-splitin="none"
								data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300"
							style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">VIVAMUS ORCI SEM, CONSECTETUR UT VESTIBULUM.
							</div>-->
							<!-- LAYER NR. 4 -->
							<!--<div class="tp-caption sfb rs-parallaxlevel-0" data-x="center" data-hoffset="0" data-y="380"  data-speed="300" data-start="2250" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300"
							style="z-index: 5;"><img src="img/rs-images/dummy.png" alt="" data-lazyload="img/rs-images/lines.png">
							</div>-->
						</li>
                        <li data-transition="fade" data-slotamount="7" data-delay="10400" data-masterspeed="700" >
							<!-- MAIN IMAGE -->
							<img src="img/rs-images/dummy.png"  alt="slide1-2" data-lazyload="../Images/img/new/slide1.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
							<!-- LAYERS -->
							<!-- LAYER NR. 1 -->
							<div class="tp-caption tp-fade rs-parallaxlevel-3" data-x="center" data-hoffset="1" data-y="45"  data-speed="1000" data-start="1100" data-easing="Power4.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500"
							style="z-index: 2;"><img src="img/rs-images/dummy.png" alt="" data-lazyload="../Images/img/logo-avacom.png">
							</div>
							<!-- LAYER NR. 2 -->
							<div class="tp-caption big_extraheavy_60 skewfromrightshort tp-resizeme rs-parallaxlevel-0 customin customout" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:-100;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-x="center" data-hoffset="0" data-y="305" data-speed="700" data-start="500" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="500" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;font-size:54px;">With Avacom it is simple</div>
							<!-- LAYER NR. 3 -->
							<!--<div class="tp-caption extrabold_littlesub_open customin tp-resizeme rs-parallaxlevel-0" data-x="center" data-hoffset="0" data-y="385" 
								data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-speed="300" data-start="2250" data-easing="Power3.easeInOut" data-splitin="none"
								data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300"
							style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">VIVAMUS ORCI SEM, CONSECTETUR UT VESTIBULUM.
							</div>-->
							<!-- LAYER NR. 4 -->
							<!--<div class="tp-caption sfb rs-parallaxlevel-0" data-x="center" data-hoffset="0" data-y="380"  data-speed="300" data-start="2250" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300"
							style="z-index: 5;"><img src="img/rs-images/dummy.png" alt="" data-lazyload="img/rs-images/lines.png">
							</div>-->
						</li>
						<!-- SLIDE  -->
						<!--<li data-transition="random" data-slotamount="7" data-masterspeed="300" data-thumb="img/rs-images/slide1-2-320x200.jpg"  data-saveperformance="off"  data-title="Slide">
							
							<img src="img/rs-images/dummy.png"  alt="slide1-2" data-lazyload="img/new/slide2.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
							<div class="tp-caption tp-fade rs-parallaxlevel-3" data-x="center" data-hoffset="1" data-y="155"  data-speed="800" data-start="1600" data-easing="Power4.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300"
							style="z-index: 2;"><img src="img/rs-images/dummy.png" alt="" data-lazyload="img/logo.png">
							</div>
							
							<div class="tp-caption big_extraheavy_60 skewfromrightshort tp-resizeme rs-parallaxlevel-0 customin customout" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:-100;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-x="center" data-hoffset="0" data-y="285" data-speed="500" data-start="1100" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">Manage your compliance effectively</div>
														<div class="tp-caption extrabold_littlesub_open customin tp-resizeme rs-parallaxlevel-0" data-x="center" data-hoffset="0" data-y="385" 
								data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-speed="300" data-start="2250" data-easing="Power3.easeInOut" data-splitin="none"
								data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300"
							style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">VIVAMUS ORCI SEM, CONSECTETUR UT VESTIBULUM.
							</div>
							
							<div class="tp-caption sfb rs-parallaxlevel-0" data-x="center" data-hoffset="0" data-y="380"  data-speed="300" data-start="2250" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300"
							style="z-index: 5;"><img src="img/rs-images/dummy.png" alt="" data-lazyload="img/rs-images/lines.png">
							</div>
						</li>-->
                        
					<div class="tp-bannertimer"></div>	</div>

					<script type="text/javascript">
					    /******************************************
					    -	PREPARE PLACEHOLDER FOR SLIDER	-
					    ******************************************/
					    var setREVStartSize = function () {
					        var tpopt = new Object();
					        tpopt.startwidth = 1170;
					        tpopt.startheight = 810;
					        tpopt.container = jQuery('#rev_slider_22_1');
					        tpopt.fullScreen = "off";
					        tpopt.forceFullWidth = "off";
					        tpopt.container.closest(".rev_slider_wrapper").css({ height: tpopt.container.height() }); tpopt.width = parseInt(tpopt.container.width(), 0); tpopt.height = parseInt(tpopt.container.height(), 0); tpopt.bw = tpopt.width / tpopt.startwidth; tpopt.bh = tpopt.height / tpopt.startheight; if (tpopt.bh > tpopt.bw) tpopt.bh = tpopt.bw; if (tpopt.bh < tpopt.bw) tpopt.bw = tpopt.bh; if (tpopt.bw < tpopt.bh) tpopt.bh = tpopt.bw; if (tpopt.bh > 1) { tpopt.bw = 1; tpopt.bh = 1 } if (tpopt.bw > 1) { tpopt.bw = 1; tpopt.bh = 1 } tpopt.height = Math.round(tpopt.startheight * (tpopt.width / tpopt.startwidth)); if (tpopt.height > tpopt.startheight && tpopt.autoHeight != "on") tpopt.height = tpopt.startheight; if (tpopt.fullScreen == "on") { tpopt.height = tpopt.bw * tpopt.startheight; var cow = tpopt.container.parent().width(); var coh = jQuery(window).height(); if (tpopt.fullScreenOffsetContainer != undefined) { try { var offcontainers = tpopt.fullScreenOffsetContainer.split(","); jQuery.each(offcontainers, function (e, t) { coh = coh - jQuery(t).outerHeight(true); if (coh < tpopt.minFullScreenHeight) coh = tpopt.minFullScreenHeight }) } catch (e) { } } tpopt.container.parent().height(coh); tpopt.container.height(coh); tpopt.container.closest(".rev_slider_wrapper").height(coh); tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh); tpopt.container.css({ height: "100%" }); tpopt.height = coh; } else { tpopt.container.height(tpopt.height); tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height); tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height); }
					    };
					    /* CALL PLACEHOLDER */
					    setREVStartSize();
					    var tpj = jQuery;
					    tpj.noConflict();
					    var revapi22;
					    tpj(document).ready(function () {
					        if (tpj('#rev_slider_22_1').revolution == undefined)
					            revslider_showDoubleJqueryError('#rev_slider_22_1');
					        else
					            revapi22 = tpj('#rev_slider_22_1').show().revolution(
							{
							    dottedOverlay: "none",
							    delay: 16000,
							    startwidth: 1170,
							    startheight: 810,
							    hideThumbs: "on",
							    thumbWidth: 100,
							    thumbHeight: 50,
							    thumbAmount: 2,
							    simplifyAll: "off",
							    navigationType: "none",
							    navigationArrows: "solo",
							    navigationStyle: "preview4",
							    touchenabled: "on",
							    onHoverStop: "on",
							    nextSlideOnWindowFocus: "off",
							    swipe_threshold: 0.7,
							    swipe_min_touches: 1,
							    drag_block_vertical: false,
							    parallax: "mouse",
							    parallaxBgFreeze: "on",
							    parallaxLevels: [4, 5, 3, 2, 5, 4, 3, 5, 5, 0],
							    parallaxDisableOnMobile: "on",
							    keyboardNavigation: "off",
							    navigationHAlign: "center",
							    navigationVAlign: "bottom",
							    navigationHOffset: 0,
							    navigationVOffset: 20,
							    soloArrowLeftHalign: "left",
							    soloArrowLeftValign: "center",
							    soloArrowLeftHOffset: 20,
							    soloArrowLeftVOffset: 0,
							    soloArrowRightHalign: "right",
							    soloArrowRightValign: "center",
							    soloArrowRightHOffset: 20,
							    soloArrowRightVOffset: 0,
							    shadow: 0,
							    fullWidth: "on",
							    fullScreen: "off",
							    spinner: "spinner4",
							    stopLoop: "off",
							    stopAfterLoops: -1,
							    stopAtSlide: -1,
							    shuffle: "off",
							    autoHeight: "off",
							    forceFullWidth: "off",
							    hideThumbsOnMobile: "off",
							    hideNavDelayOnMobile: 1500,
							    hideBulletsOnMobile: "off",
							    hideArrowsOnMobile: "off",
							    hideThumbsUnderResolution: 0,
							    hideSliderAtLimit: 0,
							    hideCaptionAtLimit: 0,
							    hideAllCaptionAtLilmit: 0,
							    startWithSlide: 0
							});
					    }); /*ready*/
					</script>
			</div><!-- END REVOLUTION SLIDER -->
			<script>
			    /* Fix The Revolution Slider Loading Height issue */
			    jQuery(document).ready(function ($) {
			        $('.rev_slider_wrapper').each(function () {
			            $(this).css('height', '');
			            var revStartHeight = parseInt($('>.rev_slider', this).css('height'));
			            $(this).height(revStartHeight);
			            $(this).parents('#slider').height(revStartHeight);

			            $(window).load(function () {
			                $('#slider').css('height', '');
			            });
			        });
			    });
			</script>
		</div>
    </section>
	<!-- Container -->
	<div class="container">
		<section class="full-width-bg blue-gradient-bg normal-padding alternate-slider-bg Avantis-main-top-iconbox">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="service style1">
						<div class="service-icon">
							<img src="../Images/img/new/a.png" width="42" height="42">
						</div>
						<h3>New Governance Requirements</h3>
						<div class="content_box">Clause 49 1 C (i) Are you ready?</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="service style1">
						<div class="service-icon">
							<img src="../Images/img/new/b.png" width="42" height="42">
						</div>
						<h3>Strict requirements</h3>
						<div class="content_box">For unlisted public and private companies</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="service style1">
						<div class="service-icon">
							<img src="../Images/img/new/c.png" width="42" height="42">
						</div>
						<h3>Liability</h3>
						<div class="content_box">Onus on Independent Director’s, Auditors and Audit

Committee</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="service style1">
						<div class="service-icon">
							<img src="../Images/img/new/d.png" width="42" height="42">
						</div>
						<h3>Penalties</h3>
						<div class="content_box">Severe consequences for non-compliance</div>
					</div>
				</div>
			</div>
		</section>
		<div class="row">
			<section class="col-lg-12 col-md-12 col-sm-12 small-padding">
			<div class="full_bg full-width-bg Avantis-main-bg" data-animation="">
				<h2 class="special-text Avantis-main-special-h3" data-animation="">With Avacom it's this simple</h2>
				<div class="special-text Avantis-main-special" data-animation="" style="font-size:15px; font-weight:bold;">
					Avacom is an incredibly flexible statutory compliance software. Now preloaded with 200+ Acts. You get to reduce costs, improve efficiencies, and 
increase visibility across core compliance functions. With avacom you maximise top and bottom lines by reducing noncompliance fines and penalties
				</div>
				<div class="clearfix clearfix-height40">
			</div><div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 service">
               <div class="wow animated fadeInUp animated" data-animation="fadeInUp" data-duration:"0.5s" data-start="1600" data-elementdelay="0.1" data-endelementdelay="0.1"  style="visibility: visible; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;animation-delay: 0.5s; -webkit-animation-delay: 0.5s; "> <h3 style="color: #32ACC0; font-weight: 400; margin: 15px 0px;">Analyse</h3>
                <img src="../Images/img/process-02.png" style="display:block; margin:0px auto; width:45%;">
             
<ul class="list wow animated animated" style="visibility: visible; margin: 15px 0px; color:#FFF;">
						<li class="icon-arrows-cw">Understand business</li>
<li class="icon-arrows-cw">Document the compliance structure/ organogram</li>
<li class="icon-arrows-cw">Identify applicable compliances</li>
</ul></div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 service"><div class="wow animated fadeInUp animated" data-duration:"0.10s" data-start="3000" data-elementdelay="0.3" data-endelementdelay="0.3"  data-animation="fadeInUp" style="visibility: visible; animation-name: fadeInUp; -webkit-animation-name: fadeInUp; animation-delay: 1s; -webkit-animation-delay: 1s;">
                <h3 style="color: #32ACC0; font-weight: 400; margin: 15px 0px;">Deploy</h3>                <img src="../Images/img/process-01.png" style="display:block; margin:0px auto; width:45%;">
					<ul class="list wow animated animated" style="visibility: visible; margin: 15px 0px; color:#FFF;">
                    <li class="icon-arrows-cw">Configure solution by mapping compliances</li>

<li class="icon-arrows-cw">Implement Avacom</li></ul></div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 service"><div class="wow animated fadeInUp animated" data-start="4400" data-elementdelay="0.5" data-duration:"0.15s" data-endelementdelay="0.5"  data-animation="fadeInUp" style="visibility: visible; animation-name: fadeInUp; -webkit-animation-name: fadeInUp; animation-delay: 1.5s; -webkit-animation-delay: 1.5s;">
                <h3 style="color: #32ACC0; font-weight: 400; margin: 15px 0px;">Support</h3>                <img src="../Images/img/process-03.png" style="display:block; margin:0px auto; width:45%;">
					<ul class="list wow animated animated" style="visibility: visible; margin: 15px 0px; color:#FFF;">
                    <li class="icon-arrows-cw">User training & acceptance</li>
<li class="icon-arrows-cw">Software support handholding</li>
<li class="icon-arrows-cw">Continuous enhancements
&<br>supportthrough AMC</li> </ul></div>
				</div>
			</div><div class="clearfix clearfix-height40">
			</div>
				<!--<section class="full-width projects-section dark-gray-bg ">
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="project">
						<div class="project-image wow animated fadeInLeft">
							<img src="img/new/02.jpg" alt="Team Spirit"/>
							<div class="project-hover">
								<a class="link-icon" href="#"></a>
								<a class="search-icon prettyPhoto" href="img/new/02.jpg"></a>
							</div>
						</div>
						<div class="project-meta wow animated fadeInLeft" data-wow-offset="0">
							<h4>Team Spirit</h4>
							<span class="project-category">Print</span>
							<div class="project-like" data-post="3820">
								<i class=" icons icon-heart-7"></i>
								<span class="like-count">162</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="project">
						<div class="project-image wow animated fadeInLeft">
							<img src="img/new/01.jpg" alt="Sample Item"/>
							<div class="project-hover">
								<a class="link-icon" href="#"></a>
								<a class="search-icon prettyPhoto" href="img/new/01.jpg"></a>
							</div>
						</div>
						<div class="project-meta wow animated fadeInLeft" data-wow-offset="0">
							<h4>Sample Item</h4>
							<span class="project-category">Print Web</span>
							<div class="project-like" data-post="3767">
								<i class=" icons icon-heart-7"></i>
								<span class="like-count">207</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="project">
						<div class="project-image wow animated fadeInLeft">
							<img src="img/new/03.jpg" alt="Project Title"/>
							<div class="project-hover">
								<a class="link-icon" href="#"></a>
								<a class="search-icon prettyPhoto" href="img/new/03.jpg"></a>
							</div>
						</div>
						<div class="project-meta wow animated fadeInLeft" data-wow-offset="0">
							<h4>Project Title</h4>
							<span class="project-category">Print Web Print</span>
							<div class="project-like" data-post="3605">
								<i class=" icons icon-heart-7"></i>
								<span class="like-count">133</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="project">
						<div class="project-image wow animated fadeInLeft">
							<img src="img/new/04.jpg" alt="Maecenas sodales"/>
							<div class="project-hover">
								<a class="link-icon" href="#"></a>
								<a class="search-icon prettyPhoto" href="img/new/04.jpg"></a>
							</div>
						</div>
						<div class="project-meta wow animated fadeInLeft" data-wow-offset="0">
							<h4>Maecenas sodales</h4>
							<span class="project-category">Print Web Print Business</span>
							<div class="project-like" data-post="2422">
								<i class=" icons icon-heart-7"></i>
								<span class="like-count">88</span>
							</div>
						</div>
					</div>
				</div>
				</section>-->
				<div class="clearfix">
				</div>
				<div class="clearfix">
				</div>
			</div>
            <!--<div class="clearfix clearfix-height40">
			</div>-->
			<section class="sc-call-to-action full-width-bg light-gray-bg small-padding Avantis-main-call-action">
			<div class="row">
				<div class="col-lg-9 col-md-9 col-sm-9">
					
                    <p>We understand the complex compliance environment your company is operating under. You are subject to central, state, local and panchayat level compliances in labour, financial, environmental and secretarial categories. Documentation may be required at monthly, quarterly, half-yearly, annual or bi-annual frequencies, that too from different business units, geographies and hierarchical structures. Penalties range from a small fine to prison term for the directors. The demands of compliance are far from simple. But there is a highly cost effective, efficient and simple solution. Enter Avacom.</p>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 align-right">
					<a href="ContactUs.aspx" class="button biggest"><i class="icons icon-info-circled-alt"></i>Ask For Demo</a>
				</div>
			</div>
			</section>
			<div class="clearfix clearfix-height40">
			</div><div class="row"><div class="col-lg-6 col-md-6 col-sm-12"><h3>AVANTIS</h3>
            <p>Avantis is a products company born out of the experience of its founders

and advisors Sandeep, Chetan and Rishi. Avantis makes intuitive, scalable 

and flexible products to alleviate business pain-points by leveraging 

technology cost-effectively.</p><a href="AboutUs.aspx" class="button small"><i class="icons icon-info-circled-alt"></i>READ MORE</a></div>
            <div class="col-lg-6 col-md-6 col-sm-12"><h3>AVACOM</h3><p>Avacom provides highly automated and flexible compliance solution to organisations in 

diverse industries like Avantis, manufacturing, IT, media and financial institutions. It 

allows access to compliance monitoring dashboards from anywhere on the planet. If you 

can launch a browser you can access avacom.</p><a href="Product.aspx" class="button small"><i class="icons icon-info-circled-alt"></i>READ MORE</a></div>
            </div>
			<!--<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<h3>Recent Posts</h3>
					<div class="blog-post recent-post wow animated fadeInDown">
						<div class="post-image">
							<div class="recent-post-meta">
								<span class="post-date">
								<span class="post-day">11</span><br>
								 Jun, 2014 </span>
								<span class="post-format">
								<span class="photo-icon"></span></span>
							</div>
							<div class="post-thumbnail">
								<a href="#" title="Single Blog Post"><img src="https://placehold.it/130x130" alt="Single Blog Post"/></a>
							</div>
						</div>
						<div class="post-content">
							<ul class="post-meta">
								<li>Byadmin</li>
								<li><a href="#">Business</a></li>
								<li>No comments.</li>
								<li><a class="read-more" href="#">Read more</a></li>
							</ul>
							<h4><a href="blog-single.html">Single Blog Post</a></h4>
							 Suspendisse ornare tincidunt massa et malesuada. Integer consequat suscipit velit quis
						</div>
					</div>
					<div class="blog-post recent-post wow animated fadeInDown">
						<div class="post-image">
							<div class="recent-post-meta">
								<span class="post-date">
								<span class="post-day">08</span><br>
								 Jun, 2014 </span>
								<span class="post-format">
								<span class="document-icon"></span></span>
							</div>
							<div class="post-thumbnail">
								<a href="#" title="A Simple Text Post"><img src="https://placehold.it/130x130" alt="A Simple Text Post"/></a>
							</div>
						</div>
						<div class="post-content">
							<ul class="post-meta">
								<li>Byadmin</li>
								<li><a href="#">Business</a></li>
								<li>1 comment.</li>
								<li><a class="read-more" href="#">Read more</a></li>
							</ul>
							<h4><a href="blog-single.html">A Simple Text Post</a></h4>
							 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed eleifend urna
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<h3>Accordion</h3>
					<div class="accordions">
						<div class="accordion accordion-active">
							<div class="accordion-header">
								<div class="accordion-icon">
								</div>
								<h5>Vivamus orci sem sectetur</h5>
							</div>
							<div class="accordion-content display-block">
								<p>
									 Sed entum velit vel ipsum bibendum em lacus, itor et aliquam eget, iaculis id lacus. Praesent tudin.
								</p>
								<p>
									 Aiquam eget, iaculis id lacus. Praesent tudin. Ut sem lacus, ttitor putate uam mi nec hendrerit.
								</p>
							</div>
						</div>
						<div class="accordion ">
							<div class="accordion-header">
								<div class="accordion-icon">
								</div>
								<h5>Sed entum velit vel ipsum</h5>
							</div>
							<div class="accordion-content">
								 Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
							</div>
						</div>
						<div class="accordion ">
							<div class="accordion-header">
								<div class="accordion-icon">
								</div>
								<h5>Portittor et aliquam eget</h5>
							</div>
							<div class="accordion-content">
								 Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<h3>What People Say</h3>
					<div class="testimonial wow animated fadeInUp">
						<div class="testimonial-header">
							<div class="testimonial-image">
								<img src="https://placehold.it/81x81" alt="Jack Wills"/>
							</div>
							<div class="testimonial-meta">
								<span class="testimonial-author">Jack Wills</span>
								<span class="testimonial-job">CEO, X Studioz</span>
							</div>
						</div>
						<blockquote class="testimonial-quote">Vivamus orci sem, consectetur ut vestibulum</blockquote>
						<div class="testimonial-desc">
							<p>
								 Vivamus orci sem, consectetur ut vestibulum a, semper ac dui. Proin vulputate aliquam mi nec rerit.
							</p>
							<p>
								 Sed entum velit vel ipsum bibendum tristique. Ut sem lacus, ttitor putate liquam.
							</p>
						</div>
					</div>
				</div>
			</div>-->
			<div class="clearfix clearfix-height40">
			</div>
			<div class="full_bg full-width-bg Avantis-main-bg1" style="background-image:url(../Images/img/bg1a.jpg)" data-animation="">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<div class="clearfix clearfix-height30">
					</div>
					<img src="../Images/img/bg1b.png" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<ul class="services-list ">
						<li>
						<i class="icons icon-clock" style="color: #8ad8fe; padding: 10px 0px;"></i>
						<h3 class="Avantis-header6-h3-color">Timely </h3>
						<p class="Avantis-main-p-col">
							 With email reminders and dashboard alerts
						</p>
						</li>
						<li>
						<i class="icons icon-target" style="color: #8ad8fe; padding: 10px 0px;"></i>
						<h3 class="Avantis-header6-h3-color">Accurate</h3>
						<p class="Avantis-main-p-color">
							 Automatic updating of compliance and acts database year-round
						</p>
						</li>
						<li>
						<i class="icons icon-check-outline" style="color: #8ad8fe; padding: 10px 0px;"></i>
						<h3 class="Avantis-header6-h3-color">Complete </h3>
						<p class="Avantis-main-p-color">
							Risk management and disaster recovery is built-in. All documents uploaded and saved in one place.
						</p>
						</li>
                        <li>
						<i class="icons icon-dollar" style="color: #8ad8fe; padding: 10px 0px;"></i>
						<h3 class="Avantis-header6-h3-color">Accountable</h3>
						<p class="Avantis-main-p-color">
							Each compliance is assigned to a Performer, and also has a Reviewer and an Approver. Specific dashboards for each role. 
						</p>
						</li>
					</ul>
					<div class="clearfix">
					</div>
				</div>
				<div class="clearfix">
				</div>
			</div>
			
			<!--<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6">
					<img src="img/c1.jpg" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<img src="img/c2.jpg" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<img src="img/c3.jpg" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<img src="img/c4.jpg" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
				</div>
			</div>-->
			<!--<div class="clearfix clearfix-height30">
			</div>-->
			<div class="full_bg full-width-bg Avantis-map-bg" data-animation="" style="background-image:url(../Images/img/map.jpg)">
				<h3 class="special-text wow animated fadeInLeft form-map-h3" data-animation="fadeInLeft">Get In Touch!</h3>
				<div class="col-lg-6 col-md-6 col-sm-6 col-lg-push-3 col-md-push-3 col-sm-push-3">
					<form class="get-in-touch contact-form wow animated fadeInUp align-center" method="post">
						<input type="hidden" id="contact_nonce" name="contact_nonce" value="68592e213c"/><input type="hidden" name="" value="/"/>
						<input type="hidden" name="contact-form-value" value="1"/>
						<div class="iconic-input">
							<input type="text" name="name" placeholder="Name*">
							<i class="icons icon-user-1"></i>
						</div>
						<div class="iconic-input">
							<input type="text" name="email" placeholder="Email*">
							<i class="icons icon-email"></i>
						</div>
						<textarea name="msg" placeholder="Message"></textarea>
						<input type="submit" value="Send">
						<div class="iconic-button">
							<input type="reset" value="Clear">
							<i class="icons icon-cancel-circle-1"></i>
						</div>
					</form>
					<div id="msg">
					</div>
				</div>
				<div class="clearfix">
				</div>
			</div>
			</section>
		</div>
	</div>
	<!-- /Container -->
	</section>
	<!-- /Main Content -->
</div>
    <!-- /Avantis Conten Inner -->
    <!-- Footer -->
<footer id="footer">
<!-- Upper Footer -->

<!-- /Upper Footer -->
<!-- Main Footer -->
<div id="main-footer" class="smallest-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div id="text-2" class="widget widget_text">
					<div class="textwidget">
						<img src="../Images/img/logo.png" alt="logo">
						<p>
							Avantis’ Compliance Product provides a comprehensive Solution to Organization’s complete compliance requirements. It is a secured web based product which can be accessed by all business and management users over the internet
						</p>
					</div>
				</div>
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div id="text-4" class="widget widget_text">
					<h4>Contact</h4>
					<div class="textwidget">
						<ul class="iconic-list">
							<li>
							<i class="icons icon-location-7"></i>
							Pune, Maharashtra, India
							<li>
							<i class="icons icon-mobile-6"></i>
							Phone: +91 98234 92350 <br>
							 Fax: +91 98234 92350 </li>
							<li>
							<i class="icons icon-mail-7"></i>
							sandeep@avantis.co.in </li>
						</ul>
					</div>
				</div>
			</div>
			<%--<div class="col-lg-4 col-md-4 col-sm-4">
				<h4>Login</h4>
				<form class="get-in-touch contact-form wow animated fadeInUp align-center animated" method="post" style="visibility: visible; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
						<input type="hidden" id="contact_nonce" name="contact_nonce" value="68592e213c"><input type="hidden" name="" value="/">
						<input type="hidden" name="contact-form-value" value="1">
						<div class="iconic-input">
							<input type="text" name="name" placeholder="Name*">
							<i class="icons icon-user-1"></i>
						</div>
						<div class="iconic-input">
							<input type="password" name="passowrd" placeholder="passowrd*">
							<i class="icons icon-email"></i>
						</div>
						<input type="submit" value="Login" style="float:right;">
						
					</form>
			</div>--%>
		</div>
	</div>
</div>
<!-- /Main Footer -->
<!-- Lower Footer -->
<div id="lower-footer">
	<div class="container">
		<span class="copyright">© 2014 Avantis. All Rights Reserved</span>
	</div>
</div>
<!-- /Lower Footer -->
</footer>
    <!-- /Footer -->
</div>
<!-- /Content Wrapper -->
<!-- JavaScript -->
<script type='text/javascript' src='../Scripts/New/bootstrap.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery-ui.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.mousewheel.min.js'></script>
<script type='text/javascript' src='../Scripts/New/SmoothScroll.min.js'></script>
<script type='text/javascript' src='../Scripts/New/prettyphoto/js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='../Scripts/New/modernizr.js'></script>
<script type='text/javascript' src='../Scripts/New/wow.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.sharre.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.flexslider-min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.knob.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.mixitup.min.js'></script>
<script type='text/javascript' src='../Scripts/New/masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='../Scripts/New/jquery.masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='../Scripts/New/jquery.fitvids.js'></script>
<script type='text/javascript' src='../Scripts/New/perfect-scrollbar-0.4.10.with-mousewheel.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.nouislider.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.validity.min.js'></script>
<script type='text/javascript' src='../Scripts/New/tweetie.min.js'></script>
<script type='text/javascript' src='../Scripts/New/script.js'></script>
<script type='text/javascript' src='../Scripts/New/rs-plugin/js/jquery.themepunch.enablelog.js'></script>
<script type='text/javascript' src='../Scripts/New/rs-plugin/js/jquery.themepunch.revolution.js'></script>
<script type='text/javascript' src='../Scripts/New/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>
<script type='text/javascript' src='../Scripts/New/rs-plugin/js/jquery.themepunch.tools.min.js'></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.tp-banner').show().revolution(
	{
	    dottedOverlay: "none",
	    delay: 16000,
	    startwidth: 1170,
	    startheight: 700,
	    hideThumbs: 200,
	    thumbWidth: 100,
	    thumbHeight: 50,
	    thumbAmount: 5,
	    navigationType: "bullet",
	    navigationArrows: "solo",
	    navigationStyle: "preview2",
	    touchenabled: "on",
	    onHoverStop: "on",
	    swipe_velocity: 0.7,
	    swipe_min_touches: 1,
	    swipe_max_touches: 1,
	    drag_block_vertical: false,
	    parallax: "mouse",
	    parallaxBgFreeze: "on",
	    parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
	    keyboardNavigation: "off",
	    navigationHAlign: "center",
	    navigationVAlign: "bottom",
	    navigationHOffset: 0,
	    navigationVOffset: 20,
	    soloArrowLeftHalign: "left",
	    soloArrowLeftValign: "center",
	    soloArrowLeftHOffset: 20,
	    soloArrowLeftVOffset: 0,
	    soloArrowRightHalign: "right",
	    soloArrowRightValign: "center",
	    soloArrowRightHOffset: 20,
	    soloArrowRightVOffset: 0,
	    shadow: 0,
	    fullWidth: "on",
	    fullScreen: "off",
	    spinner: "spinner4",
	    stopLoop: "off",
	    stopAfterLoops: -1,
	    stopAtSlide: -1,
	    shuffle: "off",
	    autoHeight: "off",
	    forceFullWidth: "off",
	    hideThumbsOnMobile: "off",
	    hideNavDelayOnMobile: 1500,
	    hideBulletsOnMobile: "off",
	    hideArrowsOnMobile: "off",
	    hideThumbsUnderResolution: 0,
	    hideSliderAtLimit: 0,
	    hideCaptionAtLimit: 0,
	    hideAllCaptionAtLilmit: 0,
	    startWithSlide: 0,
	    videoJsPath: "rs-plugin/videojs/",
	    fullScreenOffsetContainer: ""
	});
    }); //ready
</script>
<!--<script src="js/hightlight.js"></script>-->
</body>
</html>
