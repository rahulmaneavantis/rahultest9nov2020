﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.CLRA
{
    public partial class PrincipleEmployerCreation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string BindClientList()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ClientDetails> ContractorDetailsList = new List<ClientDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var ClientObj = (from Client in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                     where Client.BranchType == "E"
                                     select Client).ToList();
                    foreach (var row in ClientObj)
                    {
                        ClientDetails Details = new ClientDetails();
                        Details.ClientID = row.CM_ClientID;
                        Details.ClientName = row.CM_ClientName;
                        ContractorDetailsList.Add(Details);
                    }
                    return serializer.Serialize(ContractorDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }
        [WebMethod]
        public static string SavePrincipleEmployerDetails(PrincipleEmployeeDetails DetailsObj)
        {
            try
            {
                bool Success = false;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (true)
                {
                    int id = Convert.ToInt32(DetailsObj.ID);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {

                        RLCS_PrincipleEmployerMaster updateDetails = (from row in entities.RLCS_PrincipleEmployerMaster
                                                                      where row.PEID == id
                                                                      select row).FirstOrDefault();
                        bool isact = false;
                        if (DetailsObj.IsCentralAct == "1")
                            isact = true;

                        if (updateDetails != null)
                        {
                            #region Update Details

                            updateDetails.PEName = DetailsObj.PEName;
                            updateDetails.NatureOfBusiness = DetailsObj.NatureOfBusiness;
                            updateDetails.ContractFrom = GetDate(DetailsObj.ContractFrom);
                            updateDetails.ContractTo = GetDate(DetailsObj.ContractTo);
                            updateDetails.NoOfEmployees = Convert.ToInt32(DetailsObj.NoOfEmployees);
                            updateDetails.Address = DetailsObj.Address;
                            updateDetails.ContractValue = Convert.ToDouble(DetailsObj.Contractvalue);
                            updateDetails.SecurityDeposit = Convert.ToDouble(DetailsObj.SecurityDeposit);
                            updateDetails.IsCentralAct = isact;
                            updateDetails.ModifiedDate = DateTime.Now;
                            updateDetails.ModifiedBy = "";
                            updateDetails.Status = DetailsObj.Status;

                            entities.SaveChanges();
                            Success = true;

                            #endregion


                        }
                        else
                        {
                            #region add new detail
                            bool isact1 = false;
                            if (DetailsObj.IsCentralAct == "1")
                                isact1 = true;


                            RLCS_PrincipleEmployerMaster PrincipleEmployerMaster = new RLCS_PrincipleEmployerMaster()
                            {
                                ClientID = DetailsObj.ClientID,
                                PEName = DetailsObj.PEName,
                                NatureOfBusiness = DetailsObj.NatureOfBusiness,
                                ContractFrom = GetDate(DetailsObj.ContractFrom),
                                ContractTo = GetDate(DetailsObj.ContractTo),
                                NoOfEmployees = Convert.ToInt32(DetailsObj.NoOfEmployees),
                                Address = DetailsObj.Address,
                                ContractValue = Convert.ToDouble(DetailsObj.Contractvalue),
                                SecurityDeposit = Convert.ToDouble(DetailsObj.SecurityDeposit),
                                IsCentralAct = isact1,
                                CreatedDate = DateTime.Now,
                                CreatedBy = "",
                                Status = DetailsObj.Status
                            };
                            entities.RLCS_PrincipleEmployerMaster.Add(PrincipleEmployerMaster);
                            entities.SaveChanges();

                            Success = true;
                            #endregion
                        }

                        return serializer.Serialize(Success);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }

        [WebMethod]
        public static string BindPrincipleEmployerTable(PrincipleEmployeeDetails DetailsObj)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<PrincipleEmployeeDetails> EmployerrDetailsList = new List<PrincipleEmployeeDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var EmployerObj = (from Employer in entities.RLCS_PrincipleEmployerMaster
                                       where Employer.ClientID == DetailsObj.ClientID
                                       select Employer).ToList();
                    foreach (var row in EmployerObj)
                    {
                        PrincipleEmployeeDetails Details = new PrincipleEmployeeDetails();
                        Details.ID = row.PEID;
                        Details.ClientID = row.ClientID;
                        Details.PEName = row.PEName;
                        Details.NatureOfBusiness = row.NatureOfBusiness;
                        Details.ContractFrom = Convert.ToDateTime(row.ContractFrom).ToString("dd/MM/yyyy");
                        Details.ContractTo = Convert.ToDateTime(row.ContractTo).ToString("dd/MM/yyyy");
                        if (Details.ContractFrom == "01-Jan-0001")
                            Details.ContractFrom = "";

                        if (Details.ContractTo == "01-Jan-0001")
                            Details.ContractTo = "";
                     
                        Details.NoOfEmployees = row.NoOfEmployees.ToString();
                        Details.Address = row.Address;
                        Details.Contractvalue = row.ContractValue.ToString();
                        Details.SecurityDeposit = row.SecurityDeposit.ToString();
                        Details.Status = row.Status.ToString();
                        Details.IsCentralAct = row.IsCentralAct.ToString();

                        EmployerrDetailsList.Add(Details);
                    }
                    
                    return serializer.Serialize(EmployerrDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }
        [WebMethod]
        public static string GetPrincipleEmployerDetails(PrincipleEmployeeDetails DetailsObj)
        {
            try
            {
                int PEID = Convert.ToInt32(DetailsObj.ID);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<PrincipleEmployeeDetails> EmployerDetailsList = new List<PrincipleEmployeeDetails>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var employerObj = (from employer in entities.RLCS_PrincipleEmployerMaster
                                         where employer.PEID == PEID
                                         select employer).ToList();

                    foreach (var row in employerObj)
                    {
                        PrincipleEmployeeDetails Details = new PrincipleEmployeeDetails();
                        Details.ID = row.PEID;
                        Details.ClientID = row.ClientID;
                        Details.PEName = row.PEName;
                        Details.NatureOfBusiness = row.NatureOfBusiness;
                        Details.ContractFrom = Convert.ToDateTime(row.ContractFrom).ToString();
                        Details.ContractTo = Convert.ToDateTime(row.ContractTo).ToString();
                        Details.NoOfEmployees = row.NoOfEmployees.ToString();
                        Details.Address = row.Address;
                        Details.Contractvalue = row.ContractValue.ToString();
                        Details.SecurityDeposit = row.SecurityDeposit.ToString();
                        Details.Status = row.Status.ToString();
                        Details.IsCentralAct = row.IsCentralAct.ToString();

                        EmployerDetailsList.Add(Details);
                    }

                    return serializer.Serialize(EmployerDetailsList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return ex.Message.ToString();
            }
        }
        public static DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public class PrincipleEmployeeDetails
        {
            public int ID { get; set; }
            public string ClientID { get; set; }
            public string PEName { get; set; }
            public string NatureOfBusiness { get; set; }
            public string ContractFrom { get; set; }
            public string ContractTo { get; set; }
            public string NoOfEmployees { get; set; }
            public string Address { get; set; }
            public string Contractvalue { get; set; }
            public string SecurityDeposit { get; set; }
            public string Status { get; set; }
            public string IsCentralAct { get; set; }

        }

        public class ClientDetails
        {
            public string ClientID { get; set; }
            public string ClientName { get; set; }
        }
    }
}