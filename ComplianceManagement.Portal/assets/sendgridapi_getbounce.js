var data = "{}";
var url = "";


function getBounces(){
  $('#response2').find("tr:gt(0)").remove();
  $('#response').find("tr:gt(0)").remove();
  var userinput = document.getElementById("emailToDelete").value
  if (userinput != '' && userinput != 'all'){
    url = "https://api.sendgrid.com/v3/suppression/bounces/"+userinput+"?";
  }
  
  else if (userinput == 'all'){
   url = "https://api.sendgrid.com/v3/suppression/bounces?";
  }
  
  else{
    $('#loader').hide();
    alert("Please enter an email address");
    return false;
  }
  
  url+="end_time=";
  url+= document.getElementById("unixEnd").value;
  url+="&start_time=";
  url+=document.getElementById("unixStart").value;
  //console.log(url)
  var xhr = new XMLHttpRequest();
  xhr.withCredentials = false;
  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === this.DONE) {
        //console.log("done");
    
    data = JSON.parse(this.responseText);
    console.log(data);
    var tr;
        
    for (var i = 0; i < data.length; i++){
      tr = $('<tr/>');
      tr.append("<td><input type='checkbox' id='"+data[i].email+"' /></td>");
      tr.append("<td>" + getFormattedTime(data[i].created) + "</td>");
      tr.append("<td>" + data[i].email + "</td>");
      tr.append("<td>" + data[i].reason + "</td>");
      tr.append("<td>" + data[i].status + "</td>");
      $('#response2').append(tr);}
      $('#loader').hide();
      bindCheckbox();
    }
  });
  xhr.open("GET", url);
  xhr.setRequestHeader("authorization", "Bearer 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");
  xhr.setRequestHeader("accept", "application/json");
  xhr.send(data);
}

function getFormattedTime(unixTime)
{var a = new Date(unixTime * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
  return time;
}


