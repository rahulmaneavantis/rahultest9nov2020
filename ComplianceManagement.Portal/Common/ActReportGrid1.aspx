﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ActReportGrid1.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.ActReportGrid1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
     <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
        <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <style type="text/css">
           .div.k-grid-footer, div.k-grid-header {
            border-top-style: solid;
            border-top-width: 1px;
        }
        .k-grid-content {
            min-height: 377px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        /*k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }*/
           k-draghandle.k-state-selected:hover, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            /*margin-top: -16px;*/
            color: inherit;
            margin: 0px 2px 2px 2px;
        }
     
        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
            min-height:30px;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
            margin-top:-33px;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
            margin-top: 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
              line-height: 14px;
        }
        .k-grid tbody tr {
            height: 38px;
        }
    </style>
       <style type="text/css">
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: #1fd9e1;
            background-color: #f7f7f7;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .panel-heading .nav > li > a {
            border-bottom: 0px;
        }

        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }

        .chosen-single {
            color: #8e8e93;
        }

        .container {
            max-width: 100%;
        }

        ul.multiselect-container.dropdown-menu {
            width: 100%;
            height: 100px;
            overflow-y: auto;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
        }

        span.multiselect-selected-text {
            float: left;
            color: #444;
            font-family: 'Roboto', sans-serif !important;
        }

        b.caret {
            float: right;
            margin-top: 8px;
        }

        label {
            font-weight: 500;
            color: #666;
        }

        .fixed {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
        }
        .k-grid-content > .k-grid-norecords > .k-grid-norecords-template 
        {
            top: 8%;
            left: 50%;
            margin-left: -10em;
            margin-top: -2em;
            position: absolute;
        }
    </style>

    <title></title>
     <script type="text/javascript">
       
         $(document).ready(function () {
             act1();
             legal();
             assigncompliance();
             allcompliance();
         });

         function act1() {
             var gridDocuments = $("#actgrid").kendoGrid({
                 dataSource: {
                     transport: {
                         read: '<% =Path%>/Data/ActReport?actId=<%= ActID%>'
                     },
                     pageSize: 10,
                 },
                 height: 453,
                 sortable: true,
                 filterable: true,
                 columnMenu: true,
                 pageable: true,
                 reorderable: true,
                 resizable: true,
                 multi: true,
                 noRecords: true,
                 columns: [
                      { hidden: true, field: "ActID", title: "ActID" },
                      { hidden: true, field: "Id", title: "Id" },
                     {
                         field: "Id", title: 'Sr No',
                         width: "10%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     }, {
                         field: "FileName", title: 'Document Name',
                         width: "40%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     },
                     {
                         command: [
                             {
                                 name: "edit2", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit",
                                 width:"10%;"
                             },
                             {
                                 name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download",
                             }
                         ], title: "Action", lock: true,
                          width: "10%",
                     }
                 ]
             });
             $("#actgrid").kendoTooltip({
                 filter: ".k-grid-edit2",
                 content: function (e) {
                     return "Overview";
                 }
             });
             $("#actgrid").kendoTooltip({
                 filter: ".k-grid-edit1",
                 content: function (e) {
                     return "Download";
                 }
             });

             $("#actgrid").kendoTooltip({
                 filter: "td:nth-child(1)", 
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#actgrid").kendoTooltip({
                 filter: "td:nth-child(2)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");

             $(document).on("click", "#actgrid tbody tr .ob-edit", function (e) {
                 var item = $("#actgrid").data("kendoGrid").dataItem($(this).closest("tr"));
                 OpenOverViewpupMain(item.Act_ID,item.Id);
                 return true;
             });

             $(document).on("click", "#actgrid tbody tr .ob-download", function (e) {
                 debugger;
                 var item = $("#actgrid").data("kendoGrid").dataItem($(this).closest("tr"));
                // alert(item.Act_ID,item.Id);
                 PerActODDOPopPup(item.Act_ID,item.Id);
                 return true;
             });
         }
          function legal() {
              var gridDocuments = $("#legalgrid").kendoGrid({
                    dataSource: {
                        transport: {
                            read: '<% =Path%>/Data/KendoActOverviewUpdates?actid=<% =ActID%>'
                        },
                        pageSize: 10,
                    },
                  height: 450,
                    sortable: true,
                    filterable: true,
                    columnMenu: true,
                    pageable: true,
                    reorderable: true,
                    resizable: true,
                    multi: true,
                    noRecords: true,
                    columns: [
                        {
                            field: "Title", title: 'Title',
                            width: "70%;",
                            attributes: {
                                style: 'white-space: nowrap;'

                            }
                        },
                        {
                            field: "Date", title: 'Date',
                            type: "date",
                            template: "#= kendo.toString(kendo.parseDate(Date, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",

                            attributes: {
                                style: 'white-space: nowrap;'

                            }, filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"

                                    }
                                }
                            }
                        },
                        {
                            command: [
                                { name: "edit2", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit" }
                            ], title: "Action", lock: true
                        }
                    ]
                });

              $("#legalgrid").kendoTooltip({
                    filter: ".k-grid-edit2",
                    content: function (e) {
                        return "Overview";
                    }
                });
              $("#legalgrid").kendoTooltip({
                    filter: "td:nth-child(1)",
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");

             
              $(document).on("click", "#legalgrid tbody tr .ob-edit", function (e) {
                  var item = $("#legalgrid").data("kendoGrid").dataItem($(this).closest("tr"));
                  OpendocumentsUpdates(item.Description);
                  return true;
              });
              function OpendocumentsUpdates(title) {

                  document.getElementById('detailUpdate').innerHTML = title;


                  var myWindowAdv = $("#ViewUpdateDetails");

                  function onClose() {

                  }

                  myWindowAdv.kendoWindow({
                      width: "85%",
                      height: "500px",
                      title: "Legal Updates",
                      visible: false,
                      actions: [
                          "Maximize",
                          "Close"
                      ],

                      close: onClose
                  });
                  myWindowAdv.data("kendoWindow").center().open();
                  return false;
              }

        }

          function assigncompliance() {
              var gridDocuments = $("#compliancegrid").kendoGrid({
                 dataSource: {
                   transport: {
                       read: '<% =Path%>/Data/AssignActReport?&customerID=<% =CustId%>&actid=<% =ActID%>'
                     },
                     pageSize: 10,
                 },
                  height: 410,
                 sortable: true,
                 filterable: true,
                 columnMenu: true,
                 pageable: true,
                 reorderable: true,
                 resizable: true,
                 multi: true,
                 noRecords: true,
                 columns: 
                     [
                     {
                         field: "Title", title: 'Short Description',
                         width: "10%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     }
                   ,
                     {
                         field: "Description", title: 'Description',
                         width: "10%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     }
                     ,
                     {
                         field: "Sections", title: 'Section',
                         width: "10%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     }
                      ,
                     {
                         field: "Risk", title: 'Risk',
                         width: "10%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     }
                     ,
                     {
                         field: "Frequency", title: 'Frequency',
                         width: "10%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     },
                     ]
             });
              $("#compliancegrid").kendoTooltip({
                  filter: "td:nth-child(1)",
                  position: "down",
                  content: function (e) {
                      var content = e.target.context.textContent;
                      return content;
                  }
              }).data("kendoTooltip");
              $("#compliancegrid").kendoTooltip({
                  filter: "td:nth-child(2)",
                  position: "down",
                  content: function (e) {
                      var content = e.target.context.textContent;
                      return content;
                  }
              }).data("kendoTooltip");
          }
         function allcompliance() {
              var gridDocuments = $("#allcompliancegrid").kendoGrid({
                 dataSource: {
                     transport: {
                         read: '<% =Path%>/Data/AssignComplianceReport?customerID=<%=CustId%>&flag=ALL'
                     },
                     pageSize: 10,
                 },
                 height: 451,
                 sortable: true,
                 filterable: true,
                 columnMenu: true,
                 pageable: true,
                 reorderable: true,
                 resizable: true,
                 multi: true,
                 noRecords: true,
                 columns: 
                     [

                     {
                         field: "ShortDescription", title: 'Short Description',
                         width: "10%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     }
                   ,
                     {
                         field: "Description", title: 'Description',
                         width: "10%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     }
                     ,
                     {
                         field: "Section", title: 'Section',
                         width: "10%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     }
                      ,
                     {
                         field: "Risk", title: 'Risk',
                         width: "10%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     }
                     ,
                     {
                         field: "Frequency", title: 'Frequency',
                         width: "10%;",
                         attributes: {
                             style: 'white-space: nowrap;'
                         }
                     },
                     ]
             });
              $("#allcompliancegrid").kendoTooltip({
                  filter: "td:nth-child(1)", 
                  position: "down",
                  content: function (e) {
                      var content = e.target.context.textContent;
                      return content;
                  }
              }).data("kendoTooltip");
              $("#allcompliancegrid").kendoTooltip({
                  filter: "td:nth-child(2)",
                  position: "down",
                  content: function (e) {
                      var content = e.target.context.textContent;
                      return content;
                  }
              }).data("kendoTooltip");
          }
         function act(type)
         {
             if (type == "Act")
             {
                 $('#liActDocuments').css('background-color', '#1fd9e1');
                 $('#liActDocuments').css('color', 'white');
                 $('#liActDocuments').css('border', '1px solid #1fd9e1');
                 $('#actdocument').css('border', 'block');
                 

                 $('#liLegalUpdates').css('background-color', '');
                 $('#liLegalUpdates').css('color', 'black');
                 $('#liLegalUpdates').css('border', 'none');
                 $('#legalupdate').css('border', 'none');

                 $('#liAssignedcompliances').css('background-color', '');
                 $('#liAssignedcompliances').css('color', 'black');
                 $('#liAssignedcompliances').css('border', 'none');
                 $('#allcompliance').css('border', 'none');
                 $('#assigncompliance').css('border', 'none');


                 $('#liAllCompliances').css('background-color', '');
                 $('#liAllCompliances').css('color', 'black');
                 $('#liAllCompliances').css('border', 'none');
                 $('#compliance').css('border', 'none');
                 
                 $('#liActDocument').addClass('active');
                 $('#actdocument').addClass('active');
                 $('#liLegalUpdate').removeClass('active');
                 $('#legalupdate').removeClass('active');
                 $('#liAssignedcompliance').removeClass('active');
                 $('#compliance').removeClass('active');
                 $('#liAllCompliance').removeClass('active');
                 $('#allcompliance').removeClass('active');
             }
             else if(type=="LegalUpdates")
             {
                 
                 $('#liActDocuments').css('background-color', '');
                 $('#liActDocuments').css('color', 'black');
                 $('#liActDocuments').css('border', 'none');
                 $('#actdocument').css('border', 'none');

                 $('#liAssignedcompliances').css('background-color', '');
                 $('#liAssignedcompliances').css('color', 'black');
                 $('#liAssignedcompliances').css('border', 'none');


                 $('#liLegalUpdates').css('background-color', '#1fd9e1');
                 $('#liLegalUpdates').css('color', 'white');
                 $('#liLegalUpdates').css('border', '1px solid #1fd9e1');
                 $('#legalupdate').css('border', 'block');
               

                 $('#liAllCompliances').css('background-color', '');
                 $('#liAllCompliances').css('color', 'black');
                 $('#liAllCompliances').css('border', 'none');
                 $('#compliance').css('border', 'none');
                 $('#allcompliance').css('border', 'none');

                 $('#liActDocument').removeClass('active');
                 $('#liLegalUpdate').addClass('active');
                 $('#liAssignedcompliance').removeClass('active');
                 $('#compliance').removeClass('active');
                 $('#actdocument').removeClass('active');
                 $('#liAllCompliances').removeClass('active');
                 $('#allcompliance').removeClass('active');
                 $('#legalupdate').addClass('active');
             }
             else if (type == "AssignedCompliance")
             {
                 $('#liActDocuments').css('background-color', '');
                 $('#liActDocuments').css('color', 'black');
                 $('#liActDocuments').css('border', 'none');

                 $('#liLegalUpdates').css('background-color', '');
                 $('#liLegalUpdates').css('color', 'black');
                 $('#liLegalUpdates').css('border', 'none');

                 $('#liAssignedcompliances').css('background-color', '#1fd9e1');
                 $('#liAssignedcompliances').css('color', 'white');
                 $('#liAssignedcompliances').css('border', '1px solid #1fd9e1');
                 $('#compliance').css('border', 'none');

                 $('#liAllCompliances').css('background-color', '');
                 $('#liAllCompliances').css('color', 'black');
                 $('#liAllCompliances').css('border', 'none');
                 $('#allcompliance').css('border', 'none');
                 
                 $('#liActDocument').removeClass('active');
                 $('#liLegalUpdate').removeClass('active');
                 $('#liAssignedcompliance').addClass('active');
                 $('#actdocument').removeClass('active');
                 $('#liAllCompliances').removeClass('active');
                 $('#allcompliance').removeClass('active');
                 $('#legalupdate').removeClass('active');
                 $('#compliance').addClass('active');
             }
             else if(type=="AllCompliance")
             {
                 $('#liActDocuments').css('background-color', '');
                 $('#liActDocuments').css('color', 'black');
                 $('#liActDocuments').css('border', 'none');

                 $('#liLegalUpdates').css('background-color', '');
                 $('#liLegalUpdates').css('color', 'black');
                 $('#liLegalUpdates').css('border', 'none');


                 $('#liAllCompliances').css('background-color', '#1fd9e1');
                 $('#liAllCompliances').css('color', 'white');
                 $('#liAllCompliances').css('border', '1px solid #1fd9e1');
                 $('#allcompliance').css('border', 'none');

                 $('#liAssignedcompliances').css('background-color', '');
                 $('#liAssignedcompliances').css('color', 'black');
                 $('#liAssignedcompliances').css('border', 'none');
                 $('#compliance').css('border', 'none');

                 $('#liAllCompliances').addClass('active');
                 $('#liActDocument').removeClass('active');
                 $('#liLegalUpdate').removeClass('active');
                 $('#liAssignedcompliance').removeClass('active');

                 $('#allcompliance').addClass('active');

                 $('#actdocument').removeClass('active');
                 $('#legalupdate').removeClass('active');
                 $('#compliance').removeClass('active');
                
             }
         }
         </script>
    <script type="text/javascript">
        function OpenOverViewpupMain(ActID,Id) {
            $('#divPerActOverView').modal('show');
            $('#PerActOverViews').attr('width', '100%');
            $('#PerActOverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '100%');
            $('#PerActOverViews').removeAttr('src');
            $('#PerActOverViews').attr('src', "../Common/ActOverview.aspx?ActID=" + ActID + "&ID=" + Id);
          
        }
        function PerActODDOPopPup(ActID,Id) {
            $('#PerActDownloadViews').attr('src', "../Common/DownloadActOverview.aspx?ActID=" + ActID+"&ID=" + Id);
        }
        function OpenOverViewpup(ActID, Id) {
            $('#divPerActOverviewopen').modal('show');
            $('#PerActOverViewopen').attr('width', '100%');
            $('#PerActOverViewopen').attr('height', '600px');
            $('.modal-dialog').css('width', '100%');
            $('#PerActOverViewopen').removeAttr('src');
            $('#PerActOverViews').attr('src', "../Common/ActOverview.aspx?ActID=" + ActID + "&ID=" + Id);
        }

</script>
      <style type="text/css">
        tr.spaceUnder > td {
            padding-top: 1em;
        }
        .k-pager-wrap > .k-link > .k-icon {
    margin-top: 0px;
    color: inherit;
}
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .circle {
            width: 24px;
            height: 24px;
            border-radius: 59%;
            display: inline-block;
            margin-left: 8px;
        }

        .RiskControl {
            border: 1px solid #e4d8d8;
            border-left: 5px solid;
            border-radius: 6px;
            width: 141px;
            height: 33px;
        }

        .StatusControl {
            border: 1px solid #e4d8d8;
            border-left: 5px solid;
            border-radius: 6px;
            width:auto;
            width: 174px;
            height: 33px;
        }

        .DueDateControl {
            border: 1px solid #e4d8d8;
            border-left: 5px solid;
            border-radius: 6px;
            height: 33px;
            width: 184px;
        }

        .DetailHeader {
            background: white;
            float: left;
            color: black;
            font-size: 14px;
            font-weight: bold;
        }

        .dataDetail {
            width: 300px;
            font-size: 13px;
            color: #333;
        }

        .SummaryData {
            width: 20%;
            color: black;
            font-size: 14px;
            /*font-weight: bold;*/
        }

          .SummaryData {
              width: 80%;
              font-size: 13px;
              color: #333;
          }
       
    </style>

</head>
<body>
    <form id="form1" runat="server">
      <%-- <div class="col-lg-8 col-md-8 " style="padding-left: 16px; background: white; padding-top: 9px; padding-right: 50px;width:102%;height:100%">
                     <ul class="nav nav-tabs calender-li" role="tablist" style="background: white;font-family: roboto;">--%>
        <div class="col-lg-8 col-md-8 " style="padding-left: 16px; background: white; padding-top: 9px; padding-right: 50px;width:102%;height:100%">
     <ul class="nav nav-tabs calender-li" role="tablist" style="background: white;font-family: 'Roboto',sans-serif;">
 
                        <li id="liActDocument" style="cursor: pointer;"  runat="server"><a id="liActDocuments" onclick="act('Act')" runat="server" aria-controls="actdocument" role="tab" style="background-color: #1fd9e1; color: white;" data-toggle="tab">Act Document</a></li>
                        <li id="liLegalUpdate" runat="server" style="cursor: pointer;"><a id="liLegalUpdates" onclick="act('LegalUpdates')" runat="server" aria-controls="legalupdate" role="tab" data-toggle="tab">Legal Updates</a></li>
                        <li id="liAssignedcompliance" runat="server" style="cursor: pointer;"><a id="liAssignedcompliances" onclick="act('AssignedCompliance')"  runat="server" aria-controls="compliance" role="tab" data-toggle="tab">Assigned Compliance(s)</a></li>
                        <li id="liAllCompliance" runat="server" style="cursor: pointer;"><a id="liAllCompliances" onclick="act('AllCompliance')" runat="server" aria-controls="allcompliance" role="tab" data-toggle="tab">All Compliances</a></li>
                    </ul>

           <%-- <div class="tab-content" style="padding-top: 2px; border: 1px solid #1fd9e1; min-height: 580px; max-height: 580px; overflow-y: auto; top: 40px"> --%>
            <div class="tab-content" style="padding-top: 2px; border: 1px solid #e4d8d8; min-height: 500px; max-height: 580px; /*overflow-y: auto;*/ top: 40px;">         
            <div style="height:45px;" >
                        <div class="col-lg-2 col-md-2 col-sm-2 SummaryHeader"style="width:8%">
                       <asp:Label ID="Label1"  Style="padding-left: 3px; width: 300px; font-size: 12px; color: #333; font-weight:bold"
                            maximunsize="300px" autosize="true" runat="server" >Act Name: </asp:Label>
                            </div>
                       <%-- <asp:Label ID="lblactname" Style="font-family: Roboto; padding-left: 7px; width: 300px; font-size: 16px; color: #333;"
                            maximunsize="300px" autosize="true" runat="server"></asp:Label>--%>
                        <div class="col-lg-10 col-md-10 col-sm-10 SummaryData">
                            <asp:Label ID="lblactname" Style="width: 100%; font-size: 13px;"
                                autosize="true" runat="server" OnClientClick="openInNewTab2" OnClick="linkActName_Click" />
                        </div>

                    </div>
                    <div role="tabpanel" runat="server" id="actdocument">
                        <div class="tabbable-panel">
                            <div class="tabbable-line">
                                <div id="actgrid" style="margin-left: -2px;margin-right: -1px;"></div>
                            </div>
                                 </div>
                      
                        <div class="modal fade" id="divPerActOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                            <div class="modal-dialog" style="width: 100%;">
                                <div class="modal-content" style="width: 100%;">
                                    <div class="modal-header" style="border-bottom: none;">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <iframe id="PerActOverViews" src="about:blank" width="100%;" height="100%" frameborder="0"></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                <div class="modal-dialog" style="width: 650px;">
                                    <div class="modal-content" style="width: 100%;">
                                        <div class="modal-header" style="border-bottom: none;">
                                                      <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                                                  </div>
                                                  <div class="modal-body">
                                                      <iframe id="PerActDownloadViews" src="about:blank" width="535px" height="350px" frameborder="0"></iframe>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                               </div>

                <div role="tabpanel" runat="server" id="legalupdate">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                     <div id="legalgrid" style="margin-left: -1px;margin-right: -1px;"></div>
                                    </div>
                                </div>
                    </div>
                            <div role="tabpanel" runat="server" id="compliance">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                     <div id="compliancegrid" style="margin-left: -1px;margin-right: -1px;"></div>
                                    </div>
                                </div>
                            </div>
                 <div id="ViewUpdateDetails" style="margin-bottom: -21px;">
                    <label id="detailUpdate"></label>
                </div>

                          <div role="tabpanel" runat="server" id="allcompliance">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                     <div id="allcompliancegrid" style="margin-left: -1px;margin-right: -1px;"></div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
           </div>
    </form>
</body>
</html>
