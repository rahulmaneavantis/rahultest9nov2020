﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class ComplianceDashboardDisplay : System.Web.UI.Page
    {
        protected string title;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (Page.RouteData.Values["filter"] != null)
                    {
                        if (Page.RouteData.Values["filter"].ToString().Equals("Upcoming"))
                        {
                            if (Page.RouteData.Values["Type"].ToString().Equals("Statutory"))
                            {
                                hdnTitle.Value = Page.RouteData.Values["filter"].ToString() + " Compliances";
                                lbltagLine.Text = Page.RouteData.Values["filter"].ToString() + " Compliances for " + Page.RouteData.Values["Role"].ToString();
                            }
                            else
                            {
                                hdnTitle.Value = Page.RouteData.Values["filter"].ToString() + " Internal Compliances";
                                lbltagLine.Text = Page.RouteData.Values["filter"].ToString() + " Internal Compliances for " + Page.RouteData.Values["Role"].ToString();
                            }
                        }
                        else if (Page.RouteData.Values["filter"].ToString().Equals("Pending"))
                        {

                            if (Page.RouteData.Values["Type"].ToString().Equals("Statutory"))
                            {
                                if (Page.RouteData.Values["Role"].ToString().Equals("Performer"))
                                {
                                    lbltagLine.Text = "Overdue Compliances for " + Page.RouteData.Values["Role"].ToString();
                                    hdnTitle.Value = "Overdue Compliances";
                                }
                                else
                                {
                                    lbltagLine.Text = "Compliances Not Submited for " + Page.RouteData.Values["Role"].ToString();
                                    hdnTitle.Value = "Compliances Not Submited";
                                }
                            }
                            else
                            {
                                if (Page.RouteData.Values["Role"].ToString().Equals("Performer"))
                                {
                                    lbltagLine.Text = "Overdue Internal Compliances for " + Page.RouteData.Values["Role"].ToString();
                                    hdnTitle.Value = "Overdue Internal Compliances";
                                }
                                else
                                {
                                    lbltagLine.Text = "Internal Compliances Not Submited for " + Page.RouteData.Values["Role"].ToString();
                                    hdnTitle.Value = "Internal Compliances Not Submited";
                                }
                            }
                        }
                        else if (Page.RouteData.Values["filter"].ToString().Equals("PendingForReview"))
                        {
                            if (Page.RouteData.Values["Type"].ToString().Equals("Statutory"))
                            {
                                lbltagLine.Text = "Pending for Review Compliances for " + Page.RouteData.Values["Role"].ToString();
                                hdnTitle.Value = "Pending for review Compliances.";
                            }
                            else
                            {
                                lbltagLine.Text = "Pending for Review Internal Compliances for " + Page.RouteData.Values["Role"].ToString();
                                hdnTitle.Value = "Pending for review Internal Compliances.";
                            }
                        }
                        else if (Page.RouteData.Values["filter"].ToString().Equals("PendingforApproval"))
                        {
                            if (Page.RouteData.Values["Type"].ToString().Equals("Statutory"))
                            {
                                if (Page.RouteData.Values["Role"].ToString().Equals("Performer"))
                                {
                                    lbltagLine.Text = "Pending for Review Compliances for " + Page.RouteData.Values["Role"].ToString();
                                    hdnTitle.Value = "Pending for review Compliances";
                                }
                                else
                                {
                                    lbltagLine.Text = "Pending for Approval Compliances for " + Page.RouteData.Values["Role"].ToString();
                                    hdnTitle.Value = "Pending for approval Compliances";
                                }
                            }
                            else
                            {
                                if (Page.RouteData.Values["Role"].ToString().Equals("Performer"))
                                {
                                    lbltagLine.Text = "Pending for Review Internal Compliances for " + Page.RouteData.Values["Role"].ToString();
                                    hdnTitle.Value = "Pending for review Internal Compliances";
                                }
                                else
                                {
                                    lbltagLine.Text = "Pending for Approval Internal Compliances for " + Page.RouteData.Values["Role"].ToString();
                                    hdnTitle.Value = "Pending for approval Internal Compliances";
                                }
                            }
                        }
                       else if (Page.RouteData.Values["filter"].ToString().Equals("Summary"))
                        {
                            if (Page.RouteData.Values["Type"].ToString().Equals("Statutory"))
                            {
                                if (Page.RouteData.Values["Role"].ToString().Equals("Performer"))
                                {
                                    ucPerformanceSummaryforPerformer.Visible = true;
                                }
                                else if (Page.RouteData.Values["Role"].ToString().Equals("Reviewer"))
                                {
                                    ucPerformanceSummaryforReviewer.Visible = true;
                                }
                                lbltagLine.Text = "Performance Summary for " + Page.RouteData.Values["Role"].ToString();
                                hdnTitle.Value = "Performance Summary";
                            }
                            else
                            {
                                if (Page.RouteData.Values["Role"].ToString().Equals("Performer"))
                                {
                                    UcInternalPerformanceSummaryforPerformer.Visible = true;
                                }
                                else if (Page.RouteData.Values["Role"].ToString().Equals("Reviewer"))
                                {
                                    UcInternalPerformanceSummaryforReviewer.Visible = true;
                                }
                                lbltagLine.Text = "Internal Performance Summary for " + Page.RouteData.Values["Role"].ToString();
                                hdnTitle.Value = "Internal Performance Summary";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }           
        }
    }
}