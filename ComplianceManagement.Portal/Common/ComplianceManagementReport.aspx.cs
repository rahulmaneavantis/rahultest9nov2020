﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class ComplianceManagementReport : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BindLocationFilter();
                if (tvLocation.Nodes[0].Value != null)
                {
                    ShowGraph(Convert.ToInt32(tvLocation.Nodes[0].Value));
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "AssignRowColor(" + tvLocation.Nodes[0].Value + ");", true);
                }
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                var bracnhes = AssignEntityManagement.GetLocation(AuthenticationHelper.UserID);
                string htmalTable = GetHtmlTable();

                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    tvLocation.Nodes.Add(node);
                    DataTable managementData = AssignEntityManagement.GetMangementSummaryReport1(item.ID, AuthenticationHelper.UserID);

                    htmalTable += "<tr style='height: 30px;' id='" + item.ID + "'><td style='border:1px solid black;'><a ToolTip='Click here to generate a graph.' href='javascript:RefreshChart(" + item.ID + ");'>" + managementData.Rows[0]["Highdelayed"] + "</a>"
                                   + "<td style='border:1px solid black;'><a ToolTip='Click here to generate a graph.' href='javascript:RefreshChart(" + item.ID + ");'>" + managementData.Rows[0]["HighPending"] + "</a>"
                                   + "</td><td style='border:1px solid black;'>" + managementData.Rows[0]["Medium"]
                                   + "</td><td style='border:1px solid black;'>" + managementData.Rows[0]["Low"] + "</td></tr>";

                }

                htmalTable += "</table>";
                tvLocation.CollapseAll();
                divCountTableContainer.InnerHtml = htmalTable;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void tvLocation_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
        {
            try
            {
                string htmlTable = GetHtmlTable();
                foreach (TreeNode tn in tvLocation.Nodes)
                {
                    DataTable managementData = AssignEntityManagement.GetMangementSummaryReport1(Convert.ToInt32(tn.Value), AuthenticationHelper.UserID);


                    htmlTable += "<tr style='height: 30px;' id='" + tn.Value + "'><td style='border:1px solid black;'><a ToolTip='Click here to generate a graph.' href='javascript:RefreshChart(" + tn.Value + ");'>" + managementData.Rows[0]["Highdelayed"] + "</a>"
                                  + "<td style='border:1px solid black;'><a ToolTip='Click here to generate a graph.' href='javascript:RefreshChart("  + tn.Value + ");'>" + managementData.Rows[0]["HighPending"] + "</a>"
                                  + "</td><td style='border:1px solid black;'>" + managementData.Rows[0]["Medium"]
                                  + "</td><td style='border:1px solid black;'>" + managementData.Rows[0]["Low"] + "</td></tr>";

                    if (tn.Expanded == true)
                    {
                        htmlTable = SubNodeCounts(tn, htmlTable);
                    }
                }
                htmlTable += "</table>";

                divCountTableContainer.InnerHtml = htmlTable;
                ShowGraph(Convert.ToInt32(tvLocation.Nodes[0].Value));
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "AssignRowColor(" + tvLocation.Nodes[0].Value + ");", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected string SubNodeCounts(TreeNode node, string htmlTable)
        {
                      foreach (TreeNode tn1 in node.ChildNodes)
                        {
                            DataTable managementData = AssignEntityManagement.GetMangementSummaryReport1(Convert.ToInt32(tn1.Value), AuthenticationHelper.UserID);

                          

                            string rowid = "child" + tn1.Value;

                            htmlTable += "<tr style='height: 30px;' id='" + rowid + "'><td style='border:1px solid black;'><a ToolTip='Click here to generate a graph.' href='javascript:RefreshChart(\"" + rowid + "\");'>" + managementData.Rows[0]["Highdelayed"] + "</a>"
                                 + "<td style='border:1px solid black;'><a ToolTip='Click here to generate a graph.' href='javascript:RefreshChart(\"" + rowid + "\");'>" + managementData.Rows[0]["HighPending"] + "</a>"
                                 + "</td><td style='border:1px solid black;'>" + managementData.Rows[0]["Medium"]   
                                 + "</td><td style='border:1px solid black;'>" + managementData.Rows[0]["Low"] + "</td></tr>";


                            if (tn1.Expanded == true)
                            {
                               htmlTable = SubNodeCounts(tn1, htmlTable);
                            }
                        }
                      return htmlTable;
        }

        protected void tvLocation_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
        {
            try
            {
                string htmlTable = GetHtmlTable();
                foreach (TreeNode tn in tvLocation.Nodes)
                {
                    DataTable managementData = AssignEntityManagement.GetMangementSummaryReport1(Convert.ToInt32(tn.Value), AuthenticationHelper.UserID);

                  

                    htmlTable += "<tr style='height: 30px;' id='" + tn.Value + "'><td style='border:1px solid black;'><a ToolTip='Click here to generate a graph.' href='javascript:RefreshChart(" + tn.Value + ");'>" + managementData.Rows[0]["Highdelayed"] + "</a>"
                                 + "<td style='border:1px solid black;'><a ToolTip='Click here to generate a graph.' href='javascript:RefreshChart(" + tn.Value + ");'>" + managementData.Rows[0]["HighPending"] + "</a>"
                                 + "</td><td style='border:1px solid black;'>" + managementData.Rows[0]["Medium"]
                                 + "</td><td style='border:1px solid black;'>" + managementData.Rows[0]["Low"] + "</td></tr>";

                    if (tn.Expanded == true)
                    {
                        htmlTable = SubNodeCounts(tn, htmlTable);
                    }
                }
                htmlTable += "</table>";

                divCountTableContainer.InnerHtml = htmlTable;
                ShowGraph(Convert.ToInt32(tvLocation.Nodes[0].Value));
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "AssignRowColor(" + tvLocation.Nodes[0].Value + ");", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        private string GetHtmlTable()
        {
          

            string htmlTable = "<table id='ComplianceCountTable' class='tvtable' style='text-align:center;margin-bottom: 52px;margin-left: 2px;border-collapse:collapse;' >"
                            + "<tr><th style='width:200px; border-right:1px solid #FFFFFF; border-right:1px solid #FFFFFF; border-bottom:1px solid #FFFFFF;' colspan='2'>High</th>"
                            + "<th style='width:100px; border-right:1px solid #FFFFFF;' rowspan='2'>Medium</th>"
                            + "<th style='width:100px;' rowspan='2'>Low</th></tr>"
                            + "<tr><th style='width:100px;border-right:1px solid #FFFFFF;'>Delayed</th><th style='width:100px;'>Pending</th></tr>";

            return htmlTable;

        }

        private void ShowGraph(int branchId)
        {
            try
            {
                bool displayPendingGraph = false;
                chrtPendingCompliances.DataSource = AssignEntityManagement.GetMangementSummaryGraph(branchId, AuthenticationHelper.UserID, "pending", out displayPendingGraph);
                chrtPendingCompliances.DataBind();
                chrtPendingCompliances.Visible = displayPendingGraph;
                if (displayPendingGraph == false)
                {
                    titlePendingCompliances.Text = "No data to display";
                }
                else
                {
                    if (AssignEntityManagement.GetCategoryByUserID(AuthenticationHelper.UserID) > 1)
                        titlePendingCompliances.Text = "High Risk Pending Compliances";
                    else
                        titlePendingCompliances.Text = "Pending Compliances";
                }
                 
                chrtDelayedCompliances.DataSource = AssignEntityManagement.GetMangementSummaryGraph(branchId, AuthenticationHelper.UserID, "delayed",out displayPendingGraph);
                chrtDelayedCompliances.DataBind();
                chrtDelayedCompliances.Visible = displayPendingGraph;
                if (displayPendingGraph == false)
                {
                    titleDelayedCompliances.Text = "No data to display";
                }
                else
                {
                    if (AssignEntityManagement.GetCategoryByUserID(AuthenticationHelper.UserID) > 1)
                        titleDelayedCompliances.Text = "High Risk Delayed Compliances";
                    else
                        titleDelayedCompliances.Text = "Delayed Compliances";
                }
                   

            }catch(Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void button_Click(object sender, EventArgs e)
        {
            string nodeValue = hdnUpdate.Value;
            if (hdnUpdate.Value.ToString().Contains("child"))
            {
                string[] nodeValues = Convert.ToString(hdnUpdate.Value).Split('d');
                nodeValue = nodeValues[1];
            }
            ShowGraph(Convert.ToInt32(nodeValue));
            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "AssignRowColor(" + hdnUpdate.Value +");", true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "AssignRowColor", string.Format("AssignRowColor('{0}');", hdnUpdate.Value), true);
            lblErrorMessage.Text = "Server Error Occured. Please try again.";
            
        }

        protected void tvLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            ShowGraph(Convert.ToInt32(tvLocation.SelectedNode.Value));

            string rowid = tvLocation.SelectedNode.Value;
            if (!(tvLocation.SelectedNode.Parent == null))
            {
                rowid = "child" + tvLocation.SelectedNode.Value;
            }
            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "AssignRowColor(" + rowid + ");", true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "AssignRowColor", string.Format("AssignRowColor('{0}');", rowid), true);
            lblErrorMessage.Text = "Server Error Occured. Please try again.";
        }

    }
}