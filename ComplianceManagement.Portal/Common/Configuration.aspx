﻿<%@ Page Title="Configuration List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="Configuration.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.Configuration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upFinancialYearList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right" style="width: 20%;">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td align="right" class="newlink">
                        <asp:LinkButton Text="Manage Financial year" runat="server" ID="btnAddFinancialYear"
                            OnClick="btnAddFinancialYear_Click" />
                        <asp:LinkButton Text="Manage Compliance reminders" runat="server" ID="btnManageReminders"
                            OnClick="btnManageReminders_Click" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdFinancialYear" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdFinancialYear_Sorting"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdFinancialYear_RowCommand" OnRowCreated="grdFinancialYear_RowCreated"
                OnPageIndexChanging="grdFinancialYear_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-Height="20px" SortExpression="Name" />
                    <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_FINANCIAL_YEAR"
                                CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Financial Year" title="Edit Financial Year" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_FINANCIAL_YEAR"
                                CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this financial year?');"><img src="../Images/delete_icon.png" alt="Delete Financial Year" title="Delete Financial Year" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divComplianceTypeDialog">
        <asp:UpdatePanel ID="upComplianceType" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceTypeValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceTypeValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Financial Year Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 16px; width: 200px;" MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Financial Year Name can not be empty."
                            ControlToValidate="tbxName" runat="server" ValidationGroup="ComplianceTypeValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                            ValidationGroup="ComplianceTypeValidationGroup" ErrorMessage="Please enter a valid Financial Year Name."
                            ControlToValidate="tbxName" ValidationExpression="^[a-zA-Z0-9_ .-]*$"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 66px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceTypeValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divComplianceTypeDialog').dialog('close');" />
                    </div>
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divReminderConfiguration">
        <asp:UpdatePanel ID="upReminderConfiguration" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Monthly:</label>
                        <asp:CheckBoxList runat="server" ID="cblMonthly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Quarterly:</label>
                        <asp:CheckBoxList runat="server" ID="cblQuarterly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            FourMonthly:</label>
                        <asp:CheckBoxList runat="server" ID="cblFourMonthly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Half Yearly:</label>
                        <asp:CheckBoxList runat="server" ID="cblHalfYearly" RepeatDirection="Horizontal"
                            RepeatLayout="Table" CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Annual:</label>
                        <asp:CheckBoxList runat="server" ID="cblYearly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Two Yearly:</label>
                        <asp:CheckBoxList runat="server" ID="cblTwoYearly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333; padding-top: 10px">
                            Seven Yearly:</label>
                        <asp:CheckBoxList runat="server" ID="cblSevenYearly" RepeatDirection="Horizontal" RepeatLayout="Table"
                            CellPadding="5">
                        </asp:CheckBoxList>
                    </div>

                    <div style="margin-bottom: 7px; float: right;">
                        <asp:Button Text="Save" runat="server" ID="btnSaveReminderConfiguration" OnClick="btnSaveReminderConfiguration_Click"
                            CssClass="button" />
                        <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divReminderConfiguration').dialog('close');" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#divComplianceTypeDialog').dialog({
                height: 300,
                width: 500,
                autoOpen: false,
                draggable: true,
                title: "Financial Year",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divReminderConfiguration').dialog({
                height: 390,
                width: 530,
                autoOpen: false,
                draggable: true,
                title: "Reminders Configuration",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>
