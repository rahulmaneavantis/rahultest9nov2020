﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class ActReportGrid1 : System.Web.UI.Page
    {
        protected static string Path;
        protected static string FlagIsApp;
        protected static int CustId;
        protected static int UId;
        public static int ActID;
        protected static int compInstanceID;
        public static int actID;
        public static int ID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                FlagIsApp =Convert.ToString(AuthenticationHelper.CustomerID);

                liActDocument.Attributes.Add("class", "active");
                liLegalUpdate.Attributes.Add("class", "");
                liAssignedcompliance.Attributes.Add("class", "");
                liAllCompliance.Attributes.Add("class", "");

                actdocument.Attributes.Remove("class");
                actdocument.Attributes.Add("class", "tab-pane active");

                legalupdate.Attributes.Remove("class");
                legalupdate.Attributes.Add("class", "tab-pane");

                compliance.Attributes.Remove("class");
                compliance.Attributes.Add("class", "tab-pane");

                allcompliance.Attributes.Remove("class");
                allcompliance.Attributes.Add("class", "tab-pane");

                ActID = Convert.ToInt32(Request.QueryString["ActID"]);
                var actname = GetAllAct(ActID);
                lblactname.Text = Convert.ToString(actname);
                ID = Convert.ToInt32(Request.QueryString["ID"]);
            }
        }

        public static object GetAllAct(int newact)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Acts
                            where row.ID == newact
                            select row.Name).FirstOrDefault();
                return acts;
            }
        }
        
    }

}