﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventDetailKendo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventDetailKendo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

        #gridEvent .k-grid-content {
            min-height: 400px !important;
            max-height: 440px;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
            border-bottom-width: 1px;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            margin-left: 10px;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
            min-height: 30px;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
            margin-left: 2px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow-y:auto;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 1px 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
            line-height: 14px;
        }

        #gridEvent.k-grid tbody tr {
            height: 100px;
            line-height: 50px;
        }

        #gridEvent div.k-grid-header {
            margin-right: 1px;
        }
    </style>


    <script type="text/javascript">

        $(document).ready(function () {
            BindGrid();
        });

        function BindGrid() {
            var gridexist = $('#gridEvent').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#gridEvent').empty();

            var gridEvent = $("#gridEvent").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(8)').remove();
                },
                dataSource: {
                    transport: {
                        read: "<% =Path%>Data/GetEventData?ParentEventID=<%=parentID%>&BranchID=<%=branchID%>"

                    },
                    pageSize: 5
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                scrollable:true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [{
                    title: "Sr No",
                    template: "#= ++record #",
                    width: "4.5%"
                },
                {
                    field: "IntermediatesubEvent",
                    title: "Sub Event",
                    width: "15%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "ActName",
                    title: "Act Name",
                    width: "15%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "Sections",
                    title: "Sections",
                    width: "10%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "ShortDescription",
                    title: "Compliance",
                    width: "10%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "Description",
                    title: "Description",
                    width: "20%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "PenaltyDescription",
                    title: "Penalty Description",
                    width: "20%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                ]
            });

       
            $("#gridEvent").kendoTooltip({
                filter: "td:nth-child(2)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");


            $("#gridEvent").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#gridEvent").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "top",
                width: 200,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#gridEvent").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#gridEvent").kendoTooltip({
                filter: "td:nth-child(6)", //this filter selects the second column's cells
                position: "top",
                width: 300,
                animation: {
                    open: {
                        effects: "fade:in"
                    }
                },
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#gridEvent").kendoTooltip({
                filter: "td:nth-child(7)", //this filter selects the second column's cells
                position: "top",
                width: 300,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
        }


    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smAddReminder" runat="server"></asp:ScriptManager>
         <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
        <div>
                <asp:Label ID="lblEventName" Text="Event Name" runat="server" Style="color: #666666; font-family: 'Roboto' sans-serif; font-weight: normal; font: bold; font-size: 20px;"></asp:Label>

                <asp:Button Text="Back" CssClass="btn btn-primary" ToolTip="Go to Previous" data-toggle="tooltip"
                    runat="server" ID="lnkbtnPrevious" Style="width: 50px;display:none;height: 20px; margin-left: 980px;" OnClick="lnkPreviousSwitch_Click" />
               <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>
                <div id="gridEvent" style="border: none;"></div>
        </div>

    </form>
</body>
</html>
