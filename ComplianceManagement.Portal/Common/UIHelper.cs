﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public class UIHelper
    {
        public static void ClearDropDownList(DropDownList ddlControl)
        {
            ddlControl.DataSource = null;
            ddlControl.DataBind();
            ddlControl.ClearSelection();
        }
    }
}