﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class Configuration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindComplianceTypes();
            }
        }

        private void BindComplianceTypes()
        {
            try
            {
                grdFinancialYear.DataSource = ConfigurationManagement.GetAll(tbxFilter.Text);
                grdFinancialYear.DataBind();
                upFinancialYearList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdFinancialYear_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int typeID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_FINANCIAL_YEAR"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["FinancialYearID"] = typeID;

                    FinancialYear userParameter = ConfigurationManagement.GetByID(typeID);

                    tbxName.Text = userParameter.Name;


                    upComplianceType.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divComplianceTypeDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_FINANCIAL_YEAR"))
                {
                    ConfigurationManagement.Delete(typeID);
                    BindComplianceTypes();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdFinancialYear_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdFinancialYear.PageIndex = e.NewPageIndex;
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnManageReminders_Click(object sender, EventArgs e)
        {

            try
            {
                var reminderTemplates = ConfigurationManagement.GetAllReminderTemplates();

                cblMonthly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.Monthly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsSubscribed }).ToList().ForEach(item =>
                {
                    cblMonthly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });

                cblQuarterly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.Quarterly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsSubscribed }).ToList().ForEach(item =>
                {
                    cblQuarterly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });
                //added by rahul on 14 Jan 2015
                cblFourMonthly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.FourMonthly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsSubscribed }).ToList().ForEach(item =>
                {
                    cblFourMonthly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });           
                cblHalfYearly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.HalfYearly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsSubscribed }).ToList().ForEach(item =>
                {
                    cblHalfYearly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });

                cblYearly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.Annual).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsSubscribed }).ToList().ForEach(item =>
                {
                    cblYearly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });
                //added by rahul on 14 Jan 2015
                cblTwoYearly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.TwoYearly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsSubscribed }).ToList().ForEach(item =>
                {
                    cblTwoYearly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });

                //added by rahul on 14 Jan 2015
                cblSevenYearly.Items.Clear();
                reminderTemplates.Where(entry => entry.Frequency == (byte)Frequency.SevenYearly).Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsSubscribed }).ToList().ForEach(item =>
                {
                    cblSevenYearly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                }); 
               
                //Monthly = 0,
                //Quarterly,
                //HalfYearly,
                //Annual,
                //FourMonthly,
                //TwoYearly,
                //SevenYearly

                upReminderConfiguration.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divReminderConfiguration\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddFinancialYear_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                tbxName.Text = string.Empty;


                upComplianceType.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divComplianceTypeDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            grdFinancialYear.PageIndex = 0;
            BindComplianceTypes();
        }

        protected void btnSaveReminderConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                List<ReminderTemplate> reminders = new List<ReminderTemplate>();

                foreach (ListItem item in cblMonthly.Items)
                {
                    reminders.Add(new ReminderTemplate()
                    {
                        ID = Convert.ToInt32(item.Value),
                        IsSubscribed = item.Selected
                    });
                }

                foreach (ListItem item in cblQuarterly.Items)
                {
                    reminders.Add(new ReminderTemplate()
                    {
                        ID = Convert.ToInt32(item.Value),
                        IsSubscribed = item.Selected
                    });
                }

                foreach (ListItem item in cblHalfYearly.Items)
                {
                    reminders.Add(new ReminderTemplate()
                    {
                        ID = Convert.ToInt32(item.Value),
                        IsSubscribed = item.Selected
                    });
                }

                foreach (ListItem item in cblYearly.Items)
                {
                    reminders.Add(new ReminderTemplate()
                    {
                        ID = Convert.ToInt32(item.Value),
                        IsSubscribed = item.Selected
                    });
                }

                ConfigurationManagement.UpdateReminderTemplates(reminders);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divReminderConfiguration\").dialog('close')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                FinancialYear financialYear = new FinancialYear()
                {
                    Name = tbxName.Text,
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    financialYear.ID = Convert.ToInt32(ViewState["FinancialYearID"]);
                }

                if (ConfigurationManagement.Exists(financialYear))
                {
                    cvDuplicateEntry.ErrorMessage = "Financial year already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    ConfigurationManagement.Create(financialYear);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    ConfigurationManagement.Update(financialYear);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceTypeDialog\").dialog('close')", true);
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdFinancialYear_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var financialYearList = ConfigurationManagement.GetAll(tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    financialYearList = financialYearList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    financialYearList = financialYearList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdFinancialYear.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdFinancialYear.Columns.IndexOf(field);
                    }
                }

                grdFinancialYear.DataSource = financialYearList;
                grdFinancialYear.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdFinancialYear_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }

        }

     void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;
          
            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        


    }
}