﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Compliances;
using System.Data;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class Dashboard : System.Web.UI.Page
    {
        public static string CalendarDate;
        public static DateTime CalendarTodayOrNextDate;
        public static string date = "";
        protected static string CalenderDateString;
        protected string CustomerName;
        protected string user_Roles;
        protected List<Int32> roles;
        protected List<Int32> TaskRoles;
        protected bool IsTaskAssigned = false;
        protected bool DeptHead = false;

        protected int checkInternalapplicable = 0;
        protected int checkTaskapplicable = 0;
        protected int Test = 30;
        //Satutory
        public int PerformerTotal = 0;
        public int Performercomplited = 0;
        public int ReviewerTotal = 0;
        public int Reviewercomplited = 0;
        protected bool IsApplicableInternal = false;
        //Satutory CheckList
        public int CheckListPerformerTotal = 0;
        public int CheckListPerformercomplited = 0;
        //Internal
        public int InternalPerformerTotal = 0;
        public int InternalPerformercomplited = 0;
        public int InternalReviewerTotal = 0;
        public int InternalReviewercomplited = 0;
        protected static string UserInformation;

        protected bool IsTLEntityAssignment = false;

        #region Satutory
        public static List<Sp_GetComplitedCount_Result> GetComplitedCountProcedure(int Customerbranchid, int userid, int roleid, DateTime startdate, DateTime Enddate)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<Sp_GetComplitedCount_Result> complianceReminders = new List<Sp_GetComplitedCount_Result>();
                if (Enddate.Date > DateTime.Today.Date)
                {
                    complianceReminders = entities.Sp_GetComplitedCount(Customerbranchid, userid, roleid, startdate, DateTime.Today.Date).ToList();
                }
                else
                {
                    complianceReminders = entities.Sp_GetComplitedCount(Customerbranchid, userid, roleid, startdate, Enddate).ToList();
                }
                return complianceReminders;
            }
        }
        public static List<Sp_GetComplitedCount_Result> GetComplitedCountProcedure(int Customerbranchid, int userid, int roleid, DateTime startdate, DateTime Enddate, List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MasterQuery)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<Sp_GetComplitedCount_Result> complianceReminders = new List<Sp_GetComplitedCount_Result>();
                //if (Enddate.Date > DateTime.Today.Date)
                //{
                //  //  complianceReminders = entities.Sp_GetComplitedCount(Customerbranchid, userid, roleid, startdate, DateTime.Today.Date).ToList();

                //}
                //else
                //{
                // //   complianceReminders = entities.Sp_GetComplitedCount(Customerbranchid, userid, roleid, startdate, Enddate).ToList();
                //}
                List<int> status = new List<int>();
                status.Add(4);
                status.Add(5);
                status.Add(9);
                complianceReminders = (from row in MasterQuery
                                       join branch in entities.CustomerBranches on row.CustomerBranchID equals branch.ID
                                       where status.Contains(row.ComplianceStatusID)
                                       select new Sp_GetComplitedCount_Result
                                       {
                                           ComplianceInstanceID = row.ComplianceInstanceID,
                                           RoleID = row.RoleID,
                                           CustomerBranchid = row.CustomerBranchID,
                                           NAME = branch.Name,
                                           SCheduledonid = row.ScheduledOnID,
                                           ScheduleOn = row.ScheduledOn

                                       }).ToList();

                return complianceReminders;
            }
        }

        public static List<Sp_GetTotalCount_Result> GetTotalCountProcedure(int Customerbranchid, int userid, int roleid, DateTime startdate, DateTime Enddate)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<Sp_GetTotalCount_Result> complianceReminders = new List<Sp_GetTotalCount_Result>();
                if (Enddate.Date > DateTime.Today.Date)
                {
                    complianceReminders = entities.Sp_GetTotalCount(Customerbranchid, userid, roleid, startdate, DateTime.Today.Date).ToList();
                }
                else
                {
                    complianceReminders = entities.Sp_GetTotalCount(Customerbranchid, userid, roleid, startdate, Enddate).ToList();
                }

                return complianceReminders;
            }
        }

        public static List<Sp_GetTotalCount_Result> GetTotalCountProcedure(int Customerbranchid, int userid, int roleid, DateTime startdate, DateTime Enddate, List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MasterQuery)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<Sp_GetTotalCount_Result> complianceReminders = new List<Sp_GetTotalCount_Result>();
                if (Enddate.Date > DateTime.Today.Date)
                {
                    // complianceReminders = entities.Sp_GetTotalCount(Customerbranchid, userid, roleid, startdate, DateTime.Today.Date).ToList();
                    complianceReminders = (from row in MasterQuery

                                           select new Sp_GetTotalCount_Result
                                           {
                                               ComplianceInstanceID = row.ComplianceInstanceID,
                                               RoleID = row.RoleID,
                                               CustomerBranchid = row.CustomerBranchID,
                                               ScheduledOnId = row.ScheduledOnID,
                                               ScheduleOn = row.ScheduledOn

                                           }).ToList();
                }
                else
                {
                    //complianceReminders = entities.Sp_GetTotalCount(Customerbranchid, userid, roleid, startdate, Enddate).ToList();
                    complianceReminders = (from row in MasterQuery

                                           select new Sp_GetTotalCount_Result
                                           {
                                               ComplianceInstanceID = row.ComplianceInstanceID,
                                               RoleID = row.RoleID,
                                               CustomerBranchid = row.CustomerBranchID,
                                               ScheduledOnId = row.ScheduledOnID,
                                               ScheduleOn = row.ScheduledOn

                                           }).ToList();

                }

                return complianceReminders;
            }
        }
        public static List<Sp_GetPerformerAndReviewer_Result> GetPerformerAndReviewerProcedure(int Customerid, int userid, int roleid)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var complianceReminders = entities.Sp_GetPerformerAndReviewer(Customerid, userid, roleid).ToList();
                complianceReminders = complianceReminders.GroupBy(a => a.Name).Select(a => a.FirstOrDefault()).ToList();
                return complianceReminders;
            }
        }
        #endregion

        #region Check List
        public static List<Sp_GetCheckListComplitedCount_Result> GetChecKComplitedCountProcedure(int Customerbranchid, int userid, int roleid, DateTime startdate, DateTime Enddate)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<Sp_GetCheckListComplitedCount_Result> complianceReminders = new List<Sp_GetCheckListComplitedCount_Result>();
                if (Enddate.Date > DateTime.Today.Date)
                {
                    complianceReminders = entities.Sp_GetCheckListComplitedCount(Customerbranchid, userid, roleid, startdate, DateTime.Today.Date).ToList();
                }
                else
                {
                    complianceReminders = entities.Sp_GetCheckListComplitedCount(Customerbranchid, userid, roleid, startdate, Enddate).ToList();
                }
                return complianceReminders;
            }
        }
        public static List<Sp_GetTotalCheckListCount_Result> GetChecKTotalCountProcedure(int Customerbranchid, int userid, int roleid, DateTime startdate, DateTime Enddate)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<Sp_GetTotalCheckListCount_Result> complianceReminders = new List<Sp_GetTotalCheckListCount_Result>();
                if (Enddate.Date > DateTime.Today.Date)
                {
                    complianceReminders = entities.Sp_GetTotalCheckListCount(Customerbranchid, userid, roleid, startdate, DateTime.Today.Date).ToList();
                }
                else
                {
                    complianceReminders = entities.Sp_GetTotalCheckListCount(Customerbranchid, userid, roleid, startdate, Enddate).ToList();
                }

                return complianceReminders;
            }
        }
        public static List<Sp_GetCheckListPerformerAndReviewer_Result> GetChecKPerformerAndReviewerProcedure(int Customerid, int userid, int roleid)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var complianceReminders = entities.Sp_GetCheckListPerformerAndReviewer(Customerid, userid, roleid).ToList();
                return complianceReminders;
            }
        }
        #endregion
        public static long GetSatutoryFinancialYear(int CsutomerbranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                long Year = (from row in entities.ComplianceInstances
                             where row.CustomerBranchID == CsutomerbranchID
                             orderby row.ScheduledOn
                             select row.ScheduledOn.Year).FirstOrDefault();
                return Year;
            }
        }
        public List<Tuple<DateTime, DateTime>> GetSatutorydetails(int CustomerBranchid)
        {
            List<Tuple<DateTime, DateTime>> tr = new List<Tuple<DateTime, DateTime>>();
            long year = GetSatutoryFinancialYear(CustomerBranchid);
            long currentyear = DateTime.Today.Date.Year;
            DateTime start_date = new DateTime();
            DateTime end_date = new DateTime();
            while (year <= currentyear)
            {
                start_date = GetDate("01/04/" + (year - 1));
                end_date = GetDate("31/03/" + year);
                tr.Add(new Tuple<DateTime, DateTime>(start_date, end_date));
                year = year + 1;
            }
            start_date = GetDate("01/04/" + (currentyear));
            end_date = DateTime.Today.Date;
            tr.Add(new Tuple<DateTime, DateTime>(start_date, end_date));
            return new List<Tuple<DateTime, DateTime>>(tr);
        }
        public static long GetInternalFinancialYear(int CsutomerbranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                long Year = (from row in entities.InternalComplianceInstances
                             where row.CustomerBranchID == CsutomerbranchID
                             orderby row.StartDate
                             select row.StartDate.Year).FirstOrDefault();
                return Year;
            }
        }
        public List<Tuple<DateTime, DateTime>> GetInternaldetails(int CustomerBranchid)
        {
            List<Tuple<DateTime, DateTime>> tr = new List<Tuple<DateTime, DateTime>>();
            long year = GetInternalFinancialYear(CustomerBranchid);
            long currentyear = DateTime.Today.Date.Year;
            DateTime start_date = new DateTime();
            DateTime end_date = new DateTime();
            while (year <= currentyear)
            {
                start_date = GetDate("01/04/" + (year - 1));
                end_date = GetDate("31/03/" + year);
                tr.Add(new Tuple<DateTime, DateTime>(start_date, end_date));
                year = year + 1;
            }
            start_date = GetDate("01/04/" + (currentyear));
            end_date = DateTime.Today.Date;
            tr.Add(new Tuple<DateTime, DateTime>(start_date, end_date));
            return new List<Tuple<DateTime, DateTime>>(tr);
        }
        public decimal GetPercentage(string Values, string Roles)
        {
            return 0;
            decimal returnvalue = 0;
            decimal PUS = 0;
            decimal PUI = 0;
            decimal POS = 0;
            decimal POI = 0;
            decimal PPRS = 0;
            decimal PPRI = 0;
            decimal PCS = 0;
            decimal PCI = 0;
            decimal PRS = 0;
            decimal PRI = 0;

            decimal RDS = 0;
            decimal RDI = 0;
            decimal RPRS = 0;
            decimal RPRI = 0;
            decimal RCS = 0;
            decimal RCI = 0;
            decimal RRS = 0;
            decimal RRI = 0;

            decimal AES = 0;
            decimal AEI = 0;
            decimal ACES = 0;
            decimal ACEI = 0;
            decimal CES = 0;
            decimal CEI = 0;
            try
            {
                switch (Values)
                {
                    //Performer Upcoming Satutory
                    case "PUS":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divupcomingPREOcount.InnerText) != 0)
                            {
                                PUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divupcomingPRcount.InnerText) != 0)
                            {
                                PUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPRcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPRcount.InnerText))) * 100);
                            }
                        }
                        else if (Roles == "PEO")
                        {
                            if (Convert.ToInt32(divupcomingPEOcount.InnerText) != 0)
                            {
                                PUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPEOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPEOcount.InnerText))) * 100);
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divPerformerOnlyupcomingcount.InnerText) != 0)
                            {
                                PUS = Convert.ToInt32((Convert.ToDecimal(divPerformerOnlyupcomingcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divPerformerOnlyupcomingcount.InnerText))) * 100);
                            }
                        }
                        returnvalue = PUS;
                        break;
                    //Performer Upcoming Internal
                    case "PUI":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divupcomingInternalPREOcount.InnerText) != 0)
                            {
                                PUI = Convert.ToInt32((Convert.ToDecimal(divupcomingInternalPREOcount.InnerText) / (InternalPerformerTotal + Convert.ToDecimal(divupcomingInternalPREOcount.InnerText))) * 100);
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divupcomingInternalPRcount.InnerText) != 0)
                            {
                                PUI = Convert.ToInt32((Convert.ToDecimal(divupcomingInternalPRcount.InnerText) / (InternalPerformerTotal + Convert.ToDecimal(divupcomingInternalPRcount.InnerText))) * 100);
                            }
                        }
                        else if (Roles == "PEO")
                        {
                            if (Convert.ToInt32(divupcomingInternalPEOcount.InnerText) != 0)
                            {
                                PUI = Convert.ToInt32((Convert.ToDecimal(divupcomingInternalPEOcount.InnerText) / (InternalPerformerTotal + Convert.ToDecimal(divupcomingInternalPEOcount.InnerText))) * 100);
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divPerformerOnlyupcomingInternalcount.InnerText) != 0)
                            {
                                PUI = Convert.ToInt32((Convert.ToDecimal(divPerformerOnlyupcomingInternalcount.InnerText) / (InternalPerformerTotal + Convert.ToDecimal(divPerformerOnlyupcomingInternalcount.InnerText))) * 100);
                            }
                        }
                        returnvalue = PUI;
                        break;
                    //Performer Overdue Satutory
                    case "POS":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divOverduePREOcount.InnerText) != 0)
                            {
                                POS = Convert.ToInt32((Convert.ToDecimal(divOverduePREOcount.InnerText) / PerformerTotal) * 100);
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divOverduePRcount.InnerText) != 0)
                            {
                                POS = Convert.ToInt32((Convert.ToDecimal(divOverduePRcount.InnerText) / PerformerTotal) * 100);
                            }
                        }
                        else if (Roles == "PEO")
                        {
                            if (Convert.ToInt32(divOverduePEOcount.InnerText) != 0)
                            {
                                POS = Convert.ToInt32((Convert.ToDecimal(divOverduePEOcount.InnerText) / PerformerTotal) * 100);
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divPerformerOnlyOverduecount.InnerText) != 0)
                            {
                                POS = Convert.ToInt32((Convert.ToDecimal(divPerformerOnlyOverduecount.InnerText) / PerformerTotal) * 100);
                            }
                        }
                        returnvalue = POS;
                        break;
                    //Performer Overdue Internal
                    case "POI":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divOverdueInternalPREOcount.InnerText) != 0)
                            {
                                POI = Convert.ToInt32((Convert.ToDecimal(divOverdueInternalPREOcount.InnerText) / InternalPerformerTotal) * 100);
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divOverdueInternalPRcount.InnerText) != 0)
                            {
                                POI = Convert.ToInt32((Convert.ToDecimal(divOverdueInternalPRcount.InnerText) / InternalPerformerTotal) * 100);
                            }
                        }
                        else if (Roles == "PEO")
                        {
                            if (Convert.ToInt32(divOverdueInternalPEOcount.InnerText) != 0)
                            {
                                POI = Convert.ToInt32((Convert.ToDecimal(divOverdueInternalPEOcount.InnerText) / InternalPerformerTotal) * 100);
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divPerformerOnlyOverdueInternalcount.InnerText) != 0)
                            {
                                POI = Convert.ToInt32((Convert.ToDecimal(divPerformerOnlyOverdueInternalcount.InnerText) / InternalPerformerTotal) * 100);
                            }
                        }
                        returnvalue = POI;
                        break;
                    //Performer Pending For Review Satutory
                    case "PPRS":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divPerformerPendingforReviewPREOcount.InnerText) != 0)
                            {
                                if (Performercomplited != 0)
                                {
                                    PPRS = Convert.ToInt32((Convert.ToDecimal(divPerformerPendingforReviewPREOcount.InnerText) / Performercomplited) * 100);
                                }
                                else
                                {
                                    PPRS = 100;
                                }

                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divPerformerPendingforReviewPRcount.InnerText) != 0)
                            {
                                if (Performercomplited != 0)
                                {
                                    PPRS = Convert.ToInt32((Convert.ToDecimal(divPerformerPendingforReviewPRcount.InnerText) / Performercomplited) * 100);
                                }
                                else
                                {
                                    PPRS = 100;
                                }

                            }
                        }
                        else if (Roles == "PEO")
                        {
                            if (Convert.ToInt32(divPerformerPendingforReviewPEOcount.InnerText) != 0)
                            {
                                if (Performercomplited != 0)
                                {
                                    PPRS = Convert.ToInt32((Convert.ToDecimal(divPerformerPendingforReviewPEOcount.InnerText) / Performercomplited) * 100);
                                }
                                else
                                {
                                    PPRS = 100;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divPerformerOnlyPendingforReviewcount.InnerText) != 0)
                            {
                                if (Performercomplited != 0)
                                {
                                    PPRS = Convert.ToInt32((Convert.ToDecimal(divPerformerOnlyPendingforReviewcount.InnerText) / Performercomplited) * 100);
                                }
                                else
                                {
                                    PPRS = 100;
                                }
                            }
                        }
                        returnvalue = PPRS;
                        break;
                    //Performer Pending For Review Internal
                    case "PPRI":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divPerformerPendingforRevieweInternalPREOcount.InnerText) != 0)
                            {
                                if (InternalPerformercomplited != 0)
                                {
                                    PPRI = Convert.ToInt32((Convert.ToDecimal(divPerformerPendingforRevieweInternalPREOcount.InnerText) / InternalPerformercomplited) * 100);
                                }
                                else
                                {
                                    PPRI = 100;
                                }
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divPerformerPendingforRevieweInternalPRcount.InnerText) != 0)
                            {
                                if (InternalPerformercomplited != 0)
                                {
                                    PPRI = Convert.ToInt32((Convert.ToDecimal(divPerformerPendingforRevieweInternalPRcount.InnerText) / InternalPerformercomplited) * 100);
                                }
                                else
                                {
                                    PPRI = 100;
                                }
                            }
                        }
                        else if (Roles == "PEO")
                        {
                            if (Convert.ToInt32(divPerformerPendingforRevieweInternalPEOcount.InnerText) != 0)
                            {
                                if (InternalPerformercomplited != 0)
                                {
                                    PPRI = Convert.ToInt32((Convert.ToDecimal(divPerformerPendingforRevieweInternalPEOcount.InnerText) / InternalPerformercomplited) * 100);
                                }
                                else
                                {
                                    PPRI = 100;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divPerformerOnlyPendingforRevieweInternalcount.InnerText) != 0)
                            {
                                if (InternalPerformercomplited != 0)
                                {
                                    PPRI = Convert.ToInt32((Convert.ToDecimal(divPerformerOnlyPendingforRevieweInternalcount.InnerText) / InternalPerformercomplited) * 100);
                                }
                                else
                                {
                                    PPRI = 100;
                                }
                            }
                        }
                        returnvalue = PPRI;
                        break;
                    //Performer Rejected Satutory
                    case "PRS":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divPerformerRejectedPREOcount.InnerText) != 0)
                            {
                                if (Performercomplited != 0)
                                {
                                    PRS = Convert.ToInt32((Convert.ToDecimal(divPerformerRejectedPREOcount.InnerText) / Performercomplited) * 100);
                                }
                                else
                                {
                                    PRS = 100;
                                }
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divPerformerRejectedPRcount.InnerText) != 0)
                            {
                                if (Performercomplited != 0)
                                {
                                    PRS = Convert.ToInt32((Convert.ToDecimal(divPerformerRejectedPRcount.InnerText) / Performercomplited) * 100);
                                }
                                else
                                {
                                    PRS = 100;
                                }
                            }

                        }
                        else if (Roles == "PEO")
                        {
                            if (Convert.ToInt32(divPerformerRejectedPEOcount.InnerText) != 0)
                            {
                                if (Performercomplited != 0)
                                {
                                    PRS = Convert.ToInt32((Convert.ToDecimal(divPerformerRejectedPEOcount.InnerText) / Performercomplited) * 100);
                                }
                                else
                                {
                                    PRS = 100;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divPerformerOnlyRejectedcount.InnerText) != 0)
                            {
                                if (Performercomplited != 0)
                                {
                                    PRS = Convert.ToInt32((Convert.ToDecimal(divPerformerOnlyRejectedcount.InnerText) / Performercomplited) * 100);
                                }
                                else
                                {
                                    PRS = 100;
                                }
                            }
                        }
                        returnvalue = PRS;
                        break;
                    //Performer Rejected Internal
                    case "PRI":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divPerformerRejectedInternalPREOcount.InnerText) != 0)
                            {
                                if (InternalPerformercomplited != 0)
                                {
                                    PRI = Convert.ToInt32((Convert.ToDecimal(divPerformerRejectedInternalPREOcount.InnerText) / InternalPerformercomplited) * 100);
                                }
                                else
                                {
                                    PRI = 100;
                                }
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divPerformerRejectedInternalPRcount.InnerText) != 0)
                            {
                                if (InternalPerformercomplited != 0)
                                {
                                    PRI = Convert.ToInt32((Convert.ToDecimal(divPerformerRejectedInternalPRcount.InnerText) / InternalPerformercomplited) * 100);
                                }
                                else
                                {
                                    PRI = 100;
                                }
                            }
                        }
                        else if (Roles == "PEO")
                        {
                            if (Convert.ToInt32(divPerformerRejectedInternalPEOcount.InnerText) != 0)
                            {
                                if (InternalPerformercomplited != 0)
                                {
                                    PRI = Convert.ToInt32((Convert.ToDecimal(divPerformerRejectedInternalPEOcount.InnerText) / InternalPerformercomplited) * 100);
                                }
                                else
                                {
                                    PRI = 100;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divPerformerOnlyRejectedInternalcount.InnerText) != 0)
                            {
                                if (InternalPerformercomplited != 0)
                                {
                                    PRI = Convert.ToInt32((Convert.ToDecimal(divPerformerOnlyRejectedInternalcount.InnerText) / InternalPerformercomplited) * 100);
                                }
                                else
                                {
                                    PRI = 100;
                                }
                            }
                        }
                        returnvalue = PRI;
                        break;
                    //Performer Checklist Satutory
                    case "PCS":
                        if (Roles == "PREO")
                        {
                            //(Nos closed)/ (Nos Assigned)*Frequency * 100'
                            PCS = Convert.ToInt32((Convert.ToDecimal(CheckListPerformercomplited) / Convert.ToDecimal(CheckListPerformerTotal)) * 100);
                            //CheckListPerformerTotal += Total;
                            //CheckListPerformercomplited += complited;
                            //divPerformerChecklistPREOcount
                        }
                        else if (Roles == "PR")
                        {
                            PCS = Convert.ToInt32((Convert.ToDecimal(CheckListPerformercomplited) / Convert.ToDecimal(CheckListPerformerTotal)) * 100);
                            //divPerformerChecklistPRcount
                        }
                        else if (Roles == "PEO")
                        {
                            PCS = Convert.ToInt32((Convert.ToDecimal(CheckListPerformercomplited) / Convert.ToDecimal(CheckListPerformerTotal)) * 100);
                            //divPerformerChecklistPEOcount
                        }
                        else
                        {
                            PCS = Convert.ToInt32((Convert.ToDecimal(CheckListPerformercomplited) / Convert.ToDecimal(CheckListPerformerTotal)) * 100);
                            //divPerformerOnlyChecklistcount
                        }
                        returnvalue = PCS;
                        break;
                    //Performer Checklist Internal
                    case "PCI":
                        if (Roles == "PREO")
                        {
                            //divPerformerChecklistInternalPREOcount
                        }
                        else if (Roles == "PR")
                        {

                            //divPerformerChecklistInternalPRcount
                        }
                        else if (Roles == "PEO")
                        {
                            //divPerformerChecklistInternalPEOcount
                        }
                        else
                        {
                            //divPerformerOnlyChecklistInternalcount
                        }
                        PCI = 0;
                        returnvalue = PCI;
                        break;

                    //Reviewer DueButNotSubmitted Satutory             
                    case "RDS":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divDueButNotSubmittedPREOcount.InnerText) != 0)
                            {
                                if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedPREOcount.InnerText) / ReviewerTotal) * 100), 2) < 1)
                                {
                                    RDS = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedPREOcount.InnerText) / ReviewerTotal) * 100), 2));
                                }
                                else
                                {
                                    RDS = Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedPREOcount.InnerText) / ReviewerTotal) * 100));
                                }
                            }

                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divDueButNotSubmittedPRcount.InnerText) != 0)
                            {
                                if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedPRcount.InnerText) / ReviewerTotal) * 100), 2) < 1)
                                {
                                    RDS = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedPRcount.InnerText) / ReviewerTotal) * 100), 2));
                                }
                                else
                                {
                                    RDS = Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedPRcount.InnerText) / ReviewerTotal) * 100));
                                }
                            }

                            //RDS = Convert.ToInt32((Convert.ToDecimal(divDueButNotSubmittedPRcount.InnerText) / PerformerTotal) * 100);
                        }
                        else if (Roles == "REO")
                        {
                            if (Convert.ToInt32(divDueButNotSubmittedREOcount.InnerText) != 0)
                            {
                                if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedREOcount.InnerText) / ReviewerTotal) * 100), 2) < 1)
                                {
                                    RDS = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedREOcount.InnerText) / ReviewerTotal) * 100), 2));
                                }
                                else
                                {
                                    RDS = Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedREOcount.InnerText) / ReviewerTotal) * 100));
                                }
                            }
                            //RDS = Convert.ToInt32((Convert.ToDecimal(divDueButNotSubmittedREOcount.InnerText) / PerformerTotal) * 100);                            
                        }
                        else
                        {
                            if (Convert.ToInt32(divRevieweronlyDueButNotSubmittedcount.InnerText) != 0)
                            {
                                if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(divRevieweronlyDueButNotSubmittedcount.InnerText) / ReviewerTotal) * 100), 2) < 1)
                                {
                                    RDS = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(divRevieweronlyDueButNotSubmittedcount.InnerText) / ReviewerTotal) * 100), 2));
                                }
                                else
                                {
                                    RDS = Math.Round(Convert.ToDecimal((Convert.ToDecimal(divRevieweronlyDueButNotSubmittedcount.InnerText) / ReviewerTotal) * 100));
                                }
                            }

                            //RDS = Convert.ToInt32((Convert.ToDecimal(divRevieweronlyDueButNotSubmittedcount.InnerText) / PerformerTotal) * 100);                            
                        }
                        returnvalue = RDS;
                        break;
                    //Reviewer DueButNotSubmitted Internal   
                    case "RDI":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divDueButNotSubmittedInternalPREOcount.InnerText) != 0)
                            {
                                if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedInternalPREOcount.InnerText) / InternalReviewerTotal) * 100), 2) < 1)
                                {
                                    RDI = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedInternalPREOcount.InnerText) / InternalReviewerTotal) * 100), 2));
                                }
                                else
                                {
                                    RDI = Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedInternalPREOcount.InnerText) / InternalReviewerTotal) * 100));
                                }
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divDueButNotSubmittedInternalPRcount.InnerText) != 0)
                            {
                                if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedInternalPRcount.InnerText) / InternalReviewerTotal) * 100), 2) < 1)
                                {
                                    RDI = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedInternalPRcount.InnerText) / InternalReviewerTotal) * 100), 2));
                                }
                                else
                                {
                                    RDI = Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedInternalPRcount.InnerText) / InternalReviewerTotal) * 100));
                                }
                            }
                        }
                        else if (Roles == "REO")
                        {
                            if (Convert.ToInt32(divDueButNotSubmittedInternalREOcount.InnerText) != 0)
                            {
                                if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedInternalREOcount.InnerText) / InternalReviewerTotal) * 100), 2) < 1)
                                {
                                    RDI = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedInternalREOcount.InnerText) / InternalReviewerTotal) * 100), 2));
                                }
                                else
                                {
                                    RDI = Math.Round(Convert.ToDecimal((Convert.ToDecimal(divDueButNotSubmittedInternalREOcount.InnerText) / InternalReviewerTotal) * 100));
                                }
                            }

                        }
                        else
                        {
                            if (Convert.ToInt32(divRevieweronlyDueButNotSubmittedInternalcount.InnerText) != 0)
                            {
                                if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(divRevieweronlyDueButNotSubmittedInternalcount.InnerText) / InternalReviewerTotal) * 100), 2) < 1)
                                {
                                    RDI = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(divRevieweronlyDueButNotSubmittedInternalcount.InnerText) / InternalReviewerTotal) * 100), 2));
                                }
                                else
                                {
                                    RDI = Math.Round(Convert.ToDecimal((Convert.ToDecimal(divRevieweronlyDueButNotSubmittedInternalcount.InnerText) / InternalReviewerTotal) * 100));
                                }
                            }
                        }
                        returnvalue = RDI;
                        break;
                    case "RPRS":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divReviewerPendingforReviewePREOcount.InnerText) != 0)
                            {
                                if (Reviewercomplited != 0)
                                {
                                    RPRS = Convert.ToInt32((Convert.ToDecimal(divReviewerPendingforReviewePREOcount.InnerText) / Reviewercomplited) * 100);
                                }
                                else
                                {
                                    RPRS = 100;
                                }
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divReviewerPendingforReviewerPRcount.InnerText) != 0)
                            {
                                if (Reviewercomplited != 0)
                                {
                                    RPRS = Convert.ToInt32((Convert.ToDecimal(divReviewerPendingforReviewerPRcount.InnerText) / Reviewercomplited) * 100);
                                }
                                else
                                {
                                    RPRS = 100;
                                }
                            }
                        }
                        else if (Roles == "REO")
                        {
                            if (Convert.ToInt32(divReviewerPendingforReviewerREOcount.InnerText) != 0)
                            {
                                if (Reviewercomplited != 0)
                                {
                                    RPRS = Convert.ToInt32((Convert.ToDecimal(divReviewerPendingforReviewerREOcount.InnerText) / Reviewercomplited) * 100);
                                }
                                else
                                {
                                    RPRS = 100;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divRevieweronlyReviewerPendingforReviewercount.InnerText) != 0)
                            {
                                if (Reviewercomplited != 0)
                                {
                                    RPRS = Convert.ToInt32((Convert.ToDecimal(divRevieweronlyReviewerPendingforReviewercount.InnerText) / Reviewercomplited) * 100);
                                }
                                else
                                {
                                    RPRS = 100;
                                }
                            }
                        }
                        returnvalue = RPRS;
                        break;
                    case "RPRI":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divReviewerPendingforRevieweInternalPREOcount.InnerText) != 0)
                            {
                                if (InternalReviewercomplited != 0)
                                {
                                    RPRI = Convert.ToInt32((Convert.ToDecimal(divReviewerPendingforRevieweInternalPREOcount.InnerText) / InternalReviewercomplited) * 100);
                                }
                                else
                                {
                                    RPRI = 100;
                                }
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divReviewerPendingforReviewerInternalPRcount.InnerText) != 0)
                            {
                                if (InternalReviewercomplited != 0)
                                {
                                    RPRI = Convert.ToInt32((Convert.ToDecimal(divReviewerPendingforReviewerInternalPRcount.InnerText) / InternalReviewercomplited) * 100);
                                }
                                else
                                {
                                    RPRI = 100;
                                }
                            }
                        }
                        else if (Roles == "REO")
                        {
                            if (Convert.ToInt32(divReviewerPendingforReviewerInternalREOcount.InnerText) != 0)
                            {
                                if (InternalReviewercomplited != 0)
                                {
                                    RPRI = Convert.ToInt32((Convert.ToDecimal(divReviewerPendingforReviewerInternalREOcount.InnerText) / InternalReviewercomplited) * 100);
                                }
                                else
                                {
                                    RPRI = 100;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divRevieweronlyReviewerPendingforReviewerInternalcount.InnerText) != 0)
                            {
                                if (InternalReviewercomplited != 0)
                                {
                                    RPRI = Convert.ToInt32((Convert.ToDecimal(divRevieweronlyReviewerPendingforReviewerInternalcount.InnerText) / InternalReviewercomplited) * 100);
                                }
                                else
                                {
                                    RPRI = 100;
                                }
                            }
                        }
                        returnvalue = RPRI;
                        break;
                    case "RRS":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divReviewerRejectedPREOcount.InnerText) != 0)
                            {
                                if (Reviewercomplited != 0)
                                {
                                    RRS = Convert.ToInt32((Convert.ToDecimal(divReviewerRejectedPREOcount.InnerText) / Reviewercomplited) * 100);
                                }
                                else
                                {
                                    RRS = 100;
                                }
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divReviewerRejectedPRcount.InnerText) != 0)
                            {
                                if (Reviewercomplited != 0)
                                {
                                    RRS = Convert.ToInt32((Convert.ToDecimal(divReviewerRejectedPRcount.InnerText) / Reviewercomplited) * 100);
                                }
                                else
                                {
                                    RRS = 100;
                                }
                            }
                        }
                        else if (Roles == "REO")
                        {
                            if (Convert.ToInt32(divReviewerRejectedREOcount.InnerText) != 0)
                            {
                                if (Reviewercomplited != 0)
                                {
                                    RRS = Convert.ToInt32((Convert.ToDecimal(divReviewerRejectedREOcount.InnerText) / Reviewercomplited) * 100);
                                }
                                else
                                {
                                    RRS = 100;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divRevieweronlyRejectedcount.InnerText) != 0)
                            {
                                if (Reviewercomplited != 0)
                                {
                                    RRS = Convert.ToInt32((Convert.ToDecimal(divRevieweronlyRejectedcount.InnerText) / Reviewercomplited) * 100);
                                }
                                else
                                {
                                    RRS = 100;
                                }
                            }
                        }
                        returnvalue = RRS;
                        break;
                    case "RRI":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divReviewerRejectedInternalPREOcount.InnerText) != 0)
                            {
                                if (InternalReviewercomplited != 0)
                                {
                                    RRI = Convert.ToInt32((Convert.ToDecimal(divReviewerRejectedInternalPREOcount.InnerText) / InternalReviewercomplited) * 100);
                                }
                                else
                                {
                                    RRI = 100;
                                }
                            }
                        }
                        else if (Roles == "PR")
                        {
                            if (Convert.ToInt32(divReviewerRejectedInternalPRcount.InnerText) != 0)
                            {
                                if (InternalReviewercomplited != 0)
                                {
                                    RRI = Convert.ToInt32((Convert.ToDecimal(divReviewerRejectedInternalPRcount.InnerText) / InternalReviewercomplited) * 100);
                                }
                                else
                                {
                                    RRI = 100;
                                }
                            }
                        }
                        else if (Roles == "REO")
                        {
                            if (Convert.ToInt32(divReviewerRejectedInternalREOcount.InnerText) != 0)
                            {
                                if (InternalReviewercomplited != 0)
                                {
                                    RRI = Convert.ToInt32((Convert.ToDecimal(divReviewerRejectedInternalREOcount.InnerText) / InternalReviewercomplited) * 100);
                                }
                                else
                                {
                                    RRI = 100;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divRevieweronlyRejectedInternalcount.InnerText) != 0)
                            {
                                if (InternalReviewercomplited != 0)
                                {
                                    RRI = Convert.ToInt32((Convert.ToDecimal(divRevieweronlyRejectedInternalcount.InnerText) / InternalReviewercomplited) * 100);
                                }
                                else
                                {
                                    RRI = 100;
                                }
                            }
                        }
                        returnvalue = RRI;
                        break;
                    case "RCS":
                        if (Roles == "PREO")
                        {
                            //divReviewerChecklistPREOcount                           
                            RCS = 0;// Convert.ToInt32((Convert.ToDecimal(CheckListPerformercomplited) / Convert.ToDecimal(CheckListPerformerTotal)) * 100);
                        }
                        else if (Roles == "PR")
                        {
                            RCS = 0;// Convert.ToInt32((Convert.ToDecimal(CheckListPerformercomplited) / Convert.ToDecimal(CheckListPerformerTotal)) * 100);
                            //divReviewerChecklistPRcount
                        }
                        else if (Roles == "REO")
                        {
                            RCS = 0;// Convert.ToInt32((Convert.ToDecimal(CheckListPerformercomplited) / Convert.ToDecimal(CheckListPerformerTotal)) * 100);
                            //divReviewerChecklistREOcount
                        }
                        else
                        {
                            RCS = 0;// Convert.ToInt32((Convert.ToDecimal(CheckListPerformercomplited) / Convert.ToDecimal(CheckListPerformerTotal)) * 100);
                            //divRevieweronlyChecklistcount
                        }
                        //RCS = 80;
                        returnvalue = RCS;
                        break;
                    case "RCI":
                        if (Roles == "PREO")
                        {
                            //divReviewerChecklistInternalPREOcount
                        }
                        else if (Roles == "PR")
                        {
                            //divReviewerChecklistInternalPRcount
                        }
                        else if (Roles == "REO")
                        {
                            //divReviewerChecklistInternalREOcount
                        }
                        else
                        {
                            //divRevieweronlyChecklistInternalcount
                        }
                        RCI = 0;
                        returnvalue = RCI;
                        break;
                    case "AES":
                        if (Roles == "PREO")
                        {
                            //divAssignedEventPREOcount                        
                        }
                        else if (Roles == "PEO")
                        {
                            //divAssignedEventPEOcount
                        }
                        else if (Roles == "REO")
                        {
                            //divAssignedEventREOcount
                        }
                        else
                        {
                            //divEventOwnerAssignedEventOnlycount
                        }
                        AES = 50;
                        returnvalue = AES;
                        break;
                    case "AEI":
                        AEI = 30;
                        returnvalue = AEI;
                        break;
                    case "ACES":
                        if (Roles == "PREO")
                        {
                            //divActivatedEventPREOcount                         
                        }
                        else if (Roles == "PEO")
                        {
                            //divActivatedEventPEOcount  
                        }
                        else if (Roles == "REO")
                        {
                            //divActivatedEventREOcount
                        }
                        else
                        {
                            //divEventOwnerActivatedEventOnlycount
                        }
                        ACES = 50;
                        returnvalue = ACES;
                        break;
                    case "ACEI":
                        ACEI = 30;
                        returnvalue = ACEI;
                        break;
                    case "CES":
                        if (Roles == "PREO")
                        {
                            if (Convert.ToInt32(divClosedEventPREOcount.InnerText) != 0)
                            {
                                if (Convert.ToInt32(divClosedEventPREOcount.InnerText) == 0)
                                {
                                    CES = 0;
                                }
                                else
                                {
                                    CES = 20;
                                }
                            }
                        }
                        else if (Roles == "PEO")
                        {
                            if (Convert.ToInt32(divClosedEventPEOcount.InnerText) != 0)
                            {
                                if (Convert.ToInt32(divClosedEventPEOcount.InnerText) == 0)
                                {
                                    CES = 0;
                                }
                                else
                                {
                                    CES = 20;
                                }
                            }
                        }
                        else if (Roles == "REO")
                        {
                            if (Convert.ToInt32(divClosedEventREOcount.InnerText) != 0)
                            {
                                if (Convert.ToInt32(divClosedEventREOcount.InnerText) == 0)
                                {
                                    CES = 0;
                                }
                                else
                                {
                                    CES = 20;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(divClosedEventOnlycount.InnerText) != 0)
                            {
                                if (Convert.ToInt32(divClosedEventOnlycount.InnerText) == 0)
                                {
                                    CES = 0;
                                }
                                else
                                {
                                    CES = 20;
                                }
                            }
                        }
                        returnvalue = CES;
                        break;
                    case "CEI":
                        CEI = 30;
                        returnvalue = CEI;
                        break;
                    default:
                        Console.WriteLine("Invalid grade");
                        break;
                }
                return returnvalue;

            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }
        #region Satutory Graph Display
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public void GetAllDataPerformerCheckList()
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gettotalrecords = GetChecKPerformerAndReviewerProcedure(CustomerId, AuthenticationHelper.UserID, 3);
            if (gettotalrecords != null && gettotalrecords.Count != 0)
            {
                foreach (var item in gettotalrecords)
                {
                    decimal percentge = 0;
                    List<Tuple<DateTime, DateTime>> financialyearlist = GetSatutorydetails(item.CustomerBranchid);
                    if (financialyearlist != null)
                    {
                        foreach (var lst in financialyearlist)
                        {
                            var Total = GetChecKTotalCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 3, lst.Item1, lst.Item2).Count();
                            var complited = GetChecKComplitedCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 3, lst.Item1, lst.Item2).Count();
                            if ((Convert.ToInt32(Total) - Convert.ToInt32(complited)) > 0)
                            {
                                CheckListPerformerTotal += Total;
                                CheckListPerformercomplited += complited;
                            }
                        }
                        if (CheckListPerformerTotal != 0 && CheckListPerformercomplited != 0)
                        {
                            percentge = Convert.ToInt32(((Convert.ToDecimal(CheckListPerformerTotal) - Convert.ToDecimal(CheckListPerformercomplited)) / Convert.ToDecimal(CheckListPerformerTotal)) * 100);
                        }
                    }
                }
            }
        }
        public void GetAllDataPerformer()
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gettotalrecords = GetPerformerAndReviewerProcedure(CustomerId, AuthenticationHelper.UserID, 3);
            if (gettotalrecords != null && gettotalrecords.Count != 0)
            {
                string str = "";
                foreach (var item in gettotalrecords)
                {
                    GraphDisplay display = new GraphDisplay();
                    decimal percentge = 0;
                    List<Tuple<DateTime, DateTime>> financialyearlist = GetSatutorydetails(item.CustomerBranchid);
                    if (financialyearlist != null)
                    {
                        foreach (var lst in financialyearlist)
                        {
                            var Total = GetTotalCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 3, lst.Item1, lst.Item2).Count();
                            var complited = GetComplitedCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 3, lst.Item1, lst.Item2).Count();
                            if ((Convert.ToInt32(Total) - Convert.ToInt32(complited)) > 0)
                            {
                                PerformerTotal += Total;
                                Performercomplited += complited;
                            }
                        }
                        if (PerformerTotal != 0 && Performercomplited != 0)
                        {
                            if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(Performercomplited) / PerformerTotal) * 100), 2) < 1)
                            {
                                percentge = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(Performercomplited) / PerformerTotal) * 100), 2));
                            }
                            else
                            {
                                percentge = Math.Round(Convert.ToDecimal((Convert.ToDecimal(Performercomplited) / PerformerTotal) * 100));
                            }
                            //percentge = Convert.ToInt32(((Convert.ToDecimal(Performercomplited)) / Convert.ToDecimal(PerformerTotal)) * 100);
                        }
                        display.Name = "Location";
                        display.LocationName = item.Name;
                        display.Percentage = Convert.ToInt32(percentge);
                        str += " ['" + display.Name + "', '" + display.LocationName + "', '" + display.Percentage + "'],";
                    }
                }
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript", "<script language='javascript'> var locations = [" + str.TrimEnd(',') + "];</script>");
            }
        }
        public void GetAllDataReviewer()
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gettotalrecords = GetPerformerAndReviewerProcedure(CustomerId, AuthenticationHelper.UserID, 4);
            if (gettotalrecords != null && gettotalrecords.Count != 0)
            {
                string str = "";
                foreach (var item in gettotalrecords)
                {
                    GraphDisplay display = new GraphDisplay();
                    decimal percentge = 0;
                    List<Tuple<DateTime, DateTime>> financialyearlist = GetSatutorydetails(item.CustomerBranchid);
                    if (financialyearlist != null)
                    {
                        foreach (var lst in financialyearlist)
                        {
                            var Total = GetTotalCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 4, lst.Item1, lst.Item2).Count();
                            var complited = GetComplitedCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 4, lst.Item1, lst.Item2).Count();
                            if ((Convert.ToInt32(Total) - Convert.ToInt32(complited)) > 0)
                            {
                                ReviewerTotal += Total;
                                Reviewercomplited += complited;
                            }
                        }
                        if (ReviewerTotal != 0 && Reviewercomplited != 0)
                        {
                            //  percentge = Convert.ToInt32(((Convert.ToDecimal(ReviewerTotal) - Convert.ToDecimal(Reviewercomplited)) / Convert.ToDecimal(ReviewerTotal)) * 100);

                            if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(Reviewercomplited) / ReviewerTotal) * 100), 2) < 1)
                            {
                                percentge = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(Reviewercomplited) / ReviewerTotal) * 100), 2));
                            }
                            else
                            {
                                percentge = Math.Round(Convert.ToDecimal((Convert.ToDecimal(Reviewercomplited) / ReviewerTotal) * 100));
                            }
                            //  percentge = Convert.ToInt32(((Convert.ToDecimal(Reviewercomplited)) / Convert.ToDecimal(ReviewerTotal)) * 100);
                        }
                        display.Name = "Location";
                        display.LocationName = item.Name;
                        display.Percentage = Convert.ToInt32(percentge);
                        str += " ['" + display.Name + "', '" + display.LocationName + "', '" + display.Percentage + "'],";
                    }
                }
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript1", "<script language='javascript'> var locations1 = [" + str.TrimEnd(',') + "];</script>");
            }
        }
        #endregion

        #region Internal
        public static List<Sp_InternalGetComplitedCount_Result> GetInternalComplitedCountProcedure(int Customerbranchid, int userid, int roleid, DateTime startdate, DateTime Enddate)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<Sp_InternalGetComplitedCount_Result> complianceReminders = new List<Sp_InternalGetComplitedCount_Result>();
                if (Enddate.Date > DateTime.Today.Date)
                {
                    complianceReminders = entities.Sp_InternalGetComplitedCount(Customerbranchid, userid, roleid, startdate, DateTime.Today.Date).ToList();
                }
                else
                {
                    complianceReminders = entities.Sp_InternalGetComplitedCount(Customerbranchid, userid, roleid, startdate, Enddate).ToList();
                }

                return complianceReminders;
            }
        }
        public static List<Sp_InternalGetComplitedCount_Result> GetInternalComplitedCountProcedure(int Customerbranchid, int userid, int roleid, DateTime startdate, DateTime Enddate, List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> MasterQuery)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<Sp_InternalGetComplitedCount_Result> complianceReminders = new List<Sp_InternalGetComplitedCount_Result>();
                //if (Enddate.Date > DateTime.Today.Date)
                //{
                //    complianceReminders = entities.Sp_InternalGetComplitedCount(Customerbranchid, userid, roleid, startdate, DateTime.Today.Date).ToList();
                //}
                //else
                //{
                //    complianceReminders = entities.Sp_InternalGetComplitedCount(Customerbranchid, userid, roleid, startdate, Enddate).ToList();
                //}
                List<int> status = new List<int>();
                status.Add(4);
                status.Add(5);
                status.Add(9);
                complianceReminders = (from row in MasterQuery
                                       where status.Contains((int)row.InternalComplianceStatusID)
                                       select new Sp_InternalGetComplitedCount_Result
                                       {
                                           InternalComplianceInstanceID = row.InternalComplianceInstanceID,
                                           RoleID = row.RoleID,
                                           CustomerBranchid = row.CustomerBranchID,
                                           SCheduledonid = row.InternalScheduledOnID,
                                           ScheduledOn = row.InternalScheduledOn

                                       }).ToList();
                return complianceReminders;
            }
        }
        public static List<Sp_InternalGetTotalCount_Result> GetInternalTotalCountProcedure(int Customerbranchid, int userid, int roleid, DateTime startdate, DateTime Enddate)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<Sp_InternalGetTotalCount_Result> complianceReminders = new List<Sp_InternalGetTotalCount_Result>();
                if (Enddate.Date > DateTime.Today.Date)
                {
                    complianceReminders = entities.Sp_InternalGetTotalCount(Customerbranchid, userid, roleid, startdate, DateTime.Today.Date).ToList();
                }
                else
                {
                    complianceReminders = entities.Sp_InternalGetTotalCount(Customerbranchid, userid, roleid, startdate, Enddate).ToList();
                }

                return complianceReminders;
            }
        }

        public static List<Sp_InternalGetTotalCount_Result> GetInternalTotalCountProcedure(int Customerbranchid, int userid, int roleid, DateTime startdate, DateTime Enddate, List<SP_InternalComplianceInstanceTransactionCount_DashBoard_Result> MasterQuery)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<Sp_InternalGetTotalCount_Result> complianceReminders = new List<Sp_InternalGetTotalCount_Result>();
                //if (Enddate.Date > DateTime.Today.Date)
                //{
                //    complianceReminders = entities.Sp_InternalGetTotalCount(Customerbranchid, userid, roleid, startdate, DateTime.Today.Date).ToList();
                //}
                //else
                //{
                //    complianceReminders = entities.Sp_InternalGetTotalCount(Customerbranchid, userid, roleid, startdate, Enddate).ToList();
                //}
                complianceReminders = (from row in MasterQuery

                                       select new Sp_InternalGetTotalCount_Result
                                       {
                                           InternalComplianceInstanceID = row.InternalComplianceInstanceID,
                                           RoleID = row.RoleID,
                                           CustomerBranchid = row.CustomerBranchID,
                                           ScheduledOnId = row.InternalScheduledOnID,
                                           ScheduledOn = row.InternalScheduledOn

                                       }).ToList();
                return complianceReminders;
            }
        }

        public static List<Sp_InternalGetPerformerAndReviewer_Result> GetInternalPerformerAndReviewerProcedure(int Customerid, int userid, int roleid)
        {
            //date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceReminders = entities.Sp_InternalGetPerformerAndReviewer(Customerid, userid, roleid).ToList();
                complianceReminders = complianceReminders.GroupBy(a => a.Name).Select(a => a.FirstOrDefault()).ToList();
                return complianceReminders;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var plainBytes = Encoding.UTF8.GetBytes(Session["Email"].ToString());
                UserInformation = Convert.ToBase64String(CryptographyHandler.Encrypt(plainBytes, CryptographyHandler.GetRijndaelManaged("avantis")));
            }
            catch
            {

            }

            user_Roles = AuthenticationHelper.Role;
            if (Session["User_comp_Roles"] != null)
            {
                roles = Session["User_comp_Roles"] as List<int>;
            }
            else
            {
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                Session["User_comp_Roles"] = roles;
            }
            if (Session["User_tasks_Roles"] != null)
            {
                TaskRoles = Session["User_tasks_Roles"] as List<int>;

            }
            else
            {
                TaskRoles = TaskManagment.GetAssignedTaskUserRole(AuthenticationHelper.UserID);
                Session["User_tasks_Roles"] = TaskRoles;
            }
            if (Session["User_dept"] != null)
            {
                DeptHead = (bool)Session["User_dept"];
            }
            else
            {
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                if (!string.IsNullOrEmpty(Convert.ToString(LoggedUser.IsHead)))
                {
                    DeptHead = (bool)LoggedUser.IsHead;
                }
            }
            try
            {
                if (AuthenticationHelper.ComplianceProductType == 3)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var userTLAssignedComplianceList = (entities.USP_RLCS_GetTLAssignedCompliances(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();

                        if (userTLAssignedComplianceList != null)
                            if (userTLAssignedComplianceList.Count > 0)
                                IsTLEntityAssignment = true;
                    }
                }
            }
            catch (Exception ex) { }

            if (!IsPostBack)
            {
                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                home.Value = "true";
                if (Session["userID"] != null)
                {
                    int userID = Convert.ToInt32(Session["userID"]);
                    if (AuthenticationHelper.CustomerID == -1)
                    {
                        checkInternalapplicable = 2;
                        checkTaskapplicable = 2;
                    }
                    else
                    {
                        // 17 April 2019 Commented by Narendra and written new code to get values from Authentication helper
                        //var IsInternalComplianceApplicable = CustomerManagement.GetByIcompilanceapplicableID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        //if (IsInternalComplianceApplicable != -1)
                        //{
                        //    checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                        //}

                        //var IsTaskApplicable = CustomerManagement.GetByTaskApplicableID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        //if (IsTaskApplicable != -1)
                        //{
                        //    checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                        //}
                        checkInternalapplicable = Convert.ToInt32(AuthenticationHelper.IComplilanceApplicable);
                        checkTaskapplicable = Convert.ToInt32(AuthenticationHelper.TaskApplicable);
                    }


                }

                //udcCalender.OnSaved += (inputForm, args) => { BindCompliances("1"); };

                //Commented on 17 April 2019 
                //IsApplicableInternal = TaskManagment.CheckTaskTypeApplicable(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 2);

                if (TaskRoles != null & TaskRoles.Count > 0)
                {
                    IsApplicableInternal = true;
                    setTaskCounts();
                }
                setCompliancesCounts();

                //GetCalender();
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "fcal", "fcal('" + CalendarTodayOrNextDate + "');", true);
                int UnreadNotifications = Business.ComplianceManagement.GetUnreadUserNotification(AuthenticationHelper.UserID);

                if (UnreadNotifications > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "divNotification", "openModal();", true);
                }
            }
        }

        #region Internal Graph Display
        public void GetAllDataPerformerInternal(List<Sp_InternalGetTotalCount_Result> GetTotalInternalCountList, List<Sp_InternalGetComplitedCount_Result> GetComplitedInternalCountList)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gettotalrecords = GetInternalPerformerAndReviewerProcedure(CustomerId, AuthenticationHelper.UserID, 3).Distinct().ToList();
            if (gettotalrecords != null && gettotalrecords.Count != 0)
            {
                string str = "";
                foreach (var item in gettotalrecords)
                {
                    GraphDisplay display = new GraphDisplay();
                    decimal percentge = 0;
                    List<Tuple<DateTime, DateTime>> financialyearlist = GetInternaldetails(item.CustomerBranchid);
                    if (financialyearlist != null)
                    {
                        foreach (var lst in financialyearlist)
                        {
                            // var Total = GetInternalTotalCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 3,lst.Item1, lst.Item2).Count();
                            //var complited = GetInternalComplitedCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 3, lst.Item1, lst.Item2).Count();
                            if (GetTotalInternalCountList.Count > 0)
                            {
                                var Total = GetTotalInternalCountList.Where(entry => entry.CustomerBranchid == item.CustomerBranchid
                                          && entry.RoleID == 3 && (entry.ScheduledOn >= lst.Item1 && entry.ScheduledOn <= lst.Item2)).ToList().Count();

                                if (GetComplitedInternalCountList.Count > 0)
                                {
                                    var complited = GetComplitedInternalCountList.Where(entry => entry.CustomerBranchid == item.CustomerBranchid
                                          && entry.RoleID == 3 && (entry.ScheduledOn >= lst.Item1 && entry.ScheduledOn <= lst.Item2)).ToList().Count();

                                    if ((Convert.ToInt32(Total) - Convert.ToInt32(complited)) > 0)
                                    {
                                        InternalPerformerTotal += Total;
                                        InternalPerformercomplited += complited;
                                    }
                                }//ComplitedInternalCount end
                            }//TotalInternalCount end
                        }
                        if (InternalPerformerTotal != 0 && InternalPerformercomplited != 0)
                        {
                            if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalPerformercomplited) / InternalPerformerTotal) * 100), 2) < 1)
                            {
                                percentge = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalPerformercomplited) / InternalPerformerTotal) * 100), 2));
                            }
                            else
                            {
                                percentge = Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalPerformercomplited) / InternalPerformerTotal) * 100));
                            }
                        }
                        display.Name = "Location";
                        display.LocationName = item.Name;
                        display.Percentage = Convert.ToInt32(percentge);
                        str += " ['" + display.Name + "', '" + display.LocationName + "', '" + display.Percentage + "'],";
                    }
                }
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript2", "<script language='javascript'> var locationsInternal = [" + str.TrimEnd(',') + "];</script>");
            }
        }
        public void GetAllDataReviewerInternal(List<Sp_InternalGetTotalCount_Result> GetTotalInternalCountList, List<Sp_InternalGetComplitedCount_Result> GetComplitedInternalCountList)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gettotalrecords = GetInternalPerformerAndReviewerProcedure(CustomerId, AuthenticationHelper.UserID, 4);
            if (gettotalrecords != null && gettotalrecords.Count != 0)
            {
                string str = "";
                foreach (var item in gettotalrecords)
                {
                    GraphDisplay display = new GraphDisplay();
                    decimal percentge = 0;
                    List<Tuple<DateTime, DateTime>> financialyearlist = GetInternaldetails(item.CustomerBranchid);
                    if (financialyearlist != null)
                    {
                        foreach (var lst in financialyearlist)
                        {
                            if (GetTotalInternalCountList.Count > 0)
                            {
                                //var Total = GetInternalTotalCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 4, lst.Item1, lst.Item2).Count();
                                //var complited = GetInternalComplitedCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 4, lst.Item1, lst.Item2).Count();
                                var Total = GetTotalInternalCountList.Where(entry => entry.CustomerBranchid == item.CustomerBranchid
                                          && entry.RoleID == 4 && (entry.ScheduledOn >= lst.Item1 && entry.ScheduledOn <= lst.Item2)).ToList().Count();
                                if (GetComplitedInternalCountList.Count > 0)
                                {
                                    var complited = GetComplitedInternalCountList.Where(entry => entry.CustomerBranchid == item.CustomerBranchid
                                         && entry.RoleID == 4 && (entry.ScheduledOn >= lst.Item1 && entry.ScheduledOn <= lst.Item2)).ToList().Count();

                                    if ((Convert.ToInt32(Total) - Convert.ToInt32(complited)) > 0)
                                    {
                                        InternalReviewerTotal += Total;
                                        InternalReviewercomplited += complited;
                                    }
                                }
                            }
                        }
                        if (InternalReviewerTotal != 0 && InternalReviewercomplited != 0)
                        {
                            if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalReviewercomplited) / InternalReviewerTotal) * 100), 2) < 1)
                            {
                                percentge = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalReviewercomplited) / InternalReviewerTotal) * 100), 2));
                            }
                            else
                            {
                                percentge = Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalReviewercomplited) / InternalReviewerTotal) * 100));
                            }
                        }
                        display.Name = "Location";
                        display.LocationName = item.Name;
                        display.Percentage = Convert.ToInt32(percentge);
                        str += " ['" + display.Name + "', '" + display.LocationName + "', '" + display.Percentage + "'],";
                    }
                }
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript3", "<script language='javascript'> var locationsInternal1 = [" + str.TrimEnd(',') + "];</script>");
            }
        }
        public void GetAllDataPerformerInternal()
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gettotalrecords = GetInternalPerformerAndReviewerProcedure(CustomerId, AuthenticationHelper.UserID, 3).Distinct().ToList();
            if (gettotalrecords != null && gettotalrecords.Count != 0)
            {
                string str = "";
                foreach (var item in gettotalrecords)
                {
                    GraphDisplay display = new GraphDisplay();
                    decimal percentge = 0;
                    List<Tuple<DateTime, DateTime>> financialyearlist = GetInternaldetails(item.CustomerBranchid);
                    if (financialyearlist != null)
                    {
                        foreach (var lst in financialyearlist)
                        {
                            var Total = GetInternalTotalCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 3, lst.Item1, lst.Item2).Count();
                            var complited = GetInternalComplitedCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 3, lst.Item1, lst.Item2).Count();
                            if ((Convert.ToInt32(Total) - Convert.ToInt32(complited)) > 0)
                            {
                                InternalPerformerTotal += Total;
                                InternalPerformercomplited += complited;
                            }
                        }
                        if (InternalPerformerTotal != 0 && InternalPerformercomplited != 0)
                        {
                            if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalPerformercomplited) / InternalPerformerTotal) * 100), 2) < 1)
                            {
                                percentge = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalPerformercomplited) / InternalPerformerTotal) * 100), 2));
                            }
                            else
                            {
                                percentge = Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalPerformercomplited) / InternalPerformerTotal) * 100));
                            }
                            // percentge = Convert.ToInt32(((Convert.ToDecimal(InternalPerformercomplited)) / Convert.ToDecimal(InternalPerformerTotal)) * 100);
                        }
                        display.Name = "Location";
                        display.LocationName = item.Name;
                        display.Percentage = Convert.ToInt32(percentge);
                        str += " ['" + display.Name + "', '" + display.LocationName + "', '" + display.Percentage + "'],";
                    }
                }
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript2", "<script language='javascript'> var locationsInternal = [" + str.TrimEnd(',') + "];</script>");
            }
        }
        public void GetAllDataReviewerInternal()
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gettotalrecords = GetInternalPerformerAndReviewerProcedure(CustomerId, AuthenticationHelper.UserID, 4);
            if (gettotalrecords != null && gettotalrecords.Count != 0)
            {
                string str = "";
                foreach (var item in gettotalrecords)
                {
                    GraphDisplay display = new GraphDisplay();
                    decimal percentge = 0;
                    List<Tuple<DateTime, DateTime>> financialyearlist = GetInternaldetails(item.CustomerBranchid);
                    if (financialyearlist != null)
                    {
                        foreach (var lst in financialyearlist)
                        {
                            var Total = GetInternalTotalCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 4, lst.Item1, lst.Item2).Count();
                            var complited = GetInternalComplitedCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 4, lst.Item1, lst.Item2).Count();
                            if ((Convert.ToInt32(Total) - Convert.ToInt32(complited)) > 0)
                            {
                                InternalReviewerTotal += Total;
                                InternalReviewercomplited += complited;
                            }
                        }
                        if (InternalReviewerTotal != 0 && InternalReviewercomplited != 0)
                        {
                            // percentge = Convert.ToInt32(((Convert.ToDecimal(InternalReviewercomplited)) / Convert.ToDecimal(InternalReviewerTotal)) * 100);

                            if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalReviewercomplited) / InternalReviewerTotal) * 100), 2) < 1)
                            {
                                percentge = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalReviewercomplited) / InternalReviewerTotal) * 100), 2));
                            }
                            else
                            {
                                percentge = Math.Round(Convert.ToDecimal((Convert.ToDecimal(InternalReviewercomplited) / InternalReviewerTotal) * 100));
                            }
                        }
                        display.Name = "Location";
                        display.LocationName = item.Name;
                        display.Percentage = Convert.ToInt32(percentge);
                        str += " ['" + display.Name + "', '" + display.LocationName + "', '" + display.Percentage + "'],";
                    }
                }
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript3", "<script language='javascript'> var locationsInternal1 = [" + str.TrimEnd(',') + "];</script>");
            }
        }
        #endregion

        protected void setCompliancesCounts()
        {
            try
            {

                if (!string.IsNullOrEmpty(Request.QueryString["Role"]))
                {
                    string Role = Request.QueryString["Role"];
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    #region Statutory
                    entities.Database.CommandTimeout = 180;

                    var MastertransactionsQuery1 = (entities.SP_ComplianceInstanceTransactionCount_Dashboard(AuthenticationHelper.UserID,(int) AuthenticationHelper.CustomerID)
                      .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true)).ToList();

                    MastertransactionsQuery1 = MastertransactionsQuery1.Where(row => (row.IsAvantis == true || row.IsAvantis == null)).ToList();

                    var MastertransactionsQuery = (MastertransactionsQuery1
                      .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.ComplianceType != 1)).ToList();

                    var MasterStatuoryChecklistTransactionsQuery = (MastertransactionsQuery1
                      .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.ComplianceType == 1)).ToList();
                    //entities.Database.CommandTimeout = 180;
                    //var MasterStatuoryChecklistTransactionsQuery = (entities.SP_CheckListInstanceTransactionCount(AuthenticationHelper.UserID)
                    //    .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true)).ToList();

                    var MasterReviewerStatuoryChecklistTransactionsQuery = (MastertransactionsQuery1
                      .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.ComplianceType == 1)).ToList();
                    //entities.Database.CommandTimeout = 180;
                    //var MasterReviewerStatuoryChecklistTransactionsQuery = (entities.Sp_CheckListInstanceTransactionReviewerCount(AuthenticationHelper.UserID)
                    //    .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true)).ToList();

                    #endregion

                    #region Internal
                    entities.Database.CommandTimeout = 180;
                    var MasterInternaltransactionsQuery1 = (
                       entities.SP_InternalComplianceInstanceTransactionCount_DashBoard(AuthenticationHelper.UserID)
                        .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true)).ToList();

                    var MasterInternaltransactionsQuery = (
                        MasterInternaltransactionsQuery1
                         .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.IComplianceType != 1)).ToList();

                    var MasterInternalChecklistTransactionsQuery = (MasterInternaltransactionsQuery1
                       .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true && entry.IComplianceType == 1)).ToList();

                    //entities.Database.CommandTimeout = 180;
                    //var MasterInternalChecklistTransactionsQuery = (entities.Sp_InternalComplianceInstanceCheckListTransactionCount(AuthenticationHelper.UserID)
                    //    .Where(entry => entry.IsActive == true && entry.IsUpcomingNotDeleted == true)).ToList();


                    #endregion

                    #region Comment  by Rahul on 27 FEB 2020
                    //var GetAllDataTotalStatuoryList = GetTotalCountProcedure(-1, Convert.ToInt32(AuthenticationHelper.UserID), -1, DateTime.Now, DateTime.Now, MastertransactionsQuery1).ToList();
                    //var GetAllDataComplitedStatuoryList = GetComplitedCountProcedure(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), -1, DateTime.Now, DateTime.Now, MastertransactionsQuery1).ToList();

                    //var GetAllDataTotalInternalList = GetInternalTotalCountProcedure(-1, Convert.ToInt32(AuthenticationHelper.UserID), -1, DateTime.Now, DateTime.Now, MasterInternaltransactionsQuery1).ToList();
                    //var GetAllDataComplitedInternalList = GetInternalComplitedCountProcedure(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(AuthenticationHelper.UserID), 3, DateTime.Now, DateTime.Now, MasterInternaltransactionsQuery1).ToList();
                    #endregion

                    if (roles.Contains(3) && roles.Contains(4) && roles.Contains(10))
                    {
                        #region Performer
                        //Upcoming  Statutory                                         
                        divupcomingPREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Upcoming   Internal                            
                        divupcomingInternalPREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //Overdue Statutory                                             
                        divOverduePREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerOverDueDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Overdue).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Overdue Internal                            
                        divOverdueInternalPREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Overdue).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //CheckList  Statutory                         
                        divPerformerChecklistPREOcount.InnerText = Convert.ToString(Business.ComplianceManagement.GetMappedComplianceCheckListRahul1(AuthenticationHelper.UserID, MasterStatuoryChecklistTransactionsQuery, true, null).Count);
                        //CheckList  Internal                         
                        divPerformerChecklistInternalPREOcount.InnerText = Convert.ToString(Business.InternalComplianceManagement.SPGetMappedComplianceCheckListInternal(AuthenticationHelper.UserID, MasterInternalChecklistTransactionsQuery, true, null).Count);

                        //Rejected Statutory                        
                        divPerformerRejectedPREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Rejected Internal
                        divPerformerRejectedInternalPREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //Pending For Review Statutory                            
                        divPerformerPendingforReviewPREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Pending For Review Internal                           
                        divPerformerPendingforRevieweInternalPREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceStatusID != -1).Count());
                        #endregion

                        #region Reviewer
                        //Due But Not Submitted  Statutory                           
                        divDueButNotSubmittedPREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Due But Not Submitted  Internal                          
                        divDueButNotSubmittedInternalPREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        // Pending For Review Statutory                                            
                        divReviewerPendingforReviewePREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        // Pending For Review Internal                          
                        divReviewerPendingforRevieweInternalPREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        //CheckList Statutory                    
                        divReviewerChecklistPREOcount.InnerText = Convert.ToString(Business.ComplianceManagement.SPGetMappedComplianceCheckListReviewer(AuthenticationHelper.UserID, MasterReviewerStatuoryChecklistTransactionsQuery, false, null).Count);
                        //CheckList Internal
                        divReviewerChecklistInternalPREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerInternalChecklistDisplayCount(AuthenticationHelper.UserID, MasterInternalChecklistTransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        //Rejected Statutory                        
                        divReviewerRejectedPREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Rejected Internal
                        divReviewerRejectedInternalPREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
                        #endregion

                        #region Event Owner
                        //Assigned Event
                        divAssignedEventPREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataAssignedEventCount(AuthenticationHelper.UserID).Count);
                        //Activated Event
                        divActivatedEventPREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataActiveEventCount(AuthenticationHelper.UserID).Count);
                        //Closed Event
                        divClosedEventPREOcount.InnerText = Convert.ToString(EventManagement.GetClosedEvents(AuthenticationHelper.UserID, -1).Count);
                        #endregion

                        #region Comment  by Rahul on 27 FEB 2020
                        //GetAllDataPerformer(GetAllDataTotalStatuoryList, GetAllDataComplitedStatuoryList);
                        //GetAllDataReviewer(GetAllDataTotalStatuoryList, GetAllDataComplitedStatuoryList);
                        //if (checkInternalapplicable.Equals(1))
                        //{
                        //    GetAllDataPerformerInternal(GetAllDataTotalInternalList, GetAllDataComplitedInternalList);
                        //    GetAllDataReviewerInternal(GetAllDataTotalInternalList, GetAllDataComplitedInternalList);
                        //}
                        #endregion
                    }
                    else if (roles.Contains(3) && roles.Contains(4))
                    {

                        #region Performer
                        //Upcoming  Statutory                                             
                        divupcomingPRcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Upcoming   Internal                            
                        divupcomingInternalPRcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //Overdue Statutory                                             
                        divOverduePRcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerOverDueDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Overdue).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Overdue Internal                            
                        divOverdueInternalPRcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Overdue).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //CheckList  Statutory                         
                        divPerformerChecklistPRcount.InnerText = Convert.ToString(Business.ComplianceManagement.GetMappedComplianceCheckListRahul1(AuthenticationHelper.UserID, MasterStatuoryChecklistTransactionsQuery, true, null).Count);
                        //CheckList  Internal                         
                        divPerformerChecklistInternalPRcount.InnerText = Convert.ToString(Business.InternalComplianceManagement.SPGetMappedComplianceCheckListInternal(AuthenticationHelper.UserID, MasterInternalChecklistTransactionsQuery, true, null).Count);

                        //Rejected Statutory                        
                        divPerformerRejectedPRcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Rejected Internal
                        divPerformerRejectedInternalPRcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //Pending For Review Statutory                            
                        divPerformerPendingforReviewPRcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Pending For Review Internal                           
                        divPerformerPendingforRevieweInternalPRcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceStatusID != -1).Count());
                        #endregion

                        #region Reviewer
                        //Due But Not Submitted  Statutory                           
                        divDueButNotSubmittedPRcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Due But Not Submitted  Internal                          
                        divDueButNotSubmittedInternalPRcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        // Pending For Review Statutory                                            
                        divReviewerPendingforReviewerPRcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        // Pending For Review Internal                          
                        divReviewerPendingforReviewerInternalPRcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        //CheckList Statutory                    
                        divReviewerChecklistPRcount.InnerText = Convert.ToString(Business.ComplianceManagement.SPGetMappedComplianceCheckListReviewer(AuthenticationHelper.UserID, MasterReviewerStatuoryChecklistTransactionsQuery, false, null).Count);
                        //CheckList Internal
                        divReviewerChecklistInternalPRcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerInternalChecklistDisplayCount(AuthenticationHelper.UserID, MasterInternalChecklistTransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        //Rejected Statutory                        
                        divReviewerRejectedPRcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Rejected Internal
                        divReviewerRejectedInternalPRcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        #endregion

                        #region Comment  by Rahul on 27 FEB 2020
                        //GetAllDataPerformer(GetAllDataTotalStatuoryList, GetAllDataComplitedStatuoryList);
                        //GetAllDataReviewer(GetAllDataTotalStatuoryList, GetAllDataComplitedStatuoryList);
                        //if (checkInternalapplicable.Equals(1))
                        //{
                        //    GetAllDataPerformerInternal(GetAllDataTotalInternalList, GetAllDataComplitedInternalList);
                        //    GetAllDataReviewerInternal(GetAllDataTotalInternalList, GetAllDataComplitedInternalList);
                        //}
                        #endregion

                    }
                    else if (roles.Contains(3) && roles.Contains(10))
                    {
                        #region Performer
                        //Upcoming  Statutory                          
                        divupcomingPEOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Upcoming   Internal                            
                        divupcomingInternalPEOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //Overdue Statutory                                             
                        divOverduePEOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerOverDueDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Overdue).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Overdue Internal                            
                        divOverdueInternalPEOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Overdue).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //CheckList  Statutory                         
                        divPerformerChecklistPEOcount.InnerText = Convert.ToString(Business.ComplianceManagement.GetMappedComplianceCheckListRahul1(AuthenticationHelper.UserID, MasterStatuoryChecklistTransactionsQuery, true, null).Count);
                        //CheckList  Internal                         
                        divPerformerChecklistInternalPEOcount.InnerText = Convert.ToString(Business.InternalComplianceManagement.SPGetMappedComplianceCheckListInternal(AuthenticationHelper.UserID, MasterInternalChecklistTransactionsQuery, true, null).Count);

                        //Rejected Statutory                        
                        divPerformerRejectedPEOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Rejected Internal
                        divPerformerRejectedInternalPEOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //Pending For Review Statutory                            
                        divPerformerPendingforReviewPEOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Pending For Review Internal                           
                        divPerformerPendingforRevieweInternalPEOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceStatusID != -1).Count());
                        #endregion

                        #region Event Owner
                        //Assigned Event
                        divAssignedEventPEOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataAssignedEventCount(AuthenticationHelper.UserID).Count);
                        //Activated Event
                        divActivatedEventPEOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataActiveEventCount(AuthenticationHelper.UserID).Count);
                        //Closed Event
                        divClosedEventPEOcount.InnerText = Convert.ToString(EventManagement.GetClosedEvents(AuthenticationHelper.UserID, -1).Count);
                        #endregion
                        #region Comment  by Rahul on 27 FEB 2020
                        //GetAllDataPerformer(GetAllDataTotalStatuoryList, GetAllDataComplitedStatuoryList);
                        //if (checkInternalapplicable.Equals(1))
                        //{
                        //    GetAllDataPerformerInternal(GetAllDataTotalInternalList, GetAllDataComplitedInternalList);
                        //}
                        #endregion
                    }
                    else if (roles.Contains(4) && roles.Contains(10))
                    {
                        #region Reviewer
                        //Due But Not Submitted  Statutory                           
                        divDueButNotSubmittedREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Due But Not Submitted  Internal                          
                        divDueButNotSubmittedInternalREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        // Pending For Review Statutory                                            
                        divReviewerPendingforReviewerREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        // Pending For Review Internal                          
                        divReviewerPendingforReviewerInternalREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        //CheckList Statutory                    
                        divReviewerChecklistREOcount.InnerText = Convert.ToString(Business.ComplianceManagement.SPGetMappedComplianceCheckListReviewer(AuthenticationHelper.UserID, MasterReviewerStatuoryChecklistTransactionsQuery, false, null).Count);
                        //CheckList Internal
                        divReviewerChecklistInternalREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerInternalChecklistDisplayCount(AuthenticationHelper.UserID, MasterInternalChecklistTransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        //Rejected Statutory                        
                        divReviewerRejectedREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Rejected Internal                    
                        divReviewerRejectedInternalREOcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
                        #endregion

                        #region Event Owner
                        //Assigned Event
                        divAssignedEventREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataAssignedEventCount(AuthenticationHelper.UserID).Count);
                        //Activated Event
                        divActivatedEventREOcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataActiveEventCount(AuthenticationHelper.UserID).Count);
                        //Closed Event
                        divClosedEventREOcount.InnerText = Convert.ToString(EventManagement.GetClosedEvents(AuthenticationHelper.UserID, -1).Count);
                        #endregion
                        #region Comment  by Rahul on 27 FEB 2020
                        //GetAllDataReviewer(GetAllDataTotalStatuoryList, GetAllDataComplitedStatuoryList);
                        //if (checkInternalapplicable.Equals(1))
                        //{
                        //    GetAllDataReviewerInternal(GetAllDataTotalInternalList, GetAllDataComplitedInternalList);
                        //}
                        #endregion
                    }
                    else if (roles.Contains(3))
                    {
                        #region Performer
                        //Upcoming  Statutory                                             
                        divPerformerOnlyupcomingcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Upcoming   Internal                            
                        divPerformerOnlyupcomingInternalcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //Overdue Statutory                                             
                        divPerformerOnlyOverduecount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerOverDueDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Overdue).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Overdue Internal                            
                        divPerformerOnlyOverdueInternalcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Overdue).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //CheckList  Statutory                         
                        divPerformerOnlyChecklistcount.InnerText = Convert.ToString(Business.ComplianceManagement.GetMappedComplianceCheckListRahul1(AuthenticationHelper.UserID, MasterStatuoryChecklistTransactionsQuery, true, null).Count);
                        //CheckList  Internal                         
                        divPerformerOnlyChecklistInternalcount.InnerText = Convert.ToString(Business.InternalComplianceManagement.SPGetMappedComplianceCheckListInternal(AuthenticationHelper.UserID, MasterInternalChecklistTransactionsQuery, true, null).Count);

                        //Rejected Statutory                        
                        divPerformerOnlyRejectedcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Rejected Internal
                        divPerformerOnlyRejectedInternalcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.InternalComplianceStatusID != -1).Count());

                        //Pending For Review Statutory                            
                        divPerformerOnlyPendingforReviewcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Pending For Review Internal                           
                        divPerformerOnlyPendingforRevieweInternalcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceStatusID != -1).Count());
                        #endregion
                        #region Comment  by Rahul on 27 FEB 2020
                        //GetAllDataPerformer(GetAllDataTotalStatuoryList, GetAllDataComplitedStatuoryList);
                        //if (checkInternalapplicable.Equals(1))
                        //{
                        //    GetAllDataPerformerInternal(GetAllDataTotalInternalList, GetAllDataComplitedInternalList);
                        //}
                        #endregion
                    }
                    else if (roles.Contains(4))
                    {
                        #region Reviewer
                        //Due But Not Submitted  Statutory                           
                        divRevieweronlyDueButNotSubmittedcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Due But Not Submitted  Internal  

                        divRevieweronlyDueButNotSubmittedInternalcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
                        // Pending For Review Statutory                                            
                        divRevieweronlyReviewerPendingforReviewercount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        // Pending For Review Internal    

                        divRevieweronlyReviewerPendingforReviewerInternalcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
                        //CheckList Statutory                    
                        divRevieweronlyChecklistcount.InnerText = Convert.ToString(Business.ComplianceManagement.SPGetMappedComplianceCheckListReviewer(AuthenticationHelper.UserID, MasterReviewerStatuoryChecklistTransactionsQuery, false, null).Count);
                        //CheckList Internal
                        divRevieweronlyChecklistInternalcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerInternalChecklistDisplayCount(AuthenticationHelper.UserID, MasterInternalChecklistTransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        //Rejected Statutory                        
                        divRevieweronlyRejectedcount.InnerText = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MastertransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //Rejected Internal                       
                        divRevieweronlyRejectedInternalcount.InnerText = Convert.ToString(InternalDashboardManagement.SPDashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Rejected).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
                        #endregion
                        #region Comment  by Rahul on 27 FEB 2020
                        //GetAllDataReviewer(GetAllDataTotalStatuoryList, GetAllDataComplitedStatuoryList);
                        //if (checkInternalapplicable.Equals(1))
                        //{
                        //    GetAllDataReviewerInternal(GetAllDataTotalInternalList, GetAllDataComplitedInternalList);
                        //}
                        #endregion
                    }
                    else if (roles.Contains(10))
                    {
                        #region Event Owner
                        //Assigned Event
                        divEventOwnerAssignedEventOnlycount.InnerText = Convert.ToString(DashboardManagement.DashboardDataAssignedEventCount(AuthenticationHelper.UserID).Count);

                        //Activated Event
                        divEventOwnerActivatedEventOnlycount.InnerText = Convert.ToString(DashboardManagement.DashboardDataActiveEventCount(AuthenticationHelper.UserID).Count);

                        //Closed Event
                        divClosedEventOnlycount.InnerText = Convert.ToString(EventManagement.GetClosedEvents(AuthenticationHelper.UserID, -1).Count);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }
        #region Task

        public decimal GetTaskPercentage(string Values, string Roles)
        {
            decimal TPUS = 0;
            try
            {
                if (Values == "TPUS" && Roles == "PREO")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TPUI" && Roles == "PREO")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TPOS" && Roles == "PREO")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TPOI" && Roles == "PREO")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TPRS" && Roles == "PREO")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TPRI" && Roles == "PREO")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TPPS" && Roles == "PREO")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TPPI" && Roles == "PREO")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TRDS" && Roles == "Reviewer")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TRDI" && Roles == "Reviewer")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TRPS" && Roles == "Reviewer")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TRPI" && Roles == "Reviewer")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TRRS" && Roles == "Reviewer")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                if (Values == "TRRI" && Roles == "Reviewer")
                {
                    TPUS = Convert.ToInt32((Convert.ToDecimal(divupcomingPREOcount.InnerText) / (PerformerTotal + Convert.ToDecimal(divupcomingPREOcount.InnerText))) * 100);
                }
                return TPUS;
            }
            catch (Exception ex)
            {
                return 0;
                throw;
            }
        }

        protected void setTaskCounts()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var MasterQUery = (entities.SP_TaskInstanceTransactionStatutoryView_Dashboard(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID)).ToList();

                    string Statutory = "Statutory";
                    string Internal = "Internal";
                    if (TaskRoles.Contains(3) && TaskRoles.Contains(4))
                    {
                        //Performer  
                        //Upcoming

                        DivPerformerUpcomingStatBoth.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 1, CannedReportFilterForPerformer.Upcoming, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivPerformerUpcomingIntBoth.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 2, CannedReportFilterForPerformer.Upcoming, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        //Overdue
                        DivPerformerOverdueStatBoth.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 1, CannedReportFilterForPerformer.Overdue, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivPerformerOverdueIntBoth.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 2, CannedReportFilterForPerformer.Overdue, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        //Performer Rejected
                        DivPerformerRejectedStatBoth.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 1, CannedReportFilterForPerformer.Rejected, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivPerformerRejectedIntBoth.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 2, CannedReportFilterForPerformer.Rejected, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        //Performer Pending  For Review
                        DivPerformerPendingReviewStatBoth.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 1, CannedReportFilterForPerformer.PendingForReview, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivPerformerPendingReviewIntBoth.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 2, CannedReportFilterForPerformer.PendingForReview, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());



                        //Reviewer
                        //Due But Not Submitted     
                        DivReviewerDueNotSubStatBoth.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue, 1, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivReviewerDueNotSubIntBoth.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue, 2, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        // Reviewer Pending  For Review
                        DivReviewerPendingReviewStatBoth.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview, 1, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivReviewerPendingReviewIntBoth.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview, 2, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        //Reviewer Rejected
                        DivReviewerRejectedStatBoth.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.Rejected, 1, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivReviewerRejectedIntBoth.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.Rejected, 2, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());

                    }
                    else if (TaskRoles.Contains(3))
                    {
                        //Performer  
                        ////Upcoming
                        DivPerformerUpcomingStat.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 1, CannedReportFilterForPerformer.Upcoming, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivPerformerUpcomingInt.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 2, CannedReportFilterForPerformer.Upcoming, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        //Overdue
                        DivPerformerOverdueStat.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 1, CannedReportFilterForPerformer.Overdue, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivPerformerOverdueInt.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 2, CannedReportFilterForPerformer.Overdue, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        //Performer Rejected
                        DivPerformerRejectedStat.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 1, CannedReportFilterForPerformer.Rejected, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivPerformerRejectedInt.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 2, CannedReportFilterForPerformer.Rejected, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        //Performer Pending  For Review
                        DivPerformerPendingReviewStat.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 1, CannedReportFilterForPerformer.PendingForReview, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivPerformerPendingReviewInt.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForPerformerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, 2, CannedReportFilterForPerformer.PendingForReview, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());

                    }
                    else if (TaskRoles.Contains(4))
                    {
                        //Reviewer
                        //Due But Not Submitted     
                        DivReviewerDueNotSubStat.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue, 1, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivReviewerDueNotSubInt.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue, 2, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        // Reviewer Pending  For Review
                        DivReviewerPendingReviewStat.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview, 1, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivReviewerPendingReviewInt.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview, 2, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        //Reviewer Rejected
                        DivReviewerRejectedStat.InnerText = Convert.ToString(TaskManagment.StatutoryTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.Rejected, 1, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());
                        DivReviewerRejectedInt.InnerText = Convert.ToString(TaskManagment.InternalTaskDashboardDataForReviewerDisplayCount(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, CannedReportFilterForPerformer.Rejected, 2, MasterQUery).Where(entry => entry.TaskInstanceID != -1).Count());


                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }
        #endregion
        public void GetAllDataPerformer(List<Sp_GetTotalCount_Result> gettotalcount, List<Sp_GetComplitedCount_Result> getComplitedcount)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gettotalrecords = GetPerformerAndReviewerProcedure(CustomerId, AuthenticationHelper.UserID, 3);
            if (gettotalrecords != null && gettotalrecords.Count != 0)
            {
                string str = "";
                foreach (var item in gettotalrecords)
                {
                    GraphDisplay display = new GraphDisplay();
                    decimal percentge = 0;
                    List<Tuple<DateTime, DateTime>> financialyearlist = GetSatutorydetails(item.CustomerBranchid);
                    if (financialyearlist != null)
                    {
                        foreach (var lst in financialyearlist)
                        {
                            // var Total = GetTotalCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 3, lst.Item1, lst.Item2).Count();
                            //var complited = GetComplitedCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 3, lst.Item1, lst.Item2).Count();
                            if (gettotalcount.Count > 0)
                            {
                                var Total = gettotalcount.Where(entry => entry.CustomerBranchid == item.CustomerBranchid
                                       && entry.RoleID == 3 && (entry.ScheduleOn >= lst.Item1 && entry.ScheduleOn <= lst.Item2)).ToList().Count();

                                if (getComplitedcount.Count > 0)
                                {
                                    var complited = getComplitedcount.Where(entry => entry.CustomerBranchid == item.CustomerBranchid
                                           && entry.RoleID == 3 && (entry.ScheduleOn >= lst.Item1 && entry.ScheduleOn <= lst.Item2)).ToList().Count();

                                    if ((Convert.ToInt32(Total) - Convert.ToInt32(complited)) > 0)
                                    {
                                        PerformerTotal += Total;
                                        Performercomplited += complited;
                                    }
                                }//complited count end
                            }//Total count end
                        }
                        if (PerformerTotal != 0 && Performercomplited != 0)
                        {
                            if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(Performercomplited) / PerformerTotal) * 100), 2) < 1)
                            {
                                percentge = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(Performercomplited) / PerformerTotal) * 100), 2));
                            }
                            else
                            {
                                percentge = Math.Round(Convert.ToDecimal((Convert.ToDecimal(Performercomplited) / PerformerTotal) * 100));
                            }
                            //percentge = Convert.ToInt32(((Convert.ToDecimal(Performercomplited)) / Convert.ToDecimal(PerformerTotal)) * 100);
                        }
                        display.Name = "Location";
                        display.LocationName = item.Name;
                        display.Percentage = Convert.ToInt32(percentge);
                        str += " ['" + display.Name + "', '" + display.LocationName + "', '" + display.Percentage + "'],";
                    }
                }
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript", "<script language='javascript'> var locations = [" + str.TrimEnd(',') + "];</script>");
            }
        }
        public void GetAllDataReviewer(List<Sp_GetTotalCount_Result> gettotalcount, List<Sp_GetComplitedCount_Result> getComplitedcount)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gettotalrecords = GetPerformerAndReviewerProcedure(CustomerId, AuthenticationHelper.UserID, 4);
            if (gettotalrecords != null && gettotalrecords.Count != 0)
            {
                string str = "";
                foreach (var item in gettotalrecords)
                {
                    GraphDisplay display = new GraphDisplay();
                    decimal percentge = 0;
                    List<Tuple<DateTime, DateTime>> financialyearlist = GetSatutorydetails(item.CustomerBranchid);
                    if (financialyearlist != null)
                    {
                        foreach (var lst in financialyearlist)
                        {
                            //var Total = GetTotalCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 4, lst.Item1, lst.Item2).Count();
                            // var complited = GetComplitedCountProcedure(item.CustomerBranchid, Convert.ToInt32(AuthenticationHelper.UserID), 4, lst.Item1, lst.Item2).Count();
                            if (gettotalcount.Count > 0)
                            {
                                var Total = gettotalcount.Where(entry => entry.CustomerBranchid == item.CustomerBranchid
                                      && entry.RoleID == 4 && (entry.ScheduleOn >= lst.Item1 && entry.ScheduleOn <= lst.Item2)).ToList().Count();
                                if (getComplitedcount.Count > 0)
                                {
                                    var complited = getComplitedcount.Where(entry => entry.CustomerBranchid == item.CustomerBranchid
                                                && entry.RoleID == 4 && (entry.ScheduleOn >= lst.Item1 && entry.ScheduleOn <= lst.Item2)).ToList().Count();

                                    if ((Convert.ToInt32(Total) - Convert.ToInt32(complited)) > 0)
                                    {
                                        ReviewerTotal += Total;
                                        Reviewercomplited += complited;
                                    }
                                }//Complited count end
                            }//Total count end
                        }
                        if (ReviewerTotal != 0 && Reviewercomplited != 0)
                        {
                            //  percentge = Convert.ToInt32(((Convert.ToDecimal(ReviewerTotal) - Convert.ToDecimal(Reviewercomplited)) / Convert.ToDecimal(ReviewerTotal)) * 100);

                            if (Math.Round(Convert.ToDecimal((Convert.ToDecimal(Reviewercomplited) / ReviewerTotal) * 100), 2) < 1)
                            {
                                percentge = Math.Ceiling(Math.Round(Convert.ToDecimal((Convert.ToDecimal(Reviewercomplited) / ReviewerTotal) * 100), 2));
                            }
                            else
                            {
                                percentge = Math.Round(Convert.ToDecimal((Convert.ToDecimal(Reviewercomplited) / ReviewerTotal) * 100));
                            }
                        }
                        display.Name = "Location";
                        display.LocationName = item.Name;
                        display.Percentage = Convert.ToInt32(percentge);
                        str += " ['" + display.Name + "', '" + display.LocationName + "', '" + display.Percentage + "'],";
                    }
                }
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScript1", "<script language='javascript'> var locations1 = [" + str.TrimEnd(',') + "];</script>");
            }
        }

        protected void ddlRiskType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        protected void btnCreateICS_Click(object sender, EventArgs e)
        {
            DataTable statutorydatatable = new DataTable();
            DataTable Internaldatatable = new DataTable();
            DataTable mgmtstatutorydatatable = new DataTable();
            DataTable mgmtInternaldatatable = new DataTable();

            string finalitem = string.Empty;
            string CalendarItem = string.Empty;
            string FileName = "CalendarItem";
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userid = AuthenticationHelper.UserID;

                var masterbranchlist = (from row in entities.CustomerBranches
                                        where row.CustomerID == customerid
                                        select row).ToList();

                #region Statutory
                entities.Database.CommandTimeout = 180;
                var QueryStatutory = (from row in entities.ComplianceInstanceTransactionView_UserWise(customerid, userid, "All")
                                      select row).ToList();
                if (QueryStatutory.Count > 0)
                {
                    QueryStatutory = QueryStatutory.Where(entry => entry.ComplianceStatusID == 1).ToList();
                    //QueryStatutory = QueryStatutory.Where(entry => entry.RoleID == 3 && entry.PerformerScheduledOn <= DateTime.Today).ToList();                   
                    QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (QueryStatutory.Count > 0)
                {
                    var Statutory = (from row in QueryStatutory
                                     select new
                                     {
                                         ScheduledOn = row.PerformerScheduledOn,
                                         ComplianceStatusID = row.ComplianceStatusID,
                                         RoleID = row.RoleID,
                                         ScheduledOnID = row.ScheduledOnID,
                                         CustomerBranchID = row.CustomerBranchID,
                                         ShortDescription = row.ShortDescription,
                                     }).Distinct().ToList();


                    statutorydatatable = Statutory.ToDataTable();
                }
                #endregion

                #region Internal                                                        

                entities.Database.CommandTimeout = 180;
                var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionView_UserWise(customerid, userid, "All")
                                     select row).ToList();

                if (QueryInternal.Count > 0)
                {
                    QueryInternal = QueryInternal.Where(entry => entry.InternalComplianceStatusID == 1).ToList();
                    //QueryInternal = QueryInternal.Where(entry => entry.RoleID == 3 && entry.InternalScheduledOn <= DateTime.Today).ToList();                                        
                    QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (QueryInternal.Count > 0)
                {
                    var Internal = (from row in QueryInternal
                                    select new
                                    {
                                        ScheduledOn = row.InternalScheduledOn,
                                        ComplianceStatusID = row.InternalComplianceStatusID,
                                        RoleID = row.RoleID,
                                        ScheduledOnID = row.InternalScheduledOnID,
                                        CustomerBranchID = row.CustomerBranchID,
                                        ShortDescription = row.ShortDescription,
                                    }).Distinct().ToList();

                    Internaldatatable = Internal.ToDataTable();
                }
                #endregion
                DataTable newtable = new DataTable();
                // Performer or reviewer
                if (statutorydatatable.Rows.Count > 0)
                {
                    newtable.Merge(statutorydatatable);
                }
                if (Internaldatatable.Rows.Count > 0)
                {
                    newtable.Merge(Internaldatatable);
                }
                var listEmployee = newtable;
                if (listEmployee.Rows.Count > 0)
                {
                    string removableChars = Regex.Escape(@"@&'()<>#,");
                    string pattern = "[" + removableChars + "]";


                    //create a new stringbuilder instance
                    StringBuilder sb = new StringBuilder();
                    //start the calendar item
                    sb.AppendLine("BEGIN:VCALENDAR");
                    sb.AppendLine("VERSION:2.0");
                    sb.AppendLine("PRODID:avantis.co.in");
                    sb.AppendLine("CALSCALE:GREGORIAN");
                    sb.AppendLine("METHOD:PUBLISH");
                    foreach (DataRow item in listEmployee.Rows)
                    {
                        var branchname = masterbranchlist.Where(entry => entry.ID == Convert.ToInt32(item["CustomerBranchID"])).Select(en => en.Name).FirstOrDefault();
                        DateTime DateStart = Convert.ToDateTime(item["ScheduledOn"]);
                        DateTime DateEnd = DateStart.AddDays(1);
                        string Summary = Regex.Replace(item["ShortDescription"].ToString(), pattern, "");//item["ShortDescription"].ToString();// item.Item1;
                        //string Location = branchname.Replace(',', ' ').Trim();//item.Item3;
                        string Location = Regex.Replace(branchname.ToString(), pattern, "");
                        string Description = Regex.Replace(item["ShortDescription"].ToString(), pattern, "");//item["ShortDescription"].ToString().Replace(',', ' ').Trim();// item.Item1;

                        //create a new stringbuilder instance
                        StringBuilder sbevent = new StringBuilder();
                        //add the event
                        sbevent.AppendLine("BEGIN:VEVENT");
                        sbevent.AppendLine("DTSTART:" + DateStart.ToString("yyyyMMddTHHmm00"));
                        sbevent.AppendLine("DTEND:" + DateEnd.ToString("yyyyMMddTHHmm00"));
                        sbevent.AppendLine("SUMMARY:" + Summary + "");
                        sbevent.AppendLine("LOCATION:" + Location + "");
                        sbevent.AppendLine("DESCRIPTION:" + Description + "");
                        sbevent.AppendLine("PRIORITY:3");
                        sbevent.AppendLine("END:VEVENT");

                        //create a string from the stringbuilder
                        CalendarItem += sbevent.ToString();
                    }

                    var end = "END:VCALENDAR";
                    finalitem = sb.ToString() + CalendarItem + end;

                    Response.Buffer = true;
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "text/calendar";
                    Response.AddHeader("content-length", finalitem.Length.ToString());
                    Response.AddHeader("content-disposition", "attachment; filename=\"" + FileName + ".ics\"");
                    Response.Write(finalitem);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                }
            }
        }

        protected void lnkShowTLDashboard_Click(object sender, EventArgs e)
        {
            try
            {
                var userRecord = RLCSManagement.GetRLCSUserRecord(Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.ProfileID);

                if (userRecord != null)
                {
                    if (userRecord.AVACOM_UserRole != null)
                    {
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                        ProductMappingStructure _obj = new ProductMappingStructure();

                        //customerID --- User's CustomerID not Selected CustomerID
                        _obj.ReAuthenticate_User(customerID, userRecord.AVACOM_UserRole);
                        Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}