﻿using iTextSharp.text;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public class PDFExportHelper
    {
        public static Phrase FormatPageHeaderPhrase(string value)
        {
            return new Phrase(value, FontFactory.GetFont("Tahoma", 10, Font.BOLD, BaseColor.BLACK));
        }

        public static Phrase FormatHeaderPhrase(string value)
        {
            return new Phrase(value, FontFactory.GetFont("Tahoma", 8, Font.BOLD, BaseColor.BLACK));
        }

        public static Phrase FormatPhrase(string value)
        {
            return new Phrase(value, FontFactory.GetFont("Tahoma", 8));
        }
    }
}