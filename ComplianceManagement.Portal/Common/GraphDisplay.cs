﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public class GraphDisplay
    {
        public string Name { get; set; }
        public string LocationName { get; set; }
        public long Percentage { get; set; }
       
    }
}