﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Logger;
using System.Reflection;
using Logger;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public class LoggerMessage
    {
        public static void InsertLog(Exception ex, string ClassName, string FunctionName)
        {
            try
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();            
                msg.ClassName = ClassName;               
                msg.FunctionName = FunctionName;
                msg.CreatedOn = DateTime.Now;
                if (ex != null)
                {
                    msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                    msg.StackTrace = ex.StackTrace;
                }
                else
                {
                    msg.Message = ClassName;
                    msg.StackTrace = ClassName;
                }                             
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                //com.VirtuosoITech.Logger.Logger.Instance.Insert(msg); 
                if (ex != null)
                {
                    ReminderUserList.Add(new com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage() { LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ex.Message + "----\r\n" + ex.InnerException, StackTrace = ex.StackTrace, CreatedOn = DateTime.Now });
                }
                else
                {
                    ReminderUserList.Add(new com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage() { LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error, ClassName = ClassName, FunctionName = FunctionName, Message = ClassName, StackTrace = ClassName, CreatedOn = DateTime.Now });
                }
                Business.ComplianceManagement.InsertLogToDatabase(ReminderUserList);
            }
            catch (Exception e)
            {
                throw;
                
            }
        }

        public static void InsertErrorMsg_DBLog(string errorMessage, string ClassName, string FunctionName)
        {
            try
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> lstLogMsgs = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();

                com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage msg = new com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage()
                {
                    LogLevel = 1,
                    ClassName = ClassName,
                    FunctionName = FunctionName,
                    Message = errorMessage,
                    CreatedOn = DateTime.Now,
                };

                lstLogMsgs.Add(msg);

                Business.ComplianceManagement.InsertLogToDatabase(lstLogMsgs);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}