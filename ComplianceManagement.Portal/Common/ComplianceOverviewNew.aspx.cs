﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class ComplianceOverviewNew : System.Web.UI.Page
    {
        protected static int CustId;
        protected static int UId;
        protected static int SOnID;
        protected static int compInstanceID;
        protected static string KendoPath;
        protected static string NumberofDays;
        protected static string NumberofMonths;
        static string sampleFormPath1 = "";
        public static string CompDocReviewPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]) && !string.IsNullOrEmpty(Request.QueryString["ComplainceInstatnceID"]))
            {
                if (!IsPostBack)
                {
                    //NumberofDays = "-9,-30,-13,-28,-29,21,29,1,10,-26,8,-7";
                                       
                    //List<string> names = new List<string>();
                    //names.Add("July 2018");
                    //names.Add("July 2018");
                    //names.Add("June 2018");
                    //names.Add("June 2018");
                    //names.Add("June 2018");
                    //names.Add("May 2018");
                    //names.Add("May 2018");
                    //names.Add("April 2018");
                    //names.Add("April 2018");
                    //names.Add("April 2018");
                    //names.Add("March 2018");

                    //NumberofMonths = Convert.ToString(JsonConvert.SerializeObject(names));
                    
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                    int complianceInstanceID = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);

                    BindTransactionDetails(ScheduledOnID, complianceInstanceID);


                    KendoPath = ConfigurationManager.AppSettings["KendoPathApp"];
                    SOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                    compInstanceID = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
                    BindAuditLogs(ScheduledOnID, complianceInstanceID);
                    BindUserDetail();

                    ViewState["complianceInstanceScheduleOnID"] = ScheduledOnID;
                    ViewState["complianceInstanceAuditID"] = complianceInstanceID;

                    //ComplianceDBEntities entities = new ComplianceDBEntities();
                    //var ParentEvent = entities.SP_GetAuditLogDate(ScheduledOnID).ToList();
                    //gvParentGrid.DataSource = ParentEvent;
                    //gvParentGrid.DataBind();

                    licomplianceoverview1.Attributes.Add("class", "active");

                //       $('#licomplianceoverview').css('background-color', '#1fd9e1');
                //$('#licomplianceoverview').css('color', 'white');

                    licomplianceoverview1.Attributes.Add("class", "active");
                    lidocuments1.Attributes.Add("class", "");
                    liAudit1.Attributes.Add("class", "");
                    liUpdates1.Attributes.Add("class", "");
                    liAuditlog1.Attributes.Add("class", "");

                    complianceoverview.Attributes.Remove("class");
                    complianceoverview.Attributes.Add("class", "tab-pane active");
                    documents.Attributes.Remove("class");
                    documents.Attributes.Add("class", "tab-pane");
                    audits.Attributes.Remove("class");
                    audits.Attributes.Add("class", "tab-pane");

                    updateView.Attributes.Remove("class");
                    updateView.Attributes.Add("class", "tab-pane");

                    auditlogView.Attributes.Remove("class");
                    auditlogView.Attributes.Add("class", "tab-pane");


                    divMgmtRemrks.Visible = false;

                    if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.ComplianceProductType == 3)
                    {
                        divMgmtRemrks.Visible = true;
                    }
                }
            }
        }

        public void BindUserDetail()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var allUsers = UserManagement.GetAllUser(customerID).ToList();

                var users = (from row in allUsers
                             select new
                             {
                                 ID = row.ID,
                                 Name = row.FirstName + " " + row.LastName
                             }).OrderBy(entry => entry.Name).ToList<object>();

                ddlUsers_AuditLog.DataValueField = "ID";
                ddlUsers_AuditLog.DataTextField = "Name";
                ddlUsers_AuditLog.DataSource = users;
                ddlUsers_AuditLog.DataBind();
            }
        }

        public void BindAuditLogs(int ScheduledOnID,int complianceInstanceID)
        {
            try
            {
                long totalRowCount = 0;
                                
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int userID = -1;
                    if (!(string.IsNullOrEmpty(ddlUsers_AuditLog.SelectedValue)))
                    {
                        userID = Convert.ToInt32(ddlUsers_AuditLog.SelectedValue);
                    }
                    string fromDate = string.Empty;
                    string toDate = string.Empty;
                
                if (complianceInstanceID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {                       
                        var lstAuditLogs = entities.SP_GetAuditLogDetailNew(complianceInstanceID).ToList();
                                    
                        if (userID != -1 && userID != 0)
                            lstAuditLogs = lstAuditLogs.Where(row => row.UserId == userID).ToList();

                        if (!(string.IsNullOrEmpty(txtFromDate_AuditLog.Text.Trim())))
                        {
                            try
                            {
                                fromDate = txtFromDate_AuditLog.Text.Trim();
                                lstAuditLogs = lstAuditLogs.Where(row => row.SendDate >= Convert.ToDateTime(fromDate)).ToList();
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }
                        if (!(string.IsNullOrEmpty(txtToDate_AuditLog.Text.Trim())))
                        {
                            try
                            {
                                toDate = txtToDate_AuditLog.Text.Trim();
                                lstAuditLogs = lstAuditLogs.Where(row => row.SendDate <= Convert.ToDateTime(toDate)).ToList();
                            }
                            catch (Exception)
                            {

                            }
                        }
                        divAuditLog_Vertical.InnerHtml = ShowAuditLog_Centered_Clickable(lstAuditLogs);
                    }
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string ShowAuditLog_Centered_Clickable(List<SP_GetAuditLogDetailNew_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    var minDate = lstAuditLogs.Select(row => row.SendDate).Min();                  
                    var maxDate = lstAuditLogs.Select(row => row.SendDate).Max();

                    if (minDate != null && maxDate != null)
                    {
                        strTimeLineHTML.Append("<ul class=\"timeline\">");

                        int accordionCount = 0;
                        var startDate = maxDate;
                        int flag = 0;
                        do
                        {
                            flag = flag + 1;
                            DateTime MonthStartDate = ContractCommonMethods.GetFirstDayOfMonth(Convert.ToDateTime(startDate));
                            DateTime MonthEndDate = ContractCommonMethods.GetLastDayOfMonth(Convert.ToDateTime(startDate));

                            var auditLogsMonthWise = lstAuditLogs.Where(row => row.SendDate >= MonthStartDate && row.SendDate <= MonthEndDate).OrderByDescending(row => row.SendDate).ToList();

                            if (auditLogsMonthWise.Count > 0)
                            {
                                accordionCount++;

                                string accordionCollapseClass = "panel-collapse collapse ";//change by Ruchi on 12th sep 2018

                                string faIcon = "fa fa-minus";
                                if (accordionCount > 1)
                                {
                                    accordionCollapseClass = "panel-collapse collapse";
                                    faIcon = "fa fa-minus";
                                }

                                if (accordionCount % 2 == 0)
                                    strTimeLineHTML.Append("<li class=\"timeline-inverted\">");
                                else
                                    strTimeLineHTML.Append("<li>");

                                strTimeLineHTML.Append("<div class=\"timeline-badge\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\"" +
                                    "data-toggle=\"tooltip\" data-placement=\"bottom\" ToolTip=\"View Detail\"><i id=\"i" + accordionCount + "\" class=\"" + faIcon + "\"></i></div>");

                                strTimeLineHTML.Append("<div class=\"timeline-panel\">");

                                strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                                    "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                                    "<h2 class=\"panel-title new-products-title\">" +
                                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToDateTime(startDate).Month) + "-" + Convert.ToDateTime(startDate).Year + "</h2>" +
                                    "</a>" +
                                "</div>");

                                strTimeLineHTML.Append("<div id=\"divCollapse" + accordionCount + "\" class=\"panel-body in\" style =\"max-height: 300px; overflow-y: auto; padding: 0px;\">" +
                                                        "<div id=\"collapse" + accordionCount + "\" class=" + accordionCollapseClass + "\">" +
                                                                "<ul class=newProductsList>");

                                auditLogsMonthWise.ForEach(eachAuditLog =>
                                {
                                    strTimeLineHTML.Append("<li>" +
                                         "<div class=\"timeline-body\">" +
                                          "<div class=\"col-md-12 pl0\">" +
                                             //"<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.SendDate + "</p>" +
                                             "<p><i class=\"fa fa-clock-o mlr5\"></i>" + String.Format("{0:dd MMM yyyy} {0:t}", eachAuditLog.SendDate) + "</p>" +
                                            //"<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.SendDate.ToString("dd-MM-yyyy h:mm tt") + "</p>" +
                                            "</div>" +
                                             "<div class=\"col-md-12 pl0\">" +
                                                "<p class=\"timeline-title\">" + eachAuditLog.UserName + "</p>" +
                                             "</div>" +
                                            "<div class=\"col-md-12 pl0\">" +
                                                "<p>" + eachAuditLog.Remarks + "</p>" +
                                            "</div>" +
                                             "<div class=\"col-md-12 pl0\">" +
                                                "<p>" + "" + "</p>" +
                                            "</div>" +
                                         "</div>" +
                                   "</li>");
                                    strTimeLineHTML.Append("<div class=\"clearfix\"></div>");

                                    //<small class=\"text-muted\"></small>
                                });

                                strTimeLineHTML.Append("</ul></div>");
                                strTimeLineHTML.Append("</div>");

                                startDate = MonthStartDate.AddDays(-1);
                            }
                            else
                            {
                                startDate = MonthStartDate.AddDays(-1);
                            }

                        }while (startDate >= minDate);

                        strTimeLineHTML.Append("</ul>");
                    }
                }


                //if (lstAuditLogs.Count > 0)
                //{
                //    //var minDate = lstAuditLogs.Select(row => row.SendDate).Min();
                //    var minDate = lstAuditLogs.Select(row => row.SendDate).Min();
                //    var maxDate = lstAuditLogs.Select(row => row.SendDate).Max();

                //    if (minDate != null && maxDate != null)
                //    {
                //        strTimeLineHTML.Append("<ul class=\"timeline\">");

                //        int accordionCount = 0;
                //        var startDate = maxDate;
                //        int flag = 0;
                //        do
                //        {
                //            flag = flag + 1;
                //            DateTime MonthStartDate = ContractCommonMethods.GetFirstDayOfMonth(Convert.ToDateTime(startDate));
                //            DateTime MonthEndDate = ContractCommonMethods.GetLastDayOfMonth(Convert.ToDateTime(startDate));

                //            var auditLogsMonthWise = lstAuditLogs.Where(row => row.SendDate >= MonthStartDate && row.SendDate <= MonthEndDate).OrderByDescending(row => row.SendDate).ToList();

                //            if (auditLogsMonthWise.Count > 0)
                //            {
                //                accordionCount++;

                //                string accordionCollapseClass = "panel-collapse collapse ";//change by Ruchi on 12th sep 2018

                //                string faIcon = "fa fa-minus";
                //                if (accordionCount > 1)
                //                {
                //                    accordionCollapseClass = "panel-collapse collapse";
                //                    faIcon = "fa fa-minus";
                //                }

                //                if (accordionCount % 2 == 0)
                //                    strTimeLineHTML.Append("<li class=\"timeline-inverted\">");
                //                else
                //                    strTimeLineHTML.Append("<li>");

                //                strTimeLineHTML.Append("<div class=\"timeline-badge\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\"" +
                //                    "data-toggle=\"tooltip\" data-placement=\"bottom\" ToolTip=\"View Detail\"><i id=\"i" + accordionCount + "\" class=\"" + faIcon + "\"></i></div>");

                //                strTimeLineHTML.Append("<div class=\"timeline-panel\">");

                //                strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                //                    "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                //                    "<h2 class=\"panel-title new-products-title\">" +
                //                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToDateTime(startDate).Month) + "-" + Convert.ToDateTime(startDate).Year + "</h2>" +
                //                    "</a>" +
                //                "</div>");

                //                strTimeLineHTML.Append("<div id=\"divCollapse" + accordionCount + "\" class=\"panel-body in\" style =\"max-height: 300px; overflow-y: auto; margin-top: 5px;\">" +
                //                                        "<div id=\"collapse" + accordionCount + "\" class=" + accordionCollapseClass + "\">" +
                //                                                "<ul class=newProductsList>");

                //                auditLogsMonthWise.ForEach(eachAuditLog =>
                //                {
                //                    strTimeLineHTML.Append("<li>" +
                //                         "<div class=\"timeline-body\">" +
                //                          "<div class=\"col-md-12 pl0\">" +
                //                             "<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.SendDate + "</p>" +
                //                            //"<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.SendDate.ToString("dd-MM-yyyy h:mm tt") + "</p>" +
                //                            "</div>" +
                //                             "<div class=\"col-md-12 pl0\">" +
                //                                "<p class=\"timeline-title\">" + eachAuditLog.UserName + "</p>" +
                //                             "</div>" +
                //                            "<div class=\"col-md-12 pl0\">" +
                //                                "<p>" + eachAuditLog.Remarks + "</p>" +
                //                            "</div>" +
                //                             "<div class=\"col-md-12 pl0\">" +
                //                                "<p>" + "" + "</p>" +
                //                            "</div>" +
                //                         "</div>" +
                //                   "</li>");
                //                    strTimeLineHTML.Append("<div class=\"clearfix\"></div>");

                //                    //<small class=\"text-muted\"></small>
                //                });

                //                strTimeLineHTML.Append("</ul></div>");
                //                strTimeLineHTML.Append("</div>");

                //                startDate = MonthStartDate.AddDays(-1);
                //            }

                //        }while(lstAuditLogs.Count > flag)
                //        //while (startDate >= minDate);

                //        strTimeLineHTML.Append("</ul>");
                //    }
                //}

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }


        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                lblPenalty.Text = string.Empty;
                lblRisk.Text = string.Empty;
                var complianceType = Business.ComplianceManagement.GetComplianceType(complianceInstanceID).ComplianceType;
                int customerbranchID = -1;
                if (complianceType == 1)  //CheckList
                {
                    
                    var AllComData = Business.DocumentManagement.GetPeriodBranchLocationChecklist(ScheduledOnID, complianceInstanceID);                    
                    if (AllComData != null)
                    {
                        customerbranchID = AllComData.CustomerBranchID;
                        if (Convert.ToDateTime(AllComData.ScheduledOn) < DateTime.Now)
                        {
                            if (AllComData.Status.ToLower() == "open" || AllComData.Status.ToLower() == "complied but pending review" || AllComData.Status.ToLower() == "complied delayed but pending review" || AllComData.Status.ToLower() == "not complied")
                            {
                                lblStatus.Text = "Overdue";
                                divRiskType.Attributes["style"] = "background-color:red;";
                                StatusColor.Attributes["style"] = "border-left-color:red;";
                            }
                            else if (AllComData.Status.ToLower() == "closed-timely" || AllComData.Status.ToLower() == "closed-delayed" || AllComData.Status.ToLower() == "approved" || AllComData.Status.ToLower() == "revise compliance")
                            {
                                lblStatus.Text = "Completed";
                                divRiskType.Attributes["style"] = "background-color:green;";
                                StatusColor.Attributes["style"] = "border-left-color:green;";
                            }
                        }
                        if (Convert.ToDateTime(AllComData.ScheduledOn) >= DateTime.Now)
                        {
                            if (AllComData.Status.ToLower() == "open" || AllComData.Status.ToLower() == "complied but pending review" || AllComData.Status.ToLower() == "complied delayed but pending review" || AllComData.Status.ToLower() == "not complied")
                            {
                                lblStatus.Text = "Upcoming";
                                divRiskType.Attributes["style"] = "background-color:#ffbf00;";
                                StatusColor.Attributes["style"] = "border-left-color:#ffbf00;";
                            }
                            else if (AllComData.Status.ToLower() == "closed-timely" || AllComData.Status.ToLower() == "closed-delayed" || AllComData.Status.ToLower() == "revise compliance")
                            {
                                lblStatus.Text = "Completed";
                                divRiskType.Attributes["style"] = "background-color:green;";
                                StatusColor.Attributes["style"] = "border-left-color:green;";
                            }
                        }
                        lblLocation.Text = AllComData.Branch;
                        lblDueDate.Text = Convert.ToDateTime(AllComData.ScheduledOn).ToString("dd-MMM-yyyy");
                        lblPeriod.Text = AllComData.ForMonth;
                    }
                }
                else  //Statutory
                {
                    var AllComData = Business.ComplianceManagement.GetPeriodBranchLocation(ScheduledOnID, complianceInstanceID);                    
                    if (AllComData != null)
                    {
                        customerbranchID = AllComData.CustomerBranchID;
                        if (AllComData.Status == "Interim Rejected" || AllComData.Status == "Interim Review Approved" || AllComData.Status == "Submitted For Interim Review")
                        {
                            lblStatus.Text = AllComData.Status.ToLower();
                            divRiskType.Attributes["style"] = "background-color:red;";
                            StatusColor.Attributes["style"] = "border-left-color:red;";
                        }
                        if (Convert.ToDateTime(AllComData.PerformerScheduledOn) < DateTime.Now)
                        {
                            if (AllComData.Status.ToLower() == "open" || AllComData.Status.ToLower() == "complied but pending review" || AllComData.Status.ToLower() == "complied delayed but pending review" || AllComData.Status.ToLower() == "not complied")
                            {
                                lblStatus.Text = "Overdue";
                                divRiskType.Attributes["style"] = "background-color:red;";
                                StatusColor.Attributes["style"] = "border-left-color:red;";
                            }
                            else if (AllComData.Status.ToLower() == "closed-timely" || AllComData.Status.ToLower() == "closed-delayed" || AllComData.Status.ToLower() == "approved" || AllComData.Status.ToLower() == "revise compliance")
                            {
                                lblStatus.Text = "Completed";
                                divRiskType.Attributes["style"] = "background-color:green;";
                                StatusColor.Attributes["style"] = "border-left-color:green;";
                            }
                        }
                        if (Convert.ToDateTime(AllComData.PerformerScheduledOn) >= DateTime.Now)
                        {
                            if (AllComData.Status.ToLower() == "open" || AllComData.Status.ToLower() == "complied but pending review" || AllComData.Status.ToLower() == "complied delayed but pending review" || AllComData.Status.ToLower() == "not complied")
                            {
                                lblStatus.Text = "Upcoming";
                                divRiskType.Attributes["style"] = "background-color:#ffbf00;";
                                StatusColor.Attributes["style"] = "border-left-color:#ffbf00;";
                            }
                            else if (AllComData.Status.ToLower() == "closed-timely" || AllComData.Status.ToLower() == "closed-delayed" || AllComData.Status.ToLower() == "revise compliance")
                            {
                                lblStatus.Text = "Completed";
                                divRiskType.Attributes["style"] = "background-color:green;";
                                StatusColor.Attributes["style"] = "border-left-color:green;";
                            }
                        }
                        lblLocation.Text = AllComData.Branch;
                        lblDueDate.Text = Convert.ToDateTime(AllComData.PerformerScheduledOn).ToString("dd-MMM-yyyy");
                        lblPeriod.Text = AllComData.ForMonth;
                    }
                }                
                ViewState["ScheduledOnID"] = ScheduledOnID;
                var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);                
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var dataChart = entities.Sp_Get_ComplianceHistory(Convert.ToInt32(complianceInfo.ID), Convert.ToInt32(customerbranchID)).ToList();
                    NumberofDays = String.Join(",", dataChart.Select(x => x.DaysDiff).ToList());                    
                    NumberofMonths = Convert.ToString(JsonConvert.SerializeObject(dataChart.Select(x=>x.MonthStatusChangedOn).ToList()));
                }               
                if (complianceInfo != null)
                {
                    lnkSampleForm.Text = Convert.ToString(complianceInfo.SampleFormLink);
                    lblComplianceDiscription.Text = complianceInfo.ShortDescription;
                    lblDetailedDiscription.Text = complianceInfo.Description;
                    lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));
                    lblRefrenceText.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);
                    lblPenalty.Text = complianceInfo.PenaltyDescription;
                    string risk = Business.ComplianceManagement.GetRiskType(complianceInfo, complianceInstanceID);
                    lblRisk.Text = risk;
                    hdnActId.Value =Convert.ToString(complianceInfo.ActID);
                    hdnComplianceId.Value = Convert.ToString(complianceInfo.ID);
                    var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);

                    if (risk == "HIGH")
                    {
                        lblRisk.Attributes["style"] = "color:red;font-weight: bold;";
                        RiskColor.Attributes["style"] = "border-left-color:red;";
                    }
                    else if (risk == "MEDIUM")
                    {
                        lblRisk.Attributes["style"] = "color:#ffbf00;font-weight: bold;";
                        RiskColor.Attributes["style"] = "border-left-color:#ffbf00;";
                    }
                    else if (risk == "LOW")
                    {
                        lblRisk.Attributes["style"] = "color:green;font-weight: bold;";
                        RiskColor.Attributes["style"] = "border-left-color:green;";
                    }

                    if (ActInfo != null)
                    {
                        lblActName.Text = ActInfo.Name;
                    }
                    lblRule.Text = complianceInfo.Sections;
                    lblFormNumber.Text = complianceInfo.RequiredForms;
                }             
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        
        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {

                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }
        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

       

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["ScheduleOnID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                        lblMessage.Text = "";
                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);
                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        //protected void gvParentToComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        Label lblUserName = (Label)e.Row.FindControl("lblUserName");
        //        string UserName = Convert.ToString(lblUserName.Text);

        //        Label lblStatus = (Label)e.Row.FindControl("lblStatus");
        //        string Status = Convert.ToString(lblStatus.Text);

        //        Label lblRemarks = (Label)e.Row.FindControl("lblRemarks");
        //        string Remarks = Convert.ToString(lblRemarks.Text);

        //        Label lblDocumentVersionView = (Label)e.Row.FindControl("lblcustomlabel");
        //        if (Status.ToLower() == "open" && Remarks.ToLower() == "new compliance assigned.")
        //        {
        //            lblDocumentVersionView.Text = "Compliance assinged to " + UserName;
        //        }
        //        if (Status.ToLower() == "not complied" || Status.ToLower() == "rejected")
        //        {
        //            lblDocumentVersionView.Text = "Compliance has been rejected by " + UserName;
        //        }
        //        if (Status.ToLower() == "complied but pending review")
        //        {
        //            if (Remarks != "")
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " in time with remark " + Remarks;
        //            else
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " in time ";
        //        }
        //        if (Status.ToLower() == "complied delayed but pending review")
        //        {
        //            if (Remarks != "")
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date time with remark " + Remarks;
        //            else
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date ";
        //        }
        //        if (Status.ToLower() == "complied delayed but pending review")
        //        {
        //            if (Remarks != "")
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date time with remark " + Remarks;
        //            else
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date ";
        //        }

        //        if (Status.ToLower() == "closed-timely")
        //        {
        //            if (Remarks != "")
        //                lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " in time with remark " + Remarks;
        //            else
        //                lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " in time ";
        //        }

        //        if (Status.ToLower() == "closed-delayed")
        //        {
        //            if (Remarks != "")
        //                lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " after due date with remark" + Remarks;
        //            else
        //                lblDocumentVersionView.Text = "Compliance has been  reviewed and closed by " + UserName + " after due date ";
        //        }

        //        if (Status.ToLower() == "approved")
        //        {
        //            lblDocumentVersionView.Text = "Compliance has been approved and closed by " + UserName;
        //        }

        //        if (Status.ToLower() == "revise compliance")
        //        {
        //            lblDocumentVersionView.Text = "Compliance has been revised by " + UserName;
        //        }
        //        if (Status.ToLower() == "submitted for interim review")
        //        {
        //            lblDocumentVersionView.Text = "Interim compliance has been reviewed by " + UserName;
        //        }

        //        if (Status.ToLower() == "interim review approved")
        //        {
        //            lblDocumentVersionView.Text = "Interim compliance has been submitted for review approved by " + UserName;
        //        }
        //        if (Status.ToLower() == "interim rejected")
        //        {
        //            lblDocumentVersionView.Text = "Interim compliance has been rejected by " + UserName;
        //        }

        //        if (Status.ToLower() == "reminder")
        //        {
        //            if (Remarks.ToLower() == "upcoming")
        //                lblDocumentVersionView.Text = "Upcoming compliance reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "overdue")
        //                lblDocumentVersionView.Text = "Overdue compliance reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "escalation notification")
        //                lblDocumentVersionView.Text = "Escalation notification compliance reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "penalty performer")
        //                lblDocumentVersionView.Text = "Penalty performer reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "penalty reviewer")
        //                lblDocumentVersionView.Text = "Penalty Reviewer reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "pending for review")
        //                lblDocumentVersionView.Text = "Pending for review compliance reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "checklist reminder")
        //                lblDocumentVersionView.Text = "Checklist compliance reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "checklist escalation notification")
        //                lblDocumentVersionView.Text = "Checklist escalation notification reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "upcoming perofrmer monthly")
        //                lblDocumentVersionView.Text = "Upcoming perofrmer monthly reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "upcoming reviewer monthly")
        //                lblDocumentVersionView.Text = "Upcoming reviewer monthly reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "upcoming management")
        //                lblDocumentVersionView.Text = "Upcoming management reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "weekly management")
        //                lblDocumentVersionView.Text = "Weekly management reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "upcoming approver")
        //                lblDocumentVersionView.Text = "Upcoming approver reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "weekly  approver")
        //                lblDocumentVersionView.Text = "Weekly approver reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "approver notification")
        //                lblDocumentVersionView.Text = "Approver notification reminder has been sent " + UserName;
        //        }
        //        if (Status.ToLower() == "managementremark")
        //        {
        //            lblDocumentVersionView.Text = UserName + " has added remark " + Remarks;
        //        }
        //    }
        //}
        //protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            if (!String.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]))
        //            {
        //                if (gvParentGrid.DataKeys[e.Row.RowIndex]["SendDate"] != null)
        //                {
        //                    DateTime? Date = Convert.ToDateTime(gvParentGrid.DataKeys[e.Row.RowIndex]["SendDate"]);

        //                    //DateTime? Date = Convert.ToDateTime(e.Row.Cells[1].Text);
        //                    int? ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);

        //                    //GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
        //                    //string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

        //                    ComplianceDBEntities entities = new ComplianceDBEntities();
        //                    GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
        //                    var compliance = entities.SP_GetAuditLogDetail(ScheduledOnID, Date).ToList();
        //                    gv1.DataSource = compliance;
        //                    gv1.DataBind();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void lnkSampleForm_Click(object sender, EventArgs e)
        {
            try
            {
                if (lnkSampleForm.Text != "")
                {
                    string url = lnkSampleForm.Text;

                    string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
                    lnkSampleForm.Attributes.Add("OnClick", fullURL);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveRemarks_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtManagementRemarks.Text != "")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        ManagementComment cm = new ManagementComment();
                        cm.remarks = txtManagementRemarks.Text;
                        cm.createdBy = AuthenticationHelper.UserID;
                        cm.createdDate = DateTime.UtcNow;
                        cm.ComplainceInstatnceID = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
                        cm.ComplianceScheduleID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                        entities.ManagementComments.Add(cm);
                        entities.SaveChanges();
                        int instanceId = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
                        var compliance= (from row in  entities.ComplianceAssignments
                                         where row.ComplianceInstanceID== instanceId
                                         select row).ToList();

                        //hdnActId.Value = Convert.ToString(complianceInfo.ActID);
                        //hdnComplianceId.Value = Convert.ToString(complianceInfo.ID);
                        string buildstringfornotification = "Management sent you remarks on compliance assigned to you for period " + lblPeriod.Text;
                        buildstringfornotification += "\n Remarks is:" + txtManagementRemarks.Text;
                        Notification ntc = new Notification();
                        ntc.Remark = buildstringfornotification;
                        ntc.UpdatedBy = AuthenticationHelper.UserID;
                        ntc.UpdatedOn = DateTime.UtcNow;
                        ntc.CreatedBy = AuthenticationHelper.UserID;
                        ntc.CreatedOn = DateTime.UtcNow;
                        ntc.Type = "Compliance";
                        ntc.ActID = Convert.ToInt32(hdnActId.Value);
                        ntc.ComplianceID = Convert.ToInt32(hdnComplianceId.Value);

                        entities.Notifications.Add(ntc);
                        entities.SaveChanges();
                      

                        var perf = (from row in compliance where row.RoleID == 3
                                    select row.UserID).FirstOrDefault();

                        var rev = (from row in compliance
                                    where row.RoleID == 4
                                    select row.UserID).FirstOrDefault();

                        try
                        {
                            if (perf != 0)
                            {
                                var User = UserManagement.GetByID(Convert.ToInt32(perf));
                                var MgmtUser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                                string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(AuthenticationHelper.CustomerID)).Name;
                                var ShortDescription = Business.ComplianceManagement.GetCompliance(Convert.ToInt32(hdnComplianceId.Value)).ShortDescription;
                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
                                                    .Replace("@User", User.FirstName + " " + User.LastName)
                                                    .Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
                                                    .Replace("@ShortDescription", ShortDescription)
                                                    .Replace("@Period", lblPeriod.Text)
                                                    .Replace("@Remarks", txtManagementRemarks.Text)
                                                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                    .Replace("@From", ReplyEmailAddressName);

                                new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { User.Email }).ToList(), null, null, "AVACOM Notification for Management Remarks/Comment.", message); }).Start();
                            }

                            if (rev != 0)
                            {
                                var User = UserManagement.GetByID(Convert.ToInt32(rev));
                                var MgmtUser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                                string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(AuthenticationHelper.CustomerID)).Name;
                                var ShortDescription = Business.ComplianceManagement.GetCompliance(Convert.ToInt32(hdnComplianceId.Value)).ShortDescription;
                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
                                                    .Replace("@User", User.FirstName + " " + User.LastName)
                                                    .Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
                                                    .Replace("@ShortDescription", ShortDescription)
                                                    .Replace("@Period", lblPeriod.Text)
                                                    .Replace("@Remarks", txtManagementRemarks.Text)
                                                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                    .Replace("@From", ReplyEmailAddressName);

                                new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { User.Email }).ToList(), null, null, "AVACOM Notification for Management Remarks/Comment.", message); }).Start();
                            }
                        }
                        catch (Exception)
                        {
                        }
                        
                        UserNotification unt = new UserNotification();
                        unt.NotificationID = ntc.ID;
                        unt.UserID = Convert.ToInt32(perf);
                        unt.IsRead = false;
                        entities.UserNotifications.Add(unt);
                        entities.SaveChanges();

                        unt = new UserNotification();
                        unt.NotificationID = ntc.ID;
                        unt.UserID = Convert.ToInt32(rev);
                        unt.IsRead = false;
                        entities.UserNotifications.Add(unt);
                        entities.SaveChanges();

                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "Management Remarks Saved Successfully.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void btnApplyFilter_AuditLog_Click(object sender, EventArgs e)
        {
            try
            {
                BindAuditLogs(Convert.ToInt32(ViewState["complianceInstanceScheduleOnID"]), Convert.ToInt32(ViewState["complianceInstanceAuditID"]));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTypes", "UpdatedrebindTypes();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearFilter_AuditLog_Click(object sender, EventArgs e)
        {
            try
            {
                ddlUsers_AuditLog.ClearSelection();

                txtFromDate_AuditLog.Text = string.Empty;
                txtToDate_AuditLog.Text = string.Empty;

                BindAuditLogs(Convert.ToInt32(ViewState["complianceInstanceScheduleOnID"]), Convert.ToInt32(ViewState["complianceInstanceAuditID"]));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTypes", "UpdatedrebindTypes();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}