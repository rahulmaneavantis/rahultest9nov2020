﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class TaskDownloadDoc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!string.IsNullOrEmpty(Request.QueryString["TaskScheduleOnID"]))
                {
                    string[] commandArg = Request.QueryString["TaskScheduleOnID"].ToString().Split(',');

                    int IsStatutory = Convert.ToInt32(Request.QueryString["IsFlag"]);

                    using (ZipFile ComplianceZip = new ZipFile())
                    {

                        foreach (var gvrow in commandArg)
                        {

                            int ScheduledOnID = Convert.ToInt32(gvrow);
                            if (ScheduledOnID != 0)
                            {
                                if (IsStatutory == -1 || IsStatutory == 1 || IsStatutory == 2)
                                {
                                    var ComplianceData = GetForMonth(ScheduledOnID);
                                    List<GetTaskDocumentView> fileData = TaskManagment.GetTaskRelatedFileData(ScheduledOnID).ToList();// DocumentManagement.GetFileData1(ScheduledOnID);
                                    fileData = fileData.Where(x => x.ISLink == false).ToList();
                                    //craeted subdirectory
                                    //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                    //string directoryName = ComplianceData.TaskDescription + "/" + ComplianceData.ForMonth;

                                    var CustomerBranch = CustomerBranchManagement.GetByID(ComplianceData.CustomerBranchID).Name;
                                    string directoryName = CustomerBranch + "/" + ComplianceData.TaskTitle + "/" + ComplianceData.ForMonth;
                                    int i = 0;
                                    foreach (var file in fileData)
                                    {
                                        string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                        var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                        if (dictionary == null)
                                            ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;

                        byte[] data = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=TaskDocument.zip");
                        Response.BinaryWrite(data);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();

                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static TaskInstanceTransactionView GetForMonth(int ScheduledOnID)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var ScheduleOnData = (from row in entities.TaskInstanceTransactionViews
                                          where row.TaskScheduledOnID == ScheduledOnID
                                          select row).FirstOrDefault();

                    if (ScheduleOnData != null)
                    {
                        return ScheduleOnData;
                    }
                    else
                    {
                        return null;
                    }

                }
            }
       
    }
}