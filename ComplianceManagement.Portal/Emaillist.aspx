﻿ 
<%@ Page Title="Email List" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="Emaillist.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Emaillist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
    <style type="text/css">
        #progressbar .ui-progressbar-value
        {
            background-color: #ccc;
        }
    </style>
    <style>
        .ui-widget-header{border:0px !important; background:inherit;font-size: 20px;color: #666666;font-weight: normal;padding-top: 0px;margin-top: 5px;}
        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color:#666666 !important;
                text-decoration:none !important; 
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
                right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                    position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upUserReminderList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right" class="pagefilter">
                        Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true" />
                    </td>
                    
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdUserReminder" AutoGenerateColumns="false" GridLines="none"
                 CssClass="table" 
                   AllowPaging="True" PageSize="12" Width="100%"  >
                <Columns>
              
                    <asp:TemplateField HeaderText ="Sender" ItemStyle-Width="300px">
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:300px;">
                                <asp:Label  runat="server" Text='<%# Eval("Sendemail") %>' ToolTip='<%# Eval("Sendemail") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
	                </asp:TemplateField>
                       <asp:TemplateField HeaderText ="Subject" SortExpression="Branch">
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:150px;">
                                <asp:Label  runat="server" Text='<%# Eval("Subjecttext") %>' ToolTip='<%# Eval("Subjecttext") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
	                </asp:TemplateField>
               
                   <%-- <asp:TemplateField HeaderText="Sent Date" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn" >
                        <ItemTemplate>
                            <%# ((DateTime)Eval("createddate")).ToString("dd-MMM-yyyy")%>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    
                     <asp:TemplateField HeaderText="View">
                        <ItemTemplate>
                             <a href="maildetails.aspx?id=<%# Eval("id") %>">View</a>
                        </ItemTemplate>
                  
                    </asp:TemplateField>
                </Columns>
                 
                 
                        <RowStyle CssClass="clsROWgrid"   />
                        <HeaderStyle CssClass="clsheadergrid"    />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>
