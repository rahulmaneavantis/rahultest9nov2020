﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows; 
using Google.Apis.Util.Store;
using Newtonsoft.Json; 
using System.IO; 
using System.Net;
using System.Net.Http;
using System.Text; 
using System.Web.Script.Serialization;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Web.Security;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class externalogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Google.Apis.Auth.OAuth2.ClientSecrets client = new Google.Apis.Auth.OAuth2.ClientSecrets();
            //client.ClientId = "551609060837";
            //client.ClientSecret = "rzAZB6MWo-id7lTmiBxXjYXZ";
            //  getgoogleplususerdataSer((string)Request.QueryString["code"]);


            string code = Request.QueryString["code"].ToString();
            string scope = Request.QueryString["scope"].ToString();
            string url = "https://accounts.google.com/o/oauth2/token";
            string postString = "code=" + code + "&client_id=551609060837-4kvqjmskko7kkiqn13eibo2gl8vbkojg.apps.googleusercontent.com&client_secret=rzAZB6MWo-id7lTmiBxXjYXZ&redirect_uri=" + ConfigurationManager.AppSettings["AuthReturnUrl"] + "&grant_type=authorization_code";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url.ToString());
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            UTF8Encoding utfenc = new UTF8Encoding();
            byte[] bytes = utfenc.GetBytes(postString);
            Stream os = null;
            try
            {
                request.ContentLength = bytes.Length;
                os = request.GetRequestStream();
                os.Write(bytes, 0, bytes.Length);
            }
            catch
            { }

            try
            {
                HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
                Stream responseStream = webResponse.GetResponseStream();
                StreamReader responseStreamReader = new StreamReader(responseStream);
                var result = responseStreamReader.ReadToEnd();//
                var json = new JavaScriptSerializer();

                GoogleAuthorizationData authData = json.Deserialize<GoogleAuthorizationData>(result);

                HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create("https://www.googleapis.com/oauth2/v2/userinfo");
                request1.Method = "GET";
                request1.ContentLength = 0;
                request1.Headers.Add("Authorization", string.Format("{0} {1}", authData.token_type, authData.access_token));
                HttpWebResponse webResponse1 = (HttpWebResponse)request1.GetResponse();
                Stream responseStream1 = webResponse1.GetResponseStream();
                StreamReader responseStreamReader1 = new StreamReader(responseStream1);
                GoogleUserInfo userinfo = json.Deserialize<GoogleUserInfo>(responseStreamReader1.ReadToEnd());
                // Response.Write(userinfo.email);
                string ipaddress = string.Empty;
                string Macaddress = string.Empty;
                Macaddress = Util.GetMACAddress();
                ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ipaddress == "" || ipaddress == null)
                    ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                Business.Data.User user = null;
                if (UserManagement.IsValidUser(userinfo.email.Trim(), out user))
                {
                    Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> blockip = null;
                    if (user != null)
                    {
                        try
                        {
                            if (user.CustomerID != null)
                            {
                                blockip = UserManagement.GetBlockIpAddress((int)user.CustomerID, user.ID, ipaddress);
                                if (!blockip.Item1)
                                {
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin.";
                                    return;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }


                        bool Auditorexpired = false;
                        if (user.RoleID == 9)
                        {
                            if (user.Enddate == null)
                            {
                                Auditorexpired = true;
                            }
                            if (DateTime.Now.Date > Convert.ToDateTime(user.Enddate).Date || DateTime.Now < Convert.ToDateTime(user.Startdate).Date)
                            {
                                Auditorexpired = true;
                            }
                        }
                        if (!Auditorexpired)
                        {
                            bool Success = true;
                            //if (!(user.WrongAttempt >= 3))
                            //{
                            if (user.IsActive)
                            {
                                Session["userID"] = user.ID;
                                Session["ContactNo"] = user.ContactNumber;
                                Session["Email"] = user.Email;
                                if (user.RoleID == 9)
                                {
                                    Session["Auditstartdate"] = user.AuditStartPeriod;
                                    Session["Auditenddate"] = user.AuditEndPeriod;
                                }
                                DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                                DateTime currentDate = DateTime.Now;
                                LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;


                                int customerID = 0;
                                customerID = Convert.ToInt32(user.CustomerID);

                                //int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                                Random random = new Random();
                                int value = random.Next(1000000);
                                if (value == 0)
                                {
                                    value = random.Next(1000000);
                                }
                                Session["ResendOTP"] = Convert.ToString(value);
                                long Contact;
                                VerifyOTP OTPData = new VerifyOTP()
                                {
                                    UserId = Convert.ToInt32(user.ID),
                                    EmailId = userinfo.email.Trim(),
                                    OTP = Convert.ToInt32(value),
                                    CreatedOn = DateTime.Now,
                                    IsVerified = false,
                                    IPAddress = ipaddress
                                };
                                VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.                                                   
                                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                try
                                {
                                    if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                    {
                                        //Send Email on User Mail Id.
                                        if (user.CustomerID != 5)
                                        {
                                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Success = false;
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                                bool OTPresult = false;
                                try
                                {
                                    OTPresult = long.TryParse(user.ContactNumber, out Contact);
                                    if (OTPresult)
                                    {
                                        OTPData.MobileNo = Contact;
                                    }
                                    else
                                    {
                                        OTPData.MobileNo = 0;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    OTPData.MobileNo = 0;
                                    OTPresult = false;
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }

                                if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
                                {
                                    try
                                    {
                                        if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                        {
                                            //Send SMS on User Mobile No.
                                            SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.");
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Success = false;
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                MaintainLoginDetail objdata = new MaintainLoginDetail()
                                {
                                    UserId = Convert.ToInt32(user.ID),
                                    Email = user.Email,
                                    CreatedOn = DateTime.Now,
                                    IPAddress = ipaddress,
                                    MACAddress = Macaddress,
                                    LoginFrom = "WC",
                                    //ProfileID = user.ID
                                };
                                UserManagement.Create(objdata);
                                //    ProcessAuthenticationInformation(user);
                                Session["EA"] = Util.CalculateAESHash(user.Email);
                                Session["MA"] = Util.CalculateAESHash(Macaddress.Trim());
                                Response.Redirect("~/Users/OTPVerify.aspx", false);
                                Session["CustomerID_new"] = user.CustomerID;
                            }
                            else
                            {
                                cvLogin.IsValid = false;
                                message.InnerText = "Your Account is Disabled.";
                            }
                            //}
                            //else
                            //{

                            //    Session["otpvrifyWattp"] = true;
                            //    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknew('Login', 'LoginUnsucessful', 'Page-Login', '0');", true);
                            //}
                        }
                        else
                        {
                            cvLogin.IsValid = false;
                            message.InnerText = "Your Account is Disabled.";
                        }
                    }
                }
                else
                {

                    cvLogin.IsValid = false;
                    message.InnerText = "You are not authorised to access portal, contact your administrator";
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknew('Login', 'LoginUnsucessful', 'Page-Login', '0');", true);
                }

            }
            catch (Exception eX)
            {
                throw eX;
            }
        }
        private void ProcessAuthenticationInformation(Business.Data.User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID(user.RoleID).Code;
                mst_User mstuser = UserManagementRisk.GetByID(Convert.ToInt32(user.ID));

                string mstRole = string.Empty;
                if (mstuser != null)
                {
                    mstRole = RoleManagementRisk.GetByID(mstuser.RoleID).Code;
                }
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                int ServiceproviderID = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                }
                else
                {
                    Customer c = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                    var IsInternalComplianceApplicable = c.IComplianceApplicable;
                    if (IsInternalComplianceApplicable != -1)
                    {
                        checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    }
                    var IsTaskApplicable = c.TaskApplicable;
                    if (IsTaskApplicable != -1)
                    {
                        checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    }
                    var IsVerticlApplicable = c.VerticalApplicable;
                    if (IsVerticlApplicable != null)
                    {
                        if (IsVerticlApplicable != -1)
                        {
                            checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                        }
                    }
                    if (c.IsPayment != null)
                    {
                        IsPaymentCustomer = Convert.ToBoolean(c.IsPayment);
                    }

                    var IsLabelApplicable = c.IsLabelApplicable;
                    if (IsLabelApplicable != -1)
                    {
                        checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    }

                    if (c.ComplianceProductType != null)
                    {
                        complianceProdType = Convert.ToInt32(c.ComplianceProductType);
                    }
                }

                Business.Data.User userToUpdate = new Business.Data.User();
                userToUpdate.ID = user.ID;
                userToUpdate.LastLoginTime = DateTime.UtcNow;
                userToUpdate.WrongAttempt = 0;
                UserManagement.Update(userToUpdate);
                ServiceproviderID = UserManagementRisk.GetServiceproviderID((int)user.ID);
                if (role.Equals("SADMN"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Users/UserSummary.aspx", false);
                }
                else if (role.Equals("IMPT"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
                else if (role.Equals("UPDT"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/LegalUpdateAdmin.aspx", false);
                }
                else if (role.Equals("RPER"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/ResearchPerformerAdmin.aspx", false);
                }
                else if (role.Equals("RREV"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/ResearchReviewerAdmin.aspx", false);
                }
                else if (role.Equals("HVADM"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else
                {
                    ProductMappingStructure _obj = new ProductMappingStructure();
                    var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(user.CustomerID));
                    if (ProductMappingDetails.Count == 1)
                    {
                        if (ProductMappingDetails.Contains(1))
                        {
                            _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(2))
                        {
                            _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(3))
                        {
                            _obj.FormsAuthenticationRedirect_IFC(mstuser, mstRole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(4))
                        {
                            _obj.FormsAuthenticationRedirect_ARS(mstuser, mstRole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(5))
                        {
                            _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(6))
                        {
                            _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(7))
                        {
                            _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                    }
                    else if (ProductMappingDetails.Count > 1)
                    {
                        if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(2) && user.LitigationRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(mstuser, mstRole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(mstuser, mstRole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(5) && user.ContractRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(mstuser, mstRole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(mstuser, mstRole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(6) && user.LicenseRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(mstuser, mstRole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(mstuser, mstRole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(7) && user.VendorRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(mstuser, mstRole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(mstuser, mstRole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                            Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                        }
                    }
                    else
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType, ServiceproviderID), false);
                        TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                        Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                        Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                    }
                }

                DateTime LastPasswordChangedDate = Convert.ToDateTime(UserManagement.GetByID(Convert.ToInt32(user.ID)).ChangPasswordDate);
                DateTime currentDate = DateTime.Now;
                LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                if (user.CustomerID == 6)
                {
                    noDays = 90;
                }
                int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                if (dateDifference == noDays || dateDifference > noDays)
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
                else
                {
                    Session["ChangePassword"] = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
    public class GoogleAuthorizationData
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string scope { get; set; }
        public string token_type { get; set; }
        public string id_token { get; set; }

    }
    public class GoogleUserInfo
    {
        public string id { get; set; }
        public string email { get; set; }
        public bool verified_email { get; set; }
        public string name { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string picture { get; set; }
        public string locale { get; set; }
        public string hd { get; set; }

    }
}