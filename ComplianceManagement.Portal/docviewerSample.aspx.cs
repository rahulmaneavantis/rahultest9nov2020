﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class docviewerSample : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docurl"]))
                {
                    int complianceID = Convert.ToInt32(Request.QueryString["docurl"]);
                    string Internalsatutory = Convert.ToString(Request.QueryString["Internalsatutory"]);

                    if (Internalsatutory == "S")
                    {
                        var complianceForm_new = Business.ComplianceManagement.GetComplianceFormByID(complianceID);

                        if (complianceForm_new != null)
                        {
                            string FileName = complianceForm_new.FilePath;
                            CompDocReviewPath = FileName;
                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                        }

                        doccontrol.Document = Server.MapPath(CompDocReviewPath);

                        var complianceForm = Business.ComplianceManagement.GetMultipleComplianceFormByID(complianceID);

                        rptComplianceVersionView.DataSource = complianceForm;
                        rptComplianceVersionView.DataBind();
                    }
                    else if (Internalsatutory == "I") //Internal
                    {
                        var complianceForm_new = Business.InternalComplianceManagement.GetComplianceFormByID(complianceID);

                        if (complianceForm_new != null)
                        {
                            string filePath = complianceForm_new.Name;
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + File;
                            string extension = System.IO.Path.GetExtension(filePath);
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + FileDate;
                            string FileName = DateFolder + "/" + User + "" + extension;
                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            bw.Write(complianceForm_new.FileData);
                            bw.Close();

                            doccontrol.Document = Server.MapPath(FileName);

                            //string FileName = complianceForm_new.FilePath;
                            //CompDocReviewPath = FileName;
                            //CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                        }


                        var complianceForm = Business.InternalComplianceManagement.GetMultipleInternalComplianceForm(complianceID);

                        rptComplianceVersionView.DataSource = complianceForm;
                        rptComplianceVersionView.DataBind();
                    }

                }
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArg = e.CommandArgument.ToString().Split(',');

                if (e.CommandName.Equals("View"))
                {
                    string Internalsatutory = Convert.ToString(Request.QueryString["Internalsatutory"]);

                    if (Internalsatutory == "S")
                    {
                        int ID = Convert.ToInt32(commandArg[1]);
                        int complianceID = Convert.ToInt32(commandArg[0]);
                        var complianceForm_new = Business.ComplianceManagement.GetSelectedComplianceFileName(ID, complianceID);

                        if (complianceForm_new != null)
                        {
                            CompDocReviewPath = complianceForm_new.FilePath;
                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                            doccontrol.Document = Server.MapPath(CompDocReviewPath);
                        }
                    }
                    else if (Internalsatutory == "I")
                    {
                        int ID = Convert.ToInt32(commandArg[1]);
                        long complianceID = Convert.ToInt64(commandArg[0]);
                        var complianceForm_new = Business.InternalComplianceManagement.GetSelectedInternalComplianceFormByID(ID, complianceID);

                        if (complianceForm_new != null)
                        {
                            string filePath = complianceForm_new.Name;
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + File;
                            string extension = System.IO.Path.GetExtension(filePath);
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + FileDate;
                            string FileName = DateFolder + "/" + User + "" + extension;
                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            bw.Write(complianceForm_new.FileData);
                            bw.Close();

                            doccontrol.Document = Server.MapPath(FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}