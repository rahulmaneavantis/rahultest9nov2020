﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PracticeManagement.Master" AutoEventWireup="true" CodeBehind="PMEntityBranch.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters.PMEntityBranch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .aspNetDisabled {
            cursor: not-allowed;
        }

        .clearfix:after {
            clear: both;
            content: "";
            display: block;
            height: 0;
        }

        .pull-right {
            float: right;
        }

        .step > a {
            color: #333;
            text-decoration: none;
        }

        .step.current > a {
            color: white;
            text-decoration: none;
        }

        .step > a:active {
            color: #333;
            text-decoration: none;
        }

        .step > a:hover {
            color: white;
        }

        .step {
            clear: revert !important;
        }
        /* Breadcrups CSS */

        .arrow-steps .step {
            font-size: 14px;
            text-align: center;
            color: #666;
            cursor: default;
            margin: 0 3px;
            padding: 10px 10px 6px 30px;
            min-width: 10%;
            float: left;
            position: relative;
            background-color: #d9e3f7;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            transition: background-color 0.2s ease;
        }

            .arrow-steps .step:after,
            .arrow-steps .step:before {
                content: " ";
                position: absolute;
                top: 0;
                right: -17px;
                width: 0;
                height: 0;
                border-top: 19px solid transparent;
                border-bottom: 17px solid transparent;
                border-left: 17px solid #d9e3f7;
                z-index: 2;
                transition: border-color 0.2s ease;
            }

            .arrow-steps .step:before {
                right: auto;
                left: 0;
                border-left: 17px solid #fff;
                z-index: 0;
            }

            .arrow-steps .step:first-child:before {
                border: none;
            }

            .arrow-steps .step:last-child:after {
                border: none;
            }

            .arrow-steps .step:first-child {
                border-top-left-radius: 4px;
                border-bottom-left-radius: 4px;
            }

            .arrow-steps .step:last-child {
                border-top-right-radius: 4px;
                border-bottom-right-radius: 4px;
            }

            .arrow-steps .step span {
                position: relative;
            }

                .arrow-steps .step span:before {
                    opacity: 0;
                    content: "✔";
                    position: absolute;
                    top: -2px;
                    left: -20px;
                }

            .arrow-steps .step.done span:before {
                opacity: 1;
                -webkit-transition: opacity 0.3s ease 0.5s;
                -moz-transition: opacity 0.3s ease 0.5s;
                -ms-transition: opacity 0.3s ease 0.5s;
                transition: opacity 0.3s ease 0.5s;
            }

            .arrow-steps .step.current {
                color: #fff;
                background-color: #23468c;
            }

                .arrow-steps .step.current:after {
                    border-left: 17px solid #23468c;
                }

        .float-child {
            width: 84%;
            float: left;
        }

        .float-child1 {
            width: 10%;
            float: left;
        }
    </style>

    <script type="text/javascript">
        function fopenpopupPMCu() {
            $('#divCustomersBranchesDialog').modal('show');
        };

        function fClosepopupPMCu() {
            $('#divCustomersBranchesDialog').modal('hide');
        };

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/PracManagement/Masters/PracticeMGMTSetup.aspx" runat="server" Text="Customer/Entity"></asp:LinkButton>
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Location/ Branch"></asp:LinkButton>
                   <%-- PostBackUrl="~/PracManagement/Masters/PMEntityBranch.aspx"--%>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/PracManagement/Masters/PMUser_List.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                 <div class="step">
                    <asp:LinkButton ID="lnkBtnTax" PostBackUrl="~/PracManagement/Masters/PMTaxDetails.aspx" runat="server" Text="Tax Details"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/PracManagement/Masters/PMUserCustomerMapping.aspx" runat="server" Text="User Customer Assingment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/PracManagement/Masters/PMCustomerServices.aspx" runat="server" Text="Customer Invoicing Master"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upCustomerBranchList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                        <div style="margin-bottom: 4px" />

                        <div class="col-md-12 colpadding0" style="margin-bottom: 5px;">
                            <asp:DataList runat="server" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                                RepeatLayout="Flow" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                        runat="server" Style="text-decoration: none; color: Black" />
                                </ItemTemplate>
                                <ItemStyle Font-Size="12" ForeColor="Black" Font-Underline="true" />
                                <SeparatorStyle Font-Size="12" />
                                <SeparatorTemplate>
                                    &gt;
                                </SeparatorTemplate>
                            </asp:DataList>
                        </div>

                        <div class="col-md-12 colpadding0" style="margin-bottom: 5px;">
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-6 colpadding0" style="width:47%;">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>    
                                  <div class="col-md-6 colpadding0">                           
                                 <asp:DropDownListChosen runat="server" ID="ddlPageSize" Width="95%" AllowSingleDeselect="false" DisableSearchThreshold="2"
                                    CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownListChosen>
                                          </div>   
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 35%">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px; margin-left: -10%;">Customer</p>
                                </div>
                                <div class="col-md-9 colpadding0">                                
                                     <asp:DropDownListChosen ID="ddlCustomerlist" AllowSingleDeselect="false" runat="server"                                          
                                         DisableSearchThreshold="2" CssClass="form-control" Width="100%">
                                    </asp:DropDownListChosen> 
                                </div>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 35%">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Filter</p>
                                </div>
                                <div class="col-md-9 colpadding0" style="margin-left: -5%;">
                                    <asp:TextBox runat="server" ID="tbxFilter" class="form-control" Style="height: 32px;" MaxLength="50" AutoPostBack="true"
                                        OnTextChanged="tbxFilter_TextChanged" />
                                </div>
                            </div>
                            
                            <div class="col-md-1 colpadding0 entrycount" style="margin-top: 5px; padding-left: 18px; float: right;">
                                <asp:LinkButton Text="Add New" runat="server" ID="btnAddCustomerBranch" OnClick="btnAddCustomerBranch_Click" CssClass="btn btn-primary" />
                            </div>

                        </div>

                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="CustomerBranchValidationGroup1" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="CustomerBranchValidationGroup1"  Display="none" class="alert alert-block alert-danger fade in" />
                        </div>


                        <div style="margin-bottom: 4px;">

                            <asp:GridView runat="server" ID="grdCustomerBranch" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                   PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"                              
                                Font-Size="12px" OnRowCommand="grdCustomerBranch_RowCommand" 
                                OnPageIndexChanging="grdCustomerBranch_PageIndexChanging">

                                <Columns>                                   
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                    <%--<asp:BoundField DataField="TypeName" HeaderText="Type Name"  />--%>
                                    <%--<asp:BoundField DataField="ContactPerson" HeaderText="Contact Person" HeaderStyle-Height="20px" ItemStyle-Height="25px"  />--%>
                                    <asp:BoundField DataField="EmailID" HeaderText="Email" />                                    
                                    <asp:BoundField DataField="BranchStatus" HeaderText="Status" />
                                    <asp:TemplateField HeaderText="Created On">
                                        <ItemTemplate>
                                            <%# ((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yy HH:mm")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField  HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CommandName="VIEW_CHILDREN" CommandArgument='<%# Eval("ID") %>' ToolTip="View Branch">Entity/Branch</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField  ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_CUSTOMER_BRANCH" CommandArgument='<%# Eval("ID") %>'>
                                            <img src="../../Images/edit_icon_new.png" alt="Edit" title="Edit Entity Details" /></asp:LinkButton>

                                       

                                   <%--         <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_CUSTOMER_BRANCH" CommandArgument='<%# Eval("ID") %>'
                                                OnClientClick="return confirm('This will also delete all the sub-entities associated with current entity. Are you certain you want to delete this entity?');">
                                    <img src="../../Images/delete_icon_new.png" alt="Disable" title="Disable Entity/Branch" />
                                            </asp:LinkButton>--%>

                                           
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <HeaderStyle BackColor="#ECF0F1" />
                                <PagerSettings Visible="false" />
                                <PagerTemplate>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <div style="float: right;">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                    class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page                                          
                                        </p>
                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>

                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="modal fade" id="divCustomersBranchesDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upCustomerBranches" runat="server" UpdateMode="Conditional" OnLoad="upCustomerBranches_Load">
                        <ContentTemplate>
                            <div style="margin: 5px">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="CustomerBranchValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                        Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="CustomerBranchValidationGroup" />
                                </div>
                                <div style="margin-bottom: 7px;color: #333;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Customer</label>
                                    <asp:Literal ID="litCustomer" runat="server" />
                                </div>
                                <div id="divParent" runat="server" style="margin-bottom: 7px;color: #333;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Parent</label>
                                    <asp:Literal ID="litParent" runat="server" />
                                </div>
                                <div runat="server" id="divState" style="margin-bottom: 0px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        State</label>
                                    <%--<asp:DropDownList runat="server" ID="ddlState" Style="padding: 5px; margin: 0px; width: 390px;"
                                        CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" />--%>

                                        <asp:DropDownListChosen runat="server" ID="ddlState" Width="64%" AllowSingleDeselect="false" DisableSearchThreshold="2"
                                    CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                </asp:DropDownListChosen>

                                    <asp:CompareValidator ErrorMessage="Please select State." ControlToValidate="ddlState"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                        Display="None" />
                                </div>
                                <div class="clearfix"></div>
                                <div style="margin-bottom: 7px; margin-top: 7px;" class="float-container">
                                    <div class="float-child">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Gst Details</label>
                                        <asp:TextBox runat="server" ID="tbxGstNumber" CssClass="form-control" autocomplete="off" Style="width: 290px;" MaxLength="500" />
                                    </div>
                                    <div class="float-child1" id="divgst" runat="server">
                                        <asp:Button Text="Get Details" runat="server" ID="btrgetGstdetails" OnClick="btrgetGstdetails_Click" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div style="margin-bottom: 7px; margin-top: 7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                      Branch Name</label>
                                    <asp:TextBox runat="server" ID="tbxName" Style="width: 390px;" MaxLength="50" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                                        runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                        ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" ValidationExpression="[a-zA-Z0-9\s()\/@#.,-]*$"></asp:RegularExpressionValidator>
                                </div>
                              
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Address Line 1</label>
                                    <asp:TextBox runat="server" ID="tbxAddressLine1" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="150" />
                                    <asp:RequiredFieldValidator ErrorMessage="Address Line 1 can not be empty." ControlToValidate="tbxAddressLine1"
                                        runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Address Line 2</label>
                                    <asp:TextBox runat="server" ID="tbxAddressLine2" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="150" />
                                </div>
                                
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        City</label>
                                   <%-- <asp:DropDownList runat="server" ID="ddlCity" Style="padding: 5px; margin: 0px; width: 390px;"
                                        CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged" />--%>

                                    
                                        <asp:DropDownListChosen runat="server" ID="ddlCity" Width="64%" AllowSingleDeselect="false" DisableSearchThreshold="2"
                                    CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                </asp:DropDownListChosen>

                                    <asp:CompareValidator ErrorMessage="Please select City." ControlToValidate="ddlCity"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                        Display="None" />
                                </div>
                                <div runat="server" id="divOther" style="margin-bottom: 7px">
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Others</label>
                                    <asp:TextBox runat="server" ID="tbxOther" Style="width: 390px;" MaxLength="50" />
                                    <asp:RequiredFieldValidator ErrorMessage="Other City Name can not be empty." ControlToValidate="tbxOther"
                                        runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Pin Code</label>
                                    <asp:TextBox runat="server" ID="tbxPinCode" Style="width: 390px;" MaxLength="6" CssClass="form-control" />

                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Contact Person</label>
                                    <asp:TextBox runat="server" ID="tbxContactPerson" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="200" />                                 
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Landline No.</label>
                                    <asp:TextBox runat="server" ID="tbxLandline" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="15" />

                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Mobile No.</label>
                                    <asp:TextBox runat="server" ID="tbxMobile" Style="width: 390px;" MaxLength="15" CssClass="form-control" />

                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Email</label>
                                    <asp:TextBox runat="server" ID="tbxEmail" Style="width: 390px;" MaxLength="200" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ErrorMessage="Email can not be empty." ControlToValidate="tbxEmail"
                                        runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                        ErrorMessage="Please enter a valid email." ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Status</label>
                                    <asp:DropDownList runat="server" ID="ddlCustomerStatus" Style="padding: 5px; margin: 0px; width: 390px;"
                                        CssClass="form-control m-bot15" />
                                </div>

                                <div style="margin-bottom: 7px; display:none;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Person Responsible Applicable</label>
                                    <asp:DropDownList runat="server" ID="ddlPersonResponsibleApplicable" Style="padding: 5px; margin: 0px; width: 390px;"
                                        CssClass="form-control m-bot15">
                                        <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div style="margin-bottom: 7px; margin-left: 257px; margin-top: 10px;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                        ValidationGroup="CustomerBranchValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" OnClientClick="fClosepopupPMCu();" />
                                </div>
                            </div>
                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
