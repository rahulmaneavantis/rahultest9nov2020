﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters
{
    public partial class PMEntityBranch : System.Web.UI.Page
    {     
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["CustomerID"] = Session["CustomerID"];
                    ViewState["ParentID"] = null;
                    BindCustomerStatus();                  
                    BindStates();
                    BindCustomers();
                    BindGrid();
                    bindPageNumber();
                    int cid = Convert.ToInt32(ViewState["CustomerID"]);
                    if (cid != -1)
                    {
                        bool serviceProviderId = checkISServiseProvider(cid);
                        if (serviceProviderId)
                        {
                            bool CheckUserLimit = ICIAManagement.ServiceProviderUserLimit(Convert.ToInt32(cid), cid);
                            if (!CheckUserLimit)
                            {
                                btnAddCustomerBranch.Enabled = false;
                                btnAddCustomerBranch.ToolTip = "Facility to create more entities is not available in the Free version. Kindly contact Avantis to activate the same.";
                            }
                        }
                    }                                                    
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }            
        }
        private void BindCustomers()
        {
            try
            {
                int distributorID = -1;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                ddlCustomerlist.DataTextField = "Name";
                ddlCustomerlist.DataValueField = "ID";

                var data = PTCustomerManagement.GetAllCustomers(customerID, AuthenticationHelper.Role, distributorID, tbxFilter.Text);
                ddlCustomerlist.DataSource = data;
                ddlCustomerlist.DataBind();
                ddlCustomerlist.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomerlist.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)) != null)
                    {
                        ddlCustomerlist.ClearSelection();
                        ddlCustomerlist.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }                
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdCustomerBranch.PageIndex = chkSelectedPage - 1;
                grdCustomerBranch.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
            }
        }
        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";
                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        public bool checkISServiseProvider(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ServiceproviderId = false;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select (bool)cust.IsDistributor).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerBranch.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCustomerBranch.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["CUTotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            //dlBreadcrumb_ItemCommand(null,null);
        }
        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                        ViewState["EntityClientID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                    BindGrid();
                    upCustomerBranchList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindGrid()
        {
            try
            {
                long parentID = -1;              
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);                  
                }               
                dlBreadcrumb.DataSource = CustomerBranchManagement.GetHierarchy(Convert.ToInt32(ViewState["CustomerID"]), parentID);
                dlBreadcrumb.DataBind();
                var DataBranch = CustomerBranchManagement.GetAll(Convert.ToInt32(ViewState["CustomerID"]), parentID, tbxFilter.Text);
                //foreach (var item in DataBranch)
                //{
                //    if (item.TypeName == "Branch")
                //    {                       
                //        btnAddCustomerBranch.Visible = false;                       
                //        break;
                //    }
                //    else
                //    {                       
                //        btnAddCustomerBranch.Visible = true;                       
                //        break;
                //    }
                //}
                grdCustomerBranch.DataSource = DataBranch;
                grdCustomerBranch.DataBind();
                Session["CUTotalRows"] = DataBranch.Count;
                upCustomerBranchList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public static string ShowClientID(int custBranchID)
        {
            try
            {
                string clientID = string.Empty;
                clientID = RLCS_Master_Management.GetClientIDByBranchID(custBranchID);
                return clientID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        protected bool ShowHideButtons(string btnType, byte type, int complianceProdType)
        {
            bool showButton = false;
            if (!string.IsNullOrEmpty(btnType))
            {
                if (Convert.ToInt32(complianceProdType) > 0)
                {
                    if (btnType.Trim().ToUpper().Equals("C"))
                    {
                        if (Convert.ToInt32(type) == 1)
                        {
                            showButton = true;
                        }
                        else
                        {
                            showButton = false;
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(type) != 1)
                        {
                            showButton = true;
                        }
                    }                    
                }
            }

            return showButton;
        }

        protected bool ShowHideButtons(string btnType, int custBranchID)
        {
            bool showButton = false;
            if (!string.IsNullOrEmpty(btnType))
            {
                if (custBranchID > 0)
                {
                    string branchType = RLCS_Master_Management.GetBranchType(custBranchID);
                    if (btnType.Trim().ToUpper().Equals("C"))
                    {
                        if (branchType == "E")
                            showButton = true;
                        else
                            showButton = false;
                    }
                    else if (btnType.Trim().ToUpper().Equals("B"))
                    {
                        if (branchType == "B")
                            showButton = true;
                        else
                            showButton = false;
                    }
                }
            }
            return showButton;
        }
        protected void upCustomerBranches_Load(object sender, EventArgs e)
        {           
        }      
        private void PopulateInputForm()
        {
            try
            {                            
                divParent.Visible = ViewState["ParentID"] != null;
                litCustomer.Text = CustomerManagement.GetByID(Convert.ToInt32(ViewState["CustomerID"])).Name;
                if (ViewState["ParentID"] != null)
                {
                    litParent.Text = CustomerBranchManagement.GetByID(Convert.ToInt32(ViewState["ParentID"])).Name;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        protected void grdCustomerBranch_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int customerBranchID = 0;
            try
            {
                if (!String.IsNullOrEmpty(Convert.ToString(e.CommandArgument)))
                {
                    customerBranchID = Convert.ToInt32(e.CommandArgument);
                }
                if (e.CommandName.Equals("EDIT_CUSTOMER_BRANCH"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["CustomerBranchID"] = customerBranchID;                  
                    CustomerBranch customerBranch = CustomerBranchManagement.GetByID(customerBranchID);
                    PopulateInputForm();
                    if (!string.IsNullOrEmpty(customerBranch.Name))
                    {
                        tbxName.Text = customerBranch.Name;
                    }                                                                
                    tbxAddressLine1.Text = customerBranch.AddressLine1;
                    tbxAddressLine2.Text = customerBranch.AddressLine2;
                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.StateID)))
                    {
                        if (customerBranch.StateID != -1)
                        {
                            ddlState.SelectedValue = customerBranch.StateID.ToString();
                            ddlState_SelectedIndexChanged(null, null);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.CityID)))
                    {
                        if (customerBranch.CityID != -1)
                        {
                            ddlCity.SelectedValue = customerBranch.CityID.ToString();
                            ddlCity_SelectedIndexChanged(null, null);
                        }
                    }
                    tbxOther.Text = customerBranch.Others;
                    tbxPinCode.Text = customerBranch.PinCode;                
                    tbxContactPerson.Text = customerBranch.ContactPerson;
                    tbxLandline.Text = customerBranch.Landline;
                    tbxMobile.Text = customerBranch.Mobile;
                    tbxEmail.Text = customerBranch.EmailID;
                    tbxGstNumber.Text = customerBranch.GSTNumber;
                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Status)))
                    {
                        if (customerBranch.Status != -1)
                        {
                            ddlCustomerStatus.SelectedValue = Convert.ToString(customerBranch.Status);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.AuditPR)))
                    {
                        if (customerBranch.AuditPR == false)
                        {
                            ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(0);
                        }
                        else
                        {
                            ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(1);
                        }
                    }                                     
                    upCustomerBranches.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopupPMCu()", true);                   
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER_BRANCH"))
                {
                    if (CustomerManagement.CustomerBranchDelete(customerBranchID))
                    {
                        CustomerBranchManagement.Delete(customerBranchID);
                        CustomerBranchManagementRisk.Delete(customerBranchID);
                        CustomerBranchManagement.Delete_RLCSMappingBranch(customerBranchID);
                        BindGrid();
                        bindPageNumber();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('CustomerBranch is associated with assigned compliance, can not be deleted')", true);
                    }
                }                
                else if (e.CommandName.Equals("VIEW_CHILDREN"))
                {
                    try
                    {
                      
                        ViewState["ParentID"] = customerBranchID;                        
                        BindGrid();
                        bindPageNumber();
                        if (ViewState["EntityClientID"] == null)
                        {
                            string clientID = RLCS_Master_Management.GetClientIDByBranchID(customerBranchID);
                            if (clientID == "")
                            {                                
                                ViewState["EntityClientID"] = clientID;
                            }
                            else
                            {
                                ViewState["EntityClientID"] = clientID;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                ddlState.DataSource = AddressManagement.GetAllStates();
                ddlState.DataBind();

                ddlState.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCustomerBranch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = e.NewPageIndex;
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            int stateid = 0;
            if (!string.IsNullOrEmpty(ddlState.SelectedValue))
            {
                if (ddlState.SelectedValue.ToString() != "-1")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int cid = Convert.ToInt32(ViewState["CustomerID"]);
                        stateid = Convert.ToInt32(ddlState.SelectedValue);
                        var users = (from row in entities.CustomerBranches
                                     where row.IsDeleted == false
                                     && row.CustomerID == cid
                                     && row.StateID == stateid
                                     select row).FirstOrDefault();
                        if (users != null)
                        {                                                      
                            divgst.Attributes.Add("style", "display:none");
                        }
                        else
                        {
                            divgst.Attributes.Add("style", "display:block");
                            tbxGstNumber.Text = string.Empty;
                        }
                    }
                }
            }


            BindCities();
            ddlCity.SelectedValue = "-1";
        }
        private void BindCities()
        {
            try
            {
                ddlCity.DataSource = null;
                ddlCity.DataBind();
                ddlCity.ClearSelection();

                ddlCity.DataTextField = "Name";
                ddlCity.DataValueField = "ID";

                ddlCity.DataSource = AddressManagement.GetAllCitiesByState(Convert.ToInt32(ddlState.SelectedValue));
                ddlCity.DataBind();

                ddlCity.Items.Insert(0, new ListItem("< Other >", "0"));
                ddlCity.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCity.SelectedValue == "0" && divOther.Visible == false)
                {
                    divOther.Visible = true;
                    tbxOther.Text = string.Empty;
                }
                else if (ddlCity.SelectedValue != "0")
                {
                    divOther.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddCustomerBranch_Click(object sender, EventArgs e)
        {
            ViewState["Mode"] = 0;                       
            tbxName.Text = tbxAddressLine1.Text = tbxAddressLine2.Text = tbxOther.Text = tbxPinCode.Text = tbxContactPerson.Text = tbxLandline.Text = tbxMobile.Text = tbxEmail.Text = string.Empty;
            PopulateInputForm();                                  
            ddlState.SelectedValue = "-1";
            ddlState_SelectedIndexChanged(null, null);
            ddlCity_SelectedIndexChanged(null, null);
            ddlCustomerStatus.SelectedIndex = -1;           
            upCustomerBranches.Update();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopupPMCu()", true);           
        }

        public static string Invoke(string Method, string Uri, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            int _TimeoutSec = 90;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            
            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "POSTJson":
                    {


                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }

        #region 
        public class Compliance
        {
            public object filingFrequency { get; set; }
        }

        public class Addr
        {
            public string st { get; set; }
            public string lg { get; set; }
            public string stcd { get; set; }
            public string pncd { get; set; }
            public string bno { get; set; }
            public string flno { get; set; }
            public string dst { get; set; }
            public string loc { get; set; }
            public string bnm { get; set; }
            public string lt { get; set; }
            public string city { get; set; }
        }

        public class Pradr
        {
            public string ntr { get; set; }
            public Addr addr { get; set; }
        }

        public class TaxpayerInfo
        {
            public object errorMsg { get; set; }
            public string ctj { get; set; }
            public Pradr pradr { get; set; }
            public string cxdt { get; set; }
            public string sts { get; set; }
            public string gstin { get; set; }
            public string ctjCd { get; set; }
            public string ctb { get; set; }
            public string stj { get; set; }
            public string dty { get; set; }
            public List<object> adadr { get; set; }
            public object frequencyType { get; set; }
            public string lgnm { get; set; }
            public List<string> nba { get; set; }
            public string rgdt { get; set; }
            public string stjCd { get; set; }
            public string tradeNam { get; set; }
            public string panNo { get; set; }
        }

        public class Root
        {
            public Compliance compliance { get; set; }
            public TaxpayerInfo taxpayerInfo { get; set; }
            public List<object> filing { get; set; }
        }
        #endregion
        protected void btrgetGstdetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(tbxGstNumber.Text))
                {
                    //string postURL = "https://appyflow.in/api/verifyGST?gstNo=27BHLPS8412D1Z5&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                    string postURL = "https://appyflow.in/api/verifyGST?gstNo=" + tbxGstNumber.Text.Trim() + "&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                    string responseData = Invoke("GET", postURL, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        var myDeserializedClass = JsonConvert.DeserializeObject<Root>(responseData);

                        var panno = myDeserializedClass.taxpayerInfo.panNo;
                        var tradeNam = myDeserializedClass.taxpayerInfo.tradeNam;
                        var address = myDeserializedClass.taxpayerInfo.pradr.addr.bnm + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.lt + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.st + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.bno + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.dst + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.loc + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.pncd;

                        tbxName.Text = tradeNam;
                        tbxAddressLine1.Text = address;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = 0;
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
       
        public int GETServiseProviderID(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ServiceproviderId = -1;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select (int)cust.ServiceProviderID).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int comType = 0;
                #region 
                CustomerBranch customerBranch = new CustomerBranch()
                {
                    Name = tbxName.Text.Trim(),
                    Type = Convert.ToByte(1),
                    ComType = Convert.ToByte(comType),
                    AddressLine1 = tbxAddressLine1.Text,
                    AddressLine2 = tbxAddressLine2.Text,
                    StateID = Convert.ToInt32(ddlState.SelectedValue),
                    CityID = Convert.ToInt32(ddlCity.SelectedValue),
                    Others = tbxOther.Text,
                    PinCode = tbxPinCode.Text,
                    ContactPerson = tbxContactPerson.Text,
                    Landline = tbxLandline.Text,
                    Mobile = tbxMobile.Text,
                    EmailID = tbxEmail.Text,
                    CustomerID = Convert.ToInt32(ViewState["CustomerID"]),
                    ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                };
                if (ddlPersonResponsibleApplicable.SelectedItem.Text == "Yes")
                {
                    customerBranch.AuditPR = true;
                }
                customerBranch.LegalRelationShipID = null;
                customerBranch.LegalEntityTypeID = null;
                customerBranch.GSTNumber = tbxGstNumber.Text;
                if ((int)ViewState["Mode"] == 1)
                {
                    customerBranch.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                }
                if (CustomerBranchManagement.Exists(customerBranch, Convert.ToInt32(ViewState["CustomerID"])))
                {
                    cvDuplicateEntry.ErrorMessage = "Customer branch name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }
                if ((int)ViewState["Mode"] == 0)
                {
                    CustomerBranchManagement.Create(customerBranch);

                    try
                    {
                        if (!string.IsNullOrEmpty(tbxGstNumber.Text))
                        {
                            TM_TaxDetails Taxobj = new TM_TaxDetails();
                            Taxobj.TaxType = "GST";
                            Taxobj.RegNo = tbxGstNumber.Text;
                            Taxobj.CustomerId = Convert.ToInt32(ViewState["CustomerID"]);
                            Taxobj.BranchId = customerBranch.ID;
                            Taxobj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            Taxobj.Updatedby = Convert.ToInt32(AuthenticationHelper.UserID);
                            PTCustomerManagement.CreateUpdate_TAXDetailsData(Taxobj);
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    // Added by SACHIN 28 April 2016
                    string ReplyEmailAddressName = "Avantis";
                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerBranchCreaded
                                        .Replace("@NewCustomer", litCustomer.Text)
                                        .Replace("@BranchName", tbxName.Text)
                                        .Replace("@LoginUser", AuthenticationHelper.User)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    ;

                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                    string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();

                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM customer branch added.", message);

                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    CustomerBranchManagement.Update(customerBranch);

                    try
                    {
                        if (!string.IsNullOrEmpty(tbxGstNumber.Text))
                        {
                            TM_TaxDetails Taxobj = new TM_TaxDetails();
                            Taxobj.TaxType = "GST";
                            Taxobj.RegNo = tbxGstNumber.Text;
                            Taxobj.CustomerId = Convert.ToInt32(ViewState["CustomerID"]);
                            Taxobj.BranchId = customerBranch.ID;
                            Taxobj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            Taxobj.Updatedby = Convert.ToInt32(AuthenticationHelper.UserID);
                            PTCustomerManagement.CreateUpdate_TAXDetailsData(Taxobj);
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranch1 = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch()
                {
                    Name = tbxName.Text.Trim(),
                    Type = Convert.ToByte(1),
                    ComType = Convert.ToByte(comType),
                    AddressLine1 = tbxAddressLine1.Text,
                    AddressLine2 = tbxAddressLine2.Text,
                    StateID = Convert.ToInt32(ddlState.SelectedValue),
                    CityID = Convert.ToInt32(ddlCity.SelectedValue),
                    Others = tbxOther.Text,
                    PinCode = tbxPinCode.Text,
                    ContactPerson = tbxContactPerson.Text,
                    Landline = tbxLandline.Text,
                    Mobile = tbxMobile.Text,
                    EmailID = tbxEmail.Text,
                    CustomerID = Convert.ToInt32(ViewState["CustomerID"]),
                    ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                };
                if (ddlPersonResponsibleApplicable.SelectedItem.Text == "Yes")
                {
                    customerBranch1.AuditPR = true;
                }
                customerBranch1.GSTNumber = tbxGstNumber.Text;
                customerBranch1.LegalRelationShipID = null;
                customerBranch1.LegalEntityTypeID = null;
                if ((int)ViewState["Mode"] == 1)
                {
                    customerBranch1.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                }
                if (CustomerBranchManagement.Exists1(customerBranch1, Convert.ToInt32(ViewState["CustomerID"])))
                {
                    cvDuplicateEntry1.ErrorMessage = "Customer branch name already exists.";
                    cvDuplicateEntry1.IsValid = false;
                    return;
                }
                if ((int)ViewState["Mode"] == 0)
                {
                    CustomerBranchManagement.Create1(customerBranch1);                   
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    CustomerBranchManagement.Update1(customerBranch1);
                }
                BindGrid();
                bindPageNumber();
                upCustomerBranches.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:fClosepopupPMCu()", true);
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}