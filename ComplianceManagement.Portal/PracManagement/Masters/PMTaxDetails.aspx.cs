﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters
{
    public partial class PMTaxDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomers();
                BindGrid();
                bindPageNumber();
            }
        }
        private void BindGrid()
        {
            try
            {
                int serviceproviderID = -1;
                if (AuthenticationHelper.Role == "DADMN")
                {
                    serviceproviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    int? serProID = PTCustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));

                    if (serProID != null)
                        serviceproviderID = Convert.ToInt32(serProID);

                }
                var DataBranch = PTCustomerManagement.GetTaxDetailsAll(serviceproviderID, tbxFilter.Text);
                int customerID = -1;
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                if (customerID !=-1)
                {
                    DataBranch = DataBranch.Where(en => en.CustomerId == customerID).ToList();
                }

                grdTax.DataSource = DataBranch;
                grdTax.DataBind();
                Session["TaxTotalRows"] = DataBranch.Count;
                upTaxList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void grdTax_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int tid = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_TAX"))
                {
                    EditTaxInformation(tid);
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void EditTaxInformation(int tid)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox(0);", true);
                ViewState["Mode"] = 1;
                ViewState["TaxDetailsID"] = tid;
                TM_TaxDetails tdetails = PTCustomerManagement.GetByID(tid);
                tbxGovernmentLink.Text = tdetails.GovernmentLink;
                tbxPassword.Text = tdetails.Password;
                tbxUserID.Text = tdetails.UserId;
                tbxRegistrationNo.Text = tdetails.RegNo;
                tbxtaxType.Text = tdetails.TaxType;
                upTaxes.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopupPMTax()", true);                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdTax_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTax.PageIndex = e.NewPageIndex;
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {            
                #region tdetails
                TM_TaxDetails objtdetails = new TM_TaxDetails()
                {
                    GovernmentLink = tbxGovernmentLink.Text,
                    Password = tbxPassword.Text,
                    UserId = tbxUserID.Text,
                    RegNo = tbxRegistrationNo.Text,
                    TaxType = tbxtaxType.Text,
                };
                #endregion
                if ((int)ViewState["Mode"] == 1)
                {
                    objtdetails.ID = Convert.ToInt32(ViewState["TaxDetailsID"]);
                }                
                if ((int)ViewState["Mode"] == 0)
                {
                    PTCustomerManagement.CreateUpdate_TAXDetailsData(objtdetails);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    PTCustomerManagement.CreateUpdate_TAXDetailsData(objtdetails);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvDuplicateEntry1.IsValid = false;
                CvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomers()
        {
            try
            {
                int distributorID = -1;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                var data = PTCustomerManagement.GetAllCustomers(customerID, AuthenticationHelper.Role, distributorID, tbxFilter.Text);
                ddlCustomerList.DataSource = data;
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomerList.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)) != null)
                    {
                        ddlCustomerList.ClearSelection();
                        ddlCustomerList.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upTaxes_Load(object sender, EventArgs e)
        {
            
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdTax.PageIndex = 0;
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdTax.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdTax.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void btnAddTax_Click(object sender, EventArgs e)
        {

            ViewState["Mode"] = 0;
            tbxGovernmentLink.Text = tbxPassword.Text = tbxUserID.Text = tbxRegistrationNo.Text = tbxtaxType.Text  = string.Empty;          
            upTaxes.Update();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopupPMTax()", true);
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TaxTotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                //int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                //grdUser.PageIndex = chkSelectedPage - 1;
                //grdUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //BindGrid();
            }
        }

    }
}