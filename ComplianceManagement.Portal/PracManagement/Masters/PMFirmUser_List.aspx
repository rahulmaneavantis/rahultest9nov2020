﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PracticeManagement.Master" AutoEventWireup="true" CodeBehind="PMFirmUser_List.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters.PMFirmUser_List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .aspNetDisabled {
            cursor: not-allowed;
        }

        .clearfix:after {
            clear: both;
            content: "";
            display: block;
            height: 0;
        }

        .pull-right {
            float: right;
        }

        .step > a {
            color: #333;
            text-decoration: none;
        }

        .step.current > a {
            color: white;
            text-decoration: none;
        }

        .step > a:active {
            color: #333;
            text-decoration: none;
        }

        .step > a:hover {
            color: white;
        }

        .step {
            clear: revert !important;
        }
        /* Breadcrups CSS */

        .arrow-steps .step {
            font-size: 14px;
            text-align: center;
            color: #666;
            cursor: default;
            margin: 0 3px;
            padding: 10px 10px 6px 30px;
            min-width: 10%;
            float: left;
            position: relative;
            background-color: #d9e3f7;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            transition: background-color 0.2s ease;
        }

            .arrow-steps .step:after,
            .arrow-steps .step:before {
                content: " ";
                position: absolute;
                top: 0;
                right: -17px;
                width: 0;
                height: 0;
                border-top: 19px solid transparent;
                border-bottom: 17px solid transparent;
                border-left: 17px solid #d9e3f7;
                z-index: 2;
                transition: border-color 0.2s ease;
            }

            .arrow-steps .step:before {
                right: auto;
                left: 0;
                border-left: 17px solid #fff;
                z-index: 0;
            }

            .arrow-steps .step:first-child:before {
                border: none;
            }

            .arrow-steps .step:last-child:after {
                border: none;
            }

            .arrow-steps .step:first-child {
                border-top-left-radius: 4px;
                border-bottom-left-radius: 4px;
            }

            .arrow-steps .step:last-child {
                border-top-right-radius: 4px;
                border-bottom-right-radius: 4px;
            }

            .arrow-steps .step span {
                position: relative;
            }

                .arrow-steps .step span:before {
                    opacity: 0;
                    content: "✔";
                    position: absolute;
                    top: -2px;
                    left: -20px;
                }

            .arrow-steps .step.done span:before {
                opacity: 1;
                -webkit-transition: opacity 0.3s ease 0.5s;
                -moz-transition: opacity 0.3s ease 0.5s;
                -ms-transition: opacity 0.3s ease 0.5s;
                transition: opacity 0.3s ease 0.5s;
            }

            .arrow-steps .step.current {
                color: #fff;
                background-color: #23468c;
            }

                .arrow-steps .step.current:after {
                    border-left: 17px solid #23468c;
                }

        .float-child {
            width: 84%;
            float: left;
        }

        .float-child1 {
            width: 10%;
            float: left;
        }
    </style>

    <script type="text/javascript">
        function fopenpopupPMU() {

            $('#divUsersDialog').modal('show');
        };

        function fClosepopupPMU() {
            $('#divUsersDialog').modal('hide');
        };


        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkProduct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkProduct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkProduct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='ProductSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
        function initializeJQueryUI(textBoxID, divID) {          
            $("#" + textBoxID).unbind('click');
            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

      <%--  function initializeJQueryUI() {
            $("#<%= tbxBranch.ClientID %>").unbind('click');
            $("#<%= tbxBranch.ClientID %>").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });
        }--%>


        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkCustomer") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkCustomer']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkCustomer']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='CustomerSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/PracManagement/Masters/FirmMaster.aspx" runat="server" Text="Firm"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Office-Location"></asp:LinkButton>                   
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/PracManagement/Masters/PMFirmUser_List.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
               
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">
                        <section class="panel">
                        <div style="margin-bottom: 4px" />                      
                        <div class="col-md-12 colpadding0" style="margin-bottom: 5px;">
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-6 colpadding0"  style="width:47%;">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                            <div class="col-md-6 colpadding0">
                                 <asp:DropDownListChosen runat="server" ID="ddlPageSize" Width="95%" AllowSingleDeselect="false" DisableSearchThreshold="2"
                                    CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownListChosen>
                                 </div>
                            </div>
                          
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 35%;">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px; margin-left: -10%;">User</p>
                                </div>
                                <div class="col-md-9 colpadding0">
                                     <asp:DropDownListChosen ID="ddlUserList" AllowSingleDeselect="false" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserList_SelectedIndexChanged"
                                         DisableSearchThreshold="2" CssClass="form-control" Width="100%">
                                    </asp:DropDownListChosen>                                  
                                </div>
                            </div>                               
                              <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 35%">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Filter</p>
                                </div>
                                <div class="col-md-9 colpadding0" style="margin-left: -5%;">
                                    <asp:TextBox runat="server" ID="tbxFilter" class="form-control"  Style="height: 32px;"  MaxLength="50" AutoPostBack="true"
                                        OnTextChanged="tbxFilter_TextChanged" />
                                </div>
                            </div>
                            <div class="col-md-1 colpadding0 entrycount" style="margin-top: 5px; padding-left: 18px; float: right;">
                                <asp:LinkButton Text="Add New" runat="server" ID="btnAddUser" OnClick="btnAddUser_Click" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        </div>
                        <div style="margin-bottom: 4px;">                             
                            <asp:GridView runat="server" ID="grdUser" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                   PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"                              
                                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdUser_RowCommand" OnPageIndexChanging="grdUser_PageIndexChanging">
                                <Columns>                                   
                                    <asp:BoundField DataField="FirstName" HeaderText="First Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="LastName" HeaderText="Last Name"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Email" HeaderText="Email"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="ContactNumber" HeaderText="Contact No." />
                                    <asp:BoundField DataField="Role" HeaderText="Role"  />
                                    <asp:TemplateField ItemStyle-Width="60px" HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ID") %>' ID="lbtnChangeStatus"
                                                ToolTip="Click to toggle the status..." OnClientClick="return confirm('Are you certain you want to change the status of this User?');"><%# (Convert.ToBoolean(Eval("IsActive"))) ? "Active" : "Disabled" %></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="110px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CommandName="EDIT_USER" ID="lbtnEdit" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png" alt="Edit User" title="Edit User" /></asp:LinkButton>

                                            <asp:LinkButton runat="server" CommandName="DELETE_USER" CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete"
                                                OnClientClick="return confirm('Are you certain you want to delete this User?');"><img src="../../Images/delete_icon_new.png" alt="Delete User" title="Delete User" /></asp:LinkButton>

                                            <asp:LinkButton runat="server" CommandName="RESET_PASSWORD" ID="lbtnReset" CommandArgument='<%# Eval("ID") %>'
                                                OnClientClick="return confirm('Are you certain you want to reset password for this user?');"><img src="../../Images/reset_password_new.png" alt="Reset Password" title="Reset Password" /></asp:LinkButton>

                                            <asp:LinkButton runat="server" CommandName="UNLOCK_USER" CommandArgument='<%# Eval("ID") %>'
                                                Visible='<%# IsLocked((string)Eval("Email")) %>'><img src="../../Images/permissions_icon.png" alt="Unblock User" title="Unblock User" /></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <HeaderStyle BackColor="#ECF0F1" />
                                <PagerSettings Visible="false" />
                                <PagerTemplate>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <div style="float: right;">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                    class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page                                          
                                        </p>
                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                         </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="modal fade" id="divUsersDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upUsers" runat="server" UpdateMode="Conditional" OnLoad="upUsers_Load">
                        <ContentTemplate>
                            <div style="margin: 5px">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="UserValidationGroup" />
                                    <asp:CustomValidator ID="CvDuplicateEntry1" runat="server" EnableClientScript="False"
                                        ValidationGroup="UserValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                </div>                               
                                <%--<div style="margin-bottom: 7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Customer</label>
                                    <asp:TextBox runat="server" ID="txtCustomerType"    class="form-control" Style="width: 390px;" />
                                    <div style="display: none; margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvProduct">
                                        <asp:Repeater ID="rptCustomerType" runat="server">
                                            <HeaderTemplate>
                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                    <tr>
                                                        <td style="width:75px;">
                                                            <asp:CheckBox ID="CustomerSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                                        <td style="width: 220px;">
                                                            <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" /></td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 20px;">
                                                        <asp:CheckBox ID="chkCustomer" runat="server" onclick="UncheckHeader();" /></td>
                                                    <td style="width: 200px;">
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>--%>

                                 <%--<div runat="server" id="divCustomer" style="margin-bottom: 7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Customer</label>
                                  
                                     <asp:DropDownListChosen ID="ddlCustomer" AllowSingleDeselect="false" runat="server"                                        
                                         DisableSearchThreshold="2" CssClass="form-control" Width="64%">
                                    </asp:DropDownListChosen>    
                                    <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select customer."
                                        ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                        ValidationGroup="UserValidationGroup" Display="None" />
                                </div>--%>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Email</label>
                                    <asp:TextBox runat="server" ID="tbxEmail" Style="width: 390px;" MaxLength="200" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Email can not be empty."
                                        ControlToValidate="tbxEmail" runat="server" ValidationGroup="UserValidationGroup"
                                        Display="None" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server" CssClass="form-control"
                                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid email."
                                        ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        First Name</label>
                                    <asp:TextBox runat="server" ID="tbxFirstName" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="100" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="First Name can not be empty."
                                        ControlToValidate="tbxFirstName" runat="server" ValidationGroup="UserValidationGroup"
                                        Display="None" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid first name."
                                        ControlToValidate="tbxFirstName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Last Name</label>
                                    <asp:TextBox runat="server" ID="tbxLastName" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="100" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Last Name can not be empty."
                                        ControlToValidate="tbxLastName" runat="server" ValidationGroup="UserValidationGroup"
                                        Display="None" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid last name."
                                        ControlToValidate="tbxLastName" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Designation</label>
                                    <asp:TextBox runat="server" ID="tbxDesignation" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Designation can not be empty."
                                        ControlToValidate="tbxDesignation" runat="server" ValidationGroup="UserValidationGroup"
                                        Display="None" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid designation."
                                        ControlToValidate="tbxDesignation" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Mobile No</label>
                                    <asp:TextBox runat="server" ID="tbxContactNo" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="32" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Mobile Number can not be empty."
                                        ControlToValidate="tbxContactNo" runat="server" ValidationGroup="UserValidationGroup"
                                        Display="None" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter a valid Mobile number."
                                        ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxContactNo" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                        ValidationGroup="UserValidationGroup" ErrorMessage="Please enter only 10 digit."
                                        ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                </div>
                                <div runat="server" id="divComplianceRole" style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Compliance  Role</label>
                                    <asp:DropDownList runat="server" ID="ddlRole" Style="padding: 0px; margin: 0px; width: 180px;"
                                        CssClass="form-control m-bot15" AutoPostBack="true"  />
                                    <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Role." ControlToValidate="ddlRole"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserValidationGroup"
                                        Display="None" />
                                </div>
                                <div runat="server" id="divAuditRole" style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Audit Role</label>
                                    <asp:DropDownList runat="server" ID="ddlAuditRole" Enabled="true" Style="padding: 0px; margin: 0px; width: 180px;"
                                        CssClass="form-control m-bot15" AutoPostBack="true">
                                        <asp:ListItem Text="< Select Role >" Value="-1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Executive" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Manager" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Partner" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div runat="server" id="divSECRole" style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Secretarial Role</label>
                                    <asp:DropDownList runat="server" ID="ddlSECRole" Style="padding: 0px; margin: 0px; width: 180px;"
                                        CssClass="form-control m-bot15" AutoPostBack="true" />
                                </div>
                                <div runat="server" id="divHRRole" style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Labour Role</label>
                                    <asp:DropDownList runat="server" ID="ddlHRRole" Style="padding: 0px; margin: 0px; width: 180px;"
                                        CssClass="form-control m-bot15" AutoPostBack="true"  />
                                </div>   
                                
                                 <div runat="server" id="divPM" style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Practice Role</label>
                                    <asp:DropDownList runat="server" ID="ddlPMRole" Style="padding: 0px; margin: 0px; width: 180px;"
                                        CssClass="form-control m-bot15" AutoPostBack="true"  />
                                </div>  
                                <div runat="server" id="divPyroll" style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Payroll Role</label>
                                    <asp:DropDownList runat="server" ID="ddlPyrollRole" Style="padding: 0px; margin: 0px; width: 180px;"
                                        CssClass="form-control m-bot15" AutoPostBack="true"  />
                                </div>                          
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Address</label>
                                    <asp:TextBox runat="server" ID="tbxAddress" Style="height: 50px; width: 390px;" MaxLength="500"
                                        TextMode="MultiLine" />
                                </div>
                                <div style="margin-bottom: 7px; float: right; margin-right: 257px; margin-top: 10px;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" CausesValidation="true"
                                        ValidationGroup="UserValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" OnClientClick="fClosepopupPMU()" />
                                </div>
                            </div>
                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                            </div>
                            <div class="clearfix" style="height: 50px">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
