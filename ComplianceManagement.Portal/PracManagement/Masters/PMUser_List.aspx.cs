﻿using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters
{
    public partial class PMUser_List : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        protected static string CustomerName;
        protected static int ProductID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {

                        BindCustomers();
                        BindGrid();
                        bindPageNumber();
                        Session["CurrentRole"] = AuthenticationHelper.Role;
                        Session["CurrentUserId"] = AuthenticationHelper.UserID;
                        BindRoles();
                        BindRolesHR();
                        BindRolesSEC();
                        BindRolesRisk();
                        BindRolesPayroll();
                        BindRolesPM();
                        int customerID = -1;
                        int serviceproviderID = -1;
                        if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                        {
                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                        else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "DADMN")
                        {
                            serviceproviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            customerID = -1;
                        }
                        else
                        {
                            var aa = UserManagement.GetByID(AuthenticationHelper.UserID).IsAuditHeadOrMgr ?? null;
                            if (aa == "AM" || aa == "AH")
                            {
                                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            }
                        }                        
                        if (customerID != -1)
                        {
                            bool serviceProviderId = checkISServiseProvider(customerID);
                            if (serviceProviderId)
                            {                               
                                bool CheckUserLimit = ICIAManagement.ServiceProviderUserLimit(Convert.ToInt32(customerID), customerID);
                                if (!CheckUserLimit)
                                {
                                    btnAddUser.Enabled = false;                                   
                                    btnAddUser.ToolTip = "Facility to create more users is not available in the Free version. Kindly contact Avantis to activate the same.";
                                }
                            }
                        } 
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {
                    //added by rahul on 12 June 2018 Url Sequrity
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
        public class PMClass
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }
        public static List<PMClass> GetAll_PM_Roles(int customerID = 0)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<PMClass> hrRoleCodes = new List<PMClass>();
                PMClass obj = new PMClass();
                obj.ID = 1;
                obj.Name = "Yes";

                PMClass obj1 = new PMClass();
                obj1.ID = 2;
                obj1.Name = "No";

                hrRoleCodes.Add(obj);
                hrRoleCodes.Add(obj1);
                return hrRoleCodes.ToList();
            }
        }

        private void BindRolesPM()
        {
            try
            {
                ddlPMRole.DataTextField = "Name";
                ddlPMRole.DataValueField = "ID";
                var roles = GetAll_PM_Roles();
                ddlPMRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlPMRole.DataBind();
                ddlPMRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        public bool checkISServiseProvider(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ServiceproviderId = false;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select (bool)cust.IsDistributor).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }

        #region user Detail
        public void BindGrid()
        {
            try
            {
                int distributorID = -1;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
             

                var uselist = UserManagement.GetAllUser(customerID, tbxFilter.Text).Where(entry => entry.CustomerID != null).ToList();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (AuthenticationHelper.Role == "DADMN")
                    {
                        var clist = (from row in entities.Customers
                                     where row.ParentID == distributorID
                                     select row.ID).ToList();

                        uselist = uselist.Where(an => clist.Contains((int)an.CustomerID)).ToList();
                    }
                }
                grdUser.DataSource = uselist;

                Session["UMTotalRows"] = uselist.Count;
                grdUser.DataBind();
                upUserList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    
        protected bool IsLocked(string emailID)
        {
            try
            {
                if (UserManagement.WrongAttemptCount(emailID.Trim()) >= 3)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void grdUser_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_USER"))
                {
                    EditUserInformation(userID);
                }
                else if (e.CommandName.Equals("DELETE_USER"))
                {
                    if (UserManagement.HasCompliancesAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                    else if (EventManagement.GetAllAssignedInstancesByUser(userID).Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "EventInform", "alert('Account can not be deleted. One or more Event are assigned to user, please re-assign to other user.');", true);
                    }
                    else
                    {
                        UserManagement.Delete(userID);
                        UserManagementRisk.Delete(userID);
                    }
                    BindGrid();
                    bindPageNumber();
                }
                else if (e.CommandName.Equals("CHANGE_STATUS"))
                {
                    if (UserManagement.IsActive(userID) && UserManagement.HasCompliancesAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deactivated. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                    else
                    {
                        UserManagement.ToggleStatus(userID);
                        UserManagementRisk.ToggleStatus(userID);

                    }
                    BindGrid();
                    bindPageNumber();
                }
                else if (e.CommandName.Equals("RESET_PASSWORD"))
                {
                    User user = UserManagement.GetByID(userID);
                    mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                    string passwordText = Util.CreateRandomPassword(10);
                    user.Password = Util.CalculateAESHash(passwordText);
                    mstuser.Password = Util.CalculateAESHash(passwordText);

                    string ReplyEmailAddressName = "";
                    if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                    {
                        ReplyEmailAddressName = "Avantis";
                    }
                    else if (AuthenticationHelper.Role == "CADMN")
                    {

                        ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName((int)AuthenticationHelper.CustomerID);
                    }

                    string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]), ReplyEmailAddressName);
                    bool result = UserManagement.ChangePassword(user);
                    bool result1 = UserManagementRisk.ChangePassword(mstuser);
                    if (result && result1)
                    {
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<string>(new string[] { user.Email }), null, null, "AVACOM account password changed", message);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Password reset successfully.');", true);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }              
                else if (e.CommandName.Equals("UNLOCK_USER"))
                {
                    UserManagement.WrongAttemptCountUpdate(userID);
                    UserManagementRisk.WrongAttemptCountUpdate(userID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "Unloack", "alert('User unlocked successfully.');", true);
                    BindGrid();
                    bindPageNumber();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindNewUserList(int customerID, DropDownList ddllist)
        {
            try
            {
                var data = UserManagement.GetAllUserCustomerID(customerID, null);
                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";               
                if (data != null)
                {
                    ddllist.DataSource = data;
                    ddllist.DataBind();                    
                }
                ddllist.Items.Insert(0, new ListItem("< Select user >", "-1"));              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUser.PageIndex = e.NewPageIndex;
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    
    
        #endregion

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
            {
                int customerid = -1;
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    AddNewUser(Convert.ToInt32(ddlCustomerList.SelectedValue));
                }
                else
                {
                    AddNewUser(customerid);
                }
            }
            upUsers.Update();

            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopupPMU()", true);
        }

        public void AddNewUser(int CustomerID)
        {

            try
            {
                ViewState["Mode"] = 0;
                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                tbxEmail.Enabled = true;
                ddlCustomer.Enabled = true;               
                ddlRole.ClearSelection();
                ddlSECRole.ClearSelection();
                ddlAuditRole.ClearSelection();
                ddlHRRole.ClearSelection();
                ddlRole.SelectedValue = "-1";
                ddlCustomer.SelectedValue = "-1";
                ddlRole_SelectedIndexChanged(null, null);                                         
                EnableDisableRole(CustomerID);                             
                upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();                
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdUser.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["UMTotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdUser.PageIndex = chkSelectedPage - 1;
                grdUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
            }
        }
        
        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
       
       
        public string GetUserRole(long UserID, int ProductID)
        {
            try
            {
                string result = "";
                ComplianceDBEntities entities = new ComplianceDBEntities();

                if (ProductID == 1) //Compliance
                {
                    var data = (from row in entities.Users
                                join row1 in entities.Roles
                                on row.RoleID equals row1.ID
                                where row.ID == UserID
                                select row1.Name).FirstOrDefault();

                    if (data != null)
                    {
                        result = data;
                    }
                    return result;
                }
                if (ProductID == 4) //Audit
                {
                    var data = (from row in entities.Users
                                join row1 in entities.Roles
                                on row.RoleID equals row1.ID
                                where row.ID == UserID
                                select row1.Name).FirstOrDefault();

                    if (data != null)
                    {
                        result = data;
                    }
                    return result;
                }
                if (ProductID == 8) // Secretrial
                {
                    var data = (from row in entities.Users
                                join row1 in entities.Roles
                                on row.SecretarialRoleID equals row1.ID
                                where row.ID == UserID
                                select row1.Name).FirstOrDefault();

                    if (data != null)
                    {
                        result = data;
                    }
                    return result;
                }
                if (ProductID == 9) //HRProduct
                {
                    var data = (from row in entities.Users
                                join row1 in entities.Roles
                                on row.RoleID equals row1.ID
                                where row.ID == UserID
                                select row1.Name).FirstOrDefault();

                    if (data != null)
                    {
                        result = data;
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return "";
        }    
        public static List<Role> GetSECLimitedRole()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.IsForSecretarial == true
                             select row);

                return roles.ToList();
            }
        }
        private void BindSeretrialUserRoles(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                var roles = GetSECLimitedRole();
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN")).ToList();
                }

                if (roles.Count > 0)
                {
                    List<string> secRoleCodes = new List<string> { "CEXCT", "DADMN", "CSMGR" };

                    roles = roles.Where(row => secRoleCodes.Contains(row.Code)).ToList();
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                ddlUserList.DataSource = roles.OrderBy(entry => entry.Name);
                ddlUserList.DataBind();
                ddlUserList.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void BindHRUserRoles(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {          
                var roles = RoleManagement.GetAll_HRCompliance_Roles();
                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                ddlUserList.DataSource = roles.OrderBy(entry => entry.Name);
                ddlUserList.DataBind();
                ddlUserList.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }                   
        protected void upUsers_Load(object sender, EventArgs e)
        {
            try
            {                                
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);            
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewEdit", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }      
        protected void upDepeList_Load(object sender, EventArgs e)
        {
        }
        
        public static List<Role> GetAll(bool? onlyForCompliance = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.ID <= 19 || row.Code.Equals("SPADM")
                             || row.Code.Equals("HVADM") || row.Code.Equals("HVAUD")                           
                             select row);

                if (onlyForCompliance.HasValue)
                {
                    roles = roles.Where(entry => entry.IsForCompliance == onlyForCompliance.Value && entry.Code != "APPR");
                }
                roles = roles.Where(entry => entry.ID != -1);
                return roles.ToList();
            }
        }
        private void BindRolesRisk()
        {
            try
            {
                ddlAuditRole.DataTextField = "Name";
                ddlAuditRole.DataValueField = "ID";

                var roles = GetLimitedRole(false);
                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN")).ToList();
                }

                ddlAuditRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlAuditRole.DataBind();

                ddlAuditRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRoles()
        {
            try
            {
                ddlRole.DataTextField = "Name";
                ddlRole.DataValueField = "ID";

                var roles = GetAll(false);
                ddlRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

   

        public static List<Role> GetHRLimitedRole()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.ID == 14 || row.ID == 15 || row.ID == 16 || row.ID == 17 || row.ID == 18
                             select row);

                return roles.ToList();
            }
        }

        public static List<Role> GetAll_HRCompliance_Roles(int customerID = 0)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<string> hrRoleCodes = new List<string> { "CADMN", "HEXCT", "HMGR" };
             
                if (customerID != 0)
                {
                    var customerRecord = (from row in entities.Customers
                                          where row.ID == customerID
                                          select row).FirstOrDefault();

                    if (customerRecord != null)
                    {
                        if (customerRecord.IsServiceProvider != null)
                        {
                            if (Convert.ToBoolean(customerRecord.IsServiceProvider))
                                hrRoleCodes.Add("SPADM");
                        }

                        if (customerRecord.IsDistributor != null)
                        {
                            if (Convert.ToBoolean(customerRecord.IsDistributor))
                                hrRoleCodes.Add("DADMN");
                        }

                        if (customerRecord.ComplianceProductType != null)
                        {
                            if (Convert.ToInt32(customerRecord.ComplianceProductType) == 3)
                            {
                                hrRoleCodes.Add("HMGMT");
                                hrRoleCodes.Add("LSPOC");
                                hrRoleCodes.Add("HAPPR");
                            }
                        }
                    }
                }

                var roles = (from row in entities.Roles
                             where (hrRoleCodes.Contains(row.Code))
                             select row);

                return roles.ToList();
            }
        }

     
        private void BindRolesPayroll()
        {
            try
            {
                ddlPyrollRole.DataTextField = "Name";
                ddlPyrollRole.DataValueField = "ID";
                List<Role> roles = new List<Role>();
                roles = GetAll_HRCompliance_Roles();

                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN") && !entry.Code.Equals("IMPT")).ToList();
                }

                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("IMPT")).ToList();
                }

                bool showDADMNRole = false;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        var customerDetails = CustomerManagement.GetByID(Convert.ToInt32(ddlCustomer.SelectedValue));

                        if (customerDetails != null)
                            if (customerDetails.IsDistributor != null)
                                if (Convert.ToBoolean(customerDetails.IsDistributor))
                                    showDADMNRole = true;
                    }
                }

                if (!showDADMNRole)
                    roles = roles.Where(entry => !entry.Code.Equals("DADMN")).ToList();

                ddlPyrollRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlPyrollRole.DataBind();

                ddlPyrollRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRolesHR()
        {
            try
            {
                ddlHRRole.DataTextField = "Name";
                ddlHRRole.DataValueField = "ID";
                List<Role> roles = new List<Role>();              
                roles = GetAll_HRCompliance_Roles();

                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN") && !entry.Code.Equals("IMPT")).ToList();
                }

                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("IMPT")).ToList();
                }

                bool showDADMNRole = false;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        var customerDetails = CustomerManagement.GetByID(Convert.ToInt32(ddlCustomer.SelectedValue));

                        if (customerDetails != null)
                            if (customerDetails.IsDistributor != null)
                                if (Convert.ToBoolean(customerDetails.IsDistributor))
                                    showDADMNRole = true;
                    }
                }

                if (!showDADMNRole)
                    roles = roles.Where(entry => !entry.Code.Equals("DADMN")).ToList();

                ddlHRRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlHRRole.DataBind();

                ddlHRRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<Role> GetLimitedRole(bool? onlyForCompliance = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.ID == 2 || row.ID == 7 || row.ID == 8 || row.ID == 13 || row.ID == 9
                              || row.ID == 19
                             select row);

                if (onlyForCompliance.HasValue)
                {
                    roles = roles.Where(entry => entry.IsForCompliance == onlyForCompliance.Value && entry.Code != "APPR");
                }
                return roles.ToList();
            }
        }

        public List<RoleVM> GetSecretarialRoles_Limited()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<string> secRoleCodes = new List<string> { "HDCS", "CS", "DRCTR" };

                var roles = (from row in entities.Roles
                             where row.IsForSecretarial == true
                             && secRoleCodes.Contains(row.Code)
                             select new RoleVM
                             {
                                 RoleID = row.ID,
                                 RoleName = row.Name,
                                 RoleCode = row.Code
                             }).ToList();
                return roles;
            }
        }

   
        private void BindRolesSEC()
        {
            try
            {               
                ddlSECRole.DataTextField = "RoleName";
                ddlSECRole.DataValueField = "RoleID";
                var roles = GetSecretarialRoles_Limited();             
                ddlSECRole.DataSource = roles;
                ddlSECRole.DataBind();
                ddlSECRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlAuditRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int Aurole = -1;
                if (ddlAuditRole.SelectedValue == "1")  //EXecutive
                {
                    Aurole = 7;
                }
                else if (ddlAuditRole.SelectedValue == "2") //Manager
                {
                    Aurole = 7;
                }
                else if (ddlAuditRole.SelectedValue == "4") //Partner
                {
                    Aurole = 2;
                }


                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(Aurole));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                if (roleCode.Equals("VAUDT"))
                {
                    divCustomerBranch.Visible = roleCode.Equals("VAUDT");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlSECRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlSECRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                if (roleCode.Equals("VAUDT"))
                {
                    divCustomerBranch.Visible = roleCode.Equals("VAUDT");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlHRRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlHRRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                if (roleCode.Equals("VAUDT"))
                {
                    divCustomerBranch.Visible = roleCode.Equals("VAUDT");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                if (roleCode.Equals("VAUDT"))
                {
                    divCustomerBranch.Visible = roleCode.Equals("VAUDT");
                }
                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewRoleChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }           
                if (divCustomerBranch.Visible)
                {
                    ddlCustomer_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewRoleChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<Customer> GetAllCustomers(int customerID, string rolecode, int distributorid, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var clist = (from row in entities.Customers
                             where row.IsDeleted == false
                              && row.ComplianceProductType != 1
                             select row);

                if (rolecode == "DADMN")
                {
                    clist = clist.Where(entry => entry.ParentID == distributorid);
                }
                else
                {
                    if (customerID != -1)
                    {
                        clist = clist.Where(entry => entry.ID == customerID);
                    }
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    clist = clist.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter) || entry.BuyerContactNumber.Contains(filter));
                }

                return clist.OrderBy(entry => entry.Name).ToList();
            }
        }
        private void BindCustomers()
        {
            try
            {
                int distributorID = -1;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                var data = GetAllCustomers(customerID, AuthenticationHelper.Role, distributorID, tbxFilter.Text);
                ddlCustomerList.DataSource = data;
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));

                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataSource = data;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomerList.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)) != null)
                    {
                        ddlCustomerList.ClearSelection();
                        ddlCustomerList.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = string.Empty;
                BindCustomerBranches();
                BindReportingTo();
                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewCustomerChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindReportingTo()
        {
            try
            {
                ddlReportingTo.DataTextField = "Name";
                ddlReportingTo.DataValueField = "ID";

                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }

                ddlReportingTo.DataSource = UserManagement.GetAllByCustomerID(Convert.ToInt32(ddlCustomer.SelectedValue), roleCode);
                ddlReportingTo.DataBind();

                ddlReportingTo.Items.Insert(0, new ListItem("< Select Reporting to person >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Branch >";
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (customerID != -1)
                {
                    tvBranches.Nodes.Clear();
                    NameValueHierarchy branch = null;
                    var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    if (branchs.Count > 0)
                    {
                        branch = branchs[0];
                    }
                    tbxBranch.Text = "< Select Location >";
                    List<TreeNode> nodes = new List<TreeNode>();
                    BindBranchesHierarchy(null, branch, nodes);
                    foreach (TreeNode item in nodes)
                    {
                        tvBranches.Nodes.Add(item);
                    }
                    tvBranches.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    
        public static List<long> GetByProductID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var productmapping = (from row in entities.ProductMappings
                                      where row.CustomerID == customerID && row.IsActive == false
                                      select (long)row.ProductID).ToList();

                return productmapping;
            }
        }
        public void EnableDisableRole(int CustomerID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ab = false;
                int customerID = -1;
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = CustomerID;
                }
                else
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                var Listofproduct = GetByProductID(customerID);
                if (Listofproduct.Count > 0)
                {

                    divComplianceRole.Visible = false;
                    ddlRole.Enabled = false;

                    divAuditRole.Visible = false;
                    ddlAuditRole.Enabled = false;

                    divSECRole.Visible = false;
                    ddlSECRole.Enabled = false;

                    divHRRole.Visible = false;
                    ddlHRRole.Enabled = false;

                    divPM.Visible = false;
                    ddlPMRole.Enabled = false;

                    divPyroll.Visible = false;
                    ddlPyrollRole.Enabled = false;

                    if (Listofproduct.Contains(1))
                    {
                        divComplianceRole.Visible = true;
                        ddlRole.Enabled = true;
                    }
                    if ((Listofproduct.Contains(3) || Listofproduct.Contains(4)))
                    {
                        divAuditRole.Visible = true;
                        ddlAuditRole.Enabled = true;
                    }
                    if (Listofproduct.Contains(8))
                    {
                        divSECRole.Visible = true;
                        ddlSECRole.Enabled = true;
                    }
                    if (Listofproduct.Contains(9))
                    {
                        divHRRole.Visible = true;
                        ddlHRRole.Enabled = true;
                    }
                    if (Listofproduct.Contains(10))
                    {
                        divPM.Visible = true;
                        ddlPMRole.Enabled = true;
                    }
                    if (Listofproduct.Contains(11))
                    {
                        divPyroll.Visible = true;
                        ddlHRRole.Enabled = true;
                    }
                }
            }
        }
        
        public void EditUserInformation(int userID)
        {
            try
            {                            
                ViewState["Mode"] = 1;
                ViewState["UserID"] = userID;
                User user = UserManagement.GetByID(userID);
                List<UserParameterValueInfo> userParameterValues = UserManagement.GetParameterValuesByUserID(userID);

                tbxFirstName.Text = user.FirstName;
                tbxLastName.Text = user.LastName;
                tbxDesignation.Text = user.Designation;
                tbxEmail.Text = user.Email;
                tbxContactNo.Text = user.ContactNumber;
                tbxAddress.Text = user.Address;               
                var Productdetails = GetByProductID(Convert.ToInt32(user.CustomerID));

                if (Productdetails.Contains(1))
                {
                    divComplianceRole.Visible = true;
                }
                else
                {
                    divComplianceRole.Visible = false;
                }

                if (Productdetails.Contains(3))
                {
                    divAuditRole.Visible = true;
                }
                else if (Productdetails.Contains(4))
                {
                    divAuditRole.Visible = true;
                }
                else
                {
                    divAuditRole.Visible = false;
                }

                if (Productdetails.Contains(8))
                {
                    divSECRole.Visible = true;
                }
                else
                {
                    divSECRole.Visible = false;
                }

                if (Productdetails.Contains(9))
                {
                    divHRRole.Visible = true;
                }
                else
                {
                    divHRRole.Visible = false;
                }              
                if (Productdetails.Contains(10))
                {
                    divPM.Visible = true;
                }
                else
                {
                    divPM.Visible = false;
                }
                if (Productdetails.Contains(11))
                {
                    divPyroll.Visible = true;
                }
                else
                {
                    divPyroll.Visible = false;
                }

                ddlRole.SelectedValue = user.RoleID != null ? user.RoleID.ToString() : "-1";
                ddlRole_SelectedIndexChanged(null, null);
                ddlHRRole.SelectedValue = user.HRRoleID != null ? user.HRRoleID.ToString() : "-1";
                ddlSECRole.SelectedValue = user.SecretarialRoleID != null ? user.SecretarialRoleID.ToString() : "-1";
                ddlPMRole.SelectedValue = user.PMRoleID != null ? user.PMRoleID.ToString() : "-1";
                ddlPyrollRole.SelectedValue = user.PayrollRoleID != null ? user.PayrollRoleID.ToString() : "-1";


                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                
                ddlPyrollRole.SelectedValue = mstuser.RoleID != null ? mstuser.RoleID.ToString() : "-1";
                ddlAuditRole_SelectedIndexChanged(null, null);
                if (divCustomer.Visible && user.CustomerID.HasValue)
                {
                    ddlCustomer.SelectedValue = user.CustomerID.Value.ToString();
                    ddlCustomer_SelectedIndexChanged(null, null);
                }
                ddlReportingTo.SelectedValue = user.ReportingToID != null ? user.ReportingToID.ToString() : "-1";
                if (divCustomerBranch.Visible && user.CustomerBranchID.HasValue)
                {
                    Queue<TreeNode> queue = new Queue<TreeNode>();
                    foreach (TreeNode node in tvBranches.Nodes)
                    {
                        queue.Enqueue(node);
                    }
                    while (queue.Count > 0)
                    {
                        TreeNode node = queue.Dequeue();
                        if (node.Value == user.CustomerBranchID.Value.ToString())
                        {
                            node.Selected = true;
                            break;
                        }
                        else
                        {
                            foreach (TreeNode child in node.ChildNodes)
                            {
                                queue.Enqueue(child);
                            }
                        }
                    }
                    tvBranches_SelectedNodeChanged(null, null);
                }
                upUsers.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopupPMU()", true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewEdit", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public bool GetServiseProviderId(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ServiceproviderId = false;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select (bool)cust.IsDistributor).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }

        public static bool CreateUpdate_UserCustomerMapping(UserCustomerMapping _objRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.UserCustomerMappings
                                      where row.UserID == _objRecord.UserID
                                      && row.CustomerID == _objRecord.CustomerID
                                      && row.ProductID == _objRecord.ProductID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.MgrID = _objRecord.MgrID;
                        prevRecord.ProductID = _objRecord.ProductID;
                        prevRecord.IsActive = _objRecord.IsActive;
                        prevRecord.UpdatedOn = DateTime.Now;

                        saveSuccess = true;
                    }
                    else
                    {
                        _objRecord.CreatedOn = DateTime.Now;
                        entities.UserCustomerMappings.Add(_objRecord);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                int getproductCOMPLIANCE = -1;
                int getproductAudit = -1;
                int getproductHR = -1;
                int getproductSec = -1;
                string PrimaryRoleAudit = "";

                int getproductPayroll = -1;
                int getproductPM = -1;

                var Productdetails = GetByProductID(Convert.ToInt32(customerID));
                string Isauditheadormgr = null;

                if (Productdetails.Contains(1))
                {
                    getproductCOMPLIANCE = Convert.ToInt32(ddlRole.SelectedValue);
                }
                if (Productdetails.Contains(3) || Productdetails.Contains(4))
                {
                    getproductAudit = Convert.ToInt32(ddlAuditRole.SelectedValue);                    
                }                
                if (Productdetails.Contains(8))
                {
                    getproductSec = Convert.ToInt32(ddlSECRole.SelectedValue);
                }
                if (Productdetails.Contains(9))
                {
                    getproductHR = Convert.ToInt32(ddlHRRole.SelectedValue);
                }
                if (Productdetails.Contains(10))
                {
                    getproductPM = Convert.ToInt32(ddlPMRole.SelectedValue);
                }
                if (Productdetails.Contains(11))
                {
                    getproductPayroll = Convert.ToInt32(ddlPyrollRole.SelectedValue);
                }
                bool val = true;
                if (getproductSec == -1 && getproductCOMPLIANCE == -1 && getproductAudit == -1 && getproductHR == -1)
                {
                    val = false;
                }
                if (val)
                {

                    if (getproductCOMPLIANCE == -1)
                    {
                        getproductCOMPLIANCE = -1;
                    }

                    if (getproductAudit == -1)
                    {
                        getproductAudit = -1;
                    }

                    if (getproductPayroll == -1)
                    {
                        getproductPayroll = -1;
                    }

                    if (getproductPM == -1)
                    {
                        getproductPM = 2;
                    }

                    #region limit
                    #region Compliance User
                    User user = new User()
                    {
                        FirstName = tbxFirstName.Text,
                        LastName = tbxLastName.Text,
                        Designation = tbxDesignation.Text,
                        Email = tbxEmail.Text,
                        ContactNumber = tbxContactNo.Text,
                        Address = tbxAddress.Text,
                        RoleID = getproductCOMPLIANCE,
                        IsExternal = false,
                        IsAuditHeadOrMgr = Isauditheadormgr,
                    };

                    if (getproductSec != -1)
                    {
                        user.SecretarialRoleID = getproductSec;
                    }
                    else
                    {
                        user.SecretarialRoleID = null;
                    }

                    user.HRRoleID = getproductHR;

                    if (divCustomer.Visible)
                    {
                        user.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }

                    if (divCustomerBranch.Visible)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(tvBranches.SelectedNode)))
                        {
                            user.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                        }
                    }

                    if (ddlReportingTo.SelectedValue != "-1" && ddlReportingTo.SelectedValue != "")
                    {
                        user.ReportingToID = Convert.ToInt64(ddlReportingTo.SelectedValue);
                    }

                    user.PayrollRoleID = getproductPayroll;
                    user.PMRoleID = getproductPM;
                    List<UserParameterValue> parameters = new List<UserParameterValue>();
                    #endregion

                    #region Risk User
                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                    {
                        FirstName = tbxFirstName.Text,
                        LastName = tbxLastName.Text,
                        Designation = tbxDesignation.Text,
                        Email = tbxEmail.Text,
                        ContactNumber = tbxContactNo.Text,
                        Address = tbxAddress.Text,
                        RoleID = getproductAudit,
                        IsExternal = false,
                        IsAuditHeadOrMgr = Isauditheadormgr,
                    };

                    if (getproductSec != -1)
                    {
                        mstUser.SecretarialRoleID = getproductSec;
                    }
                    else
                    {
                        mstUser.SecretarialRoleID = null;
                    }

                    mstUser.HRRoleID = getproductHR;
                    mstUser.IsHead = false;
                    user.IsHead = false;

                    mstUser.PayrollRoleID = getproductPayroll;
                    mstUser.PMRoleID = getproductPM;
                    if (divCustomer.Visible)
                    {
                        mstUser.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    if (divCustomerBranch.Visible)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(tvBranches.SelectedNode)))
                        {
                            mstUser.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                        }
                    }

                    List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk> parametersRisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>();

                    #endregion

                    if (getproductCOMPLIANCE == 2)
                    {
                        user.VendorRoleID = 2;
                        mstUser.VendorRoleID = 2;
                        var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                        if (ProductMappingDetails.Contains(2))
                        {
                            if (user.LitigationRoleID == null)
                            {
                                user.LitigationRoleID = 2;
                                mstUser.LitigationRoleID = 2;
                            }
                        }
                        if (ProductMappingDetails.Contains(5))
                        {
                            if (user.ContractRoleID == null)
                            {
                                user.ContractRoleID = 2;
                                mstUser.ContractRoleID = 2;
                            }
                        }
                        if (ProductMappingDetails.Contains(6))
                        {
                            if (user.LicenseRoleID == null)
                            {
                                user.LicenseRoleID = 2;
                                mstUser.LicenseRoleID = 2;
                            }
                        }
                    }
                    if ((int)ViewState["Mode"] == 1)
                    {
                        user.ID = Convert.ToInt32(ViewState["UserID"]);
                        mstUser.ID = Convert.ToInt32(ViewState["UserID"]);
                    }
                    bool emailExists;
                    UserManagement.Exists(user, out emailExists);
                    if (emailExists)
                    {
                        CvDuplicateEntry1.IsValid = false;
                        CvDuplicateEntry1.ErrorMessage = "User with Same Email Already Exists.";
                        return;
                    }
                    UserManagementRisk.Exists(mstUser, out emailExists);
                    if (emailExists)
                    {
                        CvDuplicateEntry1.IsValid = false;
                        CvDuplicateEntry1.ErrorMessage = "User with Same Email Already Exists.";
                        return;
                    }
                    bool result = false;
                    int resultValue = 0;

                    long cid = -1;
                    if (divCustomer.Visible)
                    {
                        cid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    else
                    {
                        cid = AuthenticationHelper.UserID;
                    }
                    user.PrimaryRoleAudit = PrimaryRoleAudit;
                    mstUser.PrimaryRoleAudit = PrimaryRoleAudit;
                    if ((int)ViewState["Mode"] == 0)
                    {
                        user.CreatedBy = AuthenticationHelper.UserID;
                        user.CreatedByText = AuthenticationHelper.User;
                        string passwordText = Util.CreateRandomPassword(10);
                        user.Password = Util.CalculateAESHash(passwordText);
                        string message = SendNotificationEmail(user, passwordText);

                        mstUser.CreatedBy = AuthenticationHelper.UserID;
                        mstUser.CreatedByText = AuthenticationHelper.User;
                        mstUser.Password = Util.CalculateAESHash(passwordText);
                        resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        if (resultValue > 0)
                        {
                            result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                            if (result == false)
                            {
                                UserManagement.deleteUser(resultValue);
                            }
                            if (result == true)
                            {

                                if (getproductCOMPLIANCE != -1)
                                {
                                    Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                    {
                                        UserID = (int)user.ID,
                                        CustomerID = customerID,
                                        ProductID = 1,
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        MgrID = null
                                    };
                                    result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                                }
                                if (getproductAudit != -1)
                                {
                                    Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                    {
                                        UserID = (int)user.ID,
                                        CustomerID = customerID,
                                        ProductID = 4,
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        MgrID = null
                                    };
                                    result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                                }
                                if (getproductSec != -1)
                                {
                                    Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                    {
                                        UserID = (int)user.ID,
                                        CustomerID = customerID,
                                        ProductID = 8,
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        MgrID = null
                                    };
                                    result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                                }
                                if (getproductHR != -1)
                                {
                                    Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                    {
                                        UserID = (int)user.ID,
                                        CustomerID = customerID,
                                        ProductID = 9,
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        MgrID = null
                                    };
                                    result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                                }

                                if (getproductPM != -1)
                                {
                                    Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                    {
                                        UserID = (int)user.ID,
                                        CustomerID = customerID,
                                        ProductID = 10,
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        MgrID = null
                                    };
                                    result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                                }

                                if (getproductPayroll != -1)
                                {
                                    Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                    {
                                        UserID = (int)user.ID,
                                        CustomerID = customerID,
                                        ProductID = 11,
                                        IsActive = true,
                                        CreatedOn = DateTime.Now,
                                        MgrID = null
                                    };
                                    result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                                }

                                widget swid = new widget()
                                {
                                    UserId = (Int32)user.ID,
                                    Performer = true,
                                    Reviewer = true,
                                    PerformerLocation = true,
                                    ReviewerLocation = true,
                                    DailyUpdate = true,
                                    NewsLetter = true,
                                    ComplianceSummary = true,
                                    FunctionSummary = true,
                                    RiskCriteria = true,
                                    EventOwner = true,
                                    PenaltySummary = true,
                                    TaskSummary = true,
                                    ReviewerTaskSummary = true,
                                    CustomWidget = false,
                                };
                                result = UserManagement.Create(swid);

                                if (result == false)
                                {
                                    UserManagement.deleteUser(resultValue);
                                    UserManagementRisk.deleteMstUser(resultValue);
                                }
                                if (result)
                                {
                                    CvDuplicateEntry1.IsValid = false;
                                    CvDuplicateEntry1.ErrorMessage = "User created successfully.";
                                }
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        User User = UserManagement.GetByID(Convert.ToInt32(user.ID));
                        if (tbxEmail.Text.Trim() != User.Email)
                        {
                            string message = SendNotificationEmailChanged(user);
                            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                        }


                        result = UserManagement.Update(user, parameters);
                        result = UserManagementRisk.Update(mstUser, parametersRisk);
                        if (result)
                        {
                            if (getproductCOMPLIANCE != -1)
                            {
                                Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                {
                                    UserID = (int)user.ID,
                                    CustomerID = customerID,
                                    ProductID = 1,
                                    IsActive = true,
                                    CreatedOn = DateTime.Now,
                                    MgrID = null
                                };
                                result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                            }
                            if (getproductAudit != -1)
                            {
                                Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                {
                                    UserID = (int)user.ID,
                                    CustomerID = customerID,
                                    ProductID = 4,
                                    IsActive = true,
                                    CreatedOn = DateTime.Now,
                                    MgrID = null
                                };
                                result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                            }
                            if (getproductSec != -1)
                            {
                                Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                {
                                    UserID = (int)user.ID,
                                    CustomerID = customerID,
                                    ProductID = 8,
                                    IsActive = true,
                                    CreatedOn = DateTime.Now,
                                    MgrID = null
                                };
                                result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                            }
                            if (getproductHR != -1)
                            {
                                Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                {
                                    UserID = (int)user.ID,
                                    CustomerID = customerID,
                                    ProductID = 9,
                                    IsActive = true,
                                    CreatedOn = DateTime.Now,
                                    MgrID = null
                                };
                                result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                            }

                            if (getproductPM != -1)
                            {
                                Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                {
                                    UserID = (int)user.ID,
                                    CustomerID = customerID,
                                    ProductID = 10,
                                    IsActive = true,
                                    CreatedOn = DateTime.Now,
                                    MgrID = null
                                };
                                result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                            }

                            if (getproductPayroll != -1)
                            {
                                Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                {
                                    UserID = (int)user.ID,
                                    CustomerID = customerID,
                                    ProductID = 11,
                                    IsActive = true,
                                    CreatedOn = DateTime.Now,
                                    MgrID = null
                                };
                                result = CreateUpdate_UserCustomerMapping(objUserCustMapping);
                            }

                            CvDuplicateEntry1.IsValid = false;
                            CvDuplicateEntry1.ErrorMessage = "User updated successfully.";
                        }
                    }
                    else
                    {
                        CvDuplicateEntry1.IsValid = false;
                        CvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                    #endregion limit               
                }
                else
                {
                    CvDuplicateEntry1.ErrorMessage = "Please select atleast one role";
                    CvDuplicateEntry1.IsValid = false;
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvDuplicateEntry1.IsValid = false;
                CvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    ;

                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }        
    }
}