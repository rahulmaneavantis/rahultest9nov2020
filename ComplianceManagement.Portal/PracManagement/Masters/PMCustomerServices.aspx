﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PracticeManagement.Master" AutoEventWireup="true" CodeBehind="PMCustomerServices.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters.PMCustomerServices" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style type="text/css">
        .aspNetDisabled {
            cursor: not-allowed;
        }

        .clearfix:after {
            clear: both;
            content: "";
            display: block;
            height: 0;
        }

        .pull-right {
            float: right;
        }

        .step > a {
            color: #333;
            text-decoration: none;
        }

        .step.current > a {
            color: white;
            text-decoration: none;
        }

        .step > a:active {
            color: #333;
            text-decoration: none;
        }

        .step > a:hover {
            color: white;
        }

        .step {
            clear: revert !important;
        }
        /* Breadcrups CSS */

        .arrow-steps .step {
            font-size: 14px;
            text-align: center;
            color: #666;
            cursor: default;
            margin: 0 3px;
            padding: 10px 10px 6px 30px;
            min-width: 10%;
            float: left;
            position: relative;
            background-color: #d9e3f7;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            transition: background-color 0.2s ease;
        }

            .arrow-steps .step:after,
            .arrow-steps .step:before {
                content: " ";
                position: absolute;
                top: 0;
                right: -17px;
                width: 0;
                height: 0;
                border-top: 19px solid transparent;
                border-bottom: 17px solid transparent;
                border-left: 17px solid #d9e3f7;
                z-index: 2;
                transition: border-color 0.2s ease;
            }

            .arrow-steps .step:before {
                right: auto;
                left: 0;
                border-left: 17px solid #fff;
                z-index: 0;
            }

            .arrow-steps .step:first-child:before {
                border: none;
            }

            .arrow-steps .step:last-child:after {
                border: none;
            }

            .arrow-steps .step:first-child {
                border-top-left-radius: 4px;
                border-bottom-left-radius: 4px;
            }

            .arrow-steps .step:last-child {
                border-top-right-radius: 4px;
                border-bottom-right-radius: 4px;
            }

            .arrow-steps .step span {
                position: relative;
            }

                .arrow-steps .step span:before {
                    opacity: 0;
                    content: "✔";
                    position: absolute;
                    top: -2px;
                    left: -20px;
                }

            .arrow-steps .step.done span:before {
                opacity: 1;
                -webkit-transition: opacity 0.3s ease 0.5s;
                -moz-transition: opacity 0.3s ease 0.5s;
                -ms-transition: opacity 0.3s ease 0.5s;
                transition: opacity 0.3s ease 0.5s;
            }

            .arrow-steps .step.current {
                color: #fff;
                background-color: #23468c;
            }

                .arrow-steps .step.current:after {
                    border-left: 17px solid #23468c;
                }

        .float-child {
            width: 84%;
            float: left;
        }

        .float-child1 {
            width: 10%;
            float: left;
        }
    </style>

    <script type="text/javascript">
        function fopenpopupPMCS() {
            $('#divcustserviceDialog').modal('show');
        };
        function fClosepopupPMCS() {
            $('#divcustserviceDialog').modal('hide');
        };

            function initializeConfirmDatePicker(date) {
            
            var startDate = new Date();
            $('#<%= tbxBillingDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            });
            initializeDatePicker();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/PracManagement/Masters/PracticeMGMTSetup.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity-Branch"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/PracManagement/Masters/PMUser_List.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnTax" PostBackUrl="~/PracManagement/Masters/PMTaxDetails.aspx" runat="server" Text="Tax Details"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/PracManagement/Masters/PMUserCustomerMapping.aspx" runat="server" Text="User Customer Assingment"></asp:LinkButton>
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/PracManagement/Masters/PMCustomerServices.aspx" runat="server" Text="Customer Services"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>


    <asp:UpdatePanel ID="upCSList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">
                        <section class="panel">
                        <div style="margin-bottom: 4px" />                      
                        <div class="col-md-12 colpadding0" style="margin-bottom: 5px;">
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-6 colpadding0" style="width:47%;">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                 <div class="col-md-6 colpadding0">
                                 <asp:DropDownListChosen runat="server" ID="ddlPageSize" Width="95%" AllowSingleDeselect="false" DisableSearchThreshold="2"
                                    CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownListChosen>
                                 </div>

                              
                            </div>
                          
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 35%">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px; margin-left: -10%;">Customer</p>
                                </div>
                                <div class="col-md-9 colpadding0">

                                    <asp:DropDownListChosen ID="ddlCustomerList" AllowSingleDeselect="false" runat="server" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged"
                                         DisableSearchThreshold="2" CssClass="form-control" Width="100%">
                                    </asp:DropDownListChosen>                                  
                                </div>
                            </div>
                              <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 35%">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Filter</p>
                                </div>
                                <div class="col-md-9 colpadding0" style="margin-left: -5%;">
                                    <asp:TextBox runat="server" ID="tbxFilter" class="form-control" Style="height: 32px;" MaxLength="50" AutoPostBack="true"
                                        OnTextChanged="tbxFilter_TextChanged" />
                                </div>
                            </div>
                            <div class="col-md-1 colpadding0 entrycount" style="margin-top: 5px; padding-left: 18px; float: right;">
                                <asp:LinkButton Text="Add New" runat="server" ID="btnAddTax" OnClick="btnAddTax_Click" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        </div>
                        <div style="margin-bottom: 4px;">                             
                            <asp:GridView runat="server" ID="grdCustomerServices" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                   PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"                              
                                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdCustomerServices_RowCommand" OnPageIndexChanging="grdCustomerServices_PageIndexChanging">
                                <Columns>                                    
                                    <asp:BoundField DataField="Entity" HeaderText="Entity Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="ServiceName" HeaderText="Service Name"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Frequency" HeaderText="Frequency"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Billing Date" HeaderText="Billing Date" />
                                   
                                    <asp:TemplateField ItemStyle-Width="110px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CommandName="EDIT_USER" ID="lbtnEdit" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png" alt="Edit User" title="Edit User" /></asp:LinkButton>

                                        </ItemTemplate>
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <HeaderStyle BackColor="#ECF0F1" />
                                <PagerSettings Visible="false" />
                                <PagerTemplate>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <div style="float: right;">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                    class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page                                          
                                        </p>
                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                         </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="modal fade" id="divcustserviceDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upCS" runat="server" UpdateMode="Conditional" OnLoad="upCS_Load">
                        <ContentTemplate>
                            <div style="margin: 5px">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="CSValidationGroup" />
                                    <asp:CustomValidator ID="CvDuplicateEntry1" runat="server" EnableClientScript="False"
                                        ValidationGroup="CSValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Entity</label>
                                      <asp:DropDownListChosen runat="server" ID="ddlCustomerPage" Width="64%" AllowSingleDeselect="false" DisableSearchThreshold="2"
                                    CssClass="form-control" AutoPostBack="true" >
                                </asp:DropDownListChosen>
                                    <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Please select entity." ControlToValidate="ddlCustomerPage"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CSValidationGroup"
                                        Display="None" />
                                </div>                               
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                       Service Name</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlServiceName" Width="64%" AllowSingleDeselect="false" DisableSearchThreshold="2"
                                    CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlServiceName_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                                   <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select service name." ControlToValidate="ddlServiceName"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CSValidationGroup"
                                        Display="None" />
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Frequency</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlFrequency" Width="64%" AllowSingleDeselect="false" DisableSearchThreshold="2"
                                    CssClass="form-control" AutoPostBack="true" >
                                </asp:DropDownListChosen>
                                   <asp:CompareValidator ID="CompareValidator5" ErrorMessage="Please select frequency." ControlToValidate="ddlFrequency"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CSValidationGroup"
                                        Display="None" />
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Billing Date</label>
                                    <asp:TextBox runat="server" ID="tbxBillingDate" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage=" Billing Date can not be empty."
                                        ControlToValidate="tbxBillingDate" runat="server" ValidationGroup="CSValidationGroup"
                                        Display="None" />                                   
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Service Fees</label>
                                    <asp:TextBox runat="server" ID="tbxServiceFees" Style="width: 390px;" CssClass="form-control"
                                         />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Mobile Number can not be empty."
                                        ControlToValidate="tbxServiceFees" runat="server" ValidationGroup="CSValidationGroup"
                                        Display="None" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                        ValidationGroup="CSValidationGroup" ErrorMessage="Please enter a valid Mobile number."
                                        ControlToValidate="tbxServiceFees" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxServiceFees" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                        ValidationGroup="CSValidationGroup" ErrorMessage="Please enter only 10 digit."
                                        ControlToValidate="tbxServiceFees" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                </div>                                                                
                                <div style="margin-bottom: 7px; float: right; margin-right: 257px; margin-top: 10px;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" CausesValidation="true"
                                        ValidationGroup="CSValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" OnClientClick="fClosepopupPMCS()" />
                                </div>                                
                            </div>
                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                            </div>
                             <div class="clearfix" style="height: 50px">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
