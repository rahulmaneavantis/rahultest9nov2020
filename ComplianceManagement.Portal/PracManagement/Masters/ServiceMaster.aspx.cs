﻿
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters
{
    public partial class ServiceMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    {
                        BindCustomers();
                        BindStates();
                        BindFrequency();
                        bindPageNumber();
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
        private void BindCustomers()
        {
            try
            {
                int customerID = 1;
                var customerData = GetAll(customerID, tbxFilter.Text);
                Session["TotalRows"] = customerData.Count;
                grdCustomer.DataSource = customerData;
                grdCustomer.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<TM_tbl_ServiceCode> GetAllStates()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var states = (from row in entities.TM_tbl_ServiceCode
                              orderby row.ServiceDetails ascending
                              select row).ToList();

                return states.OrderBy(entry => entry.ServiceDetails).ToList();
            }
        }
        public static List<TM_tbl_Frequency> GetAllFrequency()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var states = (from row in entities.TM_tbl_Frequency                              
                              select row).ToList();

                return states.OrderBy(entry => entry.FrequencyName).ToList();
            }
        }
        protected void btnAddCustomerBranch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxName.Text  = tbxName.Text  = string.Empty;
                ddlServiceCode.SelectedValue = "-1";
                ddlServiceCode_SelectedIndexChanged(null, null);
                upCustomerBranches.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopup()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindFrequency()
        {
            try
            {
                ddlFrequency.DataTextField = "FrequencyName";
                ddlFrequency.DataValueField = "FrequencyId";
                ddlFrequency.DataSource = GetAllFrequency();
                ddlFrequency.DataBind();
                ddlFrequency.Items.Insert(0, new ListItem(" Select ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindStates()
        {
            try
            {
                ddlServiceCode.DataTextField = "ServiceCode";
                ddlServiceCode.DataValueField = "ID";
                ddlServiceCode.DataSource = GetAllStates();
                ddlServiceCode.DataBind();
                ddlServiceCode.Items.Insert(0, new ListItem(" Select ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void EditCustomerInformation(int firmid)
        {
            try
            {
                using (PracticeEntities entities = new PracticeEntities())
                {
                    var customerData = (from row in entities.TM_Sp_EditServiceDetails(firmid)                                       
                                        select row).FirstOrDefault();

                    if (customerData != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(customerData.ServiceCodeId)))
                        {
                            if (customerData.ServiceCodeId != -1)
                            {
                                ddlServiceCode.SelectedValue = customerData.ServiceCodeId.ToString();

                                int scodeid = Convert.ToInt32(ddlServiceCode.SelectedValue);
                                txbservicecodedetails.Text = string.Empty;
                                
                                    txbservicecodedetails.Text = customerData.ServiceDetails;
                                
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(customerData.Frequency)))
                        {
                            if (customerData.Frequency != -1)
                            {
                                ddlFrequency.SelectedValue = customerData.Frequency.ToString();
                            }
                        }
                       
                        tbxName.Text = customerData.ServiceName;
                    }
                }
                upCustomerBranches.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopup()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<TM_tbl_ServiceSchedule> GetScheduleByComplianceID(long ServiceID)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var scheduleList = (from row in entities.TM_tbl_ServiceSchedule
                                    where row.ServiceID == ServiceID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }

        public static void GenerateDefaultScheduleForComplianceID(long serviceID, byte frequency, bool deleteOldData = false)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {

                if (deleteOldData)
                {
                    var ids = (from row in entities.TM_tbl_ServiceSchedule
                               where row.ServiceID == serviceID
                               select row.ID).ToList();
                    ids.ForEach(entry =>
                    {
                        var schedule = (from row in entities.TM_tbl_ServiceSchedule
                                        where row.ID == entry
                                        select row).FirstOrDefault();
                        entities.TM_tbl_ServiceSchedule.Remove(schedule);
                    });
                    entities.SaveChanges();
                }

                var compliance = (from row in entities.TM_tbl_ServiceDetails
                                  where row.ID == serviceID
                                  select row).First();
                int step = 1;
                switch ((Frequency)frequency)
                {
                    case Frequency.Quarterly:
                        step = 3;
                        break;
                    case Frequency.FourMonthly:
                        step = 4;
                        break;
                    case Frequency.HalfYearly:
                        step = 6;
                        break;
                    case Frequency.Annual:
                        step = 12;
                        break;
                    case Frequency.TwoYearly:
                        step = 12;
                        break;
                    case Frequency.SevenYearly:
                        step = 12;
                        break;
                }
                byte startMonth = 4;
                if (frequency == 0)
                {
                    startMonth = Convert.ToByte(1);
                }
                else if (frequency == 1)
                {
                    startMonth = Convert.ToByte(1);
                }
                int SpecialMonth;
                for (int month = startMonth; month <= 12; month += step)
                {
                    TM_tbl_ServiceSchedule complianceShedule = new TM_tbl_ServiceSchedule();
                    complianceShedule.ServiceID = serviceID;
                    complianceShedule.ForMonth = month;


                    SpecialMonth = month;
                    if (frequency == 0 && SpecialMonth == 12)
                    {
                        SpecialMonth = 1;
                    }
                    else if (frequency == 1 && SpecialMonth == 10)
                    {
                        SpecialMonth = 1;
                    }
                    else if (frequency == 2 && SpecialMonth == 7)
                    {
                        SpecialMonth = 1;
                    }
                    else if (frequency == 2 && SpecialMonth == 10)
                    {
                        SpecialMonth = 4;
                    }
                    else if (frequency == 4 && SpecialMonth == 9)
                    {
                        SpecialMonth = 1;
                    }
                    else if (frequency == 4 && SpecialMonth == 12)
                    {
                        SpecialMonth = 4;
                    }
                    else if ((frequency == 3 || frequency == 5 || frequency == 6) && SpecialMonth == 4)
                    {
                        SpecialMonth = 4;
                    }
                    else
                    {
                        SpecialMonth = SpecialMonth + step;
                    }

                    int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);

                    if (Convert.ToInt32(1.ToString("D2")) > lastdate)
                    {
                        complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                    }
                    else
                    {
                        complianceShedule.SpecialDate = 1.ToString("D2") + SpecialMonth.ToString("D2");
                    }
                    entities.TM_tbl_ServiceSchedule.Add(complianceShedule);
                }
                entities.SaveChanges();
            }
        }       
        protected bool ViewSchedule(object frequency)
        {
            try
            {
                if (Convert.ToString(frequency) == "4")
                {
                    return false;
                }  
                else
                {
                    return true;
                }                                            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }
        private void OpenScheduleInformation(TM_tbl_ServiceDetails compliance)
        {
            try
            {
                
                ViewState["serviceID"] = compliance.ID;
                ViewState["Frequency"] = compliance.Frequency.Value;
                
                var scheduleList = GetScheduleByComplianceID(compliance.ID);
                if (scheduleList.Count == 0)
                {
                    GenerateDefaultScheduleForComplianceID(compliance.ID, (byte)compliance.Frequency);                   
                    scheduleList = GetScheduleByComplianceID(compliance.ID);
                }
                int step = 0;
                if (compliance.Frequency.Value == 0)
                    step = 0;
                else if (compliance.Frequency.Value == 1)
                    step = 2;
                else if (compliance.Frequency.Value == 2)
                    step = 5;
                else if (compliance.Frequency.Value == 4)
                    step = 3;
                else
                    step = 11;

                var dataSource = scheduleList.Select(entry => new
                {
                    ID = entry.ID,
                    ForMonth = entry.ForMonth,
                    ForMonthName = compliance.Frequency.Value == 0 ? ((Month)entry.ForMonth).ToString() : ((Month)entry.ForMonth).ToString() + " - " + ((Month)((entry.ForMonth + step) > 12 ? (entry.ForMonth + step) - 12 : (entry.ForMonth + step))).ToString(),
                    SpecialDay = Convert.ToByte(entry.SpecialDate.Substring(0, 2)),
                    SpecialMonth = Convert.ToByte(entry.SpecialDate.Substring(2, 2))
                }).ToList();               
                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();

                upSchedulerRepeter.Update();
                upComplianceScheduleDialog.Update();
                upCustomerBranches.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopup1()", true);                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //saveopo.Value = "true";
                var dataSource = new List<object>();
                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");


                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = Convert.ToByte(hdnForMonth.Value),
                        ForMonthName = ((Month)Convert.ToByte(hdnForMonth.Value)).ToString(),
                        SpecialDay = ddlDays.SelectedValue,
                        SpecialMonth = ddlMonths.SelectedValue
                    });

                    Month month = (Month)Convert.ToInt32(ddlMonths.SelectedValue);
                    int totalDays = 0;
                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var daysdataSource = new List<object>();

                    for (int j = 1; j <= totalDays; j++)
                    {
                        daysdataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = daysdataSource;
                    ddlDays.DataBind();
                    upSchedulerRepeter.Update();
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static void UpdateScheduleInformation(List<TM_tbl_ServiceSchedule> scheduleList)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                scheduleList.ForEach(schedule =>
                {                  
                    TM_tbl_ServiceSchedule complianceScheduleToUpdate = (from row in entities.TM_tbl_ServiceSchedule
                                                                         where row.ID == schedule.ID
                                                                     select row).FirstOrDefault();

                    complianceScheduleToUpdate.ServiceID = schedule.ServiceID;
                    complianceScheduleToUpdate.ForMonth = schedule.ForMonth;
                    complianceScheduleToUpdate.SpecialDate = schedule.SpecialDate;
                });
                entities.SaveChanges();
            }
        }
        protected void btnSaveSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["serviceID"].ToString() != null && ViewState["serviceID"].ToString() != "")
                {


                    List<TM_tbl_ServiceSchedule> scheduleList = new List<TM_tbl_ServiceSchedule>();

                    for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                    {
                        RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                        HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                        HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                        DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                        DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");

                        scheduleList.Add(new TM_tbl_ServiceSchedule()
                        {
                            ID = Convert.ToInt32(hdnID.Value),
                            ServiceID = Convert.ToInt64(ViewState["serviceID"]),
                            ForMonth = Convert.ToInt32(hdnForMonth.Value),
                            SpecialDate = string.Format("{0}{1}", Convert.ToByte(ddlDays.SelectedValue).ToString("D2"), Convert.ToByte(ddlMonths.SelectedValue).ToString("D2"))
                        });
                    }
                    UpdateScheduleInformation(scheduleList);
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:fClosepopup1()", true);
                    upSchedulerRepeter.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }    
        protected void repComplianceSchedule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    DropDownList ddlDays = (DropDownList)e.Item.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)e.Item.FindControl("ddlMonths");
                    ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterAsyncPostBackControl(ddlMonths);
                    }
                    ddlMonths.DataSource = Enumerations.GetAll<Month>();
                    ddlMonths.DataBind();
                    int totalDays = 28;
                    int day = Convert.ToInt16(Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialDay").GetValue(e.Item.DataItem, null).ToString()));
                    Month month = (Month)Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialMonth").GetValue(e.Item.DataItem, null).ToString());
                    ddlMonths.SelectedValue = ((byte)month).ToString();
                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var dataSource = new List<object>();
                    for (int i = 1; i <= totalDays; i++)
                    {
                        dataSource.Add(new { ID = i, Name = i.ToString() });
                    }
                    ddlDays.DataSource = dataSource;
                    ddlDays.DataBind();
                    if (day > totalDays)
                    {
                        day = totalDays;
                    }
                    ddlDays.SelectedValue = day.ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int serviceID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_CUSTOMER"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["serviceID"] = serviceID;
                    EditCustomerInformation(serviceID);
                }
                if (e.CommandName.Equals("SHOW_SCHEDULE"))
                {
                    ViewState["serviceID"] = serviceID;
                    var compliance = GetByID(serviceID);
                    OpenScheduleInformation(compliance);
                }
                else if (e.CommandName.Equals("VIEW_COMPANIES"))
                {
                    Session["serviceID"] = serviceID;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static TM_tbl_ServiceDetails GetByID(long serviceID)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var compliance = (from row in entities.TM_tbl_ServiceDetails
                                  where row.ID == serviceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }
        protected void grdCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = e.NewPageIndex;
                BindCustomers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = 0;
                BindCustomers();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        public static List<TM_Sp_ServiceDetails_Result> GetAll(int customerID,string filter = null)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var users = (from row in entities.TM_Sp_ServiceDetails()                             
                             select row);               
                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.ServiceName.Contains(filter));
                }
                return users.OrderBy(entry => entry.ServiceName).ToList();
            }
        }
        protected void grdCustomer_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = 1;
                var customerlist = GetAll(customerID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    customerlist = customerlist.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    customerlist = customerlist.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }
                foreach (DataControlField field in grdCustomer.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCustomer.Columns.IndexOf(field);
                    }
                }
                grdCustomer.DataSource = customerlist;
                grdCustomer.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCustomer_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdCustomer.PageIndex = chkSelectedPage - 1;
                grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCustomers();
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCustomers();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCustomer.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void ddlServiceCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlServiceCode.SelectedValue))
            {
                if (ddlServiceCode.SelectedValue.ToString() != "-1")
                {
                    int scodeid = Convert.ToInt32(ddlServiceCode.SelectedValue);
                    using (PracticeEntities entities = new PracticeEntities())
                    {
                        txbservicecodedetails.Text = string.Empty;
                        var states = (from row in entities.TM_tbl_ServiceCode
                                      where row.ID == scodeid
                                      select row).FirstOrDefault();
                        if (states != null)
                        {
                            txbservicecodedetails.Text = states.ServiceDetails;
                        }
                    }
                }
            }
        }
        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {           
            if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
            {
                if (ddlFrequency.SelectedValue.ToString() != "-1")
                {
                }
            }
        }        
        public static int Create(TM_tbl_ServiceDetails mst_CustomerBranch)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                try
                {
                    mst_CustomerBranch.IsActive = false;
                    mst_CustomerBranch.CreatedOn = DateTime.UtcNow;                    
                    entities.TM_tbl_ServiceDetails.Add(mst_CustomerBranch);
                    entities.SaveChanges();
                    return 1;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
        public static bool Exists(TM_tbl_ServiceDetails customerBranch)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var query = (from row in entities.TM_tbl_ServiceDetails
                             where row.IsActive == false
                             && row.ServiceName.Equals(customerBranch.ServiceName) && row.ServiceCodeId == customerBranch.ServiceCodeId
                             select row);

                if (customerBranch.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customerBranch.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int resultData = 0;
                int comType = 0;
                if (!string.IsNullOrEmpty(ddlServiceCode.SelectedValue))
                {
                    if (ddlServiceCode.SelectedValue.ToString() == "-1")
                    {
                        comType = 0;
                    }
                    else
                    {
                        comType = Convert.ToInt32(ddlServiceCode.SelectedValue);
                    }
                }
                int fType = -1;
                if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
                {
                    if (ddlFrequency.SelectedValue.ToString() != "-1")
                    {
                        fType = Convert.ToInt32(ddlFrequency.SelectedValue);
                    }                    
                }
                #region Firm Update
                TM_tbl_ServiceDetails customerBranch = new TM_tbl_ServiceDetails()
                {                    
                    ServiceName = tbxName.Text,
                    ServiceCodeId = Convert.ToByte(comType),
                    CreatedBy = AuthenticationHelper.UserID,
                    Frequency= fType,
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    customerBranch.ID = Convert.ToInt32(ViewState["serviceID"]);
                }
                if ((int)ViewState["Mode"] == 0)/////////***************For Save*********************//
                {
                    if (Exists(customerBranch))
                    {
                        cvDuplicateEntry.ErrorMessage = "Service Name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    resultData = Create(customerBranch);
                  
                }
                else if ((int)ViewState["Mode"] == 1)/////////////////**************For Update****************///////////////
                {
                    resultData = UpdateCustBranch(customerBranch);            
                }                
                resultData = UpdateCustBranch(customerBranch);
                if (resultData < 0)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                else
                {
                    GenerateDefaultScheduleForComplianceID(customerBranch.ID, (byte)customerBranch.Frequency);
                }
                #endregion
                BindCustomers();
                bindPageNumber();
                upCustomerBranchList.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:fClosepopup()", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static int UpdateCustBranch(TM_tbl_ServiceDetails customerBranch)
        {
            try
            {
                using (PracticeEntities entities = new PracticeEntities())
                {
                    TM_tbl_ServiceDetails customerBranchToUpdate = (from row in entities.TM_tbl_ServiceDetails
                                                                    where row.ID == customerBranch.ID && row.IsActive == false
                                                            select row).FirstOrDefault();

                    customerBranchToUpdate.ServiceName = customerBranch.ServiceName;                    
                    customerBranchToUpdate.ServiceCodeId = customerBranch.ServiceCodeId;                    
                    entities.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void upCustomerBranches_Load(object sender, EventArgs e)
        {
        }

        protected void upCustomerBranchList_Load(object sender, EventArgs e)
        {
        }
    }
}