﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters
{
    public partial class PracticeMGMTSetup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    {
                        Session["CustomerID"] = null;
                        BindGroupNameCustomers();
                        BindCustomers();
                        ChkSp();
                        BindCustomerStatus();                     
                        BindServiceProvider();
                        BindGrid();
                        bindPageNumber();
                        BindRegistrationBy();

                        int customerID = -1;                       
                        if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                        {
                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                        else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "DADMN")
                        {                           
                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                        else
                        {
                            var aa = UserManagement.GetByID(AuthenticationHelper.UserID).IsAuditHeadOrMgr ?? null;
                            if (aa == "AM" || aa == "AH")
                            {
                                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            }
                        }
                        if (customerID != -1)
                        {
                            bool serviceProviderId = checkISServiseProvider(customerID);
                            if (serviceProviderId)
                            {
                                bool CheckUserLimit = ICIAManagement.CheckCustomerLimitCompliance(Convert.ToInt32(customerID));
                                if (!CheckUserLimit)
                                {
                                    btnAddCustomer.Enabled = false;
                                    btnAddCustomer.ToolTip = "Facility to create more customers is not available in the Free version. Kindly contact Avantis to activate the same.";
                                }
                            }
                        }

                        txtProductType.Text = "< Select >";
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'dvProduct');", txtProductType.ClientID), true);                        
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
        public bool checkISServiseProvider(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ServiceproviderId = false;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select (bool)cust.IsDistributor).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }
        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();

                if (ddlCustomerStatus.Items.FindByText("Suspended") != null)
                    ddlCustomerStatus.Items.Remove(ddlCustomerStatus.Items.FindByText("Suspended"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                isGroupname.Visible = false;
                if (ddlGroupName.SelectedValue=="0")
                {
                    isGroupname.Visible = true;
                }                              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void ddlCustomerlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomers()
        {
            try
            {
                int distributorID = -1;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                ddlCustomerlist.DataTextField = "Name";
                ddlCustomerlist.DataValueField = "ID";

                var data = PTCustomerManagement.GetAllCustomers(customerID, AuthenticationHelper.Role, distributorID, tbxFilter.Text);
                ddlCustomerlist.DataSource = data;
                ddlCustomerlist.DataBind();
                ddlCustomerlist.Items.Insert(0, new ListItem("< Select Customer >", "-1"));              
                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomerlist.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)) != null)
                    {
                        ddlCustomerlist.ClearSelection();
                        ddlCustomerlist.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindGroupNameCustomers()
        {
            try
            {
                int distributorID = -1;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                ddlGroupName.DataTextField = "Name";
                ddlGroupName.DataValueField = "ID";

                var data = PTCustomerManagement.GetAllGroupCustomers(distributorID);
                ddlGroupName.DataSource = data;
                ddlGroupName.DataBind();
                ddlGroupName.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                ddlGroupName.Items.Insert(1, new ListItem("Other", "0"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        

        public void BindServiceProvider()
        {
            try
            {
                int serviceProviderID = -1;
                int distributorID = -1;
                int IsSPDist = 2;
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    IsSPDist = 0;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    IsSPDist = 1;
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    int? serProID = PTCustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));

                    if (serProID != null)
                        serviceProviderID = Convert.ToInt32(serProID);

                    IsSPDist = 0;
                }                              
                var data = PTCustomerManagement.Fill_ServiceProviders_Distributors(IsSPDist, serviceProviderID, distributorID);               
                ddlSPName.Items.Clear();
                ddlSPName.DataTextField = "Name";
                ddlSPName.DataValueField = "ID";
                ddlSPName.DataSource = data;
                ddlSPName.DataBind();
                ddlSPName.Items.Insert(0, new ListItem("< Select Service Provider >", "-1"));                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdCustomer.PageIndex = chkSelectedPage - 1;
                grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCustomer.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["CMTotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void BindGrid()
        {
            try
            {
                //if (!string.IsNullOrEmpty(ddlSPList.SelectedValue) && ddlSPList.SelectedValue != "-1")
                //{
                int customerID = -1;
                int serviceproviderID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "DADMN")
                {
                    serviceproviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = -1;
                }
                else
                {
                    var aa = UserManagement.GetByID(AuthenticationHelper.UserID).IsAuditHeadOrMgr ?? null;
                    if (aa == "AM" || aa == "AH")
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                }

                var customerData = PTCustomerManagement.GetAll_CustomersByServiceProviderOrDistributor(customerID, serviceproviderID, tbxFilter.Text);


                if (!string.IsNullOrEmpty(ddlCustomerlist.SelectedValue) && ddlCustomerlist.SelectedValue != "-1")
                {
                    int cid = Convert.ToInt32(ddlCustomerlist.SelectedValue);
                    customerData = customerData.Where(entry => entry.ID == cid).ToList();
                }
                List<object> dataSource = new List<object>();
                foreach (var customerInfo in customerData)
                {
                    dataSource.Add(new
                    {
                        customerInfo.ID,
                        customerInfo.Name,
                        customerInfo.Address,
                        customerInfo.Industry,
                        customerInfo.BuyerName,
                        customerInfo.BuyerContactNumber,
                        customerInfo.BuyerEmail,
                        customerInfo.CreatedOn,
                        customerInfo.IsDeleted,
                        customerInfo.StartDate,
                        customerInfo.EndDate,
                        customerInfo.DiskSpace,
                        customerInfo.CustomerGroupName,
                        Status = Enumerations.GetEnumByID<CustomerStatus>(Convert.ToInt32(customerInfo.Status != null ? (int)customerInfo.Status : -1)),
                    });
                }

                Session["CMTotalRows"] = dataSource.Count;
                grdCustomer.DataSource = dataSource;
                grdCustomer.DataBind();

                upCustomerList.Update();
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<long> GetProductData(int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DeptIDs = (from row in entities.ProductMappings
                               where row.CustomerID == customerid && row.IsActive == false
                               select (long)row.ProductID).ToList();

                return DeptIDs;
            }
        }
        public void EditCustomerInformation(int customerID)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'dvProduct');", txtProductType.ClientID), true);                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
                lblErrorMassage.Text = "";
                ViewState["Mode"] = 1;
                ViewState["CustomerID"] = customerID;
                tbxpan.Text = tbxTan.Text = tbxEmailInvoiceSahring.Text = tbxGroupName.Text = string.Empty;
                tbxName.Text = tbxAddress.Text = tbxBuyerName.Text = tbxBuyerContactNo.Text = tbxBuyerEmail.Text = string.Empty;
                Customer customer = CustomerManagement.GetByID(customerID);
                tbxName.Text = customer.Name;
                tbxAddress.Text = customer.Address;
                tbxBuyerName.Text = customer.BuyerName;
                tbxBuyerContactNo.Text = customer.BuyerContactNumber;
                tbxBuyerEmail.Text = customer.BuyerEmail;
                tbxGstNumber.Text = customer.GSTNum;
                tbxpan.Text = customer.Pan  ;
                tbxTan.Text =  customer.Tan  ;
                tbxEmailInvoiceSahring.Text = customer.EmailInvoiceSharing  ;
                tbxGroupName.Text = customer.CustomerGroupName  ;


                if (customer.Status != null)
                {
                    ddlCustomerStatus.SelectedValue = Convert.ToString(customer.Status);
                }

                #region Product    
                if (customer.ServiceProviderID == 0 || customer.ServiceProviderID == null)
                {
                    BindProductType(-1, false);
                }
                else
                {
                    BindProductType((int)customer.ServiceProviderID, true);
                }

                var vGetProductMappedIDs = GetProductData(customerID);
                foreach (RepeaterItem aItem in rptProductType.Items)
                {
                    CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                    chkProduct.Checked = false;
                    CheckBox ProductSelectAll = (CheckBox)rptProductType.Controls[0].Controls[0].FindControl("ProductSelectAll");

                    for (int i = 0; i <= vGetProductMappedIDs.Count - 1; i++)
                    {
                        if (((Label)aItem.FindControl("lblProductID")).Text.Trim() == vGetProductMappedIDs[i].ToString())
                        {
                            chkProduct.Checked = true;
                        }
                    }
                    if ((rptProductType.Items.Count) == (vGetProductMappedIDs.Count))
                    {
                        ProductSelectAll.Checked = true;
                    }
                    else
                    {
                        ProductSelectAll.Checked = false;
                    }
                }
                #endregion
                if (customer.IsServiceProvider == true)
                {
                    chkSp.Checked = true;
                    ChkSp();
                    customer.ServiceProviderID = null;
                }
                else
                {
                    BindServiceProvider();
                    chkSp.Checked = false;
                    ChkSp();
                    ddlSPName.SelectedValue = Convert.ToString(customer.ServiceProviderID);
                }
                upCustomers.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopupPMC()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<ProductMappingDetails> GetAllProductMapping(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var productmapping = (from p in entities.Products
                                      join pm in entities.ProductMappings
                                      on p.Id equals pm.ProductID
                                      join c in entities.Customers
                                      on pm.CustomerID equals c.ID
                                      where pm.IsActive == false
                                      select new ProductMappingDetails()
                                      {
                                          ProductID = p.Id,
                                          CustomerId = c.ID,
                                          ProductName = p.Name,
                                          CustomerName = c.Name
                                      });

                if (customerID != -1)
                {
                    productmapping = productmapping.Where(entry => entry.CustomerId == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    productmapping = productmapping.Where(entry => entry.ProductName.Contains(filter) || entry.CustomerName.Contains(filter));
                }
                return productmapping.OrderBy(entry => entry.ProductName).ToList();
            }
        }
        public void AddCustomer()
        {
            try
            {                             
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lblErrorMassage.Text = string.Empty;
                    ViewState["Mode"] = 0;
                    chkSp.Checked = false;
                    chkSp_CheckedChanged(null, null);


                    tbxpan.Text = tbxTan.Text = tbxEmailInvoiceSahring.Text= tbxGroupName.Text = string.Empty;
                    tbxName.Text = tbxAddress.Text = tbxBuyerName.Text = tbxBuyerContactNo.Text = tbxBuyerEmail.Text =  string.Empty;
                    ddlCustomerStatus.SelectedIndex = -1;
                    txtProductType.Text = "< Select >";

                    var ParentBranchLimitcount = (from row in entities.Customers
                                                  join row1 in entities.Users
                                                  on row.ID equals row1.CustomerID
                                                  where row1.ID == AuthenticationHelper.UserID
                                                  select row).FirstOrDefault();

                    ddlSPName.SelectedIndex = -1;
                    if (ParentBranchLimitcount != null)
                    {
                        if (ParentBranchLimitcount.IsServiceProvider == true)
                        {
                            ddlSPName.SelectedValue = Convert.ToString(ParentBranchLimitcount.ID);
                        }
                    }
                    ddlRegistrationBy.ClearSelection();

                    if (AuthenticationHelper.Role.Equals("IMPT"))
                    {
                        issprd.Visible = true;
                    }
                    foreach (RepeaterItem aItem in rptProductType.Items)
                    {
                        CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                        chkProduct.Checked = false;
                        CheckBox ProductSelectAll = (CheckBox)rptProductType.Controls[0].Controls[0].FindControl("ProductSelectAll");
                        ProductSelectAll.Checked = false;
                    }
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'dvProduct');", txtProductType.ClientID), true);                    
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopupPMC()", true);                   
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
                    upCustomers.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_CUSTOMER"))
                {
                    // TBD
                    EditCustomerInformation(customerID);
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER"))
                {
                    if (PTCustomerManagement.CustomerDelete(customerID))
                    {
                        PTCustomerManagement.Delete(customerID);
                        PTCustomerManagement.AuditDelete(customerID);
                        BindGrid();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('Customer is associated with assigned compliance, can not be deleted')", true);
                    }
                }
                else if (e.CommandName.Equals("VIEW_COMPANIES"))
                {
                    Session["CustomerID"] = customerID;
                    Session["ParentID"] = null;
                    Response.Redirect("~/PracManagement/Masters/PMEntityBranch.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {               
                grdCustomer.PageIndex = e.NewPageIndex;
                BindGrid();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddCustomer_Click(object sender, EventArgs e)
        {          
            AddCustomer();           
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = 0;
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_OnPreRender(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role.Equals("CADMN"))
                {
                    foreach (DataControlField column in grdCustomer.Columns)
                    {
                        if (column.HeaderText == "")
                            column.Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCustomer_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                var customerlist = PTCustomerManagement.GetAll(customerID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    customerlist = customerlist.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    customerlist = customerlist.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdCustomer.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCustomer.Columns.IndexOf(field);
                    }
                }

                grdCustomer.DataSource = customerlist;
                grdCustomer.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }


        protected void chkSp_CheckedChanged(object sender, EventArgs e)
        {
            ChkSp();
            BindProductType(-1, false);
        }
        public static object FillProduct(int serviceproviderid, bool istrue)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (istrue)
                {
                    var query = (from row in entities.Products
                                 join row1 in entities.ProductMappings
                                 on row.Id equals row1.ProductID
                                 where row1.CustomerID == serviceproviderid
                                 select row);


                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();
                    return users;

                }
                else
                {
                    var query = (from row in entities.Products
                                 select row);

                    var users = (from row in query
                                 select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();
                    return users;

                }
            }
        }
        public void BindProductType(int servicepid, bool isTrue)
        {
            try
            {
                //int customerID;
                //try
                //{
                //    lstBoxProduct.DataTextField = "ProductName";
                //    lstBoxProduct.DataValueField = "ProductID";
                //    if (servicepid != null)
                //    {
                //        customerID = Convert.ToInt32(servicepid);
                //    }
                //    else
                //    {
                //        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                //    }
                //    //   lstBoxProduct.DataSource = CommanClass.FillProduct();
                //    lstBoxProduct.DataSource = CommanClass.GetAllProductMapping(customerID);
                //    lstBoxProduct.DataBind();
                //}
                //catch (Exception ex)
                //{
                //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //    cvDuplicateEntry.IsValid = false;
                //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                //}

                if (isTrue)
                {

                    var data= FillProduct(Convert.ToInt32(servicepid), isTrue);
                  
                    rptProductType.DataSource = data;
                    rptProductType.DataBind();
                    foreach (RepeaterItem aItem in rptProductType.Items)
                    {
                        CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                        if (!chkProduct.Checked)
                        {
                            chkProduct.Checked = true;
                        }
                    }
                    CheckBox ProductSelectAll = (CheckBox)rptProductType.Controls[0].Controls[0].FindControl("ProductSelectAll");
                    ProductSelectAll.Checked = true;
                }
                else
                {
                    rptProductType.DataSource = FillProduct(-1, isTrue);
                    rptProductType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }   
        public void ChkSp()
        {
            if (chkSp.Checked == true)
            {

                lblast.Visible = false;
                lblsp.Visible = false;
                ddlSPName.Visible = false;

                lblRegAst.Visible = true;
                lblRegBy.Visible = true;
                ddlRegistrationBy.Visible = true;
            }
            else
            {
                lblast.Visible = true;
                lblsp.Visible = true;
                ddlSPName.Visible = true;

                lblRegAst.Visible = false;
                lblRegBy.Visible = false;
                ddlRegistrationBy.Visible = false;
            }
        }
      
        protected void ddlSPName_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (!string.IsNullOrEmpty(ddlSPName.SelectedValue))
            {
                if (ddlSPName.SelectedValue != "-1")
                {
                    BindProductType(Convert.ToInt32(ddlSPName.SelectedValue), true);
                    bool CheckCustomerLimit = true;
                    CheckCustomerLimit = ICIAManagement.CheckCustomerLimitCompliance(Convert.ToInt32(ddlSPName.SelectedValue));
                    if (CheckCustomerLimit)
                    {                      
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Facility to create more customers is not available in the Free version. Kindly contact Avantis to activate the same.";
                    }
                }
            }
        }
        public void BindRegistrationBy()
        {
            try
            {
                ddlRegistrationBy.DataTextField = "Name";
                ddlRegistrationBy.DataValueField = "ID";

                ddlRegistrationBy.Items.Clear();

                ddlRegistrationBy.DataSource = CustomerManagement.Fill_RegistrationBy();
                ddlRegistrationBy.DataBind();

                ddlRegistrationBy.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upCustomers_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'dvProduct');", txtProductType.ClientID), true);
                //ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "initializeJQueryUI", "initializeJQueryUI();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region 
        public class Compliance
        {
            public object filingFrequency { get; set; }
        }

        public class Addr
        {
            public string st { get; set; }
            public string lg { get; set; }
            public string stcd { get; set; }
            public string pncd { get; set; }
            public string bno { get; set; }
            public string flno { get; set; }
            public string dst { get; set; }
            public string loc { get; set; }
            public string bnm { get; set; }
            public string lt { get; set; }
            public string city { get; set; }
        }

        public class Pradr
        {
            public string ntr { get; set; }
            public Addr addr { get; set; }
        }

        public class TaxpayerInfo
        {
            public object errorMsg { get; set; }
            public string ctj { get; set; }
            public Pradr pradr { get; set; }
            public string cxdt { get; set; }
            public string sts { get; set; }
            public string gstin { get; set; }
            public string ctjCd { get; set; }
            public string ctb { get; set; }
            public string stj { get; set; }
            public string dty { get; set; }
            public List<object> adadr { get; set; }
            public object frequencyType { get; set; }
            public string lgnm { get; set; }
            public List<string> nba { get; set; }
            public string rgdt { get; set; }
            public string stjCd { get; set; }
            public string tradeNam { get; set; }
            public string panNo { get; set; }
        }

        public class Root
        {
            public Compliance compliance { get; set; }
            public TaxpayerInfo taxpayerInfo { get; set; }
            public List<object> filing { get; set; }
        }

        #endregion

        public static string Invoke(string Method, string Uri, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            int _TimeoutSec = 90;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
           
            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "POSTJson":
                    {


                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }
        protected void btrgetGstdetails_Click(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(tbxGstNumber.Text))
                {
                    //string postURL = "https://appyflow.in/api/verifyGST?gstNo=27BHLPS8412D1Z5&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                    string postURL = "https://appyflow.in/api/verifyGST?gstNo=" + tbxGstNumber.Text.Trim() + "&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                    string responseData = Invoke("GET", postURL, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        var myDeserializedClass = JsonConvert.DeserializeObject<Root>(responseData);

                        var panno = myDeserializedClass.taxpayerInfo.panNo;
                        var tradeNam = myDeserializedClass.taxpayerInfo.tradeNam;
                        var address = myDeserializedClass.taxpayerInfo.pradr.addr.bnm + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.lt + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.st + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.bno + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.dst + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.loc + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.pncd;

                        tbxName.Text = tradeNam;
                        tbxAddress.Text = address;
                        tbxpan.Text = panno;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void btnAddEmail_Click(object sender, EventArgs e)
        {
            using (PracticeEntities db = new PracticeEntities())
            {
                TM_tbl_Email customer = new TM_tbl_Email();
                customer.Email = tbxBuyerEmail.Text.Trim();
                //customer.CustomerId = tbxe.Text;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string customername = string.Empty;
                if (!string.IsNullOrEmpty(ddlGroupName.SelectedValue))
                {
                    if (ddlGroupName.SelectedValue != "-1")
                    {
                        if (ddlGroupName.SelectedValue == "0")
                        {
                            customername = ddlGroupName.SelectedItem.Text;
                        }
                    }                    
                }
                if (!string.IsNullOrEmpty(customername))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        #region
                        Customer customer = new Customer();
                        customer.Name = tbxName.Text.Trim();
                        customer.Address = tbxAddress.Text;
                        customer.BuyerName = tbxBuyerName.Text;
                        customer.BuyerContactNumber = tbxBuyerContactNo.Text;
                        customer.BuyerEmail = tbxBuyerEmail.Text;
                        customer.Pan = tbxpan.Text;
                        customer.Tan = tbxTan.Text;
                        customer.EmailInvoiceSharing = tbxEmailInvoiceSahring.Text;
                        customer.GSTNum = tbxGstNumber.Text;
                        customer.CustomerGroupName = tbxGroupName.Text;
                        if ((int)ViewState["Mode"] == 1)
                        {
                            customer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                        }
                        if (CustomerManagement.Exists(customer))
                        {
                            cvDuplicateEntry1.ErrorMessage = "Customer name already exists.";
                            cvDuplicateEntry1.IsValid = false;
                            return;
                        }
                        if (chkSp.Checked == false)
                        {
                            if (ddlSPName.SelectedIndex == 0)
                            {
                                cvDuplicateEntry1.ErrorMessage = "Please Select Service Provider Name.";
                                cvDuplicateEntry1.IsValid = false;
                                return;
                            }
                        }
                        if (chkSp.Checked)
                        {
                            if (string.IsNullOrEmpty(ddlRegistrationBy.SelectedValue) || ddlRegistrationBy.SelectedValue == "-1")
                            {
                                cvDuplicateEntry1.ErrorMessage = "Please Select Registration By";
                                cvDuplicateEntry1.IsValid = false;
                                return;
                            }
                        }
                        customer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                        customer.IsServiceProvider = chkSp.Checked;
                        if (chkSp.Checked == true)
                        {
                            customer.IsServiceProvider = true;
                            customer.ServiceProviderID = null;
                            customer.IsDistributor = true;

                            //Added for ICAI/ICSI
                            if (!string.IsNullOrEmpty(ddlRegistrationBy.SelectedValue))
                            {
                                if (ddlRegistrationBy.SelectedValue != "-1")
                                {
                                    customer.RegistrationBy = Convert.ToInt32(ddlRegistrationBy.SelectedValue);
                                }
                            }
                        }
                        else
                        {
                            customer.ParentID = Convert.ToInt32(ddlSPName.SelectedValue);
                            customer.IsDistributor = false;
                            customer.IsServiceProvider = false;
                            customer.ServiceProviderID = Convert.ToInt32(ddlSPName.SelectedValue);

                            //Added for ICAI/ICSI
                            var serviceProviderCustomerRecord = CustomerManagement.GetByID(Convert.ToInt32(ddlSPName.SelectedValue));

                            if (serviceProviderCustomerRecord != null)
                                customer.RegistrationBy = serviceProviderCustomerRecord.RegistrationBy;
                        }
                        customer.ComplianceProductType = 0;

                        com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer mstCustomer = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer();
                        mstCustomer.Name = tbxName.Text.Trim();
                        mstCustomer.Address = tbxAddress.Text;
                        mstCustomer.BuyerName = tbxBuyerName.Text;
                        mstCustomer.BuyerContactNumber = tbxBuyerContactNo.Text;
                        mstCustomer.BuyerEmail = tbxBuyerEmail.Text;
                        mstCustomer.Pan = tbxpan.Text;
                        mstCustomer.Tan = tbxTan.Text;
                        mstCustomer.GSTNum = tbxGstNumber.Text;
                        mstCustomer.EmailInvoiceSharing = tbxEmailInvoiceSahring.Text;
                        mstCustomer.CustomerGroupName = tbxGroupName.Text;
                        if ((int)ViewState["Mode"] == 1)
                        {
                            mstCustomer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                        }
                        if (CustomerManagementRisk.Exists(mstCustomer))
                        {
                            cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }
                        mstCustomer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                        mstCustomer.IsServiceProvider = chkSp.Checked;
                        if (chkSp.Checked == true)
                        {
                            mstCustomer.IsServiceProvider = true;
                            mstCustomer.ServiceProviderID = null;
                            mstCustomer.IsDistributor = true;
                        }
                        else
                        {
                            mstCustomer.ParentID = Convert.ToInt32(ddlSPName.SelectedValue);
                            mstCustomer.IsDistributor = false;
                            mstCustomer.IsServiceProvider = false;
                            mstCustomer.ServiceProviderID = Convert.ToInt32(ddlSPName.SelectedValue);
                        }
                        mstCustomer.ComplianceProductType = 0;
                        mstCustomer.RegistrationBy = customer.RegistrationBy;
                        if ((int)ViewState["Mode"] == 0)
                        {
                            bool resultDataChk = false;
                            int resultData = 0;
                            resultData = CustomerManagement.Create(customer);
                            if (resultData > 0)
                            {
                                resultDataChk = CustomerManagementRisk.Create(mstCustomer);
                                if (resultDataChk == false)
                                {
                                    CustomerManagement.deleteCustReset(resultData);
                                }
                                else
                                {
                                    try
                                    {
                                        int comType = 0;
                                        CustomerBranch customerBranch = new CustomerBranch()
                                        {
                                            Name = tbxName.Text.Trim(),
                                            Type = Convert.ToByte(1),
                                            ComType = Convert.ToByte(comType),
                                            AddressLine1 = tbxAddress.Text,                                            
                                            StateID =-1,
                                            CityID =-1,
                                            Others =null,
                                            PinCode = null,
                                            ContactPerson = tbxBuyerName.Text,
                                            Landline = null,
                                            Mobile = tbxBuyerContactNo.Text,
                                             EmailID = tbxBuyerEmail.Text,
                                            CustomerID = customer.ID,
                                            ParentID =null,
                                            Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                                        };

                                        customerBranch.LegalRelationShipID = null;
                                        customerBranch.LegalEntityTypeID = null;
                                        customerBranch.GSTNumber = tbxGstNumber.Text;
                                        //if ((int)ViewState["Mode"] == 1)
                                        //{
                                        //    customerBranch.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                                        //}
                                        //if (CustomerBranchManagement.Exists(customerBranch, Convert.ToInt32(ViewState["CustomerID"])))
                                        //{
                                        //    cvDuplicateEntry.ErrorMessage = "Customer branch name already exists.";
                                        //    cvDuplicateEntry.IsValid = false;
                                        //    return;
                                        //}
                                    }
                                    catch (Exception ex)
                                    {

                                        throw;
                                    }



                                    try
                                    {
                                        if (!string.IsNullOrEmpty(tbxpan.Text))
                                        {
                                            TM_TaxDetails Taxobj = new TM_TaxDetails();
                                            Taxobj.TaxType = "PAN";
                                            Taxobj.RegNo = tbxpan.Text;
                                            Taxobj.CustomerId = customer.ID;
                                            Taxobj.BranchId = -1;
                                            Taxobj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                            Taxobj.Updatedby = Convert.ToInt32(AuthenticationHelper.UserID);
                                            PTCustomerManagement.CreateUpdate_TAXDetailsData(Taxobj);
                                        }
                                        if (!string.IsNullOrEmpty(tbxTan.Text))
                                        {
                                            TM_TaxDetails Taxobj = new TM_TaxDetails();
                                            Taxobj.TaxType = "Tan";
                                            Taxobj.RegNo = tbxTan.Text;
                                            Taxobj.CustomerId = customer.ID;
                                            Taxobj.BranchId = -1;
                                            Taxobj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                            Taxobj.Updatedby = Convert.ToInt32(AuthenticationHelper.UserID);
                                            PTCustomerManagement.CreateUpdate_TAXDetailsData(Taxobj);
                                        }
                                        if (!string.IsNullOrEmpty(tbxGstNumber.Text))
                                        {
                                            TM_TaxDetails Taxobj = new TM_TaxDetails();
                                            Taxobj.TaxType = "GST";
                                            Taxobj.RegNo = tbxGstNumber.Text;
                                            Taxobj.CustomerId = customer.ID;
                                            Taxobj.BranchId = -1;
                                            Taxobj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                            Taxobj.Updatedby = Convert.ToInt32(AuthenticationHelper.UserID);
                                            PTCustomerManagement.CreateUpdate_TAXDetailsData(Taxobj);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }

                                    var masterlimit = entities.Master_Limitations.FirstOrDefault();
                                    if (chkSp.Checked == true)
                                    {
                                        ServiceProviderCustomerLimit obj = new ServiceProviderCustomerLimit();
                                        if (masterlimit != null)
                                        {
                                            obj.ServiceProviderID = mstCustomer.ID;
                                            obj.CustomerLimit = masterlimit.CustomerLimit;
                                            obj.UserLimit = masterlimit.UserLimit;
                                            obj.DistributorID = mstCustomer.ID;
                                            obj.ParentBranchLimit = 25;
                                            obj.BranchLimit = 1;
                                            obj.EmployeeLimit = 25;
                                        }
                                        else
                                        {
                                            obj.ServiceProviderID = mstCustomer.ID;
                                            obj.CustomerLimit = 25;
                                            obj.UserLimit = 5;
                                            obj.DistributorID = mstCustomer.ID;
                                            obj.ParentBranchLimit = 25;
                                            obj.BranchLimit = 1;
                                            obj.EmployeeLimit = 25;
                                        }
                                        ICIAManagement.CreateServiceProviderCustomerLimit(obj);
                                    }
                                    else
                                    {
                                        CustomerBranchLimit cobj = new CustomerBranchLimit();
                                        if (masterlimit != null)
                                        {
                                            cobj.ServiceproviderID = Convert.ToInt32(ddlSPName.SelectedValue);
                                            cobj.CustoemerID = mstCustomer.ID;
                                            cobj.ParentBranchLimit = masterlimit.ParentBranchLimit;
                                            cobj.DistributorID = Convert.ToInt32(ddlSPName.SelectedValue);
                                            cobj.BranchLimit = masterlimit.BranchLimit;
                                            cobj.EmployeeLimit = masterlimit.EmployeeLimit;
                                        }
                                        else
                                        {
                                            cobj.ServiceproviderID = Convert.ToInt32(ddlSPName.SelectedValue);
                                            cobj.CustoemerID = mstCustomer.ID;
                                            cobj.ParentBranchLimit = 25;
                                            cobj.DistributorID = Convert.ToInt32(ddlSPName.SelectedValue);
                                            cobj.BranchLimit = 1;
                                            cobj.EmployeeLimit = 25;
                                        }
                                        ICIAManagement.CreateCustomerBranchLimit(cobj);
                                    }
                                    List<int> ProductIds = new List<int>();
                                    foreach (RepeaterItem aItem in rptProductType.Items)
                                    {
                                        CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                                        if (chkProduct.Checked)
                                        {
                                            ProductIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblProductID")).Text.Trim()));
                                            Business.Data.ProductMapping productmapping = new Business.Data.ProductMapping()
                                            {
                                                CustomerID = mstCustomer.ID,
                                                ProductID = Convert.ToInt32(((Label)aItem.FindControl("lblProductID")).Text.Trim()),
                                                IsActive = false,
                                                CreatedOn = DateTime.Now,
                                                CreatedBy = AuthenticationHelper.UserID
                                            };
                                            if (productmapping.ProductID == 3 || productmapping.ProductID == 4)
                                            {
                                                //add if audit product
                                                ProcessManagement.CreateProcessSubprocessPredefined(resultData, AuthenticationHelper.UserID);
                                            }
                                            Business.ComplianceManagement.CreateProductMapping(productmapping);
                                        }
                                    }
                                }
                            }

                            // Added by SACHIN 28 April 2016
                            string ReplyEmailAddressName = "Avantis";
                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerCreaded
                                                .Replace("@NewCustomer", customer.Name)
                                                .Replace("@LoginUser", AuthenticationHelper.User)
                                                .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                .Replace("@From", ReplyEmailAddressName)
                                                .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                            ;

                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM customer account created.", message);

                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            CustomerManagement.Update(customer);
                            CustomerManagementRisk.Update(mstCustomer);//added by rahul on 18 April 2016

                            try
                            {
                                if (!string.IsNullOrEmpty(tbxpan.Text))
                                {
                                    TM_TaxDetails Taxobj = new TM_TaxDetails();
                                    Taxobj.TaxType = "PAN";
                                    Taxobj.RegNo = tbxpan.Text;
                                    Taxobj.CustomerId = customer.ID;
                                    Taxobj.BranchId = -1;
                                    Taxobj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                    Taxobj.Updatedby = Convert.ToInt32(AuthenticationHelper.UserID);
                                    PTCustomerManagement.CreateUpdate_TAXDetailsData(Taxobj);
                                }
                                if (!string.IsNullOrEmpty(tbxTan.Text))
                                {
                                    TM_TaxDetails Taxobj = new TM_TaxDetails();
                                    Taxobj.TaxType = "Tan";
                                    Taxobj.RegNo = tbxTan.Text;
                                    Taxobj.CustomerId = customer.ID;
                                    Taxobj.BranchId = -1;
                                    Taxobj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                    Taxobj.Updatedby = Convert.ToInt32(AuthenticationHelper.UserID);
                                    PTCustomerManagement.CreateUpdate_TAXDetailsData(Taxobj);
                                }
                                if (!string.IsNullOrEmpty(tbxGstNumber.Text))
                                {
                                    TM_TaxDetails Taxobj = new TM_TaxDetails();
                                    Taxobj.TaxType = "GST";
                                    Taxobj.RegNo = tbxGstNumber.Text;
                                    Taxobj.CustomerId = customer.ID;
                                    Taxobj.BranchId = -1;
                                    Taxobj.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                                    Taxobj.Updatedby = Convert.ToInt32(AuthenticationHelper.UserID);
                                    PTCustomerManagement.CreateUpdate_TAXDetailsData(Taxobj);
                                }
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                            List<int> ProductIds = new List<int>();
                            Business.ComplianceManagement.UpdateProductMapping(customer.ID);
                            foreach (RepeaterItem aItem in rptProductType.Items)
                            {
                                CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                                if (chkProduct.Checked)
                                {
                                    ProductIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblProductID")).Text.Trim()));

                                    Business.Data.ProductMapping productmapping = new Business.Data.ProductMapping()
                                    {
                                        CustomerID = mstCustomer.ID,
                                        ProductID = Convert.ToInt32(((Label)aItem.FindControl("lblProductID")).Text.Trim()),
                                        IsActive = false,
                                        CreatedOn = DateTime.Now,
                                        CreatedBy = AuthenticationHelper.UserID
                                    };
                                    Business.ComplianceManagement.CreateProductMapping(productmapping);
                                }
                            }
                        }
                        BindServiceProvider();
                        BindGrid();
                        upCustomerList.Update();
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fClosepopupPMC()", true);
                        #endregion
                    }
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}