﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters
{
    public partial class PMUserCustomerMapping : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindProductType();
                BindCustomers();
                BindUsers();             
                BindUserCustomerMapping();
                bindPageNumber();
                txtProductType.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'dvProduct');", txtProductType.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
            }
        }
        public void BindProductType()
        {
            try
            {
                var data = CommanClass.GetAllProductMapping(Convert.ToInt32(AuthenticationHelper.CustomerID));
                rptProductType.DataSource = data;
                rptProductType.DataBind();
                foreach (RepeaterItem aItem in rptProductType.Items)
                {
                    CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                    if (!chkProduct.Checked)
                    {
                        chkProduct.Checked = true;
                    }
                }
                CheckBox ProductSelectAll = (CheckBox)rptProductType.Controls[0].Controls[0].FindControl("ProductSelectAll");
                ProductSelectAll.Checked = true;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void EditRecord(long recordID)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'dvProduct');", txtProductType.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
            var objRecord = UserCustomerMappingManagement.GetRecord_UserCustomerMapping(recordID);

            if (objRecord != null)
            {
                if (ddlCustomerPopup.Items.FindByValue(objRecord.CustomerID.ToString()) != null)
                {
                    ddlCustomerPopup.ClearSelection();
                    ddlCustomerPopup.Items.FindByValue(objRecord.CustomerID.ToString()).Selected = true;

                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                }

                if (ddlUserPopup.Items.FindByValue(objRecord.UserID.ToString()) != null)
                {
                    ddlUserPopup.ClearSelection();
                    ddlUserPopup.Items.FindByValue(objRecord.UserID.ToString()).Selected = true;
                }

                if (objRecord.MgrID != null)
                {
                    if (ddlMgrPopup.Items.FindByValue(objRecord.MgrID.ToString()) != null)
                    {
                        ddlMgrPopup.ClearSelection();
                        ddlMgrPopup.Items.FindByValue(objRecord.MgrID.ToString()).Selected = true;
                    }
                }

                #region Product    

                int distributorID = -1;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                List<long> vGetProductMappedIDs = new List<long>();
                 vGetProductMappedIDs.Add((int)objRecord.ProductID);
                foreach (RepeaterItem aItem in rptProductType.Items)
                {
                    CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                    chkProduct.Checked = false;
                    CheckBox ProductSelectAll = (CheckBox)rptProductType.Controls[0].Controls[0].FindControl("ProductSelectAll");

                    for (int i = 0; i <= vGetProductMappedIDs.Count - 1; i++)
                    {
                        if (((Label)aItem.FindControl("lblProductID")).Text.Trim() == vGetProductMappedIDs[i].ToString())
                        {
                            chkProduct.Checked = true;
                        }
                    }
                    if ((rptProductType.Items.Count) == (vGetProductMappedIDs.Count))
                    {
                        ProductSelectAll.Checked = true;
                    }
                    else
                    {
                        ProductSelectAll.Checked = false;
                    }
                }
                #endregion                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopupPMUCM()", true);
            }
        }       
        private void BindUsers()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = -1;
                }
                int distID = -1;
                if (AuthenticationHelper.Role == "DADMN")
                {
                    distID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                int serviceProviderID = -1;
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                var lstUsers = UserCustomerMappingManagement.GetAll_Users(customerID, serviceProviderID, distID);
                BindUserToDropDown(lstUsers, ddlUserPage);
                BindUserToDropDown(lstUsers, ddlMgrPage, "HMGR");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<IMA_Distributor_CustomerUser_Result> GetDistributorUsers(int distributorid, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var mappings = entities.IMA_Distributor_CustomerUser(distributorid, customerid).ToList();
                if (mappings.Count() > 0)
                    mappings = mappings.OrderBy(row => row.Name).ToList();
                return mappings.ToList();
            }
        }
        private void BindCustomersPoppup()
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {                  
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                var customerList = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2, false);
                #region Page Drop Down
                ddlCustomerPopup.DataTextField = "Name";
                ddlCustomerPopup.DataValueField = "ID";
                ddlCustomerPopup.DataSource = customerList;
                ddlCustomerPopup.DataBind();
                ddlCustomerPopup.Items.Insert(0, new ListItem("Select Customer", "-1"));
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private void BindCustomers()
        {
            try
            {            
                int distributorID = -1;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                ddlCustomerPage.DataTextField = "Name";
                ddlCustomerPage.DataValueField = "ID";
                var data = PTCustomerManagement.GetAllCustomers(customerID, AuthenticationHelper.Role, distributorID, "");
                ddlCustomerPage.DataSource = data;
                ddlCustomerPage.DataBind();
                ddlCustomerPage.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomerPage.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)) != null)
                    {
                        ddlCustomerPage.ClearSelection();
                        ddlCustomerPage.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdUserCustomerMapping.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindUserCustomerMapping();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdUserCustomerMapping.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void grdUserCustomerMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdUserCustomerMapping.PageIndex = e.NewPageIndex;
            BindUserCustomerMapping();
            bindPageNumber();
        }
        protected void grdUserCustomerMapping_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    long recordID = Convert.ToInt64(e.CommandArgument);

                    if (e.CommandName.Equals("EDIT_UserCustomerMapping"))
                    {
                        EditRecord(recordID);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "fopenpopupPMUCM();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void ddlUserPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomerPage.ClearSelection();        
            BindUserCustomerMapping();
            bindPageNumber();
        }
        private void BindUserCustomerMapping()
        {
            try
            {
                int serviceProviderID = -1;
                int distributorID = -1;
                int customerID = -1;
                int selectedCustID = -1;
                if (!string.IsNullOrEmpty(ddlCustomerPage.SelectedValue))
                {
                    selectedCustID = string.IsNullOrEmpty(ddlCustomerPage.SelectedValue) ? -1 : Convert.ToInt32(ddlCustomerPage.SelectedValue);
                }
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    selectedCustID = customerID;
                }
                int selectedUserID = string.IsNullOrEmpty(ddlUserPage.SelectedValue) ? -1 : Convert.ToInt32(ddlUserPage.SelectedValue);
                int selectedMgrID = string.IsNullOrEmpty(ddlMgrPage.SelectedValue) ? -1 : Convert.ToInt32(ddlMgrPage.SelectedValue);
                var lstUserCustomerMapping = UserCustomerMappingManagement.GetUserCustomerMappingList(selectedUserID, selectedCustID, serviceProviderID, distributorID, selectedMgrID);
                if (lstUserCustomerMapping.Count > 0)
                {                    
                    grdUserCustomerMapping.DataSource = lstUserCustomerMapping;
                    Session["TotalCustomMapping"] = 0;
                    Session["TotalCustomMapping"] = lstUserCustomerMapping.Count();
                    grdUserCustomerMapping.DataBind();
                    upUCMList.Update();
                }
                else
                {
                    grdUserCustomerMapping.DataSource = null;
                    Session["TotalCustomMapping"] = 0;
                    Session["TotalCustomMapping"] = lstUserCustomerMapping.Count();
                    grdUserCustomerMapping.DataBind();
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        public static List<SP_GETUserCustomerMapping_ALL_Result> GetUserCustomerMappingList(int userID, int customerID, int serviceProviderID, int distributorID)
        {
            List<SP_GETUserCustomerMapping_ALL_Result> UserCustomerMappingList = new List<SP_GETUserCustomerMapping_ALL_Result>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    UserCustomerMappingList = (entities.SP_GETUserCustomerMapping_ALL(Convert.ToInt32(userID), Convert.ToInt32(customerID), serviceProviderID, distributorID, -1)).OrderBy(row => row.CustomerName).ToList();                  
                }
                return UserCustomerMappingList;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        protected void ddlCustomerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlUserPage.ClearSelection();                
                BindUserCustomerMapping();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void ddlMgrPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomerPage.ClearSelection();
            ddlUserPage.ClearSelection();
            BindUserCustomerMapping();
            bindPageNumber();
        }
        protected void upUCM_Load(object sender, EventArgs e)
        {
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUserCustomerMapping.PageIndex = 0;
                BindUserCustomerMapping();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        protected void btnAddTax_Click(object sender, EventArgs e)
        {
            foreach (RepeaterItem aItem in rptProductType.Items)
            {
                CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                chkProduct.Checked = false;
                CheckBox ProductSelectAll = (CheckBox)rptProductType.Controls[0].Controls[0].FindControl("ProductSelectAll");
                ProductSelectAll.Checked = false;
            }
            ddlUserPopup.ClearSelection();
            ddlMgrPopup.ClearSelection();
            BindCustomersPoppup();
            BindUsers_Popup();            
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopupPMUCM()", true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'dvProduct');", txtProductType.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
            upUCM.Update();
            
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalCustomMapping"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private void BindUsers_Popup()
        {
            try
            {               
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = -1;
                }
                int distID = -1;
                if (AuthenticationHelper.Role == "DADMN")
                {
                    distID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                int serviceProviderID = -1;
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                var lstUsers = UserCustomerMappingManagement.GetAll_Users(customerID, serviceProviderID, distID);
                BindUserToDropDown(lstUsers, ddlUserPopup);
                BindUserToDropDown(lstUsers, ddlMgrPopup, "HMGR");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvDuplicateEntry1.IsValid = false;
                CvDuplicateEntry1.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private void BindUserToDropDown(List<SP_RLCS_GetAllUser_ServiceProviderDistributor_Result> lstUsers, DropDownList ddltoBind, string roleCode = "")
        {
            try
            {
                if (lstUsers.Count > 0 && !string.IsNullOrEmpty(roleCode))
                    lstUsers = lstUsers.Where(row => row.RoleCode == roleCode).ToList();
                List<object> users = new List<object>();
                users = (from row in lstUsers
                         select new { ID = row.ID, Name = row.FirstName + " " + row.LastName, RoleCode = row.RoleCode }).Distinct().OrderBy(row => row.Name).ToList<object>();
                ddltoBind.Items.Clear();
                ddltoBind.DataTextField = "Name";
                ddltoBind.DataValueField = "ID";
                if (roleCode == "HMGR")
                {
                    users.Insert(0, new { ID = -1, Name = "Select Manager" });
                }
                else
                {
                    users.Insert(0, new { ID = -1, Name = "Select Executive" });
                }
                ddltoBind.DataSource = users;
                ddltoBind.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvDuplicateEntry1.IsValid = false;
                CvDuplicateEntry1.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void ddlCustomerPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlUserPopup.ClearSelection();
                ddlMgrPopup.ClearSelection();
                BindUsers_Popup();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'dvProduct');", txtProductType.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideProductList", "$(\"#dvProduct\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvDuplicateEntry1.IsValid = false;
                CvDuplicateEntry1.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdUserCustomerMapping.PageIndex = chkSelectedPage - 1;
                grdUserCustomerMapping.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindUserCustomerMapping(); 
            }
        }
        public static bool CreateUpdate_UserCustomerMapping(UserCustomerMapping _objRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.UserCustomerMappings
                                      where row.UserID == _objRecord.UserID
                                      && row.CustomerID == _objRecord.CustomerID
                                      && row.ProductID == _objRecord.ProductID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.MgrID = _objRecord.MgrID;
                        prevRecord.ProductID = _objRecord.ProductID;
                        prevRecord.IsActive = _objRecord.IsActive;
                        prevRecord.UpdatedOn = DateTime.Now;

                        saveSuccess = true;
                    }
                    else
                    {
                        _objRecord.CreatedOn = DateTime.Now;
                        entities.UserCustomerMappings.Add(_objRecord);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }  
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue) && ddlCustomerPopup.SelectedValue != "-1")
                {
                    if (!string.IsNullOrEmpty(ddlUserPopup.SelectedValue) && ddlUserPopup.SelectedValue != "-1")
                    {                        
                        int userID = Convert.ToInt32(ddlUserPopup.SelectedValue);
                        int customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);                        
                        List<int> ProductIds = new List<int>();
                        foreach (RepeaterItem aItem in rptProductType.Items)
                        {
                            CheckBox chkProduct = (CheckBox)aItem.FindControl("chkProduct");
                            if (chkProduct.Checked)
                            {
                                ProductIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblProductID")).Text.Trim()));
                                Business.Data.UserCustomerMapping objUserCustMapping = new Business.Data.UserCustomerMapping()
                                {
                                    UserID = userID,
                                    CustomerID = customerID,
                                    ProductID = Convert.ToInt32(((Label)aItem.FindControl("lblProductID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedOn = DateTime.Now,
                                    MgrID = Convert.ToInt32(ddlMgrPopup.SelectedValue)
                                };
                                saveSuccess = CreateUpdate_UserCustomerMapping(objUserCustMapping);                                
                            }
                        }

                        if (saveSuccess)
                        {
                            ddlUserPopup.ClearSelection();
                            ddlCustomerPopup.ClearSelection();
                            ddlMgrPopup.ClearSelection();

                            CvDuplicateEntry1.IsValid = false;
                            CvDuplicateEntry1.ErrorMessage = "Details Save Successfully";
                            //vsUserCustomerMapping.CssClass = "alert alert-success";

                            BindUserCustomerMapping();
                            bindPageNumber();
                        }
                        else
                        {
                            CvDuplicateEntry1.IsValid = false;
                            CvDuplicateEntry1.ErrorMessage = "Something went wrong, please try again";
                        }                       
                    }
                    else
                    {
                        CvDuplicateEntry1.IsValid = false;
                        CvDuplicateEntry1.ErrorMessage = "Select Executive to Assign";
                    }
                }
                else
                {
                    CvDuplicateEntry1.IsValid = false;
                    CvDuplicateEntry1.ErrorMessage = "Select Customer to Assign";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvDuplicateEntry1.IsValid = false;
                CvDuplicateEntry1.ErrorMessage = "Something went wrong, Please try again";
            }
        }
    }
}