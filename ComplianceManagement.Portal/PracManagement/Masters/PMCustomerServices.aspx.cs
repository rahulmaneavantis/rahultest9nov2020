﻿using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters
{
    public partial class PMCustomerServices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomers();               
            }
        }
      
        private void BindCustomers()
        {
            try
            {
                int distributorID = -1;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";
                var data = PTCustomerManagement.GetAllCustomers(customerID, AuthenticationHelper.Role, distributorID, "");
                ddlCustomerList.DataSource = data;
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomerList.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)) != null)
                    {
                        ddlCustomerList.ClearSelection();
                        ddlCustomerList.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private void BindServiceNamePopPUP()
        {
            try
            {

                ddlServiceName.DataTextField = "ServiceName";
                ddlServiceName.DataValueField = "ID";
                var data = PTCustomerManagement.GetAllServiceDetails();
                ddlServiceName.DataSource = data;
                ddlServiceName.DataBind();
                ddlServiceName.Items.Insert(0, new ListItem("< Select Service Name >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private void BindFrequencyPopPUP()
        {
            try
            {                
                ddlFrequency.DataTextField = "FrequencyName";
                ddlFrequency.DataValueField = "FrequencyId";
                var data = PTCustomerManagement.GetAllFrequency();
                ddlFrequency.DataSource = data;
                ddlFrequency.DataBind();
                ddlFrequency.Items.Insert(0, new ListItem("< Select Frequency >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private void BindCustomersPopPUP()
        {
            try
            {
                int distributorID = -1;
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                ddlCustomerPage.DataTextField = "Name";
                ddlCustomerPage.DataValueField = "ID";
                var data = PTCustomerManagement.GetAllCustomers(customerID, AuthenticationHelper.Role, distributorID, "");
                ddlCustomerPage.DataSource = data;
                ddlCustomerPage.DataBind();
                ddlCustomerPage.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomerPage.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)) != null)
                    {
                        ddlCustomerPage.ClearSelection();
                        ddlCustomerPage.Items.FindByValue(Convert.ToString(AuthenticationHelper.CustomerID)).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
        }
        protected void grdCustomerServices_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdCustomerServices.PageIndex = e.NewPageIndex;
            //BindGrid();
            bindPageNumber();
        }
        protected void grdCustomerServices_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    long recordID = Convert.ToInt64(e.CommandArgument);

                    if (e.CommandName.Equals("EDIT_UserCustomerMapping"))
                    {
                        BindFrequencyPopPUP();
                        BindCustomersPopPUP();
                        BindServiceNamePopPUP();
                        //EditRecord(recordID);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "fopenpopupPMCS();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerServices.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //BindGrid();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCustomerServices.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void upCS_Load(object sender, EventArgs e)
        {
            DateTime date = DateTime.MinValue;
            if (DateTime.TryParseExact(tbxBillingDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
            }
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //BindGrid();
                //bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlServiceName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                using (PracticeEntities entities = new PracticeEntities())
                {
                    if (!string.IsNullOrEmpty(ddlServiceName.SelectedValue) && ddlServiceName.SelectedValue != "-1")
                    {
                        int sid = Convert.ToInt32(ddlServiceName.SelectedValue);
                        var clist = (from row in entities.TM_tbl_ServiceDetails
                                     where row.ID == sid
                                     select row).FirstOrDefault();
                        if (clist != null)
                        {                            
                            if (ddlFrequency.Items.FindByValue(clist.Frequency.ToString()) != null)
                            {
                                ddlFrequency.ClearSelection();
                                ddlFrequency.Items.FindByValue(clist.Frequency.ToString()).Selected = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnAddTax_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "fopenpopupPMCS();", true);
            BindFrequencyPopPUP();
            BindCustomersPopPUP();
            BindServiceNamePopPUP();
            upCS.Update();
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TaxTotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdCustomerServices.PageIndex = chkSelectedPage - 1;
                grdCustomerServices.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //BindGrid();
            }
        }

    }
}