﻿
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters
{
    public partial class FirmOfficesMaster : System.Web.UI.Page
    {

        protected int pageid = 1;      
        protected void Page_Load(object sender, EventArgs e)
        {           
            cvDuplicateEntry.CssClass = "alert alert-danger";
            if (!IsPostBack)
            {
                try
                {                                      
                    ViewState["ParentID"] = null;
                    BindCustomerBranches();                                       
                    BindStates();                                       
                    bindPageNumber();                  
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdCustomerBranch.PageIndex = chkSelectedPage - 1;
                grdCustomerBranch.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCustomerBranches();
            }
        }      
        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                    BindCustomerBranches();
                    upCustomerBranchList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<TM_FirmOfficeMaster> GetAll(int firmID, long parentID, string filter)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var customerBranches = (from row in entities.TM_FirmOfficeMaster
                                        where row.IsDeleted == false && row.FirmID == firmID
                                        select row);
                if (parentID != -1)
                {
                    customerBranches = customerBranches.Where(entry => entry.ParentID == parentID);
                }
                else
                {
                    customerBranches = customerBranches.Where(entry => entry.ParentID == null);
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    customerBranches = customerBranches.Where(entry => entry.Name.Contains(filter));
                }
                return customerBranches.ToList();
            }
        }
        public static object GetHierarchy(int firID, long parentFirmBranchID)
        {
            List<NameValue> hierarchy = new List<NameValue>();
            using (PracticeEntities entities = new PracticeEntities())
            {
                if (parentFirmBranchID != -1)
                {
                    long? parentID = parentFirmBranchID;
                    while (parentID.HasValue)
                    {
                        var customerBranch = (from row in entities.TM_FirmOfficeMaster
                                              where row.ID == parentID.Value
                                              select new { row.ID, row.Name, row.ParentID }).SingleOrDefault();

                        hierarchy.Add(new NameValue() { ID = (int)customerBranch.ID, Name = customerBranch.Name });
                        parentID = customerBranch.ParentID;
                    }
                }
                var customerName = (from row in entities.TM_FirmMaster
                                    where row.ID == firID
                                    select row.Name).SingleOrDefault();

                hierarchy.Add(new NameValue() { ID = firID, Name = customerName });
            }
            hierarchy.Reverse();
            return hierarchy;
        }
        private void BindCustomerBranches()
        {
            try
            {
                long parentID = -1;
                int firmID = -1;
                if (Session["firmID"] != null)
                {
                    firmID = Convert.ToInt32(Session["firmID"]);
                }                              
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);                    
                }
                else
                {
                    ViewState["ParentID"] = null;
                }
                dlBreadcrumb.DataSource =GetHierarchy(firmID, parentID);
                dlBreadcrumb.DataBind();

                var CustomerBranchList = GetAll(firmID, parentID, tbxFilter.Text);
                grdCustomerBranch.DataSource = CustomerBranchList;
                Session["TotalRows"] = CustomerBranchList.Count;
                grdCustomerBranch.DataBind();
                upCustomerBranchList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<State> GetAllStates()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var states = (from row in entities.States
                              orderby row.Name ascending
                              select row).ToList();

                return states.OrderBy(entry => entry.Name).ToList();
            }
        }
        private void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";
                ddlState.DataSource = GetAllStates();
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem(" Select ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            int stateid = 0;
            int customerID = 1;
            if (!string.IsNullOrEmpty(ddlState.SelectedValue))
            {
                if (ddlState.SelectedValue.ToString() != "-1")
                {
                    using (PracticeEntities entities = new PracticeEntities())
                    {
                        stateid = Convert.ToInt32(ddlState.SelectedValue);
                        var users = (from row in entities.TM_FirmMaster
                                     where row.IsDeleted == false
                                     && row.Customerid == customerID
                                     && row.StateId == stateid
                                     select row).FirstOrDefault();
                        if (users != null)
                        {
                            divgst.Attributes.Add("style", "display:none");
                        }
                        else
                        {
                            divgst.Attributes.Add("style", "display:block");
                            tbxGstNumber.Text = string.Empty;
                        }

                    }
                }
            }
        }     
        protected void grdCustomerBranch_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerBranchID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_CUSTOMER_BRANCH"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["firmBranchID"] = customerBranchID;
                    using (PracticeEntities entities = new PracticeEntities())
                    {
                        var customerBranch = (from row in entities.TM_FirmOfficeMaster
                                            where row.IsDeleted == false
                                            && row.ID == customerBranchID
                                            select row).FirstOrDefault();                       
                        if (customerBranch != null)
                        {
                            PopulateInputForm();
                            if (!string.IsNullOrEmpty(customerBranch.Name))
                            {
                                tbxName.Text = customerBranch.Name;
                            }
                            tbxAddress.Text = customerBranch.Address;
                            tbxGstNumber.Text = customerBranch.GSTNumber;
                            if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.StateId)))
                            {
                                if (customerBranch.StateId != -1)
                                {
                                    ddlState.SelectedValue = customerBranch.StateId.ToString();
                                    ddlState_SelectedIndexChanged(null, null);
                                }
                            }                                                                             
                            upCustomerBranches.Update();
                        }
                    }
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER_BRANCH"))
                {
                    //CustomerBranchManagement.Delete(customerBranchID);
                    //CustomerBranchManagementRisk.Delete(customerBranchID);
                    //UserManagementRisk.DeleteBranchVertical(customerBranchID);
                    BindCustomerBranches();
                    bindPageNumber();
                    upCustomerBranches.Update();
                }
                else if (e.CommandName.Equals("VIEW_CHILDREN"))
                {
                    try
                    {
                        ViewState["ParentID"] = customerBranchID;                       
                        BindCustomerBranches();
                        bindPageNumber();
                        upCustomerBranches.Update();
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomerBranch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = e.NewPageIndex;
                BindCustomerBranches();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCustomerBranch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;                              
                tbxName.Text = tbxAddress.Text = tbxName.Text  = tbxGstNumber.Text  = string.Empty;
                PopulateInputForm();             
                ddlState.SelectedValue = "-1";
                ddlState_SelectedIndexChanged(null, null);              
                upCustomerBranches.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopup()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string GetFirmByID(long firmid)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var customerBranch = (from row in entities.TM_FirmMaster
                                      where row.ID == firmid && row.IsDeleted == false
                                      select row.Name).FirstOrDefault();
                return customerBranch;
            }
        }
        public string GetFirmOfficeByID(long firmparentid)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var customerBranch = (from row in entities.TM_FirmOfficeMaster
                                      where row.ID == firmparentid && row.IsDeleted == false
                                      select row.Name).FirstOrDefault();
                return customerBranch;
            }
        }
        private void PopulateInputForm()
        {
            try
            {
                int firmID = -1;
                if (Session["firmID"] != null)
                {
                    firmID = Convert.ToInt32(Session["firmID"]);
                }
                divParent.Visible = ViewState["ParentID"] != null;
                litCustomer.Text = GetFirmByID(firmID);
                if (ViewState["ParentID"] != null)
                {
                    litParent.Text = GetFirmOfficeByID(Convert.ToInt32(ViewState["ParentID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = 0;
                BindCustomerBranches();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #region 
        public class Compliance
        {
            public object filingFrequency { get; set; }
        }

        public class Addr
        {
            public string st { get; set; }
            public string lg { get; set; }
            public string stcd { get; set; }
            public string pncd { get; set; }
            public string bno { get; set; }
            public string flno { get; set; }
            public string dst { get; set; }
            public string loc { get; set; }
            public string bnm { get; set; }
            public string lt { get; set; }
            public string city { get; set; }
        }

        public class Pradr
        {
            public string ntr { get; set; }
            public Addr addr { get; set; }
        }

        public class TaxpayerInfo
        {
            public object errorMsg { get; set; }
            public string ctj { get; set; }
            public Pradr pradr { get; set; }
            public string cxdt { get; set; }
            public string sts { get; set; }
            public string gstin { get; set; }
            public string ctjCd { get; set; }
            public string ctb { get; set; }
            public string stj { get; set; }
            public string dty { get; set; }
            public List<object> adadr { get; set; }
            public object frequencyType { get; set; }
            public string lgnm { get; set; }
            public List<string> nba { get; set; }
            public string rgdt { get; set; }
            public string stjCd { get; set; }
            public string tradeNam { get; set; }
            public string panNo { get; set; }
        }

        public class Root
        {
            public Compliance compliance { get; set; }
            public TaxpayerInfo taxpayerInfo { get; set; }
            public List<object> filing { get; set; }
        }

        #endregion
        public static string Invoke(string Method, string Uri, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            int _TimeoutSec = 90;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //var authValue = "5E8E3D45-172D-4D58-8DF8-EB0B0E";
            //cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));
            //var _UserAgent = "f2UKmAm0KlI98bEYGGxEHQ==";
            //// You can actually also set the User-Agent via a built-in property
            //cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);
            //// You get the following exception when trying to set the "Content-Type" header like this:
            //// cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
            //// "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "POSTJson":
                    {


                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }
        protected void btrgetGstdetails_Click(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(tbxGstNumber.Text))
                {
                    //string postURL = "https://appyflow.in/api/verifyGST?gstNo=27BHLPS8412D1Z5&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                    string postURL = "https://appyflow.in/api/verifyGST?gstNo=" + tbxGstNumber.Text.Trim() + "&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                    string responseData = Invoke("GET", postURL, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        var myDeserializedClass = JsonConvert.DeserializeObject<Root>(responseData);

                        var panno = myDeserializedClass.taxpayerInfo.panNo;
                        var tradeNam = myDeserializedClass.taxpayerInfo.tradeNam;
                        var address = myDeserializedClass.taxpayerInfo.pradr.addr.bnm + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.lt + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.st + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.bno + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.dst + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.loc + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.pncd;

                        tbxName.Text = tradeNam;
                        tbxAddress.Text = address;                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int resultData = 0;
                int comType = 0;
                if (!string.IsNullOrEmpty(ddlState.SelectedValue))
                {
                    if (ddlState.SelectedValue.ToString() == "-1")
                    {
                        comType = 0;
                    }
                    else
                    {
                        comType = Convert.ToInt32(ddlState.SelectedValue);
                    }
                }
                int firmID = -1;
                if (Session["firmID"] != null)
                {
                    firmID = Convert.ToInt32(Session["firmID"]);
                }
                #region Firm Update
                TM_FirmOfficeMaster customerBranch = new TM_FirmOfficeMaster()
                {
                    Name = tbxName.Text,
                    StateId = Convert.ToByte(comType),
                    Address = tbxAddress.Text,                    
                    GSTNumber = tbxGstNumber.Text,
                    FirmID = firmID,
                    ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                };
                if ((int)ViewState["Mode"] == 1)
                {
                    customerBranch.ID = Convert.ToInt32(ViewState["firmBranchID"]);
                    
                }

                if ((int)ViewState["Mode"] == 0)/////////***************For Save*********************//
                {
                    if (Exists(customerBranch, firmID))
                    {
                        cvDuplicateEntry.ErrorMessage = "Customer branch name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }

                    resultData = Create(customerBranch);
                }
                else if ((int)ViewState["Mode"] == 1)/////////////////**************For Update****************///////////////
                {
                    resultData = UpdateCustBranch(customerBranch);
                }
              

                if (resultData < 0)
                {                
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                #endregion

                BindCustomerBranches();
                bindPageNumber();
                upCustomerBranchList.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:fClosepopup()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static int Create(TM_FirmOfficeMaster mst_CustomerBranch)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                try
                {
                    mst_CustomerBranch.IsDeleted = false;
                    mst_CustomerBranch.CreatedOn = DateTime.UtcNow;
                    entities.TM_FirmOfficeMaster.Add(mst_CustomerBranch);
                    entities.SaveChanges();
                    return 1;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
        public static bool Exists(TM_FirmOfficeMaster customerBranch, int fid)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var query = (from row in entities.TM_FirmOfficeMaster
                             where row.IsDeleted == false
                             && row.Name.Equals(customerBranch.Name) && row.FirmID == fid
                             select row);

                if (customerBranch.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customerBranch.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static int UpdateCustBranch(TM_FirmOfficeMaster customerBranch)
        {
            try
            {
                using (PracticeEntities entities = new PracticeEntities())
                {
                    TM_FirmOfficeMaster customerBranchToUpdate = (from row in entities.TM_FirmOfficeMaster
                                                                  where row.ID == customerBranch.ID && row.IsDeleted == false
                                                            select row).FirstOrDefault();

                    customerBranchToUpdate.Name = customerBranch.Name;
                    customerBranchToUpdate.Address = customerBranch.Address;                    
                    customerBranchToUpdate.StateId = customerBranch.StateId;
                    customerBranchToUpdate.GSTNumber = customerBranch.GSTNumber;
                    entities.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void upCustomerBranches_Load(object sender, EventArgs e)
        {
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCustomerBranch_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int firmID = -1;
                if (Session["firmID"] != null)
                {
                    firmID = Convert.ToInt32(Session["firmID"]);
                }
                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }

                var customerBranchList = GetAll(firmID, parentID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    customerBranchList = customerBranchList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    customerBranchList = customerBranchList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }
                foreach (DataControlField field in grdCustomerBranch.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCustomerBranch.Columns.IndexOf(field);
                    }
                }

                grdCustomerBranch.DataSource = customerBranchList;
                grdCustomerBranch.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }             
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerBranch.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCustomerBranches();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCustomerBranch.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}