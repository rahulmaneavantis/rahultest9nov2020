﻿
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters
{
    public partial class FirmMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    {                      
                        BindGrid();
                        BindStates();
                        bindPageNumber();                       
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
        private void BindGrid()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "DADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (customerID != -1)
                {
                    var customerData = GetAll(customerID, tbxFilter.Text);
                    List<object> dataSource = new List<object>();
                    foreach (var customerInfo in customerData)
                    {
                        dataSource.Add(new
                        {
                            customerInfo.ID,
                            customerInfo.Name,
                            customerInfo.Address,
                            customerInfo.Pan,
                            customerInfo.GSTNumber,
                            customerInfo.CreatedOn,
                            customerInfo.IsDeleted,
                        });
                    }
                    Session["TotalRows"] = dataSource.Count;
                    grdCustomer.DataSource = dataSource;
                    grdCustomer.DataBind();
                }      
                            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<State> GetAllStates()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var states = (from row in entities.States
                              orderby row.Name ascending
                              select row).ToList();

                return states.OrderBy(entry => entry.Name).ToList();
            }
        }
        private void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";
                ddlState.DataSource = GetAllStates();
                ddlState.DataBind();

                ddlState.Items.Insert(0, new ListItem(" Select ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void EditCustomerInformation(int firmid)
        {
            try
            {
                using (PracticeEntities entities = new PracticeEntities())
                {
                    var customerData = (from row in entities.TM_FirmMaster
                                 where row.IsDeleted == false
                                 && row.ID == firmid
                                 select row).FirstOrDefault() ;
                   
                    if (customerData != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(customerData.StateId)))
                        {
                            if (customerData.StateId != -1)
                            {
                                ddlState.SelectedValue = customerData.StateId.ToString();                                
                            }
                        }
                        tbxName.Text = customerData.Name;
                        tbxAddress.Text = customerData.Address;
                        tbxpan.Text = customerData.Pan;
                        tbxGstNumber.Text = customerData.GSTNumber;
                    }                                                          
                }
                upCustomerBranches.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "javascript:fopenpopup()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int firmID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_CUSTOMER"))
                {
                    ViewState["firmID"] = firmID;                    
                    EditCustomerInformation(firmID);                  
                }            
                else if (e.CommandName.Equals("VIEW_COMPANIES"))
                {
                    Session["firmID"] = firmID;
                    Session["firmParentID"] = null;                    
                    Response.Redirect("~/PracManagement/Masters/FirmOfficesMaster.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }      
        protected void grdCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {                
                grdCustomer.PageIndex = e.NewPageIndex;
                BindGrid();             
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }              
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = 0;
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        public static List<TM_FirmMaster> GetAll(int customerID, string filter = null)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var users = (from row in entities.TM_FirmMaster
                             where row.IsDeleted == false                           
                             select row);

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.Customerid == customerID);
                }


                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.Contains(filter));
                }

                return users.OrderBy(entry => entry.Name).ToList();
            }
        }
        protected void grdCustomer_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = 1;               
                var customerlist = GetAll(customerID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    customerlist = customerlist.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    customerlist = customerlist.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }
                foreach (DataControlField field in grdCustomer.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCustomer.Columns.IndexOf(field);
                    }
                }
                grdCustomer.DataSource = customerlist;
                grdCustomer.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCustomer_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdCustomer.PageIndex = chkSelectedPage - 1;
                grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCustomer.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            int stateid = 0;
            int customerID = 1;
            if (!string.IsNullOrEmpty(ddlState.SelectedValue))
            {
                if (ddlState.SelectedValue.ToString() != "-1")
                {
                    using (PracticeEntities entities = new PracticeEntities())
                    {
                        stateid = Convert.ToInt32(ddlState.SelectedValue);
                        var users = (from row in entities.TM_FirmMaster
                                     where row.IsDeleted == false
                                     && row.Customerid == customerID
                                     && row.StateId== stateid
                                     select row).FirstOrDefault();
                        if (users != null)
                        {
                            divgst.Attributes.Add("style", "display:none");
                        }
                        else
                        {
                            divgst.Attributes.Add("style", "display:block");
                            tbxGstNumber.Text = string.Empty;
                        }

                    }
                }
            }
        }

        #region 
        public class Compliance
        {
            public object filingFrequency { get; set; }
        }

        public class Addr
        {
            public string st { get; set; }
            public string lg { get; set; }
            public string stcd { get; set; }
            public string pncd { get; set; }
            public string bno { get; set; }
            public string flno { get; set; }
            public string dst { get; set; }
            public string loc { get; set; }
            public string bnm { get; set; }
            public string lt { get; set; }
            public string city { get; set; }
        }

        public class Pradr
        {
            public string ntr { get; set; }
            public Addr addr { get; set; }
        }

        public class TaxpayerInfo
        {
            public object errorMsg { get; set; }
            public string ctj { get; set; }
            public Pradr pradr { get; set; }
            public string cxdt { get; set; }
            public string sts { get; set; }
            public string gstin { get; set; }
            public string ctjCd { get; set; }
            public string ctb { get; set; }
            public string stj { get; set; }
            public string dty { get; set; }
            public List<object> adadr { get; set; }
            public object frequencyType { get; set; }
            public string lgnm { get; set; }
            public List<string> nba { get; set; }
            public string rgdt { get; set; }
            public string stjCd { get; set; }
            public string tradeNam { get; set; }
            public string panNo { get; set; }
        }

        public class Root
        {
            public Compliance compliance { get; set; }
            public TaxpayerInfo taxpayerInfo { get; set; }
            public List<object> filing { get; set; }
        }

        #endregion
        public static string Invoke(string Method, string Uri, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            int _TimeoutSec = 90;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //var authValue = "5E8E3D45-172D-4D58-8DF8-EB0B0E";
            //cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));
            //var _UserAgent = "f2UKmAm0KlI98bEYGGxEHQ==";
            //// You can actually also set the User-Agent via a built-in property
            //cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);
            //// You get the following exception when trying to set the "Content-Type" header like this:
            //// cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
            //// "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "POSTJson":
                    {


                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }
        protected void btrgetGstdetails_Click(object sender, EventArgs e)
        {
           
                try
                {
                    if (!string.IsNullOrEmpty(tbxGstNumber.Text))
                    {
                        //string postURL = "https://appyflow.in/api/verifyGST?gstNo=27BHLPS8412D1Z5&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                        string postURL = "https://appyflow.in/api/verifyGST?gstNo=" + tbxGstNumber.Text.Trim() + "&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                        string responseData = Invoke("GET", postURL, "");

                        if (!string.IsNullOrEmpty(responseData))
                        {
                            var myDeserializedClass = JsonConvert.DeserializeObject<Root>(responseData);

                            var panno = myDeserializedClass.taxpayerInfo.panNo;
                            var tradeNam = myDeserializedClass.taxpayerInfo.tradeNam;
                            var address = myDeserializedClass.taxpayerInfo.pradr.addr.bnm + " " +
                                        myDeserializedClass.taxpayerInfo.pradr.addr.lt + " " +
                                        myDeserializedClass.taxpayerInfo.pradr.addr.st + " " +
                                        myDeserializedClass.taxpayerInfo.pradr.addr.bno + " " +
                                        myDeserializedClass.taxpayerInfo.pradr.addr.dst + " " +
                                        myDeserializedClass.taxpayerInfo.pradr.addr.loc + " " +
                                        myDeserializedClass.taxpayerInfo.pradr.addr.pncd;

                            tbxName.Text = tradeNam;
                            tbxAddress.Text = address;
                            tbxpan.Text = panno;                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
           
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int resultData = 0;
                int comType = 0;
                if (!string.IsNullOrEmpty(ddlState.SelectedValue))
                {
                    if (ddlState.SelectedValue.ToString() == "-1")
                    {
                        comType = 0;
                    }
                    else
                    {
                        comType = Convert.ToInt32(ddlState.SelectedValue);
                    }
                }

                #region Firm Update
                TM_FirmMaster customerBranch = new TM_FirmMaster()
                {
                    Name = tbxName.Text,
                    StateId = Convert.ToByte(comType),
                    Address = tbxAddress.Text,
                    Pan = tbxpan.Text,
                    GSTNumber = tbxGstNumber.Text,
                };
                customerBranch.ID = Convert.ToInt32(ViewState["firmID"]);
                resultData = UpdateCustBranch(customerBranch);
                if (resultData < 0)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                #endregion
                BindGrid();
                bindPageNumber();
                upCustomerBranchList.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:fClosepopup()", true);
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static int UpdateCustBranch(TM_FirmMaster customerBranch)
        {
            try
            {
                using (PracticeEntities entities = new PracticeEntities())
                {
                    TM_FirmMaster customerBranchToUpdate = (from row in entities.TM_FirmMaster
                                                            where row.ID == customerBranch.ID && row.IsDeleted == false
                                                             select row).FirstOrDefault();

                    customerBranchToUpdate.Name = customerBranch.Name;                   
                    customerBranchToUpdate.Address = customerBranch.Address;
                    customerBranchToUpdate.Pan = customerBranch.Pan;
                    customerBranchToUpdate.StateId = customerBranch.StateId;                   
                    customerBranchToUpdate.GSTNumber = customerBranch.GSTNumber;                    
                    entities.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void upCustomerBranches_Load(object sender, EventArgs e)
        {
        }         
    }
}