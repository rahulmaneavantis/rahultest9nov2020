﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PracticeManagement.Master" AutoEventWireup="true" CodeBehind="PMTaxDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters.PMTaxDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .aspNetDisabled {
            cursor: not-allowed;
        }

        .clearfix:after {
            clear: both;
            content: "";
            display: block;
            height: 0;
        }

        .pull-right {
            float: right;
        }

        .step > a {
            color: #333;
            text-decoration: none;
        }

        .step.current > a {
            color: white;
            text-decoration: none;
        }

        .step > a:active {
            color: #333;
            text-decoration: none;
        }

        .step > a:hover {
            color: white;
        }

        .step {
            clear: revert !important;
        }
        /* Breadcrups CSS */

        .arrow-steps .step {
            font-size: 14px;
            text-align: center;
            color: #666;
            cursor: default;
            margin: 0 3px;
            padding: 10px 10px 6px 30px;
            min-width: 10%;
            float: left;
            position: relative;
            background-color: #d9e3f7;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            transition: background-color 0.2s ease;
        }

            .arrow-steps .step:after,
            .arrow-steps .step:before {
                content: " ";
                position: absolute;
                top: 0;
                right: -17px;
                width: 0;
                height: 0;
                border-top: 19px solid transparent;
                border-bottom: 17px solid transparent;
                border-left: 17px solid #d9e3f7;
                z-index: 2;
                transition: border-color 0.2s ease;
            }

            .arrow-steps .step:before {
                right: auto;
                left: 0;
                border-left: 17px solid #fff;
                z-index: 0;
            }

            .arrow-steps .step:first-child:before {
                border: none;
            }

            .arrow-steps .step:last-child:after {
                border: none;
            }

            .arrow-steps .step:first-child {
                border-top-left-radius: 4px;
                border-bottom-left-radius: 4px;
            }

            .arrow-steps .step:last-child {
                border-top-right-radius: 4px;
                border-bottom-right-radius: 4px;
            }

            .arrow-steps .step span {
                position: relative;
            }

                .arrow-steps .step span:before {
                    opacity: 0;
                    content: "✔";
                    position: absolute;
                    top: -2px;
                    left: -20px;
                }

            .arrow-steps .step.done span:before {
                opacity: 1;
                -webkit-transition: opacity 0.3s ease 0.5s;
                -moz-transition: opacity 0.3s ease 0.5s;
                -ms-transition: opacity 0.3s ease 0.5s;
                transition: opacity 0.3s ease 0.5s;
            }

            .arrow-steps .step.current {
                color: #fff;
                background-color: #23468c;
            }

                .arrow-steps .step.current:after {
                    border-left: 17px solid #23468c;
                }

        .float-child {
            width: 84%;
            float: left;
        }

        .float-child1 {
            width: 10%;
            float: left;
        }
    </style>

    <script type="text/javascript">
        function fopenpopupPMTax() {
            $('#divTaxDetailsDialog').modal('show');
        };
        function fClosepopupPMTax() {
            $('#divTaxDetailsDialog').modal('hide');
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/PracManagement/Masters/PracticeMGMTSetup.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity-Branch"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/PracManagement/Masters/PMUser_List.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnTax" PostBackUrl="~/PracManagement/Masters/PMTaxDetails.aspx" runat="server" Text="Tax Details"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/PracManagement/Masters/PMUserCustomerMapping.aspx" runat="server" Text="User Customer Assingment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/PracManagement/Masters/PMCustomerServices.aspx" runat="server" Text="Customer Services"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>


    <asp:UpdatePanel ID="upTaxList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">
                        <section class="panel">
                        <div style="margin-bottom: 4px" />                      
                        <div class="col-md-12 colpadding0" style="margin-bottom: 5px;">
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-6 colpadding0" style="width:47%;">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <div class="col-md-6 colpadding0">
                                 <asp:DropDownListChosen runat="server" ID="ddlPageSize" Width="95%" AllowSingleDeselect="false" DisableSearchThreshold="2"
                                    CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownListChosen>
                                 </div>
                            </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 35%">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px; margin-left: -10%;">Customer</p>
                                </div>
                                <div class="col-md-9 colpadding0">

                                      <asp:DropDownListChosen ID="ddlCustomerList" AllowSingleDeselect="false" runat="server"   OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged"                                       
                                         DisableSearchThreshold="2" CssClass="form-control" Width="100%">
                                    </asp:DropDownListChosen>                                   
                                </div>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 35%">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Filter</p>
                                </div>
                                <div class="col-md-9 colpadding0" style="margin-left: -5%;">
                                    <asp:TextBox runat="server" ID="tbxFilter" class="form-control" Style="height: 32px;" MaxLength="50" AutoPostBack="true"
                                        OnTextChanged="tbxFilter_TextChanged" />
                                </div>
                            </div>
                           
                            <div class="col-md-1 colpadding0 entrycount" style="margin-top: 5px; padding-left: 18px; float: right;">
                                <asp:LinkButton Text="Add New" runat="server" ID="btnAddTax" OnClick="btnAddTax_Click" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                ValidationGroup="TaxValidationGroupPage" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="TaxValidationGroupPage" Display="None" />
                        </div>
                        <div style="margin-bottom: 4px;">                                                                                           
                            <asp:GridView runat="server" ID="grdTax" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                   PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"                              
                                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdTax_RowCommand" OnPageIndexChanging="grdTax_PageIndexChanging">
                                <Columns>                                    
                                    <asp:BoundField DataField="TaxType" HeaderText="Tax Type" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="RegNo" HeaderText="Reg No"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="UserId" HeaderText="UserId"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="GovernmentLink" HeaderText="Government Link" />
                                   
                                  
                                    <asp:TemplateField ItemStyle-Width="110px" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CommandName="EDIT_TAX" ID="lbtnEdit" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png" alt="Edit Tax Details" title="Edit Tax Details" /></asp:LinkButton>                                    
          
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <HeaderStyle BackColor="#ECF0F1" />
                                <PagerSettings Visible="false" />
                                <PagerTemplate>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <div style="float: right;">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                    class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page                                          
                                        </p>
                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                         </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="modal fade" id="divTaxDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upTaxes" runat="server" UpdateMode="Conditional" OnLoad="upTaxes_Load">
                        <ContentTemplate>
                            <div style="margin: 5px">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="TaxValidationGroup" />
                                    <asp:CustomValidator ID="CvDuplicateEntry1" runat="server" EnableClientScript="False"
                                        ValidationGroup="TaxValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />

                                </div>
                                 <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Tax Type</label>
                                    <asp:TextBox runat="server" ID="tbxtaxType" Style="width: 390px;" MaxLength="200" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Tax Type can not be empty."
                                        ControlToValidate="tbxtaxType" runat="server" ValidationGroup="TaxValidationGroup"
                                        Display="None" />                                    
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Registration Number</label>
                                    <asp:TextBox runat="server" ID="tbxRegistrationNo" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="100" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Registration Number can not be empty."
                                        ControlToValidate="tbxRegistrationNo" runat="server" ValidationGroup="TaxValidationGroup"
                                        Display="None" />
                                   </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        UserID</label>
                                    <asp:TextBox runat="server" ID="tbxUserID" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="100" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="UserID can not be empty."
                                        ControlToValidate="tbxUserID" runat="server" ValidationGroup="TaxValidationGroup"
                                        Display="None" />
                                    
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Password</label>
                                    <asp:TextBox runat="server" ID="tbxPassword" Style="width: 390px;" CssClass="form-control"
                                        MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Password can not be empty."
                                        ControlToValidate="tbxPassword" runat="server" ValidationGroup="TaxValidationGroup"
                                        Display="None" />
                                  
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Government Link</label>
                                    <asp:TextBox runat="server" ID="tbxGovernmentLink" Style="width: 390px;" CssClass="form-control" />                                   
                                </div>
                                                                                                                
                                <div style="margin-bottom: 7px; float: right; margin-right: 257px; margin-top: 10px;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" CausesValidation="true"
                                        ValidationGroup="TaxValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" OnClientClick="fClosepopupPMTax()" />
                                </div>                                
                            </div>
                           <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                            </div>
                             <div class="clearfix" style="height: 50px">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
