﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PracticeManagement.Master" AutoEventWireup="true" CodeBehind="ServiceMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters.ServiceMaster" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .EDIT_CUSTOMER_BRANCH a:hover {
            background-color: blue;
        }

        .table {
            margin-bottom: 0px;
        }

        .float-child {
            width: 61%;
            float: left;
        }

        .float-child1 {
            width: 30%;
            float: left;
        }

        .vertical-center {
            margin: 0;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            Bindgrid();
        });
        function fopenpopup() {
            $('#divCustomerBranchesDialog').modal('show');
        };

        function fClosepopup() {
            $('#divCustomerBranchesDialog').modal('hide');
        };

        function fopenpopup1() {
            $('#divComplianceScheduleDialog').modal('show');
        };

        function fClosepopup1() {
            $('#divComplianceScheduleDialog').modal('hide');
        };

        
        function CloseWin() {
            $('#Newaddremider').modal('show');
        };

        var record = 0;
        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: '../activity/getAll'
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    if (this.ReminderStatus == 0) { template: "#=pending" }
                },
                columns: [
               {
                   field: "ID", title: 'ID', hidden: true,
                   attributes: {
                       style: 'white-space: nowrap;'

                   }, filterable: {
                       extra: false,
                       operators: {
                           string: {
                               eq: "Is equal to",
                               neq: "Is not equal to",
                               contains: "Contains"
                           }
                       }
                   }, width: "15%",
               },

                 {
                     field: "Name", title: 'Product',
                     attributes: {
                         style: 'white-space: nowrap;'

                     }, filterable: {
                         extra: false,
                         operators: {
                             string: {
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         }
                     }, width: "10%",
                 },
                   {
                       field: "ActivityName", title: 'Activities',
                       attributes: {
                           style: 'white-space: nowrap;'

                       }, filterable: {
                           extra: false,
                           operators: {
                               string: {
                                   eq: "Is equal to",
                                   neq: "Is not equal to",
                                   contains: "Contains"
                               }
                           }
                       }, width: "15%",
                   },
                {
                    command: [
                        { name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit" },
                        { name: "edit1", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-deleteuser" }], title: "Action", lock: true, width: "5%;",// width: 150,
                }
                ]
            });
            $("#grid").kendoTooltip({
                filter: "td:nth-child(2)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Delete";
                }
            });
        }
        //Edit User Deatais
        $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
            $('#popup').modal('show');
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            $scope.DisplayForm = true;
            $scope.addminus = false;
            $scope.addplus = false;
            $scope.editminus = true;
            $scope.AddAlert = false;
            $scope.EditAlert = false;
            $scope.DeleteAlert = false;
            $http({
                method: 'POST',
                url: '../activity/getActivityByNo/',
                data: JSON.stringify({ id: item.ID })
            }).success(function (data, status, headers, config) {
                debugger;
                $scope.ID = data[0].ID;
                $scope.ActivityName = data[0].ActivityName;
                $scope.ProductID = data[0].ProductID;
                $scope.Action = "Update";
            }).error(function (data, status, headers, config) {
                $scope.message = 'Unexpected Error';
            });
        });

        //Delete User Details
        $(document).on("click", "#grid tbody tr .ob-deleteuser", function (e) {

            var retVal = confirm("Do you want to Delete Record..?");
            if (retVal == true) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $http({
                    method: 'POST',
                    url: '/activity/DeleteActivity/',
                    data: JSON.stringify({ Id: item.ID })
                }).success(function (data, status, headers, config) {
                    $scope.delete = data;
                    $scope.DeleteAlert = true;
                    $scope.AddAlert = false;
                    $scope.EditAlert = false;
                    $scope.popup = false;
                    $scope.IsEmailValid = true;
                    Bindgrid();
                }).error(function (data, status, headers, config) {
                    $scope.message = 'Unexpected Error';
                });
            }
            return true;
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upCustomerBranchList" runat="server" UpdateMode="Conditional" OnLoad="upCustomerBranchList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                              <div class="col-md-12 colpadding0" style="margin-bottom: 5px;">
                                  <div style="text-align:left;" >                                   
                                  </div>
                                </div>

                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-6 colpadding0">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                    <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div>                           
                            <div class="col-md-3 colpadding0 entrycount" id="divFilter" runat="server" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0" >
                                    <p style="color: #999; margin-top: 5px;">Filter</p>
                                </div>
                                <asp:TextBox runat="server" ID="tbxFilter" class="form-control" Style="Width:150px" MaxLength="50" AutoPostBack="true"
                                OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                            <div style="text-align:right"> 
                                <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="fopenpopup()" ID="btnAddCustomerBranch" OnClick="btnAddCustomerBranch_Click" />                                
                            </div> 
                     
                            <div style="margin-bottom: 4px;"> 
                                &nbsp
                                <asp:GridView runat="server" ID="grdCustomer" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" 
                                DataKeyNames="ID"  OnSorting="grdCustomer_Sorting" OnRowCommand="grdCustomer_RowCommand" OnPageIndexChanging="grdCustomer_PageIndexChanging" >
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ServiceCode" HeaderText="ServiceCode" />                              
                                     <asp:BoundField DataField="ServiceDetails" HeaderText="ServiceDetails" />
                                     <asp:BoundField DataField="ServiceName" HeaderText="ServiceName" />
                                    <asp:BoundField DataField="FrequencyName" HeaderText="Frequency" />
                                    
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Justify" HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1"   runat="server"   ToolTip="Edit"
                                                  CommandName="EDIT_CUSTOMER"  CommandArgument='<%# Eval("ID") %>'>
                                                <img src="../Images/edit_icon_new.png" data-toggle="tooltip" data-placement="top" alt="Edit"  title="Edit"/>

                                                </asp:LinkButton>
                                                 <asp:LinkButton ID="LinkButton3" runat="server"  ToolTip="Display Schedule Information" 
                                                     CommandName="SHOW_SCHEDULE" CommandArgument='<%# Eval("ID") %>'
                                                     
                                                       Visible='<%# ViewSchedule(Eval("Frequency")) %>'>
                                              <img src="../Images/icon_viewA.png" data-toggle="tooltip" data-placement="top" alt="Display Schedule Information" 
                                                   title="Display Schedule Information" /></asp:LinkButton>

                                                                                    
                                        </ItemTemplate>                       
                                    </asp:TemplateField>
                                      
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" /> 
                                <HeaderStyle BackColor="#ECF0F1" />
                                <PagerSettings Visible="false" />             
                                <PagerTemplate>
                                
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                </asp:GridView>
                                  <div style="float: right;">
                                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                        class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                        </asp:DropDownListChosen>                                       
                                  </div>
                            </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0"  style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">                                    
                                    <div class="table-paging-text" style="float:right;">
                                        <p>
                                            Page
                                          
                                        </p>
                                    </div>                                    
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="divCustomerBranchesDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">

                    <asp:UpdatePanel ID="upCustomerBranches" runat="server" UpdateMode="Conditional" OnLoad="upCustomerBranches_Load">
                        <ContentTemplate>
                            <div>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="CustomerBranchValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="CustomerBranchValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>

                                <div runat="server" id="divState" style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Service Code</label>
                                    <asp:DropDownList runat="server" ID="ddlServiceCode" Style="padding: 0px; margin: 0px; width: 300px;"
                                        CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlServiceCode_SelectedIndexChanged" />
                                    <asp:CompareValidator ErrorMessage="Please select Service Code." ControlToValidate="ddlServiceCode"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                        Display="None" />
                                </div>

                                <div class="clearfix"></div>
                                <div style="margin-bottom: 7px; margin-top: 7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Service Code Details</label>
                                    <asp:TextBox runat="server" ID="txbservicecodedetails" Enabled="false" CssClass="form-control" autocomplete="off" Style="width: 300px;" MaxLength="500" />

                                </div>

                                <div class="clearfix"></div>
                                <div style="margin-bottom: 7px; margin-top: 7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Service Type</label>
                                    <asp:TextBox runat="server" ID="tbxName" CssClass="form-control" autocomplete="off" Style="width: 300px;" MaxLength="500" />
                                    <asp:RequiredFieldValidator ErrorMessage="Please Enter Name." ControlToValidate="tbxName"
                                        runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                                </div>
                                <div class="clearfix"></div>
                                <div style="margin-bottom: 7px; margin-top: 7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Frequency</label>
                                    <asp:DropDownList runat="server" ID="ddlFrequency" Style="padding: 0px; margin: 0px; width: 300px;"
                                        CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged" />
                                    <asp:CompareValidator ErrorMessage="Please select Frequency." ControlToValidate="ddlFrequency"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                        Display="None" />
                                </div>

                                <div style="margin-bottom: 7px; margin-left: 200px; margin-top: 25px;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                        ValidationGroup="CustomerBranchValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                </div>
                            </div>
                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                            </div>
                            <div class="clearfix" style="height: 50px">
                            </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>





    <div class="modal fade" id="divComplianceScheduleDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <%--OnLoad="upComplianceScheduleDialog_Load"--%>
                    <asp:UpdatePanel ID="upComplianceScheduleDialog" runat="server" UpdateMode="Conditional" >
                        <ContentTemplate>
                            <div>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ComplianceValidationGroup" />
                                    <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>


                                <div style="margin-bottom: 7px; margin-top: 7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style=" display: block;  font-size: 13px; color: #333; text-align:center;">
                                     <h2>   Billing Schedule Dates</h2></label>                                   
                                </div>
                                <div class="clearfix"></div>
                                 

                                <div class="clearfix"></div>
                                <asp:UpdatePanel runat="server" ID="upSchedulerRepeter" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div runat="server" style="margin-bottom: 7px; padding-left: 130px">
                                            <asp:Repeater runat="server" ID="repComplianceSchedule" OnItemDataBound="repComplianceSchedule_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table>
                                                        <tr>
                                                            <th style="width: 150px; border: 1px solid #666;     text-align: center;">For Period
                                                            </th>
                                                            <th style="width: 200px; border: 1px solid #666;     text-align: center;">Day
                                                            </th>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div style="margin-bottom: 7px">
                                                        <tr>
                                                            <td align="center" style="border: 1px solid gray">
                                                                <%# Eval("ForMonthName")%>
                                                                <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnForMonth" Value='<%# Eval("ForMonth") %>' />
                                                            </td>
                                                            <td align="center" style="border: 1px solid gray">

                                                                <asp:DropDownList runat="server" ID="ddlMonths" Style="padding: 0px; margin: 0px; height: 22px; width: 100px;"
                                                                    CssClass="txtbox" DataTextField="Name" DataValueField="ID" OnSelectedIndexChanged="ddlMonths_SelectedIndexChanged"
                                                                    AutoPostBack="true" />
                                                                <asp:DropDownList runat="server" ID="ddlDays" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                                                    CssClass="txtbox" DataTextField="Name" DataValueField="ID" />
                                                            </td>
                                                        </tr>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <div class="clearfix"></div>


                                <div style="margin-bottom: 7px; margin-left: 200px; margin-top: 25px;">
                                    <asp:Button Text="Save" runat="server" ID="btnSaveSchedule" OnClick="btnSaveSchedule_Click" CssClass="btn btn-primary"
                                        ValidationGroup="ComplianceValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="Button3" CssClass="btn btn-primary" data-dismiss="modal" />
                                </div>
                            </div>
                                                      
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
        $(document).ready(function () {
            setactivemenu('Service Master');
            fhead('Service Master');
        });
    </script>
</asp:Content>
