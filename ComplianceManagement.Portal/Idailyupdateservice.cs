﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "Idailyupdateservice" in both code and config file together.
    [ServiceContract]
 
    public interface Idailyupdateservice
    {

         [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
         [OperationContract]
          // [WebGet(ResponseFormat = WebMessageFormat.Json)]
          List<DailyUpdate> DailyUpdate();
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        // [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<emailview> Mails(int Userid);

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        // [WebGet(ResponseFormat = WebMessageFormat.Json)]
        int Mailscnt(int Userid);

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        //[WebGet(ResponseFormat = WebMessageFormat.Json)]
         List<NewsLetter> DailyNewsLetter();

        /// <summary>
        /// For switchin on and off widgets
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET")]
        bool widget(int id,string control,bool yes);
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]

        bool ticket(string subject, string message, int requestor, string tickettype);
        /// <summary>
        /// Whether API
        /// </summary>  
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        RootObject GetWhetherInfo(string city);

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        // [WebGet(ResponseFormat = WebMessageFormat.Json)]
        widget getwidgets(string userid);

        //[WebInvoke(Method = "Post", ResponseFormat = WebMessageFormat.Json,BodyStyle =WebMessageBodyStyle.Wrapped)]
        //[OperationContract]
        // [WebInvoke(Method = "POST", UriTemplate = "/UserMessaging?recepient={recepient}&sender={sender}&subject={subject}&message={message}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]

        // [WebGet(ResponseFormat = WebMessageFormat.Json)]
        bool UserMessaging(string recepient,int sender,string subject,string message);



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        // [WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<String> BindNotifications(int userid);

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<String> BindReports(string type,string subtype, int userid);

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        // [WebGet(ResponseFormat = WebMessageFormat.Json)]
        int NotificationMail(int Userid);


        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool upcolor(string high, int sender, string medium, string low , string critical);

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        String BindAutoComplete(string SearchText, int CustomerID, int UserID, String Role);


        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped,  RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]

        int createticket(string name, string email, string subject, string message);
    }
}
