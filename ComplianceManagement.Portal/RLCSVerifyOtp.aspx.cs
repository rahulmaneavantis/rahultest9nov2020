﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class RLCSVerifyOtp : System.Web.UI.Page
    {
        public static string key = "A!9HHhi%XjjYY4YP2@Nob009X";
        static int WrongOTPCount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    divVerifyotp.Visible = false;
                    spangenarteotp.Visible = false;
                    Labelmsg.Text = "will be send";                    
                    int userId = -1;
                    string DecryptedID = string.Empty;

                    if (!string.IsNullOrEmpty(Request.QueryString["Id"])
                        && !string.IsNullOrEmpty(Request.QueryString["AID"])
                        && !string.IsNullOrEmpty(Request.QueryString["SID"]))
                    {
                        DecryptedID = Decrypt(Request.QueryString["Id"].Replace(" ", "+"));
                        if (!string.IsNullOrEmpty(DecryptedID))
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                User user = (from row in entities.Users
                                             where row.Email == DecryptedID
                                               && row.IsActive == true
                                               && row.IsDeleted == false
                                             select row).FirstOrDefault();

                                if (user != null)
                                {

                                    userId = Convert.ToInt32(user.ID);
                                    UserEmail.Text = user.Email;
                                    FirstNameUser.Text = user.FirstName.ToString();
                                    UserId.Text = userId.ToString();
                                    Label1.Text = user.Email.Substring(0, 1).ToUpper();

                                    #region Email,Time,contact number display
                                    // For Email Id Logic Set.
                                    string[] spl = Convert.ToString(user.Email).Split('@');
                                    int lenthusername = (spl[0].Length / 2);
                                    int mod = spl[0].Length % 2;
                                    if (mod > 0)
                                    {
                                        lenthusername = lenthusername - mod;
                                    }
                                    string beforemanupilatedemail = "";
                                    if (lenthusername == 1)
                                    {
                                        beforemanupilatedemail = spl[0].Substring(lenthusername);
                                    }
                                    else
                                    {
                                        beforemanupilatedemail = spl[0].Substring(lenthusername + 1);
                                    }

                                    string appendstar = "";
                                    for (int i = 0; i < beforemanupilatedemail.Length; i++) /*lenthusername*/
                                    {
                                        appendstar += "*";
                                    }

                                    string manupilatedemail = spl[0].Replace(beforemanupilatedemail, appendstar);

                                    string[] spl1 = spl[1].Split('.');
                                    int lenthatname = (spl1[0].Length / 2);
                                    int modat = spl1[0].Length % 2;
                                    if (modat > 0)
                                    {
                                        lenthatname = lenthatname - modat;
                                    }

                                    string beforatemail = "";
                                    if (lenthatname == 1)
                                    {
                                        beforatemail = spl1[0].Substring(lenthatname);
                                    }
                                    else
                                    {
                                        beforatemail = spl1[0].Substring(lenthatname + 1);
                                    }
                                    string appendstar1 = "";
                                    for (int i = 0; i < beforatemail.Length; i++) /*lenthatname*/
                                    {
                                        appendstar1 += "*";
                                    }

                                    string manupilatedatemail = spl1[0].Replace(beforatemail, appendstar1);
                                    string emailid = manupilatedemail;

                                    DateTime NewTime = DateTime.Now.AddMinutes(30);
                                    Time.Text = NewTime.Hour + ":" + NewTime.Minute + NewTime.ToString(" tt");
                                    email.Text = Convert.ToString(user.Email).Replace(spl[0], manupilatedemail).Replace(spl1[0], manupilatedatemail);

                                    if (!string.IsNullOrEmpty(Convert.ToString(user.ContactNumber)))
                                    {
                                        // For Mobile Number Logic Set.
                                        if (Convert.ToString(user.ContactNumber).Length == 10 && (Convert.ToString(user.ContactNumber).StartsWith("9") || Convert.ToString(user.ContactNumber).StartsWith("8") || Convert.ToString(user.ContactNumber).StartsWith("7")))
                                        {
                                            string s = Convert.ToString(user.ContactNumber);
                                            int l = s.Length;
                                            s = s.Substring(l - 4);
                                            string r = new string('*', l - 4);
                                            //r = r + s;
                                            mobileno.Text = r + s;
                                        }
                                        else
                                        {
                                            mobileno.Text = "";
                                            //cvLogin.IsValid = false;
                                            //cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                                        }
                                    }
                                    else
                                    {
                                        mobileno.Text = "";
                                        //cvLogin.IsValid = false;
                                        //cvLogin.ErrorMessage = "You don't seem to have a correct registered mobile number with us. However OTP sent on your registered email. Please update your mobile number to use this feature in future.";
                                    }
                                    #endregion                                   
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                Response.Redirect("Blank.aspx", false);
            }
        }



        public static string Encrypt(string text)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["EncryptKey"].ToString()));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateEncryptor())
                    {
                        byte[] textBytes = UTF8Encoding.UTF8.GetBytes(text);
                        byte[] bytes = transform.TransformFinalBlock(textBytes, 0, textBytes.Length);
                        return Convert.ToBase64String(bytes, 0, bytes.Length);
                    }
                }
            }
        }

        public static string Decrypt(string cipher)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateDecryptor())
                    {
                        byte[] cipherBytes = Convert.FromBase64String(cipher);
                        byte[] bytes = transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
                        return UTF8Encoding.UTF8.GetString(bytes);
                    }
                }
            }
        }

        protected void btnOTP_Click(object sender, EventArgs e)
        {

            try
            {
                int userID = -1;
                if (!string.IsNullOrEmpty(UserId.Text))
                {
                    userID = Convert.ToInt32(UserId.Text);
                }
                if (userID != null)
                {
                    if (VerifyOTPManagement.verifyOTP(Convert.ToInt32(txtOTP.Text), userID))
                    {
                        User user = UserManagement.GetByID(userID);

                        VerifyOTP OTPData = VerifyOTPManagement.GetByID(Convert.ToInt32(userID));
                        TimeSpan span = DateTime.Now.Subtract(OTPData.CreatedOn.Value);
                        if (0 <= span.Minutes && span.Minutes <= 30)
                        {
                            VerifyOTPManagement.UpdateVerifiedFlag(Convert.ToInt32(userID), Convert.ToInt32(txtOTP.Text));

                            ProcessAuthenticationInformation(user);                          
                        }
                        else
                        {
                            cvLogin.ErrorMessage = "OTP Expired.";
                            cvLogin.IsValid = false;
                            return;
                        }
                    }
                    else
                    {
                        if (WrongOTPCount < 3)
                        {
                            cvLogin.ErrorMessage = "Invalid OTP, Please Enter Again.";
                            cvLogin.IsValid = false;
                            WrongOTPCount++;
                            return;
                        }
                        else
                        {
                            cvLogin.ErrorMessage = "Invalid OTP, Please Enter Again.";
                            cvLogin.IsValid = false;
                            return;
                        }
                    }
                }
                else
                {
                    cvLogin.ErrorMessage = "Please Enter Correct OTP.";
                    cvLogin.IsValid = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvLogin.IsValid = false;
                cvLogin.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void ProcessAuthenticationInformation(User user)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Id"])
                           && !string.IsNullOrEmpty(Request.QueryString["AID"])
                           && !string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    string AuditID = Convert.ToString(Request.QueryString["AID"]);
                    string ScheduleOnID = Convert.ToString(Request.QueryString["SID"]);

                    string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                    string role = RoleManagement.GetByID(user.RoleID).Code;
                    int checkInternalapplicable = 0;
                    int checkTaskapplicable = 0;
                    int checkLabelApplicable = 0;
                    int checkVerticalapplicable = 0;
                    bool IsPaymentCustomer = false;
                    if (user.CustomerID == null)
                    {
                        checkInternalapplicable = 2;
                        checkTaskapplicable = 2;
                        checkVerticalapplicable = 2;
                        checkLabelApplicable = 2;
                    }
                    else
                    {
                        Customer c = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                        var IsInternalComplianceApplicable = c.IComplianceApplicable;
                        if (IsInternalComplianceApplicable != -1)
                        {
                            checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                        }
                        var IsTaskApplicable = c.TaskApplicable;
                        if (IsTaskApplicable != -1)
                        {
                            checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                        }
                        var IsVerticlApplicable = c.VerticalApplicable;
                        if (IsVerticlApplicable != null)
                        {
                            if (IsVerticlApplicable != -1)
                            {
                                checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                            }
                        }
                        //if (c.IsPayment != null)
                        //{
                        //    IsPaymentCustomer = Convert.ToBoolean(c.IsPayment);
                        //}
                        var IsLabelApplicable = c.IsLabelApplicable;
                        if (IsLabelApplicable != -1)
                        {
                            checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                        }
                    }

                    User userToUpdate = new User();
                    userToUpdate.ID = user.ID;
                    userToUpdate.LastLoginTime = DateTime.Now;
                    userToUpdate.WrongAttempt = 0;
                    UserManagement.Update(userToUpdate);

                    if (role.Equals("HVEND"))
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty,checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable), false);                        
                        TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                        Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                        Response.Redirect("~/RLCSVendorAudit/RLCSVendorAuditChecklist.aspx?AID=" + AuditID + "&SOID=" + ScheduleOnID + "&Flag=byOtp", false);
                    }

                    DateTime LastPasswordChangedDate = Convert.ToDateTime(UserManagement.GetByID(Convert.ToInt32(user.ID)).ChangPasswordDate);
                    DateTime currentDate = DateTime.Now;
                    LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                    int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                    int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                    if (dateDifference == noDays || dateDifference > noDays)
                    {
                        Session["ChangePassword"] = true;
                        Response.Redirect("~/Account/ChangePassword.aspx", false);
                    }
                    else
                    {
                        Session["ChangePassword"] = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkResendOTP_Click(object sender, EventArgs e)
        {
            bool Success = false;
            int userID = -1;
            if (!string.IsNullOrEmpty(UserId.Text))
            {
                userID = Convert.ToInt32(UserId.Text);
            }
            if (userID != null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    User user = (from row in entities.Users
                                 where (row.ID == userID)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                 select row).FirstOrDefault();
                    if (user != null)
                    {
                        if (user.IsActive)
                        {
                            string ipaddress = string.Empty;
                            try
                            {
                                ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                                if (ipaddress == "" || ipaddress == null)
                                    ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            Random random = new Random();
                            int value = random.Next(1000000);
                            long Contact;

                            VerifyOTP OTPData = new VerifyOTP()
                            {
                                UserId = Convert.ToInt32(user.ID),
                                EmailId = user.Email.ToString(),
                                OTP = Convert.ToInt32(value),
                                CreatedOn = DateTime.Now,
                                IsVerified = false,
                                IPAddress = ipaddress
                            };
                            bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                            if (OTPresult)
                            {
                                OTPData.MobileNo = Contact;
                            }
                            else
                            {
                                OTPData.MobileNo = 0;
                            }
                            VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            try
                            {
                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                                Success = true;
                            }
                            catch (Exception ex)
                            {
                                Success = false;
                            }
                            if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
                            {
                                try
                                {
                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.");
                                    Success = true;
                                }
                                catch (Exception ex)
                                {
                                    Success = false;
                                }
                            }
                            if (Success)
                            {
                                cvLogin.ErrorMessage = "OTP Sent Successfully.";
                                cvLogin.IsValid = false;
                            }
                            else
                            {
                                //cvLogin.ErrorMessage = "OTP Sent Successfully.";
                                //cvLogin.IsValid = false;
                            }
                        }
                    }
                }
            }
        }

        protected void BtnGenerate_Click(object sender, EventArgs e)
        {
            divgenarteotp.Visible = false;
            divVerifyotp.Visible = true;
            spangenarteotp.Visible = true;
            Labelmsg.Text = "has been sent";

            bool Success = false;
            int userID = -1;
            if (!string.IsNullOrEmpty(UserId.Text))
            {
                userID = Convert.ToInt32(UserId.Text);
            }
            if (userID != null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    User user = (from row in entities.Users
                                 where (row.ID == userID)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                 select row).FirstOrDefault();
                    if (user != null)
                    {
                        if (user.IsActive)
                        {
                            string ipaddress = string.Empty;
                            try
                            {
                                ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                                if (ipaddress == "" || ipaddress == null)
                                    ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            Random random = new Random();
                            int value = random.Next(1000000);
                            long Contact;

                            VerifyOTP OTPData = new VerifyOTP()
                            {
                                UserId = Convert.ToInt32(user.ID),
                                EmailId = user.Email.ToString(),
                                OTP = Convert.ToInt32(value),
                                CreatedOn = DateTime.Now,
                                IsVerified = false,
                                IPAddress = ipaddress
                            };
                            bool OTPresult = long.TryParse(user.ContactNumber, out Contact);
                            if (OTPresult)
                            {
                                OTPData.MobileNo = Contact;
                            }
                            else
                            {
                                OTPData.MobileNo = 0;
                            }
                            VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.
                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                            try
                            {
                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Avantis");
                                Success = true;
                            }
                            catch (Exception ex)
                            {
                                Success = false;
                            }
                            if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
                            {
                                try
                                {
                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.");
                                    Success = true;
                                }
                                catch (Exception ex)
                                {
                                    Success = false;
                                }
                            }
                            if (Success)
                            {
                                cvLogin.ErrorMessage = "OTP Sent Successfully.";
                                cvLogin.IsValid = false;
                            }
                            else
                            {
                                //cvLogin.ErrorMessage = "OTP Sent Successfully.";
                                //cvLogin.IsValid = false;
                            }
                        }
                    }
                }
            }
        }
    }
}