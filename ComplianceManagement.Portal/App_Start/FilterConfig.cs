﻿using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new PageViewFilter());
        }
    }
}
