﻿using System.Web;
using System.Web.Optimization;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                         "~/Newjs/jquery.dataTables.min.js",
                        "~/Newjs/dataTables.bootstrap.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Newjs/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/gjquery").Include(
                 "~/Newjs/jquery.1113_.min.js",
                "~/Newjs/jquery.3.3.1.js"
                ));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/NewCss/css").Include(
                      "~/NewCSS/bootstrap.min.css",
                      "~/NewCSS/dataTables.bootstrap.min.css"));

            #region Kendo 2014.2.903
            bundles.Add(new ScriptBundle("~/Scripts/kendo/2014.2.903/jskendo").Include(
                      "~/Scripts/kendo/2014.2.903/kendo.web.min.js",
                      "~/Scripts/kendo/2014.2.903/kendo.all.min.js",
                      "~/Scripts/kendo/2014.2.903/kendo.aspnetmvc.min.js"));

            bundles.Add(new StyleBundle("~/Content/css/kendo/2014.2.903/cssKendo").Include(
                      "~/Content/css/kendo/2014.2.903/kendo.common-bootstrap.min.css",
                      "~/Content/css/kendo/2014.2.903/kendo.bootstrap.min.css"));
            #endregion


            #region Kendo 2018.3.1017
            bundles.Add(new ScriptBundle("~/Content/2018.3.1017/js/jskendo").Include(
                      "~/Content/2018.3.1017/js/jszip.min.js",
                      "~/Content/2018.3.1017/js/kendo.all.min.js",
                      "~/Content/2018.3.1017/js/kendo.aspnetmvc.min.js",
                      "~/Content/2018.3.1017/js/jquery.unobtrusive-ajax.min.js",
                      "~/Content/2018.3.1017/js/moment.min.js"));

            bundles.Add(new StyleBundle("~/Content/2018.3.1017/css/cssKendo").Include(
                      "~/Content/2018.3.1017/css/kendo.common.min.css",
                      "~/Content/2018.3.1017/css/kendo.default.min.css"));
            #endregion

        }
    }
}
