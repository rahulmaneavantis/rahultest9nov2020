﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="MgmtAuditMyReportNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages.MgmtAuditMyReportNew" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script type="text/javascript">                               
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        // For Enter Only Number Only.
      

        function ShowViewDocument(file) {
            $('#divViewDocument').modal('show');
         
        }

        function fopenSampleFile(file) {
            $('#ContentPlaceHolder1_docViewerAll').attr('src', "../Common/blank.html");
            $('#SampleFilePopUp').modal('show');
            $('#ContentPlaceHolder1_docViewerAll').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        function fopendocfile() {
            $('#SampleFilePopUp').modal('show');
        }
    
    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <div style="margin-bottom: 4px">
                            <asp:CustomValidator ID="cvDuplicateEntry" CssClass="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblMsg1" CssClass="alert alert-block alert-danger fade in" runat="server" Visible="false"></asp:Label>
                        </div>
                        <section class="panel">
                           <header class="panel-heading tab-bg-primary ">                              
                            </header>
                            <div class="clearfix"></div>
                            <div class="panel-body">
                            <div class="col-md-12 colpadding0">

                                <div class="col-md-10 colpadding0" style="width:80%;">
                                    
                                    <div class="col-md-2 colpadding0" style="width:16%;">
                                        <div class="col-md-3 colpadding0" >
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 60px;margin-left:10px; float: left" 
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                                            <asp:ListItem Text="5" Selected="True"/>
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>                                        
                                    </div>

                                    <div class="col-md-2 colpadding0"> 
                                         <asp:DropDownList runat="server" ID="ddlStatus" class="form-control m-bot15" Style="width: 100px;margin-left:10px; float: left" 
                                            AutoPostBack="true">
                                            <asp:ListItem Text="All" Value="-1" Selected="True"/>
                                            <asp:ListItem Text="Open" value="0"/>
                                            <asp:ListItem Text="Closed" value="1"/>                                        
                                        </asp:DropDownList>  
                                    </div>

                                    <div class="col-md-8 colpadding0"> 
                                            <div style="float:left;margin-right: 1%;">
                                                <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 373px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                                CssClass="txtbox" />                                                       
                                                <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                    <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="325px"   NodeStyle-ForeColor="#8e8e93"
                                                    Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                                                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                </asp:TreeView>
                                            </div>  
                                            </div>           
                                        </div>
                               
                                </div>
                                   <div class="col-md-2 colpadding0" style="text-align: right; float: left;width: 19%;">
                                    
                                 
                                        <div class="col-md-6 colpadding0" style="margin-left: 10px;width: 41%;">               
                                        <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search"  runat="server" Text="Apply" OnClick="btnSearch_Click"/> 
                                        </div>
                                        <div class="col-md-6 colpadding0">   
                                             <asp:LinkButton runat="server" ID="btnAddNew" OnClick="btnAddNew_Click" CssClass="btn btn-primary"
                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Export Report">
                                                Export Report</asp:LinkButton>
                                        </div>
                                 
                                </div>  
                             
                               
                                <!-- Advance Search scrum-->
                                 <div class="clearfix"></div>
                            <div class="col-md-12 AdvanceSearchScrum">
                                 
                                    <div runat="server" id="DivRecordsScrum" style="float: right;">
                                        <p style="padding-right: 0px !Important;">
                                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                            </div>
                            </div>
                            </div>
                        <div class="clearfix"></div>                                                                      
                        
                        <div id="ReviewerGrids" runat="server">                               
                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdReviewerComplianceDocument" AutoGenerateColumns="false" CssClass="table" GridLines="None" BorderWidth="0px"
                                    CellPadding="4" Width="100%" OnRowCommand="grdReviewerComplianceDocument_RowCommand"
                                     AllowPaging="True" PageSize="5" AutoPostBack="true">
                                    <Columns>                                        
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>     
                                             <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>                                             
                                                  <div style="overflow: hidden; text-align:left; text-overflow: ellipsis; white-space: nowrap; width: 160px;">
                                                <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Eval("LocationName") %>'></asp:Label> 
                                                 <asp:Label ID="lblAuditID" runat="server" visible="false" Text='<%# Eval("Id") %>'></asp:Label>   
                                                      </div>                                        
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Vendor Name">
                                        <ItemTemplate>
                                            <div style="overflow: hidden;text-align:left; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblActName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AuditName">
                                            <ItemTemplate>                                             
                                                  <div style="overflow: hidden; text-align:left; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblAuditName" runat="server" Text='<%# Eval("AuditName") %>'></asp:Label>   
                                                      </div>                                        
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Audit Start Date">
                                            <ItemTemplate>                                             
                                                  <div style="overflow: hidden; text-align:left; text-overflow: ellipsis; white-space: nowrap; width: 120px;">
                                                <asp:Label ID="AuditStartDatelbl" runat="server" Text=' <%# Convert.ToDateTime(Eval("AuditStartDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>   
                                                      </div>                                        
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Audit End Date">
                                            <ItemTemplate>                                             
                                                  <div style="overflow: hidden; text-align:left; text-overflow: ellipsis; white-space: nowrap; width: 120px;">
                                                <asp:Label ID="AuditEndDatelbl" runat="server" Text=' <%# Convert.ToDateTime(Eval("AuditEndDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>   
                                                      </div>                                        
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                         <asp:LinkButton ID="lbkView" runat="server"  CommandName="ViewRecord" ToolTip="View" ><img src="../../Images/view-doc.png" alt="View"/></asp:LinkButton>
                                            </ItemTemplate>
                                           </asp:TemplateField>                        


                                       <%-- <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                              <asp:Button ID="btnAdd" Width="72px" ValidationGroup="ComplianceInstanceValidationGroup" Text="Save" runat="server" OnClientClick="return ValidateDaysAndIntermDays(this);" OnClick="btnAdd_Click" CssClass="btn btn-primary" />                                            
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>                               
                            </div>                                                     
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 15px;"></asp:Label></p>
                                    </div>                                    
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" style="margin-bottom: 20px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="Previous_Click"/>
                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>

                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="Next_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                     
                         </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnAddNew" />
        </Triggers>
    </asp:UpdatePanel>
  
  
     <div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 60%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 470px;">
                        <div class="col-md-12 colpadding0">
                            <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                        </div>
                     
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-2 colpadding0">
                                <table class="col-md-12">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                 <asp:Panel ID="pnl" ScrollBars="Vertical" Height="400px" runat="server">
                                                <asp:UpdatePanel ID="upLitigationDetails" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                       
                                                            <asp:GridView runat="server" ID="grdChecklist" OnRowCommand="grdChecklist_RowCommand" AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true"
                                                                AllowSorting="true" AllowPaging="false" Height="400px" CssClass="table" Width="100%">
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="Clause">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                <asp:Label ID="lblSrNo" runat="server" Text='<%# Eval("SrNo")%>'></asp:Label>
                                                                                <asp:Label ID="lblAuditDetailID" Visible="false" runat="server" ToolTip='<%# Eval("AuditDetailID") %>' Text='<%# Eval("AuditDetailID")%>'></asp:Label>
                                                                                <asp:Label ID="lblChecklistMappingID" Visible="false" runat="server" ToolTip='<%# Eval("CheckListMappingID") %>' Text='<%# Eval("CheckListMappingID")%>'></asp:Label>
                                                                                <asp:Label ID="lblChecklistID" Visible="false" runat="server" ToolTip='<%# Eval("CheckListID") %>' Text='<%# Eval("CheckListID")%>'></asp:Label>
                                                                                <asp:Label ID="lblAuditStartDate" Visible="false" runat="server" ToolTip='<%# Eval("AuditStartDate") %>' Text='<%# Eval("AuditStartDate")%>'></asp:Label>
                                                                                <asp:Label ID="lblAuditEndDate" Visible="false" runat="server" ToolTip='<%# Eval("AuditEndDate") %>' Text='<%# Eval("AuditEndDate")%>'></asp:Label>

                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Category Name" ItemStyle-Width="15px">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                <asp:Label ID="lblCategoryName" runat="server" Text='<%# Eval("CategoryName")%>' ToolTip='<%# Eval("CategoryName") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="CheckList Name" ItemStyle-Width="25px">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 350px;">
                                                                                <asp:Label ID="lblCheckListName" runat="server" Text='<%# Eval("CheckListName")%>' ToolTip='<%# Eval("CheckListName") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="Action">
                                                                        <ItemTemplate>
                                                                            <%-- <asp:LinkButton ID="lbkdocumentView" runat="server" CommandName="View" ToolTip="View"><img src="../../Images/view-doc.png" alt="View"/></asp:LinkButton>--%>
                                                                            <asp:LinkButton ID="lbkView" runat="server" ToolTip="View" CommandName="View" CommandArgument='<%# Eval("CheckListID") + "," + Eval("CheckListMappingID") %>'><img src="../../Images/view-doc.png" alt="View"/></asp:LinkButton>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>

                                                            </asp:GridView>
                                                   
                                                    </ContentTemplate>
                                                  
                                                </asp:UpdatePanel>
                                                          </asp:Panel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


      <div class="modal fade" id="SampleFilePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
    <div class="modal-dialog" style="width: 100%;">
        <div class="modal-content">
            <div class="modal-header">
                <%-- data-dismiss-modal="modal2"--%>
                <button type="button" class="close" onclick="$('#SampleFilePopUp').modal('hide');" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="height: 570px;">
               <%-- <div style="width: 100%;">--%>
                    <div style="float: left; width: 50%">
                        <table width="100%" style="text-align: left; margin-left: 5%;">
                            <thead>
                                <tr>
                                    <td valign="top">
                                        

                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdatleMode="Conditional">

                                            <ContentTemplate>
                                                <div>
                                                    <asp:ValidationSummary runat="server" CssClass="vdsummary" ForeColor="Red"
                                                        ValidationGroup="ComplianceValidationGroup1" />
                                                    <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" 
                                                        ValidationGroup="ComplianceValidationGroup1" ForeColor="Red" Display="None" />
                                                </div>
                                                <asp:Panel ID="Panel1" ScrollBars="Vertical" Width="93%" Height="450px" runat="server">
                                                <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true"
                                                    AllowSorting="true" OnRowUpdating="grdDocument_RowUpdating" OnRowCommand="grdDocument_RowCommand"
                                                    DataKeyNames="AuditDetailID" OnRowDataBound="grdDocument_RowDataBound" AllowPaging="false" CssClass="table" Width="90%">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="checkAll" runat="server" AutoPostBack="true" OnCheckedChanged="checkAll_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ShortDescription" ItemStyle-Width="10px">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                    <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription")%>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                                    <asp:Label ID="lblFileID" Visible="false" runat="server" ToolTip='<%# Eval("FileID") %>' Text='<%# Eval("FileID")%>'></asp:Label>
                                                                    <asp:Label ID="lblChecklistMappingID" Visible="false" runat="server" ToolTip='<%# Eval("CheckListMappingID") %>' Text='<%# Eval("CheckListMappingID")%>'></asp:Label>
                                                                    <asp:Label ID="lblChecklistID" Visible="false" runat="server" ToolTip='<%# Eval("CheckListID") %>' Text='<%# Eval("CheckListID")%>'></asp:Label>
                                                                    <asp:Label ID="lblType" Visible="false" runat="server" ToolTip='<%# Eval("DocumentType") %>' Text='<%# Eval("DocumentType")%>'></asp:Label>

                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="FileName" ItemStyle-Width="10px">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName")%>' ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ForMonth" ItemStyle-Width="10px">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("ForMonth")%>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="View">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="upFileUploadPanel1" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="lbkRecordView" runat="server" CommandArgument='<%# Eval("AuditDetailID") + "," + Eval("CheckListMappingID") + "," + Eval("DocumentType") + "," + Eval("FileID") %>' ToolTip="View" CommandName="View"><img src="../../Images/view-doc.png" alt="View"/></asp:LinkButton>
                                                                        <%--CommandArgument='<%# Eval("AuditDetailID") + "," + Eval("CheckListMappingID") + "," + Eval("AuditStartDate") + "," + Eval("AuditEndDate") + "," + Eval("CheckListName") %>'--%>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 55px;">
                                                                    <asp:LinkButton ID="lnkhidebutton" style="color: blue;text-decoration-line: underline;" runat="server" CommandName="HideUnHide" CommandArgument='<%# Eval("AuditDetailID") + "," + Eval("CheckListMappingID") + "," + Eval("DocumentType") + "," + Eval("FileID") %>' Text='<%# HideUnhide(Eval("AuditDetailID"), Eval("ChecklistMappingID"),Eval("FileID")) %>'></asp:LinkButton>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                    <RowStyle CssClass="clsROWgrid" />
                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                    <EmptyDataTemplate>
                                                        No Records Found.
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                                    </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdatleMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Button Text="Hide" ID="btnSaveHideUnHide" class="btn btn-search" runat="server" OnClientClick="Confirm();" OnClick="btnSaveHideUnHide_Click" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                   <%--     <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdatleMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Repeater ID="rptComplianceSampleView" runat="server"   OnItemCommand="rptComplianceSampleView_ItemCommand"
                                                    OnItemDataBound="rptComplianceSampleView_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="tblComplianceDocumnets">
                                                            <thead>
                                                                <th>Documents</th>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ChecklistID") +"," +Eval("Hideid") %>' ID="lblSampleView"
                                                                            runat="server" ToolTip='<%# Eval("Name") + "," + Eval("UserName") + "," + Eval("CreatedOn")%>' Text='<%# Eval("Name") %>'>
                                                                        </asp:LinkButton>
                                                                       <asp:linkbutton id="lblhide" runat="server" CommandArgument='<%#Eval("Hideid") %>'  Text='<%# Eval("DataHide") %>' CommandName="UnHide" ></asp:linkbutton>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="lblSampleView" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                            <Triggers>
                                            </Triggers>
                                        </asp:UpdatePanel>--%>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div style="float: right; width: 50%">
                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                            <iframe src="about:blank" id="docViewerAll" runat="server" width="100%" height="550px"></iframe>
                        </fieldset>
                    </div>
               <%-- </div>--%>
            </div>
        </div>
    </div>
</div>



 
    <script type="text/javascript">
        $(document).ready(function () {
            fhead('Audit Report');
            setactivemenu('Auditreport');
            fmaters1();
        });
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }


    </script>
</asp:Content>

