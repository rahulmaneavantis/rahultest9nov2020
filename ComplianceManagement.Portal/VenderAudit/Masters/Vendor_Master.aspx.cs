﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.VenderAudits;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.Masters
{
    public partial class Vendor_Master : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    //if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    //{
                        BindVendor();
                        if ((AuthenticationHelper.Role.Equals("CADMN")))
                        {
                            btnAddVendor.Visible = true;
                        }
                    //}
                    //else
                    //{
                    //    //added by rahul on 12 June 2018 Url Sequrity
                    //    FormsAuthentication.SignOut();
                    //    Session.Abandon();
                    //    FormsAuthentication.RedirectToLoginPage();
                    //}
                }
                else
                {
                    //added by rahul on 12 June 2018 Url Sequrity
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }           
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplienceVender.PageIndex = 0;
                BindVendor();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindVendor()
        {
            try
            {
                var VandorList = VenderAuditManagment.GetVendorListDtls(CustomerID);

                if (VandorList.Count > 0)
                    VandorList = VandorList.OrderBy(entry => entry.Name).ToList();
                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    VandorList = VandorList.Where(entry => entry.Name.ToUpper().Trim().Contains(tbxFilter.Text.ToUpper().Trim())).ToList();
                }

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);

                    }
                }
                Session["TotalRows"] = null;

                if (VandorList.Count > 0)
                {
                    grdComplienceVender.DataSource = VandorList;
                    Session["TotalRows"] = VandorList.Count;
                    grdComplienceVender.DataBind();
                }
                else
                {
                    grdComplienceVender.DataSource = VandorList;
                    grdComplienceVender.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        protected void upVendor_Load(object sender, EventArgs e)
        {

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxVendor.Text != "")
                {
                    AuditVendor newvendor = new AuditVendor()
                    {
                        Name = tbxVendor.Text.Trim(),
                        CreatedBy = AuthenticationHelper.UserID,
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        Isactive = false,
                        CreatedOn = DateTime.Now,
                    };
                    if ((int)ViewState["Mode"] == 1)
                    {
                        newvendor.ID = Convert.ToInt32(ViewState["VendorId"]);
                    }
                    if (VenderAuditManagment.Exists(newvendor))
                    {
                        cvDuplicateEntry.ErrorMessage = "Vendor name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        saveopo.Value = "true";
                        return;
                    }
                    if ((int)ViewState["Mode"] == 0)
                    {
                        VenderAuditManagment.CreateVendor(newvendor);
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        newvendor.UpdatedBy = AuthenticationHelper.UserID;
                        newvendor.UpdatedOn = DateTime.Now;
                        VenderAuditManagment.UpdateVendor(newvendor);
                    }

                    BindVendor();
                    saveopo.Value = "true";
                    cvDuplicateEntry.ErrorMessage = "Vendor saved sucessfully.";
                    cvDuplicateEntry.IsValid = false;
                    upVendor.Update();
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divVendorDialog\").dialog('close')", true);
                    //upVendor.Update();
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdComplienceVender_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void grdComplienceVender_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        protected void grdComplienceVender_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_Vendor"))
                {
                    int VendorID = Convert.ToInt32(e.CommandArgument);
                    var _objVendor = VenderAuditManagment.GetVendor(VendorID, CustomerID);
                    if (_objVendor != null)
                    {
                        tbxVendor.Text = _objVendor.Name;
                        ViewState["Mode"] = 1;
                        ViewState["VendorId"] = VendorID;
                    }
                    upVendor.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divVendorDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_Vendor"))
                {
                    int licenseId = Convert.ToInt32(e.CommandArgument);
                    if (VenderAuditManagment.AuditVendorExists(licenseId))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Vendor already used in system you can not delete.')", true);
                    }
                    else
                    {
                        VenderAuditManagment.Delete(licenseId, AuthenticationHelper.UserID);
                        upVendor.Update();
                        BindVendor();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdComplienceVender_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var VandorList = VenderAuditManagment.GetVendorListDtls(CustomerID);

                if (VandorList.Count > 0)
                    VandorList = VandorList.OrderBy(entry => entry.Name).ToList();

                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    VandorList = VandorList.Where(entry => entry.Name.Contains(tbxFilter.Text)).ToList();
                }                
                if (direction == SortDirection.Ascending)
                {
                    VandorList = VandorList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    VandorList = VandorList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }
                foreach (DataControlField field in grdComplienceVender.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplienceVender.Columns.IndexOf(field);
                    }
                }
                grdComplienceVender.DataSource = VandorList;
                grdComplienceVender.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdComplienceVender_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                saveopo.Value = "true";
                grdComplienceVender.PageIndex = e.NewPageIndex;
                BindVendor();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddVendor_Click(object sender, EventArgs e)
        {
            saveopo.Value = "false";
            ViewState["Mode"] = 0;
            tbxVendor.Text = string.Empty;
            upVendor.Update();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divVendorDialog\").dialog('open')", true);            
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;


                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../../Images/down_arrow1.png";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../../Images/up_arrow1.png";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }

        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        
    }
}