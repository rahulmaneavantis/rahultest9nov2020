﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages
{
    public partial class AuditScheduleAdd : System.Web.UI.Page
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["Mode"] = Request.QueryString["Mode"]; 
                ViewState["AuditID"] = Request.QueryString["AuditID"];
                long AuditID = Convert.ToInt64(ViewState["AuditID"]);
                tbxBranch.Text = "< Select >";
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }

                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker1", string.Format("initializeConfirmDatePicker1(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker1", "initializeConfirmDatePicker1(null);", true);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker1", "initializeConfirmDatePicker1();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);

                if (Convert.ToString(ViewState["Mode"]) == "0")
                {
                    ViewState["CHECKED_ITEMS"] = null;
                    ddlAuditor.SelectedValue = "-1";
                    //ddlVendor.SelectedValue = "-1";
                    tbxStartDate.Text = "";
                    tbxEndDate.Text = "";
                    txtAuditName.Text = "";
                    grdChecklist.DataSource = null;
                    grdChecklist.DataBind();

                    BindVendor();
                    BindAuditors(ddlAuditor);
                    BindLocation();
                    tbxBranch.Attributes.Add("readonly", "readonly");

                    tbxBranch.Enabled = true;
                    ddlVendor.Enabled = true;

                }
                if (Convert.ToString(ViewState["Mode"]) == "1")
                {
                    BindVendor();
                    BindAuditors(ddlAuditor);
                    com.VirtuosoITech.ComplianceManagement.Business.Data.AuditDetail auditDetail = Business.VenderAudits.VenderAuditManagment.GetAuditDetailByID(AuditID);

                    txtAuditName.Text = auditDetail.AuditName;
                    ddlAuditor.SelectedValue = Convert.ToString(auditDetail.AuditorID);
                    ddlVendor.SelectedValue = Convert.ToString(auditDetail.VendorID);
                    BindChecklist(Convert.ToInt32(auditDetail.VendorID));
                    DateTime dtstart = auditDetail.AuditStartDate;
                    DateTime dtend = auditDetail.AuditEndDate;
                    tbxStartDate.Text = dtstart.ToString("dd-MM-yyyy");
                    tbxEndDate.Text = dtend.ToString("dd-MM-yyyy");

                    BindGridCheckList(AuditID);
                    PopulateCheckedValues(AuditID);

                    tbxBranch.Text = CustomerBranchManagement.GetByID(auditDetail.BranchID).Name;
                    ddlVendor.Enabled = false;
                    tbxBranch.Enabled = false;
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);

            }
        }

        protected void upEventList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }

                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker1", string.Format("initializeConfirmDatePicker1(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker1", "initializeConfirmDatePicker1(null);", true);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker1", "initializeConfirmDatePicker1();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? Regex.Replace(tvBranches.SelectedNode.Text, "<.*?>", string.Empty) : "< Select >";

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindGridCheckList(long AuditID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int CustomerBranchID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var checklist = (from row in entities.AuditChecklists
                                     join row1 in entities.AuditCheckListMappings on row.ID equals row1.ChecklistID
                                     join row2 in entities.AuditDetails on row1.AuditDetailID equals row2.ID
                                     where row.Isactive == false && row2.ID == AuditID
                                     select new
                                     {
                                         row.ID,
                                         AuditID = row2.ID,
                                         row.Name
                                     });

                    //var Checklist = checklist.ToList();

                    //if (ViewState["SortOrder"].ToString() == "Asc")
                    //{
                    //    if (ViewState["SortExpression"].ToString() == "ID")
                    //    {
                    //        Checklist = Checklist.OrderBy(entry => entry.ID).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "Name")
                    //    {
                    //        Checklist = Checklist.OrderBy(entry => entry.Name).ToList();
                    //    }
                    //    direction = SortDirection.Descending;
                    //}
                    //else
                    //{
                    //    if (ViewState["SortExpression"].ToString() == "ID")
                    //    {
                    //        Checklist = Checklist.OrderByDescending(entry => entry.ID).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "Name")
                    //    {
                    //        Checklist = Checklist.OrderByDescending(entry => entry.Name).ToList();
                    //    }
                    //    direction = SortDirection.Ascending;
                    //}
                    //grdChecklist.DataSource = Checklist;
                    //grdChecklist.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";

            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        private void BindAuditors(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                
                int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users =Business.VenderAudits.VenderAuditManagment.GetAllAuditor(customerID);
                users.Insert(0, new { ID = -1, Name = ddlUserList == ddlAuditor ? "< Select >" : "< All >" });

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocation()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvBranches.Nodes.Add(node);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindVendor()
        {
            try
            {
                ddlVendor.DataTextField = "Name";
                ddlVendor.DataValueField = "ID";
                ddlVendor.Items.Clear();

                var users = Business.VenderAudits.VenderAuditManagment.GetAllVender(AuthenticationHelper.CustomerID);


                ddlVendor.DataSource = users;
                ddlVendor.DataBind();
                ddlVendor.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //protected void upEvent_Load(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DateTime date = DateTime.MinValue;
        //        if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
        //        {
        //            ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean flg = false;
                foreach (GridViewRow gvrow in grdChecklist.Rows)
                {
                    CheckBox chkChecklist = (CheckBox)gvrow.FindControl("chkChecklist");
                    if (chkChecklist.Checked)
                    {
                        flg = true;
                        break;
                    }
                }

                if (flg == true)
                {
                    int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int branchID = CustomerBranchManagement.GetByName(tbxBranch.Text.Trim(), CustomerID).ID;
                    DateTime startDate;
                    DateTime endDate;

                    startDate = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    endDate = DateTime.ParseExact(tbxEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    int checkDiffernce = Convert.ToInt32((endDate.Date - startDate.Date).Days);
                    if (checkDiffernce > 0)
                    {
                        com.VirtuosoITech.ComplianceManagement.Business.Data.AuditDetail auditDetail = new com.VirtuosoITech.ComplianceManagement.Business.Data.AuditDetail()
                        {
                            AuditName = txtAuditName.Text.Trim(),
                            CustomerID = CustomerID,
                            BranchID = branchID,
                            AuditorID = Convert.ToInt32(ddlAuditor.SelectedValue),
                            VendorID = Convert.ToInt32(ddlVendor.SelectedValue),
                            AuditStartDate = startDate,
                            AuditEndDate = endDate,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            Isactive = false
                        };
                        if (Convert.ToString(ViewState["Mode"]) == "1")
                        {
                            auditDetail.ID = Convert.ToInt32(ViewState["AuditID"]);
                        }
                        if (Convert.ToString(ViewState["Mode"]) == "0")
                        {
                            if (Business.VenderAudits.VenderAuditManagment.CheckAuditExists(CustomerID, auditDetail.AuditName))
                            {
                                cvDuplicateEntry.ErrorMessage = "Audit name already exists.";
                                cvDuplicateEntry.IsValid = false;
                                return;
                            }
                            Business.VenderAudits.VenderAuditManagment.CreateAuditDetails(auditDetail);

                            List<int> ParentEventIds = new List<int>();
                            foreach (GridViewRow gvrow in grdChecklist.Rows)
                            {
                                CheckBox chkChecklist = (CheckBox)gvrow.FindControl("chkChecklist");
                                if (chkChecklist.Checked)
                                {
                                    AuditCheckListMapping auditCheckListMapping = new AuditCheckListMapping()
                                    {
                                        ChecklistID = Convert.ToInt32(((Label)gvrow.FindControl("lblCheckListID")).Text.Trim()),
                                        AuditDetailID = Convert.ToInt32(auditDetail.ID),
                                        VendorID = Convert.ToInt32(ddlVendor.SelectedValue),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                        Isactive = false
                                    };
                                    Business.VenderAudits.VenderAuditManagment.CreateauditCheckListMapping(auditCheckListMapping);
                                }
                            }
                        }
                        else if (Convert.ToString(ViewState["Mode"]) == "1")
                        {
                            auditDetail.UpdatedBy = AuthenticationHelper.UserID;
                            auditDetail.UpdatedOn = DateTime.Now;
                            Business.VenderAudits.VenderAuditManagment.UpdateAuditDetails(auditDetail);

                            List<int> ParentEventIds = new List<int>();
                            foreach (GridViewRow gvrow in grdChecklist.Rows)
                            {
                                CheckBox chkChecklist = (CheckBox)gvrow.FindControl("chkChecklist");
                                int ChecklistID = Convert.ToInt32(((Label)gvrow.FindControl("lblCheckListID")).Text.Trim());
                                int AuditDetailID = Convert.ToInt32(auditDetail.ID);

                                if (chkChecklist.Checked)
                                {
                                    if (!Business.VenderAudits.VenderAuditManagment.CheckAuditCheckListMappingExists(AuditDetailID, ChecklistID))
                                    {
                                        AuditCheckListMapping auditCheckListMapping = new AuditCheckListMapping()
                                        {
                                            ChecklistID = ChecklistID,
                                            AuditDetailID = AuditDetailID,
                                            VendorID = Convert.ToInt32(ddlVendor.SelectedValue),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            Isactive = false
                                        };
                                        Business.VenderAudits.VenderAuditManagment.CreateauditCheckListMapping(auditCheckListMapping);
                                    }
                                }
                                else
                                {
                                    if (Business.VenderAudits.VenderAuditManagment.CheckAuditCheckListMappingExists(AuditDetailID, ChecklistID))
                                    {
                                        if (Business.VenderAudits.VenderAuditManagment.CheckAuditCheckListMappingObservationExists(AuditDetailID, ChecklistID))
                                        {
                                            AuditCheckListMapping auditCheckListMapping = new AuditCheckListMapping()
                                            {
                                                AuditDetailID = AuditDetailID,
                                                ChecklistID = ChecklistID,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now,
                                                Isactive = true
                                            };
                                            Business.VenderAudits.VenderAuditManagment.UpdateauditCheckListMapping(auditCheckListMapping);
                                        }
                                    }
                                }
                            }
                        }
                        Response.Redirect("~/VenderAudit/aspxpages/AuditSchedule.aspx?", false);
                    }
                    else {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "End date should be greater than Start date";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select atleast one checklist";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/VenderAudit/aspxpages/AuditSchedule.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                int VendorID = Convert.ToInt32(ddlVendor.SelectedValue);
                BindChecklist(VendorID);
            }
            catch (Exception)
            {

            }

        }

        private void PopulateCheckedValues(long AuditID)
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int CustomerBranchID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var checklist = (from row in entities.AuditChecklists
                                     join row1 in entities.AuditCheckListMappings on row.ID equals row1.ChecklistID
                                     join row2 in entities.AuditDetails on row1.AuditDetailID equals row2.ID
                                     where row1.Isactive == false && row2.ID == AuditID
                                     select new
                                     {
                                         row.ID,
                                         AuditID = row2.ID,
                                         row.Name
                                     });

                    if (checklist != null )
                    {
                        foreach (GridViewRow gvrow in grdChecklist.Rows)
                        {
                            int index = Convert.ToInt32(grdChecklist.DataKeys[gvrow.RowIndex].Value);
                            var rmdata = checklist.Where(t => t.ID == index).FirstOrDefault();
                            if (rmdata != null)
                            {
                                CheckBox chkChecklist = (CheckBox)gvrow.FindControl("chkChecklist");
                                chkChecklist.Checked = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindChecklist(int VendorID)
        {
            try
            {
                grdChecklist.DataSource = Business.VenderAudits.VenderAuditManagment.GetAuditChecklist(VendorID); ;
                grdChecklist.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
       
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Session["Mode"] = 0;
                Session["AuditID"] = 0;
                Response.Redirect("~/VenderAudit/aspxpages/AuditScheduleAdd.aspx?Mode=0&AuditID=0", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void btnEditAuditSchedule_Click(object sender, EventArgs e)
        {
            try
            {
             
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditScheduleDialog\").dialog('open');", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        public static List<Sp_Get_AuditScheduleList_Result> GetAllAuditComplianceDetails(long AuditID,long ChecklistID, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DTLList = (from row in entities.Sp_Get_AuditScheduleList(AuditID,ChecklistID, flag)
                              
                               select row).ToList();

                

                return DTLList;
            }
        }
        private void BindAuditChecklistDetails(long AuditID, long ChecklistID, string flag)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    GrdAuditschedule.DataSource = null;
                    GrdAuditschedule.DataBind();

                    //GrdAuditschedule.EditIndex = -1;

                    var dataSource = GetAllAuditComplianceDetails(AuditID, ChecklistID, flag);
                    if (flag == "S")
                    {
                        lblType.Text = "Statutory";
                    }
                    else
                    {
                        lblType.Text = "Internal";
                    }

                    GrdAuditschedule.Visible = true;
                    GrdAuditschedule.DataSource = dataSource;
                    GrdAuditschedule.DataBind();

                 }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        public static string GetScheduleByChecklistID( long ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ChecklistListName = (from row in entities.AuditChecklists
                                         where row.ID==ID

                                         select row.Name).FirstOrDefault();
                return ChecklistListName;


               // return ChecklistListName.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static string GetScheduleByChecklistType(long ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ChecklistType = (from row in entities.AuditChecklistComplianceMappings
                                     where row.ChecklistID  == ID
                                     select row.ComplianceType).FirstOrDefault();
                return ChecklistType;
            }
        }

        public static List<object> GetAllLocation(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Locations = (from row in entities.CustomerBranches
                                 where row.IsDeleted == false 
                                 && row.CustomerID == customerID
                                 orderby row.Name ascending
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                return Locations.ToList<object>();
            
            }
        }


        private void BindAllLocations(ListBox ddlLocationListNew)
        {
            try
            {

                long customerID = AuthenticationHelper.CustomerID;
                var LocationDetails = GetAllLocation(customerID);
                ddlLocationListNew.DataTextField = "Name";
                ddlLocationListNew.DataValueField = "ID";
                ddlLocationListNew.DataSource = LocationDetails;
                ddlLocationListNew.DataBind();
                ddlLocationListNew.Items.Insert(0, new ListItem("< Select Location >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      
        protected void grdChecklist_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(e.CommandArgument);
               
                lblAuditName.Text = txtAuditName.Text;
                var ChecklistListName = GetScheduleByChecklistID(ID);
                long AuditID = Convert.ToInt64(ViewState["AuditID"]);
                var checklistType = GetScheduleByChecklistType(ID);
                lblType.Text = Convert.ToString(checklistType);
                lblChecklistName.Text = Convert.ToString(ChecklistListName);
                ViewState["CheckListID"] = ID;
                ViewState["checklistType"] = checklistType;
                BindAuditChecklistDetails(AuditID, ID, checklistType);
              //  DropDownList dp = (DropDownList)e.Row.FindControl("ddlLocationList");
               
               upchecklist.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      
       
        protected void GrdAuditschedule_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("Save_COMPLIANCE"))
                {
                    GrdAuditschedule.DataSource = null;
                    GrdAuditschedule.DataBind();

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long complianceID = Convert.ToInt32(commandArgs[0]);
                    long AuditID = Convert.ToInt64(ViewState["AuditID"]);
                    int ChecklistiD = Convert.ToInt32(ViewState["CheckListID"]);
                    string checklistType = Convert.ToString(ViewState["checklistType"]);
                    string flag = lblType.Text;
                    GridViewRow curruntRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    ListBox ddlCopyStatus = (ListBox)curruntRow.FindControl("ddlLocationListNew") as ListBox;
                    string selectedNewValue = ddlCopyStatus.SelectedValue;

                    Business.VenderAudits.VenderAuditManagment.DeleteAuditcomplianceBranchTable(AuditID, Convert.ToInt32(ChecklistiD), complianceID);
                    foreach (ListItem item in ddlCopyStatus.Items)
                    {
                        if (item.Selected)
                        {
                            com.VirtuosoITech.ComplianceManagement.Business.Data.AuditComplianceBranchMapping auditcompliancebranch = new com.VirtuosoITech.ComplianceManagement.Business.Data.AuditComplianceBranchMapping()
                            {
                                AuditDetailID = Convert.ToInt32(AuditID),
                                ChecklistID = Convert.ToInt32(ChecklistiD),
                                BranchID = Convert.ToInt32(item.Value.ToString()),
                                ComplianceID = Convert.ToInt32(complianceID),
                                Type = checklistType,
                            };
                            Business.VenderAudits.VenderAuditManagment.CreateAuditComplianceBranchDetails(auditcompliancebranch);
                        }
                    }
                    // upchecklist.Update();
                    GrdAuditschedule.EditIndex = -1;
                    BindAuditChecklistDetails(AuditID, ChecklistiD, checklistType);


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string SelectedBranchName(long  AuditID,long ChecklistID,long complinaceID)
        {
            try
            {
                string branchname = "";
                string branchname1 = "";

                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data = (from row in entities.AuditComplianceBranchMappings
                            where row.AuditDetailID == AuditID &&
                            row.ChecklistID == ChecklistID &&
                            row.ComplianceID == complinaceID
                            select row.BranchID).ToList();

                if (data != null)
                {
                    if (data.Count() > 0)
                    {
                        var branchnamelist = (from row in entities.CustomerBranches
                                              where data.Contains(row.ID)
                                              select row.Name).ToList();
                        foreach (var item in branchnamelist)
                        {
                            branchname1 += item.ToString() + " ,";
                        }
                      // branchname1.TrimEnd(',', ' ');

                        branchname1 = branchname1.Remove(branchname1.Length - 1);
                    }
                }

                branchname = branchname1;

                return branchname;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return "branchname1";
        }

        protected void GrdAuditschedule_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lbtSave = (LinkButton)e.Row.FindControl("lbtSave");
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    //Find the TextBox control.

                    ListBox dp = (ListBox)e.Row.FindControl("ddlLocationListNew");
                    Label lbllocation = (Label)e.Row.FindControl("lbllocation");
                    BindAllLocations(dp);

                    // upchecklist.Update();
                    // dp.SelectedValue = GrdAuditschedule.DataKeys[e.Row.RowIndex].Values[1].ToString();
                    // dp.Items.FindByText(lbllocation.Text).Selected = true;
                }
            }


           
        }

        protected void GrdAuditschedule_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GrdAuditschedule.EditIndex = -1;

            long AuditID = Convert.ToInt64(ViewState["AuditID"]);

            int ChecklistID = Convert.ToInt32(ViewState["CheckListID"]);
            string checklistType = Convert.ToString(ViewState["checklistType"]);
            
            BindAuditChecklistDetails(AuditID, ChecklistID, checklistType);
        }

        protected void GrdAuditschedule_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GrdAuditschedule.EditIndex = e.NewEditIndex;
            
            //int CustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

            long AuditID = Convert.ToInt64(ViewState["AuditID"]);

            int ChecklistID = Convert.ToInt32(ViewState["CheckListID"]);
            string checklistType =Convert.ToString(ViewState["checklistType"]);
           
            BindAuditChecklistDetails(AuditID, ChecklistID, checklistType);
            upchecklist.Update();
            //(GrdAuditschedule.Rows[e.NewEditIndex].FindControl("ddlLocationListNew") as ListBox).Items[e.NewEditIndex + 1].Selected = true;
        }

        //protected void GrdAuditschedule_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    // GrdAuditschedule.EditIndex = -1;
        //    long AuditID = Convert.ToInt64(ViewState["AuditID"]);

        //    int ChecklistID = Convert.ToInt32(ViewState["CheckListID"]);
        //    string checklistType = Convert.ToString(ViewState["checklistType"]);

        //    //int ID = Convert.ToInt32(GrdAuditschedule.DataKeys[e.RowIndex].Values[0].ToString());
        //    // int TempAssignmentID = Convert.ToInt32(GrdAuditschedule.Rows[e.RowIndex].FindControl("ID"));

        //     int ComplianceID = Convert.ToInt32(GrdAuditschedule.DataKeys[e.RowIndex].Values[0].ToString());
        //    // ListBox ddl = (ListBox)GrdAuditschedule.Rows[e.RowIndex].FindControl("ddlLocationListNew");
        //    // DropDownList ddl = (DropDownList)grdComplianceRoleMatrix.Rows[e.RowIndex].FindControl("ddlUserList");
        //    ListBox ddl = (ListBox)GrdAuditschedule.Rows[e.RowIndex].FindControl("ddlLocationListNew");
        //    Business.ComplianceManagement.SaveAssignedLocation(ComplianceID, Convert.ToInt32(ddl.SelectedValue),ChecklistID);
          

        //    GrdAuditschedule.EditIndex = -1;
        //    BindAuditChecklistDetails(AuditID, ChecklistID, checklistType);



        //}

    }
 }



