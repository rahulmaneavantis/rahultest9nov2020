﻿<%@ Page Title="Audit Schedule List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="AuditSchedule.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages.AuditSchedule"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div style="width: 100%">

                <div style="float: right; margin-top: 10px; margin-right: 15px;">
                    <asp:LinkButton Text="Add New" runat="server" ID="btnAdd" OnClick="btnAdd_Click" />
                </div>
                <br />
                <br />
                <br />
                <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                    <asp:GridView runat="server" ID="grdAuditDetail" AutoGenerateColumns="false" GridLines="Vertical"
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdComplianceType_RowCreated"
                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdAuditDetail_Sorting"
                        Font-Size="12px" DataKeyNames="ID" OnRowDataBound="grdAuditDetail_RowDataBound" OnRowCommand="grdAuditDetail_RowCommand"  OnPageIndexChanging="grdAuditDetail_PageIndexChanging">
                        <Columns>
                            <asp:BoundField DataField="ID" HeaderText="ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="ID" />
                            <asp:TemplateField HeaderText="Location" SortExpression="Branch">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                         <asp:Label ID="lblAuditID" Visible="false" runat="server" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VendorName" SortExpression="VendorName">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                        <asp:Label runat="server" Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AuditName" SortExpression="AuditName">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                        <asp:Label runat="server" Text='<%# Eval("AuditName") %>' ToolTip='<%# Eval("AuditName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="AuditStartDate">
                                <ItemTemplate>
                                    <%# Eval("AuditStartDate")!= null?((DateTime)Eval("AuditStartDate")).ToString("dd-MMM-yyyy"):""%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="AuditEndDate">
                                <ItemTemplate>
                                    <%# Eval("AuditEndDate")!= null?((DateTime)Eval("AuditEndDate")).ToString("dd-MMM-yyyy"):""%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_Audit" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon.png" alt="Edit Audit" title="Edit Audit" /></asp:LinkButton>
<%--                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_ACT" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this Event?');"><img src="../Images/delete_icon.png" alt="Delete Event" title="Delete Event" /></asp:LinkButton>--%>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divAssignComplianceDialog">
        <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Vendor Name</label>
                        <asp:DropDownList ID="ddlVendor" runat="server" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:CompareValidator ErrorMessage="Please select Vendor." ControlToValidate="ddlVendor"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Auditor Name</label>
                        <asp:DropDownList ID="ddlAuditor" runat="server" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlAuditor_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:CompareValidator ErrorMessage="Please select Auditor." ControlToValidate="ddlAuditor"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                            Display="None" />
                    </div>
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Location</label>
                                <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" />
                                <div style="margin-left: 150px; position: absolute; z-index: 10" id="divBranches">
                                    <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                        BorderWidth="1" Height="200px" Width="394px"
                                        Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div>
                                <asp:CompareValidator ControlToValidate="tbxBranch" ErrorMessage="Please select Location."
                                    runat="server" ValueToCompare="< Select Location >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Audit Name</label>
                                <asp:TextBox runat="server" ID="txtAuditName" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Audit Name can not be empty."
                                    ControlToValidate="txtAuditName" runat="server" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Checklist
                                </label>
                                <asp:TextBox runat="server" ID="txtChecklist" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" />
                                <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; width: 570px; border: 1px solid gray; height: 200px;" id="dvChecklist">
                                    <asp:Repeater ID="rptChecklist" runat="server">
                                        <HeaderTemplate>
                                            <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                <tr>
                                                    <td style="width: 100px;">
                                                        <asp:CheckBox ID="ChecklistSelectAll" Text="Select All" runat="server" onclick="checkAllChecklist(this)" /></td>
                                                    <td style="width: 282px;">
                                                        <asp:Button runat="server" ID="btnChecklistRepeater" Text="Ok" Style="float: left" OnClick="btnChecklistRepeater_Click" /></td>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width: 20px;">
                                                    <asp:CheckBox ID="chkChecklist" runat="server" onclick="UncheckCheckListHeader();" /></td>
                                                <td style="width: 470px;">
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 460px; padding-bottom: 5px;">
                                                        <asp:Label ID="lblChecklistID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                        <asp:Label ID="lblChecklistName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <div style="margin-bottom: 7px; clear: both">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Start Date</label>
                                <asp:TextBox runat="server" ID="tbxStartDate" Style="height: 16px; width: 390px;" AutoPostBack="true" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Start Date can not be empty."
                                    ControlToValidate="tbxStartDate" runat="server" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>

                            <div style="margin-bottom: 7px; clear: both">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    End Date</label>
                                <asp:TextBox runat="server" ID="tbxEndDate" Style="height: 16px; width: 390px;" AutoPostBack="true" />
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="End Date can not be empty."
                                    ControlToValidate="tbxEndDate" runat="server" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>

                            <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">

                                <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                            </div>
                            <div style="margin-bottom: 7px">
                                <asp:Panel runat="server" style="height:40px;">
                                    <asp:GridView runat="server" ID="grdChecklist" AutoGenerateColumns="false" GridLines="Vertical"
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                        CellPadding="4" ForeColor="Black" AllowPaging="false" PageSize="10" Width="100%"
                                        Font-Size="12px" DataKeyNames="ID">
                                        <Columns>
                                            <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
                                            <asp:TemplateField HeaderText="Checklist Name" SortExpression="Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                        <asp:Label runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                        <PagerSettings Position="Top" />
                                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="#E6EFF7" />
                                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                        <EmptyDataTemplate>
                                            No Records Found.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>

                    </asp:UpdatePanel>

                    <div style="margin-bottom: 7px; float: right; margin-right: 467px; margin-top: 10px; clear: both">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="button" OnClientClick="$('#divAssignComplianceDialog').dialog('close');" />
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divAssignComplianceDialog').dialog({
                height: 650,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Audit Schedule",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            initializeCombobox();

        });

        function initializeCombobox() {
            $("#<%= ddlVendor.ClientID %>").combobox();
            $("#<%= ddlAuditor.ClientID %>").combobox();
        }


        function initializeDatePicker(date) {

            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1
            });

        }

        function initializeConfirmDatePicker1(date) {
            var startDate = new Date();
            $('#<%= tbxEndDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1
            });

        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function UncheckCheckListHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkChecklist']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkChecklist']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='ChecklistSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function checkAllChecklist(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkChecklist") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
