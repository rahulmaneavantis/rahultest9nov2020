﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="UploadAuditChecklist.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages.UploadAuditChecklist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#divCustomerBranchesDialog').dialog({
                height: 600,
                width: 1050,
                autoOpen: false,
                draggable: true,
                title: "Audit Checklist Compliance Mapping",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            initializeCombobox();
        });
        function initializeCombobox() {
            $("#<%= ddlVendor.ClientID %>").combobox();
        }

        $(document).ready(function () {
            if (document.getElementById('BodyContent_saveopo').value == "true") {

                $('#divCustomerBranchesDialog').dialog({
                    height: 600,
                    width: 1050,
                    autoOpen: false,
                    draggable: true,
                    title: "Audit Checklist Compliance Mapping",
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    }
                });
                newfun();
            }
        });
        function newfun() {

            $("#divCustomerBranchesDialog").dialog('open');

        }

        function CheckComplianceType()
        {
            var complianceType="";
            var complianceTypeID = $("#<%= ComplianceType.ClientID %> input:checked"); 

            if (complianceTypeID.val() == 'S')
                complianceType = "Statutory";
            else
                complianceType = "Internal";

            return confirm('Are you certain you want to save mapping for ' + complianceType + ' Compliance Type ?');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdatePanel ID="upCustomerBranchList" runat="server" UpdateMode="Conditional" OnLoad="upCustomerBranchList_Load">
        <ContentTemplate>
            <div>
                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                    ValidationGroup="oplValidationGroup" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
            </div>
            <div>
                <table width="100%">
                    <tr>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlVendor" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                CssClass="txtbox" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true" />
                            <asp:CompareValidator ErrorMessage="Please select Vendor." ControlToValidate="ddlVendor"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="oplValidationGroup"
                                Display="None" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                                OnTextChanged="tbxFilter_TextChanged" />
                        </td>
                        <td>
                            <asp:FileUpload ID="MasterFileUpload" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="MasterFileUpload" ErrorMessage="File Required!" ValidationGroup="oplValidationGroup" Display="None">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                CssClass="button" OnClick="btnUploadFile_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>

                            <asp:LinkButton ID="lnksampleForm" class="newlink" Font-Underline="True" OnClick="lnksampleForm_Click"
                                data-toggle="tooltip" data-placement="bottom"
                                ToolTip="Download Sample Excel Document Format for Audit Detail's Upload" runat="server">Sample Format</asp:LinkButton>

                        </td>
                    </tr>
                </table>
            </div>
            <div>

                <asp:GridView runat="server" ID="grdAuditChecklist" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                    OnRowCommand="grdAuditChecklist_RowCommand"
                    OnRowDataBound="grdAuditChecklist_RowDataBound"
                    OnRowCreated="grdAuditChecklist_RowCreated"
                    OnSorting="grdAuditChecklist_Sorting"
                    OnPageIndexChanging="grdAuditChecklist_PageIndexChanging"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="25" Width="100%" Font-Size="12px">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblCheckListID" runat="server" Text='<%# Eval("CheckListID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="AuditCategory">
                            <ItemTemplate>
                                <asp:Label ID="lblAuditCategory" runat="server" Text='<%# Eval("AuditCategory") %>' ToolTip='<%# Eval("AuditCategory") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Clause No" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="ClauseNo">
                            <ItemTemplate>
                                <asp:Label ID="lblClauseNo" runat="server" Text='<%# Eval("ClauseNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CheckList" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="CheckList">
                            <ItemTemplate>
                                <asp:Label ID="lblCheckList" runat="server" Text='<%# Eval("CheckList") %>' ToolTip='<%# Eval("CheckList") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_Checklist" CommandArgument='<%# Eval("CheckListID") +","+ Eval("CategoryID") %>'><img src="../../Images/edit_icon.png" alt="Edit Audit-Checklist" title="Edit Audit-Checklist" /></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
            <asp:PostBackTrigger ControlID="lnksampleForm" />
        </Triggers>
    </asp:UpdatePanel>

    <div id="divCustomerBranchesDialog">
        <asp:UpdatePanel ID="upCustomerBranches" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">

                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntryPopPup" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Is Statutory / Internal</label>
                        <asp:RadioButtonList ID="ComplianceType" AutoPostBack="true" runat="server"
                            OnSelectedIndexChanged="ComplianceType_SelectedIndexChanged" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Statutory" Value="S" />
                            <asp:ListItem Text="Internal" Value="I" />
                        </asp:RadioButtonList>
                    </div>
                    <div style="margin-bottom: 7px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Category</label>
                        <asp:DropDownList runat="server" ID="ddlCategory" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Clause No</label>

                        <asp:Label ID="lblClauseNo" runat="server" Text=""></asp:Label>

                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            CheckList</label>
                        <asp:TextBox runat="server" ID="tbxCheckList" Style="height: 30px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="recChecklist" ErrorMessage="CheckList can not be empty."
                            ControlToValidate="tbxCheckList" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            ComplianceID</label>
                        <asp:TextBox runat="server" ID="txtComplianceID" AutoPostBack="true"
                            OnTextChanged="txtComplianceID_TextChanged" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Compliance ID can not be empty."
                            ControlToValidate="txtComplianceID" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px;">
                        <asp:GridView runat="server" ID="GridRemarks" AutoGenerateColumns="false" AllowSorting="true"
                            AllowPaging="true" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" CssClass="table"
                            CellPadding="4" ForeColor="Black" Font-Size="12px" Width="100%"
                            PageSize="12" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                            OnPageIndexChanging="GridRemarks_OnPageIndexChanging" OnRowCommand="GridRemarks_OnRowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="ChecklistID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCheacklistID" runat="server" Text='<%# Eval("ChecklistID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TaskID" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTaskID" runat="server" Text='<%# Eval("TaskID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Short Description" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                            <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>                                       
                                        <asp:LinkButton ID="lblDelete" runat="server" CommandName="DELETE_COMPLIANCE"
                                            CommandArgument='<%# Eval("TaskID") %>' OnClientClick="return confirm('Are you certain you want to delete this compliance ?');"><img src="../../Images/delete_icon.png" alt="Delete Compliance" title="Delete Compliance" /></asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>

                    <div style="margin-bottom: 7px">

                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Sample Document</label>
                        <asp:FileUpload ID="fuSampleEvidence" runat="server" />

                        <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="button" OnClick="btnUpload_Click" />

                    </div>

                    <div style="margin-bottom: 7px; margin-top: 10px;">

                        <asp:GridView runat="server" ID="rptComplianceDocumnets"
                            AutoGenerateColumns="false"
                            GridLines="Vertical"
                            BackColor="White"
                            BorderColor="#DEDFDE"
                            BorderStyle="Solid"
                            BorderWidth="1px"
                            AllowSorting="true"
                            CellPadding="4"
                            ForeColor="Black"
                            AllowPaging="True"
                            PageSize="25"
                            Width="100%"
                            Font-Size="12px"
                            OnRowCommand="rptComplianceDocumnets_RowCommand"
                            OnRowDeleting="rptComplianceDocumnets_RowDeleting"
                            OnRowDataBound="rptComplianceDocumnets_RowDataBound"
                            OnPageIndexChanging="rptComplianceDocumnets_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="FileName" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%--<asp:UpdatePanel runat="server" ID="aaa1" UpdateMode="Conditional">
                                            <ContentTemplate>--%>
                                        <asp:LinkButton
                                            CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                            ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                        </asp:LinkButton>
                                        <%-- </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnComplianceDocumnets" />
                                            </Triggers>
                                        </asp:UpdatePanel>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%-- <asp:UpdatePanel runat="server" ID="aaa2" UpdateMode="Conditional">
                                            <ContentTemplate>--%>
                                        <asp:LinkButton CommandArgument='<%# Eval("FileID")%>'
                                            AutoPostBack="true" CommandName="Delete"
                                            OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                            ID="lbtLinkDocbutton" runat="server">
                                                    <img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' 
                                                     alt="Delete" title="Delete" width="15px" height="15px" />
                                        </asp:LinkButton>
                                        <%-- </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lbtLinkDocbutton" />
                                            </Triggers>
                                        </asp:UpdatePanel>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>

                    </div>

                    <div style="margin-bottom: 7px; margin-left: 350px; margin-top: 10px;">
                        <asp:HiddenField ID="saveopo" runat="server" Value="false" />
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup" OnClientClick="return CheckComplianceType();"/>
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divCustomerBranchesDialog').dialog('close');" />
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnUpload" />
            </Triggers>
        </asp:UpdatePanel>
    </div>


</asp:Content>
