﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages
{
    public partial class AuditMyReportNew : System.Web.UI.Page
    {
        //static bool ReviewerFlag;
        protected static List<Int32> roles;
        protected string Reviewername;
        protected string Performername;
        static int UserRoleID;
        public static string CompDocReviewPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    bindVendorList();
                    bindAuditList();
                    // ReviewerFlag = false;
                    BindLocationFilter();
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                   
                    btnSearch_Click(sender, e);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        public static List<AuditVendor> GetVendorList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var VendorDetails = (from row in entities.AuditVendors
                                     where row.Isactive == false
                                          && row.CustomerID == customerID
                                     select row).ToList();

                return VendorDetails;
            }
        }
        private void bindVendorList()
        {
            try
            {
                ddlVendor.DataTextField = "Name";
                ddlVendor.DataValueField = "ID";

                ddlVendor.DataSource = GetVendorList();
                ddlVendor.DataBind();

                ddlVendor.Items.Insert(0, new ListItem("Select Vendor", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<AuditDetail> GetAuditList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int userID = -1;
                userID = Convert.ToInt32(AuthenticationHelper.UserID);

                var AuditDetails = (from row in entities.AuditDetails
                                     where row.Isactive == false && row.AuditorID== userID
                                          && row.CustomerID == customerID
                                     select row).ToList();

                return AuditDetails;
            }
        }
        private void bindAuditList()
        {
            try
            {
                ddlAudit.DataTextField = "AuditName";
                ddlAudit.DataValueField = "ID";

                ddlAudit.DataSource = GetAuditList();
                ddlAudit.DataBind();

                ddlAudit.Items.Insert(0, new ListItem("Select Audit", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDownloadList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);

                var LocationList = CustomerBranchManagement.GetAuditorAssignedLocationList(AuthenticationHelper.UserID, customerID);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        protected void grdReviewerComplianceDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    TextBox EscDays = (TextBox)e.Row.FindControl("txtEscDays");
                    TextBox IntreimDays = (TextBox)e.Row.FindControl("txtInterimDays");
                    Button btnAdd = (Button)e.Row.FindControl("btnAdd");

                    if (EscDays.Text != "" && IntreimDays.Text != "")
                    {
                        btnAdd.Text = "Update";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdReviewerComplianceDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            FillComplianceDocuments();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                //if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                //{
                //    grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdReviewerComplianceDocument.PageIndex = 0;
                //}
                grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdReviewerComplianceDocument.PageIndex = 0;
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


      




        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public void FillComplianceDocuments()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int userID = -1;
                    userID = Convert.ToInt32(AuthenticationHelper.UserID);
                    String location = tvFilterLocation.SelectedNode.Text;
                    int StatusValue = Convert.ToInt32(ddlStatus.SelectedValue);

                    Session["TotalRows"] = 0;
                    var AuditList = entities.SP_Get_AuditDetail(customerID, userID).ToList();

                    if (AuditList.Count > 0)
                    {                         
                        if (Convert.ToInt64(ddlAudit.SelectedValue) != -1)
                        {
                            AuditList = AuditList.Where(x => x.Id == Convert.ToInt64(ddlAudit.SelectedValue)).ToList();
                        }
                        if (Convert.ToInt64(ddlVendor.SelectedValue) != -1)
                        {
                            AuditList = AuditList.Where(x => x.VendorID == Convert.ToInt64(ddlVendor.SelectedValue)).ToList();
                        }
                        if (!string.IsNullOrEmpty(location))
                        {
                            if (location != "Entity/Sub-Entity/Location")
                                AuditList = AuditList.Where(entry => entry.LocationName == location).ToList();
                        }
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 0)//for open
                        {
                            AuditList = AuditList.Where(x => x.AuditCloseFlag != true).ToList();
                        }
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 1)//for open
                        {
                            AuditList = AuditList.Where(x => x.AuditCloseFlag == true).ToList();
                        }                       
                    }

                    grdReviewerComplianceDocument.DataSource = AuditList;
                    grdReviewerComplianceDocument.DataBind();
                    Session["TotalRows"] = AuditList.Count;
                    grdReviewerComplianceDocument.Visible = true;
                    GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

       

      
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                //{
                    SelectedPageNo.Text = "1";
                    int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                    if (currentPageNo < GetTotalPagesCount())
                    {
                        SelectedPageNo.Text = (currentPageNo).ToString();
                    }

                    //if (ReviewerFlag)
                    //{
                        grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    //}
                //}
                //Reload the Grid
                FillComplianceDocuments();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                //if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                //{
                    //if (ReviewerFlag)
                    //{
                        grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    //}
                //}
                //Reload the Grid
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                //if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                //{

                    grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //}
                //Reload the Grid
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
          
        }

       
        protected void btnExcelReport_Click(object sender, EventArgs e)
        {
            try
            {
                int Customerid = -1;
                Customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = -1;
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                String location = tvFilterLocation.SelectedNode.Text;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_Get_AuditDetailWithChecklist_Result> MasterManagementCompliancesSummaryQuery = new List<SP_Get_AuditDetailWithChecklist_Result>();
                    entities.Database.CommandTimeout = 180;
                    MasterManagementCompliancesSummaryQuery = (entities.SP_Get_AuditDetailWithChecklist(Customerid, userID)).ToList();

                    if (MasterManagementCompliancesSummaryQuery.Count > 0)
                    {
                        if (Convert.ToInt64(ddlAudit.SelectedValue) != -1)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(x => x.AuditID == Convert.ToInt64(ddlAudit.SelectedValue)).ToList();
                        }
                        if (Convert.ToInt64(ddlVendor.SelectedValue) != -1)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(x => x.vendorId == Convert.ToInt64(ddlVendor.SelectedValue)).ToList();
                        }
                        if (!string.IsNullOrEmpty(location))
                        {
                            if (location != "Entity/Sub-Entity/Location")
                                MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.LocationName == location).ToList();
                        }
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 0)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(x => x.AuditCloseFlag != true).ToList();
                        }
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 1)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(x => x.AuditCloseFlag == true).ToList();
                        }
                    }


                    if (MasterManagementCompliancesSummaryQuery.Count > 0)
                    {

                        DateTime EndDate = DateTime.Today.Date;
                        DateTime FromDate = DateTime.Today.AddMonths(-18);


                        Color colFromInTime = System.Drawing.ColorTranslator.FromHtml("#006500");
                        Color colFromAfterDuedate = System.Drawing.ColorTranslator.FromHtml("#ffcd70");
                        Color colFromOverdue = System.Drawing.ColorTranslator.FromHtml("#FF0000");

                        using (ExcelPackage exportPackge = new ExcelPackage())
                        {

                            #region Audit detail              
                            ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Audit Detail");
                            DataTable ExcelData1 = null;
                            DataView view = new System.Data.DataView((MasterManagementCompliancesSummaryQuery as List<SP_Get_AuditDetailWithChecklist_Result>).ToDataTable());
                            ExcelData1 = view.ToTable("Selected", false, "LocationName", "VendorName", "CategoryName", "AuditName", "AuditStartDate", "AuditEndDate", "ChecklistName", "Observation");


                            #region

                            //exWorkSheet1.Cells["A1"].Value = "Audit Detail";                          
                            //string EndRow = "A2:A" + b;
                            //exWorkSheet1.Cells["A2"].Value = "";
                            //exWorkSheet1.Cells[EndRow].Merge = true;
                            //exWorkSheet1.Cells["A2"].AutoFitColumns(5);
                            //exWorkSheet1.Cells["B2"].Value = "";
                            //exWorkSheet1.Cells["B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //exWorkSheet1.Cells["B2"].Style.Fill.BackgroundColor.SetColor(Color.Red);

                            exWorkSheet1.Cells["A3"].LoadFromDataTable(ExcelData1, true);

                            exWorkSheet1.Cells["A3"].Value = "Location Name";
                            exWorkSheet1.Cells["A3"].Merge = true;
                            exWorkSheet1.Cells["A3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["A3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A3"].Style.Fill.BackgroundColor.SetColor(Color.MistyRose);
                            exWorkSheet1.Cells["A3"].Style.WrapText = true;


                            exWorkSheet1.Cells["B3"].Value = "Vendor Name";
                            exWorkSheet1.Cells["B3"].Merge = true;
                            exWorkSheet1.Cells["B3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["B3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["B3"].Style.WrapText = true;

                            exWorkSheet1.Cells["C3"].Value = "Category Name";
                            exWorkSheet1.Cells["C3"].Merge = true;
                            exWorkSheet1.Cells["C3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["C3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["C3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                            exWorkSheet1.Cells["C3"].Style.WrapText = true;

                            exWorkSheet1.Cells["D3"].Value = "Audit Name";
                            exWorkSheet1.Cells["D3"].Merge = true;
                            exWorkSheet1.Cells["D3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["D3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["D3"].Style.Fill.BackgroundColor.SetColor(Color.LightYellow);
                            exWorkSheet1.Cells["D3"].Style.WrapText = true;

                            exWorkSheet1.Cells["E3"].Value = "Audit Start Date";
                            exWorkSheet1.Cells["E3"].Merge = true;
                            exWorkSheet1.Cells["E3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["E3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["E3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["E3"].Style.WrapText = true;

                            exWorkSheet1.Cells["F3"].Value = "Audit End Date";
                            exWorkSheet1.Cells["F3"].Merge = true;
                            exWorkSheet1.Cells["F3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["F3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["F3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["F3"].Style.WrapText = true;

                            exWorkSheet1.Cells["G3"].Value = "Checklist Name";
                            exWorkSheet1.Cells["G3"].Merge = true;
                            exWorkSheet1.Cells["G3"].AutoFitColumns(21);
                            exWorkSheet1.Cells["G3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["G3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["G3"].Style.WrapText = true;

                            exWorkSheet1.Cells["H3"].Value = "Observation";
                            exWorkSheet1.Cells["H3"].Merge = true;
                            exWorkSheet1.Cells["H3"].AutoFitColumns(54);
                            exWorkSheet1.Cells["H3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["H3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                            exWorkSheet1.Cells["H3"].Style.WrapText = true;

                            #endregion

                            using (ExcelRange col = exWorkSheet1.Cells[7, 1, 7 + ExcelData1.Rows.Count, 6])
                            {
                                col.Style.WrapText = true;
                                //col.Style
                            }
                            //string ActulValue = string.Empty;
                            //string NewValue = string.Empty;
                            //int j = 5;
                            //int k = 5;
                            //for (int i = 5; i <= 4 + ExcelData1.Rows.Count; i++)
                            //{
                            //    string chke = "A" + i;
                            //    string Checkcell = exWorkSheet1.Cells[chke].Value.ToString();
                            //    if (i > 5)
                            //    {
                            //        if (string.IsNullOrEmpty(Checkcell))
                            //        {
                            //            j++;
                            //        }
                            //        else
                            //        {
                            //            string checknow4 = "A" + k + ":A" + j;
                            //            exWorkSheet1.Cells[checknow4].Merge = true;
                            //            k = i;
                            //            j = i;
                            //        }
                            //        if (j == (2 + ExcelData1.Rows.Count))
                            //        {
                            //            string checknow4 = "A" + k + ":A" + j;
                            //            exWorkSheet1.Cells[checknow4].Merge = true;
                            //        }
                            //    }
                            //}


                            #endregion

                            using (ExcelRange col = exWorkSheet1.Cells[1, 1, 3 + ExcelData1.Rows.Count, 8])
                            {
                                col.Style.WrapText = true;
                                using (ExcelRange col1 = exWorkSheet1.Cells[1, 1, 3 + ExcelData1.Rows.Count, 15])
                                {
                                    col1.Style.Numberformat.Format = "dd-MMM-yyyy";
                                }
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                exWorkSheet1.Cells["A3:H3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A3:H3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                //exWorkSheet1.Cells["A4:H4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["A4:H4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                // Assign borders
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                Byte[] fileBytes = exportPackge.GetAsByteArray();
                                Response.ClearContent();
                                Response.Buffer = true;
                                Response.AddHeader("content-disposition", "attachment;filename=Audit Detail.xlsx");
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.ms-excel";
                                StringWriter sw = new StringWriter();
                                Response.BinaryWrite(fileBytes);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }

                        }
                    }

                    else
                    {
                        //Library.WriteErrorLog("No data found so not create excel");
                    }
                }
            }
            catch (Exception ex)
            {
                // Library.WriteErrorLog("Exception: " + ex.ToString() + " {0}");
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}