﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VendorAudit.Master" AutoEventWireup="true" CodeBehind="VendorAuditDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages.VendorAuditDashboard" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AVANTIS - Products that simplify">
    <meta name="author" content="AVANTIS - Development Team">
    <link href="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
        <link href="../../newcss/spectrum.css" rel="stylesheet" />
        <script type="text/javascript" src="../../newjs/spectrum.js"></script>

        <style type="text/css">
            #dailyupdates .bx-viewport {
                height: 190px !important;
            }

            .info-box:hover {
                color: #FF7473;
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
                z-index: 5;
            }

            .function-selector-radio-label {
                font-size: 12px;
            }

            .colorPickerWidget {
                padding: 10px;
                margin: 10px;
                text-align: center;
                width: 360px;
                border-radius: 5px;
                background: #fafafa;
                border: 2px solid #ddd;
            }

            #ContentPlaceHolder1_grdGradingRepportSummary.table tr td {
                border: 1px solid white;
            }

            .TMB {
                padding-left: 0px !important;
                padding-right: 0px !important;
                width: 16.5% !important;
            }

            .TMB1 {
                padding-left: 0px !important;
                padding-right: 0px !important;
                width: 19.5% !important;
            }

            .TMBImg {
                padding-left: 0px !important;
                padding-right: 0px !important;
                margin-left: -7%;
                margin-right: -3%;
            }

            .clscircle {
                margin-right: 7px !important;
            }

            .fixwidth {
                width: 20% !important;
            }

            .badge {
                font-size: 10px !important;
                font-weight: 200 !important;
            }

            .responsive-calendar .day {
                width: 13.7% !important;
                height: 45px;
            }

                .responsive-calendar .day.cal-header {
                    border-bottom: none !important;
                    width: 13.9% !important;
                    font-size: 17px;
                    height: 25px;
                }

            #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
                left: 0%;
            }

            #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
                left: 0%;
            }

            .bx-viewport {
                height: 285px !important;
            }

            #dailyupdates .bx-viewport {
                height: 190px !important;
            }

            .graphcmp {
                margin-left: 36%;
                font-size: 16px;
                margin-top: -3%;
                color: #666666;
                font-family: 'Roboto';
            }

            .days > div.day {
                margin: 1px;
                background: #eee;
            }

            .overdue ~ div > a {
                background: red;
            }

            .info-box {
                min-height: 95px !important;
            }

            .Dashboard-white-widget {
                padding: 5px 10px 0px !important;
            }

            .dashboardProgressbar {
                display: none;
            }

            .TMBImg > img {
                width: 47px;
            }

            #reviewersummary {
                height: 150px;
            }

            #performersummary {
                height: 150px;
            }

            #eventownersummary {
                height: 150px;
            }

            #performersummarytask {
                height: 150px;
            }

            #reviewersummarytask {
                height: 150px;
            }

            div.panel {
                margin-bottom: 12px;
            }

            .panel .panel-heading .panel-actions {
                height: 25px !important;
            }

            hr {
                margin-bottom: 8px;
            }

            .panel .panel-heading h2 {
                font-size: 18px;
            }

            td > label {
                padding: 6px;
            }

            .radioboxlist radioboxlistStyle {
                font-size: x-large;
                padding-right: 20px;
            }

            span.input-group-addon {
                padding: 0px;
            }

            td > label {
                padding: 3px 4px 0 4px;
                margin-top: -1%;
            }

            .nav-tabs > li > a {
                color: #333 !important;
            }

            .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
                color: #1fd9e1 !important;
            }
        </style>
    <style type="text/css">

        .clspenaltysave
        {
            font-weight:bold;
            margin-left :15px;
        }
        .btnss {
            background-image: url(../../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }
.table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

          .Inforamative{
        color:blue   !important;
        border-top: 1px solid #dddddd !important;
    }
    tr.Inforamative > td{
        color:blue   !important;
        border-top: 1px solid #dddddd !important;
    }
       

        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }
    </style>

    <script type="text/javascript">
        function openModal() {
            $('#AuditPopUp').modal('show');

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix" style="height: 0px"></div>
    <section class="wrapper" style="width: 98%; margin: 0;">
                <div class="row">
                    <div class="dashboard">                       
                          
                                        <!--Performer Summary-->
                                        <div id="performersummary" class="col-lg-12 col-md-12 colpadding0">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h2>Audit Summary</h2>
                                                        <div class="panel-actions" style="display:none;">
                                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer"><i class="fa fa-chevron-up"></i></a>
                                                            <a href="javascript:closeDiv('performersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>                              
                                                <div id="collapsePerformer" class="panel-collapse collapse in">
                                                    <div class="row "  >
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                                            <div class="info-box white-bg">
                                                                <div class="title">Open</div>                                                                                                                                                                                                                                                                                                                                                                    
                                                              
                                                                    <div class="col-md-12">
                                                                          
                                                                        <a href="../aspxpages/VendorAuditDashboard.aspx?AuditStatus=0">                                                                                                                                                                                                                                                                                         
                                                                              <div class="count" runat="server" id="divOpencount">0</div>
                                                                         </a>
                                                                   <%--  <div class="desc">Statutory</div>--%>
                                                               </div>
                                                                <div class="clearfix"></div>                                                                                                                           
                                                             </div>                                                            
                                                        </div>
                                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colpadding0" style="width:12px !important;">
                                                            </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                                            <div class="info-box white-bg">
                                                                <div class="title">Closed</div>                                                                                                                                                                                         
                                                                    <div class="col-md-12">
                                                                    <a href="../aspxpages/VendorAuditDashboard.aspx?AuditStatus=1">                                                                                                                                                                                            
                                                                        <div class="count" runat="server" id="divClosedcount">0</div>
                                                                        </a>
                                                                 <%--   <div class="desc">Statutory</div>--%>
                                                                </div>   
                                                                <div class="clearfix"></div>                                                                
                                                            </div>
                                                        </div>
                                               </div>                                         
                                          </div>

                                                    </div>
                                            </div>
                        </div>
      </section>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
        <ContentTemplate>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                                <%--Header Section--%>
                                <div class="clearfix"></div>

                                <div class="panel-body">
                                    <div class="col-md-12 colpadding0">

                                        <div class="col-md-12 colpadding0" style="text-align: right; float: right">
                                            
                                            <div class="col-md-10 colpadding0" style="width:69.666667%;">
                                                <div class="col-md-2 colpadding0 entrycount">
                                            <div class="col-md-3 colpadding0">
                                                <p style="color:#999; margin-top:5px"> Show </p>
                                            </div>
                                            <asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"  class="form-control m-bot15" Style="width: 64px; float: left;margin-left:10px;">
                                                <asp:ListItem Text="5"  Selected="True"/>
                                                <asp:ListItem Text="10" />
                                                <asp:ListItem Text="20" />
                                                <asp:ListItem Text="50" />
                                            </asp:DropDownList>
                                          
                                        </div>

                                                <div class="col-md-2 colpadding0 entrycount" style="float:left;margin-right: -1%;">
                                                     <asp:DropDownList runat="server" ID="ddlAuditType" class="form-control m-bot15 search-select" OnSelectedIndexChanged="ddlAuditType_SelectedIndexChanged" style="width:105px;" 
                                                         AutoPostBack="true">                
                                                         <asp:ListItem Text="All" Value="-1" Selected="True"/>
                                                <asp:ListItem Text="Open" Value="0" />
                                                <asp:ListItem Text="Closed" Value="1" />
                                                    </asp:DropDownList>         
                                                    </div> 
                                                 <div class="col-md-2 colpadding0"> 
                                     <asp:DropDownList runat="server" ID="ddlVendor" Style="width: 100px;margin-left:10px; float: left" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" 
                                            class="form-control m-bot15" AutoPostBack="true" />
                                                 </div>     
                                                <div class="col-md-2 colpadding0"> 
                                        <asp:DropDownList runat="server" ID="ddlAudit" Style="width: 100px;margin-left:10px; float: left" OnSelectedIndexChanged="ddlAudit_SelectedIndexChanged"
                                            class="form-control m-bot15" AutoPostBack="true" />
                                    </div>
                                                     <div class="col-md-4 colpadding0"> 
                                        <div style="float:left;margin-right: 1%;">
                                            <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 373px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                            CssClass="txtbox" />                                                       
                                            <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="325px"   NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>  
                                        </div>           
                                    </div>
                                                                                               
                                            </div>    
                                             <div class="col-md-2 colpadding0" style="width: 22.333333%; float:right">
                                                 <div class="col-md-6 colpadding0">  </div>
                                                <div class="col-md-6 colpadding0">                                                    
                                                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" Style="margin-left:33px !important;float: right;" runat="server" Text="Apply" onclick="btnSearch_Click"/>
                                                </div>
                                            </div>              
                                        </div>
                                       
                                         <div class="clearfix"> 
   <div runat="server" id="DivRecordsScrum" style="float: right;"  class="AdvanceSearchScrum">
             <p style="padding-right: 0px !Important;">
                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                </p>
             </div>

                                    </div>
                                </div>
                                <div class="tab-content ">
                                   <div runat="server" id="performerdocuments"  class="tab-pane active">

                                    <asp:GridView runat="server" ID="grdAuditDetail" AutoGenerateColumns="false" AllowSorting="true"
                                        GridLines="None" CssClass ="table"  OnRowCommand="grdAuditDetail_RowCommand" AllowPaging="True" PageSize="5" 
                                        DataKeyNames="ID">
                                        <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lbllocation" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>' ></asp:Label>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Vendor Name">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label ID="LabeVendorName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Audit Name">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label ID="LabeAuditName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("AuditName") %>' ToolTip='<%# Eval("AuditName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date">
                                            <ItemTemplate>
                                                   <%# Eval("AuditStartDate")!= null?((DateTime)Eval("AuditStartDate")).ToString("dd-MMM-yyyy"):""%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                            <asp:TemplateField HeaderText="End Date">
                                            <ItemTemplate>
                                                   <%# Eval("AuditStartDate")!= null?((DateTime)Eval("AuditEndDate")).ToString("dd-MMM-yyyy"):""%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">     
                                            <ItemTemplate> 
                                                 <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                 <ContentTemplate>
                                                <asp:Button runat="server" ID="btnChangeStatus" CssClass="btnss" ToolTip="Edit"
                                                CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ID") + "," + Eval("VendorID") %>'/>                                                           
                                              </ContentTemplate>
                                             <Triggers >
                                                 <asp:AsyncPostBackTrigger ControlID="btnChangeStatus" />
                                             </Triggers>
                                             </asp:UpdatePanel>
                                                 </ItemTemplate>
                                        </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Right" />
                                        <RowStyle CssClass="clsROWgrid"   />
                                        <HeaderStyle CssClass="clsheadergrid"    />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                         <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                        </asp:GridView> 
                                        <div style="margin-bottom: 4px">
                                                <table width="100%" style="height: 10px">
                                                    <tr>
                                                        <td>
                                                            <div style="margin-bottom: 4px">
                                                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                                    <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                      <div class="clearfix"> </div>                                                   
                                    </div>
                               
                                   <div class="col-md-12 colpadding0">
                                            <div class="col-md-6 colpadding0">
                                            </div>
                                            <div class="col-md-6 colpadding0">
                                                <div class="table-paging" style="margin-bottom:20px">
                                                    <asp:ImageButton ID="lBPrevious" CssClass ="table-paging-left"  runat="server" ImageUrl ="../../img/paging-left-active.png" onclick="lBPrevious_Click"/>

                                                    <div class="table-paging-text">
                                                        <p><asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label></p> 
                                                    </div>
                                                    <asp:ImageButton ID="lBNext" CssClass ="table-paging-right"  runat="server" ImageUrl ="../../img/paging-right-active.png" OnClick="lBNext_Click" />
                                                </div>
                                            </div>
                                   </div>
                            </div>    
                                                             
                        </section>
                    </div>
                </div>
            </div>
            <tr id="trErrorMessage" runat="server" visible="true">
                <td colspan="3" style="background-color: #e9e1e1;">
                    <asp:Label ID="GridViewPagingError" runat="server" Font-Names="Verdana" Font-Size="9pt"
                        ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </td>
            </tr>

             <div>
            <div class="modal fade" id="AuditPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 85%">

                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        </div>
                        <div class="modal-body" style="background-color: #f7f7f7; width:100%; max-height:650px; overflow-y:auto;">
                             <asp:UpdatePanel ID="upChecklistDetails" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <asp:ValidationSummary runat="server"
                                        ValidationGroup="ComplianceValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                    <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                                </div>
                                <div>
                                     <asp:Panel ID="Panel2" Width="100%" runat="server" Style="min-height: 250px; max-height: 400px; width: 100%; overflow-y: auto;">
                                                    <asp:GridView runat="server" ID="grdChecklist" AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true"
                                                        AllowSorting="true"
                                                        DataKeyNames="AuditDetailID" AllowPaging="True" PageSize="100" Width="100%">
                                                        <Columns>
                                                             <asp:TemplateField HeaderText="SequenceID">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="lblSequenceID" runat="server" Text='<%# Eval("SequenceID")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="SrNo">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="lblSrNo" runat="server" Text='<%# Eval("SrNo")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Category Name">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="lblCategoryName" runat="server" Text='<%# Eval("CategoryName")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="CheckList Name">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="lblCheckListName" runat="server" Text='<%# Eval("CheckListName")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="Observation">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="lblObservation" runat="server" Text='<%# Eval("Observation")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="clsROWgrid" />
                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                        <PagerTemplate>
                                                            <table style="display: none">
                                                                <tr>
                                                                    <td>
                                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </PagerTemplate>
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </asp:Panel>
                                   
                                </div>
                            </ContentTemplate>
                       
                        </asp:UpdatePanel>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
