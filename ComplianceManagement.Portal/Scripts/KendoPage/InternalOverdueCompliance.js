﻿function exportReport(e) {
   
    var ReportName = "Report of Internal Compliances";
    var customerName = document.getElementById('CustName').value;
    var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
    var grid = $("#grid").getKendoGrid();

    var rows = [
        {
            cells: [
                { value: "Entity/ Location:", bold: true },
                { value: customerName }
            ]
        },
        {
            cells: [
                { value: "Report Name:", bold: true },
                { value: ReportName }
            ]
        },
        {
            cells: [
                { value: "Report Generated On:", bold: true },
                { value: todayDate }
            ]
        },
        {
            cells: [
                { value: "" }
            ]
        },
        {
            cells: [
                { value: "Location", bold: true },           
                { value: "Short Description", bold: true },
                { value: "Performer", bold: true },           
                { value: "Due Date", bold: true },
                { value: "For Month", bold: true },          
                { value: "OverdueBy(Days)", bold: true }
            ]
        }
    ];

    var trs = grid.dataSource;
    var filteredDataSource = new kendo.data.DataSource({
        data: trs.data(),
        filter: trs.filter()
    });

    filteredDataSource.read();
    var data = filteredDataSource.view();
    for (var i = 0; i < data.length; i++) {
        var dataItem = data[i];
        rows.push({
            cells: [
                { value: dataItem.Branch },            
                { value: dataItem.ShortDescription },
                { value: dataItem.Performer },            
                { value: dataItem.InternalScheduledOn },
                { value: dataItem.ForMonth },
                { value: dataItem.OverdueBy }
            ]
        });
    }
    for (var i = 4; i < rows.length; i++) {
        for (var j = 0; j < 6; j++) {
            rows[i].cells[j].borderBottom = "#000000";
            rows[i].cells[j].borderLeft = "#000000";
            rows[i].cells[j].borderRight = "#000000";
            rows[i].cells[j].borderTop = "#000000";
            rows[i].cells[j].hAlign = "left";
            rows[i].cells[j].vAlign = "top";
            rows[i].cells[j].wrap = true;
        }
    }
    excelExport(rows, ReportName);
    e.preventDefault();
}

function excelExport(rows, ReportName) {

    var workbook = new kendo.ooxml.Workbook({
        sheets: [
            {
                columns: [
                    { width: 180 },
                    { width: 300 },
                    { width: 300 },
                    { width: 180 },
                    { width: 180 },
                    { width: 100 },
                    { width: 100 },
                    { width: 150 },
                    { width: 100 }
                ],
                title: "Report",
                rows: rows
            },
        ]
    });

    var nameOfPage = "InternalOverdueReport";
    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });

}

function FilterGrid() {

    //location details
    var locationsdetails = [];
    if ($("#dropdowntree").data("kendoDropDownTree") != undefined && $("#dropdowntree").data("kendoDropDownTree") != '') {
        var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;       
        $.each(list1, function (i, v) {
            locationsdetails.push({
                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
            });
        });
    }

    //risk Details
    var Riskdetails = [];
    var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
    $.each(list3, function (i, v) {
      
        Riskdetails.push({
            field: "Risk", operator: "eq", value: parseInt(v)
        });
    });



    //Function Details
    var Functiondetails = [];    
    if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {
       
        Functiondetails.push({
            field: "IComplianceCategoryId", operator: "eq", value: parseInt($("#dropdownfunction").val())
        });
    }

    //datefilter
    var datedetails = [];
    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
        });
    }
    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
        });
    }

    var dataSource = $("#grid").data("kendoGrid").dataSource;

    if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Functiondetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Functiondetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "or",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Riskdetails.length > 0
        && Functiondetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && Functiondetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Riskdetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Riskdetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && Functiondetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                }
            ]
        });
    }

    else if (locationsdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && Functiondetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "or",
                    filters: Functiondetails
                }
            ]
        });
    }

    else if (Riskdetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Functiondetails.length > 0
        && datedetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Functiondetails
                },
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }

    else if (Functiondetails.length > 0) {
        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Functiondetails
                }
            ]
        });
    }
    else if (datedetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "and",
                    filters: datedetails
                }
            ]
        });
    }
    else if (locationsdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: locationsdetails
                }
            ]
        });
    }
    else if (Riskdetails.length > 0) {

        dataSource.filter({
            logic: "and",
            filters: [
                {
                    logic: "or",
                    filters: Riskdetails
                }
            ]
        });
    }
    else {
        $("#grid").data("kendoGrid").dataSource.filter({});
    }
}

function ClearAllFilterMain(e) {
    $("#dropdowntree").data("kendoDropDownTree").value([]);
    $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
    $('#ClearfilterMain').css('display', 'none');
    $("#grid").data("kendoGrid").dataSource.filter({});
    e.preventDefault();
}

function fcloseStory(obj) {

    var DataId = $(obj).attr('data-Id');
    var dataKId = $(obj).attr('data-K-Id');
    var seq = $(obj).attr('data-seq');
    var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
    $(deepspan).trigger('click');
    var upperli = $('#' + dataKId);
    $(upperli).remove();

    //for rebind if any pending filter is present (Main Grid)
    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');

    CheckFilterClearorNotMain();
};

function CheckFilterClearorNotMain() {
    if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
        $('#ClearfilterMain').css('display', 'none');
    }
}

function fCreateStoryBoard(Id, div, filtername) {

    var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
    $('#' + div).html('');
    $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
    $('#' + div).css('display', 'block');

    if (div == 'filtersstoryboard') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filtertype') {
        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
    }
    else if (div == 'filterrisk') {
        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterstatus') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterpstData1') {
        $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCategory') {
        $('#' + div).append('Category&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterAct') {
        $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompSubType') {
        $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompType') {
        $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtersstoryboard1') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtertype1') {
        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterrisk1') {
        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterFY') {
        $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterUser') {
        $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterstatus1') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }

    for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
        var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
        $(button).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
        var buttontest = $($(button).find('span')[0]).text();
        if (buttontest.length > 10) {
            buttontest = buttontest.substring(0, 10).concat("...");
        }
        $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
    }

    if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
        $('#' + div).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

    }
    CheckFilterClearorNotMain();
}
