﻿
(function ($) {

    var LinkedComboBoxPlugIn = function (element, settings) {
        var elem = $(element);
        var obj = this;
        var datasource = settings.datasource;
        var config = settings;
        var selecting = false;

        this.initialize = function () {
            $("#" + elem[0].id).html('');
            $("#" + elem[0].id).append("<input type='hidden' id='" + elem[0].id + "_Value'></input>");
            loadList(null, 1);
        };

        this.setData = function (jsOnString) {
            datasource = jsOnString;
        };

        this.load = function (id, newIndex) {
            loadList(id, newIndex);
        }

        function loadList(id, newIndex) {
            var newSelectID = 'select_' + elem[0].id + '_' + newIndex;
            var dataList = null;
            var options = "";

            if (id == null) {
                hideNext(0);
                dataList = datasource;
            }
            else {
                dataList = GetDataItem(id, datasource);
                options = "<option value='" + id + "'> -- Parent --</option>";
            }

            if (dataList.length > 0) {
                if ($('#' + newSelectID).length <= 0) {
                    $("#" + elem[0].id).append('<select id=\'' + newSelectID + '\'></select>');
                }

                for (var index = 0; index < dataList.length; index++) {
                    if (dataList[index].ID == -1) {
                        options += "<option value='" + dataList[index].ID + "'>" + dataList[index].Name + "</option>";
                    }
                    else {
                        options += "<option value='" + dataList[index].ID + "'>" + dataList[index].Name + " [" + dataList[index].Category + "]" + "</option>";
                    }
                }

                $('#' + newSelectID).html(options);

                $('#' + newSelectID).change(function () {
                    hideNext(newIndex);
                    $('#' + elem[0].id + '_Value').val($('#' + newSelectID).val());
                    if (!selecting && typeof config.onSelected == "function") {
                        config.onSelected.call(this, $('#' + newSelectID).val());
                    }

                    if ($('#' + newSelectID).prop("selectedIndex") != "0") {

                        loadList($('#' + newSelectID).val(), newIndex + 1);
                    }
                });
            }
        }

        function GetDataItem(id, dataList) {
            var returnVal = null;

            for (var index = 0; index < dataList.length; index++) {
                if (dataList[index].ID == id) {
                    returnVal = dataList[index].Children;
                }
                else if (dataList[index].Children.length > 0) {
                    returnVal = GetDataItem(id, dataList[index].Children);
                }
                if (returnVal != null)
                    break;
            }

            return returnVal;
        }

        function hideNext(controlIndex) {
            for (var index = controlIndex + 1; index < 100; index++) {
                if ($('#select_' + elem[0].id + '_' + index).length > 0) {
                    $('#select_' + elem[0].id + '_' + index).remove();
                }
            }
        }

        this.setValue = function (selectedValue) {
            selecting = true;
            //$('#select_1').val(selectedValue).change();
            var hierarchy = "";
            hierarchy = GetSelectedHierarchy(selectedValue, datasource, hierarchy);
            if (hierarchy.length > 0) {
                var tokens = hierarchy.split(",");
                for (var index = 0; index < tokens.length; index++) {
                    if ($('#select_' + elem[0].id + '_' + (index + 1)).length > 0) {
                        $('#select_' + elem[0].id + '_' + (index + 1)).val(tokens[index]).change();
                    }
                }
            }
            selecting = false ;
        }

        function GetSelectedHierarchy(selectedValue, dataList, hierarchy) {
            for (var index = 0; index < dataList.length; index++) {
                if (dataList[index].ID == selectedValue) {
                    hierarchy = hierarchy + dataList[index].ID;
                }
                else if (dataList[index].Children.length > 0) {
                    hierarchy = GetSelectedHierarchy(selectedValue, dataList[index].Children, hierarchy);
                    if (hierarchy.length > 0) {
                        hierarchy = dataList[index].ID + "," + hierarchy;
                    }
                }
                if (hierarchy.length > 0)
                    break;
            }
            return hierarchy;
        }

    };


    $.fn.linkedcombobox = function (options) {
        var settings = $.extend({}, { datasource: "" }, options);
        return this.each(function () {
            var element = $(this);
            // Return early if this element already has a plugin instance
            if (element.data(this.id)) return;
            var linkedCombobox = new LinkedComboBoxPlugIn(this, settings);
            linkedCombobox.initialize();
            // Store plugin object in this element's data
            element.data(this.id, linkedCombobox);
        });
    };

})(jQuery);
