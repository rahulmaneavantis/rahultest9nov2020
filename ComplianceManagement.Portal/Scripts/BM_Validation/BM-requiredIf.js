﻿function dataValidation(e,msg) {
        e.attr("data-val", "true");
        e.attr("data-val-required", msg);
}

function removeDataValidation(e) {
        e.attr("data-val", "false");
        e.removeAttr("data-val-required");
}

function dataValidationDropDown(e,msg,v) {
    dataValidation(e, msg);
    e.attr("data-val-range-min", v);
    e.attr("data-val-range-max", "2147483647");
    e.attr("data-val-range", msg);
}

function dataValidationDropDownMax(e, msg, v) {
    dataValidation(e, msg);
    e.attr("data-val-range-min", v);
    e.attr("data-val-range-max", "2147483647");
    e.attr("data-val-range", msg);
}

function removeDataValidationDropDown(e) {
    removeDataValidation(e);
    e.removeAttr("data-val-range-min");
    e.removeAttr("data-val-range");
}

function bmValidator(e)
{
    return $(e).kendoValidator({ errorTemplate: '<div class="k-widget k-tooltip k-tooltip-validation" style="margin:0.5em"><span class="k-icon k-warning"> </span>#=message#<div class="k-callout k-callout-n"></div></div>' }).data("kendoValidator");
}