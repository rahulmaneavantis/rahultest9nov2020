﻿<%@ Page Title="User Customer Mapping" Language="C#" AutoEventWireup="true" MasterPageFile="~/AuditTool.Master" CodeBehind="UserCustomerMapping.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers.UserCustomerMapping" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 30px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
        div.dd_chk_drop {
            top: 30px;
        }

        .chosen-results {
            height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>


    <script type="text/javascript">
        function fopenpopup() {
            $('#divUserCustomerMapping').modal('show');
        }

        function closepopup() {
            location.reload();
        }
    </script>

    <script type="text/javascript">
        //function preventBack() { window.history.forward(); }
        //setTimeout("preventBack()", 10);
        //window.onunload = function () { null };

        $(document).ready(function () {
            //setactivemenu('Department Master');
            fhead('User Customer Mapping');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                            <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0" style="width:23%">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">                        
                        <asp:ListItem Text="5" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" Selected="True" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                
                </div>
                    <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px;">
                    <asp:TextBox ID="tbxFilter" runat="server" CssClass="form-control" MaxLength="50" PlaceHolder="Type to Search" AutoPostBack="true" Width="80%" OnTextChanged="tbxFilter_TextChanged"></asp:TextBox>
                     </div>
                   <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px;display:none">
                                <asp:DropDownListChosen ID="ddlCustomer" runat="server" class="form-control m-bot15" Width="70%" Height="32px" DataPlaceHolder="Customer"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"></asp:DropDownListChosen>
                   </div>
            <div style="text-align:right">
                        <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="fopenpopup()" ID="btnUserCustomerMapping" Visible="false" />
                    </div>
                   &nbsp;
            <div style="margin-bottom: 4px"> 
                <asp:GridView runat="server" ID="gridViewUserCustomerMapping" AutoGenerateColumns="false" AllowSorting="true" 
                   PageSize="20" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"  DataKeyNames="ID" OnRowCommand="gridViewUserCustomerMapping_RowCommand">
                    <Columns>
                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="10%">
                         <ItemTemplate>
                                          <%#Container.DataItemIndex+1 %>
                         </ItemTemplate>
                         </asp:TemplateField>
                        <asp:BoundField DataField="FullName" HeaderText="User Name" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="RoleName" HeaderText="Role" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%" />                                                
                        <asp:TemplateField  HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_Mapping" OnClientClick="fopenpopup()"
                                    CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png" alt="Edit Observation" data-toggle="tooltip" data-placement="top" title="Edit User Customer Mapping Details" /></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_Mapping"
                                    CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Mapping Details?');"><img src="../../Images/delete_icon_new.png" alt="Delete Department Details" data-toggle="tooltip" data-placement="top" title="Delete User Customer Mapping Details" /></asp:LinkButton>
                            </ItemTemplate>                            
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" /> 
                    <HeaderStyle BackColor="#ECF0F1" /> 
                     <PagerSettings Visible="false" />             
                    <PagerTemplate>
                                     
                      </PagerTemplate>
                </asp:GridView>
                       <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">                                   
                                    <div class="table-paging-text" style="float:right;">
                                        <p>Page
                                          
                                        </p>
                                    </div>                                    
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
            <%--</asp:Panel>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="divUserCustomerMapping" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 900px; position: absolute; left: 50%; top: 450%; transform: translate(-50%, -50%);">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closepopup();">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="AuditAssignment">
                        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                        <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                            Customer Name</label>
                                        <asp:DropDownListChosen ID="ddlCustomerName" runat="server" DataPlaceHolder="Select Customer" Width="90%" class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true">
                                        </asp:DropDownListChosen>

                                        <asp:RequiredFieldValidator ID="rfv_customerName" runat="server" ErrorMessage="Select Customer Name" ControlToValidate="ddlCustomerName"
                                            ValidationGroup="PromotorValidationGroup" Display="None">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                        <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                            User Name
                                        </label>
                                        <asp:DropDownListChosen ID="ddlUserName" runat="server" DataPlaceHolder="Select User" Width="90%" class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3">
                                        </asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ID="rfv_userName" runat="server" ErrorMessage="Select User Name" ControlToValidate="ddlUserName"
                                            ValidationGroup="PromotorValidationGroup" Display="None">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                        <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                            Role
                                        </label>
                                        <asp:DropDownListChosen ID="ddlRole1" runat="server" DataPlaceHolder="Select Role" Width="90%" class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3">
                                            <asp:ListItem Text="Select Role" Value="-1" Selected="True" />
                                            <asp:ListItem Text="Audit Head" Value="1" />
                                            <%--<asp:ListItem Text="Auditor" Value="2"/>--%>
                                        </asp:DropDownListChosen>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Select User Name" ControlToValidate="ddlRole1"
                                            ValidationGroup="PromotorValidationGroup" Display="None">
                                        </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Select Role." ControlToValidate="ddlRole1"
                                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="PromotorValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-top: 10px" align="center">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary"
                                            ValidationGroup="PromotorValidationGroup" OnClick="btnSave_Click" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="closepopup();" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
