﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditCustomerListDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers.AuditCustomerListDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('Customers');
            fhead('Customer');
        });

        function closeWin() {
            $('#divCustomersDialog').hide();
            location.reload();
        }

        function ShowDialog(CustomerID) {
            $('#divCustomersDialog').modal('show');
            $('#ICustomer').attr('width', '95%');
            $('#ICustomer').attr('height', '90%');
            $('.modal-dialog').css('width', '70%');
            $('#ICustomer').attr('src', "../Customers/AddCustomerMaster.aspx?CustomerID=" + CustomerID);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                <div style="margin: 5px">
                    <asp:UpdatePanel ID="UpdateCustomer" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 4px">
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="AuditCustomerValidationGroup" Display="None" />
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0" style="width: 23%">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" Selected="True" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-5 colpadding0 entrycount" style="margin-top: 5px;" runat="server">
                                <div class="col-md-2 colpadding0 entrycount">
                                    <label style="font-size: 13px; color: #999; margin-top: 5px;">&nbsp;</label>
                                </div>
                                <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50" PlaceHolder="Type to Search" AutoPostBack="true" Width="80%" OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                            <div style="text-align: right">
                                <span runat="server" id="CustomerTooltip" class="tool-tip" data-toggle="tooltip" data-placement="top" title="Facility to create more customer is not available in the Free version. Kindly contact Avantis to activate the same.">
                                    <asp:LinkButton Text="Add New" CssClass="btn btn-primary disabled" runat="server" ID="btnTemp"/>
                                </span>
                                <asp:Button Text="Add New" runat="server" CssClass="btn btn-primary" Style="margin-top: 5px;" OnClientClick="ShowDialog(0);" ID="btnAddCustomer" />
                                <%--OnClick="btnAddCustomer_Click"--%>
                            </div>
                            <div class="panel" style="margin-bottom: 4px;">
                                &nbsp
                                <asp:GridView runat="server" ID="grdCustomer" AutoGenerateColumns="false" AllowPaging="true" AutoPostBack="true"
                                    CssClass="table" GridLines="none" PageSize="20" Width="100%"
                                    DataKeyNames="ID" OnRowCommand="grdCustomer_RowCommand" OnPageIndexChanging="grdCustomer_PageIndexChanging" OnRowDataBound="grdCustomer_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Name" HeaderText="Customer Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />

                                        <asp:TemplateField HeaderText="Address" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 200px;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Address") %>' ToolTip='<%# Eval("Address") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Contact Person" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 130px;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("BuyerName") %>' ToolTip='<%# Eval("BuyerName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Status" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 130px;">
                                                    <asp:Label runat="server" ID="lblStatus" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <div class="text_NlinesusingCSS" style="width: 130px;">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("BuyerEmail") %>' ToolTip='<%# Eval("BuyerEmail") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-Width="5%" HeaderText="Action">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 130px;">
                                                    <asp:LinkButton runat="server" ToolTip="Change customer details" data-toggle="tooltip" data-placement="top" CommandName="EDIT_CUSTOMER" ID="lbtnEdit" CommandArgument='<%# Eval("ID") %>'><img src="/Images/edit_icon_new.png"/></asp:LinkButton>
                                                    <asp:LinkButton runat="server" CommandName="DELETE_CUSTOMER" ToolTip="Remove Customer" data-toggle="tooltip" data-placement="top" CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete" Visible="false"
                                                        OnClientClick="return confirm('Are you certain you want to delete this Customer?');"><img src="/Images/delete_icon_new.png"/></asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <HeaderStyle BackColor="#ECF0F1" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                </asp:GridView>
                                <div style="float: right;">
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                        class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                            </div>
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page
                                        </p>
                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="divCustomersDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 700px; height: 520px;">
                <div class="modal-header">
                    Add Customer <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--%>
                </div>
                <div class="modal-body" style="padding-right: 8px; height: 100%">
                    <iframe id="ICustomer" src="about:blank" frameborder="0" style="margin-left: 25px;"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
