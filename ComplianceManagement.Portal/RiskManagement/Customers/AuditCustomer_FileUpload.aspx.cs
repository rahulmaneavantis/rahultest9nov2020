﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Customers
{
    public partial class AuditCustomer_FileUpload : System.Web.UI.Page
    {
        protected static string customerID;
        public static string DocumentPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    BindCustomerListFilter();
                    BindDocumentCustomerList();
                    if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
                    {
                        if (ddlCustomerFilter.SelectedValue != "-1")
                        {
                            Session["CustomerId"] = ddlCustomerFilter.SelectedValue;
                        }
                    }
                    ViewState["ParentID"] = null;
                    if (!(AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT")))
                    {
                        //  btnAddCustomerBranch.Visible = false;
                    }
                    BindRelatedDocuments(Convert.ToInt32(Session["CustomerId"]));
                    bindPageNumber();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdCustomerFile.PageIndex = chkSelectedPage - 1;
                grdCustomerFile.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                if (Session["CustomerId"] != null)
                {
                    BindRelatedDocuments(Convert.ToInt32(Session["CustomerId"]));
                }
                upCustomerDocuments.Update();
                //bindPageNumber();
            }
        }
        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerFile.PageIndex = 0;
                BindRelatedDocuments(Convert.ToInt32(Session["CustomerId"]));
                bindPageNumber();
                upCustomerDocuments.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerFile.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindRelatedDocuments(Convert.ToInt32(Session["CustomerId"]));
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCustomerFile.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void BindCustomerListFilter()
        {
            long userId = AuthenticationHelper.UserID;
            int ServiceProviderID = AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDLForMaster(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomerFilter.DataTextField = "CustomerName";
                ddlCustomerFilter.DataValueField = "CustomerId";
                ddlCustomerFilter.DataSource = customerList;
                ddlCustomerFilter.DataBind();
                ddlCustomerFilter.Items.Insert(0, new ListItem("Select Customer", "-1"));
                ddlCustomerFilter.SelectedIndex = 2;
            }
            else
            {
                ddlCustomerFilter.DataSource = null;
                ddlCustomerFilter.DataBind();
                Response.Write("<script>alert('Please Select Customer.');</script>");
            }
        }

        protected void ddlCustomerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
            {
                if (ddlCustomerFilter.SelectedValue != "-1")
                {
                    Session["CustomerId"] = ddlCustomerFilter.SelectedValue;
                    BindRelatedDocuments(Convert.ToInt32(Session["CustomerId"]));
                }
                else
                {
                    grdCustomerFile.DataSource = null;
                    grdCustomerFile.DataBind();
                }
            }
        }

        public void BindRelatedDocuments(int CustomerId)
        {
            try
            {
                string DocType = "A";
                string TypeToSearch = string.Empty;
                string DisplayError = string.Empty;
                List<Customer_FileUpload> lstDocs = new List<Customer_FileUpload>();
                lstDocs = CustomerManagementRisk.GetDocumentMapping(DocType, CustomerId);

                if (lstDocs.Count > 0)
                {
                    lstDocs = (from g in lstDocs
                               group g by new
                               {
                                   g.CustomerId,
                                   g.FileName,
                                   g.Id,
                                   g.CreatedDate
                               } into GCS
                               select new Customer_FileUpload()
                               {
                                   CustomerId = GCS.Key.CustomerId,
                                   FileName = GCS.Key.FileName,
                                   Id = GCS.Key.Id,
                                   CreatedDate = GCS.Key.CreatedDate,

                               }).ToList();
                    if (!string.IsNullOrEmpty(tbxFilter.Text.Trim()))
                    {
                        lstDocs = lstDocs.Where(e => e.FileName.ToLower().Contains(tbxFilter.Text.ToLower().Trim())).ToList();
                    }
                    grdCustomerFile.DataSource = lstDocs;
                    Session["TotalRows"] = lstDocs.Count;
                    grdCustomerFile.DataBind();
                }
                else
                {
                    grdCustomerFile.DataSource = null;
                    grdCustomerFile.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvNoticePopUp.IsValid = false;
                //cvNoticePopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                //VSNoticePopup.CssClass = "alert alert-danger";
            }
        }

        protected void grdCustomerFile_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownLoadDoc = (LinkButton)e.Row.FindControl("lnkBtnDownLoadDoc");

            if (lnkBtnDownLoadDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownLoadDoc);
            }

            LinkButton lnkBtnDeleteDoc = (LinkButton)e.Row.FindControl("lnkBtnDeleteDoc");
            if (lnkBtnDeleteDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteDoc);

            }
        }

        protected void grdCustomerFile_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadDoc"))
                    {
                        DownloadDocument(Convert.ToInt32(e.CommandArgument));
                    }
                    else if (e.CommandName.Equals("DeleteDoc"))
                    {
                        DeleteFile(Convert.ToInt32(e.CommandArgument));
                        upCustomerDocuments.Update();
                    }
                    else if (e.CommandName.Equals("ViewDocView"))
                    {
                        var AllinOneDocumentList = CustomerManagementRisk.GetDocumentByID(Convert.ToInt32(e.CommandArgument));
                        if (AllinOneDocumentList != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (AllinOneDocumentList.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            else
                            {
                                lblMessage.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenDocviewer();", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void grdCustomerFile_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdCustomerFile.PageIndex = e.NewPageIndex;
                    BindRelatedDocuments(Convert.ToInt32(Session["CustomerId"]));
                    upCustomerDocuments.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void DownloadDocument(int FileID)
        {
            try
            {
                var file = CustomerManagementRisk.GetDocumentByID(FileID);
                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void DeleteFile(int FileID)
        {
            try
            {
                if (FileID != 0)
                {
                    if (CustomerManagementRisk.DeleteDocument(FileID, Convert.ToInt32(ddlCustomerFilter.SelectedValue)))
                    {
                        cvDocument.IsValid = false;
                        cvDocument.ErrorMessage = "Document Deleted Successfully.";
                        if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
                        {
                            if (ddlCustomerFilter.SelectedValue != "-1")
                            {
                                BindRelatedDocuments(Convert.ToInt32(ddlCustomerFilter.SelectedValue));
                                bindPageNumber();
                            }
                        }
                    }
                    else
                    {
                        cvDocument.IsValid = false;
                        cvDocument.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occurred. Please try again.";
                CustomValidator1.CssClass = "alert alert-danger";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearModelPopControls();
        }

        private void ClearModelPopControls()
        {
            drpdownCustomer.SelectedValue = "";
        }
        private void BindDocumentCustomerList()
        {
            long userId = AuthenticationHelper.UserID;
            int ServiceProviderID = AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDLForMaster(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                drpdownCustomer.DataTextField = "CustomerName";
                drpdownCustomer.DataValueField = "CustomerId";
                drpdownCustomer.DataSource = customerList;
                drpdownCustomer.DataBind();
                //drpdownCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
                //drpdownCustomer.SelectedIndex = 0;
            }
            else
            {
                drpdownCustomer.DataSource = null;
                drpdownCustomer.DataBind();
                Response.Write("<script>alert('Please Select Customer.');</script>");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (fuTaskDocUpload.HasFile)
                {
                    if (drpdownCustomer.SelectedValue != "0" && drpdownCustomer.SelectedValue != "-1" && drpdownCustomer.SelectedValue != "" && fuTaskDocUpload.HasFile)
                    {

                        Customer_FileUpload objNoticeDoc = new Customer_FileUpload()
                        {
                            CustomerId = Convert.ToInt32(drpdownCustomer.SelectedValue),
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedDate = DateTime.Now,
                            IsDeleted = false,
                            EnType = "A"
                        };

                        HttpFileCollection fileCollection = Request.Files;

                        if (fileCollection.Count > 0)
                        {
                            List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                            string directoryPath = "";
                            String fileName = "";

                            if (objNoticeDoc.CustomerId > 0)
                            {
                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadedFile = fileCollection[i];

                                    if (uploadedFile.ContentLength > 0)
                                    {
                                        string[] keys1 = fileCollection.Keys[i].Split('$');

                                        if (keys1[keys1.Count() - 1].Equals("fuTaskDocUpload"))
                                        {
                                            fileName = uploadedFile.FileName;
                                        }

                                        objNoticeDoc.FileName = fileName;

                                        //Get Document Version
                                        var taskDocVersion = CustomerManagementRisk.ExistsCustomereDocumentReturnVersion(objNoticeDoc);

                                        taskDocVersion++;
                                        objNoticeDoc.Version = taskDocVersion + ".0";

                                        directoryPath = Server.MapPath("~/CustomerDocument/" + objNoticeDoc.CustomerId + "/" + objNoticeDoc.Version);

                                        if (!Directory.Exists(directoryPath))
                                            Directory.CreateDirectory(directoryPath);

                                        Guid fileKey1 = Guid.NewGuid();
                                        string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                        Stream fs = uploadedFile.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                        fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                        objNoticeDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        objNoticeDoc.FileKey = Convert.ToString(fileKey1);
                                        objNoticeDoc.VersionDate = DateTime.Now;
                                        objNoticeDoc.UpdatedOn = DateTime.Now;
                                        objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                        DocumentManagement.Litigation_SaveDocFiles(fileList);
                                        CustomerManagementRisk.CreateCustomerDocumentMapping(objNoticeDoc);

                                        fileList.Clear();
                                    }
                                }
                            }
                        }
                        cvAddcustomer.IsValid = false;
                        cvAddcustomer.ErrorMessage = "Documents Saved Successfully.";
                        bindPageNumber();
                        upCustomerDocuments.Update();
                    }
                }
                else
                {
                    cvAddcustomer.ErrorMessage = "Please Upload Document.";
                    cvAddcustomer.IsValid = false;
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openAddModalControl();", true);
            }
            catch (Exception ex)
            {
                cvAddcustomer.IsValid = false;
                cvAddcustomer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCustomerDocuments_Click(object sender, EventArgs e)
        {
            ClearModelPopControls();
        }
    }
}