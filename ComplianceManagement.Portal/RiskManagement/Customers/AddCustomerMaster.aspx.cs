﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers
{
    public partial class AddCustomerMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    if (Convert.ToInt32(Request.QueryString["CustomerID"]) != 0)
                    {
                        BindProductType();
                        ViewState["Mode"] = 1;
                        BindEnditData(Convert.ToInt32(Request.QueryString["CustomerID"]));
                        ViewState["CustomerID"] = Convert.ToInt32(Request.QueryString["CustomerID"]);
                        BindCustomerStatus();
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                        BindProductType();
                        BindCustomerStatus();
                    }
                }
            }
        }

        public void BindProductType()
        {
            ddlProductType.DataTextField = "ProductName";
            ddlProductType.DataValueField = "ProductID";
            //   ddlProductType.DataSource = CommanClass.FillProduct();
            ddlProductType.DataSource = CommanClass.GetAllProductMapping(Convert.ToInt32(AuthenticationHelper.CustomerID));
            ddlProductType.DataBind();
        }

        protected void BindEnditData(int CustomerID)
        {
            Customer customer = CustomerManagement.GetByID(CustomerID);
            if (customer != null)
            {
                tbxCustomerName.Text = customer.Name;
                tbxAddress.Text = customer.Address;
                tbxContactPerson.Text = customer.BuyerName;
                tbxcontactNumber.Text = customer.BuyerContactNumber;
                tbxEmail.Text = customer.BuyerEmail;
                ddlCustomerStatus1.SelectedValue = Convert.ToString(customer.Status);
                var vGetProductMappedIDs = GetProductData(CustomerID);
                for (int i = 0; i < ddlProductType.Items.Count; i++)
                {
                    if (vGetProductMappedIDs != null)
                    {
                        foreach (var item in vGetProductMappedIDs)
                        {
                            if (ddlProductType.Items[i].Value == Convert.ToString(item))
                            {
                                ddlProductType.Items[i].Selected = true;
                            }
                        }
                    }
                }
                ViewState["Mode"] = 1;
                UpdatePanel1.Update();
            }
        }

        public static List<long> GetProductData(int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DeptIDs = (from row in entities.ProductMappings
                               where row.CustomerID == customerid && row.IsActive == false
                               select (long)row.ProductID).ToList();

                return DeptIDs;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool CheckCustomrCount = ICIAManagement.CheckCustomerLimit(AuthenticationHelper.ServiceProviderID);

                bool CheckCustomer = false;
                List<int?> ProductTypeList = new List<int?>();
                ProductTypeList.Clear();
                if (ddlProductType.Items.Count > 0)
                {
                    for (int i = 0; i < ddlProductType.Items.Count; i++)
                    {
                        if (ddlProductType.Items[i].Selected)
                        {
                            ProductTypeList.Add(Convert.ToInt32(ddlProductType.Items[i].Value));
                        }
                    }
                }

                if (!string.IsNullOrEmpty(tbxCustomerName.Text.Trim()) && !string.IsNullOrEmpty(tbxAddress.Text.Trim()) && !string.IsNullOrEmpty(tbxContactPerson.Text.Trim()) &&
                    !string.IsNullOrEmpty(tbxcontactNumber.Text.Trim()) && !string.IsNullOrEmpty(tbxEmail.Text.Trim()) && tbxcontactNumber.Text.Trim().Length == 10)
                {
                    Customer customer = new Customer();
                    customer.Name = tbxCustomerName.Text.Trim();
                    customer.Address = tbxAddress.Text.Trim();
                    customer.BuyerName = tbxContactPerson.Text.Trim();
                    customer.BuyerContactNumber = tbxcontactNumber.Text.Trim();
                    customer.BuyerEmail = tbxEmail.Text.Trim();
                    customer.ServiceProviderID = AuthenticationHelper.ServiceProviderID;
                    customer.VerticalApplicable = 0;
                    customer.IsServiceProvider = false;
                    //customer.Status = 1;
                    customer.Status = Convert.ToInt32(ddlCustomerStatus1.SelectedValue);// change by sagar
                    customer.IsDistributor = customer.IsServiceProvider;
                    customer.ParentID = customer.ServiceProviderID;
                    customer.ComplianceProductType = 0;
                    customer.IsDistributor = false;
                    //Added for ICAI/ICSI
                    if (customer.ParentID != null)
                    {
                        var serviceProviderCustomerRecord = CustomerManagement.GetByID(Convert.ToInt32(customer.ParentID));

                        if (serviceProviderCustomerRecord != null)
                            customer.RegistrationBy = serviceProviderCustomerRecord.RegistrationBy;
                    }

                    if ((int)ViewState["Mode"] == 1)
                    {
                        customer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                    }
                    Business.DataRisk.mst_Customer mstCustomer = new Business.DataRisk.mst_Customer();
                    mstCustomer.Name = tbxCustomerName.Text.Trim();
                    mstCustomer.Address = tbxAddress.Text.Trim();
                    mstCustomer.BuyerName = tbxContactPerson.Text.Trim();
                    mstCustomer.BuyerContactNumber = tbxcontactNumber.Text.Trim();
                    mstCustomer.BuyerEmail = tbxEmail.Text.Trim();
                    mstCustomer.ServiceProviderID = AuthenticationHelper.ServiceProviderID;
                    mstCustomer.VerticalApplicable = 0;
                    mstCustomer.Status = Convert.ToInt32(ddlCustomerStatus1.SelectedValue);// change by sagar;
                    mstCustomer.IsServiceProvider = false;
                    mstCustomer.IsDistributor = customer.IsServiceProvider;
                    mstCustomer.ParentID = customer.ServiceProviderID;
                    mstCustomer.ComplianceProductType = 0;
                    mstCustomer.IsDistributor = false;
                    mstCustomer.RegistrationBy = customer.RegistrationBy;

                    if ((int)ViewState["Mode"] == 1)
                    {
                        mstCustomer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                    }

                    if (CustomerManagement.Exists(customer))
                    {
                        CheckCustomer = false;
                    }
                    else
                    {
                        CheckCustomer = true;
                    }
                    if (CustomerManagementRisk.Exists(mstCustomer))
                    {
                        CheckCustomer = false;
                    }
                    else
                    {
                        CheckCustomer = true;
                    }
                    if (CheckCustomer)
                    {
                        if ((int)ViewState["Mode"] == 0)
                        {
                            if (CheckCustomrCount)
                            {
                                #region Add
                                bool resultDataChk = false;
                                int resultData = 0;

                                resultData = CustomerManagement.Create(customer);
                                CustomerManagement.AddCustomerCorporateMapping(resultData);
                                if (resultData > 0)
                                {
                                    resultDataChk = CustomerManagementRisk.Create(mstCustomer);
                                    if (resultDataChk)
                                    {
                                        if (fuTaskDocUpload.HasFiles)
                                        {
                                            Customer_FileUpload objNoticeDoc = new Customer_FileUpload()
                                            {
                                                CustomerId = customer.ID,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedDate = DateTime.Now,
                                                IsDeleted = false,
                                                EnType = "A"
                                            };

                                            HttpFileCollection fileCollection = Request.Files;

                                            if (fileCollection.Count > 0)
                                            {
                                                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                                string directoryPath = "";
                                                String fileName = "";

                                                if (customer.ID > 0)
                                                {
                                                    for (int i = 0; i < fileCollection.Count; i++)
                                                    {
                                                        HttpPostedFile uploadedFile = fileCollection[i];

                                                        if (uploadedFile.ContentLength > 0)
                                                        {
                                                            string[] keys1 = fileCollection.Keys[i].Split('$');

                                                            if (keys1[keys1.Count() - 1].Equals("fuTaskDocUpload"))
                                                            {
                                                                fileName = uploadedFile.FileName;
                                                            }

                                                            objNoticeDoc.FileName = fileName;

                                                            //Get Document Version
                                                            var taskDocVersion = CustomerManagementRisk.ExistsCustomereDocumentReturnVersion(objNoticeDoc);

                                                            taskDocVersion++;
                                                            objNoticeDoc.Version = taskDocVersion + ".0";

                                                            directoryPath = Server.MapPath("~/CustomerDocument/" + customer.Name + "/" + objNoticeDoc.Version);

                                                            if (!Directory.Exists(directoryPath))
                                                                Directory.CreateDirectory(directoryPath);

                                                            Guid fileKey1 = Guid.NewGuid();
                                                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                            Stream fs = uploadedFile.InputStream;
                                                            BinaryReader br = new BinaryReader(fs);
                                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                            fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                                            objNoticeDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                            objNoticeDoc.FileKey = Convert.ToString(fileKey1);
                                                            objNoticeDoc.VersionDate = DateTime.Now;
                                                            objNoticeDoc.UpdatedOn = DateTime.Now;
                                                            objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                                            DocumentManagement.Litigation_SaveDocFiles(fileList);
                                                            CustomerManagementRisk.CreateCustomerDocumentMapping(objNoticeDoc);

                                                            fileList.Clear();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (resultDataChk == false)
                                    {
                                        CustomerManagement.deleteCustReset(resultData);
                                    }
                                    //CustomerProduct mapping
                                    foreach (int aItem in ProductTypeList)
                                    {
                                        Business.Data.ProductMapping productmapping = new Business.Data.ProductMapping()
                                        {
                                            CustomerID = mstCustomer.ID,
                                            ProductID = Convert.ToInt32(aItem),
                                            IsActive = false,
                                            CreatedOn = DateTime.Now,
                                            CreatedBy = AuthenticationHelper.UserID
                                        };
                                        Business.ComplianceManagement.CreateProductMapping(productmapping);

                                        Business.DataRisk.ProductMapping_Risk ProductMapping_Risk = new Business.DataRisk.ProductMapping_Risk()
                                        {
                                            CustomerID = mstCustomer.ID,
                                            ProductID = Convert.ToInt32(aItem),
                                            IsActive = false,
                                            CreatedOn = DateTime.Now,
                                            CreatedBy = AuthenticationHelper.UserID
                                        };
                                        Business.ComplianceManagement.CreateProductMapping(productmapping);
                                        UserManagementRisk.CreateCustomerProductMapping(ProductMapping_Risk);
                                    }

                                    ProcessManagement.CreateProcessSubprocessPredefined(resultData, AuthenticationHelper.UserID);
                                    #region code to mapped IFC product
                                    //Business.DataRisk.ProductMapping_Risk productMappingRisk = new Business.DataRisk.ProductMapping_Risk()
                                    //{
                                    //    CustomerID = resultData,
                                    //    ProductID = 4,
                                    //    IsActive = false,
                                    //    CreatedBy = AuthenticationHelper.UserID,
                                    //    CreatedOn = DateTime.Now
                                    //};

                                    //Business.Data.ProductMapping productMapping = new Business.Data.ProductMapping()
                                    //{
                                    //    CustomerID = resultData,
                                    //    ProductID = 4,
                                    //    IsActive = false,
                                    //    CreatedBy = AuthenticationHelper.UserID,
                                    //    CreatedOn = DateTime.Now
                                    //};

                                    //bool isExistsInAudit = UserManagementRisk.IsCustomerProductMappingExists(productMappingRisk.CustomerID);
                                    //bool isExistsInCompliance = UserManagement.IsCustomerProductMappingExistsCompliance(productMapping.CustomerID);
                                    //if (!isExistsInAudit && !isExistsInCompliance)
                                    //{
                                    //    UserManagementRisk.CreateCustomerProductMapping(productMappingRisk);
                                    //    UserManagement.CreateCustomerProductMappingCompliance(productMapping);
                                    //}
                                    #endregion

                                    #region code added by sagar to mapped audit product.
                                    //Business.DataRisk.ProductMapping_Risk productMappingRisk1 = new Business.DataRisk.ProductMapping_Risk()
                                    //{
                                    //    CustomerID = resultData,
                                    //    ProductID = 3,
                                    //    IsActive = false,
                                    //    CreatedBy = AuthenticationHelper.UserID,
                                    //    CreatedOn = DateTime.Now
                                    //};

                                    //Business.Data.ProductMapping productMapping1 = new Business.Data.ProductMapping()
                                    //{
                                    //    CustomerID = resultData,
                                    //    ProductID = 3,
                                    //    IsActive = false,
                                    //    CreatedBy = AuthenticationHelper.UserID,
                                    //    CreatedOn = DateTime.Now
                                    //};

                                    //bool isExistsInAudit1 = UserManagementRisk.IsCustomerProductMappingExists(productMappingRisk1.CustomerID);
                                    //bool isExistsInCompliance1 = UserManagement.IsCustomerProductMappingExistsCompliance(productMapping1.CustomerID);
                                    //if (!isExistsInAudit1 && !isExistsInCompliance1)
                                    //{
                                    //    UserManagementRisk.CreateCustomerProductMapping(productMappingRisk1);
                                    //    UserManagement.CreateCustomerProductMappingCompliance(productMapping1);
                                    //}

                                    #endregion


                                    var ServiceProviderdetails = ICIAManagement.Get_ServiceProviderCustomerLimit(AuthenticationHelper.ServiceProviderID);

                                    CustomerBranchLimit obj = new CustomerBranchLimit();
                                    obj.ServiceproviderID = AuthenticationHelper.ServiceProviderID;
                                    obj.CustoemerID = Convert.ToInt32(resultData);
                                    obj.ParentBranchLimit = (int)ServiceProviderdetails.ParentBranchLimit;
                                    obj.BranchLimit = (int)ServiceProviderdetails.BranchLimit;
                                    obj.DistributorID = AuthenticationHelper.ServiceProviderID;
                                    obj.EmployeeLimit = (int)ServiceProviderdetails.EmployeeLimit;
                                    ICIAManagement.CreateCustomerBranchLimit(obj);
                                }
                                if (resultDataChk)
                                {
                                    cvAddcustomer.IsValid = false;
                                    cvAddcustomer.ErrorMessage = "Customer details added successfully.";
                                    tbxCustomerName.Text = string.Empty;
                                    tbxAddress.Text = string.Empty;
                                    tbxEmail.Text = string.Empty;
                                    tbxcontactNumber.Text = string.Empty;
                                    tbxContactPerson.Text = string.Empty;
                                }
                                #endregion
                            }
                            else
                            {
                                cvAddcustomer.ErrorMessage = "Customer limit exeed";
                                cvAddcustomer.IsValid = false;
                            }
                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            #region Update
                            CustomerManagement.Update(customer);
                            CustomerManagementRisk.Update(mstCustomer);

                            if (fuTaskDocUpload.HasFiles)
                            {
                                Customer_FileUpload objNoticeDoc = new Customer_FileUpload()
                                {
                                    CustomerId = customer.ID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedDate = DateTime.Now,
                                    IsDeleted = false,
                                    EnType = "A"
                                };

                                HttpFileCollection fileCollection = Request.Files;

                                if (fileCollection.Count > 0)
                                {
                                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                    string directoryPath = "";
                                    String fileName = "";

                                    if (customer.ID > 0)
                                    {
                                        for (int i = 0; i < fileCollection.Count; i++)
                                        {
                                            HttpPostedFile uploadedFile = fileCollection[i];

                                            if (uploadedFile.ContentLength > 0)
                                            {
                                                string[] keys1 = fileCollection.Keys[i].Split('$');

                                                if (keys1[keys1.Count() - 1].Equals("fuTaskDocUpload"))
                                                {
                                                    fileName = uploadedFile.FileName;
                                                }

                                                objNoticeDoc.FileName = fileName;

                                                //Get Document Version
                                                var taskDocVersion = CustomerManagementRisk.ExistsCustomereDocumentReturnVersion(objNoticeDoc);

                                                taskDocVersion++;
                                                objNoticeDoc.Version = taskDocVersion + ".0";

                                                directoryPath = Server.MapPath("~/CustomerDocument/" + customer.Name + "/" + objNoticeDoc.Version);

                                                if (!Directory.Exists(directoryPath))
                                                    Directory.CreateDirectory(directoryPath);

                                                Guid fileKey1 = Guid.NewGuid();
                                                string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                Stream fs = uploadedFile.InputStream;
                                                BinaryReader br = new BinaryReader(fs);
                                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                                fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                                objNoticeDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                objNoticeDoc.FileKey = Convert.ToString(fileKey1);
                                                objNoticeDoc.VersionDate = DateTime.Now;
                                                objNoticeDoc.UpdatedOn = DateTime.Now;
                                                objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                                DocumentManagement.Litigation_SaveDocFiles(fileList);
                                                CustomerManagementRisk.CreateCustomerDocumentMapping(objNoticeDoc);

                                                fileList.Clear();
                                            }
                                        }
                                    }
                                }
                            }

                            Business.ComplianceManagement.UpdateProductMapping(customer.ID);
                            UserManagementRisk.UpdateProductMapping(customer.ID);
                            foreach (int aItem in ProductTypeList)
                            {
                                Business.Data.ProductMapping productmapping = new Business.Data.ProductMapping()
                                {
                                    CustomerID = mstCustomer.ID,
                                    ProductID = Convert.ToInt32(aItem),
                                    IsActive = false,
                                    CreatedOn = DateTime.Now,
                                    CreatedBy = AuthenticationHelper.UserID
                                };
                                Business.DataRisk.ProductMapping_Risk ProductMapping_Risk = new Business.DataRisk.ProductMapping_Risk()
                                {
                                    CustomerID = mstCustomer.ID,
                                    ProductID = Convert.ToInt32(aItem),
                                    IsActive = false,
                                    CreatedOn = DateTime.Now,
                                    CreatedBy = AuthenticationHelper.UserID
                                };
                                Business.ComplianceManagement.CreateProductMapping(productmapping);
                                UserManagementRisk.CreateCustomerProductMapping(ProductMapping_Risk);
                            }
                            cvAddcustomer.IsValid = false;
                            cvAddcustomer.ErrorMessage = "Customer details updated successfully.";
                            tbxCustomerName.Text = string.Empty;
                            tbxAddress.Text = string.Empty;
                            tbxEmail.Text = string.Empty;
                            tbxcontactNumber.Text = string.Empty;
                            tbxContactPerson.Text = string.Empty;
                            #endregion
                        }
                    }
                    else
                    {
                        cvAddcustomer.ErrorMessage = "Customer name already exists.";
                        cvAddcustomer.IsValid = false;
                    }
                }

            }
            catch (Exception ex)
            {
                cvAddcustomer.IsValid = false;
                cvAddcustomer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        #region added by sagar
        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus1.DataTextField = "Name";
                ddlCustomerStatus1.DataValueField = "ID";
                var statuslist= Enumerations.GetAll<CustomerStatus>();
                statuslist = statuslist.Where(entry => entry.Name != "Suspended").ToList();
                ddlCustomerStatus1.DataSource = statuslist;
                ddlCustomerStatus1.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        } 
        #endregion

    }
}