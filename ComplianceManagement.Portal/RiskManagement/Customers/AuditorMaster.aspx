﻿<%@ Page Title="Auditor Master" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditorMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers.AuditorMaster" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <script type="text/javascript">
        function fopenpopup() {
            $('#divAuditorDialog').modal('show');
        }
    </script>
        <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <script type="text/javascript">

        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        };

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            //setactivemenu('leftremindersmenu');
            fhead('Auditor Master');
        });
        $(document).ready(function () {
            //setactivemenu('Auditor Master');
            fhead('Auditor Master');
        });

    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0" style="width:23%">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" Selected="True" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
            <div style="text-align:right">
                        <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="fopenpopup()" ID="btnAddPromotor" OnClick="btnAddPromotor_Click" />
                    </div>
            <div style="margin-bottom: 4px"> 
                &nbsp;                                           
            <%--<asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">--%>
                <asp:GridView runat="server" ID="grdAuditor" AutoGenerateColumns="false" AllowSorting="true" OnRowDataBound="grdAuditor_RowDataBound"
                     OnSorting="grdAuditor_Sorting"  DataKeyNames="ID" OnRowCommand="grdAuditor_RowCommand"
                     PageSize="20" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"
                     OnPageIndexChanging="grdAuditor_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                         <ItemTemplate>
                                          <%#Container.DataItemIndex+1 %>
                         </ItemTemplate>
                         </asp:TemplateField>
                 <asp:TemplateField HeaderText="Name" >
                        <ItemTemplate>
                            <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:150px;">
                            <asp:Label  runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                           </div>
                          </ItemTemplate>
                    </asp:TemplateField> 
                        <asp:BoundField DataField="AuditMName" HeaderText="Audit Manager's" />                        
                        <asp:BoundField DataField="AuditMEmail" HeaderText="Email" />
                        <asp:BoundField DataField="AuditMContactNo" HeaderText="Contact" />
                        <asp:TemplateField ItemStyle-Width="8%" HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_PROMOTER" OnClientClick="fopenpopup()" data-toggle="tooltip" data-placement="top" ToolTip="Edit Auditor Details"
                                    CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png"/></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_PROMOTER" Visible="false" data-toggle="tooltip" data-placement="top" ToolTip="Delete Auditor Details"
                                    CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Auditor Details?');"><img src="../../Images/delete_icon_new.png"/></asp:LinkButton>
                            </ItemTemplate>                            
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />  
                    <HeaderStyle BackColor="#ECF0F1" />
                       <PagerSettings Visible="false" />            
                    <PagerTemplate>
                                      <%--  <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                      </PagerTemplate>
                    </asp:GridView>
                  <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                    </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0"  style="float:right">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float: right;">
                                        <p>Page
                                            <%--<asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>--%>
                                            <%--<asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
            <%--</asp:Panel>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="divAuditorDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 730px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="dvAuditorDialog">
                        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional" OnLoad="upPromotor_Load">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" 
                                            ValidationGroup="PromotorValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 170px; display: block; float: left; font-size: 13px; color: #333;">
                                            Audit Firm Name</label>
                                        <asp:TextBox runat="server" ID="txtFName" CssClass="form-control" autocomplete="off" Style="width: 250px;" MaxLength="100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty." ControlToValidate="txtFName"
                                            runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="PromotorValidationGroup"
                                            ErrorMessage="Please enter a valid name." ControlToValidate="txtFName"
                                            ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #c7c7cc">
                                        <legend>Partner Details</legend>
                                        <table style="width: 100%">

                                            <tr>
                                                <td style="width: 25%">
                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Name</label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtPName" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Provide Partner's Name" ControlToValidate="txtPName"
                                                        runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>

                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Email</label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtPEmail" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                                        ValidationGroup="PromotorValidationGroup" ErrorMessage="Please enter a valid email."
                                                        ControlToValidate="txtPEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>

                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Contact No</label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtPContactNo" onkeypress="return allowOnlyNumber(event);" CssClass="form-control" Style="width: 250px;" MaxLength="32" />
                                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Contact Number can not be empty."
                                                        ControlToValidate="txtPContactNo" runat="server" ValidationGroup="PromotorValidationGroup"
                                                        Display="None" />
                                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                                        ValidationGroup="PromotorValidationGroup" ErrorMessage="Please enter a valid contact number."
                                                        ControlToValidate="txtPContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>                
                                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="txtPContactNo" />
                                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                                        ValidationGroup="PromotorValidationGroup" ErrorMessage="Please enter only 10 digit."
                                                        ControlToValidate="txtPContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                                      
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #c7c7cc">
                                        <legend>Audit Manager's Details</legend>
                                        <table style="width: 100%">

                                            <tr>
                                                <td style="width: 25%">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Name</label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtAName" CssClass="form-control" Style="width: 250px;" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Provide Audit Manager's Name" ControlToValidate="txtAName"
                                                        runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>

                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Email</label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtAEmail" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                                                        ValidationGroup="PromotorValidationGroup" ErrorMessage="Please enter a valid email."
                                                        ControlToValidate="txtAEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>

                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Contact No</label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtAContactNo" CssClass="form-control" Style="width: 250px;" MaxLength="32" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Contact Number can not be empty."
                                                        ControlToValidate="txtAContactNo" runat="server" ValidationGroup="PromotorValidationGroup"
                                                        Display="None" />
                                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator7" Display="None" runat="server"
                                                        ValidationGroup="PromotorValidationGroup" ErrorMessage="Please enter a valid contact number."
                                                        ControlToValidate="txtAContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>                
                                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers" TargetControlID="txtAContactNo" />
                                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator8" Display="None" runat="server"
                                                        ValidationGroup="PromotorValidationGroup" ErrorMessage="Please enter only 10 digit."
                                                        ControlToValidate="txtAContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                                    
                                                             </td>

                                            </tr>
                                        </table>
                                    </fieldset>
                                    <div style="margin-bottom: 7px; float: right; margin-right: 105px; margin-top: 10px;">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="PromotorValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
