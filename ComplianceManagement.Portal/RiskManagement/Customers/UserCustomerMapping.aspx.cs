﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers
{
    public partial class UserCustomerMapping : System.Web.UI.Page
    {
        public class UserList
        {
            public long ID { get; set; }
            public string FullName { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["recordForUpdate"] = "No";
                BindCustomerList();
                BindCustomerDDL();
                BindUserDDL();
                //BindRoleDDL();
                BindGridView();
                ViewState["Mode"] = 0;
                bindPageNumber();
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            gridViewUserCustomerMapping.PageIndex = chkSelectedPage - 1;
            gridViewUserCustomerMapping.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGridView();
        }
        private void BindGridView()
        {
            //long customerId = -1;
            //if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            //{
            //    if (ddlCustomer.SelectedValue != "-1")
            //    {
            //        customerId = Convert.ToInt32(ddlCustomer.SelectedValue);
            //    }
            //}
            //var userCustomerMappingList = RiskCategoryManagement.GetAllUserCustomersMapping().Where(e => e.CustomerId == customerId).ToList();


            var userCustomerMappingList = RiskCategoryManagement.GetAllUserCustomersMapping(AuthenticationHelper.ServiceProviderID).ToList();

            if (!string.IsNullOrEmpty(tbxFilter.Text.Trim()))
            {
                userCustomerMappingList = userCustomerMappingList.Where(entry => entry.CustomerName.Contains(tbxFilter.Text.Trim()) || entry.RoleName.Contains(tbxFilter.Text.Trim()) || entry.FullName.Contains(tbxFilter.Text.Trim())).ToList();
            }

            if (userCustomerMappingList.Count > 0)
            {
                gridViewUserCustomerMapping.DataSource = userCustomerMappingList;
                Session["TotalRows"] = userCustomerMappingList.Count;
                gridViewUserCustomerMapping.DataBind();
                upPromotorList.Update();
            }
            else
            {
                gridViewUserCustomerMapping.DataSource = null;
                Session["TotalRows"] = userCustomerMappingList.Count;
                gridViewUserCustomerMapping.DataBind();
                upPromotorList.Update();
            }
        }

        public List<UserList> GetAllActiveUsersList(int ServiceProviderID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var userList = (from row in entities.mst_User
                                join row1 in entities.mst_Customer
                                on row.CustomerID equals row1.ID
                                where row.IsActive == true
                                && row1.ServiceProviderID == ServiceProviderID
                                select new UserList
                                {
                                    ID = row.ID,
                                    FullName = row.FirstName + " " + row.LastName
                                }).ToList();
                return userList;
            }
        }



        public void BindCustomerDDL()
        {
            List<mst_Customer> customersList = RiskCategoryManagement.GetAllActiveCustomersList(AuthenticationHelper.ServiceProviderID);
            if (customersList.Count > 0)
            {
                ddlCustomerName.DataTextField = "Name";
                ddlCustomerName.DataValueField = "ID";
                ddlCustomerName.DataSource = customersList;
                ddlCustomerName.DataBind();
                ddlCustomerName.Items.Insert(0, new ListItem("Select Customer Name", "-1"));
            }
        }

        public void BindUserDDL()
        {
            List<UserList> usersList = GetAllActiveUsersList(AuthenticationHelper.ServiceProviderID);
            if (usersList.Count > 0)
            {
                ddlUserName.DataTextField = "FullName";
                ddlUserName.DataValueField = "ID";
                ddlUserName.DataSource = usersList;
                ddlUserName.DataBind();
                ddlUserName.Items.Insert(0, new ListItem("Select User Name", "-1"));
            }
        }

        //public void BindRoleDDL()
        //{
        //    List<mst_Role> roleList = RiskCategoryManagement.GetAllRoles();
        //    if (roleList.Count > 0)
        //    {
        //        ddlRole1.DataTextField = "Name";
        //        ddlRole1.DataValueField = "ID";
        //        ddlRole1.DataSource = roleList;
        //        ddlRole1.DataBind();
        //        //ddlRole1.Items.Insert(0, new ListItem("Select Role", "-1"));
        //    }
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long userId = 0;
                int customerId = 0;
                List<string> RoleList = new List<string>();
                RoleList.Clear();

                if (ddlRole1.Items.Count > 0)
                {
                    if (ddlRole1.SelectedValue != "-1")
                    {
                        RoleList.Add(ddlRole1.SelectedValue);
                    }
                }

                if (RoleList.Count == 0)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Select Role";
                    return;
                }

                if (ddlCustomerName.SelectedValue != "-1")
                {
                    customerId = Convert.ToInt32(ddlCustomerName.SelectedValue);
                }

                if (ddlUserName.SelectedValue != "-1")
                {
                    userId = Convert.ToInt32(ddlUserName.SelectedValue);
                }
                if (userId > 0 && RoleList.Count > 0 && customerId > 0)
                {
                    bool Success = false;
                    bool updateSuccess = false;
                    Tbl_UserCustomerMapping objectUserCustomerMapping = new Tbl_UserCustomerMapping()
                    {
                        CustomerId = customerId,
                        RoleName = ddlRole1.SelectedItem.Text,
                        UserId = userId,
                        IsActive = true,
                    };
                    long alreadyExistedId = 0;
                    alreadyExistedId = RiskCategoryManagement.CheckUserCustomerMappingExist(objectUserCustomerMapping.UserId, objectUserCustomerMapping.CustomerId);
                    string recordForUpdate = Session["recordForUpdate"].ToString();
                    if (alreadyExistedId == 0 && recordForUpdate == "No")
                    {
                        objectUserCustomerMapping.CreatedOn = DateTime.Now;
                        objectUserCustomerMapping.CreatedBy = AuthenticationHelper.UserID;
                        Success = RiskCategoryManagement.SaveUserCustomerMapping(objectUserCustomerMapping);
                    }
                    else if (alreadyExistedId > 0 && recordForUpdate == "Yes")
                    {
                        objectUserCustomerMapping.Id = alreadyExistedId;
                        objectUserCustomerMapping.UpdatedOn = DateTime.Now;
                        objectUserCustomerMapping.UpdatedBy = AuthenticationHelper.UserID;
                        updateSuccess = RiskCategoryManagement.UpdateUserCustomerMapping(objectUserCustomerMapping);
                    }
                    if (Success)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "User mapped to Customer Successfully.";
                        BindGridView();
                    }
                    if (updateSuccess)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "User mapped to Customer update Successfully.";
                        BindGridView();
                    }
                    bindPageNumber();
                    Session["recordForUpdate"] = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void gridViewUserCustomerMapping_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int mappingId = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_Mapping"))
                {
                    ViewState["Mode"] = 1;
                    Tbl_UserCustomerMapping userCustomerMapping = RiskCategoryManagement.GetUserCustomerMappingById(mappingId);
                    mst_Customer customersList = RiskCategoryManagement.GetAllActiveCustomersList(AuthenticationHelper.ServiceProviderID).Where(e1 => e1.ID == userCustomerMapping.CustomerId).FirstOrDefault();
                    if (customersList != null)
                    {
                        ddlCustomerName.SelectedValue = Convert.ToString(customersList.ID);
                    }
                    string userName = userCustomerMapping.UserId.ToString();
                    ddlUserName.SelectedValue = userName;
                    ddlRole1.SelectedItem.Text = userCustomerMapping.RoleName;
                    if (userCustomerMapping.RoleName == "Audit Head")
                    {
                        ddlRole1.SelectedValue = "1";
                    }
                    else if (userCustomerMapping.RoleName == "Auditor")
                    {
                        ddlRole1.SelectedValue = "2";
                    }
                    Session["recordForUpdate"] = "Yes";
                    //ddlUserName.Items.FindByValue(userName).Selected = true;
                    //ddlRole1.Items.FindByText(userCustomerMapping.RoleName).Selected = true;
                    ddlCustomerName.Enabled = false;
                    ddlUserName.Enabled = false;
                    upPromotor.Update();
                }
                else if (e.CommandName.Equals("DELETE_Mapping"))
                {
                    RiskCategoryManagement.DeleteUserCustomerMapping(mappingId);
                    BindGridView(); ;
                    bindPageNumber();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                gridViewUserCustomerMapping.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGridView();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = gridViewUserCustomerMapping.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        // added by sagar more on 28-01-2020
        private void BindCustomerList()
        {
            long userId = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            var customerList = RiskCategoryManagement.GetCustomerListForDDLServiceProvider(AuthenticationHelper.ServiceProviderID);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Customer", "-1"));
                ddlCustomer.SelectedIndex = 2;
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    BindGridView();
                }
                else
                {
                    Response.Write("<script>alert('Please Select Customer.');</script>");
                }
            }

        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindGridView();
            upPromotorList.Update();
        }
    }
}