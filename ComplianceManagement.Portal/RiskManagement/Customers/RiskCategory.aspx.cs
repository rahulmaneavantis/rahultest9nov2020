﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers
{
    public partial class RiskCategory : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindRiskCategory();
                //GetPageDisplaySummary();
                btnAddProcess.Visible = true;
                rdRiskProcess_SelectedIndexChanged(null, null);
                bindPageNumber();
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdRiskcategoryList.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdRiskcategoryList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindRiskCategory();
            //bindPageNumber();
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindRiskCategory()
        {
            try
            {
                var CategoryList = ProcessManagement.GetAllCategory();

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "Name")
                    {
                        CategoryList = CategoryList.OrderBy(entry => entry.Name).ToList();
                    }

                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "Name")
                    {
                        CategoryList = CategoryList.OrderByDescending(entry => entry.Name).ToList();
                    }
                    direction = SortDirection.Ascending;
                }

                string strasd = tbxFilter.Text.ToString();
                if (!(String.IsNullOrEmpty(strasd)))
                {
                    CategoryList = CategoryList.Where(entry => entry.Name.Equals(strasd)).ToList();                    
                }                   



                grdRiskcategoryList.DataSource = CategoryList;
                Session["TotalRows"] = CategoryList.Count;
                grdRiskcategoryList.DataBind();
                upProcessList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowProcessNonProcess(string flag)
        {            
            string processnonprocess = "";
            string cc = ProcessManagement.GetRiskCategoryName(flag);
            if (cc == "I")
            {
                processnonprocess = "Internal Financial Control";
            }
            else
            {
                processnonprocess = "Enterprise Risk Management";
            }


            return processnonprocess;
        }
        protected void grdRiskcategoryList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int CategoryID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_Category"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["CategoryID"] = CategoryID;
                    string IsICRRisk = "";

                    mst_Riskcategory eventData = ProcessManagement.CategoryGetByID(CategoryID);

                    tbxName.Text = eventData.Name;
                    tbxName.ToolTip = eventData.Name;
                    txtDescription.Text = eventData.Description;
                    IsICRRisk = eventData.IsICRRisk;

                    if (IsICRRisk == "I")
                    {
                        rdRiskProcess.SelectedIndex = 0;
                        //BindProcess("P");
                    }
                    else
                    {
                        rdRiskProcess.SelectedIndex = 1;
                        //BindProcess("N");
                    }

                    //ddlProcess.SelectedValue =Convert.ToString(eventData.ProcessId);
                    upProcess.Update();
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divRiskCategoryDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_Category"))
                {
                    ProcessManagement.CategoryDelete(CategoryID);
                    BindRiskCategory();
                }             
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdRiskcategoryList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //grdRiskcategoryList.PageIndex = e.NewPageIndex;
                BindRiskCategory();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdRiskcategoryList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var CategoryList = ProcessManagement.GetAllCategory();
                if (direction == SortDirection.Ascending)
                {
                    CategoryList = CategoryList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    CategoryList = CategoryList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdRiskcategoryList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdRiskcategoryList.Columns.IndexOf(field);
                    }
                }

                grdRiskcategoryList.DataSource = CategoryList;
                grdRiskcategoryList.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void grdRiskcategoryList_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
        //        if (sortColumnIndex != -1)
        //        {
        //            AddSortImage(sortColumnIndex, e.Row);
        //        }
        //    }
        //}

        //protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        //{
        //    System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
        //    sortImage.ImageAlign = ImageAlign.AbsMiddle;

        //    if (direction == SortDirection.Ascending)
        //    {
        //        sortImage.ImageUrl = "../../Images/SortAsc.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "../../Images/SortDesc.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }
        //    headerRow.Cells[columnIndex].Controls.Add(sortImage);
        //}

        protected void grdRiskcategoryList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.Footer || e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role.Equals("SADMN"))
                //    {
                //        e.Row.Cells[6].Visible = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[6].Visible = false;
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void btnAddProcess_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxName.Text = string.Empty;
                txtDescription.Text = string.Empty;
                upProcess.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divRiskCategoryDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdRiskcategoryList.PageIndex = 0;
                BindRiskCategory();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
          
            int flag = 0;
            try
            {
                string IsICRRisk = "";
                if (rdRiskProcess.SelectedItem.Text == "Internal Financial Control")
                {
                    IsICRRisk = "I";
                }
                else
                {
                    IsICRRisk = "R";
                }
                mst_Riskcategory CategoryData = new mst_Riskcategory()
                {
                    Name = tbxName.Text,
                    Description = txtDescription.Text,                   
                    IsDeleted = false,
                    IsICRRisk = IsICRRisk,                   
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    CategoryData.Id = Convert.ToInt32(ViewState["CategoryID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (ProcessManagement.CategoryExists(CategoryData.Name))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Category name already exists.";
                        
                        return;
                    }
                    else
                    {
                        flag = 1;
                        ProcessManagement.CategoryCreate(CategoryData);                                                        
                    }
                   
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    if (ProcessManagement.CategoryExists(CategoryData.Name))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Category name already exists.";
                        return;
                    }
                    else
                    {
                        flag = 2;
                        ProcessManagement.CategoryUpdate(CategoryData);
                    }
                }

                BindRiskCategory();
                if (flag==1)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Risk Category Successfully Add";    
                }
                else if (flag==2)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Risk Category Update Successfully";     
                }
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskcategoryList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upProcess_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindRiskCategory();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //private void BindProcess(string flag)
        //{
        //    try
        //    {
        //        ddlProcess.DataTextField = "Name";
        //        ddlProcess.DataValueField = "Id";
        //        ddlProcess.DataSource = ProcessManagement.FillProcess(flag);
        //        ddlProcess.DataBind();
        //        if (flag == "P")
        //        {                    
        //            ddlProcess.Items.Insert(0, new ListItem("< Select Process >", "-1"));
        //        }
        //        else
        //        {
        //            ddlProcess.Items.Insert(0, new ListItem("< Select Non Process >", "-1"));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        protected void rdRiskProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (rdRiskProcess.SelectedItem.Text == "IFC")
            //{
            //    BindProcess("P");

            //}
            //else
            //{
            //    BindProcess("N");
            //}
        }
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdRiskcategoryList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //if (!IsValid()) { return; };
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdRiskcategoryList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdRiskcategoryList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //else
                //{

                //}
                //Reload the Grid
                BindRiskCategory();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskcategoryList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdRiskcategoryList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdRiskcategoryList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        BindRiskCategory();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdRiskcategoryList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdRiskcategoryList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        BindRiskCategory();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                return true;
                //if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                //{
                //    SelectedPageNo.Text = "1";
                //    return false;
                //}
                //else if (!IsNumeric(SelectedPageNo.Text))
                //{
                //    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                //    return false;
                //}
                //else
                //{
                //    return true;
                //}
            }
            catch (FormatException)
            {
                return false;
            }
        }

    }

}