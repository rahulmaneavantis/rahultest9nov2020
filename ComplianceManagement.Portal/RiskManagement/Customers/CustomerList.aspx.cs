﻿
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
//using AuditManagement.Business;
//using AuditManagement.Business.Data;
//using AuditManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers 
{
    public partial class CustomerList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                Session["CustomerID"] = null;
                BindCustomers();                            
                ViewState["AssignedCompliancesID"] = null;
                BindCustomerStatus();
            }
        }
        private void BindCustomers()
        {
            try
            {
                //int customerID = -1;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                //}
                //var customerData = CustomerManagement.GetAll(customerID, tbxFilter.Text);
                //if (ViewState["SortOrder"].ToString() == "Asc")
                //{
                //    if (ViewState["SortExpression"].ToString() == "Name")
                //    {
                //        customerData = customerData.OrderBy(entry => entry.Name).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "Description")
                //    {
                //        customerData = customerData.OrderBy(entry => entry.BuyerName).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "BuyerContactNumber")
                //    {
                //        customerData = customerData.OrderBy(entry => entry.BuyerContactNumber).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "BuyerEmail")
                //    {
                //        customerData = customerData.OrderBy(entry => entry.BuyerEmail).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "Status")
                //    {
                //        customerData = customerData.OrderBy(entry => entry.Status).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "StartDate")
                //    {
                //        customerData = customerData.OrderBy(entry => entry.StartDate).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "EndDate")
                //    {
                //        customerData = customerData.OrderBy(entry => entry.EndDate).ToList();
                //    }               
                //    direction = SortDirection.Descending;
                //}
                //else
                //{
                //    if (ViewState["SortExpression"].ToString() == "Name")
                //    {
                //        customerData = customerData.OrderByDescending(entry => entry.Name).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "Description")
                //    {
                //        customerData = customerData.OrderByDescending(entry => entry.BuyerName).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "BuyerContactNumber")
                //    {
                //        customerData = customerData.OrderByDescending(entry => entry.BuyerContactNumber).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "BuyerEmail")
                //    {
                //        customerData = customerData.OrderByDescending(entry => entry.BuyerEmail).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "Status")
                //    {
                //        customerData = customerData.OrderByDescending(entry => entry.Status).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "StartDate")
                //    {
                //        customerData = customerData.OrderByDescending(entry => entry.StartDate).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "EndDate")
                //    {
                //        customerData = customerData.OrderByDescending(entry => entry.EndDate).ToList();
                //    }                  
                //    direction = SortDirection.Ascending;
                //}
                //List<object> dataSource = new List<object>();
                //foreach (var customerInfo in customerData)
                //{
                //    dataSource.Add(new
                //    {
                //        customerInfo.ID,
                //        customerInfo.Name,
                //        customerInfo.Address,                       
                //        customerInfo.BuyerName,
                //        customerInfo.BuyerContactNumber,
                //        customerInfo.BuyerEmail,
                //        customerInfo.CreatedOn,
                //        customerInfo.IsDeleted,
                //        customerInfo.StartDate,
                //        customerInfo.EndDate,                       
                //        Status = Enumerations.GetEnumByID<CustomerStatus>(Convert.ToInt32(customerInfo.Status != null ? (int)customerInfo.Status : -1)),
                //    });
                //}
                //grdCustomer.DataSource = dataSource;
                //grdCustomer.DataBind();
                //upCustomerList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_CUSTOMER"))
                {
                    EditCustomerInformation(customerID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER"))
                {
                    //CustomerManagement.Delete(customerID);
                    BindCustomers();
                }
                else if (e.CommandName.Equals("VIEW_COMPANIES"))
                {
                    Session["CustomerID"] = customerID;
                    Session["ParentID"] = null;
                    Response.Redirect("~/Customers/CustomerBranchList.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CheckBoxValueSaved();
                grdCustomer.PageIndex = e.NewPageIndex;
                BindCustomers();
                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCustomer_Click(object sender, EventArgs e)
        {
            // TBD
            AddCustomer();
            BindCustomers();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);

        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = 0;
                BindCustomers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_OnPreRender(object sender, EventArgs e)
        {
            try
            {
                if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
                {
                    foreach (DataControlField column in grdCustomer.Columns)
                    {
                        if (column.HeaderText == "")
                            column.Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCustomer_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                //int customerID = -1;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                //}

                //var customerlist = CustomerManagement.GetAll(customerID, tbxFilter.Text);
                //if (direction == SortDirection.Ascending)
                //{
                //    customerlist = customerlist.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Descending;
                //    ViewState["SortOrder"] = "Asc";
                //    ViewState["SortExpression"] = e.SortExpression.ToString();
                //}
                //else
                //{
                //    customerlist = customerlist.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Ascending;
                //    ViewState["SortOrder"] = "Desc";
                //    ViewState["SortExpression"] = e.SortExpression.ToString();
                //}


                //foreach (DataControlField field in grdCustomer.Columns)
                //{
                //    if (field.SortExpression == e.SortExpression)
                //    {
                //        ViewState["SortIndex"] = grdCustomer.Columns.IndexOf(field);
                //    }
                //}

                //grdCustomer.DataSource = customerlist;
                //grdCustomer.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void grdCustomer_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
        //        if (sortColumnIndex != -1)
        //        {
        //            AddSortImage(sortColumnIndex, e.Row);
        //        }
        //    }
        //}

        //protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        //{
        //    System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
        //    sortImage.ImageAlign = ImageAlign.AbsMiddle;

        //    if (direction == SortDirection.Ascending)
        //    {
        //        sortImage.ImageUrl = "../Images/SortAsc.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "../Images/SortDesc.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }
        //    headerRow.Cells[columnIndex].Controls.Add(sortImage);
        //}      
        private void CheckBoxValueSaved()
        {
            List<int> chkList = new List<int>();
            int index = -1;
            foreach (GridViewRow gvrow in grdCustomer.Rows)
            {
                index = Convert.ToInt32(grdCustomer.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkCustomer")).Checked;

                if (ViewState["CustomerID"] != null)
                    chkList = (List<int>)ViewState["CustomerID"];

                if (result)
                {
                    if (!chkList.Contains(index))
                        chkList.Add(index);
                }
                else
                    chkList.Remove(index);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["CustomerID"] = chkList;
        }

        private void AssignCheckBoxexValue()
        {
            List<int> chkList = (List<int>)ViewState["CustomerID"];
            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdCustomer.Rows)
                {
                    int index = Convert.ToInt32(grdCustomer.DataKeys[gvrow.RowIndex].Value);
                    if (chkList.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCustomer");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        protected void btnSendNotification_Click(object sender, EventArgs e)
        {
            try
            {
                upServerDownTimeDetails.Update();
                ddlStartTime.Items.Clear();
                ddlEndTime.Items.Clear();
                SetTime(ddlStartTime);
                SetTime(ddlEndTime);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenNotificationTime", "$(\"#divNotificationTime\").dialog('open');", true);




                //CheckBoxValueSaved();
                //List<int> chkList = (List<int>)ViewState["CustomerID"];
                //if (chkList != null)
                //{
                //    List<User> userList = UserManagement.GetAll(-1).Where(entry => entry.CustomerID != null).ToList();
                //    userList = userList.Where(entry => chkList.Contains((int)entry.CustomerID)).ToList();
                //    string ReplyEmailAddressName = "Avantis";

                //    foreach (User user in userList)
                //    {
                //        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                //        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                                .Replace("@User", username)
                //                                .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                                .Replace("@Password", "")
                //                                .Replace("@From", ReplyEmailAddressName);

                //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM account created.", message);
                //    }

                //}

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upServerDownTimeDetails_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDowntimeDatePicker", string.Format("initializeDowntimeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDowntimeDatePicker", "initializeDowntimeDatePicker(null);", true);
                }



            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void SetTime(DropDownList DDl)
        {
            DDl.Items.Add(new ListItem("Select", "-1"));
            for (int i = 0; i <= 12; i++)
            {
                if (i != 12)
                    DDl.Items.Add(new ListItem(i.ToString("00") + " AM", i + " AM"));
                else
                    DDl.Items.Add(new ListItem(i.ToString("00") + " PM", i + " PM"));
            }

            for (int i = 1; i <= 11; i++)
            {
                DDl.Items.Add(new ListItem(i.ToString("00") + " PM", i + " PM"));
            }
        }

        protected void btnSend_click(object sender, EventArgs e)
        {
            new Thread(() => { SendNotification(); }).Start();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenNotificationTime", "$(\"#divNotificationTime\").dialog('close');", true);
        }

        private void SendNotification()
        {
            try
            {

                string dated = txtDate.Text;
                string starttime = ddlStartTime.SelectedValue;
                string endtime = ddlEndTime.SelectedValue;

                CheckBoxValueSaved();
                List<int> chkList = (List<int>)ViewState["CustomerID"];
                if (chkList != null)
                {
                    //List<mst_User> userList = UserManagement.GetAll(-1).Where(entry => entry.CustomerID != null).ToList();
                    //userList = userList.Where(entry => chkList.Contains((int)entry.CustomerID)).ToList();
                    //string ReplyEmailAddressName = "Avantis";

                    //foreach (mst_User user in userList)
                    //{
                        //string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ServerDown
                        //                        .Replace("@User", username)
                        //                        .Replace("@Dated", dated)
                        //                        .Replace("@StartTime", starttime)
                        //                        .Replace("@EndTime", endtime)
                        //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                        //                        .Replace("@From", ReplyEmailAddressName);

                        //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM server down.", message);
                    //}

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //user Control code customer user control
        public void EditCustomerInformation(int customerID)
        {
            try
            {
                //lblErrorMassage.Text = "";
                //ViewState["Mode"] = 1;
                //ViewState["CustomerID"] = customerID;

                //mst_Customer customer = CustomerManagement.GetByID(customerID);
                //tbxName.Text = customer.Name;
                //tbxAddress.Text = customer.Address;
                //tbxBuyerName.Text = customer.BuyerName;
                //tbxBuyerContactNo.Text = customer.BuyerContactNumber;
                //tbxBuyerEmail.Text = customer.BuyerEmail;
                //txtStartDate.Text = customer.StartDate != null ? customer.StartDate.Value.ToString("dd-MM-yyyy") : " ";
                //txtEndDate.Text = customer.EndDate != null ? customer.EndDate.Value.ToString("dd-MM-yyyy") : " ";                          
                //if (customer.Status != null)
                //{
                //    ddlCustomerStatus.SelectedValue = Convert.ToString(customer.Status);
                //}
                //upCustomers.Update();
                //ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "OpenDialog", "$(\"#divCustomersDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void AddCustomer()
        {
            try
            {
                lblErrorMassage.Text = string.Empty;
                ViewState["Mode"] = 0;

                tbxName.Text = tbxAddress.Text = tbxBuyerName.Text = tbxBuyerContactNo.Text = tbxBuyerEmail.Text = txtStartDate.Text = txtEndDate.Text = string.Empty;
                ddlCustomerStatus.SelectedIndex = -1;
                upCustomers.Update();
                ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "OpenDialog", "$(\"#divCustomersDialog\").dialog('open');initializeDatePicker(null); ", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //mst_Customer customer = new mst_Customer();

                //customer.Name = tbxName.Text;
                //customer.Address = tbxAddress.Text;
                //customer.BuyerName = tbxBuyerName.Text;
                //customer.BuyerContactNumber = tbxBuyerContactNo.Text;
                //customer.BuyerEmail = tbxBuyerEmail.Text;
                //string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                //string enddate = Request[txtEndDate.UniqueID].ToString().Trim();
                //if (strtdate != "" && enddate != "")
                //{
                //    customer.StartDate = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //    customer.EndDate = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //}
                //if ((int)ViewState["Mode"] == 1)
                //{
                //    customer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                //}
                //if (CustomerManagement.Exists(customer))
                //{
                //    cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                //    cvDuplicateEntry.IsValid = false;
                //    return;
                //}
                //customer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                
                //if ((int)ViewState["Mode"] == 0)
                //{
                //    CustomerManagement.Create(customer);
                //}
                //else if ((int)ViewState["Mode"] == 1)
                //{
                //    CustomerManagement.Update(customer);
                //}
                ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "CloseDialog", "$(\"#divCustomersDialog\").dialog('close')", true);
                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }
                BindCustomers();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCustomers_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeCombobox", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// This method for binding customer status.
        /// </summary>
        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public event EventHandler OnSaved;
    }
}