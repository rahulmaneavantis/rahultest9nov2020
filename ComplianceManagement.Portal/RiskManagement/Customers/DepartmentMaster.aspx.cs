﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers
{
    public partial class DepartmentMaster : System.Web.UI.Page
    {
        private long CustomerID = -1;
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "S")
                this.MasterPageFile = "~/LicenseManagement.Master";
            else
                this.MasterPageFile = "~/AuditTool.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindCustomerListFilter();
                BindDepartmentList();
                BindCustomerList();
                bindPageNumber();
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditor.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindDepartmentList();
        }

        protected void grdAuditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

        private void BindDepartmentList()
        {
            try
            {
                // long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
                long CustomerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
                {
                    if (ddlCustomerFilter.SelectedValue != "-1")
                    {
                        CustomerID = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
                    }
                    else
                    {
                        grdAuditor.DataSource = null;
                        grdAuditor.DataBind();
                        return;
                    }
                }
                else
                {
                    grdAuditor.DataSource = null;
                    grdAuditor.DataBind();
                    return;
                }
                var DepartmentMasterList = CompDeptManagement.GetAllDepartmentMasterListAudit(CustomerID);
                grdAuditor.DataSource = DepartmentMasterList;
                Session["TotalRows"] = DepartmentMasterList.Count;
                grdAuditor.DataBind();
                upPromotorList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                        return;
                    }
                }
                Department objDeptcompliance = new Department()
                {
                    Name = txtFName.Text.Trim(),
                    IsDeleted = false,
                    CustomerID = (int)CustomerID
                };
                mst_Department objDeptaudit = new mst_Department()
                {
                    Name = txtFName.Text.Trim(),
                    IsDeleted = false,
                    CustomerID = (int)CustomerID
                };
                if ((int)ViewState["Mode"] == 1)
                {
                    objDeptcompliance.ID = Convert.ToInt32(ViewState["DeptID"]);
                    objDeptaudit.ID = Convert.ToInt32(ViewState["DeptID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (CompDeptManagement.DepartmentExists(objDeptcompliance))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Department allready exists";
                    }
                    else
                    {
                        CompDeptManagement.CreateDepartmentMaster(objDeptcompliance);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Department saved successfully";
                        txtFName.Text = string.Empty;
                    }

                    if (CompDeptManagement.DepartmentExistsAudit(objDeptaudit))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Department already exists";
                    }
                    else
                    {
                        CompDeptManagement.CreateDepartmentMasterAudit(objDeptaudit);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Department saved successfully";
                        txtFName.Text = string.Empty;
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    if (CompDeptManagement.DepartmentExists(objDeptcompliance))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Department already exists";
                    }
                    else
                    {
                        CompDeptManagement.UpdateDepartmentMaster(objDeptcompliance);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Department updated successfully";
                    }
                    if (CompDeptManagement.DepartmentExistsAudit(objDeptaudit))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Department already exists";
                    }
                    else
                    {
                        CompDeptManagement.UpdateDepartmentMasterAudit(objDeptaudit);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Department updated successfully";
                    }
                }
                BindDepartmentList();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                BindCustomerList();
                if (!string.IsNullOrEmpty(Convert.ToString(Session["CustomerId"])))
                {
                    ddlCustomer.SelectedValue = Convert.ToString(Session["CustomerId"]);
                }
                ddlCustomer.Enabled = true;
                txtFName.Text = string.Empty;
                upPromotor.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int DeptID = Convert.ToInt32(e.CommandArgument);

                if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
                {
                    if (ddlCustomerFilter.SelectedValue != "-1")
                    {
                        CustomerID = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                        return;
                    }
                }

                if (e.CommandName.Equals("EDIT_Department"))
                {
                    BindDepartmentList();
                    string value = ddlCustomerFilter.SelectedValue;
                    if (!string.IsNullOrEmpty(value))
                    {
                        ddlCustomer.SelectedValue = value;
                    }
                    ViewState["Mode"] = 1;
                    ViewState["DeptID"] = DeptID;
                    mst_Department RPD = CompDeptManagement.DepartmentMasterGetByIDAudit(DeptID, CustomerID);
                    txtFName.Text = RPD.Name;
                    ddlCustomer.Enabled = false;
                    upPromotor.Update();
                }
                else if (e.CommandName.Equals("DELETE_Department"))
                {
                    CompDeptManagement.DeleteDepartmentMaster(DeptID, CustomerID);
                    CompDeptManagement.DeleteDepartmentMasterAudit(DeptID, CustomerID);
                    BindDepartmentList();
                    bindPageNumber();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindDepartmentList();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlCustomerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
            {
                if (ddlCustomerFilter.SelectedValue != "-1")
                {
                    BindCustomerList();
                    BindDepartmentList();
                }
                else
                {
                    grdAuditor.DataSource = null;
                    grdAuditor.DataBind();
                }
            }
            else
            {
                grdAuditor.DataSource = null;
                grdAuditor.DataBind();
            }
        }

        private void BindCustomerListFilter()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDLForMaster(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomerFilter.DataTextField = "CustomerName";
                ddlCustomerFilter.DataValueField = "CustomerId";
                ddlCustomerFilter.DataSource = customerList;
                ddlCustomerFilter.DataBind();
                ddlCustomerFilter.Items.Insert(0, new ListItem("Select Customer", "-1"));
                ddlCustomerFilter.SelectedIndex = 2;
            }
            else
            {
                ddlCustomerFilter.DataSource = null;
                ddlCustomerFilter.DataBind();
            }
            ddlCustomerFilter.Visible = true;
        }

        private void BindCustomerList()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDLForMaster(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    Response.Write("<script>alert('Please Select Customer.');</script>");
                }
            }
        }
    }
}