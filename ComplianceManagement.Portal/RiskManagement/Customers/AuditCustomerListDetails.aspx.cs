﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers
{
    public partial class AuditCustomerListDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerList();
                bindPageNumber();
                DisableAddbutton();
            }
        }

        public void BindCustomerList()
        {
            var customerData = CustomerManagement.GetAllAuditCustomer(-1, tbxFilter.Text);
            List<object> dataSource = new List<object>();
            customerData = customerData.Where(e => e.ServiceProviderID == AuthenticationHelper.ServiceProviderID && e.IsServiceProvider == false).ToList();
            foreach (var customerInfo in customerData)
            {
                dataSource.Add(new
                {
                    customerInfo.ID,
                    customerInfo.Name,
                    customerInfo.Address,
                    customerInfo.Industry,
                    customerInfo.BuyerName,
                    customerInfo.BuyerContactNumber,
                    customerInfo.BuyerEmail,
                    customerInfo.CreatedOn,
                    customerInfo.IsDeleted,
                    customerInfo.StartDate,
                    customerInfo.EndDate,
                    customerInfo.DiskSpace,
                    customerInfo.Status,
                });
            }
            grdCustomer.DataSource = dataSource;
            Session["TotalRows"] = dataSource.Count;
            grdCustomer.DataBind();
        }

        public void DisableAddbutton()
        {
            bool Result = ICIAManagement.CheckCustomerLimit(AuthenticationHelper.ServiceProviderID);
            if (!Result)
            {
                CustomerTooltip.Visible = true;
                btnAddCustomer.Visible = false;
                //btnAddCustomer.Attributes["disabled"] = "disabled";
                //btnAddCustomer.ToolTip = "Facility to create more users is not available in the Free version. Kindly contact Avantis to activate the same.";
            }
            else
            {
                CustomerTooltip.Visible = false;
                btnAddCustomer.Visible = true;
                //btnAddCustomer.Attributes.Remove("disabled");
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCustomerList(); UpdateCustomer.Update();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCustomer.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindCustomerList(); UpdateCustomer.Update();
        }

        protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_CUSTOMER"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + customerID + ");", true);
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER"))
                {
                    if (CustomerManagement.CustomerDelete(customerID))
                    {
                        CustomerManagement.Delete(customerID);
                        CustomerManagementRisk.Delete(customerID);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Customer deleted successfully.";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Audits are assigned to user, please re-assign to other user.');", true);
                    }
                    BindCustomerList(); UpdateCustomer.Update();
                }
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = e.NewPageIndex;
                BindCustomerList(); UpdateCustomer.Update();
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdCustomer.PageIndex = chkSelectedPage - 1;
            grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindCustomerList(); UpdateCustomer.Update();
        }

        #region added by sagar on 11-Sept-2020
        protected void grdCustomer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                if (lblStatus != null)
                {
                    if (lblStatus.Text == "1")
                        lblStatus.Text = "Active";
                    else if(lblStatus.Text == "0")
                        lblStatus.Text = "Inactive";
                }
            }
        } 
        #endregion
    }
}