﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
//using AuditManagement.Business;
//using AuditManagement.Business.Data;
//using AuditManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Process
{
    public partial class SubProcessList : System.Web.UI.Page
    {
        //int CustomerId = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["processId"] = Session["processId"];
                    ViewState["ParentID"] = null;
                    //BindCustomerListFilter();
                    if (Request.QueryString["customerID"] != null)
                    {
                        Session["CustomerId"] = Request.QueryString["customerID"].ToString();
                    }
                    BindProcessSubType();
                    //GetPageDisplaySummary();
                    //if (AuthenticationHelper.Role.Equals("SADMN"))
                    btnAddSubEvent.Visible = true;
                    bindPageNumber();

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSubProcessList.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdSubProcessList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindProcessSubType();
            //bindPageNumber();
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindProcessSubType()
        {
            try
            {
                long customerID = -1;
                customerID = Convert.ToInt32(Session["CustomerId"].ToString());
                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }
                dlBreadcrumb.DataSource = ProcessManagement.GetHierarchy(Convert.ToInt32(ViewState["processId"]), customerID, parentID);
                dlBreadcrumb.DataBind();

                var SubProcessList = ProcessManagement.GetAll(Convert.ToInt32(ViewState["processId"]), customerID, parentID, tbxFilter.Text);
                grdSubProcessList.DataSource = SubProcessList;
                Session["TotalRows"] = SubProcessList.Count;
                grdSubProcessList.DataBind();
                upSubProcessList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdSubProcessList.PageIndex = 0;
                BindProcessSubType();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkBackToProcess_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/RiskManagement/Process/Process.aspx");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddSubEvent_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxName.Text = string.Empty;
                txtDescription.Text = string.Empty;
                txtScores.Text = string.Empty;
                PopulateInputForm();
                upSubEvents.Update();
                //   ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divSubProcessDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string processnonprocess = "";
                if (litstatus.Text == "Process")
                {
                    processnonprocess = "P";
                }
                else
                {
                    processnonprocess = "N";
                }
                mst_Subprocess Subprocess = new mst_Subprocess()
                {
                    Name = Regex.Replace(tbxName.Text.Trim(), @"\t|\n|\r", ""),
                    ProcessId = Convert.ToInt32(ViewState["processId"]),
                    ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                    UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                    IsProcessNonProcess = processnonprocess,
                    Description = txtDescription.Text.Trim(),
                };

                if (!string.IsNullOrEmpty(txtScores.Text.Trim()))
                {
                    Subprocess.Scores = Convert.ToDecimal(txtScores.Text.Trim());
                }

                if ((int)ViewState["Mode"] == 1)
                {
                    Subprocess.Id = Convert.ToInt32(ViewState["subprocessID"]);
                }

                if (ProcessManagement.SubprocessExists(Subprocess, Convert.ToInt32(ViewState["processId"])))
                {
                    cvDuplicateEntry.ErrorMessage = "Sub Process name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }


                if ((int)ViewState["Mode"] == 0)
                {
                    ProcessManagement.SubprocessCreate(Subprocess);
                    cvDuplicateEntry.ErrorMessage = "Sub Process Add successfully.";
                    cvDuplicateEntry.IsValid = false;
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    ProcessManagement.SubprocessUpdate(Subprocess);
                    cvDuplicateEntry.ErrorMessage = "Sub Process Update successfully.";
                    cvDuplicateEntry.IsValid = false;
                }

                BindProcessSubType();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSubProcessList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divSubProcessDialog\").dialog('close')", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);
                upSubEvents.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSubProcessList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int subprocessID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_SUBPROCESS"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["subprocessID"] = subprocessID;
                    mst_Subprocess subEvent = ProcessManagement.GetSubProcessByID(subprocessID);
                    PopulateInputForm();
                    tbxName.Text = subEvent.Name;
                    tbxName.ToolTip = subEvent.Name;
                    upSubEvents.Update();
                    txtDescription.Text = subEvent.Description;
                    txtScores.Text = Convert.ToString(subEvent.Scores);
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divSubProcessDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_SUBPROCESS"))
                {
                    ProcessManagement.SubEventDelete(subprocessID);
                    BindProcessSubType();
                }
                else if (e.CommandName.Equals("VIEW_CHILDREN"))
                {
                    if (ViewState["processId"] != null)
                    {
                        Session["processId"] = null;
                        Session["processId"] = ViewState["processId"];
                        Session["subprocessId"] = null;
                        Session["subprocessId"] = subprocessID;
                        Response.Redirect("~/RiskManagement/Process/ActivityList.aspx?customerID="+ Session["CustomerId"].ToString(), false);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSubProcessList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdSubProcessList.PageIndex = e.NewPageIndex;
                BindProcessSubType();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdSubProcessList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }
                long customerID = -1;
                customerID = Convert.ToInt32(Session["CustomerId"].ToString());
                var eventCatagory = ProcessManagement.GetAll(Convert.ToInt32(ViewState["processId"]), customerID, parentID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    eventCatagory = eventCatagory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    eventCatagory = eventCatagory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdSubProcessList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdSubProcessList.Columns.IndexOf(field);
                    }
                }

                grdSubProcessList.DataSource = eventCatagory;
                grdSubProcessList.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void grdSubProcessList_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
        //        if (sortColumnIndex != -1)
        //        {
        //            AddSortImage(sortColumnIndex, e.Row);
        //        }
        //    }
        //}

        //protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        //{
        //    System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
        //    sortImage.ImageAlign = ImageAlign.AbsMiddle;

        //    if (direction == SortDirection.Ascending)
        //    {
        //        sortImage.ImageUrl = "../../Images/SortAsc.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "../../Images/SortDesc.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }
        //    headerRow.Cells[columnIndex].Controls.Add(sortImage);
        //}

        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                    BindProcessSubType();
                    upSubProcessList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateInputForm()
        {
            try
            {
                long customerID = -1;
                customerID = Convert.ToInt32(Session["CustomerId"].ToString());
                litCustomer.Text = ProcessManagement.GetByID(Convert.ToInt32(ViewState["processId"]), customerID).Name;
                litstatus.Text = Session["processnonprocess"].ToString();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSubProcessList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.Footer || e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
                //    {
                //        e.Row.Cells[4].Visible = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[4].Visible = false;
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSubProcessList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                ////if (!IsValid()) { return; };

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdSubProcessList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdSubProcessList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //else
                //{

                //}
                //Reload the Grid
                BindProcessSubType();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSubProcessList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdSubProcessList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSubProcessList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        BindProcessSubType();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdSubProcessList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSubProcessList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        BindProcessSubType();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        //private bool IsValid()
        //{
        //    try
        //    {
        //        if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
        //        {
        //            SelectedPageNo.Text = "1";
        //            return false;
        //        }
        //        else if (!IsNumeric(SelectedPageNo.Text))
        //        {
        //            //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
        //            return false;
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //    catch (FormatException)
        //    {
        //        return false;
        //    }
        //}


    }
}