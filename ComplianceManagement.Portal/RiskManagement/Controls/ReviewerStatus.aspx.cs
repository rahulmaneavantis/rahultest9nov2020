﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Controls
{
    public partial class ReviewerStatus : System.Web.UI.Page
    {
        int RiskCreationId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["ScheduledOnID"]))
                    if (!String.IsNullOrEmpty(Request.QueryString["RiskCreationId"]))
                        if (!String.IsNullOrEmpty(Request.QueryString["FinancialYear"]))
                            if (!String.IsNullOrEmpty(Request.QueryString["CustomerBranchID"]))
                                if (!String.IsNullOrEmpty(Request.QueryString["Period"]))
                                {
                                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["ScheduledOnID"]);
                                    RiskCreationId = Convert.ToInt32(Request.QueryString["RiskCreationId"]);
                                    string FinancialYear = Request.QueryString["FinancialYear"];
                                    int CustomerBranchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                                    string Period = Request.QueryString["Period"];
                                    OpenTransactionPage(ScheduledOnID, RiskCreationId, FinancialYear, CustomerBranchID, Period);
                                }
                BindResponsiblePersons(RiskCreationId);
                //Page Title
                int customerID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    customerID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                }
                int SubProcessID = 0;
                int ProcessID = 0;
                string ControlNo = string.Empty;
                int CB = 0;
                string Pname = string.Empty;
                string SPname = string.Empty;
                string VName = string.Empty;
                var BrachName = string.Empty;
                var VerticalName = string.Empty;
                var Period1 = string.Empty;
                var FY = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["FinancialYear"])))
                {
                    FY = "/" + Convert.ToString(Request.QueryString["FinancialYear"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["Period"])))
                {
                    Period1 = "/" + Convert.ToString(Request.QueryString["Period"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["CustomerBranchID"]))
                {
                    CB = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
                    BrachName = CustomerBranchManagement.GetByID(CB).Name;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SPID"]))
                {
                    SubProcessID = Convert.ToInt32(Request.QueryString["SPID"]);
                    mst_Subprocess objsubProcess = new mst_Subprocess();
                    objsubProcess = ProcessManagement.GetSubProcessByID(SubProcessID);
                    if (objsubProcess != null)
                    {
                        SPname = "/ " + objsubProcess.Name;
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                    Mst_Process objprocess = new Mst_Process();
                    objprocess = ProcessManagement.GetByID(ProcessID, customerID);
                    if (objprocess != null)
                    {
                        Pname = "/ " + objprocess.Name;
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["CID"]))
                {
                    ControlNo = "/ " + Convert.ToString(Request.QueryString["CID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalName = UserManagementRisk.GetVerticalName(customerID, Convert.ToInt32((Request.QueryString["VID"])));
                }
                if (string.IsNullOrEmpty(VerticalName))
                {
                    VName = "/ " + VerticalName;
                }
                LblPageDetails.InnerText = BrachName + FY + Period1 + Pname + SPname + VName + ControlNo;

                //End
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer", string.Format("initializeDatePickerforPerformer(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer", "initializeDatePickerforPerformer(null);", true);
                }
                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", string.Format("initializeDatePickerforPerformer1(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", "initializeDatePickerforPerformer1(null);", true);
                }


                //ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeComboboxUpcoming();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));

                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.BinaryWrite(file.FileData); // create the file
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlDesign_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }


        protected void ddlOprationEffectiveness_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void rptComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<GetAuditDocumentsView> ComplianceFileData = new List<GetAuditDocumentsView>();
                List<GetAuditDocumentsView> ComplianceDocument = new List<GetAuditDocumentsView>();
                ComplianceDocument = DashboardManagementRisk.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("version"))
                {
                    if (e.CommandName.Equals("version"))
                    {
                        rptComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                        rptComplianceDocumnets.DataBind();
                        var documentVersionData = ComplianceDocument.Select(x => new
                        {
                            ID = x.ID,
                            ScheduledOnID = x.ScheduledOnID,
                            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                            VersionDate = x.VersionDate,
                            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                        }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                        rptComplianceVersion.DataSource = documentVersionData;
                        rptComplianceVersion.DataBind();

                        upComplianceDetails.Update();


                    }
                }
                else if (e.CommandName.Equals("Download"))
                {

                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        var ComplianceData = DashboardManagementRisk.GetForMonth(Convert.ToInt32(commandArgs[0]));
                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                        if (ComplianceFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string[] filename = file.FileName.Split('.');
                                    string str = filename[0] + i + "." + filename[1];
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    i++;
                                }
                            }
                        }

                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] data = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=TestingDocuments.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        upComplianceDetails.Update();
                    }
                }
                else if (e.CommandName.Equals("View"))
                {
                    foreach (var file in ComplianceFileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + File;

                            string extension = System.IO.Path.GetExtension(filePath);

                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }

                            string customerID = string.Empty;
                            if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                            {
                                customerID = Request.QueryString["CustomerID"];
                            }
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            if (file.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();

                            string CompDocReviewPath = FileName;
                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestView('" + CompDocReviewPath + "');", true);
                        }
                        else
                        {
                            // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                        }
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
            }
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {

                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        protected void rdbtnStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlDesign.SelectedItem.Text == "Pass" && ddlOprationEffectiveness.SelectedItem.Text == "Fail" && rdbtnStatus.SelectedItem.Text.Trim() == "Closed")
                {
                    RequiredFieldValidator3.Visible = true;
                    divpersonresponsible.Visible = true;
                }
                else if (ddlDesign.SelectedItem.Text == "Fail" && ddlOprationEffectiveness.SelectedItem.Text == "Select TOE" && rdbtnStatus.SelectedItem.Text.Trim() == "Closed")
                {
                    RequiredFieldValidator3.Visible = true;
                    divpersonresponsible.Visible = true;
                }
                else
                {
                    divpersonresponsible.Visible = false;
                    RequiredFieldValidator3.Visible = false;
                }
                txtRemark.Attributes.Remove("disabled");
                tbxDate.Attributes.Remove("disabled");
                btnSave.Attributes.Remove("disabled");
                //rdbtnStatus.Attributes.Add("disabled", "disabled");               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPersonResponsible_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                int StatusID = 0;
                if (lblStatus.Visible)
                    StatusID = Convert.ToInt32(rdbtnStatus.SelectedValue);
                else
                    StatusID = Convert.ToInt32(ComplianceManagement.Business.ComplianceManagement.GetClosedTransaction(Convert.ToInt32(hdnAuditScheduledOnId.Value)).ComplianceStatusID);

                AuditTransaction transaction = new AuditTransaction()
                {
                    AuditScheduleOnID = Convert.ToInt64(hdnAuditScheduledOnId.Value),
                    AuditInstanceId = Convert.ToInt64(hdnAuditInstanceID.Value),
                    RiskCreationID = Convert.ToInt64(hdnRiskCreatinID.Value),
                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                    CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                    StatusId = StatusID,
                    StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                   // Remarks = txtRemark.Text,
                    DesignResult = Convert.ToInt32(ddlDesign.SelectedValue),
                    OprationResult = Convert.ToInt32(ddlOprationEffectiveness.SelectedValue),
                    CustomerBranchId = Convert.ToInt32(ViewState["CustomerBranchID"].ToString()),
                    FinancialYear = ViewState["FinancialYear"].ToString(),
                    Remarks = txtReasonForPassOrFailRemark.Text
                };
                UserManagementRisk.CreateTransaction(transaction, null, null, null);

                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }


                HttpFileCollection fileCollection1 = Request.Files;
                if (fileCollection1.Count > 0)
                {
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    int? customerID = -1;
                    if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                    {
                        customerID =Convert.ToInt32(Request.QueryString["CustomerID"]);
                    }
                    var InstanceData = RiskCategoryManagement.GetAuditInstanceData(Convert.ToInt32(hdnRiskCreatinID.Value), Convert.ToInt64(hdnAuditInstanceID.Value));
                    string directoryPath1 = "";
                    if (InstanceData.ProcessId != -1 && InstanceData.SubProcessId != -1 && InstanceData.CustomerBranchID != -1)
                    {
                        directoryPath1 = Server.MapPath("~/AuditTestingAdditionalDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.RiskCreationId + "/" + ViewState["FinancialYear"].ToString() + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.SubProcessId.ToString() + "/" + hdnAuditScheduledOnId.Value + "/1.0");
                    }
                    else if (InstanceData.ProcessId != -1 && InstanceData.CustomerBranchID != -1)
                    {
                        directoryPath1 = Server.MapPath("~/AuditTestingAdditionalDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.RiskCreationId + "/" + ViewState["FinancialYear"].ToString() + "/" + InstanceData.ProcessId.ToString() + "/" + hdnAuditScheduledOnId.Value + "/1.0");
                    }
                    DocumentManagement.CreateDirectory(directoryPath1);
                    for (int i = 0; i < fileCollection1.Count; i++)
                    {
                        HttpPostedFile uploadfile1 = fileCollection1[i];
                        string[] keys1 = fileCollection1.Keys[i].Split('$');
                        String fileName1 = "";
                        if (keys1[keys1.Count() - 1].Equals("FileuploadAdditionalFile"))
                        {
                            fileName1 = "AuditTestingAdditionalDocumentDoc_" + uploadfile1.FileName;
                            // list.Add(new KeyValuePair<string, int>(fileName1, 1));
                        }
                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(uploadfile1.FileName));
                        Stream fs = uploadfile1.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                        if (uploadfile1.ContentLength > 0)
                        {
                            ReviewHistory RH = new ReviewHistory()
                            {
                                RiskCreationId = Convert.ToInt64(hdnRiskCreatinID.Value),
                                CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                Dated = DateTime.Now,
                                //Remarks = txtRemark.Text,
                                Remarks = txtReasonForPassOrFailRemark.Text,
                                AuditScheduleOnID = Convert.ToInt64(hdnAuditScheduledOnId.Value),
                                FinancialYear = ViewState["FinancialYear"].ToString(),
                                CustomerBranchId = Convert.ToInt32(ViewState["CustomerBranchID"].ToString()),
                                ForMonth = ViewState["Period"].ToString(),
                                Name = fileName1,
                                FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                FileKey = fileKey1.ToString(),
                                Version = "1.0",
                                VersionDate = DateTime.UtcNow,

                            };
                            bool Flag = RiskCategoryManagement.CreateReviewRemark(RH);
                            if (Flag == true)
                                cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Remark Saved Successfully";
                            DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        }
                    }
                }
                int CustomerBranchID = Convert.ToInt32(ViewState["CustomerBranchID"].ToString());
                int RiskCreationId = Convert.ToInt32(hdnRiskCreatinID.Value);
                int ScheduledOnID = Convert.ToInt32(hdnAuditScheduledOnId.Value);

                if (ddlDesign.SelectedItem.Text == "Pass" && ddlOprationEffectiveness.SelectedItem.Text == "Fail" && rdbtnStatus.SelectedItem.Text.Trim() == "Closed")
                {


                    var fetchActionPlan = UserManagementRisk.Get_PlanAssignment(CustomerBranchID, RiskCreationId, ScheduledOnID);
                    if (fetchActionPlan != null)
                    {
                        ActionPlanAssignment transactionup = new ActionPlanAssignment()
                        {
                            RiskCreationId = RiskCreationId,
                            TOD = Convert.ToInt32(ddlDesign.SelectedValue),
                            TOE = Convert.ToInt32(ddlOprationEffectiveness.SelectedValue),
                            PersonResponsible = Convert.ToInt32(ddlPersonResponsible.SelectedValue),
                            CustomerBranchId = CustomerBranchID,
                            FinancialYear = ViewState["FinancialYear"].ToString(),
                            AuditScheduleOnID = ScheduledOnID,
                            ActionPlan = txtMngntesponce.Text,
                            TimeLine = DateTime.ParseExact(txtDateResp.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        };
                        UserManagementRisk.updateActionPlanAssignment(transactionup);
                    }
                    else
                    {
                        ActionPlanAssignment transaction1 = new ActionPlanAssignment()
                        {
                            RiskCreationId = Convert.ToInt64(hdnRiskCreatinID.Value),
                            TOD = Convert.ToInt32(ddlDesign.SelectedValue),
                            TOE = Convert.ToInt32(ddlOprationEffectiveness.SelectedValue),
                            PersonResponsible = Convert.ToInt32(ddlPersonResponsible.SelectedValue),
                            CustomerBranchId = Convert.ToInt32(ViewState["CustomerBranchID"].ToString()),
                            FinancialYear = ViewState["FinancialYear"].ToString(),
                            AuditScheduleOnID = Convert.ToInt64(hdnAuditScheduledOnId.Value),
                            ActionPlan = txtMngntesponce.Text,
                            TimeLine = DateTime.ParseExact(txtDateResp.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        };
                        UserManagementRisk.CreateActionPlanAssignment(transaction1);
                    }
                }
                else if (ddlDesign.SelectedItem.Text == "Fail" && ddlOprationEffectiveness.SelectedItem.Text == "Select TOE"
                    && rdbtnStatus.SelectedItem.Text.Trim() == "Closed")
                {
                    if (ddlDesign.SelectedItem.Text == "Fail" && rdbtnStatus.SelectedItem.Text.Trim() == "Closed")
                    {
                        var fetchActionPlan = UserManagementRisk.Get_PlanAssignment(CustomerBranchID, RiskCreationId, ScheduledOnID);
                        if (fetchActionPlan != null)
                        {
                            ActionPlanAssignment transactionup = new ActionPlanAssignment()
                            {
                                RiskCreationId = RiskCreationId,
                                TOD = Convert.ToInt32(ddlDesign.SelectedValue),
                                TOE = Convert.ToInt32(ddlOprationEffectiveness.SelectedValue),
                                PersonResponsible = Convert.ToInt32(ddlPersonResponsible.SelectedValue),
                                CustomerBranchId = CustomerBranchID,
                                FinancialYear = ViewState["FinancialYear"].ToString(),
                                AuditScheduleOnID = ScheduledOnID,
                                ActionPlan = txtMngntesponce.Text,
                                TimeLine = DateTime.ParseExact(txtDateResp.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                            };
                            UserManagementRisk.updateActionPlanAssignment(transactionup);
                        }
                        else
                        {
                            ActionPlanAssignment transaction1 = new ActionPlanAssignment()
                            {
                                RiskCreationId = Convert.ToInt64(hdnRiskCreatinID.Value),
                                TOD = Convert.ToInt32(ddlDesign.SelectedValue),
                                TOE = Convert.ToInt32(ddlOprationEffectiveness.SelectedValue),
                                PersonResponsible = Convert.ToInt32(ddlPersonResponsible.SelectedValue),
                                CustomerBranchId = Convert.ToInt32(ViewState["CustomerBranchID"].ToString()),
                                FinancialYear = ViewState["FinancialYear"].ToString(),
                                AuditScheduleOnID = Convert.ToInt64(hdnAuditScheduledOnId.Value),
                                ActionPlan = txtMngntesponce.Text,
                                TimeLine = DateTime.ParseExact(txtDateResp.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                            };
                            UserManagementRisk.CreateActionPlanAssignment(transaction1);
                        }
                    }
                }
                else
                {
                    var fetchActionPlan = UserManagementRisk.Get_PlanAssignment(CustomerBranchID, RiskCreationId, ScheduledOnID);
                    if (fetchActionPlan != null)
                    {
                        if (rdbtnStatus.SelectedItem.Text.Trim() == "Closed")
                        {
                            ActionPlanAssignment transactionup = new ActionPlanAssignment()
                            {
                                RiskCreationId = RiskCreationId,
                                TOD = Convert.ToInt32(ddlDesign.SelectedValue),
                                TOE = Convert.ToInt32(ddlOprationEffectiveness.SelectedValue),
                                PersonResponsible = Convert.ToInt32(ddlPersonResponsible.SelectedValue),
                                CustomerBranchId = CustomerBranchID,
                                FinancialYear = ViewState["FinancialYear"].ToString(),
                                AuditScheduleOnID = ScheduledOnID,
                                ActionPlan = txtMngntesponce.Text,
                                TimeLine = DateTime.ParseExact(txtDateResp.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                            };
                            UserManagementRisk.updateActionPlanAssignment(transactionup);
                        }
                    }
                }
                RiskActivityTransaction objRiskDoc = new RiskActivityTransaction()
                {
                    RiskCreationId = Convert.ToInt64(hdnRiskCreatinID.Value),
                    DocumentsExamined = txtDocumentExamined.Text.ToString()
                };
                UserManagementRisk.UpdateDocumentExamine(objRiskDoc);
                BindRemarks(Convert.ToInt32(hdnRiskCreatinID.Value), ViewState["FinancialYear"].ToString(), ViewState["Period"].ToString());
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Remark Saved Successfully";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["complianceInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceTransactionHistory = Business.DashboardManagementRisk.GetAllTransactions(Convert.ToInt32(ViewState["complianceInstanceID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory.DataSource = complianceTransactionHistory;
                grdTransactionHistory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.RiskCategoryManagement.GetFile(fileID);

                    if (file.FilePath == null)
                    {
                        using (FileStream fs = File.OpenRead(Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name))))
                        {
                            int length = (int)fs.Length;
                            byte[] buffer;

                            using (BinaryReader br = new BinaryReader(fs))
                            {
                                buffer = br.ReadBytes(length);
                            }

                            Response.Buffer = true;
                            Response.Clear();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            // processnonprocess = ProcessManagement.GetClientnameName(branchid);
            return processnonprocess;
        }
        public static List<ReviewHistory> GetFileData1(int id, int RiskCreationId, int AuditScheduleOnId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.ReviewHistories
                                where row.ID == id && row.RiskCreationId == RiskCreationId
                                && row.AuditScheduleOnID == AuditScheduleOnId
                                select row).ToList();

                return fileData;
            }
        }
        protected void DownLoadClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                //Get the row that contains this button
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblID = (Label)gvr.FindControl("lblID");
                Label lblriskID = (Label)gvr.FindControl("lblriskID");
                Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");


                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<ReviewHistory> fileData = GetFileData1(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblriskID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text));
                    int i = 0;
                    string directoryName = string.Empty;
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        directoryName = file.ForMonth;
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=TestingDocument.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int RiskCreationId, string FinancialYear, int CustomerBranchID, string Period)
        {
            try
            {
                BindResponsiblePersons(RiskCreationId);

                txtRemark.Text = tbxDate.Text = string.Empty;
                BindTransactionDetails(ScheduledOnID, RiskCreationId, FinancialYear, CustomerBranchID, Period);
                //upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();

                    }
                }


            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        private void BindResponsiblePersons(int RiskCreationID)
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    customerID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                }
                //customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                ddlPersonResponsible.DataTextField = "Name";
                ddlPersonResponsible.DataValueField = "ID";
                ddlPersonResponsible.DataSource = UserManagementRisk.GetAllNonAdminUser(customerID, RiskCreationID);
                //var users = UserManagementRisk.GetAllNonAdminUser(customerID, RiskCreationId);
                ddlPersonResponsible.DataBind();
                ddlPersonResponsible.Enabled = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                // throw new NotImplementedException();
                grdTransactionHistory.DataSource = Business.DashboardManagementRisk.GetAllTransactions(ScheduledOnID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStatusList(int statusID)
        {
            try
            {

                rdbtnStatus.Items.Clear();
                //var statusList = ComplianceStatusManagement.GetStatusList();
                var statusList = AuditStatusManagement.GetStatusList();

                List<AuditStatu> allowedStatusList = null;
                List<AuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetStatusTransitionListByInitialId(statusID);
                //List<auStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

                foreach (AuditStatu st in allowedStatusList)
                {
                    if (!(st.ID == 6))
                    {
                        rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                    }
                }

                lblStatus.Visible = allowedStatusList.Count > 0 ? true : false;
                divDated.Visible = allowedStatusList.Count > 0 ? true : false;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindRemarks(int RiskCreationId, string FinancialYear, string Period)
        {
            try
            {
                // throw new NotImplementedException();
                if (RiskCreationId != -1 && !string.IsNullOrEmpty(FinancialYear) && !string.IsNullOrEmpty(Period))
                {
                    GridRemarks.DataSource = Business.DashboardManagementRisk.GetAllRemarks(RiskCreationId, FinancialYear, Period);
                    GridRemarks.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {

                bool result = false;

                //if (userID == com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID)
                //{
                if (roleID == 3)
                {
                    result = statusID == 1;
                }
                else if (roleID == 4)
                {
                    result = statusID == 2 || statusID == 3;
                }
                else if (roleID == 5)
                {
                    result = statusID == 2 || statusID == 3;
                }
                else if (roleID == 6)
                {
                    result = statusID == 4 || statusID == 5 || statusID == 6;
                }
                //}

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int RiskCreationId, string FinancialYear, int CustomerBranchID, string Period)
        {
            try
            {
                txtDocumentExamined.Text = string.Empty;
                txtReasonforPassorFail.Text = string.Empty;
                lbDownloadSample.Text = string.Empty;
                txtReasonforPassorFail.Text = string.Empty;
                txtDateResp.Text = string.Empty;
                txtMngntesponce.Text = string.Empty;
                ViewState["complianceInstanceID"] = ScheduledOnID;

                var complianceInfo = RiskCategoryManagement.GetAuditByInstanceID(ScheduledOnID, CustomerBranchID);
                var complianceForm = RiskCategoryManagement.GetAuditSampleFormByID(complianceInfo.RiskCreationId);
                var MstRiskResult = RiskCategoryManagement.GetMst_RiskResultyID(RiskCreationId, ScheduledOnID);

                var fetchActionPlan = UserManagementRisk.Get_PlanAssignment(CustomerBranchID, RiskCreationId, ScheduledOnID);



                if (fetchActionPlan != null)
                {
                    ddlPersonResponsible.SelectedValue = Convert.ToString(fetchActionPlan.PersonResponsible);
                    ddlPersonResponsible.Enabled = false;
                    txtDateResp.Text = fetchActionPlan.TimeLine != null ? fetchActionPlan.TimeLine.Value.ToString("dd-MM-yyyy") : " ";
                    txtMngntesponce.Text = fetchActionPlan.ActionPlan;

                }


                var RecentComplianceTransaction = RiskCategoryManagement.GetCurrentStatusByComplianceID(ScheduledOnID);
                txtDocumentExamined.Text = complianceInfo.DocumentsExamined;
                txtReasonforPassorFail.Text = MstRiskResult.ResonForPassFail;
                txtSampleTested.Text = MstRiskResult.SampleTested;
                txtDeviation.Text = MstRiskResult.Deviation;

                if (MstRiskResult.TOD != null)
                {
                    ddlDesign.SelectedValue = Convert.ToString(MstRiskResult.TOD);
                }
                if (MstRiskResult.TOE != null)
                {
                    ddlOprationEffectiveness.SelectedValue = Convert.ToString(MstRiskResult.TOE);
                }
                ViewState["FinancialYear"] = null;
                ViewState["FinancialYear"] = FinancialYear;
                ViewState["CustomerBranchID"] = null;
                ViewState["CustomerBranchID"] = CustomerBranchID;
                ViewState["Period"] = null;
                ViewState["Period"] = Period;
                if (complianceForm != null)
                {
                    lbDownloadSample.Text = "Click <u>here</u> to download sample form.";
                    lbDownloadSample.CommandArgument = complianceForm.RiskCreationID.ToString();
                    lblNote.Visible = true;
                }
                else
                {
                    lblNote.Visible = false;
                }


                var documentVersionData = DashboardManagementRisk.GetFileData1(Convert.ToInt32(RecentComplianceTransaction.AuditScheduleOnID)).Select(x => new
                {
                    ID = x.ID,
                    ScheduledOnID = x.ScheduledOnID,
                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                    VersionDate = x.VersionDate,
                    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();


                rptComplianceVersion.DataSource = documentVersionData;
                rptComplianceVersion.DataBind();

                rptComplianceDocumnets.DataSource = null;
                rptComplianceDocumnets.DataBind();



                BindStatusList(Convert.ToInt32(RecentComplianceTransaction.AuditStatusID));
                BindTransactions(ScheduledOnID);
                BindRemarks(RiskCreationId, FinancialYear, Period);
                txtRemark.Text = string.Empty;
                tbxDate.Text = RecentComplianceTransaction.StatusChangedOn != null ? RecentComplianceTransaction.StatusChangedOn.Value.ToString("dd-MM-yyyy") : " ";
                hdnRiskCreatinID.Value = RiskCreationId.ToString();
                hdnAuditScheduledOnId.Value = ScheduledOnID.ToString();
                hdnAuditInstanceID.Value = RecentComplianceTransaction.AuditInstanceId.ToString();

                rdbtnStatus.Attributes.Add("disabled", "disabled");
                txtRemark.Attributes.Add("disabled", "disabled");
                tbxDate.Attributes.Add("disabled", "disabled");
                btnSave.Attributes.Add("disabled", "disabled");

                upComplianceDetails.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.onunload = CloseWindow();");
        }
    }
}