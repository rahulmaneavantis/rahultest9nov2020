﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logger;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class Mst_Audit_Config : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindComplianceTypes();
            }
        }

        private void BindComplianceTypes()
        {
            try
            {
                var reminderTemplates =GetAllReminderTemplates(Portal.Common.AuthenticationHelper.CustomerID);

                List<AuditReminderTemplate> reminder = new List<AuditReminderTemplate>();
                List<AuditReminderTemplate> escalationreminder = new List<AuditReminderTemplate>();
                reminder = reminderTemplates.Where(entry => entry.IsEscalationReminder == "R").ToList();
                escalationreminder = reminderTemplates.Where(entry => entry.IsEscalationReminder == "E").ToList();
                
                cblMonthly.Items.Clear();
                reminder.Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsSubscribed }).ToList().ForEach(item =>
                {
                    cblMonthly.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });
                
                cbescalate.Items.Clear();
                escalationreminder.Select(entry => new { ID = entry.ID, Name = entry.TimeInDays + " Days ", IsSelected = entry.IsSubscribed }).ToList().ForEach(item =>
                {
                    cbescalate.Items.Add(new ListItem(item.Name, item.ID.ToString()) { Selected = item.IsSelected });
                });
                upReminderConfiguration.Update();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        public static List<AuditReminderTemplate> GetAllReminderTemplates(long CustomerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var templateToUpdate = (from row in entities.AuditReminderTemplates
                                                          where row.CustomerID == CustomerId                                                          
                                        select row).ToList();

                return templateToUpdate;
            }
        }     
        public static bool Exists(long CustID, int days,string flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditReminderTemplates
                             where row.CustomerID== CustID                             
                             && row.TimeInDays == days
                             && row.IsEscalationReminder == flag
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void Create(AuditReminderTemplate ProcessData)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.AuditReminderTemplates.Add(ProcessData);
                entities.SaveChanges();

            }
        }
        protected void btnpresave_Click(object sender, EventArgs e)
        {
            int days = -1;
            if (!string.IsNullOrEmpty(ddlpre.SelectedValue))
            {
                days =Convert.ToInt32(ddlpre.SelectedValue);
            }
            int bdays = -1;
            if (!string.IsNullOrEmpty(txtBeforeInDays.Text))
            {
                bdays = Convert.ToInt32(txtBeforeInDays.Text);
            }
            AuditReminderTemplate Data = new AuditReminderTemplate()
            {
                TimeInDays = days ,
                IsEscalationReminder="R",
                IsSubscribed=true,               
                CustomerID = Portal.Common.AuthenticationHelper.CustomerID,
                BeforeInDays= bdays

            };

            if (Exists(Portal.Common.AuthenticationHelper.CustomerID, days, "R"))
            {
                cvDuplicateEntry.ErrorMessage = "Data already exists.";
                cvDuplicateEntry.IsValid = false;
                return;
            }
            else
            {
                Create(Data);
                cvDuplicateEntry.ErrorMessage = "Save Successfully.";
                cvDuplicateEntry.IsValid = false;
            }
            BindComplianceTypes();
            txtBeforeInDays.Text = string.Empty;
        }
        protected void btnescatate_Click(object sender, EventArgs e)
        {
            int days = -1;
            if (!string.IsNullOrEmpty(ddlescalate.SelectedValue))
            {
                days = Convert.ToInt32(ddlescalate.SelectedValue);
            }
            AuditReminderTemplate Data = new AuditReminderTemplate()
            {
                TimeInDays = days,
                IsEscalationReminder = "E",
                IsSubscribed = true,
                CustomerID = Portal.Common.AuthenticationHelper.CustomerID
            };

            if (Exists(Portal.Common.AuthenticationHelper.CustomerID, days, "E"))
            {
                cvDuplicateEntry.ErrorMessage = "Data already exists.";
                cvDuplicateEntry.IsValid = false;
                return;
            }
            else
            {
                Create(Data);
                cvDuplicateEntry.ErrorMessage = "Save Successfully.";
                cvDuplicateEntry.IsValid = false;
            }
            BindComplianceTypes();
        }        
    }
}