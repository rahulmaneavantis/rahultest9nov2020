﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class ICFRDetailedReport : System.Web.UI.Page
    {
        protected List<int> Branchlist = new List<int>();
        protected List<string> Quarterlist = new List<string>();
        protected static string AuditHeadOrManagerReport;
        protected List<Int32> roles;
        protected bool personresponsibleapplicable = false;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesICFR(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            personresponsibleapplicable = UserManagementRisk.PersonResponsibleExists(Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                //CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindCustomerList();
                BindLegalEntityData();
                BindFnancialYear();
                BindddlQuater();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFilterFinancial.ClearSelection();
                    ddlFilterFinancial.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
            }
        }
        private void BindddlQuater()
        {
            if (ddlQuarter.SelectedValue != null)
            {
                ddlQuarter.DataTextField = "Name";
                ddlQuarter.DataValueField = "ID";
                ddlQuarter.DataSource = UserManagementRisk.GetQuarterList();
                ddlQuarter.DataBind();
            }
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", string.Format("initializeDatePicker11(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", "initializeDatePicker11(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            upComplianceTypeList.Update();
        }

        protected string GetTestResult(string TOD, string TOE, string AuditStatusID)
        {
            try
            {
                string returnvalue = string.Empty;
                if ((TOD == "2" || TOD == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Design Failure (TOD)";
                }
                else if ((TOE == "2" || TOE == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Effectiveness Failure (TOE)";
                }
                else if ((TOD == null || TOE == "") || (TOE == null || TOE == ""))
                {
                    returnvalue = "Not Tested";
                }
                else if ((TOD == "2" || TOD == "-1") && AuditStatusID == "2")
                {
                    returnvalue = "Not Tested";
                }
                else
                {
                    returnvalue = "Pass";
                }
                return returnvalue;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetFrequencyName(long ID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var transactionsQuery = (from row in entities.mst_Frequency
                                             where row.Id == ID
                                             select row).FirstOrDefault();

                    return transactionsQuery.Name.Trim();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        DateTime StartDate;
                        DateTime EndDate;
                        string FnancialYear = "";
                        int CustomerBranchId = -1;
                        string CustomerBranchName = "";
                        string customerName = string.Empty;
                        if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                        {
                            if (ddlLegalEntity.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                                CustomerBranchName = ddlLegalEntity.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                        {
                            if (ddlSubEntity1.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                                CustomerBranchName = ddlSubEntity1.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                        {
                            if (ddlSubEntity2.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                                CustomerBranchName = ddlSubEntity2.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                        {
                            if (ddlSubEntity3.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                                CustomerBranchName = ddlSubEntity3.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                        {
                            if (ddlSubEntity4.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                                CustomerBranchName = ddlSubEntity4.SelectedItem.Text;
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                        {
                            if (ddlFilterFinancial.SelectedValue != "-1")
                            {
                                FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                            }
                        }
                        else
                        {
                            FnancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                        }

                        //if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && (!string.IsNullOrEmpty(tbxEndDate.Text)))
                        //{
                        //    StartDate= DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        //    EndDate = DateTime.ParseExact(tbxEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        //}

                        if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                        {
                            if (ddlSubEntity4.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                                CustomerBranchName = ddlSubEntity4.SelectedItem.Text;
                            }
                        }

                        //if (CustomerId == 0)
                        //{
                        //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        //}
                        if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                        {
                            if (ddlCustomer.SelectedValue != "-1")
                            {
                                CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                                customerName = ddlCustomer.SelectedItem.Text.ToString();
                            }
                        }
                        // added on 15-06-2020
                        List<int> branchIdsList = new List<int>();
                        if (CustomerBranchId == -1 && String.IsNullOrEmpty(CustomerBranchName))
                        {
                            //var branches = AuditKickOff_NewDetails.GetBranchAgainstCustomerForIFC(CustomerId);
                            //if (branches.Count > 0)
                            //{
                            //    foreach (var branch in branches)
                            //    {
                            //        branchIdsList.Add(branch.ID);
                            //    }
                            //}

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Unit.";
                            return;
                        }
                        else
                        {
                            branchIdsList.Add(CustomerBranchId);
                        }
                        string QuarterName = string.Empty;
                        Quarterlist.Clear();
                        for (int i = 0; i < ddlQuarter.Items.Count; i++)
                        {
                            if (ddlQuarter.Items[i].Selected)
                            {
                                QuarterName += ddlQuarter.Items[i].Text.Trim() + ",";
                                Quarterlist.Add(ddlQuarter.Items[i].Text.Trim());
                            }
                        }

                        QuarterName = QuarterName.Trim(',');

                        if (Quarterlist.Count > 0)
                        {
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchy(CustomerId, branchIdsList);
                            //  var bracnhes = GetAllHierarchy(CustomerId, CustomerBranchId);
                            var Branchlistloop = Branchlist.ToList();
                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                if (Branchlist.Count > 0)
                                {
                                    //var MasterRecords = (from row in entities.AuditInstanceTransactionViews
                                    //                         where Branchlist.Contains(row.CustomerBranchID)
                                    //                         && row.FinancialYear == FnancialYear
                                    //                         && row.UserID == com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID
                                    //                     select row).ToList();
                                    List<AuditInstanceTransactionView> MasterRecords = new List<AuditInstanceTransactionView>();
                                    if (CustomerManagementRisk.CheckIsManagement(Portal.Common.AuthenticationHelper.UserID) == 8)
                                    {
                                        MasterRecords = (from row in entities.AuditInstanceTransactionViews
                                                         join EAAR in entities.EntitiesAssignmentManagementRisks
                                                         on row.ProcessId equals (int)EAAR.ProcessId
                                                         where row.CustomerBranchID == (int)EAAR.BranchID
                                                         && EAAR.UserID == Portal.Common.AuthenticationHelper.UserID
                                                         where Branchlist.Contains(row.CustomerBranchID)
                                                         && row.FinancialYear == FnancialYear
                                                         select row).Distinct().ToList();
                                    }
                                    else if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                                    {
                                        MasterRecords = (from row in entities.AuditInstanceTransactionViews
                                                         join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                                         on row.ProcessId equals (int)EAAR.ProcessId
                                                         where row.CustomerBranchID == (int)EAAR.BranchID
                                                         && EAAR.UserID == Portal.Common.AuthenticationHelper.UserID
                                                         where Branchlist.Contains(row.CustomerBranchID)
                                                         && row.FinancialYear == FnancialYear
                                                         select row).Distinct().ToList();
                                    }
                                    else
                                    {

                                        MasterRecords = (from row in entities.AuditInstanceTransactionViews
                                                         where Branchlist.Contains(row.CustomerBranchID)
                                                         && row.FinancialYear == FnancialYear
                                                         && row.UserID == Portal.Common.AuthenticationHelper.UserID
                                                         select row).ToList();
                                    }

                                    if (Quarterlist.Count > 0)
                                    {
                                        MasterRecords = MasterRecords.Where(entry => Quarterlist.Contains(entry.ForMonth)).ToList();
                                    }

                                    if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && (!string.IsNullOrEmpty(tbxEndDate.Text)))
                                    {
                                        StartDate = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        EndDate = DateTime.ParseExact(tbxEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        MasterRecords = MasterRecords.Where(entry => entry.StatusChangedOn >= StartDate && entry.StatusChangedOn <= EndDate).ToList();
                                    }
                                    MasterRecords = MasterRecords.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                                    var details = GetICFRAuditStatus(MasterRecords, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Convert.ToInt32(CustomerId));

                                    if (MasterRecords.Count == 0)
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Data Not Found.";
                                        return;
                                    }
                                    #region Sheet 1                                   
                                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Summary_of_TestResults");
                                    DataTable ExcelData1 = null;
                                    DataView view1 = new System.Data.DataView(details);
                                    if (view1.Table.Rows.Count > 0)
                                    {
                                        ExcelData1 = view1.ToTable("Selected", false, "Process", "NoOfControlKey", "NoOfControlNonKey", "TestResultPassKey", "TestResultPassNonKey", "TestResultFailKey", "TestResultFailNonKey", "NotTestedcount");

                                        exWorkSheet1.Cells["A1"].Value = "Report Generated On :";
                                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A1"].Style.Font.Size = 12;

                                        exWorkSheet1.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                                        exWorkSheet1.Cells["B1"].Style.Font.Size = 12;

                                        exWorkSheet1.Cells["A2"].Value = customerName;
                                        exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A2"].Style.Font.Size = 12;

                                        exWorkSheet1.Cells["A3"].Value = "Entity Name :";
                                        exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A3"].Style.Font.Size = 12;

                                        exWorkSheet1.Cells["B3"].Value = CustomerBranchName;
                                        exWorkSheet1.Cells["B3"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["B3:D3"].Merge = true;

                                        exWorkSheet1.Cells["A4"].Value = "Financial Year :";
                                        exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A4"].Style.Font.Size = 12;

                                        exWorkSheet1.Cells["B4"].Value = FnancialYear;
                                        exWorkSheet1.Cells["B4"].Style.Font.Size = 12;

                                        exWorkSheet1.Cells["A5"].Value = "Quarter :";
                                        exWorkSheet1.Cells["A5"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A5"].Style.Font.Size = 12;

                                        exWorkSheet1.Cells["B5"].Value = QuarterName.Trim(',');
                                        exWorkSheet1.Cells["B5"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["B5:D5"].Merge = true;

                                        ExcelData1.Columns[0].ColumnName = " ";
                                        ExcelData1.Columns[1].ColumnName = "  ";
                                        ExcelData1.Columns[2].ColumnName = "   ";
                                        ExcelData1.Columns[3].ColumnName = "    ";
                                        ExcelData1.Columns[4].ColumnName = "     ";
                                        ExcelData1.Columns[5].ColumnName = "      ";
                                        ExcelData1.Columns[6].ColumnName = "       ";
                                        ExcelData1.Columns[7].ColumnName = "        ";
                                        ExcelData1.AcceptChanges();

                                        exWorkSheet1.Cells["A10"].LoadFromDataTable(ExcelData1, true);

                                        exWorkSheet1.Cells["A7"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["A7"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["A7"].Value = "Process";
                                        exWorkSheet1.Cells["A7:A9"].Merge = true;
                                        exWorkSheet1.Cells["A7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        exWorkSheet1.Cells["A7"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["A7"].AutoFitColumns(30);

                                        exWorkSheet1.Cells["B7"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["B7"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["B7"].Value = "No Of Controls";
                                        exWorkSheet1.Cells["B7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["B7"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["B7:C8"].Merge = true;

                                        exWorkSheet1.Cells["D7"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["D7"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["D7"].Value = "Test Results";
                                        exWorkSheet1.Cells["D7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["D7"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["D7:G7"].Merge = true;

                                        exWorkSheet1.Cells["D8"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["D8"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["D8"].Value = "Pass";
                                        exWorkSheet1.Cells["D8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["D8"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["D8:E8"].Merge = true;

                                        exWorkSheet1.Cells["F8"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["F8"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["F8"].Value = "Fail";
                                        exWorkSheet1.Cells["F8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["F8"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["F8:G8"].Merge = true;

                                        exWorkSheet1.Cells["B9"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["B9"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["B9"].Value = "Key";
                                        exWorkSheet1.Cells["B9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["B9"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["B9"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["C9"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["C9"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["C9"].Value = "Non Key";
                                        exWorkSheet1.Cells["C9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["C9"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["C9"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["D9"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["D9"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["D9"].Value = "Key";
                                        exWorkSheet1.Cells["D9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["D9"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["D9"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["E9"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["E9"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["E9"].Value = "Non Key";
                                        exWorkSheet1.Cells["E9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["E9"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["E9"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["F9"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["F9"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["F9"].Value = "Key";
                                        exWorkSheet1.Cells["F9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["F9"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["F9"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["G9"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["G9"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["G9"].Value = "Non Key";
                                        exWorkSheet1.Cells["G9"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        exWorkSheet1.Cells["G9"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["G9"].AutoFitColumns(15);

                                        exWorkSheet1.Cells["H7"].Style.Font.Bold = true;
                                        exWorkSheet1.Cells["H7"].Style.Font.Size = 12;
                                        exWorkSheet1.Cells["H7"].Value = "Not Tested";
                                        exWorkSheet1.Cells["H7:H9"].Merge = true;
                                        exWorkSheet1.Cells["H6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        exWorkSheet1.Cells["H7"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        exWorkSheet1.Cells["H7"].AutoFitColumns(15);

                                        using (ExcelRange col = exWorkSheet1.Cells[7, 1, 10 + ExcelData1.Rows.Count, 8])
                                        {
                                            col.Style.WrapText = true;

                                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                            // Assign borders
                                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        }
                                    }// If Dataview Empty end
                                    #endregion

                                    #region Sheet 2                                        
                                    ExcelWorksheet exWorkSheet2 = exportPackge.Workbook.Worksheets.Add("List_of_FailedControls");
                                    DataTable ExcelData2 = null;
                                    var FailedData = MasterRecords.ToList();
                                    FailedData = FailedData.Where(a => a.AuditStatusID == 3 && (a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2)).ToList();
                                    ExcelData2 = (DataTable)FailedData.ToDataTable();
                                    DataView view2 = new System.Data.DataView(ExcelData2);
                                    if (view2.Table.Rows.Count > 0)
                                    {
                                        ExcelData2 = view2.ToTable("Selected", false, "ControlNo", "ProcessName", "SubProcessName", "ActivityDescription", "ControlDescription", "KeyName", "Frequency", "TOD", "TOE", "AuditStatusID", "CustomerBranchID", "RiskCreationId", "ScheduledOnID");

                                        ExcelData2.Columns.Add("FrequencyName");
                                        ExcelData2.Columns.Add("TestResult");

                                        ExcelData2.Columns.Add("ManagementResponse");
                                        ExcelData2.Columns.Add("TimeLine");
                                        ExcelData2.Columns.Add("PersonResponsible");

                                        if (ExcelData2.Rows.Count > 0)
                                        {
                                            foreach (DataRow item in ExcelData2.Rows)
                                            {
                                                item["FrequencyName"] = GetFrequencyName(Convert.ToInt32(item["Frequency"].ToString()));
                                                item["TestResult"] = GetTestResult(item["TOD"].ToString(), item["TOE"].ToString(), item["AuditStatusID"].ToString());

                                                var fetchActionPlan = UserManagementRisk.Get_PlanAssignment(Convert.ToInt32(item["CustomerBranchID"].ToString()), Convert.ToInt32(item["RiskCreationId"].ToString()), Convert.ToInt32(item["ScheduledOnID"].ToString()));
                                                if (fetchActionPlan != null)
                                                {
                                                    item["ManagementResponse"] = fetchActionPlan.ActionPlan;

                                                    if (fetchActionPlan.TimeLine != null)
                                                        item["TimeLine"] = Convert.ToDateTime(fetchActionPlan.TimeLine).ToString("dd-MMM-yyyy");

                                                    var userdetails = UserManagementRisk.GetByID(Convert.ToInt32(fetchActionPlan.PersonResponsible));
                                                    if (userdetails != null)
                                                        item["PersonResponsible"] = userdetails.FirstName + ' ' + userdetails.LastName;
                                                }
                                            }

                                            ExcelData2.Columns.Remove("Frequency");
                                            ExcelData2.Columns.Remove("TOE");
                                            ExcelData2.Columns.Remove("TOD");
                                            ExcelData2.Columns.Remove("AuditStatusID");

                                            ExcelData2.Columns.Remove("CustomerBranchID");
                                            ExcelData2.Columns.Remove("RiskCreationId");
                                            ExcelData2.Columns.Remove("ScheduledOnID");

                                            exWorkSheet2.Cells["A1"].Value = "Report Generated On :";
                                            exWorkSheet2.Cells["A1"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["A1"].Style.Font.Size = 12;

                                            exWorkSheet2.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                                            exWorkSheet2.Cells["B1"].Style.Font.Size = 12;

                                            exWorkSheet2.Cells["A2"].Value = customerName;
                                            exWorkSheet2.Cells["A2"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["A2"].Style.Font.Size = 12;

                                            exWorkSheet2.Cells["A3"].Value = "Entity Name :";
                                            exWorkSheet2.Cells["A3"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["A3"].Style.Font.Size = 12;

                                            exWorkSheet2.Cells["B3"].Value = CustomerBranchName;
                                            exWorkSheet2.Cells["B3"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["B3:D3"].Merge = true;

                                            exWorkSheet2.Cells["A4"].Value = "Financial Year :";
                                            exWorkSheet2.Cells["A4"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["A4"].Style.Font.Size = 12;

                                            exWorkSheet2.Cells["B4"].Value = FnancialYear;
                                            exWorkSheet2.Cells["B4"].Style.Font.Size = 12;

                                            exWorkSheet2.Cells["A5"].Value = "Quarter :";
                                            exWorkSheet2.Cells["A5"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["A5"].Style.Font.Size = 12;

                                            exWorkSheet2.Cells["B5"].Value = QuarterName.Trim(',');
                                            exWorkSheet2.Cells["B5"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["B5:D5"].Merge = true;

                                            exWorkSheet2.Cells["A7"].LoadFromDataTable(ExcelData2, true);

                                            exWorkSheet2.Cells["A7"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["A7"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["A7"].Value = "ControlNo";
                                            exWorkSheet2.Cells["A7"].AutoFitColumns(20);

                                            exWorkSheet2.Cells["B7"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["B7"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["B7"].Value = "Process";
                                            exWorkSheet2.Cells["B7"].AutoFitColumns(30);

                                            exWorkSheet2.Cells["C7"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["C7"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["C7"].Value = "Sub Process";
                                            exWorkSheet2.Cells["C7"].AutoFitColumns(30);

                                            exWorkSheet2.Cells["D7"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["D7"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["D7"].Value = "Risk Description";
                                            exWorkSheet2.Cells["D7"].AutoFitColumns(30);

                                            exWorkSheet2.Cells["E7"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["E7"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["E7"].Value = "Control Description";
                                            exWorkSheet2.Cells["E7"].AutoFitColumns(30);

                                            exWorkSheet2.Cells["F7"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["F7"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["F7"].Value = "Key/Non Key";
                                            exWorkSheet2.Cells["F7"].AutoFitColumns(30);

                                            exWorkSheet2.Cells["G7"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["G7"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["G7"].Value = "Frequency";
                                            exWorkSheet2.Cells["G7"].AutoFitColumns(30);

                                            exWorkSheet2.Cells["H7"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["H7"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["H7"].Value = "Test Result";
                                            exWorkSheet2.Cells["H7"].AutoFitColumns(30);

                                            exWorkSheet2.Cells["I7"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["I7"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["I7"].Value = "Management Response";
                                            exWorkSheet2.Cells["I7"].AutoFitColumns(30);

                                            exWorkSheet2.Cells["J7"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["J7"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["J7"].Value = "Time Line";
                                            exWorkSheet2.Cells["J7"].AutoFitColumns(30);

                                            exWorkSheet2.Cells["K7"].Style.Font.Bold = true;
                                            exWorkSheet2.Cells["K7"].Style.Font.Size = 12;
                                            exWorkSheet2.Cells["K7"].Value = "Person Responsible";
                                            exWorkSheet2.Cells["K7"].AutoFitColumns(30);

                                            using (ExcelRange col = exWorkSheet2.Cells[7, 1, 7 + ExcelData2.Rows.Count, 11])
                                            {
                                                col.Style.Numberformat.Format = "dd-MMM-yyyy";
                                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                                col.Style.WrapText = true;
                                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Sheet 3                                        
                                    ExcelWorksheet exWorkSheet3 = exportPackge.Workbook.Worksheets.Add("Detailed_TestResults");
                                    DataTable ExcelData3 = null;
                                    ExcelData3 = (DataTable)MasterRecords.ToDataTable();
                                    DataView view3 = new System.Data.DataView(ExcelData3);
                                    if (view3.Table.Rows.Count > 0)
                                    {
                                        ExcelData3 = view3.ToTable("Selected", false, "ControlNo", "ProcessName", "SubProcessName", "ActivityDescription", "ControlDescription", "KeyName", "Frequency", "TOD", "TOE", "AuditStatusID", "SampleTested", "Deviation");

                                        ExcelData3.Columns.Add("FrequencyName");
                                        ExcelData3.Columns.Add("TestResult");

                                        if (ExcelData3.Rows.Count > 0)
                                        {
                                            foreach (DataRow item in ExcelData3.Rows)
                                            {
                                                item["FrequencyName"] = GetFrequencyName(Convert.ToInt32(item["Frequency"].ToString()));
                                                item["TestResult"] = GetTestResult(item["TOD"].ToString(), item["TOE"].ToString(), item["AuditStatusID"].ToString());
                                            }
                                            ExcelData3.Columns.Remove("Frequency");
                                            ExcelData3.Columns.Remove("TOE");
                                            ExcelData3.Columns.Remove("TOD");
                                            ExcelData3.Columns.Remove("AuditStatusID");


                                            exWorkSheet3.Cells["A1"].Value = "Report Generated On :";
                                            exWorkSheet3.Cells["A1"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["A1"].Style.Font.Size = 12;

                                            exWorkSheet3.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                                            exWorkSheet3.Cells["B1"].Style.Font.Size = 12;

                                            exWorkSheet3.Cells["A2"].Value = customerName;
                                            exWorkSheet3.Cells["A2"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["A2"].Style.Font.Size = 12;

                                            exWorkSheet3.Cells["A3"].Value = "Entity Name :";
                                            exWorkSheet3.Cells["A3"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["A3"].Style.Font.Size = 12;

                                            exWorkSheet3.Cells["B3"].Value = CustomerBranchName;
                                            exWorkSheet3.Cells["B3"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["B3:D3"].Merge = true;

                                            exWorkSheet3.Cells["A4"].Value = "Financial Year :";
                                            exWorkSheet3.Cells["A4"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["A4"].Style.Font.Size = 12;

                                            exWorkSheet3.Cells["B4"].Value = FnancialYear;
                                            exWorkSheet3.Cells["B4"].Style.Font.Size = 12;

                                            exWorkSheet3.Cells["A5"].Value = "Quarter :";
                                            exWorkSheet3.Cells["A5"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["A5"].Style.Font.Size = 12;

                                            exWorkSheet3.Cells["B5"].Value = QuarterName.Trim(',');
                                            exWorkSheet3.Cells["B5"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["B5:D5"].Merge = true;

                                            exWorkSheet3.Cells["A7"].LoadFromDataTable(ExcelData3, true);

                                            exWorkSheet3.Cells["A7"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["A7"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["A7"].Value = "ControlNo";
                                            exWorkSheet3.Cells["A7"].AutoFitColumns(20);

                                            exWorkSheet3.Cells["B7"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["B7"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["B7"].Value = "Process";
                                            exWorkSheet3.Cells["B7"].AutoFitColumns(30);

                                            exWorkSheet3.Cells["C7"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["C7"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["C7"].Value = "SubProcess";
                                            exWorkSheet3.Cells["C7"].AutoFitColumns(30);

                                            exWorkSheet3.Cells["D7"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["D7"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["D7"].Value = "Risk Description";
                                            exWorkSheet3.Cells["D7"].AutoFitColumns(30);

                                            exWorkSheet3.Cells["E7"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["E7"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["E7"].Value = "Control Description";
                                            exWorkSheet3.Cells["E7"].AutoFitColumns(30);

                                            exWorkSheet3.Cells["F7"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["F7"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["F7"].Value = "Key / Non Key";
                                            exWorkSheet3.Cells["F7"].AutoFitColumns(30);

                                            exWorkSheet3.Cells["G7"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["G7"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["G7"].Value = "Frequency";
                                            exWorkSheet3.Cells["G7"].AutoFitColumns(30);

                                            exWorkSheet3.Cells["H7"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["H7"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["H7"].Value = "Test Result";
                                            exWorkSheet3.Cells["H7"].AutoFitColumns(30);

                                            exWorkSheet3.Cells["I7"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["I7"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["I7"].Value = "SampleTested";
                                            exWorkSheet3.Cells["I7"].AutoFitColumns(30);

                                            exWorkSheet3.Cells["J7"].Style.Font.Bold = true;
                                            exWorkSheet3.Cells["J7"].Style.Font.Size = 12;
                                            exWorkSheet3.Cells["J7"].Value = "Deviation";
                                            exWorkSheet3.Cells["J7"].AutoFitColumns(30);

                                            using (ExcelRange col = exWorkSheet3.Cells[7, 1, 7 + ExcelData3.Rows.Count, 10])
                                            {
                                                col.Style.Numberformat.Format = "dd-MMM-yyyy";
                                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                                col.Style.WrapText = true;
                                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                            }
                                        }
                                    }
                                    #endregion

                                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                                    Response.ClearContent();
                                    Response.Buffer = true;
                                    Response.AddHeader("content-disposition", "attachment;filename=TestingStatusReport.xlsx");
                                    Response.Charset = "";
                                    Response.ContentType = "application/vnd.ms-excel";
                                    StringWriter sw = new StringWriter();
                                    Response.BinaryWrite(fileBytes);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }//Branch End
                            }// Using End
                        }//Quarter End     
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Quarter";
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindFnancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
            ddlFilterFinancial.Items.Insert(0, new ListItem(" Select Financial Year ", "-1"));
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }

        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        public static DataTable GetICFRAuditStatus(List<AuditInstanceTransactionView> MasterRecords, long UserID, long CustomerId)
        {
            //List<long?> TODIds = new List<long?>();
            //TODIds.Add(-1);
            //TODIds.Add(1);
            //TODIds.Add(2);
            //TODIds.Add(3);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();

                var transactionsQuery = (from row in MasterRecords
                                             //      where row.UserID== UserID
                                         select row).ToList();

                //transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                //long Keycount;
                //long NonKeycount;
                //long totalcount;
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("NoOfControlKey", typeof(long));
                table.Columns.Add("NoOfControlNonKey", typeof(long));

                table.Columns.Add("TestResultPassKey", typeof(long));
                table.Columns.Add("TestResultPassNonKey", typeof(long));

                table.Columns.Add("TestResultFailKey", typeof(long));
                table.Columns.Add("TestResultFailNonKey", typeof(long));
                table.Columns.Add("NotTestedcount", typeof(long));

                table.Columns.Add("Total", typeof(long));


                //  var ProcessList = ProcessManagement.GetAllNew(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var ProcessList = ProcessManagement.GetAllNew(CustomerId);
                long totalcount = 0;

                long NoOfControlKeyNonKeycount = 0;
                long NoOfControlKey = 0;
                long NoOfControlNonKey = 0;

                // long totalPassCount = 0;
                long PassKeyCount = 0;
                long PassNonKeyCount = 0;

                // long totalFailcount = 0;
                long FailKeyCount = 0;
                long FailNonKeyCount = 0;
                long totalNotTestedcount = 0;
                foreach (Mst_Process cc in ProcessList)
                {
                    var NotTestedcount = transactionsQuery.Where(a => a.ProcessId == cc.Id && ((a.TOD == 2 && a.TOE == -1 && a.AuditStatusID == 3) || (a.TOD == null && a.TOE == null))).ToList();
                    //MasterRecords = MasterRecords.Where(a => a.ProcessId == 26 && ((a.TOD == null && a.TOE == null) || (a.TOD == 2 && a.TOE == -1 && a.AuditStatusID == 3))).ToList();
                    totalNotTestedcount = NotTestedcount.Count;
                    NoOfControlKeyNonKeycount = transactionsQuery.Where(a => a.ProcessId == cc.Id).ToList().Count;
                    NoOfControlKey = transactionsQuery.Where(a => a.KeyId == 1 && a.ProcessId == cc.Id).ToList().Count;
                    NoOfControlNonKey = transactionsQuery.Where(a => a.KeyId == 2 && a.ProcessId == cc.Id).ToList().Count;

                    var PassCount = transactionsQuery.Where(a => a.ProcessId == cc.Id && (a.TOD == 1 && a.TOE == 1) || (a.TOD == 1 && a.TOE == 3) || (a.TOD == 3 && a.TOE == 1) || (a.TOD == 3 && a.TOE == 3)).ToList();
                    // totalPassCount = PassCount.Count;
                    PassKeyCount = PassCount.Where(a => a.AuditStatusID == 3 && a.KeyId == 1 && a.ProcessId == cc.Id).ToList().Count;
                    PassNonKeyCount = PassCount.Where(a => a.AuditStatusID == 3 && a.KeyId == 2 && a.ProcessId == cc.Id).ToList().Count;

                    var Failcount = transactionsQuery.Where(a => a.AuditStatusID == 3 && a.ProcessId == cc.Id && (a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2)).ToList();
                    //totalFailcount = Failcount.Count;
                    FailKeyCount = Failcount.Where(a => a.KeyId == 1 && a.ProcessId == cc.Id).ToList().Count;
                    FailNonKeyCount = Failcount.Where(a => a.KeyId == 2 && a.ProcessId == cc.Id).ToList().Count;

                    totalcount = NoOfControlKeyNonKeycount;


                    // Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                    //  NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                    // totalcount = TotalKeyCount + TotalNonKeyCount;

                    if (totalcount != 0)
                        table.Rows.Add(cc.Id, cc.Name, NoOfControlKey, NoOfControlNonKey, PassKeyCount,
                            PassNonKeyCount, FailKeyCount, FailNonKeyCount, totalNotTestedcount, totalcount);
                }
                return table;
            }
        }

        // changed on 15-06-2020
        public List<NameValueHierarchy> GetAllHierarchy(long customerID, List<int> customerbranchid)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             //  && row.ID == customerbranchid
                             && customerbranchid.Contains(row.ID)
                             select row);
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public void LoadSubEntities(long customerid, NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            upComplianceTypeList.Update();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            upComplianceTypeList.Update();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
        }

        public void BindLegalEntityData()
        {
            //if (CustomerId == 0)
            //{
            //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //}
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            //if (CustomerId == 0)
            //{
            //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //}
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (personresponsibleapplicable)
            {
                DRP.DataSource = AuditKickOff_NewDetails.PersonResponsibleFillSubEntityDataICFR(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (roles.Contains(3) || roles.Contains(4))
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityDataICFR(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }
        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlSubEntity4.Items.Count > 0)
                ddlSubEntity4.Items.Clear();

            for (int i = 0; i < ddlQuarter.Items.Count; i++)
            {
                if (ddlQuarter.Items[i].Selected)
                {
                    ddlQuarter.Items[i].Selected = false;
                }
            }

            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    BindLegalEntityData();
                }
                else
                {
                    ddlLegalEntity.DataSource = null;
                    ddlLegalEntity.DataBind();
                    ddlLegalEntity.Items.Clear();

                    ddlSubEntity1.DataSource = null;
                    ddlSubEntity1.DataBind();
                    ddlSubEntity1.Items.Clear();

                    ddlSubEntity2.DataSource = null;
                    ddlSubEntity2.DataBind();
                    ddlSubEntity2.Items.Clear();

                    ddlSubEntity3.DataSource = null;
                    ddlSubEntity3.DataBind();
                    ddlSubEntity3.Items.Clear();

                    ddlSubEntity4.DataSource = null;
                    ddlSubEntity4.DataBind();
                    ddlSubEntity4.Items.Clear();

                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Customer.";

                }
            }

        }

        // added on 24-06-2020 by sagar 
        // Working for Audit Head only.
        //private void BindCustomerList()
        //{
        //    long userId = Portal.Common.AuthenticationHelper.UserID;
        //    int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
        //    int RoleID = 0;
        //    if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
        //    {
        //        RoleID = 2;
        //    }
        //    var customerList = RiskCategoryManagement.GetCustomerListForDDLForMaster(userId, ServiceProviderID, RoleID);
        //    if (customerList.Count > 0)
        //    {
        //        ddlCustomer.DataTextField = "CustomerName";
        //        ddlCustomer.DataValueField = "CustomerId";
        //        ddlCustomer.DataSource = customerList;
        //        ddlCustomer.DataBind();
        //        ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
        //        ddlCustomer.SelectedIndex = 2;
        //    }
        //    else
        //    {
        //        ddlCustomer.DataSource = null;
        //        ddlCustomer.DataBind();
        //    }
        //}

        // added by sagar on 24-06-2020
        // Working for Performer and Reviewer only.
        //private void BindCustomerList()
        //{
        //    long userId = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
        //    var customerList = RiskCategoryManagement.GetAllMappedCustomerForIFCPerformer(userId);
        //    if (customerList.Count > 0)
        //    {
        //        ddlCustomer.DataTextField = "CustomerName";
        //        ddlCustomer.DataValueField = "CustomerId";
        //        ddlCustomer.DataSource = customerList;
        //        ddlCustomer.DataBind();
        //        ddlCustomer.Items.Insert(0, new ListItem("Customer", "-1"));
        //        ddlCustomer.SelectedIndex = 2;
        //    }
        //    else
        //    {
        //        ddlCustomer.DataSource = null;
        //        ddlCustomer.DataBind();
        //    }
        //}


        // added by sagar on 24-06-2020
        /// <summary>
        /// Need to review this method
        /// Working for Audit Head, Performer and Reviewer 
        /// </summary>
        private void BindCustomerList()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDL(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
                ddlCustomer.SelectedIndex = 2;
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();
            }
        }
    }
}