﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Saplin.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AuditStatusUI : System.Web.UI.Page
    {
        int Statusid;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        static bool ReviewerFlag2;
        public static List<long> Branchlist = new List<long>();
        public static List<sp_PerformerReviewView_Result> PerformerReviewViewList = new List<sp_PerformerReviewView_Result>();
        protected void Page_Load(object sender, EventArgs e)
        {
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    if (Request.QueryString["Status"] == "Open")
                    {
                        ViewState["Status"] = 1;
                    }

                    else if (Request.QueryString["Status"] == "Closed")
                    {
                        ViewState["Status"] = 3;
                    }
                }

                BindFinancialYear();
                string financialYearCommaSeparatedList = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    financialYearCommaSeparatedList = Request.QueryString["FY"].ToString();
                }
                if (!string.IsNullOrEmpty(financialYearCommaSeparatedList))
                {
                    List<string> finYearsList = financialYearCommaSeparatedList.Split(',').ToList();
                    if (finYearsList.Count > 0)
                    {
                        for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                        {
                            foreach (string fy in finYearsList)
                            {
                                if (ddlFinancialYearMultiSelect.Items[i].Text == fy)
                                {
                                    ddlFinancialYearMultiSelect.Items[i].Selected = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                    if (FinancialYear != null)
                    {
                        for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                        {
                            if (ddlFinancialYearMultiSelect.Items[i].Text == FinancialYear)
                            {
                                ddlFinancialYearMultiSelect.Items[i].Selected = true;
                            }
                        }
                    }
                }
                BindCustomerList();

                #region comment code
                string customerIdCommaSeparatedList = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerId"]))
                {
                    customerIdCommaSeparatedList = Request.QueryString["CustomerId"].ToString();
                }
                if (!string.IsNullOrEmpty(customerIdCommaSeparatedList))
                {
                    List<string> customerIdList = customerIdCommaSeparatedList.Split(',').ToList();
                    if (customerIdList.Count > 0)
                    {
                        for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                        {
                            foreach (string cId in customerIdList)
                            {
                                if (ddlCustomerMultiSelect.Items[i].Value == cId)
                                {
                                    ddlCustomerMultiSelect.Items[i].Selected = true;
                                }
                            }
                        }
                        Session["CustIdFromQueryString"] = customerIdCommaSeparatedList;
                    }
                }
                #endregion

                BindLegalEntityData();
                BindVertical();
                PerformerReviewViewList = DashboardManagementRisk.sp_GetPerFormerReviewer();
                if (roles.Contains(3))
                {
                    PerformerFlag = true;
                    ShowPerformer(sender, e);
                }
                else if (roles.Contains(4))
                {
                    ReviewerFlag = true;
                    ShowReviewer(sender, e);
                }
                else if (roles.Contains(5))
                {
                    ReviewerFlag2 = true;
                    ShowReviewer2(sender, e);
                }
                else
                {
                    PerformerFlag = true;
                }
                bindPageNumber();
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(List<int> customerIDList, List<int?> customerBranchIdList)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && customerIDList.Contains(row.CustomerID)
                             && customerBranchIdList.Contains(row.ID)
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerIDList, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(List<int> customerIDList, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && customerIDList.Contains(row.CustomerID)
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerIDList, item, false, entities);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdRiskActivityMatrix.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        if (ddlCustomer.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                    }

                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindAuditSchedule(List<string> flagList, List<int?> countList)
        {
            List<string> FlagList = new List<string>();
            try
            {
                foreach (var flag in flagList)
                {
                    if (flag == "A")
                    {
                        FlagList.Add("Annually");
                    }
                    else if (flag == "H")
                    {
                        FlagList.Add("Apr-Sep");
                        FlagList.Add("Oct-Mar");
                    }
                    else if (flag == "Q")
                    {
                        FlagList.Add("Apr-Jun");
                        FlagList.Add("Jul-Sep");
                        FlagList.Add("Oct-Dec");
                        FlagList.Add("Jan-Mar");
                    }
                    else if (flag == "M")
                    {
                        FlagList.Add("Apr");
                        FlagList.Add("May");
                        FlagList.Add("Jun");
                        FlagList.Add("Jul");
                        FlagList.Add("Aug");
                        FlagList.Add("Sep");
                        FlagList.Add("Oct");
                        FlagList.Add("Nov");
                        FlagList.Add("Dec");
                        FlagList.Add("Jan");
                        FlagList.Add("Feb");
                        FlagList.Add("Mar");
                    }
                    else if (flag == "S")
                    {
                        FlagList.Add("Select Period");
                        FlagList.Add("Special Audit");
                    }
                    else
                    {
                        int count = countList.Count;
                        if (count == 1)
                        {
                            FlagList.Add("Phase1");
                        }
                        else if (count == 2)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                        }
                        else if (count == 3)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                        }
                        else if (count == 4)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                            FlagList.Add("Phase4");
                        }
                        else
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                            FlagList.Add("Phase4");
                            FlagList.Add("Phase5");
                        }
                    }
                }
                if (FlagList.Count > 0)
                {
                    int setIndex = 0;
                    ddlPeriodMultiSelect.Items.Clear();
                    foreach (string item in FlagList)
                    {
                        ddlPeriodMultiSelect.Items.Insert(setIndex, item);
                        setIndex = setIndex + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public void BindCustomer()
        {
            List<long> customerIDList = new List<long>();
            for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
            {
                if (ddlCustomerMultiSelect.Items[i].Selected)
                {
                    customerIDList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                }
            }
            ddlFilterLocationMultiSelect.DataTextField = "Name";
            ddlFilterLocationMultiSelect.DataValueField = "ID";
            ddlFilterLocationMultiSelect.Items.Clear();
            ddlFilterLocationMultiSelect.DataSource = UserManagementRisk.FillCustomerNewForMultiSelect(customerIDList);
            ddlFilterLocationMultiSelect.DataBind();
        }

        public void BindFinancialYear()
        {
            ddlFinancialYearMultiSelect.DataTextField = "Name";
            ddlFinancialYearMultiSelect.DataValueField = "ID";
            ddlFinancialYearMultiSelect.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYearMultiSelect.DataBind();
        }

        public void BindLegalEntityData()
        {
            List<int?> CustomerIdList = new List<int?>();
            for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
            {
                if (ddlCustomerMultiSelect.Items[i].Selected)
                {
                    CustomerIdList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                }
            }

            ddlLegalEntityMultiSelect.DataTextField = "Name";
            ddlLegalEntityMultiSelect.DataValueField = "ID";
            ddlLegalEntityMultiSelect.Items.Clear();
            ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataForDashBoard(CustomerIdList);
            ddlLegalEntityMultiSelect.DataBind();
        }

        public void BindSubEntityData(DropDownCheckBoxes DRP, List<int?> ParentIdList)
        {
            List<int?> CustomerIdList = new List<int?>();
            for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
            {
                if (ddlCustomerMultiSelect.Items[i].Selected)
                {
                    CustomerIdList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                }
            }

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityDataForPerformerMultiSelect(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CustomerIdList, ParentIdList);
            DRP.DataBind();
        }

        public void BindSchedulingType(List<int> branchIdList)
        {
            ddlSchedulingTypeMultiSelect.DataTextField = "Name";
            ddlSchedulingTypeMultiSelect.DataValueField = "ID";
            ddlSchedulingTypeMultiSelect.DataSource = UserManagementRisk.FillSchedulingTypeForAuditHeadDashbaord(branchIdList);
            ddlSchedulingTypeMultiSelect.DataBind();
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    //BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    // BindSchedulingType(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }

            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    // BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    // BindSchedulingType(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }

            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    // BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    //BindSchedulingType(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.ClearSelection();

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    // BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    // BindSchedulingType(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                    {
                        // BindSchedulingType(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        BindVertical();
                        BindData("P");
                        bindPageNumber();
                        //GetPageDisplaySummary();
                    }
                }
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                    {
                        BindVertical();
                        BindData("N");
                        bindPageNumber();
                        //GetPageDisplaySummary();
                    }
                }
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N");

            // GetPageDisplaySummary();
            bindPageNumber();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            //{
            //if (ddlSchedulingType.SelectedItem.Text == "Annually")
            //{
            //    BindAuditSchedule("A", 0);
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            //{
            //    BindAuditSchedule("H", 0);
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            //{
            //    BindAuditSchedule("Q", 0);
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            //{
            //    BindAuditSchedule("M", 0);
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            //{
            //    BindAuditSchedule("S", 0);
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            //{
            //    //BindAuditSchedule("P", 5);

            //    if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            //    {
            //        if (ddlFilterLocation.SelectedValue != "-1")
            //        {
            //            int count = 0;
            //            count = UserManagementRisk.GetPhaseCount(Convert.ToInt32(ddlFilterLocation.SelectedValue));
            //            BindAuditSchedule("P", count);
            //        }
            //    }
            //}
            //}
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N");
            bindPageNumber();
        }

        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindData("P");
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindData("N");
            }
        }
        public string ShowRating(string RiskRating)
        {
            string processnonprocess = "";
            if (RiskRating == "1")
            {
                processnonprocess = "High";
            }
            else if (RiskRating == "2")
            {
                processnonprocess = "Medium";
            }
            else if (RiskRating == "3")
            {
                processnonprocess = "Low";
            }
            return processnonprocess.Trim(',');
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            liReviewer2.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
            ReviewerFlag2 = false;
            BindData("P");
        }

        protected void ShowReviewer2(object sender, EventArgs e)
        {
            liReviewer2.Attributes.Add("class", "active");
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag2 = true;
            ReviewerFlag = false;
            PerformerFlag = false;
            BindData("P");
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            liReviewer2.Attributes.Add("class", "");
            ReviewerFlag = false;
            ReviewerFlag2 = false;
            PerformerFlag = true;
            BindData("P");
        }

        private void BindData(string Flag)
        {
            try
            {
                int roleid = -1;
                List<string> FinancialYearList = new List<string>();
                List<string> PeriodList = new List<string>();
                List<long> VerticalIdList = new List<long>();
                List<int> CustomerIdList = new List<int>();
                List<int?> CustomerIdList_1 = new List<int?>();

                for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                {
                    if (ddlCustomerMultiSelect.Items[i].Selected)
                    {
                        CustomerIdList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                        CustomerIdList_1.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                    }
                }
                if (CustomerIdList.Count == 0)
                {
                    CustomerIdList.Add(-1);
                }

                bool clearLegalBranchList = false;
                bool clearSubEntity1List = false;
                bool clearSubEntity2List = false;
                bool clearSubEntity3List = false;
                bool clearFilterLocation = false;
                List<int?> CHKBranchlist = new List<int?>();
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity1MultiSelect.Items[i].Selected)
                    {
                        if (clearLegalBranchList && CHKBranchlist.Count > 0)
                        {
                            CHKBranchlist.Clear();
                            clearLegalBranchList = false;
                        }
                        clearSubEntity1List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity2MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity1List && CHKBranchlist.Count > 0)
                        {
                            CHKBranchlist.Clear();
                            clearSubEntity1List = false;
                        }
                        clearSubEntity2List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity3MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity2List && CHKBranchlist.Count > 0)
                        {
                            CHKBranchlist.Clear();
                            clearSubEntity2List = false;
                        }
                        clearSubEntity3List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                {

                    if (ddlFilterLocationMultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity3List && CHKBranchlist.Count > 0)
                        {
                            CHKBranchlist.Clear();
                            clearSubEntity3List = false;
                        }
                        clearFilterLocation = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                {
                    if (ddlFinancialYearMultiSelect.Items[i].Selected)
                    {
                        FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                    }
                }

                for (int i = 0; i < ddlPeriodMultiSelect.Items.Count; i++)
                {
                    if (ddlPeriodMultiSelect.Items[i].Selected)
                    {
                        PeriodList.Add(ddlPeriodMultiSelect.Items[i].Text);
                    }
                }

                if (!String.IsNullOrEmpty(ViewState["Status"].ToString()))
                {
                    Statusid = Convert.ToInt32(ViewState["Status"]);
                }

                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    List<int> vIdList = UserManagementRisk.VerticalgetBycustomeridListNewVersion(CustomerIdList);
                    if (vIdList.Count > 0)
                    {
                        foreach (var item in vIdList)
                        {
                            if (item != -1)
                            {
                                VerticalIdList.Add(Convert.ToInt32(item));
                            }
                        }
                    }
                }

                if (FinancialYearList.Count == 0)
                    FinancialYearList.Add(GetCurrentFinancialYear(DateTime.Now.Date));

                if (PerformerFlag)
                    roleid = 3;

                else if (ReviewerFlag)
                    roleid = 4;
                else if (ReviewerFlag2)
                    roleid = 5;

                Branchlist.Clear();
                GetAllHierarchy(CustomerIdList, CHKBranchlist);
                Branchlist.ToList();

                List<int?> BranchList_int = new List<int?>();
                if (Branchlist.Count > 0)
                {
                    foreach (var bID in Branchlist)
                    {
                        if (bID != -1)
                        {
                            BranchList_int.Add(Convert.ToInt32(bID));
                        }
                    }
                }
                if (Flag == "P")
                {
                    var AuditLists = DashboardManagementRisk.GetAuditsForMultiSelect(FinancialYearList, PeriodList, CustomerIdList_1, BranchList_int, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalIdList, roleid);
                    grdRiskActivityMatrix.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdRiskActivityMatrix.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }
                else
                {
                    var AuditLists = DashboardManagementRisk.GetAuditsForMultiSelect(FinancialYearList, PeriodList, CustomerIdList_1, BranchList_int, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalIdList, roleid);
                    grdRiskActivityMatrix.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdRiskActivityMatrix.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        protected bool CheckEnable(String Role, String SDate, String EDate)
        {
            if (Role != "Performer")
                return false;
            else if ((SDate == "" || EDate == "") && Role == "Performer")
                return true;
            else if (SDate != "" && EDate != "")
                return false;
            else
                return true;
        }

        protected bool CheckActionButtonEnable(String Role, String SDate, String EDate)
        {
            if ((SDate == "" || EDate == "") && Role == "Performer")
                return false;
            else if (SDate != "" && EDate != "")
                return true;
            else
                return true;
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void upAuditSummaryDetails_Load(object sender, EventArgs e)
        {

        }

        protected void upPromotor_Load(object sender, EventArgs e)
        {

        }
        protected void grdRiskActivityMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("SaveDate"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        long AuditID = Convert.ToInt32(commandArgs[6]);
                        GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        int RowIndex = gvr.RowIndex;
                        TextBox txtStartDate = (TextBox)grdRiskActivityMatrix.Rows[RowIndex].FindControl("txtStartDate");
                        TextBox txtEndDate = (TextBox)grdRiskActivityMatrix.Rows[RowIndex].FindControl("txtEndDate");

                        if (txtStartDate.Text != "" && txtEndDate.Text != "")
                        {
                            try
                            {
                                Convert.ToDateTime(GetDate(txtStartDate.Text));
                                Convert.ToDateTime(GetDate(txtEndDate.Text));
                            }
                            catch (Exception ex)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Provide Valid Audit Start Date and End Date.";
                                return;
                            }

                            if (Convert.ToDateTime(GetDate(txtStartDate.Text)) <= Convert.ToDateTime(GetDate(txtEndDate.Text)))
                            {

                                UserManagementRisk.UpdateInternalControlAuditAssignment(Convert.ToDateTime(GetDate(txtStartDate.Text)), Convert.ToDateTime(GetDate(txtEndDate.Text)), AuditID);

                                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                                    BindData("P");
                                else
                                    BindData("N");
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Audit Start Date should be less or equal to end date.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Provide Start Date and End Date";
                        }
                    }
                }
                else if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                        string ForMonth = Convert.ToString(commandArgs[1]);
                        string FinancialYear = Convert.ToString(commandArgs[2]);

                        int RoleID = Convert.ToInt32(commandArgs[3]);
                        long AuditID = Convert.ToInt32(commandArgs[6]);
                        int customerID = Convert.ToInt32(commandArgs[7]); ;

                        if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                        {
                            Response.Redirect("~/RiskManagement/InternalAuditTool/AuditStatusSummary.aspx?Role=" + RoleID + "&AuditID=" + AuditID + "&FY=" + FinancialYear + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString() + "&CustomerId=" + customerID, false);
                        }
                        else
                        {
                            Response.Redirect("~/RiskManagement/InternalAuditTool/AuditStatusSummary.aspx?Role=" + RoleID + "&AuditID=" + AuditID + "&FY=" + FinancialYear + "&ReturnUrl1=" + "&CustomerId=" + customerID, false);
                        }
                    }
                }
                else if (e.CommandName.Equals("ViewprerequsiteSummary"))
                {
                    int customerID = -1;
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        if (ddlCustomer.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                            return;
                        }
                    }
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        int RoleID = Convert.ToInt32(commandArgs[0]);
                        int UserID = Convert.ToInt32(commandArgs[1]);
                        long AuditID = Convert.ToInt32(commandArgs[2]);
                        Session["CustomerID"] = customerID;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + RoleID + "," + UserID + "," + AuditID + "," + customerID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData("P");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdRiskActivityMatrix_RowBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label AuditId = (Label)e.Row.FindControl("AuditId");
                Label lblProcess = (Label)e.Row.FindControl("lblProcess");
                Label lblPerformer = (Label)e.Row.FindControl("lblPerformer");
                long cbranchId = Convert.ToInt64(AuditId.Text);

                int roleid = 3;
                List<sp_PerformerReviewView_Result> Rowss = (from C in PerformerReviewViewList
                                                             where C.AuditID == cbranchId
                                                             select C).Distinct().ToList();

                var ProcessName = (from C in Rowss
                                   where C.Roleid == roleid
                                   select C.ProcessName).Distinct().ToList();

                string Process = "";
                foreach (var item in ProcessName)
                {
                    Process += item + ",";
                }
                lblProcess.Text = Process.TrimEnd(',');
                lblProcess.Attributes.Add("tooltip", lblProcess.Text);

            }

        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                //Reload the Grid
                BindData("P");
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskActivityMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        private void BindCustomerList()
        {
            long userId = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            var customerList = RiskCategoryManagement.GetAllMappedCustomerForPerformer(userId);
            if (customerList.Count > 0)
            {
                ddlCustomerMultiSelect.DataTextField = "CustomerName";
                ddlCustomerMultiSelect.DataValueField = "CustomerId";
                ddlCustomerMultiSelect.DataSource = customerList;
                ddlCustomerMultiSelect.DataBind();
            }
            else
            {
                ddlCustomerMultiSelect.DataSource = null;
                ddlCustomerMultiSelect.DataBind();
            }


        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    BindLegalEntityData();
                    BindVertical();
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        BindData("P");
                    }
                    else
                    {
                        BindData("N");
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                }
            }
        }

        protected void ddlCustomerMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<int?> customerIdsList = new List<int?>();
            customerIdsList.Clear();
            for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
            {
                if (ddlCustomerMultiSelect.Items[i].Selected)
                {
                    customerIdsList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                }
            }
            if (customerIdsList.Count > 0)
            {
                BindLegalEntityData();
                BindVertical();
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindData("P");
                }
                else
                {
                    BindData("N");
                }
            }
            else
            {
                Session["CustIdFromQueryString"] = 0;
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
            }
        }

        protected void ddlLegalEntityMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1MultiSelect.Items.Count > 0)
            {
                ddlSubEntity1MultiSelect.Items.Clear();
            }
            if (ddlSubEntity2MultiSelect.Items.Count > 0)
            {
                ddlSubEntity2MultiSelect.Items.Clear();
            }
            if (ddlSubEntity3MultiSelect.Items.Count > 0)
            {
                ddlSubEntity3MultiSelect.Items.Clear();
            }
            if (ddlFilterLocationMultiSelect.Items.Count > 0)
            {
                ddlFilterLocationMultiSelect.Items.Clear();

            }

            List<int?> branchListIds = new List<int?>();
            List<int> branchListIds_1 = new List<int>();

            for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
            {
                if (ddlLegalEntityMultiSelect.Items[i].Selected)
                {
                    branchListIds.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    branchListIds_1.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                }
            }

            if (branchListIds.Count > 0)
            {
                BindSubEntityData(ddlSubEntity1MultiSelect, branchListIds);
            }

            if (branchListIds_1.Count > 0)
            {
                BindSchedulingType(branchListIds_1);
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }

            bindPageNumber();
        }

        protected void ddlSubEntity1MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2MultiSelect.Items.Count > 0)
                ddlSubEntity2MultiSelect.Items.Clear();

            if (ddlSubEntity3MultiSelect.Items.Count > 0)
                ddlSubEntity3MultiSelect.Items.Clear();

            if (ddlFilterLocationMultiSelect.Items.Count > 0)
                ddlFilterLocationMultiSelect.Items.Clear();

            List<int?> branchListIds = new List<int?>();
            List<int> branchListIds_1 = new List<int>();

            for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity1MultiSelect.Items[i].Selected)
                {
                    branchListIds.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                    branchListIds_1.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                }
            }

            if (branchListIds.Count > 0)
            {
                BindSubEntityData(ddlSubEntity2MultiSelect, branchListIds);
            }

            if (branchListIds_1.Count > 0)
            {
                BindSchedulingType(branchListIds_1);
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }

            bindPageNumber();
        }

        protected void ddlSubEntity2MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3MultiSelect.Items.Count > 0)
                ddlSubEntity3MultiSelect.Items.Clear();

            if (ddlFilterLocationMultiSelect.Items.Count > 0)
                ddlFilterLocationMultiSelect.Items.Clear();

            List<int?> branchListIds = new List<int?>();
            List<int> branchListIds_1 = new List<int>();

            for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity2MultiSelect.Items[i].Selected)
                {
                    branchListIds.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                    branchListIds_1.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                }
            }

            if (branchListIds.Count > 0)
            {
                BindSubEntityData(ddlSubEntity3MultiSelect, branchListIds);
            }

            if (branchListIds_1.Count > 0)
            {
                BindSchedulingType(branchListIds_1);
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
        }

        protected void ddlSubEntity3MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocationMultiSelect.Items.Count > 0)
                ddlFilterLocationMultiSelect.ClearSelection();

            List<int?> branchListIds = new List<int?>();
            List<int> branchListIds_1 = new List<int>();

            for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity3MultiSelect.Items[i].Selected)
                {
                    branchListIds.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                    branchListIds_1.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                }
            }

            if (branchListIds.Count > 0)
            {
                BindSubEntityData(ddlFilterLocationMultiSelect, branchListIds);
            }

            if (branchListIds_1.Count > 0)
            {
                BindSchedulingType(branchListIds_1);
            }

            BindVertical();

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
        }

        protected void ddlFilterLocationMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<int> branchIdsList = new List<int>();
            for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
            {
                if (ddlFilterLocationMultiSelect.Items[i].Selected)
                {
                    branchIdsList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                }
            }
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                if (branchIdsList.Count > 0)
                {
                    BindSchedulingType(branchIdsList);
                    BindVertical();
                    BindData("P");
                    bindPageNumber();
                }
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();

                if (branchIdsList.Count > 0)
                {
                    BindVertical();
                    BindData("N");
                    bindPageNumber();
                }
            }
        }

        protected void ddlFinancialYearMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N");

            bindPageNumber();
        }

        protected void ddlSchedulingTypeMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> FlagList = new List<string>();
            List<int> CHKBranchlist = new List<int>();
            for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
            {
                if (ddlSchedulingTypeMultiSelect.Items[i].Selected)
                {
                    if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Annually")
                    {
                        FlagList.Add("A");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Half Yearly")
                    {
                        FlagList.Add("H");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Quarterly")
                    {
                        FlagList.Add("Q");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Monthly")
                    {
                        FlagList.Add("M");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Special Audit")
                    {
                        FlagList.Add("S");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Phase")
                    {
                        bool clearLegalBranchList = false;
                        bool clearSubEntity1List = false;
                        bool clearSubEntity2List = false;
                        bool clearSubEntity3List = false;
                        bool clearFilterLocation = false;

                        CHKBranchlist.Clear();
                        for (int j = 0; j < ddlLegalEntityMultiSelect.Items.Count; j++)
                        {
                            if (ddlLegalEntityMultiSelect.Items[j].Selected)
                            {
                                clearLegalBranchList = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[j].Value));
                            }
                        }

                        for (int k = 0; k < ddlSubEntity1MultiSelect.Items.Count; k++)
                        {
                            if (ddlSubEntity1MultiSelect.Items[k].Selected)
                            {
                                if (clearLegalBranchList && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearLegalBranchList = false;
                                }
                                clearSubEntity1List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[k].Value));
                            }

                        }

                        for (int l = 0; l < ddlSubEntity2MultiSelect.Items.Count; l++)
                        {
                            if (ddlSubEntity2MultiSelect.Items[l].Selected)
                            {
                                if (clearSubEntity1List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity1List = false;
                                }
                                clearSubEntity2List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[l].Value));
                            }
                        }

                        for (int m = 0; m < ddlSubEntity3.Items.Count; m++)
                        {
                            if (ddlSubEntity3MultiSelect.Items[m].Selected)
                            {
                                if (clearSubEntity2List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity2List = false;
                                }
                                clearSubEntity3List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[m].Value));
                            }
                        }
                    }
                    List<int?> recordCount = new List<int?>(); ;
                    recordCount = UserManagementRisk.GetPhaseCountForAuditHeadDashboard(CHKBranchlist);
                    if (recordCount.Count > 0)
                    {
                        FlagList.Add("P");
                    }
                    BindAuditSchedule(FlagList, recordCount);
                }
            }
        }

        protected void ddlPeriodMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N");
            bindPageNumber();
        }
    }
}