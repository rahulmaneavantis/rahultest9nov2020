﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class PrerequsiteAuditReport : System.Web.UI.Page
    {
             
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {                
                BindFinancialYear();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                BindLegalEntityData();
                if (ddlLegalEntity.Items.Count>1)
                {                    
                    ddlLegalEntity.ClearSelection();
                    ddlLegalEntity.SelectedValue = Convert.ToString(ddlLegalEntity.Items[1].Value);
                }                
                BindData();
                bindPageNumber();
            }
        }
        
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdRiskActivityMatrix.PageIndex = chkSelectedPage - 1;
            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindData();
        }
        
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();            
        }

        public void BindLegalEntityData()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                string FinYear = string.Empty;
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                var query = (from row in entities.SP_FillAuditName(Portal.Common.AuthenticationHelper.UserID)
                             select row);
                if (!string.IsNullOrEmpty(FinYear))
                {
                    query = query.Where(entry => entry.FinancialYear == FinYear);
                }
              
                var obj = (from row in query
                                 select new { ID = row.AuditID, Name = row.AuditName }).OrderBy(entry => entry.Name).ToList<object>();
                
                ddlLegalEntity.DataTextField = "Name";
                ddlLegalEntity.DataValueField = "ID";
                ddlLegalEntity.Items.Clear();
                ddlLegalEntity.DataSource = obj;
                ddlLegalEntity.DataBind();
                //ddlLegalEntity.Items.Insert(0, new ListItem("Select Audit", "-1"));
            }
        }            
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
        }
       
        private void BindData()
        {
            try
            {                
                int auditID = -1;
                string FinYear = String.Empty;               
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        auditID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }               
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (FinYear == "")
                    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                
                var AuditLists = GetAudits(FinYear, auditID);
                grdRiskActivityMatrix.DataSource = AuditLists;
                Session["TotalRows"] = AuditLists.Count;
                grdRiskActivityMatrix.DataBind();
                AuditLists.Clear();
                AuditLists = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<SP_PrequsiteReportData_Result> GetAudits(string FinYear, long auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SP_PrequsiteReportData_Result> Masterrecord = new List<SP_PrequsiteReportData_Result>();
               
                    entities.Database.CommandTimeout = 300;
                     Masterrecord = (from row in entities.SP_PrequsiteReportData(auditID, Portal.Common.AuthenticationHelper.UserID)
                                        select row).ToList();
                                           
                if (FinYear != "")
                    Masterrecord = Masterrecord.Where(Entry => Entry.FinancialYear == FinYear).ToList();               
                return Masterrecord;
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;
            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }
            return FinYear;
        }
        protected bool CheckEnable(String Role, String SDate, String EDate)
        {
            if (Role != "Performer")
                return false;
            else if ((SDate == "" || EDate == "") && Role == "Performer")
                return true;
            else if (SDate != "" && EDate != "")
                return false;
            else
                return true;
        }
        protected bool CheckActionButtonEnable(String Role, String SDate, String EDate)
        {
            if ((SDate == "" || EDate == "") && Role == "Performer")
                return false;
            else if (SDate != "" && EDate != "")
                return true;
            else
                return true;
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void upAuditSummaryDetails_Load(object sender, EventArgs e)
        {
        }
        protected void upPromotor_Load(object sender, EventArgs e)
        {
        }       
        protected void grdRiskActivityMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
               
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                BindData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskActivityMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
      
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
    }
}