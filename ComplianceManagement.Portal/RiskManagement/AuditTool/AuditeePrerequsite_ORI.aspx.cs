﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AuditeePrerequsite_ORI : System.Web.UI.Page
    {
             
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {

                if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrlPR"]))
                {
                    if (((Request.QueryString["ReturnUrlPR"]).ToString()) == "Status@Open")
                    {
                        ViewState["Status"] = "Open";
                    }
                    else if (Request.QueryString["ReturnUrlPR"] == "Status@Submitted")
                    {
                        ViewState["Status"] = "Submitted";
                    }
                }
                
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();
                BindProcess(customerID);
                if (ddlLegalEntity.Items.Count > 1)
                {                    
                    if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                    {
                        ddlLegalEntity.ClearSelection();
                        int auditid = Convert.ToInt32(Request.QueryString["AuditID"]);
                        ddlLegalEntity.SelectedValue = Convert.ToString(auditid);                        
                    }                   
                }
                BindData();
                bindPageNumber();
            }
        }
        public static object FillProcessDropdownPerformerAndReviewer(int Customerid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Mst_Process> query = new List<Mst_Process>();
                query = (from row in entities.InternalControlAuditAssignments
                         join row1 in entities.Mst_Process
                         on row.ProcessId equals row1.Id
                         where row1.CustomerID == Customerid
                         && row1.IsDeleted == false
                         && row.AuditID == AuditID
                         select row1).Distinct().ToList();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillSubProcess(long Processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Subprocess
                             where row.IsDeleted == false && row.ProcessId == Processid
                             select row);
                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        private void BindProcess(int CustomerID)
        {
            try
            {
                int auditID = -1;
                if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    auditID = Convert.ToInt32(Request.QueryString["AuditID"]);

                    var data = FillProcessDropdownPerformerAndReviewer(CustomerID, auditID);

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();

                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";

                    ddlProcess.DataSource = data;
                    ddlProcess.DataBind();

                  
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                   

                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue));
                    }
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcess(long Processid)
        {
            try
            {
                var data = FillSubProcess(Processid);
                ddlSubProcess.Items.Clear();
                ddlSubProcess.DataTextField = "Name";
                ddlSubProcess.DataValueField = "Id";
                ddlSubProcess.DataSource = data;
                ddlSubProcess.DataBind();
                ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
                }
            }
            else
            {
                ddlSubProcess.Items.Clear();
                ddlSubProcess.Items.Insert(0, new ListItem("Select Sub Process", "-1"));
            }
            BindData();
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Resetbuttonminisize();", true);
        }
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Resetbuttonminisize();", true);
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected bool CheckActionButtonEnable(long AuditID, long ATBDID, long ChecklistID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.AuditID== AuditID && row.ATBDId== ATBDID
                             && row.checklistId== ChecklistID
                             && row.Status!=4
                               && row.IsDeleted == false
                             select row).ToList();
                if (query.Count > 0)
                {
                    return true;
                }
                else {
                    return false;
                }                
            }
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            string url = "";
            if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrlPR"]))
            {
                url = Request.QueryString["ReturnUrlPR"];
                Response.Redirect("~/RiskManagement/AuditTool/PreRequsiteAuditStatusUI.aspx?" + url.Replace("@", "="));
            }
            else
            {
                Response.Redirect("~/RiskManagement/AuditTool/PreRequsiteAuditStatusUI.aspx?status=open");
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdRiskActivityMatrix.PageIndex = chkSelectedPage - 1;
            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindData();
        }
        
        //public void BindFinancialYear()
        //{
        //    ddlFinancialYear.DataTextField = "Name";
        //    ddlFinancialYear.DataValueField = "ID";
        //    ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
        //    ddlFinancialYear.DataBind();            
        //}

        public void BindLegalEntityData()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //string FinYear = string.Empty;
                //if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                //{
                //    if (ddlFinancialYear.SelectedValue != "-1")
                //    {
                //        FinYear = ddlFinancialYear.SelectedItem.Text;
                //    }
                //}
                var query = (from row in entities.SP_FillAuditName(Portal.Common.AuthenticationHelper.UserID)
                             select row);
                //if (!string.IsNullOrEmpty(FinYear))
                //{
                //    query = query.Where(entry => entry.FinancialYear == FinYear);
                //}
              
                var obj = (from row in query
                           select new { ID = row.AuditID, Name = row.AuditName }).OrderBy(entry => entry.Name).ToList<object>().Distinct();
                
                ddlLegalEntity.DataTextField = "Name";
                ddlLegalEntity.DataValueField = "ID";
                ddlLegalEntity.Items.Clear();
                ddlLegalEntity.DataSource = obj;
                ddlLegalEntity.DataBind();
                //ddlLegalEntity.Items.Insert(0, new ListItem("Select Audit", "-1"));
            }
        }            
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
        }
       
        private void BindData()
        {
            try
            {                
                int auditID = -1;
                string FinYear = String.Empty;
                string Status = string.Empty;
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        auditID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ViewState["Status"].ToString()))
                {
                    Status = Convert.ToString(ViewState["Status"]);
                }
                
                //if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                //{
                //    if (ddlFinancialYear.SelectedValue != "-1")
                //    {
                //        FinYear = ddlFinancialYear.SelectedItem.Text;
                //    }
                //}
                //if (FinYear == "")
                //    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                
                var AuditLists = GetAudits(FinYear, auditID, Status);
                grdRiskActivityMatrix.DataSource = AuditLists;
                Session["TotalRows"] = AuditLists.Count;
                grdRiskActivityMatrix.DataBind();
                AuditLists.Clear();
                AuditLists = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<SP_FillPrequsiteUploadGridData_Result> GetAudits(string FinYear, long auditID, string status)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SP_FillPrequsiteUploadGridData_Result> Masterrecord = new List<SP_FillPrequsiteUploadGridData_Result>();
                if (status == "Open")
                {
                    entities.Database.CommandTimeout = 300;
                     Masterrecord = (from row in entities.SP_FillPrequsiteUploadGridData(auditID)
                                        select row).ToList();

                    Masterrecord= Masterrecord.Where(entry=>entry.DocStatus== status).ToList();

                }
                else if (status == "Submitted")
                {
                    entities.Database.CommandTimeout = 300;                   
                     Masterrecord = (from row in entities.SP_FillPrequsiteUploadGridData(auditID)
                                        select row).ToList();

                    Masterrecord = Masterrecord.Where(entry => entry.DocStatus == status).ToList();
                }              
                if (FinYear != "")
                    Masterrecord = Masterrecord.Where(Entry => Entry.FinancialYear == FinYear).ToList();               
                return Masterrecord;
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;
            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }
            return FinYear;
        }
        protected bool CheckEnable(String Role, String SDate, String EDate)
        {
            if (Role != "Performer")
                return false;
            else if ((SDate == "" || EDate == "") && Role == "Performer")
                return true;
            else if (SDate != "" && EDate != "")
                return false;
            else
                return true;
        }
        protected bool CheckActionButtonEnable(String Role, String SDate, String EDate)
        {
            if ((SDate == "" || EDate == "") && Role == "Performer")
                return false;
            else if (SDate != "" && EDate != "")
                return true;
            else
                return true;
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void upAuditSummaryDetails_Load(object sender, EventArgs e)
        {
        }
        protected void upPromotor_Load(object sender, EventArgs e)
        {
        }
        public static List<PrerequisiteTran_Upload> GetFileDataDraftClosureDocument(long AuditID,long ChecklistID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.PrerequisiteTran_Upload
                                where row.AuditID == AuditID
                                && row.checklistId== ChecklistID
                                && row.Status!=4
                                && row.IsDeleted == false
                                select row).ToList();
                return fileData;
            }
        }
        protected void grdRiskActivityMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("SaveDate"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        long AuditID = Convert.ToInt32(commandArgs[6]);
                        GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        int RowIndex = gvr.RowIndex;
                        TextBox txtStartDate = (TextBox)grdRiskActivityMatrix.Rows[RowIndex].FindControl("txtStartDate");
                        TextBox txtEndDate = (TextBox)grdRiskActivityMatrix.Rows[RowIndex].FindControl("txtEndDate");

                        if (txtStartDate.Text != "" && txtEndDate.Text != "")
                        {
                            try
                            {
                                Convert.ToDateTime(GetDate(txtStartDate.Text));
                                Convert.ToDateTime(GetDate(txtEndDate.Text));
                            }
                            catch (Exception ex)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Provide Valid Audit Start Date and End Date.";
                                return;
                            }
                            if (Convert.ToDateTime(GetDate(txtStartDate.Text)) <= Convert.ToDateTime(GetDate(txtEndDate.Text)))
                            {
                                UserManagementRisk.UpdateInternalControlAuditAssignment(Convert.ToDateTime(GetDate(txtStartDate.Text)), Convert.ToDateTime(GetDate(txtEndDate.Text)), AuditID);
                                BindData();
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Audit Start Date should be less or equal to end date.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Provide Start Date and End Date";
                        }
                    }
                }
                else if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        int RoleID = Convert.ToInt32(commandArgs[3]);
                        long AuditID = Convert.ToInt32(commandArgs[6]);
                        if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                        {
                            Response.Redirect("~/RiskManagement/InternalAuditTool/AuditStatusSummary.aspx?Role=" + RoleID + "&AuditID=" + AuditID + "&ReturnUrlPR=Status@" + Request.QueryString["Status"].ToString(), false);
                        }
                        else
                        {
                            Response.Redirect("~/RiskManagement/InternalAuditTool/AuditStatusSummary.aspx?Role=" + RoleID + "&AuditID=" + AuditID + "&ReturnUrlPR=", false);
                        }
                    }
                }
                else if (e.CommandName.Equals("ViewprerequsiteSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        int RoleID = Convert.ToInt32(commandArgs[0]);
                        int UserID = Convert.ToInt32(commandArgs[1]);
                        long AuditID = Convert.ToInt32(commandArgs[2]);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + RoleID + "," + UserID + "," + AuditID + ");", true);
                    }
                }
                else if (e.CommandName.Equals("DRDocumentDownLoadfile"))
                {
                    #region     

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        int AuditID = Convert.ToInt32(commandArgs[0]);
                        int ATBDID = Convert.ToInt32(commandArgs[1]);
                        long ChecklistID = Convert.ToInt32(commandArgs[2]);


                        List<PrerequisiteTran_Upload> DraftClosureDocument = new List<PrerequisiteTran_Upload>();
                        if (DraftClosureDocument != null)
                        {
                            using (ZipFile AuditZip = new ZipFile())
                            {

                                DraftClosureDocument = GetFileDataDraftClosureDocument(AuditID, ChecklistID);
                                //AuditZip.AddDirectoryByName("test");
                                if (DraftClosureDocument.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in DraftClosureDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {

                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                            if (!AuditZip.ContainsEntry(AuditID + "/" + ChecklistID + "/" + file.Version + "/" + str))
                                                AuditZip.AddEntry(AuditID + "/" + ChecklistID + "/" + file.Version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    AuditZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=CustomerdataRequest.zip");
                                    Response.BinaryWrite(Filedata);
                                    Response.Flush();
                                    //Response.End();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                BindData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskActivityMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        public static bool CreatePrerequisiteTran_UploadResult(PrerequisiteTran_Upload DraftClosureResult)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.PrerequisiteTran_Upload.Add(DraftClosureResult);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }        
        public static bool Tran_PrerequisiteTran_UploadExists(PrerequisiteTran_Upload ICR)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.PrerequisiteTran_Upload
                             where row.AuditID == ICR.AuditID
                              && row.ATBDId == ICR.ATBDId
                             && row.checklistId == ICR.checklistId
                             && row.FileName == ICR.FileName
                              && row.IsDeleted == false
                             select row).FirstOrDefault();
               
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }        
        public static SP_getinstancedataprerequsite_Result GetInternalAuditInstanceData(long atbdid, long auditid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
            
                var instanceData = (from row in entities.SP_getinstancedataprerequsite(atbdid, auditid)                                    
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }
        public static bool UpdateTransaction(InternalAuditTransaction transaction, List<InternalFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);

                            InternalAuditTransaction RecordtoUpdate = (from row in entities.InternalAuditTransactions
                                                                       where row.ProcessId == transaction.ProcessId && row.FinancialYear == transaction.FinancialYear
                                                                           && row.ForPeriod == transaction.ForPeriod && row.CustomerBranchId == transaction.CustomerBranchId
                                                                           && row.UserID == transaction.UserID && row.RoleID == transaction.RoleID && row.ATBDId == transaction.ATBDId
                                                                       select row).FirstOrDefault();

                            RecordtoUpdate.PersonResponsible = transaction.PersonResponsible;
                            RecordtoUpdate.ObservatioRating = transaction.ObservatioRating;
                            RecordtoUpdate.ObservationCategory = transaction.ObservationCategory;
                            RecordtoUpdate.ForPeriod = transaction.ForPeriod;
                            RecordtoUpdate.Dated = DateTime.Now;
                            long transactionId = RecordtoUpdate.ID;
                            entities.SaveChanges();
                            if (files != null)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.AuditScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {


                return false;
            }
        }
        public static bool CreateTransaction(InternalAuditTransaction transaction, List<InternalFileData_Risk> files,
            List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);
                            transaction.Dated = DateTime.Now;
                            entities.InternalAuditTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.AuditScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {                          
                return false;
            }
        }
        public static bool CreateTransaction(List<InternalFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList,long ScheduleonID,long atbdid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {                          
                            var instanceData = (from row in entities.InternalAuditTransactions
                                                where row.AuditScheduleOnID == ScheduleonID
                                                && row.ATBDId == atbdid
                                                select row).OrderByDescending(a => a.ID).Take(1).FirstOrDefault();

                            if (instanceData != null)
                            {
                                DocumentManagement.Audit_SaveDocFiles(filesList);
                                if (files != null)
                                {
                                    foreach (InternalFileData_Risk fl in files)
                                    {
                                        fl.IsDeleted = false;
                                        entities.InternalFileData_Risk.Add(fl);
                                        entities.SaveChanges();

                                        InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                        fileMapping.TransactionID = instanceData.ID;
                                        fileMapping.FileID = fl.ID;
                                        fileMapping.ScheduledOnID = ScheduleonID;
                                        if (list != null)
                                        {
                                            fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                        }
                                        entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                    }
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        protected void UploadDraft_Click(object sender, EventArgs e)
        {
            try
            {
                int CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);                
                Button btn = (Button)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                FileUpload DraftFile = (FileUpload)gvr.FindControl("DraftFileUpload");

                Label lblChecklistID = (Label)gvr.FindControl("lblChecklistID");
                int ChecklistID = Convert.ToInt32(lblChecklistID.Text);

                Label lblATBDID = (Label)gvr.FindControl("lblATBDID");
                int ATBDID = Convert.ToInt32(lblATBDID.Text);

                Label lblAuditID = (Label)gvr.FindControl("lblAuditID");
                int AuditID = Convert.ToInt32(lblAuditID.Text);

                Label lblChecklistDocument = (Label)gvr.FindControl("lblChecklistDocument");
                string ChecklistDocument = Convert.ToString(lblChecklistDocument.Text);
                if (DraftFile != null)
                {
                    if (DraftFile.HasFile)
                    {
                        HttpFileCollection fileCollection = Request.Files;
                        if (fileCollection.Count > 0)
                        {
                            List<InternalFileData_Risk> Workingfiles = new List<InternalFileData_Risk>();
                            List<KeyValuePair<string, int>> Workinglist = new List<KeyValuePair<string, int>>();
                            List<KeyValuePair<string, Byte[]>> WorkingFilelist = new List<KeyValuePair<string, Byte[]>>();

                           // var getAuditScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedValue, Convert.ToInt32(ddlFilterLocation.SelectedValue), ATBDID, VerticalID, AuditID);

                            var InstanceData = GetInternalAuditInstanceData(Convert.ToInt32(ATBDID), Convert.ToInt32(AuditID));
                            string directoryPathIFD = "";
                            directoryPathIFD = Server.MapPath("~/InternalControlWorkingDocument/" + CustomerId + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalId.ToString() + "/" + InstanceData.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ScheduleonID + "/1.0");
                            DocumentManagement.CreateDirectory(directoryPathIFD);
                            InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                            {
                                AuditScheduleOnID = InstanceData.ScheduleonID,
                                ProcessId = InstanceData.ProcessId,
                                FinancialYear = InstanceData.FinancialYear,
                                ForPerid = InstanceData.ForMonth,
                                CustomerBranchId = InstanceData.CustomerBranchID,
                                IsDeleted = false,
                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                ATBDId = ATBDID,
                                RoleID = 3,
                                InternalAuditInstance = InstanceData.InternalAuditInstance,
                                VerticalID = InstanceData.VerticalId,
                                AuditID = AuditID,
                            };
                            InternalAuditTransaction transaction = new InternalAuditTransaction()
                            {
                                CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                AuditScheduleOnID = InstanceData.ScheduleonID,
                                FinancialYear = InstanceData.FinancialYear,
                                CustomerBranchId = InstanceData.CustomerBranchID,
                                InternalAuditInstance = InstanceData.InternalAuditInstance,
                                ProcessId = InstanceData.ProcessId,
                                ForPeriod = InstanceData.ForMonth,
                                ATBDId = ATBDID,
                                RoleID = 3,
                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                VerticalID = InstanceData.VerticalId,
                                AuditID = AuditID
                            };
                            
                            MstRiskResult.AuditObjective = null;                           
                            MstRiskResult.AuditSteps = "";                           
                            MstRiskResult.AnalysisToBePerofrmed = "";                           
                            MstRiskResult.ProcessWalkthrough = "";                                                    
                            MstRiskResult.ActivityToBeDone = "";                            
                            MstRiskResult.Population = "";                          
                            MstRiskResult.Sample = "";                           
                            MstRiskResult.ObservationNumber = "";                           
                            MstRiskResult.ObservationTitle = "";                          
                            MstRiskResult.Observation = "";                         
                            MstRiskResult.Risk = "";                          
                            MstRiskResult.RootCost = null;                           
                            MstRiskResult.AuditScores = null;                           
                            MstRiskResult.FinancialImpact = null;                          
                            MstRiskResult.Recomendation = "";                          
                            MstRiskResult.ManagementResponse = "";                          
                            MstRiskResult.FixRemark = "";                           
                            MstRiskResult.TimeLine = null;                           
                            MstRiskResult.PersonResponsible = null;                                                      
                            MstRiskResult.Owner = null;
                            MstRiskResult.ObservationRating = null;
                            MstRiskResult.ObservationCategory = null;
                            MstRiskResult.ObservationSubCategory = null;
                            MstRiskResult.AStatusId = 2;

                            transaction.PersonResponsible = null;
                            transaction.Owner = null;                                                           
                            transaction.ObservatioRating = null;                                                         
                            transaction.ObservationCategory = null;                        
                            transaction.ObservationSubCategory = null;

                            string directoryPath = "";
                            List<PrerequisiteTran_Upload> AnnxetureList = new List<PrerequisiteTran_Upload>();
                            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();                                                        
                            if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                            {
                                directoryPath = Server.MapPath("~/AuditCustomerdataRequest/" + CustomerId + "/"
                                  + Convert.ToInt32(AuditID) + "/" + Convert.ToInt32(ATBDID) + "/"
                                  + Convert.ToInt32(ChecklistID) + "/1.0");
                            }
                            if (!Directory.Exists(directoryPath))
                            {
                                DocumentManagement.CreateDirectory(directoryPath);
                            }

                            for (int i = 0; i < fileCollection.Count; i++)
                            {
                                #region
                                HttpPostedFile uploadfile = fileCollection[i];
                                string[] keys = fileCollection.Keys[i].Split('$');
                                String fileName = "";
                                if (keys[keys.Count() - 1].Equals("DraftFileUpload"))
                                {
                                    fileName = uploadfile.FileName;
                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                                    string finalPathPR = Path.Combine(directoryPathIFD, fileKey + Path.GetExtension(uploadfile.FileName));
                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    
                                    if (uploadfile.ContentLength > 0)
                                    {
                                        Workinglist.Add(new KeyValuePair<string, int>(fileName, 1));
                                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                        WorkingFilelist.Add(new KeyValuePair<string, Byte[]>(finalPathPR, bytes));

                                        PrerequisiteTran_Upload DraftClosureUpload = new PrerequisiteTran_Upload()
                                        {
                                            CustomerId = CustomerId,
                                            ATBDId = ATBDID,
                                            FileName = fileName,
                                            AuditID = AuditID,
                                            checklistId = ChecklistID,
                                            checklistName = ChecklistDocument,
                                            IsDeleted = false,
                                            FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                            FileKey = fileKey.ToString(),
                                            Version = "1.0",
                                            VersionDate = DateTime.Now,
                                            CreatedDate = DateTime.Today.Date,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            Status=2,
                                        };
                                        AnnxetureList.Add(DraftClosureUpload);

                                        InternalFileData_Risk file = new InternalFileData_Risk()
                                        {
                                            Name = fileName,
                                            FilePath = directoryPathIFD.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                            FileKey = fileKey.ToString(),
                                            Version = "1.0",
                                            VersionDate = DateTime.UtcNow,
                                            ProcessID = Convert.ToInt32(InstanceData.ProcessId),
                                            CustomerBranchId = Convert.ToInt32(InstanceData.CustomerBranchID),
                                            FinancialYear = InstanceData.FinancialYear,
                                            IsDeleted = false,
                                            ATBDId = ATBDID,
                                            VerticalID = InstanceData.VerticalId,
                                            AuditID = AuditID,
                                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };
                                        Workingfiles.Add(file);
                                        
                                    }
                                }
                                #endregion
                            }//For Loop End  

                            if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                            {
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                                RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                            }
                            else
                            {
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.CreatedOn = DateTime.Now;
                                RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                            }
                            if (WorkingFilelist != null && Workinglist != null)
                            {                              
                                if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                                {
                                    transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    transaction.UpdatedOn = DateTime.Now;
                                    UpdateTransaction(transaction, Workingfiles, Workinglist, WorkingFilelist);
                                }
                                else
                                {
                                    transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    transaction.CreatedOn = DateTime.Now;
                                    CreateTransaction(transaction, Workingfiles, Workinglist, WorkingFilelist);
                                }                                
                            }
                            DocumentManagement.Audit_SaveDocFiles(Filelist);
                            bool Success1 = false;
                            foreach (var item in AnnxetureList)
                            {
                                if (!Tran_PrerequisiteTran_UploadExists(item))
                                {
                                    Success1 = CreatePrerequisiteTran_UploadResult(item);
                                    if (Success1 == true)
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "File Uploaded Successfully";
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Same Name Already Exists..";
                                }
                            }                            
                        }
                    }//File.HasFile  END
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please select file for upload.";
                    }
                }

                BindData();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
    }
}