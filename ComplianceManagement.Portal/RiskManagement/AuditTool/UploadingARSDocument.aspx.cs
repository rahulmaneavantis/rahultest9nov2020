﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class UploadingARSDocument : System.Web.UI.Page
    {
        public List<int> VerticalIdS = new List<int>();
        public List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerListPop();
                ddlLegalEntityPopPup.ClearSelection();
                ddlSubEntity1PopPup.ClearSelection();
                ddlSubEntity2PopPup.ClearSelection();
                ddlSubEntity3PopPup.ClearSelection();
                ddlFilterLocationPopPup.ClearSelection();
                ddlFilterFinancialPopPup.ClearSelection();
                ddlVertical.ClearSelection();
                BindLegalEntityData(ddlLegalEntityPopPup);
                BindFnancialYear(ddlFilterFinancialPopPup);
            }
        }

        private void BindCustomerListPop()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDL(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomerPop.DataTextField = "CustomerName";
                ddlCustomerPop.DataValueField = "CustomerId";
                ddlCustomerPop.DataSource = customerList;
                ddlCustomerPop.DataBind();
                ddlCustomerPop.Items.Insert(0, new ListItem("Customer", "-1"));
                ddlCustomerPop.SelectedIndex = 2;
            }
            else
            {
                ddlCustomerPop.DataSource = null;
                ddlCustomerPop.DataBind();
            }
        }

        private void countrybindIndustory(Saplin.Controls.DropDownCheckBoxes ddlVertical, int? BranchId)
        {
            ddlVertical.DataTextField = "VerticalName";
            ddlVertical.DataValueField = "VerticalsId";
            ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId);
            ddlVertical.DataBind();
        }

        public void BindLegalEntityData(DropDownList PopFDRP)
        {
            int customerID = -1;
            //customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(ddlCustomerPop.SelectedValue))
            {
                if (ddlCustomerPop.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerPop.SelectedValue);
                }
            }
            var details = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            if (details != null)
            {
                PopFDRP.DataTextField = "Name";
                PopFDRP.DataValueField = "ID";
                PopFDRP.Items.Clear();
                PopFDRP.DataSource = details;
                PopFDRP.DataBind();
                PopFDRP.Items.Insert(0, new ListItem("Unit", "-1"));
            }
        }
        public void BindFnancialYear(DropDownList PopFDRP)
        {
            var details = UserManagementRisk.FillFnancialYear();
            if (details != null)
            {


                PopFDRP.DataTextField = "Name";
                PopFDRP.DataValueField = "ID";
                PopFDRP.Items.Clear();
                PopFDRP.DataSource = details;
                PopFDRP.DataBind();
                PopFDRP.Items.Insert(0, new ListItem("Financial Year", "-1"));
            }

        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            //customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(ddlCustomerPop.SelectedValue))
            {
                if (ddlCustomerPop.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerPop.SelectedValue);
                }
            }

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }
        public static void CreateAuditObservation(AuditObservation auditsampleform)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //AuditObservation AuditSampleFormToUpdate = (from row in entities.AuditObservations
                //                                            where row.CustomerBranchID == auditsampleform.CustomerBranchID &&
                //                                            row.VerticalID== auditsampleform.VerticalID
                //                                            && row.FinancialYear == auditsampleform.FinancialYear                                                     
                //                                            select row).FirstOrDefault();

                //if (AuditSampleFormToUpdate == null)
                //{
                entities.AuditObservations.Add(auditsampleform);
                entities.SaveChanges();
                //}
                //else
                //{
                //    AuditSampleFormToUpdate.Name = auditsampleform.Name;
                //    AuditSampleFormToUpdate.FilePath = auditsampleform.FilePath;
                //    AuditSampleFormToUpdate.FileKey = auditsampleform.FileKey;
                //    entities.SaveChanges();
                //}


            }
        }
        public static int GetAuditObservationCount(long CustomerBranchID, long VerticalID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditSampleFormToUpdate = (from row in entities.AuditObservations
                                               where row.CustomerBranchID == CustomerBranchID &&
                                               row.VerticalID == VerticalID
                                               && row.FinancialYear == FinancialYear
                                               select row.Id).ToList().Count;
                if (AuditSampleFormToUpdate != null)
                {
                    return AuditSampleFormToUpdate;
                }
                else
                {
                    return 0;
                }
            }
        }
        protected void upPromotor_Load(object sender, EventArgs e)
        {

        }
        protected void ddlLegalEntityPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
            {
                if (ddlLegalEntityPopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1PopPup, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                    //BindVerticalID(ddlVerticalIDPopPup, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                    this.countrybindIndustory(ddlVertical, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                    BindVersionDetails();
                }
            }
            else
            {
                if (ddlSubEntity1PopPup.Items.Count > 0)
                    ddlSubEntity1PopPup.Items.Clear();

                if (ddlSubEntity2PopPup.Items.Count > 0)
                    ddlSubEntity2PopPup.Items.Clear();

                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.Items.Clear();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.Items.Clear();
            }
        }
        protected void ddlSubEntity1PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
            {
                if (ddlSubEntity1PopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2PopPup, Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue));
                    //BindVerticalID(ddlVerticalIDPopPup, Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue));
                    this.countrybindIndustory(ddlVertical, Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue));
                    BindVersionDetails();
                }
            }
            else
            {
                if (ddlSubEntity2PopPup.Items.Count > 0)
                    ddlSubEntity2PopPup.ClearSelection();

                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.ClearSelection();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }
        }
        protected void ddlSubEntity2PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
            {
                if (ddlSubEntity2PopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3PopPup, Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                    //BindVerticalID(ddlVerticalIDPopPup, Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                    this.countrybindIndustory(ddlVertical, Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                    BindVersionDetails();
                }
            }
            else
            {
                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.ClearSelection();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }
        }
        protected void ddlSubEntity3PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
            {
                if (ddlSubEntity3PopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocationPopPup, Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                    //BindVerticalID(ddlVerticalIDPopPup, Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                    this.countrybindIndustory(ddlVertical, Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                    BindVersionDetails();
                }
            }
            else
            {
                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }
        }
        protected void ddlFilterLocationPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
            {
                //BindVerticalID(ddlVerticalIDPopPup, Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue));
                this.countrybindIndustory(ddlVertical, Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue));
                BindVersionDetails();
            }
        }

        protected void ddlFilterFinancialPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindVersionDetails();
        }
        protected void ddlVerticalIDPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindVersionDetails();
        }
        private void BindVersionDetails()
        {
            try
            {
                string FinancialYear = string.Empty;
                long VerticalId = -1;
                int CustomerBranchId = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
                {
                    if (ddlLegalEntityPopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
                {
                    if (ddlSubEntity1PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
                {
                    if (ddlSubEntity2PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
                {
                    if (ddlSubEntity3PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
                {
                    if (ddlFilterLocationPopPup.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterFinancialPopPup.SelectedValue))
                {
                    if (ddlFilterFinancialPopPup.SelectedValue != "-1")
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancialPopPup.SelectedItem.Text);
                    }
                }

                var AllComplianceRoleMatrix = GetAuditGridVersionDisplay(CustomerBranchId, VerticalId, FinancialYear);
                grdVersionDisplayDownload.DataSource = AllComplianceRoleMatrix;
                grdVersionDisplayDownload.DataBind();
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<ARSObservationUploadView> GetAuditGridVersionDisplay(long BranchID, long VerticalID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<ARSObservationUploadView> riskassignmentexport = new List<ARSObservationUploadView>();
                riskassignmentexport = (from C in entities.ARSObservationUploadViews
                                        select C).ToList();
                if (BranchID != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.CustomerBranchID == BranchID).ToList();
                }
                if (VerticalID != -1)
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.VerticalID == VerticalID).ToList();
                }
                if (!string.IsNullOrEmpty(FinancialYear))
                {
                    riskassignmentexport = riskassignmentexport.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                }
                return riskassignmentexport;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                VerticalIdS.Clear();
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    //int vid = UserManagementRisk.VerticalgetBycustomerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int customerID = -1;
                    if (!string.IsNullOrEmpty(ddlCustomerPop.SelectedValue))
                    {
                        if (ddlCustomerPop.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerPop.SelectedValue);
                        }
                        else
                        {
                            cvDuplicateLocation.IsValid = false;
                            cvDuplicateLocation.ErrorMessage = "Please Select Customer.";
                            return;
                        }
                    }
                    int vid = UserManagementRisk.VerticalgetBycustomerid(customerID);

                    if (vid != -1)
                    {
                        VerticalIdS.Add(vid);
                    }
                }
                else
                {
                    for (int i = 0; i < ddlVertical.Items.Count; i++)
                    {
                        if (ddlVertical.Items[i].Selected)
                        {
                            VerticalIdS.Add(Convert.ToInt32(ddlVertical.Items[i].Value));
                        }
                    }
                }
                if (VerticalIdS.Count > 0)
                {
                    foreach (var VerticalIditem in VerticalIdS)
                    {
                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                        if (FileUploadObservation.HasFile)
                        {
                            string FinancialYear = "";
                            int CustomerBranchId = -1;
                            int VerticalId = Convert.ToInt32(VerticalIditem);
                            int versioncount = 0;
                            if (!string.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
                            {
                                if (ddlLegalEntityPopPup.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue);
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
                            {
                                if (ddlSubEntity1PopPup.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue);
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
                            {
                                if (ddlSubEntity2PopPup.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue);
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
                            {
                                if (ddlSubEntity3PopPup.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue);
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
                            {
                                if (ddlFilterLocationPopPup.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue);
                                }
                            }
                            if (!String.IsNullOrEmpty(ddlFilterFinancialPopPup.SelectedItem.Text))
                            {
                                FinancialYear = Convert.ToString(ddlFilterFinancialPopPup.SelectedItem.Text);
                            }
                            string directoryPath = "";
                            if (!string.IsNullOrEmpty(FinancialYear) && CustomerBranchId != -1 && VerticalId != -1)
                            {
                                int count = GetAuditObservationCount(CustomerBranchId, VerticalId, FinancialYear);
                                versioncount = count + 1;
                                directoryPath = Server.MapPath("~/AuditObservationDocument/" + CustomerBranchId.ToString() + "/" + VerticalId.ToString() + "/" + FinancialYear.ToString());
                            }
                            if (directoryPath != "")
                            {
                                DocumentManagement.CreateDirectory(directoryPath);

                                Guid fileKey = Guid.NewGuid();
                                string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(FileUploadObservation.FileName));

                                Stream fs = FileUploadObservation.PostedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                                if (FileUploadObservation.PostedFile.ContentLength > 0)
                                {
                                    using (AuditControlEntities entities = new AuditControlEntities())
                                    {
                                        AuditObservation file1 = new AuditObservation()
                                        {
                                            Name = FileUploadObservation.PostedFile.FileName,
                                            FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                            FileKey = fileKey.ToString(),
                                            CustomerBranchID = CustomerBranchId,
                                            FinancialYear = Convert.ToString(FinancialYear),
                                            FileData = FileUploadObservation.FileBytes,
                                            VerticalID = VerticalId,
                                            Version = versioncount,
                                            CreatedOn = DateTime.Now,
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            VersionDate = DateTime.Now,
                                            EnType = "A"
                                        };
                                        CreateAuditObservation(file1);
                                        SaveDocFiles(Filelist);
                                        BindVersionDetails();
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    CustomValidator1.ErrorMessage = "Plase select Vertical.";
                    CustomValidator1.IsValid = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }

                }
            }
        }


        protected void ddlCustomerPop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomerPop.SelectedValue))
            {
                if (ddlCustomerPop.SelectedValue != "-1")
                {
                    BindLegalEntityData(ddlLegalEntityPopPup);
                }
                else
                {
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Please Select Customer.";
                }
            }
        }
    }
}