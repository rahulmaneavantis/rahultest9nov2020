﻿<%@ Page Title="My Document" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="MyDocumentAudit.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.MyDocumentAudit" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .btdownss {
            background-image: url(../../Images/icon-download.png);
            border: 0px;
            width: 30px;
            height: 30px;
            background-color: transparent;
            background-repeat: no-repeat;
            float: left;
            background-position: center;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 34px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .clsheadergrid, .table tr th label {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }

        .clsheadergrid, .table tr th input {
            color: #666;
            font-size: 15px;
            font-weight: 400;
            font-family: Roboto,sans-serif;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftdocumentmenu');
            fhead('My Document');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget" style="padding: 0px;">
                <div class="dashboard">
                    <div class="panel-body">
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <section class="panel"> 
                               <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                           <%if (roles.Contains(3))%>
                                           <%{%>
                                            <li class="active" id="liPerformer" runat="server">
                                                <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                            </li>
                                           <%}%>
                                            <%if (roles.Contains(4))%>
                                           <%{%>
                                            <li class=""  id="liReviewer" runat="server">
                                                <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer"  runat="server">Reviewer</asp:LinkButton>                                        
                                            </li>
                                          <%}%>   
                                          <%if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")%>
                                           <%{%>
                                            <li class=""  id="liAuditHead" runat="server">
                                                <asp:LinkButton ID="lnkAuditHead" OnClick="ShowAuditHead" runat="server">Audit Head</asp:LinkButton>                                        
                                            </li>
                                          <%}%>   
                                          <%if (DepartmentHead)%>
                                           <%{%>
                                                <li class=""  id="liDepartmentHead" runat="server">
                                                    <asp:LinkButton ID="lnkDepartmentHead" OnClick="ShowDepartmentHead" runat="server">Department Head</asp:LinkButton>                                        
                                                </li>
                                          <%}%>                                  
                                    </ul>
                                </header>
                                <div class="clearfix"></div>                                                                    
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" Selected="True" />
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;"> 
                                        <asp:DropDownListChosen ID="ddlCustomer" runat="server" DataPlaceHolder="Select Customer"
                                             class="form-control m-bot15" Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"></asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Entity"
                                             class="form-control m-bot15" Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Entity" 
                                            class="form-control m-bot15" Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                   

                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Entity"
                                             class="form-control m-bot15" Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                          DataPlaceHolder="Sub Entity"  AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" 
                                       DataPlaceHolder="Sub Entity"  AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"
                                            class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen ID="ddlFinancialYear" DataPlaceHolder="Financial Year" runat="server" 
                                            OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            AutoPostBack="true" class="form-control m-bot15" Width="90%" Height="32px">                                    
                                        </asp:DropDownListChosen>
                                    </div>

                                    
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true" 
                                            DataPlaceHolder="Scheduling Type" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged"
                                           class="form-control m-bot15" Width="90%" Height="32px">                                    
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                            DataPlaceHolder="Period" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged"
                                            class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>                                    
                                    <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                   <%{%>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true" DataPlaceHolder="Vertical" class="form-control m-bot15" Width="90%" Height="32px"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                    </div>
                                     <%}%>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;"> 
                                           <asp:DropDownListChosen runat="server" ID="ddlStatusDocument" class="form-control m-bot15" Width="90%" Height="32px"
                                                        AllowSingleDeselect="false" DisableSearchThreshold="3"    AutoPostBack="true" OnSelectedIndexChanged="ddlStatusDocument_SelectedIndexChanged">
                                                            <asp:ListItem Text="Closed Audit" value="1" Selected="true" />
                                                            <asp:ListItem Text="Open Audit" value="2"   />                                                            
                                                            </asp:DropDownListChosen>                                                                 
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;display:none;">
                                      
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div>
                                    &nbsp;
                                    <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverage" ShowHeaderWhenEmpty="true" style="text-align:left"
                                        AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                                        CssClass="table"  CellPadding="4" ForeColor="Black"   AllowPaging="true" 
                                        PageSize="20" Width="100%" Font-Size="12px" OnRowCommand="grdSummaryDetailsAuditCoverage_RowCommand">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>   
                                                                                            
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Process">
                                            <ItemTemplate>
                                                 <div class="text_NlinesusingCSS" style="width: 200px;">
                                                <asp:Label ID="lblProcesses" runat="server" Text='<%# Eval("CProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("CProcessName") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="SubProcess">
                                            <ItemTemplate>
                                                 <div class="text_NlinesusingCSS" style="width: 200px;">
                                                <asp:Label ID="lblSubProcesses" runat="server" Text='<%# Eval("SubProcess") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcess") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Financial Year">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblFinYear" runat="server" Text='<%# Eval("FinancialYear") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Period">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>  

                                        <asp:TemplateField ItemStyle-Width="150px" HeaderText="File">
                                           <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:DropDownList runat="server" ID="ddlDocumentFile" class="form-control m-bot15" Style="width: 210px;" AutoPostBack="true">
                                                            <asp:ListItem Text="Working Document" value="1" Selected="true" />
                                                            <asp:ListItem Text="Review Document" value="2"   />                                                                                                                      
                                                            <asp:ListItem Text="Annexure Document" value="3"/>  
                                                            <asp:ListItem Text="Audit Committee Presentation" value="5"/>
                                                            <asp:ListItem Text="Final Deliverables" value="6"/>  
                                                            <asp:ListItem Text="Deleted Documents" value="7"/>   
                                                            <asp:ListItem Text="Feedback Form" value="8"/>   
                                                            <asp:ListItem Text="Client Data" value="9"/>                            
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                       <asp:PostBackTrigger ControlID="ddlDocumentFile" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate> 
                                         </asp:TemplateField>
                                             <asp:TemplateField ItemStyle-Width="80px" HeaderText="Action">
                                               <ItemTemplate>
                                                 <asp:UpdatePanel runat="server" ID="aa2a" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="lblDocumentDownLoadfile" runat="server" 
                                                            data-toggle="tooltip" data-placement="top" ToolTip="Download" CssClass="btdownss"
                                                             CommandName="DocumentDownLoadfile"  CommandArgument='<%# Eval("CustomerBranchID") + "," + Eval("VerticalId") + "," + Eval("FinancialYear") +"," + Eval("ForMonth")+","+Eval("AuditID") %>'></asp:LinkButton> <%--Style="width: 70px;height: 25px;padding: 0px !important;margin: 3px 0px 0px 0px !important;"--%>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                       <asp:PostBackTrigger ControlID="lblDocumentDownLoadfile" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                          </ItemTemplate>
                                        </asp:TemplateField>                                                                                          
                                        <%--    <asp:BoundField DataField="ActivityTobeDone" HeaderText="Audit Step" />--%>
                                         <%--   <asp:BoundField DataField="ForMonth" HeaderText="Period" />                                                                 
                                         <asp:TemplateField ItemStyle-Width="150px" HeaderText="Original">
                                               <ItemTemplate>
                                                 <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="lblDownLoadfile" runat="server" Text="Download" CommandArgument='<%# Eval("ATBDId") + "," + Eval("CustomerBranchId") + "," + Eval("FinancialYear") +"," + Eval("RoleID") +"," + Eval("UserID")+","+Eval("VerticalID")+","+Eval("ProcessID")  %>'  OnClick="DownLoadClick"></asp:LinkButton>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                       <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                          </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField ItemStyle-Width="120px" HeaderText="Review">
                                               <ItemTemplate>
                                                 <asp:UpdatePanel runat="server" ID="aa2a" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="lblReviewDownLoadfile" runat="server" Text="Download" CommandArgument='<%# Eval("ATBDId") + "," + Eval("CustomerBranchId") + "," + Eval("FinancialYear") +"," + Eval("RoleID") +"," + Eval("UserID")+","+Eval("VerticalID")+","+Eval("ProcessID")  %>'  OnClick="ReviewDownLoadClick"></asp:LinkButton>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                       <asp:PostBackTrigger ControlID="lblReviewDownLoadfile" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                          </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField ItemStyle-Width="150px" HeaderText="Final">
                                               <ItemTemplate>
                                                 <asp:UpdatePanel runat="server" ID="aa3a" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="lblFinalDownLoadfile" runat="server" Text="Download" CommandArgument='<%# Eval("ATBDId") + "," + Eval("CustomerBranchId") + "," + Eval("FinancialYear") +"," + Eval("RoleID") +"," + Eval("UserID")+","+Eval("VerticalID")+","+Eval("ProcessID")  %>'  OnClick="FinalDownLoadClick"></asp:LinkButton>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                       <asp:PostBackTrigger ControlID="lblFinalDownLoadfile" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                          </ItemTemplate>
                                        </asp:TemplateField>--%>
                                      
                                        
                                            
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <HeaderStyle BackColor="#ECF0F1" />

                                         <PagerSettings Visible="false" />  
                                        <PagerTemplate>
                                            <%--<table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>--%>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Records Found.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px" AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click" />--%>
                                    <div class="table-paging-text" style="margin-top:-35px;margin-left:19px;background-position:center">
                                        <p>Page
                                       <%--     <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click" />--%>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
