﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
//using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class MSDTrackerReport : System.Web.UI.Page
    {
        protected List<int> Branchlist = new List<int>();
        protected List<string> Quarterlist = new List<string>();
        protected static string AuditHeadOrManagerReport=null;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {              
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();
                BindFnancialYear();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFilterFinancial.ClearSelection();
                    ddlFilterFinancial.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling Type", "-1"));
        }
        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            
        }

        protected string GetTestResult(string TOD, string TOE, string AuditStatusID)
        {
            try
            {
                string returnvalue = string.Empty;
                if ((TOD == "2" || TOD == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Design Failure (TOD)";
                }
                else if ((TOE == "2" || TOE == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Effectiveness Failure (TOE)";
                }
                else if ((TOD == null || TOE == "") || (TOE == null || TOE == ""))
                {
                    returnvalue = "Not Tested";
                }
                else if ((TOD == "2" || TOD == "-1") && AuditStatusID == "2")
                {
                    returnvalue = "Not Tested";
                }
                else
                {
                    returnvalue = "Pass";
                }
                return returnvalue;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetFrequencyName(long ID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var transactionsQuery = (from row in entities.mst_Frequency
                                             where row.Id == ID
                                             select row).FirstOrDefault();

                    return transactionsQuery.Name.Trim();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindFnancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
            ddlFilterFinancial.Items.Insert(0, new ListItem(" Select Financial Year ", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
            upComplianceTypeList.Update();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
            upComplianceTypeList.Update();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
        }

        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSchedulingType(); BindVertical();
        }

        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            upComplianceTypeList.Update();
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                {
                    if (ddlSubEntity4.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            #region Sheet 1   
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    if (CustomerId == 0)
                    {
                        CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    }
                    string chk = txtfromMonthDate.Text;
                    int Year = Convert.ToInt32(chk.Substring(chk.Length - 4));
                    string YourString = chk.Remove(chk.Length - 5);
                    int monthnumber = DateTime.ParseExact(YourString, "MMMM", CultureInfo.InvariantCulture).Month;
                    DateTime fromDatePeroid = new DateTime(Year, monthnumber, 1);

                    string chk2 = txttoMonthDate.Text;
                    int Yearto = Convert.ToInt32(chk2.Substring(chk2.Length - 4));
                    string YourString2 = chk2.Remove(chk2.Length - 5);
                    int monthnumber2 = DateTime.ParseExact(YourString2, "MMMM", CultureInfo.InvariantCulture).Month;
                    int days = DateTime.DaysInMonth(Yearto, monthnumber2);
                    DateTime toDatePeroid = new DateTime(Yearto, monthnumber2, days);
                    if (fromDatePeroid <= toDatePeroid)
                    {
                        #region code                        
                        int RoleID = -1;
                        int HighTotalRisk = 0;
                        ArrayList ItemList = new ArrayList();
                        DataTable Observationcategorytable = new DataTable();
                        Observationcategorytable.Columns.Add("ObservationCategory", typeof(string));
                        Observationcategorytable.Columns.Add("ObservationRating", typeof(int));
                        Observationcategorytable.Columns.Add("ProcessName", typeof(string));
                        if (PerformerFlag)
                            RoleID = 3;
                        if (ReviewerFlag)
                            RoleID = 4;                       
                        string ForPeriod = "";
                        string FnancialYear = "";
                        int CustomerBranchId = -1;
                        string CustomerBranchName = "";
                        int VerticalID = -1;
                        if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                        {
                            if (ddlLegalEntity.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                                CustomerBranchName = ddlLegalEntity.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                        {
                            if (ddlSubEntity1.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                                CustomerBranchName = ddlSubEntity1.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                        {
                            if (ddlSubEntity2.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                                CustomerBranchName = ddlSubEntity2.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                        {
                            if (ddlSubEntity3.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                                CustomerBranchName = ddlSubEntity3.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                        {
                            if (ddlSubEntity4.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                                CustomerBranchName = ddlSubEntity4.SelectedItem.Text;
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                        {
                            if (ddlFilterFinancial.SelectedValue != "-1")
                            {
                                FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                            }
                        }
                        else
                        {
                            FnancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                        }                      
                        if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                        {
                            int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                            if (vid != -1)
                            {
                                VerticalID = vid;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                            {
                                if (ddlVertical.SelectedValue != "-1")
                                {
                                    VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                        {
                            if (ddlSubEntity4.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                                CustomerBranchName = ddlSubEntity4.SelectedItem.Text;
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                        {
                            if (ddlPeriod.SelectedValue != "-1")
                            {
                                ForPeriod = ddlPeriod.SelectedItem.Text;
                            }
                        }
                        if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                        {
                            if (RoleID == -1)
                            {
                                RoleID = 4;
                            }
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchy(CustomerId, CustomerBranchId);
                            var Branchlistloop = Branchlist.ToList();
                        }
                        else
                        {                            
                            Branchlist.Clear();
                            var bracnhes = GetAllHierarchy(CustomerId, CustomerBranchId);
                            var Branchlistloop = Branchlist.ToList();
                        }
                        List<MSDTrackerView> table = new List<MSDTrackerView>();
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var AuditID = UserManagementRisk.GetAuditManagerAuditID(CustomerBranchId, Portal.Common.AuthenticationHelper.UserID);
                            if (AuditID.Count > 0)
                            {
                                table = (from row in entities.MSDTrackerViews
                                         where AuditID.Contains((int)row.AuditID) && (row.AuditPlanEndDate >= fromDatePeroid && row.AuditPlanEndDate <= toDatePeroid)
                                         select row).Distinct().ToList();
                                if (Branchlist.Count > 0)
                                {
                                    List<long?> BranchAssigned = Branchlist.Select(x => (long?)x).ToList();
                                    table = table.Where(Entry => BranchAssigned.Contains(Entry.CustomerBranchId)).ToList();
                                }
                                if (FnancialYear != "")
                                {
                                    table = table.Where(entry => entry.FinancialYear == FnancialYear).ToList();
                                }
                                if (ForPeriod != "")
                                {
                                    table = table.Where(entry => entry.ForPeriod == ForPeriod).ToList();
                                }
                                if (table.Count > 0)
                                {
                                    #region 
                                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("MSD Tracker");
                                    DataTable ExcelData1 = null;
                                    DataView view1 = new System.Data.DataView((table as List<MSDTrackerView>).ToDataTable());
                                    ExcelData1 = view1.ToTable("Selected", false, "Reviewer", "Performer", "AuditPlannedInTheMonth", "ProcessName", "BranchName", "AuditPeriodFrom", "AuditPeriodTo", "AuditPlanStartDate", "AuditPlanEndDate", "AuditVistiStartDate", "AuditVisitEndDate", "DraftReportSubmittedDate", "FeedbackReceivedDate", "ReportFinalised", "AuditeeRatingGrade", "Remark", "DeviationDays", "CustomerBranchId", "FinancialYear", "ForPeriod", "VerticalID");

                                    string MonthToDisplay = string.Empty;
                                    #region
                                    foreach (DataRow item in ExcelData1.Rows)
                                    {
                                        if (item["AuditPlannedInTheMonth"] != null && item["AuditPlannedInTheMonth"] != DBNull.Value)
                                            MonthToDisplay = Convert.ToDateTime(item["AuditPlannedInTheMonth"]).ToString("MMM-yyyy");
                                        string flagcondition = string.Empty;
                                        var FeedBackDate = UserManagementRisk.GetFeedBackFormDate(AuditID);
                                        if (FeedBackDate != null)
                                        {
                                            if (FeedBackDate.FileUploadedDate != null)
                                                item["FeedbackReceivedDate"] = Convert.ToDateTime(FeedBackDate.FileUploadedDate).ToString("dd-MMM-yyyy");
                                        }
                                        int TotalCount = 0;
                                        int TPercentforHigh = 0;
                                        int TPercentforMedium = 0;
                                        int TPercentforLow = 0;
                                        int High = 0;
                                        int Medium = 0;
                                        int Low = 0;
                                        int HighColumn = 0;
                                        int CustomerBranchIdNew = Convert.ToInt32(item["CustomerBranchId"]);
                                        string FinancialYearNew = Convert.ToString(item["FinancialYear"]);
                                        string ForPeriodNew = Convert.ToString(item["ForPeriod"]);
                                        int VerticalIDNew = Convert.ToInt32(item["VerticalID"]);

                                        using (AuditControlEntities entities1 = new AuditControlEntities())
                                        {
                                            string ProcessNames = Convert.ToString(item["ProcessName"]);
                                            ProcessNames = ProcessNames.Trim(',');
                                            string[] processnamescomaseprated = ProcessNames.Split(',');
                                            item["ProcessName"] = ProcessNames;
                                            if (processnamescomaseprated.Length > 0)
                                            {
                                                foreach (var plist in processnamescomaseprated)
                                                {
                                                    int ProcessID = ProcessManagement.GetProcessIDByName(plist, CustomerId);

                                                    var AudiStatus = RiskCategoryManagement.GetObservationRatingList(CustomerBranchIdNew, VerticalIDNew, FinancialYearNew, ForPeriodNew, ProcessID);
                                                    TotalCount += AudiStatus.Count();
                                                    High += AudiStatus.Where(entry => entry.ObservationRating == 1).Count();
                                                    Medium += AudiStatus.Where(entry => entry.ObservationRating == 2).Count();
                                                    Low += AudiStatus.Where(entry => entry.ObservationRating == 3).Count();

                                                    var ObjCategory = UserManagementRisk.GetObservationCatergory(CustomerBranchIdNew, VerticalIDNew, FinancialYearNew, ForPeriodNew, ProcessID);

                                                    HighColumn = ObjCategory.Count;
                                                    if (HighColumn > HighTotalRisk)
                                                    {
                                                        HighTotalRisk = HighColumn;
                                                    }
                                                    foreach (var item1 in ObjCategory)
                                                    {
                                                        bool result = ItemList.Contains(item1.CategoryName);
                                                        if (result == false)
                                                        {
                                                            ItemList.Add(item1.CategoryName);
                                                        }
                                                        Observationcategorytable.Rows.Add(item1.CategoryName, item1.ObservationRating, ProcessNames);
                                                    }

                                                    TPercentforHigh = ((TotalCount / 100) * High);
                                                    TPercentforMedium = ((TotalCount / 100) * Medium);
                                                    TPercentforLow = ((TotalCount / 100) * Low);

                                                    if (TPercentforHigh <= 5 || (TPercentforHigh + TPercentforMedium) <= 25)
                                                    {
                                                        flagcondition = "A";
                                                    }
                                                    else if (TPercentforHigh <= 10 || (TPercentforHigh + TPercentforMedium) <= 50)
                                                    {
                                                        flagcondition = "B";
                                                    }
                                                    else if (TPercentforHigh <= 20 || (TPercentforHigh + TPercentforMedium) <= 100)
                                                    {
                                                        flagcondition = "C";
                                                    }
                                                    else if (TPercentforHigh <= 40)
                                                    {
                                                        flagcondition = "D";
                                                    }
                                                    else if (TPercentforHigh > 40)
                                                    {
                                                        flagcondition = "E";
                                                    }
                                                }
                                            }
                                            item["AuditeeRatingGrade"] = flagcondition;
                                        }
                                    }
                                    #endregion
                                    ExcelData1.Columns.Remove("CustomerBranchId");
                                    ExcelData1.Columns.Remove("FinancialYear");
                                    ExcelData1.Columns.Remove("ForPeriod");
                                    ExcelData1.Columns.Remove("VerticalID");
                                    ExcelData1.Columns.Remove("DeviationDays");
                                    #region

                                    exWorkSheet1.Cells["A1"].Value = "MSD Tracker";

                                    int b = 4 + (ExcelData1.Rows.Count);
                                    string EndRow = "A2:A" + b;
                                    exWorkSheet1.Cells["A2"].Value = "";
                                    exWorkSheet1.Cells[EndRow].Merge = true;
                                    exWorkSheet1.Cells["A2"].AutoFitColumns(5);

                                    exWorkSheet1.Cells["B2"].Value = MonthToDisplay;
                                    exWorkSheet1.Cells["B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["B2"].Style.Fill.BackgroundColor.SetColor(Color.Red);

                                    exWorkSheet1.Cells["B3"].Value = "Team";
                                    exWorkSheet1.Cells["B3:B4"].Merge = true;
                                    exWorkSheet1.Cells["B3"].AutoFitColumns(8);
                                    exWorkSheet1.Cells["B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["B3"].Style.Fill.BackgroundColor.SetColor(Color.MistyRose);
                                    exWorkSheet1.Cells["B3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["C3"].Value = "Leader & Members";
                                    exWorkSheet1.Cells["C3:C4"].Merge = true;
                                    exWorkSheet1.Cells["C3"].AutoFitColumns(8);
                                    exWorkSheet1.Cells["C3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["C3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                    exWorkSheet1.Cells["C3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["D3"].Value = "Audit Planned in the Month";
                                    exWorkSheet1.Cells["D3:D4"].Merge = true;
                                    exWorkSheet1.Cells["D3"].AutoFitColumns(22);
                                    exWorkSheet1.Cells["D3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["D3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["D3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["E3"].Value = "Areas Covered";
                                    exWorkSheet1.Cells["E3:E4"].Merge = true;
                                    exWorkSheet1.Cells["E3"].AutoFitColumns(8);
                                    exWorkSheet1.Cells["E3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["E3"].Style.Fill.BackgroundColor.SetColor(Color.LightYellow);
                                    exWorkSheet1.Cells["E3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["F3"].Value = "Location/s Covered";
                                    exWorkSheet1.Cells["F3:F4"].Merge = true;
                                    exWorkSheet1.Cells["F3"].AutoFitColumns(8);
                                    exWorkSheet1.Cells["F3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["F3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                    exWorkSheet1.Cells["F3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["G3"].Value = "Audit Period";
                                    exWorkSheet1.Cells["G3:H3"].Merge = true;
                                    exWorkSheet1.Cells["G3"].AutoFitColumns(26);
                                    exWorkSheet1.Cells["G3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["G3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["G3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["G4"].Value = "From";
                                    exWorkSheet1.Cells["G4"].AutoFitColumns(13);
                                    exWorkSheet1.Cells["G4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["G4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["G4"].Style.WrapText = true;

                                    exWorkSheet1.Cells["H4"].Value = "To";
                                    exWorkSheet1.Cells["H4"].AutoFitColumns(13);
                                    exWorkSheet1.Cells["H4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["H4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["H4"].Style.WrapText = true;

                                    exWorkSheet1.Cells["I3"].Value = "Audit Planned";
                                    exWorkSheet1.Cells["I3:J3"].Merge = true;
                                    exWorkSheet1.Cells["I3"].AutoFitColumns(26);
                                    exWorkSheet1.Cells["I3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["I3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                    exWorkSheet1.Cells["I3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["I4"].Value = "From";
                                    exWorkSheet1.Cells["I4"].AutoFitColumns(13);
                                    exWorkSheet1.Cells["I4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["I4"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                    exWorkSheet1.Cells["I4"].Style.WrapText = true;

                                    exWorkSheet1.Cells["J4"].Value = "To";
                                    exWorkSheet1.Cells["J4"].AutoFitColumns(13);
                                    exWorkSheet1.Cells["J4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["J4"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                    exWorkSheet1.Cells["J4"].Style.WrapText = true;

                                    exWorkSheet1.Cells["K3"].Value = "Audit Visit";
                                    exWorkSheet1.Cells["K3:L3"].Merge = true;
                                    exWorkSheet1.Cells["K3"].AutoFitColumns(26);
                                    exWorkSheet1.Cells["K3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["K3"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                    exWorkSheet1.Cells["K3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["K4"].Value = "From";
                                    exWorkSheet1.Cells["K4"].AutoFitColumns(13);
                                    exWorkSheet1.Cells["K4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["K4"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                    exWorkSheet1.Cells["K4"].Style.WrapText = true;

                                    exWorkSheet1.Cells["L4"].Value = "To";
                                    exWorkSheet1.Cells["L4"].AutoFitColumns(13);
                                    exWorkSheet1.Cells["L4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["L4"].Style.Fill.BackgroundColor.SetColor(Color.LightGreen);
                                    exWorkSheet1.Cells["L4"].Style.WrapText = true;

                                    exWorkSheet1.Cells["M3"].Value = "Draft Report Submitted (Date)";
                                    exWorkSheet1.Cells["M3:M4"].Merge = true;
                                    exWorkSheet1.Cells["M3"].AutoFitColumns(13);
                                    exWorkSheet1.Cells["M3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["M3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["M3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["N3"].Value = "Feedback Received (Date)";
                                    exWorkSheet1.Cells["N3:N4"].Merge = true;
                                    exWorkSheet1.Cells["N3"].AutoFitColumns(13);
                                    exWorkSheet1.Cells["N3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["N3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["N3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["O3"].Value = "Report Finalised ";
                                    exWorkSheet1.Cells["O3:O4"].Merge = true;
                                    exWorkSheet1.Cells["O3"].AutoFitColumns(13);
                                    exWorkSheet1.Cells["O3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["O3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["O3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["P3"].Value = "Auditee Rating (Grade)";
                                    exWorkSheet1.Cells["P3:P4"].Merge = true;
                                    exWorkSheet1.Cells["P3"].AutoFitColumns(8);
                                    exWorkSheet1.Cells["P3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["P3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["P3"].Style.WrapText = true;

                                    exWorkSheet1.Cells["Q3"].Value = "High Observations / Remarks";
                                    exWorkSheet1.Cells["Q3:Q4"].Merge = true;
                                    exWorkSheet1.Cells["Q3"].AutoFitColumns(30);
                                    exWorkSheet1.Cells["Q3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    exWorkSheet1.Cells["Q3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                    exWorkSheet1.Cells["Q3"].Style.WrapText = true;

                                    #endregion


                                    #region Working Code 

                                    int coloumncreationStartCount = 65;
                                    int coloumncreationcount = 82;
                                    char lastcategoryprintcount = Convert.ToChar(coloumncreationcount);
                                    for (int i = 0; i < ItemList.Count; i++)
                                    {
                                        if (coloumncreationcount <= 90)
                                        {
                                            char sval = Convert.ToChar(coloumncreationcount);
                                            exWorkSheet1.Cells["" + sval + "3"].Value = Convert.ToString(ItemList[i].ToString());
                                            exWorkSheet1.Cells["" + sval + "3:" + sval + "4"].Merge = true;
                                            exWorkSheet1.Cells["" + sval + "3"].AutoFitColumns(30);
                                            exWorkSheet1.Cells["" + sval + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells["" + sval + "3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                            exWorkSheet1.Cells["" + sval + "3"].Style.WrapText = true;
                                            lastcategoryprintcount = sval;
                                            coloumncreationcount++;
                                        }
                                        else
                                        {
                                            char sval = Convert.ToChar(coloumncreationStartCount);
                                            exWorkSheet1.Cells["A" + sval + "3"].Value = Convert.ToString(ItemList[i].ToString());
                                            exWorkSheet1.Cells["A" + sval + "3:A" + sval + "4"].Merge = true;
                                            exWorkSheet1.Cells["A" + sval + "3"].AutoFitColumns(30);
                                            exWorkSheet1.Cells["A" + sval + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            exWorkSheet1.Cells["A" + sval + "3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                            exWorkSheet1.Cells["A" + sval + "3"].Style.WrapText = true;
                                            lastcategoryprintcount = sval;
                                            coloumncreationcount++;
                                            coloumncreationStartCount++;

                                        }
                                    }

                                    if (!string.IsNullOrEmpty(lastcategoryprintcount.ToString()))
                                    {
                                        if (coloumncreationStartCount == 65)
                                        {
                                            exWorkSheet1.Cells["A1:" + lastcategoryprintcount + "1"].Merge = true;
                                        }
                                        else
                                        {
                                            exWorkSheet1.Cells["A1:A" + lastcategoryprintcount + "1"].Merge = true;
                                        }
                                    }
                                    else
                                    {
                                        exWorkSheet1.Cells["A1:Q1"].Merge = true;
                                    }
                                    if (!string.IsNullOrEmpty(lastcategoryprintcount.ToString()))
                                    {
                                        if (coloumncreationStartCount == 65)
                                        {
                                            exWorkSheet1.Cells["B2:" + lastcategoryprintcount + "2"].Merge = true;
                                        }
                                        else
                                        {
                                            exWorkSheet1.Cells["B2:A" + lastcategoryprintcount + "2"].Merge = true;
                                        }
                                    }
                                    else
                                    {
                                        exWorkSheet1.Cells["B2:Q2"].Merge = true;
                                    }


                                    exWorkSheet1.Cells["B5"].LoadFromDataTable(ExcelData1, false);

                                    if (Observationcategorytable.Rows.Count > 0)
                                    {
                                        int rowval = 5;
                                        int rowcreationStartCount = 65;
                                        int rowcreationcount = 82;
                                        char rowcreationlastcategoryprintcount = Convert.ToChar(rowcreationcount);

                                        Observationcategorytable = Observationcategorytable.AsEnumerable()
                                                      .GroupBy(r => new { ObservationCategory = r.Field<string>("ObservationCategory"), ProcessName = r.Field<string>("ProcessName") })
                                                      .Select(g =>
                                                      {
                                                          var row = Observationcategorytable.NewRow();
                                                          row["ObservationCategory"] = g.Key.ObservationCategory;
                                                          row["ProcessName"] = g.Key.ProcessName;
                                                          row["ObservationRating"] = g.Sum(r => r.Field<int>("ObservationRating"));
                                                          return row;
                                                      }).CopyToDataTable();



                                        foreach (DataRow itemdata in ExcelData1.Rows)
                                        {

                                            string ProcessNames = Convert.ToString(itemdata["ProcessName"]);
                                            ProcessNames = ProcessNames.Trim(',');
                                            var newdt = Observationcategorytable.Select("ProcessName = '" + Convert.ToString(ProcessNames) + "'").CopyToDataTable();


                                            for (int i = 0; i < newdt.Rows.Count; i++)
                                            {
                                                //get dynamic created excel row headers
                                                char rowvalue = Convert.ToChar(rowcreationcount);
                                                var headervalue = exWorkSheet1.Cells["" + rowvalue + "3:" + rowvalue + "4"].ElementAt(0).Value;
                                                if (ProcessNames == newdt.Rows[i][2].ToString())
                                                {
                                                    var numberOfRecords = newdt.AsEnumerable()
                                                    .Where(xx => xx["ProcessName"].ToString() == Convert.ToString(newdt.Rows[i][2])
                                                    && xx["ObservationCategory"].ToString() == Convert.ToString(newdt.Rows[i][0])).FirstOrDefault();

                                                    while (headervalue.ToString() != Convert.ToString(newdt.Rows[i][0]))
                                                    {
                                                        if (rowcreationcount <= 90)
                                                        {
                                                            rowvalue = Convert.ToChar(rowcreationcount);
                                                            headervalue = exWorkSheet1.Cells["" + rowvalue + "3:" + rowvalue + "4"].ElementAt(0).Value;
                                                            rowcreationlastcategoryprintcount = rowvalue;
                                                            rowcreationcount++;

                                                        }
                                                        else
                                                        {
                                                            char sval1111 = Convert.ToChar(rowcreationStartCount);
                                                            headervalue = exWorkSheet1.Cells["A" + sval1111 + "3"].ElementAt(0).Value;
                                                            rowcreationlastcategoryprintcount = sval1111;
                                                            rowcreationcount++;
                                                            rowcreationStartCount++;
                                                        }
                                                    }
                                                    if (headervalue.ToString() == Convert.ToString(newdt.Rows[i][0]))
                                                    {
                                                        if (rowcreationcount <= 90)
                                                        {
                                                            char sval = Convert.ToChar(rowcreationlastcategoryprintcount);
                                                            exWorkSheet1.Cells["" + sval + "" + rowval + ""].Value = numberOfRecords["ObservationRating"].ToString();
                                                            exWorkSheet1.Cells["" + sval + "" + rowval + ""].AutoFitColumns(30);
                                                            exWorkSheet1.Cells["" + sval + "" + rowval + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                            exWorkSheet1.Cells["" + sval + "" + rowval + ""].Style.Fill.BackgroundColor.SetColor(Color.White);
                                                            exWorkSheet1.Cells["" + sval + "" + rowval + ""].Style.WrapText = true;
                                                            rowcreationlastcategoryprintcount = sval;
                                                        }
                                                        else
                                                        {
                                                            char sval = Convert.ToChar(rowcreationlastcategoryprintcount);
                                                            exWorkSheet1.Cells["A" + sval + "" + rowval + ""].Value = numberOfRecords["ObservationRating"].ToString();
                                                            exWorkSheet1.Cells["A" + sval + "" + rowval + ""].AutoFitColumns(30);
                                                            exWorkSheet1.Cells["A" + sval + "" + rowval + ""].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                                            exWorkSheet1.Cells["A" + sval + "" + rowval + ""].Style.Fill.BackgroundColor.SetColor(Color.White);
                                                            exWorkSheet1.Cells["A" + sval + "" + rowval + ""].Style.WrapText = true;
                                                            rowcreationlastcategoryprintcount = sval;
                                                        }
                                                    }

                                                    rowcreationcount = 82;
                                                    rowcreationStartCount = 65;
                                                    rowcreationlastcategoryprintcount = Convert.ToChar(rowcreationcount);
                                                }
                                            }
                                            rowval++;

                                        }
                                    }
                                    #endregion

                                    using (ExcelRange col = exWorkSheet1.Cells[1, 1, 4 + ExcelData1.Rows.Count, 17 + ItemList.Count])
                                    {
                                        col.Style.WrapText = true;
                                        using (ExcelRange col1 = exWorkSheet1.Cells[1, 1, 4 + ExcelData1.Rows.Count, 15])
                                        {
                                            col1.Style.Numberformat.Format = "dd-MMM-yyyy";
                                        }
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        exWorkSheet1.Cells["B3:Q3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["B3:Q3"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                        exWorkSheet1.Cells["G4:L4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        exWorkSheet1.Cells["G4:L4"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                                        Response.ClearContent();
                                        Response.Buffer = true;
                                        Response.AddHeader("content-disposition", "attachment;filename=MSD Tracker.xlsx");
                                        Response.Charset = "";
                                        Response.ContentType = "application/vnd.ms-excel";
                                        StringWriter sw = new StringWriter();
                                        Response.BinaryWrite(fileBytes);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    }
                                    #endregion
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Audit Available for selected period.";
                                }
                            }                            
                        }//Using End
                        #endregion
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "From Period should be less than To Period.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    
                }
            }
            #endregion
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
       
        public List<NameValueHierarchy> GetAllHierarchy(long customerID, int customerbranchid)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public void LoadSubEntities(long customerid, NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                }
            }
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}