﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class DeleteAssigment : System.Web.UI.Page
    {
      
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerList();
                Session["TotalRows"] = null;
                BindLegalEntityData();
                BindFinancialYear();
                BindddlQuater();
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFilterFinancial.ClearSelection();
                    ddlFilterFinancial.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                BindComplianceMatrix();
                bindPageNumber();
            }
        }

        private void BindCustomerList()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDL(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
                ddlCustomer.SelectedIndex = 2;
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();
            }
        }

        private void BindComplianceMatrix()
        {
            try
            {
                long pid = -1;
                long psid = -1;
                long CustomerBranchId = -1;
                string FnancialYear = "";
                string ForPeriod = "";
                List<string> ControlList = new List<string>();
                if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        pid = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
                {
                    if (ddlFilterSubProcess.SelectedValue != "-1")
                    {
                        psid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);

                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                {
                    if (ddlFilterFinancial.SelectedItem.Text != "-1")
                    {
                        FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                    }
                }
                if (!String.IsNullOrEmpty(ddlQuarter.SelectedValue))
                {
                    if (ddlQuarter.SelectedItem.Text != "-1")
                    {
                        ForPeriod = Convert.ToString(ddlQuarter.SelectedItem.Text);
                    }
                }

                if (ddlControllist.Items.Count > 0)
                {
                    for (int i = 0; i < ddlControllist.Items.Count; i++)
                    {
                        if (ddlControllist.Items[i].Selected)
                        {
                            ControlList.Add(Convert.ToString(ddlControllist.Items[i].Text));
                        }
                    }
                }

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (CustomerBranchId != -1)
                {
                    var AllComplianceRoleMatrix = RiskCategoryManagement.GetAllDataofAssignment(CustomerId, CustomerBranchId, Convert.ToInt32(pid), Convert.ToInt32(psid), FnancialYear, ForPeriod, ControlList);
                    grdComplianceRoleMatrix.DataSource = AllComplianceRoleMatrix;
                    grdComplianceRoleMatrix.DataBind();
                    Session["TotalRows"] = null;
                    Session["TotalRows"] = AllComplianceRoleMatrix.Count;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    //Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                //Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        private void BindddlQuater()
        {
            if (ddlQuarter.SelectedValue != null)
            {
                ddlQuarter.DataTextField = "Name";
                ddlQuarter.DataValueField = "ID";
                ddlQuarter.DataSource = UserManagementRisk.GetQuarterList();
                ddlQuarter.DataBind();
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindControlNo()
        {
            long pid = -1;
            long psid = -1;
            long CustomerBranchId = -1;
            string FnancialYear = "";
            string ForPeriod = "";
            List<string> ControlList = new List<string>();
            if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
            {
                if (ddlFilterProcess.SelectedValue != "-1")
                {
                    pid = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
            {
                if (ddlFilterSubProcess.SelectedValue != "-1")
                {
                    psid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
            {
                if (ddlFilterFinancial.SelectedItem.Text != "-1")
                {
                    FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                }
            }
            if (!String.IsNullOrEmpty(ddlQuarter.SelectedValue))
            {
                if (ddlQuarter.SelectedItem.Text != "-1")
                {
                    ForPeriod = Convert.ToString(ddlQuarter.SelectedItem.Text);
                }
            }
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            //Branchlist.Clear();
            //var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(CustomerBranchId));
            //var Branchlistloop = Branchlist.ToList();
            //if (Branchlist.Count > 0)
            //{
            if (CustomerBranchId != -1)
            {
                var AllComplianceRoleMatrix = RiskCategoryManagement.GetAllDataofAssignment(CustomerId, CustomerBranchId, Convert.ToInt32(pid), Convert.ToInt32(psid), FnancialYear, ForPeriod, ControlList);

                AllComplianceRoleMatrix = AllComplianceRoleMatrix.OrderBy(entry => entry.ControlNo).ToList();
                var BindControls = (from row in AllComplianceRoleMatrix
                                    select new { ID = row.RiskCreationID, ControlNo = row.ControlNo }).ToList<object>();

                ddlControllist.DataTextField = "ControlNo";
                ddlControllist.DataValueField = "Id";
                ddlControllist.DataSource = BindControls;
                ddlControllist.DataBind();
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public void BindLegalEntityData()
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            var data = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = data;
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        public void BindFinancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
            ddlFilterFinancial.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }

        private void BindProcess(long branchid)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                string role = AuthenticationHelper.Role;
                ddlFilterProcess.Items.Clear();
                ddlFilterProcess.DataTextField = "Name";
                ddlFilterProcess.DataValueField = "ID";
                if (role != "CADMN") // "SADMN"
                {
                    ddlFilterProcess.DataSource = ProcessManagement.FillProcessDropdownAuditManager(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                    ddlFilterProcess.DataBind();
                }
                else
                {

                    ddlFilterProcess.DataSource = ProcessManagement.FillProcessDropdownCompanyAdmin(CustomerId);
                    ddlFilterProcess.DataBind();
                }
                ddlFilterProcess.Items.Insert(0, new ListItem("Process", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindSubProcess()
        {
            try
            {
                long ProcessId = -1;
                int CustomerBranchId = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        ProcessId = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                    }
                }
                ddlFilterSubProcess.DataTextField = "Name";
                ddlFilterSubProcess.DataValueField = "ID";
                ddlFilterSubProcess.Items.Clear();
                //ddlFilterSubProcess.DataSource = ProcessManagement.FillCompanyAdminSubProcess(ProcessId);
                ddlFilterSubProcess.DataSource = UserManagementRisk.FillCustomerTransactionSubProcess(ProcessId, CustomerBranchId);
                ddlFilterSubProcess.DataBind();
                ddlFilterSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                BindComplianceMatrix();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceRoleMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    grdComplianceRoleMatrix.DataSource = null;
                    grdComplianceRoleMatrix.DataBind();

                    BindProcess(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.ClearSelection();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.ClearSelection();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.ClearSelection();

            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    grdComplianceRoleMatrix.DataSource = null;
                    grdComplianceRoleMatrix.DataBind();

                    BindProcess(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.ClearSelection();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.ClearSelection();

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    grdComplianceRoleMatrix.DataSource = null;
                    grdComplianceRoleMatrix.DataBind();

                    BindProcess(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.ClearSelection();

            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    grdComplianceRoleMatrix.DataSource = null;
                    grdComplianceRoleMatrix.DataBind();

                    BindProcess(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdComplianceRoleMatrix.DataSource = null;
            grdComplianceRoleMatrix.DataBind();

            if (ddlFilterLocation.SelectedValue != null)
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    BindProcess(Convert.ToInt32(ddlFilterLocation.SelectedValue));

                }
            }
            ddlPageSize_SelectedIndexChanged(sender, e);
        }


        protected void ddlFilterProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterProcess.SelectedValue != null)
            {
                if (ddlFilterProcess.SelectedValue != "-1")
                {
                    BindSubProcess();
                    BindComplianceMatrix();
                    BindControlNo();
                    bindPageNumber();
                }
            }
        }

        protected void ddlFilterSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceMatrix();
            BindControlNo();
            bindPageNumber();
        }

        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceMatrix();
            bindPageNumber();
        }

        protected void ddlControllist_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceMatrix();
        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                bool deleteSuccess = false;
                List<string> lstAuditIDErrorList = new List<string>();
                string lst = string.Empty;
                int deleteSuccessCount = 0;

                for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
                {

                    Label lblRiskCategoryCreationId = (Label)grdComplianceRoleMatrix.Rows[i].FindControl("lblRiskCategoryCreationId");
                    Label lblCustomerBranchId = (Label)grdComplianceRoleMatrix.Rows[i].FindControl("lblCustomerBranchId");
                    Label lblAuditInstanceId = (Label)grdComplianceRoleMatrix.Rows[i].FindControl("lblAuditInstanceId");
                    Label lblControlNO = (Label)grdComplianceRoleMatrix.Rows[i].FindControl("lblControlNO");
                    Label lblBranch = (Label)grdComplianceRoleMatrix.Rows[i].FindControl("lblBranch");
                    if (lblAuditInstanceId != null)
                    {
                        if (lblAuditInstanceId.Text != "")
                        {
                            bool CheckCount = RiskCategoryManagement.CheckICRAssignmentAssignmentExist(Convert.ToInt32(lblAuditInstanceId.Text), Convert.ToInt32(lblRiskCategoryCreationId.Text), Convert.ToInt32(lblCustomerBranchId.Text));
                            if (CheckCount)
                            {
                                if (lblBranch.Text != "" && lblControlNO.Text != "")
                                {
                                    lst = lblBranch.Text + '-' + lblControlNO.Text  + " - Control Assigned already performed. So, Control assignment can not be Delete...!";
                                }
                                if (!string.IsNullOrEmpty(lst))
                                {
                                    lstAuditIDErrorList.Add(lst);
                                }                                
                            }
                            else
                            {
                                
                                deleteSuccess = UserManagementRisk.DeleteAssignmentByAuditInstance(Convert.ToInt32(lblAuditInstanceId.Text));
                                if (!deleteSuccess)
                                {
                                    if (lblBranch.Text != "" && lblControlNO.Text != "")
                                    { 
                                        lst = lblBranch.Text + '-' + lblControlNO.Text  + " -  Control Assigned can not be Delete...!";
                                    }
                                    if (!string.IsNullOrEmpty(lst))
                                    {
                                        lstAuditIDErrorList.Add(lst);
                                    }
                                }
                                else
                                {
                                    deleteSuccessCount++;
                                }
                            }
                        }                        
                    }
                }

                BindComplianceMatrix();
                if (lstAuditIDErrorList.Count > 0)
                {
                    ShowErrorMessages(lstAuditIDErrorList);
                }
                else
                {
                    if (deleteSuccessCount > 0)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = deleteSuccessCount + "- Control Assignment Deleted Successfully";
                    }
                }
                upComplianceTypeList.Update();


                #region Previous Code
                //long pid = -1;
                //long psid = -1;
                //long CustomerBranchId = -1;
                //string FnancialYear = "";
                //string ForPeriod = "";
                //List<string> ControlList = new List<string>();
                //if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                //{
                //    if (ddlFilterProcess.SelectedValue != "-1")
                //    {
                //        pid = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                //    }
                //}
                //if (!String.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
                //{
                //    if (ddlFilterSubProcess.SelectedValue != "-1")
                //    {
                //        psid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                //    }
                //}
                //if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                //{
                //    if (ddlLegalEntity.SelectedValue != "-1")
                //    {
                //        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);

                //    }
                //}
                //if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                //{
                //    if (ddlSubEntity1.SelectedValue != "-1")
                //    {
                //        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                //    }
                //}
                //if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                //{
                //    if (ddlSubEntity2.SelectedValue != "-1")
                //    {
                //        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                //    }
                //}
                //if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                //{
                //    if (ddlSubEntity3.SelectedValue != "-1")
                //    {
                //        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                //    }
                //}
                //if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                //{
                //    if (ddlFilterLocation.SelectedValue != "-1")
                //    {
                //        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                //    }
                //}
                //if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                //{
                //    if (ddlFilterFinancial.SelectedItem.Text != "-1")
                //    {
                //        FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                //    }
                //}
                //if (!String.IsNullOrEmpty(ddlQuarter.SelectedValue))
                //{
                //    if (ddlQuarter.SelectedItem.Text != "-1")
                //    {
                //        ForPeriod = Convert.ToString(ddlQuarter.SelectedItem.Text);
                //    }
                //}

                //if (ddlControllist.Items.Count > 0)
                //{
                //    for (int i = 0; i < ddlControllist.Items.Count; i++)
                //    {
                //        if (ddlControllist.Items[i].Selected)
                //        {
                //            ControlList.Add(Convert.ToString(ddlControllist.Items[i].Text));
                //        }
                //    }
                //}
                //if (CustomerId == 0)
                //{
                //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //}
                //Branchlist.Clear();
                //var bracnhes = GetAllHierarchy(CustomerId, Convert.ToInt32(CustomerBranchId));
                //var Branchlistloop = Branchlist.ToList();
                //if (Branchlist.Count > 0)
                //{
                //    List<Sp_GetAllAssginmentControlForDelete_Result> AllComplianceRoleMatrix = RiskCategoryManagement.GetAllDataofAssignment(CustomerId, Branchlist.ToList(), Convert.ToInt32(pid), Convert.ToInt32(psid), FnancialYear, ForPeriod, ControlList);

                //    AllComplianceRoleMatrix = AllComplianceRoleMatrix.Where(entry => entry.StatusId != 1).ToList();
                //    if (AllComplianceRoleMatrix.Count > 0)
                //    {
                //        cvDuplicateEntry.IsValid = false;
                //        cvDuplicateEntry.ErrorMessage = "Audit already performed. So, Audit assignment can not be Delete...!";//Action performed 
                //    }
                //    else
                //    {
                //        var AllData = RiskCategoryManagement.GetAllDataofAssignment(CustomerId, Branchlist.ToList(), Convert.ToInt32(pid), Convert.ToInt32(psid), FnancialYear, ForPeriod, ControlList);

                //        List<long> InstanceIDList = (from row in AllData
                //                                     select row.AuditInstanceId).Distinct().ToList();

                //        bool Success = false;
                //        if (InstanceIDList.Count > 0)
                //        {
                //            Success = UserManagementRisk.DeleteAssignmentByAuditInstance(InstanceIDList);
                //        }
                //        if (Success)
                //        {
                //            cvDuplicateEntry.IsValid = false;
                //            cvDuplicateEntry.ErrorMessage = "Control assingment deleted successfully.";
                //            InstanceIDList.Clear(); BindControlNo(); BindComplianceMatrix(); upComplianceTypeList.Update();
                //        }
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void ShowErrorMessages(List<string> emsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdComplianceRoleMatrix.PageIndex = chkSelectedPage - 1;
                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindComplianceMatrix();
                bindPageNumber();
            }
        }

        protected void ddlQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceMatrix();
            bindPageNumber();
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLegalEntityData();
            BindComplianceMatrix();
        }
    }
}