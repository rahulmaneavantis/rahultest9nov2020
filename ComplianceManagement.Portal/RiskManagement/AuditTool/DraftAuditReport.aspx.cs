﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

using System.Data;  

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class DraftAuditReport : System.Web.UI.Page
    {
        int Statusid;
        protected List<Int32> roles;
        public static List<long> Branchlist = new List<long>();
        protected int CustomerId = 0;
        public static string DocumentPath = "";
        protected bool DepartmentHead = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);                
                BindFinancialYear();
                BindLegalEntityData();
                BindVertical();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    if (Request.QueryString["Status"] == "Open")
                    {
                        ViewState["Status"] = 1;
                    }
                    else if (Request.QueryString["Status"] == "Closed")
                    {
                        ViewState["Status"] = 3;
                    }else
                    {
                        ViewState["Status"] = 1;
                    }
                }
                else
                {
                    ViewState["Status"] = 1;
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    if (Request.QueryString["Type"] == "Process")
                    {
                        ShowProcessGrid(sender, e);
                    }
                    else if (Request.QueryString["Type"] == "Implementation")
                    {
                        ShowImplementationGrid(sender, e);
                    }
                    else
                    {
                        ShowProcessGrid(sender, e);
                    }
                }
                else
                {
                    ShowProcessGrid(sender, e);
                }
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
            {
                if (ViewState["Type"].ToString() == "Process")
                {
                    grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdProcessAudits.PageIndex = chkSelectedPage - 1;
                }
                else if (ViewState["Type"].ToString() == "Implementation")
                {
                    grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdImplementationAudits.PageIndex = chkSelectedPage - 1;
                }
            }
            BindData();
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
        }

        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);           
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            //ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                //ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindData()
        {
            try
            {
                int branchid = -1;
                string FinYear = String.Empty;
                string Period = String.Empty;
                string Flag = String.Empty;
                int VerticalID = -1;
                string departmentheadFlag = string.Empty;
                if (DepartmentHead)
                {
                    departmentheadFlag = "DH";
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ViewState["Status"].ToString()))
                {
                    Statusid = Convert.ToInt32(ViewState["Status"]);
                }
                if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
                {
                    Flag = Convert.ToString(ViewState["Type"]);
                }
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }               
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }

                if (FinYear == "")
                    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                Branchlist.Clear();
                GetAllHierarchy(CustomerId, branchid);
                Branchlist.ToList();

                if (Flag == "Process")
                {
                    grdProcessAudits.DataSource = null;
                    grdProcessAudits.DataBind();

                    grdImplementationAudits.Visible = false;
                    grdProcessAudits.Visible = true;

                    var AuditLists = DashboardManagementRisk.GetAuditManagerAudits(FinYear, Period, CustomerId, Branchlist, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalID,4, departmentheadFlag);
                    grdProcessAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdProcessAudits.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }
                else if (Flag == "Implementation")
                {
                    grdProcessAudits.DataSource = null;
                    grdProcessAudits.DataBind();

                    grdImplementationAudits.Visible = true;
                    grdProcessAudits.Visible = false;

                    var AuditLists = DashboardManagementRisk.GetAuditManagerAuditsIMP(FinYear, Period, CustomerId, branchid, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalID);
                    grdImplementationAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdImplementationAudits.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }

                bindPageNumber();
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }

            BindSchedulingType();
            BindData();
            BindVertical();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                {
                    BindSchedulingType();
                    BindVertical();
                    BindData();
                }
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            {
                BindAuditSchedule("S", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        int count = 0;
                        count = UserManagementRisk.GetPhaseCount(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        BindAuditSchedule("P", count);
                    }
                }
            }
            bindPageNumber();
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
                {
                    if (ViewState["Type"].ToString() == "Process")
                    {
                        grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    }
                    else if (ViewState["Type"].ToString() == "Implementation")
                    {
                        grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    }
                }

                BindData();
                if (ViewState["Type"].ToString() == "Process")
                {
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdProcessAudits.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
                else if (ViewState["Type"].ToString() == "Implementation")
                {
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdImplementationAudits.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void grdProcessAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    //int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                    //string ForMonth = Convert.ToString(commandArgs[1]);
                    //string FinancialYear = Convert.ToString(commandArgs[2]);
                    //int Verticalid = Convert.ToInt32(commandArgs[3]);
                    int AuditID = Convert.ToInt32(commandArgs[4]);
                    ViewState["AuditID"] = AuditID;
                    #region generate report
                    try
                    {
                        if (CustomerId == 0)
                        {
                            CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        }
                      
                        string CustomerBranchName = string.Empty;
                        string FinancialYear = string.Empty;
                        int CustomerBranchId = -1;
                        string PeriodName = string.Empty;

                        if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                        {
                            if (ddlLegalEntity.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                                CustomerBranchName = ddlLegalEntity.SelectedItem.Text.Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                        {
                            if (ddlSubEntity1.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                                CustomerBranchName = ddlSubEntity1.SelectedItem.Text.Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                        {
                            if (ddlSubEntity2.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                                CustomerBranchName = ddlSubEntity2.SelectedItem.Text.Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                        {
                            if (ddlSubEntity3.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                                CustomerBranchName = ddlSubEntity3.SelectedItem.Text.Trim();
                            }
                        }
                    
                      
                        int VerticalID = -1;
                       
                        if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                        {
                            if (ddlPeriod.SelectedValue != "-1")
                            {
                                PeriodName = ddlPeriod.SelectedItem.Text.Trim();
                            }
                        }
                        var customerName = UserManagementRisk.GetCustomerName(CustomerId);
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var AuditDetails = (from row in entities.AuditClosureDetails
                                                where row.ID == AuditID
                                                select row).FirstOrDefault();
                            CustomerBranchId = Convert.ToInt32(AuditDetails.BranchId);
                            FinancialYear = AuditDetails.FinancialYear;
                            PeriodName = AuditDetails.ForMonth;
                            VerticalID = Convert.ToInt32(AuditDetails.VerticalId);
                            CustomerBranchName = (from row in entities.mst_CustomerBranch
                                                  where row.ID == CustomerBranchId
                                                  select row.Name).FirstOrDefault().ToString();

                            var s = (from row in entities.Tran_FinalDeliverableUpload
                                     where row.AuditID == AuditID
                                     select row).ToList();
                            if (s != null)
                            {
                                entities.Tran_FinalDeliverableUpload.RemoveRange(s);
                                entities.SaveChanges();
                            }
                        }
                        #region Word Report             
                        System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();
                        System.Text.StringBuilder stringbuilderTOPSecond = new System.Text.StringBuilder();
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var itemlist = (from row in entities.SPGETObservationDetails_AsPerClient_Draft(CustomerBranchId, FinancialYear, VerticalID, PeriodName, Convert.ToInt32(AuditID))
                                            select row).ToList();
                            if (CustomerId == 65)
                            {
                                #region first Report
                                stringbuilderTOP.Append(@"<style>.break { page-break-after: always;}</style>");

                                stringbuilderTOP.Append(@"<p style='margin-top:20%;margin-bottom:20%'><b><h1 style='text-align: center;'>"
                                + "<br/><br/><br/><br/><br/><br/>"
                                + customerName + "<br/>" + CustomerBranchName + "</h1></b></p><br/><br/><br/><br/><br/><br/>");

                                stringbuilderTOP.Append(@"<p style='margin-top:20%;margin-bottom:20%'><b><h1 style='text-align: center;'>"
                                + "Internal Audit Report" + "<br/>" + PeriodName + " " + "FY" + " " + FinancialYear +
                                "</h1></b></p><br/><br/><br/><br/><br/><br/><br/>");

                                stringbuilderTOP.Append(@"<p><b><u>"
                                + "INTRODUCTION:" + "<br/><br/> </b></u>" +
                                "This report contains the results of our findings and observations during the Internal Audit of" + " " + CustomerBranchName + " " + "for the period" + " "
                                + PeriodName + " " + "FY" + " " + FinancialYear + ". " + "During the process of conducting internal audit, we have made a general review of management controls to identify preferred practices to effectively and efficiently manage operational and process risks and scrutiny of various books and records. We have also discussed with concerned persons and the information and explanations as provided by them have been duly incorporated in this report, wherever required."
                                + "<br/><br/></br>" + "<b><u>SCOPE OF WORK:<br/>" +
                                "</u></p>");

                                var DistinictProcess = (from row in itemlist
                                                        select row.ProcessName).Distinct().ToList();

                                stringbuilderTOP.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>" +
                                "<tr style='background-color:#D3D3D3;'>" +
                                "<th style='width:8%;border: 1px solid black; padding:10px;'><b>S No.</b></th>" +
                                "<th style='width:92%;border: 1px solid black; padding:10px;'><b>Area's Covered</b></th>" +
                                "</tr>");

                                int SNo = 0;
                                foreach (var Pname in DistinictProcess)
                                {
                                    stringbuilderTOP.Append(@"<tr>" +
                                    "<td style='width:8%;border: 1px solid black;vertical-align: top;padding:10px;'>" + ++SNo + "</td>" +
                                    "<td style='width:92%;border: 1px solid black;vertical-align: top;padding:10px;'>" + Pname + "</td>" +
                                    "</tr>");
                                }

                                stringbuilderTOP.Append(@"</table>" +
                                    "<br/><br/>");

                                int ProcessCount = 0;
                                stringbuilderTOP.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>");
                                foreach (var Pname in DistinictProcess)
                                {
                                    stringbuilderTOP.Append(@"<tr style='background-color:#D3D3D3'>" +
                               "<td style='width:8%;border: 1px solid black;vertical-align: top;padding:10px;'><b>" + ++ProcessCount + "." + "</b></td>" +
                               "<td style='width:92%;border: 1px solid black;vertical-align: top;padding:10px;'><b><u>" + Pname + "</u></b></td>" +
                               "</tr>");
                                    var DistinictSubProcess = (from row in itemlist
                                                               where row.ProcessName == Pname
                                                               select row.SubProcessName).Distinct().ToList();

                                    int SubProcessCount = 0;
                                    if (DistinictSubProcess.Count > 0)
                                    {
                                        foreach (var SProcess in DistinictSubProcess)
                                        {
                                            stringbuilderTOP.Append(@"<tr>" +
                                   "<td style='width:8%;border: 1px solid black;vertical-align: top;padding:10px;'><b>" + ProcessCount + "." + ++SubProcessCount + "." + "</b></td>" +
                                   "<td style='width:92%;border: 1px solid black;vertical-align: top;padding:10px;'><b><u>" + SProcess + "</u></b></td>" +
                                   "</tr>");

                                            var GetObservation = (from row in itemlist
                                                                  where row.ProcessName == Pname &&
                                                                  row.SubProcessName == SProcess
                                                                  select row).ToList();

                                            int ObservationCount = 0;
                                            foreach (var item in GetObservation)
                                            {
                                                stringbuilderTOP.Append(@"<tr>" +
                                  "<td style='width:8%;border: 1px solid black;vertical-align: top;padding:10px;'><b>" + ProcessCount + "." + SubProcessCount + "." + ++ObservationCount + "</b></td>" +
                                  "<td style='width:92%;border: 1px solid black;vertical-align: top;padding:10px;'><b><u>" + item.ObservationTitle + ":</u></b><br/>" +
                                  "<p><div style='margin-left: 5px;overflow-wrap: break-word;width:90%'>" + item.Observation + "</div><br/><br/>" +
                                   "<div style='margin-left: 5px;overflow-wrap: break-word;'>" + item.BodyContent + "</div><br/><br/>" +
                                   "<b><u>" + "Recommendation:&nbsp;" + "</b></u><div style='margin-left: 5px;overflow-wrap: break-word;width:200px'>" +
                                   item.Recomendation + "</div><br/><br/>" +
                                   "<b><u>" + "HOD Comments:&nbsp;" + "</b></u><div style='margin-left: 5px;overflow-wrap: break-word;width:200px'>" +
                                   item.ManagementResponse + "</div><br/><br/>" +
                                   "<b><u>" + "Unit Head Comments:&nbsp;" + "</b></u><div style='margin-left: 5px;overflow-wrap: break-word;width:200px'>" +
                                   item.UHComment + "</div> <br /><br/>" +
                                   "<b><u>" + "President Comments:&nbsp;" + "</b></u><div style='margin-left: 5px;overflow-wrap: break-word;width:200px'>" +
                                   item.PRESIDENTComment + "</div><br/><br/>" +
                                  "</P></td></tr>");

                                                //Image
                                                stringbuilderTOP.Append(@"<tr>" +
                                  "<td style='width:8%;border: 1px solid black;vertical-align: top;padding:10px;'></td>" +
                                  "<td style='width:92%;border: 1px solid black;vertical-align: top;padding:10px;'><div style='width:250px'> ");

                                                List<ObservationImage> AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFileList(AuditID, item.ATBDId);
                                                if (AllinOneDocumentList.Count > 0)
                                                {
                                                    foreach (var ObjImageitem in AllinOneDocumentList)
                                                    {
                                                        string filePath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                                                        if (ObjImageitem.ImagePath != null && File.Exists(filePath))
                                                        {
                                                            DocumentPath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                                                            // DocumentPath = filePath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                            stringbuilderTOP.Append("<img src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/" + CustomerId + "/"
                                                            + item.Customerbranchid + "/" + item.VerticalID + "/" + item.ProcessId + "/"
                                                            + item.FinancialYear + "/" + item.ForPerid + "/"
                                                            + AuditID + "/" +
                                                            +item.ATBDId + "/1.0/" + ObjImageitem.ImageName + "' alt ='d'></img >" + "<br>");
                                                        }
                                                    }
                                                }
                                                stringbuilderTOP.Append(@"</div></td></tr>");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var GetObservation = (from row in itemlist
                                                              where row.ProcessName == Pname
                                                              select row).ToList();

                                        int ObservationCount = 0;
                                        foreach (var item in GetObservation)
                                        {
                                            stringbuilderTOP.Append(@"<tr>" +
                                         "<td style='width:8%;border: 1px solid black;vertical-align: top;padding:10px;'><b>" + ProcessCount + "." + SubProcessCount + "." + ++ObservationCount + "</b></td>" +
                                  "<td style='width:92%;border: 1px solid black;vertical-align: top;padding:10px;'><b><u>" + item.ObservationTitle + ":</u></b><br/>" +
                                  "<p><div style='margin-left: 5px;overflow-wrap: break-word;width:90%'>" + item.Observation + "</div><br/><br/>" +
                                   "<div style='margin-left: 5px;overflow-wrap: break-word;'>" + item.BodyContent + "</div><br/><br/>" +
                                   "<b><u>" + "Recommendation:&nbsp;" + "</b></u><div style='margin-left: 5px;overflow-wrap: break-word;width:300px'>" +
                                   item.Recomendation + "</div><br/><br/>" +
                                   "<b><u>" + "HOD Comments:&nbsp;" + "</b></u><div style='margin-left: 5px;overflow-wrap: break-word;width:300px'>" +
                                   item.ManagementResponse + "</div><br/><br/>" +
                                   "<b><u>" + "Unit Head Comments:&nbsp;" + "</b></u><div style='margin-left: 5px;overflow-wrap: break-word;width:300px'>" +
                                   item.UHComment + "</div> <br /><br/>" +
                                   "<b><u>" + "President Comments:&nbsp;" + "</b></u><div style='margin-left: 5px;overflow-wrap: break-word;width:300px'>" +
                                   item.PRESIDENTComment + "</div><br/><br/>" +
                                  "</P></td></tr>");

                                            //Image
                                            stringbuilderTOP.Append(@"<tr>" +
                              "<td style='width:8%;border: 1px solid black;vertical-align: top;padding:10px;'></td>" +
                                  "<td style='width:92%;border: 1px solid black;vertical-align: top;padding:10px;'><div style='300px'> ");

                                            List<ObservationImage> AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFileList(AuditID, item.ATBDId);
                                            if (AllinOneDocumentList.Count > 0)
                                            {
                                                foreach (var ObjImageitem in AllinOneDocumentList)
                                                {
                                                    string filePath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                                                    if (ObjImageitem.ImagePath != null && File.Exists(filePath))
                                                    {
                                                        DocumentPath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                                                        stringbuilderTOP.Append("<img src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/" + CustomerId + "/"
                                                        + item.Customerbranchid + "/" + item.VerticalID + "/" + item.ProcessId + "/"
                                                        + item.FinancialYear + "/" + item.ForPerid + "/"
                                                        + AuditID + "/" +
                                                        +item.ATBDId + "/1.0/" + ObjImageitem.ImageName + "' alt ='d'></img >" + "<br>");
                                                    }
                                                }
                                            }
                                            stringbuilderTOP.Append(@"</div></td></tr>");
                                        }
                                    }
                                }
                                stringbuilderTOP.Append(@"</table>");
                                ProcessRequest_AsPerClientFirst(stringbuilderTOP.ToString(), Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId);

                                #endregion

                                #region second report
                                stringbuilderTOPSecond.Append(@"<style>.break { page-break-before: always;}</style>");

                                stringbuilderTOPSecond.Append(@"<table style ='width:100%;font-family:Calibri;'>" +
                               "<tr>" +
                               "<td colspan ='4'>Report Genrated on " + DateTime.Today.Date.ToString("dd-MMM-yyyy") + " </td></tr>" +
                                "<tr><td style ='width:10%;'><b>Location :</b></td>" +
                                "<td style ='width:40%;text-align:left;' >" + CustomerBranchName + "</td>" +
                                "<td style ='width:10%;' ><b>Vertical :</b></td>" +
                                "<td style ='width:40%;text-align:left;'>" + VerticalID + "</td></tr></table>");


                                var RiskRatinglist = (from MCRR in entities.mst_Risk_ControlRating
                                                      where MCRR.IsRiskControl == "R"
                                                      select MCRR).OrderByDescending(entry => entry.Value).ToList();

                                if (RiskRatinglist.Count > 0)
                                {
                                    foreach (var item in RiskRatinglist)
                                    {
                                        var ItemDetails = itemlist.Where(a => a.RiskRating == item.Value).ToList();
                                        int rcnt = 1;
                                        if (ItemDetails.Count > 0)
                                        {
                                            //stringbuilderTOPSecond.Append(@"<style>.break { page-break-before: always;}</style>");
                                            stringbuilderTOPSecond.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>" +
                                            "<tr>" +
                                            "<td colspan ='5'><b>" + item.Name.Trim() + " Risk </b></td></tr>" +
                                            "<tr>" +
                                            "<th style='width:5%;border: 1px solid black;'><b>Sr No.</b></th>" +
                                            "<th style='width:30%;border: 1px solid black;'><b>Process/Sub Process</b></th>" +
                                            "<th style='width:35%;border: 1px solid black;'><b>Observation/Root Cause</b></th>" +
                                            "<th style='width:15%;border: 1px solid black;'><b>Location</b></th>" +
                                            "<th style='width:15%;border: 1px solid black;'><b>Action Plan</b></th>" +
                                            "</tr>");
                                            foreach (var cat in ItemDetails)
                                            {
                                                string ControlNo = string.Empty;
                                                string Observation = string.Empty;
                                                string ManagementResponse = string.Empty;
                                                DateTime TimeLine = new DateTime();
                                                string ProcessName = string.Empty;
                                                string SubProcessName = string.Empty;
                                                string ControlObjective = string.Empty;
                                                string ControlDescription = string.Empty;
                                                string RootCause = string.Empty;
                                                string Branchname = string.Empty;
                                                string RiskRating = string.Empty;
                                                string PSPCOCD = string.Empty;
                                                string ORC = string.Empty;
                                                string APT = string.Empty;

                                                if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlNo)))
                                                {
                                                    ControlNo = cat.ControlNo;
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(cat.Observation)))
                                                {
                                                    Observation = cat.Observation;
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(cat.ManagementResponse)))
                                                {
                                                    ManagementResponse = cat.ManagementResponse;

                                                }
                                                if (cat.TimeLine != null)
                                                {
                                                    TimeLine = Convert.ToDateTime(cat.TimeLine);
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(cat.ProcessName)))
                                                {
                                                    ProcessName = cat.ProcessName;
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(cat.SubProcessName)))
                                                {
                                                    SubProcessName = cat.SubProcessName;
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlObjective)))
                                                {
                                                    ControlObjective = cat.ControlObjective;
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlDescription)))
                                                {
                                                    ControlDescription = cat.ControlDescription;
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(cat.RootCost)))
                                                {
                                                    RootCause = cat.RootCost;
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(cat.Branch)))
                                                {
                                                    Branchname = cat.Branch;
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(cat.FinancialYear)))
                                                {
                                                    FinancialYear = Convert.ToString(cat.FinancialYear);
                                                }
                                                if (!string.IsNullOrEmpty(Convert.ToString(cat.RiskRatingName)))
                                                {
                                                    RiskRating = Convert.ToString(cat.RiskRatingName);
                                                }

                                                PSPCOCD = ProcessName + " /" + SubProcessName;

                                                ORC = "<b>Observation :</b> " + Observation + "\r\n" + "<b>Root Cause  :</b>" + RootCause + " \r\n";

                                                APT = "<b>Action Plan :</b> " + ManagementResponse + " \r\n" + "<b>Due Date  :</b>" + TimeLine.ToString("dd-MMM-yyyy") + " \r\n";

                                                string finalPSPCOCD = "<b>Process/Sub Process : </b>" + PSPCOCD.Trim() + " \r\n" +
                                                     "<b>Control Objective  :</b>" + ControlObjective.Trim() + " \r\n" +
                                                     "<b>Control Activity  :</b>" + ControlDescription.Trim() + " \r\n";

                                                string finalORC = "<b>Location : " + Branchname.Trim() + "</b> \r\n" + ORC.Trim();
                                                stringbuilderTOP.Append(@"<tr>" +
                                                "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + ControlNo.Trim() + "</td>" +
                                                "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalPSPCOCD.Trim() + "</td>" +
                                                "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalORC.Trim() + "</td>" +
                                                "<td style='width:20%;border: 1px solid black;vertical-align: top;'>" + APT.Trim() + "</td>" +
                                                "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + RiskRating.Trim() + "</td>" +
                                                "</tr>");

                                                PSPCOCD = string.Empty;
                                                ORC = string.Empty;
                                                APT = string.Empty;

                                                PSPCOCD = ProcessName + " /" + SubProcessName;
                                                ORC = "<b>Observation :</b> " + Observation + "\r\n" + "<b>Root Cause  :</b>" + RootCause + " \r\n";
                                                APT = ManagementResponse;
                                                stringbuilderTOPSecond.Append(@"<tr>" +
                                                "<td style='width:5%;border: 1px solid black;vertical-align: top;'>" + rcnt + "</td>" +
                                                "<td style='width:30%;border: 1px solid black;vertical-align: top;'>" + PSPCOCD.Trim() + "</td>" +
                                                "<td style='width:35%;border: 1px solid black;vertical-align: top;'>" + ORC.Trim() + "</td>" +
                                                "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + Branchname.Trim() + "</td>" +
                                                "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + ManagementResponse.Trim() + "</td>" +
                                                "</tr>");
                                                rcnt++;
                                            }//itemdetails foreach end
                                            stringbuilderTOPSecond.Append(@"</table>");
                                        }
                                    }//Riskrating details foreach end
                                }
                                ProcessRequest_AsPerClientSECOND(stringbuilderTOPSecond.ToString(), Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId);
                                #endregion
                            }
                            else
                            {
                                if (itemlist.Count > 0)
                                {
                                    #region first Report
                                    stringbuilderTOP.Append(@"<style>.break { page-break-before: always;}</style>");

                                    stringbuilderTOP.Append(@"<table style ='width:100%;font-family:Calibri;'>" +
                                   "<tr>" +
                                   "<td colspan ='4'>Report Genrated on " + DateTime.Today.Date.ToString("dd-MMM-yyyy") + " </td></tr>" +
                                    "<tr><td style ='width:10%;'><b>Location :</b></td>" +
                                    "<td style ='width:40%;text-align:left;' >" + CustomerBranchName + "</td>" +
                                    "<td style ='width:10%;' ><b>Vertical :</b></td>" +
                                    "<td style ='width:40%;text-align:left;'>" + VerticalID + "</td></tr></table>");

                                    stringbuilderTOP.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>" +
                                    "<tr>" +
                                    "<th style='width:15%;border: 1px solid black;'><b>Control No.</b></th>" +
                                    "<th style='width:25%;border: 1px solid black;'><b>Process/Sub Process/ Control Objective & Control Activity</b></th>" +
                                    "<th style='width:25%;border: 1px solid black;'><b>Observation/Root Cause</b></th>" +
                                    "<th style='width:20%;border: 1px solid black;'><b>Action Plan/Due Date</b></th>" +
                                    "<th style='width:15%;border: 1px solid black;'><b>Risk Rating</b></th>" +
                                    "</tr>");


                                    foreach (var cat in itemlist)
                                    {
                                        string ControlNo = string.Empty;
                                        string Observation = string.Empty;
                                        string ManagementResponse = string.Empty;
                                        DateTime TimeLine = new DateTime();
                                        string ProcessName = string.Empty;
                                        string SubProcessName = string.Empty;
                                        string ControlObjective = string.Empty;
                                        string ControlDescription = string.Empty;
                                        string RootCause = string.Empty;
                                        string Branchname = string.Empty;
                                        string RiskRating = string.Empty;
                                        string PSPCOCD = string.Empty;
                                        string ORC = string.Empty;
                                        string APT = string.Empty;

                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlNo)))
                                        {
                                            ControlNo = cat.ControlNo;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.Observation)))
                                        {
                                            Observation = cat.Observation;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ManagementResponse)))
                                        {
                                            ManagementResponse = cat.ManagementResponse;

                                        }
                                        if (cat.TimeLine != null)
                                        {
                                            TimeLine = Convert.ToDateTime(cat.TimeLine);
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ProcessName)))
                                        {
                                            ProcessName = cat.ProcessName;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.SubProcessName)))
                                        {
                                            SubProcessName = cat.SubProcessName;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlObjective)))
                                        {
                                            ControlObjective = cat.ControlObjective;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlDescription)))
                                        {
                                            ControlDescription = cat.ControlDescription;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.RootCost)))
                                        {
                                            RootCause = cat.RootCost;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.Branch)))
                                        {
                                            Branchname = cat.Branch;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.FinancialYear)))
                                        {
                                            FinancialYear = Convert.ToString(cat.FinancialYear);
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(cat.RiskRatingName)))
                                        {
                                            RiskRating = Convert.ToString(cat.RiskRatingName);
                                        }

                                        PSPCOCD = ProcessName + " /" + SubProcessName;

                                        ORC = "<b>Observation :</b> " + Observation + "\r\n" + "<b>Root Cause  :</b>" + RootCause + " \r\n";

                                        APT = "<b>Action Plan :</b> " + ManagementResponse + " \r\n" + "<b>Due Date  :</b>" + TimeLine.ToString("dd-MMM-yyyy") + " \r\n";

                                        string finalPSPCOCD = "<b>Process/Sub Process : </b>" + PSPCOCD.Trim() + " \r\n" +
                                             "<b>Control Objective  :</b>" + ControlObjective.Trim() + " \r\n" +
                                             "<b>Control Activity  :</b>" + ControlDescription.Trim() + " \r\n";

                                        string finalORC = "<b>Location : " + Branchname.Trim() + "</b> \r\n" + ORC.Trim();
                                        stringbuilderTOP.Append(@"<tr>" +
                                        "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + ControlNo.Trim() + "</td>" +
                                        "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalPSPCOCD.Trim() + "</td>" +
                                        "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalORC.Trim() + "</td>" +
                                        "<td style='width:20%;border: 1px solid black;vertical-align: top;'>" + APT.Trim() + "</td>" +
                                        "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + RiskRating.Trim() + "</td>" +
                                        "</tr>");
                                    }

                                    stringbuilderTOP.Append(@"</table>");
                                    ProcessRequest_AsPerClientFirst(stringbuilderTOP.ToString(), Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId);

                                    #endregion

                                    #region second report
                                    stringbuilderTOPSecond.Append(@"<style>.break { page-break-before: always;}</style>");

                                    stringbuilderTOPSecond.Append(@"<table style ='width:100%;font-family:Calibri;'>" +
                                   "<tr>" +
                                   "<td colspan ='4'>Report Genrated on1 " + DateTime.Today.Date.ToString("dd-MMM-yyyy") + " </td></tr>" +
                                    "<tr><td style ='width:10%;'><b>Location :</b></td>" +
                                    "<td style ='width:40%;text-align:left;' >" + CustomerBranchName + "</td>" +
                                    "<td style ='width:10%;' ><b>Vertical :</b></td>" +
                                    "<td style ='width:40%;text-align:left;'>" + VerticalID + "</td></tr></table>");


                                    var RiskRatinglist = (from MCRR in entities.mst_Risk_ControlRating
                                                          where MCRR.IsRiskControl == "R"
                                                          select MCRR).OrderByDescending(entry => entry.Value).ToList();

                                    if (RiskRatinglist.Count > 0)
                                    {
                                        foreach (var item in RiskRatinglist)
                                        {
                                            var ItemDetails = itemlist.Where(a => a.RiskRating == item.Value).ToList();
                                            int rcnt = 1;
                                            if (ItemDetails.Count > 0)
                                            {
                                                //stringbuilderTOPSecond.Append(@"<style>.break { page-break-before: always;}</style>");
                                                stringbuilderTOPSecond.Append(@"<table style='width:100%; font-family:Calibri; border-collapse: collapse; border: 1px solid black;'>" +
                                                "<tr>" +
                                                "<td colspan ='5'><b>" + item.Name.Trim() + " Risk </b></td></tr>" +
                                                "<tr>" +
                                                "<th style='width:5%;border: 1px solid black;'><b>Sr No.</b></th>" +
                                                "<th style='width:30%;border: 1px solid black;'><b>Process/Sub Process</b></th>" +
                                                "<th style='width:35%;border: 1px solid black;'><b>Observation/Root Cause</b></th>" +
                                                "<th style='width:15%;border: 1px solid black;'><b>Location</b></th>" +
                                                "<th style='width:15%;border: 1px solid black;'><b>Action Plan</b></th>" +
                                                "</tr>");
                                                foreach (var cat in ItemDetails)
                                                {
                                                    string ControlNo = string.Empty;
                                                    string Observation = string.Empty;
                                                    string ManagementResponse = string.Empty;
                                                    DateTime TimeLine = new DateTime();
                                                    string ProcessName = string.Empty;
                                                    string SubProcessName = string.Empty;
                                                    string ControlObjective = string.Empty;
                                                    string ControlDescription = string.Empty;
                                                    string RootCause = string.Empty;
                                                    string Branchname = string.Empty;
                                                    string RiskRating = string.Empty;
                                                    string PSPCOCD = string.Empty;
                                                    string ORC = string.Empty;
                                                    string APT = string.Empty;

                                                    if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlNo)))
                                                    {
                                                        ControlNo = cat.ControlNo;
                                                    }
                                                    if (!string.IsNullOrEmpty(Convert.ToString(cat.Observation)))
                                                    {
                                                        Observation = cat.Observation;
                                                    }
                                                    if (!string.IsNullOrEmpty(Convert.ToString(cat.ManagementResponse)))
                                                    {
                                                        ManagementResponse = cat.ManagementResponse;

                                                    }
                                                    if (cat.TimeLine != null)
                                                    {
                                                        TimeLine = Convert.ToDateTime(cat.TimeLine);
                                                    }
                                                    if (!string.IsNullOrEmpty(Convert.ToString(cat.ProcessName)))
                                                    {
                                                        ProcessName = cat.ProcessName;
                                                    }
                                                    if (!string.IsNullOrEmpty(Convert.ToString(cat.SubProcessName)))
                                                    {
                                                        SubProcessName = cat.SubProcessName;
                                                    }
                                                    if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlObjective)))
                                                    {
                                                        ControlObjective = cat.ControlObjective;
                                                    }
                                                    if (!string.IsNullOrEmpty(Convert.ToString(cat.ControlDescription)))
                                                    {
                                                        ControlDescription = cat.ControlDescription;
                                                    }
                                                    if (!string.IsNullOrEmpty(Convert.ToString(cat.RootCost)))
                                                    {
                                                        RootCause = cat.RootCost;
                                                    }
                                                    if (!string.IsNullOrEmpty(Convert.ToString(cat.Branch)))
                                                    {
                                                        Branchname = cat.Branch;
                                                    }
                                                    if (!string.IsNullOrEmpty(Convert.ToString(cat.FinancialYear)))
                                                    {
                                                        FinancialYear = Convert.ToString(cat.FinancialYear);
                                                    }
                                                    if (!string.IsNullOrEmpty(Convert.ToString(cat.RiskRatingName)))
                                                    {
                                                        RiskRating = Convert.ToString(cat.RiskRatingName);
                                                    }

                                                    PSPCOCD = ProcessName + " /" + SubProcessName;

                                                    ORC = "<b>Observation :</b> " + Observation + "\r\n" + "<b>Root Cause  :</b>" + RootCause + " \r\n";

                                                    APT = "<b>Action Plan :</b> " + ManagementResponse + " \r\n" + "<b>Due Date  :</b>" + TimeLine.ToString("dd-MMM-yyyy") + " \r\n";

                                                    string finalPSPCOCD = "<b>Process/Sub Process : </b>" + PSPCOCD.Trim() + " \r\n" +
                                                         "<b>Control Objective  :</b>" + ControlObjective.Trim() + " \r\n" +
                                                         "<b>Control Activity  :</b>" + ControlDescription.Trim() + " \r\n";

                                                    string finalORC = "<b>Location : " + Branchname.Trim() + "</b> \r\n" + ORC.Trim();
                                                    stringbuilderTOP.Append(@"<tr>" +
                                                    "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + ControlNo.Trim() + "</td>" +
                                                    "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalPSPCOCD.Trim() + "</td>" +
                                                    "<td style='width:25%;border: 1px solid black;vertical-align: top;'>" + finalORC.Trim() + "</td>" +
                                                    "<td style='width:20%;border: 1px solid black;vertical-align: top;'>" + APT.Trim() + "</td>" +
                                                    "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + RiskRating.Trim() + "</td>" +
                                                    "</tr>");

                                                    PSPCOCD = string.Empty;
                                                    ORC = string.Empty;
                                                    APT = string.Empty;

                                                    PSPCOCD = ProcessName + " /" + SubProcessName;
                                                    ORC = "<b>Observation :</b> " + Observation + "\r\n" + "<b>Root Cause  :</b>" + RootCause + " \r\n";
                                                    APT = ManagementResponse;
                                                    stringbuilderTOPSecond.Append(@"<tr>" +
                                                    "<td style='width:5%;border: 1px solid black;vertical-align: top;'>" + rcnt + "</td>" +
                                                    "<td style='width:30%;border: 1px solid black;vertical-align: top;'>" + PSPCOCD.Trim() + "</td>" +
                                                    "<td style='width:35%;border: 1px solid black;vertical-align: top;'>" + ORC.Trim() + "</td>" +
                                                    "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + Branchname.Trim() + "</td>" +
                                                    "<td style='width:15%;border: 1px solid black;vertical-align: top;'>" + ManagementResponse.Trim() + "</td>" +
                                                    "</tr>");
                                                    rcnt++;
                                                }//itemdetails foreach end
                                                stringbuilderTOPSecond.Append(@"</table>");
                                            }
                                        }//Riskrating details foreach end
                                    }
                                    ProcessRequest_AsPerClientSECOND(stringbuilderTOPSecond.ToString(), Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID), customerName, PeriodName, FinancialYear, VerticalID, CustomerBranchId);
                                    #endregion
                                }
                            }
                        }
                        #endregion


                        #region Excel Report
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            var itemlist = (from row in entities.SPGETObservationDetails_AsPerClient_Draft(CustomerBranchId, FinancialYear, VerticalID, PeriodName, Convert.ToInt32(AuditID))
                                            select row).ToList();
                            if (itemlist.Count > 0)
                            {

                                using (ExcelPackage exportPackge = new ExcelPackage())
                                {
                                    try
                                    {
                                        String FileName = String.Empty;
                                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Observation Report");
                                        DataTable ExcelData = null;
                                        DataView view = new System.Data.DataView((itemlist as List<SPGETObservationDetails_AsPerClient_Draft_Result>).ToDataTable());

                                        ExcelData = view.ToTable("Selected", false, "ProcessName", "SubProcessName", "Risk", "ControlObjective", "ControlDescription", "Observation",
                                        "RootCost", "ManagementResponse", "PersonResponsibleName", "TimeLine", "Branch");

                                        foreach (DataRow item in ExcelData.Rows)
                                        {
                                            if (item["TimeLine"] != null && item["TimeLine"] != DBNull.Value)
                                                item["TimeLine"] = Convert.ToDateTime(item["TimeLine"]).ToString("dd-MMM-yyyy");
                                        }

                                        FileName = "Observation Report";
                                        exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                                        exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                                        exWorkSheet.Cells["A2"].Value = customerName;
                                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                                        exWorkSheet.Cells["A3"].Value = FileName;
                                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["A3"].Style.Font.Size = 12;

                                        exWorkSheet.Cells["A4"].Value = "Location";
                                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;


                                        exWorkSheet.Cells["B4"].Value = CustomerBranchName;
                                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;

                                        exWorkSheet.Cells["A5"].Value = "Vertical";
                                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["A5"].Style.Font.Size = 12;

                                        exWorkSheet.Cells["B5"].Value = VerticalID;
                                        exWorkSheet.Cells["B5"].Style.Font.Size = 12;

                                        exWorkSheet.Cells["A7"].LoadFromDataTable(ExcelData, true);

                                        exWorkSheet.Cells["A7"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["A7"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["A7"].Value = "Process";
                                        exWorkSheet.Cells["A7"].AutoFitColumns(35);

                                        exWorkSheet.Cells["B7"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["B7"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["B7"].Value = "Sub Process";
                                        exWorkSheet.Cells["B7"].AutoFitColumns(25);

                                        exWorkSheet.Cells["C7"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["C7"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["C7"].Value = "Risk";
                                        exWorkSheet.Cells["C7"].AutoFitColumns(25);

                                        exWorkSheet.Cells["D7"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["D7"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["D7"].Value = "Control Objective";
                                        exWorkSheet.Cells["D7"].AutoFitColumns(50);

                                        exWorkSheet.Cells["E7"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["E7"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["E7"].Value = "Entity Control";
                                        exWorkSheet.Cells["E7"].AutoFitColumns(50);

                                        exWorkSheet.Cells["F7"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["F7"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["F7"].Value = "Observation";
                                        exWorkSheet.Cells["F7"].AutoFitColumns(30);

                                        exWorkSheet.Cells["G7"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["G7"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["G7"].Value = "Root Cause";
                                        exWorkSheet.Cells["G7"].AutoFitColumns(30);

                                        exWorkSheet.Cells["H7"].Value = "Management Action Plan";
                                        exWorkSheet.Cells["H7"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["H7"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["H7"].AutoFitColumns(50);

                                        exWorkSheet.Cells["I7"].Value = "Process Owner";
                                        exWorkSheet.Cells["I7"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["I7"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["I7"].AutoFitColumns(30);

                                        exWorkSheet.Cells["J7"].Value = "Due Date";
                                        exWorkSheet.Cells["J7"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["J7"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["J7"].AutoFitColumns(20);

                                        exWorkSheet.Cells["K7"].Value = "Location";
                                        exWorkSheet.Cells["K7"].Style.Font.Bold = true;
                                        exWorkSheet.Cells["K7"].Style.Font.Size = 12;
                                        exWorkSheet.Cells["K7"].AutoFitColumns(30);

                                        using (ExcelRange col = exWorkSheet.Cells[7, 1, 7 + ExcelData.Rows.Count, 11])
                                        {
                                            col.Style.Numberformat.Format = "dd-MMM-yyyy";
                                            col.Style.WrapText = true;
                                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                            // Assign borders
                                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        }

                                        Byte[] fileBytes = exportPackge.GetAsByteArray();

                                        #region Save Code
                                        if (string.IsNullOrEmpty(PeriodName))
                                        {
                                            var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                                            if (peridodetails != null)
                                            {
                                                if (peridodetails.Count > 0)
                                                {
                                                    foreach (var ForPeriodName in peridodetails)
                                                    {
                                                        string fileName = "Draft Observation Report" + FinancialYear + ForPeriodName + ".xlsx";
                                                        Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                                        {
                                                            CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                                            FinancialYear = Convert.ToString(FinancialYear),
                                                            Period = Convert.ToString(ForPeriodName),
                                                            CustomerId = CustomerId,
                                                            VerticalID = Convert.ToInt32(VerticalID),
                                                            FileName = fileName,
                                                            AuditID = AuditID,
                                                        };
                                                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                                        string directoryPath1 = "";
                                                        bool Success1 = false;
                                                        if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                                        {
                                                            directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + CustomerId + "/"
                                                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                                           + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                                           + "/1.0");
                                                           // Directory.Delete(directoryPath1);
                                                            if (!Directory.Exists(directoryPath1))
                                                            {
                                                                DocumentManagement.CreateDirectory(directoryPath1);
                                                            }
                                                            Guid fileKey1 = Guid.NewGuid();
                                                            string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                                            FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                            FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                                            FinalDeleverableUpload.Version = "1.0";
                                                            FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                                            FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                                            FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                                           Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string fileName = "Draft Observation Report" + FinancialYear + PeriodName + ".xlsx";
                                            Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                            {
                                                CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                                FinancialYear = Convert.ToString(FinancialYear),
                                                Period = Convert.ToString(PeriodName),
                                                CustomerId = CustomerId,
                                                VerticalID = Convert.ToInt32(VerticalID),
                                                FileName = fileName,
                                                AuditID = AuditID,
                                            };
                                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                            string directoryPath1 = "";

                                            bool Success1 = false;
                                            if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                            {
                                                directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + CustomerId + "/"
                                               + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                               + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                                               + "/1.0");
                                                //Directory.Delete(directoryPath1);
                                                if (!Directory.Exists(directoryPath1))
                                                {
                                                    DocumentManagement.CreateDirectory(directoryPath1);
                                                }

                                                Guid fileKey1 = Guid.NewGuid();
                                                string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                                FinalDeleverableUpload.FileName = fileName;
                                                FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                                FinalDeleverableUpload.Version = "1.0";
                                                FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                                FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                                FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                                 Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                            }
                                        }
                                        #endregion

                                        #region Excel Download Code
                                        //Response.ClearContent();
                                        //Response.Buffer = true;
                                        //string fileName = string.Empty;
                                        //fileName = "asd.xlsx";
                                        //Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                                        //Response.Charset = "";
                                        //Response.ContentType = "application/vnd.ms-excel";
                                        //StringWriter sw = new StringWriter();
                                        //Response.BinaryWrite(fileBytes);
                                        //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                        #endregion

                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                            }
                        }
                        #endregion

                        #region PowerPoint Report               
                        //using (AuditControlEntities entities = new AuditControlEntities())
                        //{
                        //    var details = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName)
                        //                   select row).ToList();


                        //    string BranchName = string.Empty;
                        //    if (details.Count > 0)
                        //    {
                        //        var ProcessList = GetProcessListOrdered(details);

                        //        float xpointsection1 = presentationdynamic.SlideSize.Size.Width / 2 - 350;
                        //        float xpointsection2 = presentationdynamic.SlideSize.Size.Width / 2;


                        //        String PPTName = "Draft Internal Audit Report - " + PeriodName;

                        //        string CustomerName = GetByID(customerID).Name;
                        //        string colorname = String.Empty;


                        //        colorname = "Chocolate";

                        //        CreateDymanicHeadingFirstSlide(SlideCounter, xpointsection1, 10, 700, 520, "", 32, "L", colorname);
                        //        CreateDymanicHeading(SlideCounter, xpointsection1, 80, 700, 100, CustomerName, 32, "L", colorname);
                        //        CreateDymanicHeading(SlideCounter, xpointsection1, 150, 700, 100, BranchName, 30, "L", colorname);
                        //        CreateDymanicHeading(SlideCounter, xpointsection1, 200, 700, 60, PPTName, 33, "L", colorname);
                        //        CreateDymanicHeading(SlideCounter, xpointsection1, 250, 700, 60, "Year- " + FinancialYear + "", 24, "L", colorname);
                        //        presentationdynamic.Slides.Append();
                        //        SlideCounter++;

                        //        PPTName = PPTName + " " + "(Year " + FinancialYear + ")";

                        //        CreateDymanicPara(SlideCounter, xpointsection1, 200, 700, 205, "Ignore – to be replaced by cover letter", 22);
                        //        presentationdynamic.Slides.Append();
                        //        SlideCounter++;

                        //        CreateDymanicHeading(SlideCounter, xpointsection1, 40, 700, 100, "Table of Content", 32, "L", colorname);
                        //        DynamicTableOfContent(SlideCounter, xpointsection1, 120, 700, 300, "", 24, colorname, ProcessList);
                        //        presentationdynamic.Slides.Append();
                        //        SlideCounter++;

                        //        CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 100, "Executive Summary", 32, "L", "White");
                        //        SetSlideColor(SlideCounter, colorname);
                        //        presentationdynamic.Slides.Append();
                        //        SlideCounter++;

                        //        CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 100, "Executive Summary", 32, "L", colorname);

                        //        //Add New Blank Slide 
                        //        presentationdynamic.Slides.Append();
                        //        SlideCounter++;

                        //        int ProcessID = -1;

                        //        int Count = 0;
                        //        int ProcessNumber = 0;
                        //        int ObservationNumber = 0;

                        //        String ObservationNo = string.Empty;

                        //        details.ForEach(entry =>
                        //        {
                        //            String SectionHeading = "Section " + intToRoman(Count + 2) + "";

                        //            ObservationNumber++;

                        //            if (ProcessID == -1 || ProcessID != entry.ProcessId)
                        //            {
                        //                if (Count < ProcessList.Count)
                        //                {
                        //                    ProcessID = Convert.ToInt32(entry.ProcessId);
                        //                    ProcessNumber++;
                        //                    ObservationNumber = 1;

                        //                    CreateDymanicHeading(SlideCounter, xpointsection1, 30, 700, 50, SectionHeading, 32, "L", "White");
                        //                    CreateDymanicHeading(SlideCounter, xpointsection1, 130, 700, 50, ProcessList.ElementAt(Count), 32, "L", "White");
                        //                    SetSlideColor(SlideCounter, colorname);
                        //                    presentationdynamic.Slides.Append();
                        //                    SlideCounter++;
                        //                    Count++;
                        //                }
                        //                else
                        //                {

                        //                }
                        //            }

                        //            int checkvalueobservation = 0;
                        //            int checkvalueRisk = 0;
                        //            int checkvalueRecomentdation = 0;
                        //            int checkvalueManagementresponse = 0;
                        //            int checkvalueTimeLine = 0;

                        //            string[] arrayobservation = entry.Observation.Split(' ');
                        //            int arrobservationLength = arrayobservation.Length;
                        //            checkvalueobservation = (2 * arrobservationLength);

                        //            string[] arrayrisk = entry.Risk.Split(' ');
                        //            int arrLengthrisk = arrayrisk.Length;
                        //            checkvalueRisk = (2 * arrLengthrisk);

                        //            string[] arrayRecommendations = entry.Recomendation.Split(' ');
                        //            int arrLengthRecommendations = arrayRecommendations.Length;
                        //            checkvalueRecomentdation = (2 * arrLengthRecommendations);

                        //            string[] arrayManagementResponse = entry.ManagementResponse.Split(' ');
                        //            int arrLengthManagementResponsek = arrayManagementResponse.Length;
                        //            checkvalueManagementresponse = (2 * arrLengthManagementResponsek);

                        //            string[] arrayPersonresponsible = entry.ManagementResponse.Split(' ');
                        //            int arrLengthPersonresponsible = arrayPersonresponsible.Length;
                        //            checkvalueTimeLine = (2 * arrLengthPersonresponsible);

                        //            //add footer
                        //            presentationdynamic.SetFooterText(PPTName);

                        //            //set the footer visible
                        //            presentationdynamic.SetFooterVisible(true);

                        //            //set the page number visible
                        //            presentationdynamic.SetSlideNoVisible(true);

                        //            //set the date visible
                        //            presentationdynamic.SetDateTimeVisible(true);

                        //            if (checkvalueobservation <= 200 && checkvalueRisk <= 200 && checkvalueRecomentdation <= 150
                        //                && checkvalueManagementresponse <= 150 && checkvalueTimeLine <= 100)
                        //            {
                        //                ObservationNo = ProcessNumber + "." + ObservationNumber + " ";
                        //                //Observation Title by default null
                        //                //PrintDynamicObservationSlideFix(SlideCounter, Regex.Replace(ObservationNo, @"\t|\n|\r", ""), Regex.Replace(entry.ObservationTitle, @"\t|\n|\r", ""), Regex.Replace(entry.Observation, @"\t|\n|\r", ""), Regex.Replace(entry.Risk, @"\t|\n|\r", ""), Regex.Replace(entry.Recomendation, @"\t|\n|\r", ""), Regex.Replace(entry.ManagementResponse, @"\t|\n|\r", ""), Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), entry.ObservationRating, colorname); /*entry.ObservationNumber*/
                        //                PrintDynamicObservationSlideFix(SlideCounter, Regex.Replace(ObservationNo, @"\t|\n|\r", ""), Regex.Replace(entry.ObservationTitle, @"\t|\n|\r", ""), Regex.Replace(entry.Observation, @"\t|\n|\r", ""), Regex.Replace(entry.Risk, @"\t|\n|\r", ""), Regex.Replace(entry.Recomendation, @"\t|\n|\r", ""), Regex.Replace(entry.ManagementResponse, @"\t|\n|\r", ""), Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), entry.ObservationRating, colorname); /*entry.ObservationNumber*/
                        //                presentationdynamic.Slides.Append();
                        //                SlideCounter++;
                        //            }
                        //            else
                        //            {
                        //                //Observation Title by default null
                        //                PrintDynamicObservationSlide(SlideCounter, Regex.Replace(entry.ObservationNumber, @"\t|\n|\r", "") , Regex.Replace(entry.ObservationTitle, @"\t|\n|\r", ""), Regex.Replace(entry.Observation, @"\t|\n|\r", ""), Regex.Replace(entry.Risk, @"\t|\n|\r", ""), Regex.Replace(entry.Recomendation, @"\t|\n|\r", ""), Regex.Replace(entry.ManagementResponse, @"\t|\n|\r", ""), Convert.ToDateTime(entry.TimeLine).ToString("dd/MM/yyyy"), colorname);
                        //                presentationdynamic.Slides.Append();
                        //                SlideCounter++;
                        //            }
                        //        });

                        //        CreateDymanicHeading(SlideCounter, xpointsection1 + 100, 200, 300, 100, "Thank You", 32, "L", "White");
                        //        SetSlideColor(SlideCounter, colorname);
                        //        presentationdynamic.Slides.Append();
                        //        SlideCounter++;

                        //        string dt = DateTime.Now.ToString("dd-MMM-yy");
                        //        if ((BranchName.Split(' ').First() + "-" + PPTName + "-" + dt).Length < 218)
                        //            PPTName = BranchName.Split(' ').FirstOrDefault() + "-" + PPTName + "-" + dt;

                        //        string path = Server.MapPath("~//PPTDownload//" + PPTName + ".pptx");

                        //        presentationdynamic.DocumentProperty["_MarkAsFinal"] = true;
                        //        presentationdynamic.SaveToFile(path, FileFormat.Pptx2010);


                        //        Byte[] fileBytes = presentationdynamic.GetBytes();
                        //        if (string.IsNullOrEmpty(PeriodName))
                        //        {
                        //            var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear);
                        //            if (peridodetails != null)
                        //            {
                        //                if (peridodetails.Count > 0)
                        //                {
                        //                    foreach (var ForPeriodName in peridodetails)
                        //                    {
                        //                        string fileName = "OpenObservationReport" + FinancialYear + ForPeriodName + ".pptx";
                        //                        Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                        //                        {
                        //                            CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        //                            FinancialYear = Convert.ToString(FinancialYear),
                        //                            Period = Convert.ToString(ForPeriodName),
                        //                            CustomerId = customerID,
                        //                            VerticalID = Convert.ToInt32(VerticalID),
                        //                            FileName = fileName,
                        //                        };
                        //                        List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                        //                        string directoryPath1 = "";
                        //                        bool Success1 = false;
                        //                        if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                        //                        {
                        //                            directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                        //                           + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                        //                           + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                        //                           + "/1.0");
                        //                            if (!Directory.Exists(directoryPath1))
                        //                            {
                        //                                DocumentManagement.CreateDirectory(directoryPath1);
                        //                            }
                        //                            Guid fileKey1 = Guid.NewGuid();
                        //                            string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        //                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        //                            FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        //                            FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        //                            FinalDeleverableUpload.Version = "1.0";
                        //                            FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        //                            FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        //                            FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        //                            DocumentManagement.SaveDocFiles(Filelist1);
                        //                            Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                        //                        }

                        //                    }
                        //                }
                        //            }
                        //        }
                        //        else
                        //        {
                        //            string fileName = "OpenObservationReport" + FinancialYear + PeriodName + ".pptx";
                        //            Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                        //            {
                        //                CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        //                FinancialYear = Convert.ToString(FinancialYear),
                        //                Period = Convert.ToString(PeriodName),
                        //                CustomerId = customerID,
                        //                VerticalID = Convert.ToInt32(VerticalID),
                        //                FileName = fileName,
                        //            };
                        //            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                        //            string directoryPath1 = "";

                        //            bool Success1 = false;
                        //            if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                        //            {
                        //                directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/" + customerID + "/"
                        //               + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                        //               + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                        //               + "/1.0");
                        //                if (!Directory.Exists(directoryPath1))
                        //                {
                        //                    DocumentManagement.CreateDirectory(directoryPath1);
                        //                }

                        //                Guid fileKey1 = Guid.NewGuid();
                        //                string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        //                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        //                FinalDeleverableUpload.FileName = fileName;
                        //                FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        //                FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        //                FinalDeleverableUpload.Version = "1.0";
                        //                FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        //                FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        //                FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        //                DocumentManagement.SaveDocFiles(Filelist1);
                        //                Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                        //            }

                        //        }
                        //    }
                        //    #region PPT Download Code
                        //    //Response.ContentType = "application/octet-stream";
                        //    //Response.AppendHeader("Content-Disposition", "attachment; filename=" + PPTName + ".pptx");
                        //    //Response.TransmitFile(path);
                        //    //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        //    //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        //    //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        //    #endregion
                        //}
                        #endregion


                        List<Tran_FinalDeliverableUpload> FinalDeliverableDocument = new List<Tran_FinalDeliverableUpload>();
                        if (FinalDeliverableDocument != null)
                        {
                            using (ZipFile AuditZip = new ZipFile())
                            {
                                if (!string.IsNullOrEmpty(PeriodName))
                                {
                                    FinalDeliverableDocument = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                                }
                                else
                                {
                                    FinalDeliverableDocument = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(AuditID).ToList();//.Where(entry => entry.Version.Equals("1.0")).ToList();
                                }
                                AuditZip.AddDirectoryByName(FinancialYear);
                                if (FinalDeliverableDocument.Count > 0)
                              //  if(1==1)
                                {
                                    int i = 0;
                                    foreach (var file in FinalDeliverableDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + "." + filename[1];                                           
                                            if (file.EnType == "M")
                                            {
                                                AuditZip.AddEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                AuditZip.AddEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    AuditZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=FinalDeliverableDocuments.zip");
                                    Response.BinaryWrite(Filedata);
                                    Response.Flush();
                                    //Response.End();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "There is no any file for download.";
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void ProcessRequest_AsPerClientFirst(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                //string path = Server.MapPath("~//Images//CLCLogo.png");
                //string path = "http://avacompcdr.cloudapp.net/LiveTestApp/Images/CLCLogo.png";
                //  string directoryPath1 = Server.MapPath("~/Images/" + customerID + "/"

                string path = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/CLCLogo.png";
                //string path = Server.MapPath("~/Images/CLCLogo.png");


                //string dpath = Server.MapPath("D:/Audit/4 Spet 2018/4 Sept 2018/Live Solution/ComplianceManagement.Portal/Images/CLCLogo.png");
                string context = contexttxt
                .Replace(Environment.NewLine, "<br />")
                .Replace("\r", "<br />")
                .Replace("\n", "<br />");

                string details = "Audit Report for the FY (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:right 3.0in right 6.0in;
                font-size:14.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.5in .5in .5in .5in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                font-family:Calibri;
                border: 1px solid black;
                outline: 4px groove; 
                outline-offset: 10px;
                border: solid black 2.25pt; padding: 24.0pt 24.0pt 24.0pt 24.0pt; 
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:right;font-family:Calibri;'><b>");
                sbTop.Append(CompanyName.Trim() + "<img src ='" + path + "' alt ='d'  ></img >" + "<br>" + details + "</b></p>");

                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "Consolidated Location Report" + FinancialYear + ForPeriodName + ".doc";
                                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,

                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    try
                                    {
                                        Directory.Delete(directoryPath1);
                                    }
                                    catch (Exception ex) { }
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                    FinalDeleverableUpload.Version = "1.0";
                                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "Consolidated Location Report" + FinancialYear + PeriodName + ".doc";
                    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                        AuditID = AuditID,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                    {
                        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        try
                        {
                            Directory.Delete(directoryPath1);
                        }
                        catch (Exception ex) { }
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        FinalDeleverableUpload.FileName = fileName;
                        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        FinalDeleverableUpload.Version = "1.0";
                        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                    }
                }
                #region Word Download Code
                //string abc = "ABC";
                //string NameVertical = "testrahul";
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + abc + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }
        public void ProcessRequest_AsPerClientSECOND(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                string context = contexttxt
                .Replace(Environment.NewLine, "<br />")
                .Replace("\r", "<br />")
                .Replace("\n", "<br />");

                string details = "Audit Report for the FY (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.5in .5in .5in .5in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Calibri;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left;font-family:Calibri;'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "Audit Committee Report" + FinancialYear + ForPeriodName + ".doc";
                                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,
                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    //try { 
                                    //Directory.Delete(directoryPath1);
                                    //}
                                    //catch (Exception ex) { }
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                    FinalDeleverableUpload.Version = "1.0";
                                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "Audit Committee Report" + FinancialYear + PeriodName + ".doc";
                    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                        AuditID = AuditID,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                    {
                        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        FinalDeleverableUpload.FileName = fileName;
                        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        FinalDeleverableUpload.Version = "1.0";
                        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                    }
                }
                #region Word Download Code
                //string abc = "ABC";
                //string NameVertical = "testrahul";
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + abc + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }
        public void ProcessRequest(string context, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                string details = "Audit Report for the (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Calibri;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "OpenObservationReport" + FinancialYear + ForPeriodName + ".doc";
                                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,
                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                    FinalDeleverableUpload.Version = "1.0";
                                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "OpenObservationReport" + FinancialYear + PeriodName + ".doc";
                    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                        AuditID = AuditID,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                    {
                        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        FinalDeleverableUpload.FileName = fileName;
                        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        FinalDeleverableUpload.Version = "1.0";
                        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                    }
                }
                #region Word Download Code
                //string NameVertical = VerticalName.VerticalName;
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + companyname + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }


        protected void grdImplementationAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    //int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                    //string ForMonth = Convert.ToString(commandArgs[1]);
                    //string FinancialYear = Convert.ToString(commandArgs[2]);
                    //int Verticalid = Convert.ToInt32(commandArgs[3]);
                    long AuditID = Convert.ToInt64(commandArgs[4]);
                   
                        if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                        {
                            Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString() + "&AuditID=" + AuditID, false);
                        }
                        else
                        {
                            Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=" + "&AuditID=" + AuditID, false);
                        }
                   // }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void ShowImplementationGrid(object sender, EventArgs e)
        {
            liImplementation.Attributes.Add("class", "active");
            liProcess.Attributes.Add("class", "");
            ViewState["Type"] = "Implementation";

            BindData();
            int chknumber = Convert.ToInt32(grdProcessAudits.PageIndex);

            if (chknumber > 0)
            {
                chknumber = chknumber + 1;
                DropDownListPageNo.SelectedValue = (chknumber).ToString();
            }
        }

        protected void ShowProcessGrid(object sender, EventArgs e)
        {
            liProcess.Attributes.Add("class", "active");
            liImplementation.Attributes.Add("class", "");
            ViewState["Type"] = "Process";

            BindData();

            int chknumber = Convert.ToInt32(grdProcessAudits.PageIndex);

            if (chknumber > 0)
            {
                chknumber = chknumber + 1;
                DropDownListPageNo.SelectedValue = (chknumber).ToString();
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}