﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EndUserLicenseAgreement1.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.EndUserLicenseAgreement1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .header-style {
            text-align: center;
            font-weight: bold;
            font-size: 11pt;
        }

        .setFont-name {
            font-family: Tahoma;
        }

        .setFont-size {
            font-size: 9pt;
        }

        .setCircleBulletMargin {
            margin-top: 3px;
            display: flow-root;
            margin-bottom: 6px;
        }

        .setSquareBulletList {
            list-style-type: square;
            margin-left: 35px;
        }

        .setCircleBulletElementMargin {
            position: relative;
            left: 20px;
        }

        .setSquareBulletElementMargin {
            position: relative;
            left: 20px;
        }

        .topicContentMargin {
            margin-left: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <table class="auto-style1 setFont-name">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="header-style">END USER LICENSE AGREEMENT</td>
                </tr>
                <tr>
                    <td colspan="2" class="header-style">FOR THE USE AND LICENSE OF AVANTIS SOFTWARE (EULA)</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="setFont-size">
                        <p>This <b>END USER LICENSE AGREEMENT (EULA)</b> contains the general terms and conditions for the use and License of Avantis Software between the Practicing Professional / Firm <b>(“Client/You”)</b> and Avantis RegTech Private Limited, a company incorporated under the laws of India and having its registered office at 9th floor, Amar Avinash Corporate Plaza, 11 Bund Garden Road, Near Inox Theatre, Ghorpadi, Pune 411001 <b>(“ARPL”)</b>, which lays out the License Terms, Tenure, Payment Terms, if any, and other commercial terms for the license, access and use of the Software (defined below) and/or services provided by ARPL.</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="setFont-size">
                        <p>Both ARPL and the Client shall be collectively referred to as “Parties” and individually as a “Party”. The EULA and the Engagement Letter, if any, shall be collectively referred to as the “Agreement” and shall comprise the entire understanding between the Parties with respect to the License of the Software and shall supersede all other previous documents or other correspondence related to this subject.</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="setFont-size">
                        <p>For the purpose of this Agreement, "Software" means, collectively: (i) Avantis’ Integrated GRC product; (ii) all the modes of media through which the Software is provided, including the object code form of the software delivered via Web page; (iii) digital images, stock photographs, clip art, designs or other artistic works; (iv) Source codes, html files and database; (v) Legal /  compliance checklists and updates or other content embedded in the Software (save and except the data entered into by the Client), related explanatory written materials, help manuals, version upgrade lists and/ or any other possible documentation related thereto ('Documentation'), and (vi) upgrades, modified, modified versions, updates, additions, and copies of the Software, if any, licensed to Client under this Agreement.</p>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size"><b><span>1.</span><span style="margin-left: 10px;"></span>LICENSE OF THE SOFTWARE:</b></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="setFont-size">
                        <div class="topicContentMargin">Subject to You complying with all terms of this Agreement and Engagement Letter, if any, executed by You, ARPL  hereby grants to You a revocable, non-transferable, non-exclusive and limited right to access, use, or run the Software, during the tenure and for such number of entities, locations, users and/or such other terms as mentioned below or as mentioned in the Engagement Letter signed with ARPL for your personal or business purposes, if any:</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div class="topicContentMargin"><b>Key License Terms:</b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div class="setFont-size" style="margin-left: 2px;">
                            <ol type="i" style="margin-top: -4px;">
                                <li><span class="setCircleBulletElementMargin"><b>Products:</b></span><div>
                                    <ul class="setCircleBulletMargin">
                                        <li><span class="setCircleBulletElementMargin">RuleZbook (Mobile App for Legal Updates) </span></li>
                                        <li><span class="setCircleBulletElementMargin">Integrated GRC Product covering the following Modules:</span> </li>
                                    </ul>
                                    <ul class="setSquareBulletList">
                                        <li><span class="setSquareBulletElementMargin">Audit Management</span></li>
                                        <li><span class="setSquareBulletElementMargin">Board & Secretarial Compliance Module</span></li>
                                        <li><span class="setSquareBulletElementMargin">Legal Compliance and Legal Audit Module</span></li>
                                        <li><span class="setSquareBulletElementMargin">Payroll & Labour Compliance Automation Module</span></li>
                                        <li><span class="setSquareBulletElementMargin">Practice Management Module</span></li>
                                    </ul>
                                </div>
                                </li>
                                <li><span class="setCircleBulletElementMargin"><b>Pricing:</b> FREE (subject to below Terms)</span></li>
                                <li><span class="setCircleBulletElementMargin"><b>No of Legal Entities:</b> 25 Legal Entities (Private Companies)</span></li>
                                <li><span class="setCircleBulletElementMargin"><b>No. of Firm Users:</b> 05 Users</span></li>
                                <li><span class="setCircleBulletElementMargin"><b>No of Customer Users:</b> Unlimited</span></li>
                                <li><span class="setCircleBulletElementMargin"><b>Employees:</b> Up to 25 Employees (applicable for Payroll and Labour Compliance Product)</span></li>
                                <li><span class="setCircleBulletElementMargin"><b>Storage Space:</b> Unlimited Storage Space</span></li>

                            </ol>
                            <div style="margin-left: 10px;" class="setFont-size">
                                <p><b>License Term:</b> Software is free for all Practicing Chartered Accountants, having a valid Certificate of Practice, till July 2023. </p>
                            </div>
                            <div style="margin-left: 10px;" class="setFont-size">
                                <p><b>Rejection / Suspension/ Termination:</b> ARPL shall be free to reject / suspend / terminate the subscription of the Software or the Services, if, in the sole opinion of ARPL, there is an actual or anticipated breach of the terms of this EULA by the Client. The decision of ARPL to accept, reject, suspend or terminate any Client shall be the sole prerogative of ARPL.</p>
                            </div>
                        </div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size"><b><span>2.</span><span style="margin-left: 5px;">SERVICES:</span></b></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">Subject to the timely payment of Fee by the Client, as applicable, ARPL shall provide its support and services, to the best of its ability, in relation to the efficient performance of the Software as per the terms of the Engagement Letter</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>3.</span><span style="margin-left: 5px;">FEE FOR ADDITIONAL LICENSE / SERVICES:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">During the Term of this License, if the Client wishes to buy more users, companies, modules or any other Software or Services by ARPL or  post expiry of the Term, the Client extends this license, then the Client may engage with ARPL on such commercial terms as may be mutually agreed between the said Client and Avantis. In such case, You agree to pay to ARPL, without any delay, demur or set-off, all amounts due, as specified in the Engagement Letter signed by you with ARPL, as applicable, including payment of all fees including One-Time Setup Fee, Monthly Subscription Fee, Annual Maintenance Charges or other Fees as applicable to You and as mentioned in the Engagement Letter, by whatever name called ("Fee"), The Invoice for the Fee shall be generated at such frequency as agreed in the Engagement Letter, and the payment for the same shall be made by the due date as mentioned therein. Any payment received beyond the due date will attract such interest as mentioned in the Engagement Letter. It may also be noted that ARPL is registered under the Micro, Small and Medium Enterprises (MSME) Development Act 2006 vide UAM No MH26D0111390. Thus, as per the provisions of Section 15 and 16 of the MSME Act 2006, for invoices not paid within 45 days from the date of the invoice, the Client shall be liable to pay compound interest with monthly rests to ARPL on the outstanding amount at 3 times of the bank rate notified by the Reserve Bank.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>4.</span><span style="margin-left: 5px;">RESTRICTIONS ON THE USE OF THE SOFTWARE:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">Except to the extent allowed, you shall not directly or indirectly: (i) assign, market, sell, lease, redistribute or transfer any of the Software; (ii) modify, translate, reverse engineer, decompile, disassemble, create derivative works based on, sublicense, or distribute any of the Software; (iii) rent or lease any rights in any of the Software in any form to any person; (iv) License / Sub-License the Software to any third parties; (v) remove, alter or obscure any proprietary or copyright notice, labels, or marks on the Software; or (vi) access the Software(s) in order to build a competitive product or build a product using similar ideas, features, expressions, functions or graphics of the Software(s) and copy the ideas, features, functions or graphics of the Software(s). </div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>5.</span><span style="margin-left: 5px;">INTELLECTUAL PROPERTY:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">Unless otherwise expressly stated herein, ESLC shall have sole and exclusive intellectual property rights over the Software including the Legal / compliance checklists, updates or other content embedded in the Software. This Agreement does not transfer to you any title or any ownership right or interest in any Software or in any other Intellectual Property Rights of ARPL in any ARPL’s Software. You acknowledge that the Software is owned by ARPL and that the Software contains, embodies and is based upon patented or patentable inventions, trade secrets, copyrights and other Intellectual Property Rights owned by ARPL. You understand and acknowledge that you are provided with a license to use this software subject to the terms and conditions of this Agreement.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>6.</span><span style="margin-left: 5px;">CONFIDENTIALITY: </span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin" style="margin-top: -12px;">
                            <p>As used herein, "Confidential Information" means and includes any/all information of either Party regardless of being identified as Confidential or not   or by any such legend or not, whether, written, oral or recorded on any other media as well as all computer software developed by Parties including any Documentation, Source Code and Object Code of the Software disclosed hereunder or any non-public technical or business information of either Party , including without limitation, any information which related to the business, technology, Client Material, products marketing, financial or other activities of the Parties, whether it is received, accessed or viewed by other Party in writing, visually, electronically or orally. Further, Confidential Information shall also include any and all technical and non-technical information including but not limited to information to employees of the either Party, various modules, presentations, Intellectual Property including any patent, copyright, trademarks or  trade secret, and proprietary information, relating to either Party’s techniques, algorithms, sketches, drawings, models, Software, know-how, current and future products and services, research, engineering, designs, financial information, procurement requirements, manufacturing, Client lists, customer lists, sub-contractors, joint ventures, business forecasts, marketing plans, business plans, licensing techniques, work in progress, software source documents, website and its details  and information, the terms and conditions of this Agreement, and any other information of either Party that is disclosed to other Party pursuant to this Agreement. Additionally, Confidential Information shall also include the confidential or proprietary information of any third party that is disclosed by the Client or ARPL to the other Party pursuant to this Agreement.</p>
                            <p>ARPL also acknowledges that data entered into or uploaded by the Client in the Software (“Client Material”) uploaded on Software and/or on Cloud Sever shall be sole property of the Client and ARPL shall not access, store and/or download Clients’ Data, except for the purpose of providing services or support under this Agreement and shall at all times be responsible for safekeeping and return of the same to the Client.  ARPL shall be solely responsible for compliance all applicable laws related to storage of Clients’ Materials on external server including Cloud.</p>
                            <p>The Parties hereby acknowledge and agree that each Party, shall treat the Confidential Information received under this Agreement under strict confidence and shall not publish, disclose or disseminate the same and treat Confidential Information with reasonable degree of care to avoid disclosure to any third party and will limit the disclosure of Confidential Information to its employees with a bona fide need to access such Confidential Information. The Parties hereby agree that breach of this Clause by either Party may cause irreparable harm to the non-defaulting Party and that monetary damages will be inadequate to compensate the non-defaulting Party for such breach. In the event of a breach or threatened breach of this clause, the non-defaulting Party, in addition to and not in limitation of any other rights, remedies or damages available to it at law or in equity, shall be entitled to a temporary restraining order, preliminary injunction and/or permanent injunction in order to prevent or to restrain any such breach. Provided however, the foregoing restrictions on the disclosure and use of Confidential Information shall not apply to the extent that;</p>
                            <ol type="i" style="margin-top: -4px;">
                                <li><span class="setCircleBulletElementMargin">Such Confidential Information became generally available to the public other than as a result of unauthorized disclosures; or</span>
                                </li>
                                <li><span class="setCircleBulletElementMargin">Such Confidential Information was received by the recipient on a non-confidential basis from a third Party; or</span></li>
                                <li><span class="setCircleBulletElementMargin">Such Confidential Information is required to be disclosed to  any Government Authority or judicial order or direction, in such case, to the extent legally permitted and reasonably practicable, Party onto whom the requisition is raised, shall provide the other party prior written notice of such disclosures required to be made and reasonably cooperate with such party in obtaining confidential treatment in respect of the Confidential Information required to be disclosed.)</span></li>
                            </ol>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>7.</span><span style="margin-left: 5px;">INSTALLATION AND CONFIGURATION: </span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">ARPL represents, warrants and covenants that it is responsible for the proper configuration, updates and management of the Software, except administrator role and rights in the Software which is the responsibility of the Client. You represent and warrant to adhere strictly to the recommended reasonable and minimum requirements specified from time to time by ARPL in the Documentation. ARPL shall have no obligations under this Agreement to the extent the Software(s) fails to substantially perform the functions described in the Documentation, in whole or in part, because (i) you fail to meet ARPL’s  minimum requirements, (ii) your separate hardware fails to perform (iii) you have made any unilateral changes in any functionality of Software without knowledge and authority from ARPL.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>8.</span><span style="margin-left: 5px;">LIMITED WARRANTY:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">Except as expressly set forth in this Agreement, to the maximum extent permitted by applicable law, Parties disclaim any and all other promises, representations and warranties, whether express or implied. ARPL does not warrant that the Software(s) will meet Client's requirements or the operation of the Software(s) will be uninterrupted or error-free, however ARPL will make best endeavors to correct all errors and restore the services of Software within a reasonable time, after the same is communicated by the Client in writing. The Client shall, as per the advice of ARPL, ensure that Clients’ hardware is compatible with the Software in order to avoid any data loss, damage or security threat etc. To the best of AROL’s  knowledge, all third party technology or products used in developing the Software has been licensed/ validly procured from such third party and in the  event of any claims with respect to Software arising out of third party due to any reason whatsoever, ARPL  shall remedy the same at its own cost.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>9.</span><span style="margin-left: 5px;">LEGAL UPDATES:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">ARPL shall map in the Software, certain laws and Compliances and shall provide legal updates to the Client, with respect to amendments in such applicable laws. Without prejudice to the generality of the foregoing, the said Compliance checklist, prepared by ARPL is exclusively for the benefit and internal use of the Client for Compliance monitoring through the Software. The acceptance, rejection or applicability of any updates in compliance shall be the sole responsibility of the Client. The legal updates will be based on notifications issued by the Government in the E-Gazette or as published on relevant government websites. ARPL shall not provide Bare Acts or substantive texts of the regulations. The Compliance checklists mapped in the Software shall not be used by the Client for any other purpose, except as stated above, and does not carry any right of publication or disclosure to any third party (including the Client’s of the Client), without the prior written consent of ARPL’s. Client further acknowledges and understands that the Software is primarily used for legal compliance monitoring and reporting purposes hence ARPL in no given circumstance be responsible for any Client’s legal non-compliance or for any failure or delay to update any laws or checklist of compliances.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>10.</span><span style="margin-left: 5px;">LIMITATION OF LIABILITY:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <ol type="i" style="margin-top: -4px;">
                            <li><span class="setCircleBulletElementMargin">Except for cases of willful misconduct, ARPL or its directors, officers or employees shall not be liable to the Client or any third party in relation to the Client’s usage of the Software.</span>
                            </li>
                            <li><span class="setCircleBulletElementMargin">Under no circumstances will either party or any director, officer, employee, of the party be liable to the other party for any indirect, incidental, consequential, punitive, special or other similar damages arising under this agreement or the arrangements contemplated, under tort, common law or under public policy, even if the other party has been advised of the possibility of such damages or losses, resulting in loss of revenue, loss of goodwill or anticipated profits or lost business and be liable for any loss of data or any interruption of any property due to any cause.</span></li>
                            <li><span class="setCircleBulletElementMargin">Notwithstanding anything contained in this agreement ARPL’s aggregate liability to the Client under this Agreement shall not exceed the amounts paid by the Client under this Agreement over the last 12 months.</span></li>
                        </ol>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>11.</span><span style="margin-left: 5px;">LEGAL COMPLIANCE:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">Without prejudice to generality of provisions contained herein and subject to Parties complying their respective obligations under this Agreement, Parties agree to comply with, all applicable laws, statutes, ordinances, regulations and other types of government authority (including without limitation the laws and regulations governing export control, unfair competition, anti-discrimination, false advertising, privacy and data protection, and publicity) to their business operation and in no event shall any party be responsible for non-compliance of other party in relation to others’ business operation.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>12.</span><span style="margin-left: 5px;">GOVERNING LAW AND DISPUTE RESOLUTION:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">This Agreement shall be governed in all respects by the laws of India and in case of any disputes, the Courts in Pune, Maharashtra, India shall have exclusive jurisdiction.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>13.</span><span style="margin-left: 5px;">TERMINATION:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin" style="margin-top: -12px;">
                            <p>Termination for Non-Payment: ARPL  shall be entitled to terminate the Agreement in case Client fails to make any payment in full within 30 days from the receipt of the invoice which is not disputed by You provided prior to Termination Notice a written curing notice of 15 (fifteen) days shall be provided to the Client thereby calling upon Client to make the payment of outstanding. In the case Client fails or refuses to make the payment, within Curing period and/or Termination Notice, this Agreement shall come to end at expiry of Termination Notice and Upon such termination ARPL shall have no obligation to refund any fees paid in advance by You.</p>
                            <p>Termination for breach: ARPL  may also suspend / terminate this Agreement immediately in case Client has (i) breached any material terms or conditions of this agreement, including License Terms, Confidential Information, Intellectual Property Rights, Non-Compete and Non-Solicitation; or (ii) has gone into liquidation other than a voluntary liquidation for the purpose of reconstruction or amalgamation or shall commit an act of bankruptcy or shall compound with its creditors generally, or if a receiver, Insolvency professional  or judicial manager shall be appointed over the whole or a substantial part of the assets pursuant to the Liquidation Order of the Adjudicating Authority. Provided that prior to Termination Notice, a written curing notice of 15 (fifteen) days shall be provided to the other party thereby calling upon the other party to make good the breach.  In the case the other party fails or refuses to make good the breach, within the said Curing period of 15 days and/or Termination Notice, this Agreement shall come to end at expiry of Termination Notice.</p>
                            <p>Termination without cause: Any party may terminate this Agreement, by addressing a Termination notice of 90 (Ninety) days to Other Party and upon the expiry of such Termination Notice, this Agreement shall stand terminated. </p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>14.</span><span style="margin-left: 5px;">POST TERMINATION COVENANTS:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin" style="margin-top: -12px;">
                            <p>Upon the termination of this Agreement the Parties shall (i) promptly return to other Party any information disclosed to it in any tangible form, and all copies thereof (on whatever physical, electronic or other media such information may be stored) containing any of the Confidential Information; if such Confidential Information is stored in electronic form, it is to be immediately deleted; and (ii) provide to the other Party  a certification, in writing, executed by an authorized representative of the Party, that the Party has complied with confidentiality obligation under this Agreement, that it has retained no copies of the Confidential Information on any media and that it has retained no notes or other embodiments of the information contained in the Confidential Information. The confidentiality obligation set forth under this Agreement shall survive the termination of this Agreement and shall continue for a period of two (2) years from the date of termination of this Agreement.</p>
                            <p>Without limiting the generality of the foregoing, in case of termination by either party, ARPL shall uninstall the Software from client’s servers / discontinue the cloud subscription from the date of termination of the Agreement and the Client shall immediately clear all outstanding invoices raised by ARPL for the period of use of the Software until the date of Termination. Subject to the Client clearing all outstanding invoices, if any, before deletion of the Client Material from the Servers, ARPL shall ensure that adequate data back-ups of the Client Material are provided to the Client in excel along with all the documents as uploaded by the Client during the usage of the software, without any additional cost to the Client. In case the Client fails to receive the Client Material after due notification by ARPL, then ARPL shall be at a liberty to delete the Client Material upon the expiry of 90 days from the date of termination of this Agreement. However, ARPL shall be eligible to retain user data, including certain masters etc. in dormant form, for such time as it deems fit, as a proof of having rendered the Software / Services to the Clients.</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>15.</span><span style="margin-left: 5px;">SEVERABILITY:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">If any provision of this Agreement is held to be illegal or unenforceable for any reason, then such provision shall be deemed to be restated so as to be enforceable to the maximum extent permissible under law, and the remainder of this Agreement shall remain in full force and effect. </div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>16.</span><span style="margin-left: 5px;">FORCE MAJEURE:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">Neither Party shall be liable for any delay or failure due to a force majeure event and other causes beyond its reasonable control and the time for performance so affected or delayed will be deemed extended for the period of such delay, provided the Party claiming excuse for failure to perform the obligations under the Agreement due to such Force Majeure Event shall forthwith notify the other Party, of the same in writing, fully detailing the background to, and all relevant matters connected with, such Force Majeure Event, together with such evidence thereof that it can reasonably give and specifying the period for which such prevention or delay can reasonably be expected to continue. However, non-payment of Fee by the Client shall not be treated as a Force Majeure instance under any circumstances.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>17.</span><span style="margin-left: 5px;">ASSIGNMENT:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">This Agreement and the rights and obligations hereunder are personal and are not assignable by the Client and the Client shall not transfer any rights or interest in the Software with any other person/ entity, without prior consent of ARPL.This Agreement shall be binding upon and inure to the benefit of the Parties' successors and permitted assigns.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>18.</span><span style="margin-left: 5px;">NON-COMPETE:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">You hereby agree not to copy/re-create the software/or the platform of Avantis while using the same during the course of this Agreement or at the time of demonstration by ARPL. Provided that this restriction shall continue for a period of 2 years post termination of this Agreement.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>19.</span><span style="margin-left: 5px;">NON-SOLICITATION:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                       <div class="topicContentMargin" style="margin-top: -12px;">
                            <p>You shall not, directly or indirectly, solicit the employment of any of our partners, members, directors or employees, as the case may be, involved in performing the Services while the Services are being performed or for a period of 1 (one) year following their completion or following termination of the Agreement, without our prior written consent. This prohibition shall not prevent you at any time from running recruitment advertising campaigns nor from offering employment to any of our partners, members, directors or employees, as the case may be, who are not involved in rendering services to the Client and who may respond to any such campaign.</p>
                            <p>ARPL agrees not to solicit the customers of Clients for any competing business, other than for the purpose of this Software.</p>
                        </div>
                    </td>
                </tr>
                 <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>20.</span><span style="margin-left: 5px;">AMENDMENTS:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">This EULA may be amended by ARPL from time to time and You will be notified via Email / Web-interface and a copy of the updated and amended EULA will be available during the Product Login.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>21.</span><span style="margin-left: 5px;">USERS:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">Where the users of the Software comprise of more than one individual, each of such individuals shall be jointly and severally liable to observe and perform the Client’s obligations under this Agreement.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>22.</span><span style="margin-left: 5px;">CONFLICT OF PROVISIONS:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">In case of conflict between the provisions contained under the Engagement Letter or similar agreement and this EULA, the terms of the Engagement Letter shall prevail over EULA.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size">
                        <div style="margin-top: 10px;"><b><span>23.</span><span style="margin-left: 5px;">SURVIVAL:</span></b></div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div class="topicContentMargin">The provisions of clauses Intellectual Property (Clause 5), Confidentiality (Clause 6), Limited Warranty (Clause 8), Limitation of Liability (Clause 10), Governing Law (Clause 12), Non-Compete (Clause 18) and Non-Solicitation (Clause 19) shall survive the termination of this Agreement by 2 years.</div>
                    </td>
                </tr>
                <tr>
                    <td class="setFont-size" colspan="2">
                        <div style="margin-top: 10px;">INDEPENDENT CONTRACTOR: The Parties herein are independent contractors and shall have no authority to assume or create any obligation whatsoever express or implied, in the name of the other Party or to bind the other Party in any way or manner.</div>
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
