﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class ClientAuditReportSecondPart : System.Web.UI.Page
    {
        int Statusid;
        protected List<Int32> roles;
        public static List<long> Branchlist = new List<long>();
        protected int CustomerId = 0;
        public static string DocumentPath = "";
        protected bool DepartmentHead = false;
        protected string AuditHeadOrManagerReport;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                //CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindCustomerList();
                BindFinancialYear();
                BindLegalEntityData();
                BindVertical();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    if (Request.QueryString["Status"] == "Open")
                    {
                        ViewState["Status"] = 1;
                    }
                    else if (Request.QueryString["Status"] == "Closed")
                    {
                        ViewState["Status"] = 3;
                    }
                    else
                    {
                        ViewState["Status"] = 3;
                    }
                }
                else
                {
                    ViewState["Status"] = 3;
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    if (Request.QueryString["Type"] == "Process")
                    {
                        ShowProcessGrid(sender, e);
                    }
                    else if (Request.QueryString["Type"] == "Implementation")
                    {
                        ShowImplementationGrid(sender, e);
                    }
                    else
                    {
                        ShowProcessGrid(sender, e);
                    }
                }
                else
                {
                    ShowProcessGrid(sender, e);
                }
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
            {
                if (ViewState["Type"].ToString() == "Process")
                {
                    grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdProcessAudits.PageIndex = chkSelectedPage - 1;
                }
                else if (ViewState["Type"].ToString() == "Implementation")
                {
                    grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdImplementationAudits.PageIndex = chkSelectedPage - 1;
                }
            }
            BindData();
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
        }

        public void BindLegalEntityData()
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            else if (DepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            //ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        if (ddlCustomer.SelectedValue != "-1")
                        {
                            CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                            branchid = CustomerId;
                        }
                    }
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                //ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindData()
        {
            try
            {
                int branchid = -1;
                string FinYear = String.Empty;
                string Period = String.Empty;
                string Flag = String.Empty;
                int VerticalID = -1;
                string departmentheadFlag = string.Empty;
                if (DepartmentHead)
                {
                    departmentheadFlag = "DH";
                }
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ViewState["Status"].ToString()))
                {
                    Statusid = Convert.ToInt32(ViewState["Status"]);
                }
                if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
                {
                    Flag = Convert.ToString(ViewState["Type"]);
                }
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(CustomerId);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }

                if (FinYear == "")
                    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                Branchlist.Clear();
                GetAllHierarchy(CustomerId, branchid);
                Branchlist.ToList();

                if (Flag == "Process")
                {
                    grdProcessAudits.DataSource = null;
                    grdProcessAudits.DataBind();

                    grdImplementationAudits.Visible = false;
                    grdProcessAudits.Visible = true;

                    var AuditLists = DashboardManagementRisk.GetAuditManagerAudits(FinYear, Period, CustomerId, Branchlist, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalID, 4, departmentheadFlag);
                    grdProcessAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdProcessAudits.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }
                else if (Flag == "Implementation")
                {
                    grdProcessAudits.DataSource = null;
                    grdProcessAudits.DataBind();

                    grdImplementationAudits.Visible = true;
                    grdProcessAudits.Visible = false;

                    var AuditLists = DashboardManagementRisk.GetAuditManagerAuditsIMP(FinYear, Period, CustomerId, branchid, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalID);
                    grdImplementationAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdImplementationAudits.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }

                bindPageNumber();
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }

            BindSchedulingType();
            BindData();
            BindVertical();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                {
                    BindSchedulingType();
                    BindVertical();
                    BindData();
                }
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            {
                BindAuditSchedule("S", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        int count = 0;
                        count = UserManagementRisk.GetPhaseCount(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        BindAuditSchedule("P", count);
                    }
                }
            }
            bindPageNumber();
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
                {
                    if (ViewState["Type"].ToString() == "Process")
                    {
                        grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    }
                    else if (ViewState["Type"].ToString() == "Implementation")
                    {
                        grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    }
                }

                BindData();
                if (ViewState["Type"].ToString() == "Process")
                {
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdProcessAudits.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
                else if (ViewState["Type"].ToString() == "Implementation")
                {
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdImplementationAudits.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void grdProcessAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int AuditID = Convert.ToInt32(commandArgs[4]);
                    ViewState["AuditID"] = AuditID;
                    if (RiskCategoryManagement.CheckGeneareReportExistOrNot(AuditID))
                    {
                        #region generate report
                        try
                        {
                            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                            {
                                if (ddlCustomer.SelectedValue != "-1")
                                {
                                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Select Customer";
                                    return;
                                }
                            }
                            string ReportNameAndNumber = string.Empty;
                            string CustomerBranchName = string.Empty;
                            string FinancialYear = string.Empty;
                            int CustomerBranchId = -1;
                            string PeriodName = string.Empty;
                            List<string> AssignedUserList = new List<string>();
                            string custName = string.Empty;
                            string custNm = string.Empty;
                            string periodForFooter = string.Empty;

                            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                            {
                                if (ddlLegalEntity.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                                    CustomerBranchName = ddlLegalEntity.SelectedItem.Text.Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                            {
                                if (ddlSubEntity1.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                                    CustomerBranchName = ddlSubEntity1.SelectedItem.Text.Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                            {
                                if (ddlSubEntity2.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                                    CustomerBranchName = ddlSubEntity2.SelectedItem.Text.Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                            {
                                if (ddlSubEntity3.SelectedValue != "-1")
                                {
                                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                                    CustomerBranchName = ddlSubEntity3.SelectedItem.Text.Trim();
                                }
                            }

                            int VerticalID = -1;

                            if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                            {
                                if (ddlPeriod.SelectedValue != "-1")
                                {
                                    PeriodName = ddlPeriod.SelectedItem.Text.Trim();
                                }
                            }
                            var customerName = UserManagementRisk.GetCustomerName(CustomerId);

                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                var AuditDetails = (from row in entities.AuditClosureDetails
                                                    where row.ID == AuditID
                                                    select row).FirstOrDefault();
                                CustomerBranchId = Convert.ToInt32(AuditDetails.BranchId);
                                FinancialYear = AuditDetails.FinancialYear;
                                PeriodName = AuditDetails.ForMonth;
                                VerticalID = Convert.ToInt32(AuditDetails.VerticalId);
                                CustomerBranchName = (from row in entities.mst_CustomerBranch
                                                      where row.ID == CustomerBranchId
                                                      select row.Name).FirstOrDefault().ToString();

                                AssignedUserList = (from row in entities.InternalControlAuditAssignments
                                                    join row1 in entities.mst_User
                                                    on row.UserID equals row1.ID
                                                    where row.IsActive == true
                                                    && row.AuditID == AuditID
                                                    select row1.FirstName + " " + row1.LastName).Distinct().ToList();
                            }

                            DateTime draftDate = (DateTime)UserManagementRisk.GetDraftReportDate(CustomerId, CustomerBranchId, VerticalID, PeriodName, FinancialYear);
                            var draftReportReleaseDate = draftDate.ToString("MMM dd, yyyy");
                            var draftReportReleaseTime = draftDate.ToString("HH:mm");

                            DateTime releaseDate = (DateTime)UserManagementRisk.GetCreatedDateForGenerateReport(CustomerId, CustomerBranchId, PeriodName, FinancialYear);
                            var finalReportReleaseDate = releaseDate.ToString("MMM dd, yyyy");
                            var finalReportReleaseTime = releaseDate.ToString("HH:mm");

                            ReportNameAndNumber = RiskCategoryManagement.GetReportNameAndNumber(AuditID);
                            #region Word Report Second                 
                            System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();
                            System.Text.StringBuilder stringbuilderTOPSecond = new System.Text.StringBuilder();
                            int AnnexueId = 1;
                            string SWReportImage1 = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/Images/SWReportImage.jpg"; // added by sagar more on 21-01-2020
                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                var AuditDetails = (from row in entities.AuditClosureDetails
                                                    where row.ID == AuditID
                                                    select row).FirstOrDefault();
                                CustomerBranchId = Convert.ToInt32(AuditDetails.BranchId);
                                FinancialYear = AuditDetails.FinancialYear;
                                PeriodName = AuditDetails.ForMonth;
                                VerticalID = Convert.ToInt32(AuditDetails.VerticalId);

                                CustomerBranchName = (from row in entities.mst_CustomerBranch
                                                      where row.ID == CustomerBranchId
                                                      select row.Name).FirstOrDefault().ToString();

                                var s = (from row in entities.Tran_FinalDeliverableUpload
                                         where row.AuditID == AuditID
                                         select row).ToList();
                                AssignedUserList = (from row in entities.InternalControlAuditAssignments
                                                    join row1 in entities.mst_User
                                                    on row.UserID equals row1.ID
                                                    where row.IsActive == true
                                                    && row.AuditID == AuditID
                                                    select row1.FirstName + " " + row1.LastName).Distinct().ToList();
                            }
                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                var itemlist = (from row in entities.SPGETObservationDetails(CustomerBranchId, FinancialYear, VerticalID, PeriodName, AuditID)
                                                select row).ToList();

                                var AuditeeList = itemlist.Select(entry => entry.PersonResponsibleName).Distinct().ToList();
                                #region MyRegion


                                if (itemlist.Count > 0)
                                {
                                    // get Customer Name 
                                    custNm = itemlist.Select(b => b.CustomerName).FirstOrDefault().ToString();
                                    custName = custNm.ToUpper();
                                    // get branch Name 
                                    string branchName = itemlist.Select(b => b.Branch).FirstOrDefault().ToString();

                                    // get finacial Year
                                    string finacialYear = itemlist.Select(f => f.FinancialYear).FirstOrDefault().ToString();

                                    periodForFooter = itemlist.Select(b => b.ForPeriod).FirstOrDefault().ToString();
                                    // get process name
                                    List<string> tempListOfProcess = new List<string>();
                                    List<string> listOfProcess = new List<string>();
                                    foreach (var item in itemlist)
                                    {
                                        if (!string.IsNullOrEmpty(item.ProcessName))
                                        {
                                            tempListOfProcess.Add(item.ProcessName);
                                        }
                                    }
                                    if (tempListOfProcess.Count > 0)
                                    {
                                        listOfProcess = tempListOfProcess.Distinct().ToList();
                                    }

                                    string perfomersNameCommaSeparated = string.Empty;
                                    string reviewersNameCommaSeparated = string.Empty;
                                    var listOfPerformerReviewerWithRole = (from row in entities.sp_GetPerformerReviewerWithRole(CustomerBranchId, VerticalID, AuditID) select row).OrderBy(p => p.RoleID).ToList();
                                    if (listOfPerformerReviewerWithRole.Count > 0)
                                    {
                                        string perfomersName = string.Empty;
                                        string reviewersName = string.Empty;
                                        List<string> performersList = new List<string>();
                                        List<string> reviewerList = new List<string>();
                                        foreach (var item in listOfPerformerReviewerWithRole)
                                        {
                                            if (item.RoleID == 3)
                                            {
                                                performersList.Add(item.FullName);
                                            }
                                            else if (item.RoleID == 4)
                                            {
                                                reviewerList.Add(item.FullName);
                                            }
                                            else if (item.RoleID == 5)
                                            {
                                                reviewerList.Add(item.FullName);
                                            }
                                        }
                                        if (performersList.Count > 0)
                                        {
                                            perfomersName = String.Join(",", performersList);
                                        }
                                        if (reviewerList.Count > 0)
                                        {
                                            reviewersName = String.Join(",", reviewerList);
                                        }

                                        if (!string.IsNullOrEmpty(perfomersName))
                                        {
                                            perfomersNameCommaSeparated = perfomersName.TrimEnd(',');
                                        }
                                        if (!string.IsNullOrEmpty(reviewersName))
                                        {
                                            reviewersNameCommaSeparated = reviewersName.TrimEnd(',');
                                        }
                                    }
                                    #region first Report
                                    //stringbuilderTOP.Append(@"<table style='width:80%; margin-bottom:2%;'><tr><td colspan = '2'><img height='189' width='795' src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/ReportBackgroundImage.jpg' style='display: block; margin: 160px auto 20px;' width='980' height='175'/><td/></tr><tr><td width='75%'></td><td style='float:right;margin-right: 288px;'><img height='73' width='180' src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/GurujanaReportEndImage.png' width= '250' height='95' style='float:right;margin-top: -23px;'/></td></tr><tr><td> &nbsp;</td><td></td></tr><tr><td></td><td style='float:right;color:midnightblue;font-weight:bold;font-family: cambria; font-size: 13pt;margin-right: 100px;'><u> INTERNAL AUDIT REPORT OF <br/>" + custName + "</u><p style = 'color:MediumSeaGreen;font-weight:bold;font-family: cambria; font-size: 11pt;'>For the Period " + finacialYear + " </p></td></tr></table>");

                                    // stringbuilderTOP.Append(@"<table style='width:60%; margin-bottom:2%;'><tr><td colspan = '2'><img height='189' width='795' src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/ReportBackgroundImage.jpg' style='display: block; margin: 160px auto 20px;' width='980' height='175'/></td/></tr><tr><td width='75%'></td><td style='float:right;margin-right: 288px;' width='25%'><img height='73' width='180' src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/GurujanaReportEndImage.png' width= '250' height='95' style='float:right;margin-top: -23px;'/></td></tr><tr><td> &nbsp;</td><td></td></tr><tr><td colspan='2' style='float:right;color:midnightblue;font-weight:bold;font-family: cambria; font-size: 13pt;margin-right: 100px;' align='right'><u> INTERNAL AUDIT REPORT OF " + custName + "</u><p style = 'color:MediumSeaGreen;font-weight:bold;font-family: cambria; font-size: 11pt;' align='right'>For the Period " + finacialYear + " </p></td></tr></table>");



                                    //stringbuilderTOP.Append(@"<table style='width:80%; margin-bottom:2%;'><tr><td colspan = '2'><img height='189' width='795' src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/ReportBackgroundImage.jpg' style='display: block; margin: 160px auto 20px;' width='980' height='175'/><td/></tr><tr><td width='75%'></td><td style='float:right;margin-right: 288px;'><img height='73' width='180' src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/GurujanaReportEndImage.png' width= '250' height='95' style='float:right;margin-top: -23px;'/></td></tr><tr><td> &nbsp;</td><td></td></tr><tr><td></td><td style='float:right;color:midnightblue;font-weight:bold;font-family: cambria; font-size: 13pt;margin-right: 100px;'><u> INTERNAL AUDIT REPORT OF <br/>" + custName + "</u><p style = 'color:MediumSeaGreen;font-weight:bold;font-family: cambria; font-size: 11pt;'>For the Period " + finacialYear + " </p></td></tr></table>");

                                    // changed on 07-05-2020
                                    //stringbuilderTOP.Append(@"<table style='margin-bottom:2%;width:80%;'><tr><td colspan = '2'><img height='189' width='650' src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/ReportBackgroundImage.jpg' style='display: block;margin-right: 140px;margin: 160px auto 20px;' width='980' height='175'/></td></tr><tr><td width='65%'></td><td style='float:right;margin-right: 288px;' width='25%'><img height='73' width='180' src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/Logo.jpg' width= '250' height='95' style='float:right;margin-top: -23px;margin-right: 10px;'/></td></tr></table>");
                                    stringbuilderTOP.Append(@"<br/><div style='color:midnightblue;font-weight:bold;font-family: cambria; font-size: 13pt;text-align:right;margin-right: 20px;'><u> INTERNAL AUDIT REPORT OF " + custName + "</u><br/><p style = 'color:MediumSeaGreen;font-weight:bold;font-family: cambria; font-size: 11pt;text-align:right'>For the Period " + finacialYear + "</p></div>");
                                    stringbuilderTOP.Append(@"<table></table>");
                                    // Index 
                                    stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                    // stringbuilderTOP.Append(@"<table style='font-family:Cambria; width:80%;' align='center'><tr><td colspan = '4' style = 'text-align: center;font-family:Cambria; font-size:16pt;color: MediumSeaGreen;font-weight:bold'> INDEX </td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;width: 11px;font-size:11pt;'>I</td><td style='color: MediumSeaGreen;font-weight:bold;width: 4px;font-size:11pt;'>INTRODUCTION....................................................................................</td><td style='font-weight:bold;color: MediumSeaGreen;font-size:11pt;' align='left'>3</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>II</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>CRITICALITY AND ROOT CAUSE ANALYSIS................................</td><td style='font-weight:bold;color: MediumSeaGreen;font-size:11pt;'>6</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>III</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>EXECUTIVE SUMMARY.......................................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>7</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>IV</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>DETAILED OBSERVATIONS................................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>8</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>1</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>INVENTORY MANAGEMENT.............................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>8</td></tr><tr><td style='color:midnightblue;font-weight:bold;font-size:11pt;'>1.1</td><td style='color:midnightblue;font-weight:bold;font-size:11pt;'>INVENTORY AGEING...........................................................................</td><td style='color:midnightblue;font-weight:bold;font-size:11pt;'>8</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>V</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>PENDING INFORMATION...................................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>9</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>VI</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'> PREVIOUS AUDIT COMPLIANCE.....................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>10</td></tr></table>");

                                    //changed on 30-04-2020
                                    //stringbuilderTOP.Append(@"<table style='font-family:Cambria; width:80%;' align='center'><tr><td colspan = '4' style = 'text-align: center;font-family:Cambria; font-size:16pt;color: MediumSeaGreen;font-weight:bold'><a href='' style = 'text-align: center;font-family:Cambria; font-size:16pt;color: MediumSeaGreen;font-weight:bold'>INDEX</a></td></tr></table><table style='font-family:Cambria; width:80%;margin-right: -15px;' align='center'><tr><td style='color: MediumSeaGreen;font-weight:bold;width: 11px;font-size:11pt;'>I</td><td style='color: MediumSeaGreen;font-weight:bold;width: 4px;font-size:11pt;'>INTRODUCTION....................................................................................................................................................................................</td><td style='font-weight:bold;color: MediumSeaGreen;font-size:11pt;' align='left'>3</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>II</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>CRITICALITY AND ROOT CAUSE ANALYSIS................................................................................................................................</td><td style='font-weight:bold;color: MediumSeaGreen;font-size:11pt;'>6</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>III</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>EXECUTIVE SUMMARY.......................................................................................................................................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>7</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>IV</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>DETAILED OBSERVATIONS................................................................................................................................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>8</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>1</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>INVENTORY MANAGEMENT.............................................................................................................................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>8</td></tr><tr><td style='color:midnightblue;font-weight:bold;font-size:11pt;'>1.1</td><td style='color:midnightblue;font-weight:bold;font-size:11pt;'>INVENTORY AGEING &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='color:midnightblue;font-weight:bold;font-size:11pt;'>8</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>V</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>PENDING INFORMATION..................................................................................................................................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>9</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>VI</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'> PREVIOUS AUDIT COMPLIANCE.....................................................................................................................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>10</td></tr></table>");

                                    // changed on 07-05-2020
                                    stringbuilderTOP.Append(@"<table style='font-family:Cambria; width:80%;' align='center'><tr><td colspan = '4' style = 'text-align: center;font-family:Cambria; font-size:16pt;color: MediumSeaGreen;font-weight:bold'><a href='' style = 'text-align: center;font-family:Cambria; font-size:16pt;color: MediumSeaGreen;font-weight:bold'>INDEX</a></td></tr></table><table style='font-family:Cambria; width:80%;margin-right: -15px;' align='center'><tr><td style='color: MediumSeaGreen;font-weight:bold;width: 11px;font-size:11pt;'>I &nbsp;&nbsp;&nbsp;</td><td style='color: MediumSeaGreen;font-weight:bold;width: 4px;font-size:11pt;'>INTRODUCTION....................................................................................</td><td style='font-weight:bold;color: MediumSeaGreen;font-size:11pt;' align='left'>3</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>II&nbsp;&nbsp;&nbsp;</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>CRITICALITY AND ROOT CAUSE ANALYSIS................................</td><td style='font-weight:bold;color: MediumSeaGreen;font-size:11pt;'>6</td></tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>III&nbsp;&nbsp;&nbsp;</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>EXECUTIVE SUMMARY.......................................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>7</td></tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>IV&nbsp;&nbsp;&nbsp;</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>DETAILED OBSERVATIONS................................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>8</td></tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>V&nbsp;&nbsp;&nbsp;</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>PENDING INFORMATION...................................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>9</td></tr><td>&nbsp;</td><td>&nbsp;</td><tr><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>VI&nbsp;&nbsp;&nbsp;</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'> PREVIOUS AUDIT COMPLIANCE.....................................................</td><td style='color: MediumSeaGreen;font-weight:bold;font-size:11pt;'>10</td></tr></table>");

                                    // INTRODUCTION
                                    stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                    stringbuilderTOP.Append(@"<table style='font-family:Cambria;width:100%;'>");
                                    // changed on 29-04-2020
                                    stringbuilderTOP.Append(@"<tr><td style='background-color: midnightblue; width:2px; color:white; font-family:Cambria; font-size:13pt;'><strong>I.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTRODUCTION</strong></td></tr>");
                                    //changed on 30-04-2020
                                    stringbuilderTOP.Append(@"<tr></tr>");
                                    stringbuilderTOP.Append(@"<tr></tr>");
                                    stringbuilderTOP.Append(@"<tr><td><p style='font-family:Cambria; font-size:11pt;'>This report represents the findings of the Internal Audit of " + custName + " conducted by Guru & Jana, Chartered Accountants, Bangalore, for the period " + finacialYear + ". This review was conducted in the months of &#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95; at " + custName + ".,&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;. The review team consisted of &#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;. The audit was conducted under the overall supervision of our Partner Mr. M. Surendra Reddy.</p></td></tr>");

                                    //stringbuilderTOP.Append(@"<tr><td><p style='font-family:Cambria; font-size:11pt;'>This report represents the findings of the Internal Audit of " + custName + " conducted by Guru & Jana, Chartered Accountants, Bangalore, for the period " + finacialYear + ". This review was conducted in the months of &#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95; at " + custName + ".,&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;&#95;. The review team consisted of Audit Manager " + reviewersNameCommaSeparated + " , with Audit Executives, " + perfomersNameCommaSeparated + ". The audit was conducted under the overall supervision of our Partner Mr. M. Surendra Reddy.</p></td></tr>");
                                    stringbuilderTOP.Append(@"</table>");
                                    stringbuilderTOP.Append(@"<br/>");
                                    stringbuilderTOP.Append(@"<table>");
                                    stringbuilderTOP.Append(@"<tr><td style='color:midnightblue;font-weight: bold;font-style: italic;font-family:Cambria; font-size:11pt'>Areas Covered / Checks made</td></tr>");

                                    if (listOfProcess.Count > 0)
                                    {
                                        int processSerialNo = 1;
                                        foreach (var process in listOfProcess)
                                        {
                                            stringbuilderTOP.Append(@"<tr><td style='color:midnightblue;font-weight: bold;font-style: italic;font-family:Cambria; font-size:11pt;'>" + processSerialNo + ". &nbsp;	<span style='color:midnightblue;font-weight: bold;font-style: italic;font-family:Cambria; font-size:11pt;text-decoration: underline;'>" + process + "</span></td></tr>");
                                            processSerialNo++;
                                        }
                                    }

                                    stringbuilderTOP.Append(@"</table>");

                                    // changed on 30-04-2020
                                    stringbuilderTOP.Append(@"<br/>");

                                    // SCOPE AND OBJECTIVE, LIMITATIONS AND EXTENT OF REVIEW
                                    stringbuilderTOP.Append(@"<table><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;color: midnightblue;font-weight: bold;'>SCOPE AND OBJECTIVE, LIMITATIONS AND EXTENT OF REVIEW</p></td></tr><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;'>As agreed between Guru  & Jana and the company, we conducted a review on specific areas that the management of the company had highlighted for our attention.<br/>The audit was planned and performed to obtain reasonable assurance whether the systems, processes, and controls operate efficiently and effectively, to identify the following.</p></td></tr></table>");
                                    stringbuilderTOP.Append(@"<table style='font-family:Cambria; font-size:11pt;width:100%;'><tr><td><ul><li style='padding: 0;margin: 0;'>Redundant processes / activities and to recommend effective practices to manage the process better with minimum risk exposure possible.</li><li style='padding: 0;margin: 0;'>Appropriate records are prepared and documented.</li><li style='padding: 0;margin: 0;'>Control gaps in the design of processes and to recommend controls design to minimize residual risk exposure, inadequacies in the system and associated risks.</li><li style='padding: 0;margin: 0;'>Internal processes with Industry best practices and identify areas of improvement.</li><li style='padding: 0;margin: 0;'>Processes/ sub processes/ activities that can be further automated to ensure better process performance.</li></ul></td></tr><br/> <tr><td><p style='font-family:Cambria; font-size:11pt;'>This work undertaken by the Guru & Jana does not constitute a statutory audit. The internal control environment, operation of the systems, procedures, controls and steps undertaken to address weaknesses therein are solely the responsibility of the management and identifying and reporting on the weaknesses in internal controls, risk management and governance is the responsibility of Guru & Jana.</p></td></tr></table>");

                                    //PROGRESS TO DATE
                                    stringbuilderTOP.Append(@"<br/><br/>");
                                    stringbuilderTOP.Append(@"<table><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;color: midnightblue;font-weight: bold;'>PROGRESS TO DATE</p></td></tr><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;'>This report contains observations during the review that was conducted on the basis of walkthrough of documents and information provided to the team Guru & Jana. Consequently, Guru & Jana is not responsible for issues emanating from documents and information that were not provided. This report has been prepared on the basis of our observations during the fieldwork and we are now seeking the management&#39;s comment on our observations.</p></td></tr></table>");

                                    //METHODOLOGY
                                    stringbuilderTOP.Append(@"<br/><br/>");
                                    stringbuilderTOP.Append(@"<table><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;color: midnightblue;font-weight: bold;'>METHODOLOGY</p></td></tr><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;'>During our internal audit, we had conducted discussions with the process owners, walkthroughs of processes, reviewed the operational and financial documents provided. Based on our understanding of the risk classification, we have undertaken certain review procedures (as explained below):</p></td></tr></table>");
                                    stringbuilderTOP.Append(@"<table><tr><td style='font-family:Cambria; font-size:11pt'><ul><li>Discussions with nominated personnel of the association for low risk areas</li><li>Reading and understanding of documents for moderate to low risk areas</li><li>A combination of predictive, analytical and substantive tests for high risk areas</li></ul></td></tr></table>");

                                    //SOURCES OF INFORMATION
                                    // changed on 28-04-2020
                                    stringbuilderTOP.Append(@"<br/><br/>");
                                    stringbuilderTOP.Append(@"<table><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;color: midnightblue;font-weight: bold;'>SOURCES OF INFORMATION</p></td></tr><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;'>The information in this report is based on discussions with personnel identified by the management for interactions with us, walkthrough of processes and documents provided to us. This report has been made based on the financial information provided till March, 2016. Guru & Jana makes no undertaking to supplement or update this report on the basis of changed circumstances or facts of which we become aware after the date thereof,</p></td></tr></table>");

                                    //INHERENT RISK
                                    stringbuilderTOP.Append(@"<br/><br/>");
                                    stringbuilderTOP.Append(@"<table><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;color: midnightblue;font-weight: bold;'>INHERENT RISK</p></td></tr><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;'>It may be noted that since our work was based on agreed upon procedures, which include a combination of various approaches (as explained above); the same cannot be used to identify and detect all existing, past and future frauds or errors. We wish to highlight that there is an inherent audit risk in any management assurance review. Such an inherent risk can be defined as the likelihood of a misstatement existing in an account balance or class of transactions that would be material when aggregated with misstatements in other accounts or classifications, assuming that there were insufficient related internal controls. As business environment and risks change over time, the gaps associated with the operation of the company processes described in this report, as well as the actions necessary to reduce the exposure of such gaps, will also change. <br/>There are many factors that affect the inherent audit risk and important factors that need consideration in this respect which include estimates, sensitivity to external forces and characteristics of the industry in which the association operates. In general, the stronger the internal control structure, the more likely that material misstatement will be prevented or detected by the system. However, some control risks will always exist because of such inherent limitations.</p></td></tr></table>");

                                    //CONFIDENTIALITY
                                    stringbuilderTOP.Append(@"<br/><br/>");
                                    stringbuilderTOP.Append(@"<table><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;color: midnightblue;font-weight: bold;'>CONFIDENTIALITY</p></td></tr><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;'>This report is confidential and has been prepared exclusively for the use of the person to whom it is provided and is not to be publicized in any way. It should not be used, reproduced or circulated for any other purpose in whole or in part, without our prior written consent. We will not be responsible for any duplication/circulation of this report without our prior consent.</p></td></tr></table>");

                                    //APPRECIATION NOTE
                                    stringbuilderTOP.Append(@"<br/><br/>");
                                    stringbuilderTOP.Append(@"<table><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;color: midnightblue;font-weight: bold;'>APPRECIATION NOTE</p></td></tr><tr><td><p style='font-family:Cambria; font-size:11pt;width:100%;'>Guru & Jana is thankful for the courtesies and cooperation extended by the employees of the company for this engagement.<br/>The observations together with suggestions for improvements, if any have been given in Part III and IV of the report.</p></td></tr></table>");
                                    stringbuilderTOP.Append(@"<br/><br/>");
                                    stringbuilderTOP.Append(@"<table><tr><td style='font-family:Cambria; font-size:11pt;color: midnightblue;font-weight: bold;'>For Guru & Jana,<br/><tr><td style='font-family:Cambria; font-size:11pt;color:black'><em>Chartered Accountants</em><br/><strong>Firm Registration No.006826S</strong><br/><p style='font-family:Cambria; font-size:11pt;margin-top: 75px;'><strong>M. Surendra Reddy</strong><br/><em>Partner</em><br/><strong>Membership No. 215205</strong></p><br/><p style='font-family:Cambria; font-size:11pt;'><strong>Place:</strong> Bangalore<br/><strong>Date:</strong></p></td></tr></td></table>");
                                    stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                                    //II.CRITICALITY AND ROOT CAUSE ANALYSIS
                                    stringbuilderTOP.Append(@"<table style='font-family:Cambria; font-size:11pt;width:100%;'>");
                                    // changed on 29-04-2020
                                    stringbuilderTOP.Append(@"<tr><td style='background-color: midnightblue; width:2px; color:white; font-family:Cambria; font-size:13pt;'><strong>II.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CRITICALITY AND ROOT CAUSE ANALYSIS</strong></td></tr><tr><td style='color:MediumSeaGreen;border:none;mso-border-bottom-alt:solid windowtext .75pt;width:100%;'></td></tr></table>");
                                    stringbuilderTOP.Append(@"<table><tr><td><p style='font-family:Cambria; font-size:11pt; width:100%;'>The observation rating criteria and risk rating criteria are as per the pre-defined parameters specified by the company&#39;s management, as follows:</p></td></tr></table>");

                                    //stringbuilderTOP.Append(@"<table style='width: 100%;font-family: Cambria; font-size: 11pt;'><tr><td style='width: 30%'><table style='border: 1px solid black; border-collapse: collapse;'><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%' height='30'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%' height='30'>&nbsp;</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%;border: 1px solid black; border-collapse: collapse;' rowspan='7' ><p style='writing-mode: tb-rl;'><strong>ROOT CAUSE</strong></p></td><td style='width: 2%'>&nbsp;</td><td style='width: 4%' height='30'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%' height='30'>&nbsp;</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: midnightblue; color: white; width: 4%;' height='30' align='center'><strong>DEFICIENCY</strong></td><td style='width: 2%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: midnightblue; color: white; width: 4%;' height='30' align='center'><strong>PROCESS</strong></td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: midnightblue; color: white;width:4%;' align='center' height='30'><strong>EXECUTION DEFICIENCY</strong></td><td style='width: 2%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: midnightblue; color: white;width:4%;' align='center' height='30'><strong>PEOPLE</strong></td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: midnightblue; color: white;width:4%;' align='center' height='30'><strong>SYSTEM LIMITATION</strong></td><td style='width: 2%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: midnightblue; color: white;width:4%;' align='center' height='30'><strong>TECHNOLOGY</strong></td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td>&nbsp;</td></tr></table></td><td style='width: 30%'><table style='border: 1px solid black; border-collapse: collapse;' cellpadding='3'><tr><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td colspan='5' style='border: 1px solid black;border-collapse: collapse;'>Observations related to absence of defined process or weakness in existing processes<br /><strong>Example:</strong> Absence of inventory holding norms</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td colspan='5' style='border: 1px solid black;border-collapse: collapse;'>Observations related to non-adherence to defined process or discipline issues<br /><strong>Example:</strong> Non adherence to inventory norms</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td colspan='5' style='border: 1px solid black;border-collapse: collapse;'>Observations related to sub-optimal utilization of system functionalities.<br /><strong>Example:</strong> Non updation of inventory levels in ERP</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td></tr></table></td></tr><tr><td style='width: 30%'><table style='border: 1px solid black; border-collapse: collapse;height:250px;'><tr><td style='height:30%'>&nbsp;</td><td style='height:30%'>&nbsp;</td><td style='height:30%'>&nbsp;</td><td style='height:30%'>&nbsp;</td><td style='height:30%'>&nbsp;</td><td style='height:30%'>&nbsp;</td><td style='height:30%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%;border: 1px solid black; border-collapse: collapse;' rowspan='7' ><p style='writing-mode: tb-rl;'><strong>CRITICALITY LEVEL</strong></p></td><td style='width: 2%'>&nbsp;</td><td style='width: 4%' height='30'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%' height='30'>&nbsp;</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: red; color: white; width: 6%;' height='30' align='center'><strong>HIGH</strong></td><td style='width: 2%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: red; color: white; width: 8%;' height='30' align='center'><strong>H</strong></td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: orange; color: white;width:6%;' align='center' height='30'><strong>MEDIUM</strong></td><td style='width: 2%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: orange; color: white;width:8%;' align='center' height='30'><strong>M</strong></td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: MediumSeaGreen; color: white;width:6%;'align='center' height='30'><strong>LOW</strong></td><td style='width: 2%'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse; background-color: MediumSeaGreen; color: white;width:8%;' align='center' height='30'><strong>L</strong></td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td> <td style='width: 1%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td>&nbsp;</td></tr><tr><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table></td><td style='width: 30%'><table style='border: 1px solid black; border-collapse: collapse;height:250px;' cellpadding='3'><tr><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td colspan='5' style='border: 1px solid black;border-collapse: collapse;'>A weakness where there is substantial risk of loss, fraud, impropriety, poor value for money, or failure to achieve organizational objectives.</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td colspan='5' style='border: 1px solid black;border-collapse: collapse;'>A weakness in control which, although not fundamental, relates to shortcomings which expose individual business systems to a less immediate level of threatening risk or poor value of money.</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td><td style='width: 4%'>&nbsp;</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td colspan='5' style='border: 1px solid black;border-collapse: collapse;'>Areas that individually have no significant impact, but where management would benefit   from improved controls and/or have the opportunity to achieve greater effectiveness and/or efficiency.</td><td style='width: 2%'>&nbsp;</td></tr><tr><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td><td style='width: 1%'>&nbsp;</td></tr></table></td></tr></table>");
                                    stringbuilderTOP.Append(@"<table style='width:90%;font-family:Cambria; font-size:11pt;'>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:40%'>");

                                    #region First row first table
                                    stringbuilderTOP.Append(@"<table style='height:100%; border:1px solid black; border-collapse:collapse;font-family:Cambria;font-size:11pt;'>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1.5%;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td colspan='3' style='text-align: center; font-weight: bold; width:1%;border:1px solid black; border-collapse:collapse;'>ROOT CAUSE</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    #region added on 07-05-2020
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    #endregion
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color: midnightblue; color: white; width:4%;' align='center'><strong>DESIGN DEFICIENCY</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color: midnightblue; color: white; width:4%;' align='center'><strong>PROCESS</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color: midnightblue; color: white;width:4%;' align='center'><strong>EXECUTION DEFICIENCY</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color: midnightblue; color: white;width:4%;' align='center'><strong>PEOPLE</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height: 14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color: midnightblue; color: white;width:4%;' align='center'><strong>SYSTEM LIMITATION</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color: midnightblue; color: white;width:4%;' align='center'><strong>TECHNOLOGY</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"</table>");
                                    stringbuilderTOP.Append(@"</td>");
                                    #endregion First Block end

                                    #region First row Secode Table
                                    stringbuilderTOP.Append(@"<td style='width:60%'>");
                                    stringbuilderTOP.Append(@"<table style='height:100%;border:1px solid black; border-collapse: collapse;font-family:Cambria;font-size:11pt;'>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:20px;'></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:20px;'></td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:45px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td colspan='5' style='border:1px solid black;border-collapse:collapse;'>Observations related to absence of defined process or weakness in existing processes<br/><strong>Example:</strong> Absence of inventory holding norms</td>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:45px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    stringbuilderTOP.Append(@"<tr>");

                                    stringbuilderTOP.Append(@"<td style='width:3%;height:20px;'></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:20px;'></td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    stringbuilderTOP.Append(@"<tr>");

                                    stringbuilderTOP.Append(@"<td style='width:3%;height:45px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td colspan='5' style='border:1px solid black;border-collapse:collapse;'>Observations related to non-adherence to defined process or discipline issues<br/><strong>Example:</strong> Non adherence to inventory norms</td>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:45px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:20px;'></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:20px;'></td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:45px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td colspan='5' style='border:1px solid black;border-collapse: collapse;'>Observations related to sub-optimal utilization of system functionalities.<br/><strong>Example:</strong> Non updation of inventory levels in ERP</td>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:45px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:20px;'></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td></td>");
                                    stringbuilderTOP.Append(@"<td style='width:3%;height:20px;'></td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"</table>");
                                    stringbuilderTOP.Append(@"</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    #endregion First row secode End

                                    #region Secode row first 
                                    stringbuilderTOP.Append(@"<td style='width:40%'>");
                                    stringbuilderTOP.Append(@"<table style='border:1px solid black; border-collapse: collapse;font-family:Cambria;font-size:11pt;'>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1.5%;height:50px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='height:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='height:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='height:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width: 1%;height: 50px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    //stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='text-align: center; font-weight: bold; width:1%;border: 1px solid black; border-collapse: collapse;'colspan='3'>CRITICALITY LEVEL</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    #region added on 07-05-2020
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    #endregion

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    //stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color: red; color: white; width: 6%;' height='30' align='center'><strong>HIGH</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color: red; color: white; width: 8%;' height='30' align='center'><strong>H</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    #region added on 07-05-2020
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    #endregion

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color: orange; color: white;width:6%;' align='center' height='30'><strong>MEDIUM</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color: orange; color: white;width:8%;' align='center' height='30'><strong>M</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    //stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    #region added on 07-05-2020
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    //stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    #endregion

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color:#92D050; color: white;width:6%;' align='center' height='30'><strong>LOW</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='border:1px solid black; border-collapse: collapse; background-color:#92D050; color: white;width:8%;' align='center' height='30'><strong>L</strong></td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height: 14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    //stringbuilderTOP.Append(@"<tr>");
                                    //stringbuilderTOP.Append(@"<td style='width:1%;height:50px;'>&nbsp;</td>");
                                    //stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    //stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    //stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    //stringbuilderTOP.Append(@"<td style='width: 1%;height:50px;'>&nbsp;</td>");
                                    //stringbuilderTOP.Append(@"</tr>");

                                    #region added on 07-05-2020
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%;height:14px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    #endregion
                                    stringbuilderTOP.Append(@"</table>");
                                    #endregion Secode row first End

                                    #region Secode row second table
                                    stringbuilderTOP.Append(@"</td>");
                                    stringbuilderTOP.Append(@"<td style='width:60%'>");
                                    stringbuilderTOP.Append(@"<table style='height:100%;border: 1px solid black; border-collapse: collapse;font-family:Cambria;font-size:11pt;'>");
                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:20px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:20px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:50px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td colspan='5' style='border:1px solid black;border-collapse: collapse;'>A weakness where there is substantial risk of loss, fraud, impropriety, poor value for money, or failure to achieve organizational objectives.</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:50px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:20px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:20px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:50px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td colspan='5' style='border:1px solid black;border-collapse: collapse;'>A weakness in control which, although not fundamental, relates to shortcomings which expose individual business systems to a less immediate level of threatening risk or poor value of money.</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:50px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:20px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:2%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:4%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:20px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width: 0.5%;height: 50px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td colspan='5' style='border: 1px solid black;border-collapse: collapse;'>Areas that individually have no significant impact, but where management would benefit   from improved controls and/or have the opportunity to achieve greater effectiveness and/or efficiency.</td>");
                                    stringbuilderTOP.Append(@"<td style='width: 0.5%;height: 50px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");

                                    stringbuilderTOP.Append(@"<tr>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height: 20px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:1%'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<td style='width:0.5%;height:20px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    stringbuilderTOP.Append(@"</table>");
                                    stringbuilderTOP.Append(@"</td>");
                                    stringbuilderTOP.Append(@"</tr>");
                                    stringbuilderTOP.Append(@"</table>");
                                    #endregion second row second table
                                    ////End Table format=====================================//

                                    // III. EXECUTIVE SUMMARY
                                    stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                    stringbuilderTOP.Append(@"<table style='font-family:Cambria; font-size:11pt;width:100%;'>");
                                    // changed on 29-04-2020
                                    stringbuilderTOP.Append(@"<tr><td style='background-color: midnightblue; width:2px; color:white; font-family:Cambria; font-size:13pt;'><strong>III.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXECUTIVE SUMMARY</strong></td></tr></table>");
                                    stringbuilderTOP.Append(@"<hr>");

                                    // changed on 03-05-2020
                                    stringbuilderTOP.Append(@"<table style='border: 1px solid black;border-collapse: collapse;width:100%;font-family: cambria; font-size: 12pt;'><th style='border: 1px solid black;border-collapse: collapse;width:45px;background-color:midnightblue;color:white;'>AREA</th><th style='border: 1px solid black;border-collapse: collapse;width:150px;background-color:midnightblue;color:white'>BRIEF OBSERVATION</th><th style='border: 1px solid black;border-collapse: collapse;width:45px;background-color:midnightblue;color:white'>AMOUNTS (Rs)</th><th style='border: 1px solid black;border-collapse: collapse;width:45px;background-color:midnightblue;color:white'>OBSERVATION CATEGORY</th>");

                                    var executiveSummary = itemlist.GroupBy(entry => entry.ProcessName).Select(entry => entry.FirstOrDefault()).ToList();
                                    foreach (var item in executiveSummary)
                                    {
                                        // changed on 03-05-2020
                                        string processName = item.ProcessName;
                                        string observation = item.BriefObservation;
                                        string finacialImpact = item.FinancialImpact;
                                        string observationCategory = item.ObservationCategoryName;
                                        stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;font-family: cambria; font-size: 11pt;'>" + processName + "</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;font-family: cambria; font-size: 11pt;'>" + observation + "</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;font-family: cambria; font-size: 11pt;'>" + finacialImpact + "</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;font-family: cambria; font-size: 11pt;'>" + observationCategory + "</td></tr>");
                                    }
                                    stringbuilderTOP.Append(@"</table>");

                                    //IV.	DETAILED OBSERVATIONS
                                    stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                    stringbuilderTOP.Append(@"<table style='font-family:Cambria; font-size:11pt;width:100%;'>");
                                    // changed on 29-04-2020
                                    stringbuilderTOP.Append(@"<tr><td style='background-color: midnightblue; width:2px; color:white; font-family:Cambria; font-size:13pt;'><strong>IV.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DETAILED OBSERVATIONS</strong></td></tr></table>");

                                    int processCount = 1;
                                    var observationTitleCount = 0.0;
                                    string previousProcessName = string.Empty;
                                    foreach (var item in itemlist)
                                    {
                                        var observationDetails = (from row in entities.InternalControlAuditResults
                                                                  where row.AuditID == item.AuditID
                                                                  && row.ATBDId == item.ATBDID
                                                                  && row.AStatusId == 3
                                                                  select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                                        if (previousProcessName != item.ProcessName)
                                        {
                                            observationTitleCount = processCount + 0.1;
                                            //changed on 30-01-2020
                                            stringbuilderTOP.Append(@"<table style='font-family:Cambria; font-size:11pt;width:100%;'><tr><td style='background-color:MediumSeaGreen;width:2px;color:white;font-family:Cambria;font-size:12pt;'><strong>" + processCount + ".&nbsp;<span style='text-transform: uppercase;'>" + item.ProcessName + "</span></strong></td></tr></table><br/>");
                                            processCount++;
                                            previousProcessName = item.ProcessName;
                                        }
                                        else
                                        {
                                            observationTitleCount += 0.1;
                                            observationTitleCount = Math.Truncate(100 * observationTitleCount) / 100;
                                        }

                                        // added on 24-04-2020
                                        if (item.ObjBackground != null)
                                        {
                                            #region 28-04-2020 -- changed
                                            //changed on 30-04-2020
                                            // added 03-05-2020
                                            stringbuilderTOP.Append(@"<label><span style='color:midnightblue;font-weight: bold;font-family: cambria; font-size: 11pt;width:60%;'>" + observationTitleCount + " &nbsp; &nbsp;</span></label><label><span style='text-transform: uppercase;color:midnightblue;font-weight: bold;font-family: cambria; font-size: 11pt;'>" + item.ObservationTitle + "</span></label>");

                                            // changed on 07-05-2020
                                            stringbuilderTOP.Append(@"<table style='font-family: cambria; font-size: 11pt;width:100%;'><tr><td width='100%'><span style='font-style: italic;'><b>Observation background: </b></span><br/><ul><li>" + item.ObjBackground + "</li></ul></td><td>");
                                            #endregion  
                                        }
                                        //changed on 08-05-2020
                                        else
                                        {
                                            stringbuilderTOP.Append(@"<table style='font-family: cambria; font-size: 11pt;width:100%;'><tr><td width='60%'><span style='font-style: italic;display: none;'><b>Observation background: </b></span><br/></td><td>");
                                        }
                                        stringbuilderTOP.Append(@"</td></tr></table>");

                                        //#region changed on 07-05-2020
                                        //stringbuilderTOP.Append(@"<table align='right' style='border: 1px solid black; border-collapse: collapse;line-height: 3px;width:100%;font-family: cambria; font-size: 11pt;'><tr height='2'>");
                                        //stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;background-color:midnightblue;color:white;font-weight: bold;padding: 8px;' align='left' height='2' width='150'>Criticality</td>");

                                        //long? observationRating = item.ObservationRating;
                                        //if (observationRating == 1)
                                        //{
                                        //    // changed on 28-04-2020
                                        //    stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;color: black;font-size:9pt;' align='center' height='2' width='70'>&#10004;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: red;' align='center' height='2' width='70'>H</td>");
                                        //}
                                        //else
                                        //{
                                        //    stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;' align='center' height='2' width='70'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: red;' align='center' height='2' width='70'>H</td>");
                                        //}
                                        //#region 28-04-2020 -- changed
                                        //if (observationRating == 2)
                                        //{
                                        //    // changed on 28-04-2020
                                        //    stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;color: black;font-size:9pt;' align='center' height='2' width='70'>&#10004;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: #FFC000;' align='center' height='2' width='70'>M</td>");
                                        //}
                                        //else
                                        //{
                                        //    stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;' align='center' height='2' width='70'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: #FFC000;' align='center' height='2' width='70'>M</td>");
                                        //}
                                        //if (observationRating == 3)
                                        //{
                                        //    // changed on 28-04-2020
                                        //    stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;color: black;font-size:9pt;' align='center' height='2' width='70'>&#10004;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: #92D050;' align='center' height='2' width='70'>L</td>");
                                        //}
                                        //else
                                        //{
                                        //    stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;' align='center' height='2' width='70'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: #92D050;' align='center' height='2' width='70'>L</td>");
                                        //}
                                        //#endregion
                                        //stringbuilderTOP.Append(@"</tr></table></td></tr></table>");
                                        //#endregion
                                        stringbuilderTOP.Append("<br/>");
                                        stringbuilderTOP.Append(@"<table style='font-family: cambria; font-size: 11pt;' width='100%'>");
                                        stringbuilderTOP.Append(@"<tr><td width='60%' style='text-align: justify;text-justify: inter-word;'><span style='font-style: italic;'><b>Observation(s):</b></span><ul>");// changed on 03-05-2020
                                        stringbuilderTOP.Append(@"<li>" + item.Observation + "</li>");
                                        stringbuilderTOP.Append(@"</ul></td>");
                                        stringbuilderTOP.Append(@"<td width='40%'>");
                                        #region changed on 07-05-2020
                                        stringbuilderTOP.Append(@"<table align='right' style='border: 1px solid black; border-collapse: collapse;line-height: 3px;width:100%;font-family: cambria; font-size: 11pt;'><tr height='2'>");
                                        stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;background-color:midnightblue;color:white;font-weight: bold;padding: 8px;' align='left' height='2' width='150'>Criticality</td>");

                                        long? observationRating = item.ObservationRating;
                                        if (observationRating == 1)
                                        {
                                            // changed on 28-04-2020
                                            stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;color: black;font-size:9pt;' align='center' height='2' width='70'>&#10004;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: red;' align='center' height='2' width='70'>H</td>");
                                        }
                                        else
                                        {
                                            stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;' align='center' height='2' width='70'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: red;' align='center' height='2' width='70'>H</td>");
                                        }
                                        #region 28-04-2020 -- changed
                                        if (observationRating == 2)
                                        {
                                            // changed on 28-04-2020
                                            stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;color: black;font-size:9pt;' align='center' height='2' width='70'>&#10004;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: #FFC000;' align='center' height='2' width='70'>M</td>");
                                        }
                                        else
                                        {
                                            stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;' align='center' height='2' width='70'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: #FFC000;' align='center' height='2' width='70'>M</td>");
                                        }
                                        if (observationRating == 3)
                                        {
                                            // changed on 28-04-2020
                                            stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;color: black;font-size:9pt;' align='center' height='2' width='70'>&#10004;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: #92D050;' align='center' height='2' width='70'>L</td>");
                                        }
                                        else
                                        {
                                            stringbuilderTOP.Append(@"<td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;' align='center' height='2' width='70'>&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse;font-weight: bold;background-color: #92D050;' align='center' height='2' width='70'>L</td>");
                                        }
                                        #endregion
                                        stringbuilderTOP.Append(@"</tr></table></td></tr></table>");
                                        #endregion
                                        stringbuilderTOP.Append(@"</td></tr></table>");


                                        #region added on 03-05-2020
                                        List<ObservationImage> AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFileList(AuditID, item.ATBDID);
                                        List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetObservationAudioVideoList(AuditID, item.ATBDID);

                                        if (!string.IsNullOrEmpty(item.BodyContent) || AllinOneDocumentList.Count > 0 || observationAudioVideoList.Count > 0)
                                        {
                                            stringbuilderTOP.Append(@"<label><b><span style='font-family:cambria; font-size:11pt;font-style: italic;'>Annexure:</b>" + AnnexueId + "</span></label><br>");
                                            stringbuilderTOP.Append(@"<label><b><span style='font-family:cambria; font-size:11pt;font-style: italic;'>" + item.AnnexueTitle + "</span></b></label><br/>"); // changed by sagar more on 22-01-2020
                                            AnnexueId++;
                                        }

                                        if (!string.IsNullOrEmpty(item.BodyContent))
                                        {
                                            stringbuilderTOP.Append(@"<br/>");
                                            stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%;font-family:Arial;font-size:11pt'>" + item.BodyContent.Replace("@nbsp;", "").Replace("\r", "").Replace("\n", "") + "</p>");
                                            // stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%;font-family:cambria;font-size:11pt'>" + item.BodyContent.Replace("<table style='border-collapse: collapse; width: 60%; height: 133px;' border='1'>", "").Replace("@nbsp;", "").Replace("\r", "").Replace("\n", "") + "</p>");
                                            stringbuilderTOP.Append(@"<br/>");
                                        }
                                        #endregion

                                        string rootCauseImpact = observationDetails.RootCauseOrImpact;
                                        string designRootCauseImpact = string.Empty;
                                        string executionRootCauseImpact = string.Empty;
                                        string systemRootCauseImpact = string.Empty;

                                        List<string> rootCauseImpactList = new List<string>();
                                        if (!string.IsNullOrEmpty(rootCauseImpact))
                                        {
                                            rootCauseImpactList = rootCauseImpact.Split(',').ToList();
                                        }

                                        if (rootCauseImpactList.Count > 0)
                                        {
                                            foreach (var item1 in rootCauseImpactList)
                                            {
                                                if (item1.Contains("Design"))
                                                {
                                                    designRootCauseImpact = item1.ToString();
                                                }
                                                if (item1.Contains("Execution"))
                                                {
                                                    executionRootCauseImpact = item1.ToString();
                                                }
                                                if (item1.Contains("System"))
                                                {
                                                    systemRootCauseImpact = item1.ToString();
                                                }
                                            }
                                        }

                                        stringbuilderTOP.Append(@"<table style='font-family:Cambria; font-size:11pt;width:100%;'>");
                                        stringbuilderTOP.Append(@"<tr height='5'><td style='font-style: italic;font-weight: bold;'>Root Cause/Impact(s):</td>");
                                        // changed on 07-05-2020
                                        stringbuilderTOP.Append(@"<td height='5'><table style='font-family:Cambria; font-size:11pt;width:80%;border: 1px solid black; border-collapse: collapse;' align='right'>");
                                        stringbuilderTOP.Append(@"<tr height='5'>");
                                        #region changed on 07-05-2020
                                        if (!string.IsNullOrEmpty(designRootCauseImpact))
                                        {
                                            // changed on 28-04-2020
                                            stringbuilderTOP.Append(@"<td height='5' style='border: 1px solid black; border-collapse: collapse;color: black;font-size:9pt;' align='center'>&#10004;</td><td style='border: 1px solid black; border-collapse: collapse;background-color:midnightblue;color:white;font-weight: bold;' align='center'>Design</td>");
                                        }
                                        else
                                        {
                                            stringbuilderTOP.Append(@"<td height='5' style='border: 1px solid black; border-collapse: collapse;' align='center'>&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse;background-color:midnightblue;color:white;font-weight: bold;' align='center'>Design</td>");
                                        }
                                        if (!string.IsNullOrEmpty(executionRootCauseImpact))
                                        {
                                            stringbuilderTOP.Append(@"<td height='5' style='border: 1px solid black; border-collapse: collapse;color: black;font-size:9pt;' align='center'>&#10004;</td><td style='border: 1px solid black; border-collapse: collapse;background-color:midnightblue;color:white;font-weight: bold;' align='center'>Execution</td>");
                                        }
                                        else
                                        {
                                            stringbuilderTOP.Append(@"<td height='5' style='border: 1px solid black; border-collapse: collapse;' align='center'>&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='border: 1px solid black; border-collapse: collapse;background-color:midnightblue;color:white;font-weight: bold;' align='center'>Execution</td>");
                                        }
                                        if (!string.IsNullOrEmpty(systemRootCauseImpact))
                                        {
                                            stringbuilderTOP.Append(@"<td height='5' style='border: 1px solid black; border-collapse: collapse;color: black;font-size:9pt;' align='center'>&#10004;</td><td align='center' style='border: 1px solid black; border-collapse: collapse;background-color:midnightblue;color:white;font-weight: bold;'>System</td>");
                                        }
                                        else
                                        {
                                            stringbuilderTOP.Append(@"<td height='5' style='border: 1px solid black; border-collapse: collapse;' align='center'>&nbsp;&nbsp;&nbsp;&nbsp;</td><td align='center' style='border: 1px solid black; border-collapse: collapse;background-color:midnightblue;color:white;font-weight: bold;'>System</td>");
                                        }
                                        #endregion
                                        stringbuilderTOP.Append(@"</tr></table></td>");
                                        stringbuilderTOP.Append(@"</tr></table>");

                                        stringbuilderTOP.Append(@"<table style='font-family:Cambria; font-size:11pt;width:100%;text-align: justify;text-justify: inter-word;'>");
                                        stringbuilderTOP.Append(@"<tr><ul>");
                                        stringbuilderTOP.Append(@"<li>" + item.RootCost + "</li>");
                                        stringbuilderTOP.Append(@"</ul></tr></table>");

                                        stringbuilderTOP.Append(@"<table style='font-family:Cambria; font-size:11pt;width:100%;'>");
                                        stringbuilderTOP.Append(@"<tr><td style='font-style: italic;font-weight: bold;'>Suggestion(s):</td>");
                                        stringbuilderTOP.Append(@"<tr><td style='text-align: justify;text-justify: inter-word;'><ul>");
                                        stringbuilderTOP.Append(@"<li>" + item.Recomendation + "</li>");
                                        stringbuilderTOP.Append(@"</ul></td></tr></table>");

                                        stringbuilderTOP.Append(@"<div style='font-family:Cambria; font-size:11pt;font-style: italic;font-weight: bold;'>Management Comments(s):</div><br/><table style='font-family:Cambria; font-size:11pt;width:100%;border: 1px solid black;border-collapse: collapse;'><tr><th style='color: midnightblue;border: 1px solid black;border-collapse: collapse;width:20%;'>Responsibility</th><th  style='color: midnightblue;border: 1px solid black;border-collapse: collapse;width:45px;'>Particulars</th></tr><tr><td style='border: 1px solid black;border-collapse: collapse;width:20%;'>Name       :<br/>Department :<br/>Designation :  <br/>Comments   :</td><td>" + item.PersonResponsibleName + "<br/>" + item.DepartmentName + "<br/>" + item.Designation + "<br/>" + item.ManagementResponse + "</td></tr></table>");
                                        stringbuilderTOP.Append(@"<br/>");

                                        //List<ObservationImage> AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFileList(AuditID, item.ATBDID);
                                        //List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetObservationAudioVideoList(AuditID, item.ATBDID);

                                        //if (!string.IsNullOrEmpty(item.BodyContent) || AllinOneDocumentList.Count > 0 || observationAudioVideoList.Count > 0)
                                        //{
                                        //    stringbuilderTOP.Append(@"<label><b><span style='font-family:Cambria; font-size:10pt'>Annexure:</b>" + AnnexueId + "</span></label><br>");
                                        //    stringbuilderTOP.Append(@"<label><b><span style='font-family:Cambria; font-size:10pt'>" + item.AnnexueTitle + "</span></b></label><br/>"); // changed by sagar more on 22-01-2020
                                        //    AnnexueId++;
                                        //}

                                        //if (!string.IsNullOrEmpty(item.BodyContent))
                                        //{
                                        //    stringbuilderTOP.Append(@"<br/>");
                                        //    stringbuilderTOP.Append(@"<P style='margin-top:1%;margin-bottom:1%;font-family:Cambria;font-size:11pt'>" + item.BodyContent.Replace("@nbsp;", "").Replace("\r", "").Replace("\n", "") + "</p>");
                                        //    stringbuilderTOP.Append(@"<br/>");
                                        //}

                                        // added on 08-05-2020 

                                        if (AllinOneDocumentList.Count > 0)
                                        {
                                            stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>"); // added on 08-05-2020
                                            foreach (var ObjImageitem in AllinOneDocumentList)
                                            {
                                                string filePath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                                                if (ObjImageitem.ImagePath != null && File.Exists(filePath))
                                                {
                                                    stringbuilderTOP.Append(@"<p><br/><br/><br/></p>");
                                                    stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Cambria;font-size:11pt;width:70%;'>");
                                                    DocumentPath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                                                    stringbuilderTOP.Append("<img src='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/" + CustomerId + "/"
                                                    + CustomerBranchId + "/" + VerticalID + "/" + item.ProcessId + "/"
                                                    + item.FinancialYear + "/" + item.ForPeriod + "/"
                                                    + AuditID + "/"
                                                    + item.ATBDID + "/1.0/" + ObjImageitem.ImageName + "'alt='d'></img>" + "<br>");
                                                    stringbuilderTOP.Append(@"</p>");
                                                    stringbuilderTOP.Append(@"<br clear=all style='mso-special-character:line-break;page-break-before:always'>");
                                                }
                                            }
                                        }
                                        int audioVideoChar = 97;
                                        if (observationAudioVideoList.Count > 0)
                                        {
                                            foreach (var audioVideoLink in observationAudioVideoList)
                                            {
                                                char alpha = Convert.ToChar(audioVideoChar);
                                                stringbuilderTOP.Append(@"<span style='font-family:Cambria; font-size:11pt'><label>" + alpha + ")</label>&nbsp;&nbsp;<a href=" + audioVideoLink.AudioVideoLink.Trim() + ">" + audioVideoLink.AudioVideoLink.Trim() + "</a></span><br/>");
                                                audioVideoChar++;
                                            }
                                            stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                        }
                                        else
                                        {
                                            stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                        }
                                    }

                                    //V. PENDING INFORMATION
                                    // stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                    stringbuilderTOP.Append(@"<table style='font-family:Cambria; font-size:11pt;width:100%;'>");
                                    stringbuilderTOP.Append(@"<tr><td style='background-color: midnightblue; width:2px; color:white; font-family:Cambria; font-size:13pt;'><strong>V.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PENDING INFORMATION</strong></td></tr></table>");
                                    stringbuilderTOP.Append(@"<hr>");
                                    stringbuilderTOP.Append(@"<table style='border: 1px solid black;border-collapse: collapse;width:80%;font-family: cambria; font-size: 12pt;'><th style='border: 1px solid black;border-collapse: collapse;width:45px;background-color:midnightblue;color:white;'>AREA</th><th style='border: 1px solid black;border-collapse: collapse;width:150px;background-color:midnightblue;color:white'>OBSERVATION</th>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:150px;'>&nbsp;</td>");
                                    stringbuilderTOP.Append(@"</table>");

                                    // VI.	PREVIOUS AUDIT COMPLIANCE
                                    stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                    stringbuilderTOP.Append(@"<table style='font-family:Cambria; font-size:11pt;width:100%;'>");
                                    stringbuilderTOP.Append(@"<tr><td style='background-color: midnightblue; width:2px; color:white; font-family:Cambria; font-size:13pt;'><strong>VI.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PREVIOUS AUDIT COMPLIANCE</strong></td></tr></table>");
                                    stringbuilderTOP.Append(@"<hr>");
                                    stringbuilderTOP.Append(@"<p>&nbsp;</p>");
                                    stringbuilderTOP.Append(@"<p>&nbsp;</p>");
                                    stringbuilderTOP.Append(@"<p>&nbsp;</p>");

                                    //changed on 07-05-2020
                                    stringbuilderTOP.Append(@"<table style='font-family:Cambria; font-size:13pt;border: 1px solid black;border-collapse: collapse;' width='100%'>");
                                    stringbuilderTOP.Append(@"<tr><th style='border: 1px solid black;border-collapse: collapse;width:160px;background-color: midnightblue; color:white; '>Area</th><th style='border: 1px solid black;border-collapse: collapse;width:45px;background-color: midnightblue; color:white; '>Complied</th><th style='border: 1px solid black;border-collapse: collapse;width:45px;background-color: midnightblue; color:white; '>In progress</th><th style='border: 1px solid black;border-collapse: collapse;width:45px;background-color: midnightblue; color:white; '>Not complied</th></tr>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:160px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td></tr>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:160px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td></tr>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:160px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td></tr>");
                                    stringbuilderTOP.Append(@"<tr><td style='border: 1px solid black;border-collapse: collapse;width:160px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td><td style='border: 1px solid black;border-collapse: collapse;width:45px;'>&nbsp;</td></tr>");
                                    stringbuilderTOP.Append(@"</table>");

                                    //bottom Image 
                                    //stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");
                                    //stringbuilderTOP.Append(@"<p>&nbsp;</p>");
                                    //stringbuilderTOP.Append(@"<p>&nbsp;</p>");
                                    //stringbuilderTOP.Append(@"<p>&nbsp;</p>");
                                    //stringbuilderTOP.Append(@"<p>&nbsp;</p>");
                                    //stringbuilderTOP.Append(@"<table align='center' style='width:70%; margin-bottom:2%;'>");
                                    //stringbuilderTOP.Append(@"<tr><td colspan = '2' style='text-align: center;'><img src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/Logo.jpg' style='display: block; margin: 160px auto 20px;' width='207' height='81'/><td/></tr>");
                                    //stringbuilderTOP.Append(@"<tr><td colspan = '2' style='text-align: center;'><img src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/GurujanaThankYouLogon.png' style='display: block; margin: 160px auto 20px;' width='210' height='58'/><td/></tr>");
                                    //stringbuilderTOP.Append(@"</table>");
                                    ProcessRequestWord(stringbuilderTOP.ToString(), CustomerBranchName, ReportNameAndNumber, custName, periodForFooter);
                                    #endregion
                                }
                                #endregion
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "There is no Data for download.";
                                }
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        }
                        #endregion
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "You need to generate report first";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void ProcessRequestWord(string contexttxt, string LocationName, string ReportNameAndNumber, string custName, string forPeriod)
        {
            string hpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/Logo.jpg";
            string context = contexttxt
            .Replace(Environment.NewLine, "<br />")
            .Replace("\r", "<br />")
            .Replace("\n", "<br />")
            .Replace("“", " ")
            .Replace("”", " ");

            //li.MsoHeader, div.MsoHeader
            System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
            sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->

                
                <style>
                 p.MsoHeader,li.MsoHeader, div.MsoHeader{
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                 }
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                    margin:0in;
                    margin-bottom:.0001pt;
                    mso-pagination:widow-orphan;
                    tab-stops:left 3.0in right 6.0in;
                    tab-stops:center 3.0in right 6.0in;
                    tab-stops:right 3.0in right 6.0in;
                    font-size:14.0pt;
                }
                

                <!-- /* Style Definitions */
                @page
                {
                    mso-page-orientation: portrait; 
                    size: 21cm 29.7cm;
                    margin:1cm 1cm 1cm 1cm;
                }
                @page Section1
                {                    
                    margin: 2.25cm 1.48cm 2.25cm 1.9cm;
                    mso-header-margin:.1in;
                    mso-header:h1;
                    mso-footer: f1; 
                    mso-footer-margin:.2in;
                    border: 1px solid black;
                    outline: 4px groove; 
                    outline-offset: 10px;  
                    line-height: 1.15;                 
                }


                div.Section1
                {
                    page:Section1;
                }
                table#hrdftrtbl
                {
                        margin:0in 0in 0in 900in;
                        width:1px;
                        height:1px;
                        overflow:hidden;
                }

                -->
                </style></head>");
            //
            sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in;'> <div class=Section1>");
            sbTop.Append(context);

            sbTop.Append(@"<table id='hrdftrtbl'  style='width:100%;' border='1' cellspacing='0' cellpadding='0'>" +
               "<tr><td><div style='mso-element:header' id=h1>");

            sbTop.Append(@"<table style='width:100%;'>" + "<tr><td style='float:left'><p></p></td></tr>" + "</table>");//<img height='63' width='127' src='" + hpath + "' alt ='d'></img>

            sbTop.Append(@"<p class=MsoHeader style='color:MediumSeaGreen;border:none;mso-border-bottom-alt:solid windowtext .75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'>" +
            "<o:p></o:p>&nbsp;</p>");
            sbTop.Append("</div>");

            //changed on 30-04-2020
            sbTop.Append("<div style='mso-element:footer' id=f1><p class=MsoFooter style='border:none;mso-border-bottom-alt:solid windowtext .75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in'><o:p></o:p>&nbsp;</p><div style='font-family:Cambria; font-size:11pt;'>" + custName + " &mdash; Internal Audit Report- for the period (" + forPeriod + ")<br/><div style='font-family:Cambria; font-size:10pt;font-style: italic;'>Private and confidential</div><div align='right'>Page <span style='mso-field-code: PAGE; display: block; float: right; font-family:Cambria; font-size: 10pt;'> <span style='mso-no-proof:yes'>1</span></span> of <span style='mso-field-code: NUMPAGES '></span></div></div></div>");
            sbTop.Append(@"</table>");
            string replaceChar = ReportNameAndNumber.Replace('/', '_');
            string fileName = "Internal Audit Report_" + replaceChar + ".doc";
            sbTop.Append("</body></html>");
            string strBody = sbTop.ToString();
            Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
            #region Word Download Code
            Response.AppendHeader("Content-Type", "application/msword");
            Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName);
            Response.Write(strBody);
            #endregion

        }
        public void ProcessRequestRahul(string contexttxt)
        {

            string Headerpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/AL_Logo_C.png";
            string Footerpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/AL_Footer.png";
            string context = contexttxt
            .Replace(Environment.NewLine, "<br />")
            .Replace("\r", "<br />")
            .Replace("\n", "<br />");

            System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
            sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->




                <!-- /* Style Definitions */
                <style>
                @page
                {
                    mso-page-orientation: landscape; 
                    size:29.7cm 21cm;
                    margin:1cm 1cm 1cm 1cm;
                }
                @page Section1
                {                                                                      
                    margin:1.5in .5in .5in .5in ;
                    mso-header-margin:.5in;
                    mso-header:h1;
                    mso-footer: f1; 
                    mso-footer-margin:.5in;
                    font-family:Cambria;
                    border: 1px solid black;
                    outline: 4px groove; 
                    outline-offset: 10px;

                }

                div.Section1
                {
                    page:Section1;
                }               
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 900in;
                    width:1px;
                    height:1px;
                    overflow:hidden;
                }
                -->
                </style></head>");


            sbTop.Append("@<body style='tab-interval:.5in'><div class='Section1'>");
            sbTop.Append(context);
            sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:right;font-family:Cambria;'><b>");
            sbTop.Append("<img src ='" + Headerpath + "' alt ='d'></img ></b></p>");

            sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");
            string strBody = sbTop.ToString();
            Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);

            #region Word Download Code
            string abc = "ABC";
            string NameVertical = "testrahul";
            Response.AppendHeader("Content-Type", "application/msword");
            Response.AppendHeader("Content-disposition", "attachment; filename=" + abc + "_" + NameVertical + ".doc");
            Response.Write(strBody);
            #endregion
            //}
        }
        public void ProcessRequest_AsPerClientFirst(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            //if (VerticalID != -1)
            //{
            //    long AuditID = -1;
            //    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
            //    {
            //        AuditID = Convert.ToInt64(ViewState["AuditID"]);
            //    }
            string Headerpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/AL_Logo_C.png";
            string Footerpath = System.Configuration.ConfigurationManager.AppSettings["LoginURL"] + "/ObservationImages/AL_Footer.png";
            string context = contexttxt
            .Replace(Environment.NewLine, "<br />")
            .Replace("\r", "<br />")
            .Replace("\n", "<br />");

            //string details = "Audit Report for the FY (" + FinancialYear + ")";
            System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
            sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->

                <!-- /* Style Definitions */
                @page
                {
                    mso-page-orientation: landscape; 
                    size:29.7cm 21cm;
                    margin:1cm 1cm 1cm 1cm;
                }
                @page Section1
                {                                                                      
                    margin:1.5in .5in .5in .5in ;
                    mso-header-margin:.5in;
                    mso-header:h1;
                    mso-footer: f1; 
                    mso-footer-margin:.5in;
                    font-family:Cambria;
                    border: 1px solid black;
                    outline: 4px groove; 
                    outline-offset: 10px;
                }
               
                div.Section1
                {
                    page:Section1;
                }               
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 900in;
                    width:1px;
                    height:1px;
                    overflow:hidden;
                }
                -->
                </style></head>");

            sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

            sbTop.Append(context);
            sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                    <div style='mso-element:header' id=h1 >
                    <p class=MsoHeader style='text-align:right;font-family:Cambria;'><b>");

            sbTop.Append("<img src ='" + Headerpath + "' alt ='d'  ></img >");
            sbTop.Append(@"</div></td>);

                  sbTop.Append(<td>             
                    <div style='mso-element:footer' id=h1 >
                    <p class=MsoFooter style='text-align:right;font-family:Cambria;'><b>");

            sbTop.Append("<img src ='" + Footerpath + "' alt ='d'  ></img >");
            sbTop.Append(@"</div>
                </td></tr>
                </table>
                </body></html>
                ");

            string strBody = sbTop.ToString();
            Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
            //if (string.IsNullOrEmpty(PeriodName))
            //{
            //    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
            //    if (peridodetails != null)
            //    {
            //        if (peridodetails.Count > 0)
            //        {
            //            foreach (var ForPeriodName in peridodetails)
            //            {
            //                string fileName = "Consolidated Location Report" + FinancialYear + ForPeriodName + ".doc";
            //                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
            //                {
            //                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
            //                    FinancialYear = Convert.ToString(FinancialYear),
            //                    Period = Convert.ToString(ForPeriodName),
            //                    CustomerId = customerID,
            //                    VerticalID = Convert.ToInt32(VerticalID),
            //                    FileName = fileName,
            //                    AuditID = AuditID,

            //                };
            //                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
            //                string directoryPath1 = "";
            //                bool Success1 = false;
            //                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
            //                {
            //                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
            //                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
            //                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
            //                   + "/1.0");
            //                    try
            //                    {
            //                        Directory.Delete(directoryPath1);
            //                    }
            //                    catch (Exception ex) { }
            //                    if (!Directory.Exists(directoryPath1))
            //                    {
            //                        DocumentManagement.CreateDirectory(directoryPath1);
            //                    }
            //                    Guid fileKey1 = Guid.NewGuid();
            //                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
            //                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
            //                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
            //                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
            //                    FinalDeleverableUpload.Version = "1.0";
            //                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
            //                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
            //                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            //                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
            //                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
            //                }
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    string fileName = "Consolidated Location Report" + FinancialYear + PeriodName + ".doc";
            //    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
            //    {
            //        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
            //        FinancialYear = Convert.ToString(FinancialYear),
            //        Period = Convert.ToString(PeriodName),
            //        CustomerId = customerID,
            //        VerticalID = Convert.ToInt32(VerticalID),
            //        FileName = fileName,
            //        AuditID = AuditID,
            //    };
            //    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
            //    string directoryPath1 = "";

            //    bool Success1 = false;
            //    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
            //    {
            //        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
            //       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
            //       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
            //       + "/1.0");
            //        try
            //        {
            //            Directory.Delete(directoryPath1);
            //        }
            //        catch (Exception ex) { }
            //        if (!Directory.Exists(directoryPath1))
            //        {
            //            DocumentManagement.CreateDirectory(directoryPath1);
            //        }

            //        Guid fileKey1 = Guid.NewGuid();
            //        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
            //        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
            //        FinalDeleverableUpload.FileName = fileName;
            //        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
            //        FinalDeleverableUpload.FileKey = fileKey1.ToString();
            //        FinalDeleverableUpload.Version = "1.0";
            //        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
            //        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
            //        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            //        DocumentManagement.Audit_SaveDocFiles(Filelist1);
            //        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
            //    }
            //}
            #region Word Download Code
            string abc = "ABC";
            string NameVertical = "testrahul";
            Response.AppendHeader("Content-Type", "application/msword");
            Response.AppendHeader("Content-disposition", "attachment; filename=" + abc + "_" + NameVertical + ".doc");
            Response.Write(strBody);
            #endregion
            //}
        }
        public void ProcessRequest_AsPerClientSECOND(string contexttxt, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                string context = contexttxt
                .Replace(Environment.NewLine, "<br />")
                .Replace("\r", "<br />")
                .Replace("\n", "<br />");

                string details = "Audit Report for the FY (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.5in .5in .5in .5in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Cambria;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left;font-family:Cambria;'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "Audit Committee Report" + FinancialYear + ForPeriodName + ".doc";
                                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,
                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    //try { 
                                    //Directory.Delete(directoryPath1);
                                    //}
                                    //catch (Exception ex) { }
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                    FinalDeleverableUpload.Version = "1.0";
                                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "Audit Committee Report" + FinancialYear + PeriodName + ".doc";
                    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                        AuditID = AuditID,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                    {
                        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        FinalDeleverableUpload.FileName = fileName;
                        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        FinalDeleverableUpload.Version = "1.0";
                        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                    }
                }
                #region Word Download Code
                //string abc = "ABC";
                //string NameVertical = "testrahul";
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + abc + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }
        public void ProcessRequest(string context, int customerID, string CompanyName, string PeriodName, string FinancialYear, int VerticalID, int CustomerBranchId)
        {
            if (VerticalID != -1)
            {
                long AuditID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["AuditID"])))
                {
                    AuditID = Convert.ToInt64(ViewState["AuditID"]);
                }
                string details = "Audit Report for the (" + FinancialYear + ")";
                System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
                sbTop.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->


                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Cambria;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

                sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                sbTop.Append(context);
                sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>
                <div style='mso-element:header' id=h1 >
                <p class=MsoHeader style='text-align:left'>");
                sbTop.Append(CompanyName.Trim() + "<br>" + details + "</p>");
                sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Draft
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

                string strBody = sbTop.ToString();
                Byte[] fileBytes = Encoding.ASCII.GetBytes(strBody);
                if (string.IsNullOrEmpty(PeriodName))
                {
                    var peridodetails = RiskCategoryManagement.GetAuditClosurePeriodZEE(CustomerBranchId, VerticalID, FinancialYear, AuditID);
                    if (peridodetails != null)
                    {
                        if (peridodetails.Count > 0)
                        {
                            foreach (var ForPeriodName in peridodetails)
                            {
                                string fileName = "OpenObservationReport" + FinancialYear + ForPeriodName + ".doc";
                                Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                                {
                                    CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                                    FinancialYear = Convert.ToString(FinancialYear),
                                    Period = Convert.ToString(ForPeriodName),
                                    CustomerId = customerID,
                                    VerticalID = Convert.ToInt32(VerticalID),
                                    FileName = fileName,
                                    AuditID = AuditID,
                                };
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                                string directoryPath1 = "";
                                bool Success1 = false;
                                if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                                {
                                    directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                                   + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                                   + Convert.ToString(FinancialYear) + "/" + ForPeriodName + "/"
                                   + "/1.0");
                                    if (!Directory.Exists(directoryPath1))
                                    {
                                        DocumentManagement.CreateDirectory(directoryPath1);
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                                    FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    FinalDeleverableUpload.FileKey = fileKey1.ToString();
                                    FinalDeleverableUpload.Version = "1.0";
                                    FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                                    FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                    Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                                }
                            }
                        }
                    }
                }
                else
                {
                    string fileName = "OpenObservationReport" + FinancialYear + PeriodName + ".doc";
                    Tran_FinalDeliverableUpload FinalDeleverableUpload = new Tran_FinalDeliverableUpload()
                    {
                        CustomerBranchId = Convert.ToInt32(CustomerBranchId),
                        FinancialYear = Convert.ToString(FinancialYear),
                        Period = Convert.ToString(PeriodName),
                        CustomerId = customerID,
                        VerticalID = Convert.ToInt32(VerticalID),
                        FileName = fileName,
                        AuditID = AuditID,
                    };
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    string directoryPath1 = "";

                    bool Success1 = false;
                    if (!RiskCategoryManagement.Tran_FinalDeliverableUploadResultExists(FinalDeleverableUpload))
                    {
                        directoryPath1 = Server.MapPath("~/AuditFinalDeleverableDocument/draft/" + customerID + "/"
                       + Convert.ToInt32(CustomerBranchId) + "/" + Convert.ToInt32(VerticalID) + "/"
                       + Convert.ToString(FinancialYear) + "/" + PeriodName + "/"
                       + "/1.0");
                        if (!Directory.Exists(directoryPath1))
                        {
                            DocumentManagement.CreateDirectory(directoryPath1);
                        }

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(fileName));
                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, fileBytes));
                        FinalDeleverableUpload.FileName = fileName;
                        FinalDeleverableUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                        FinalDeleverableUpload.FileKey = fileKey1.ToString();
                        FinalDeleverableUpload.Version = "1.0";
                        FinalDeleverableUpload.VersionDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedDate = DateTime.Today.Date;
                        FinalDeleverableUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                        Success1 = RiskCategoryManagement.CreateTran_FinalDeliverableUploadResult(FinalDeleverableUpload);
                    }
                }
                #region Word Download Code
                //string NameVertical = VerticalName.VerticalName;
                //Response.AppendHeader("Content-Type", "application/msword");
                //Response.AppendHeader("Content-disposition", "attachment; filename=" + companyname + "_" + NameVertical + ".doc");
                //Response.Write(strBody);
                #endregion
            }
        }

        protected void grdImplementationAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    //int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                    //string ForMonth = Convert.ToString(commandArgs[1]);
                    //string FinancialYear = Convert.ToString(commandArgs[2]);
                    //int Verticalid = Convert.ToInt32(commandArgs[3]);
                    long AuditID = Convert.ToInt64(commandArgs[4]);

                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString() + "&AuditID=" + AuditID, false);
                    }
                    else
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=" + "&AuditID=" + AuditID, false);
                    }
                    // }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ShowImplementationGrid(object sender, EventArgs e)
        {
            liImplementation.Attributes.Add("class", "active");
            liProcess.Attributes.Add("class", "");
            ViewState["Type"] = "Implementation";

            BindData();
            int chknumber = Convert.ToInt32(grdProcessAudits.PageIndex);

            if (chknumber > 0)
            {
                chknumber = chknumber + 1;
                DropDownListPageNo.SelectedValue = (chknumber).ToString();
            }
        }

        protected void ShowProcessGrid(object sender, EventArgs e)
        {
            liProcess.Attributes.Add("class", "active");
            liImplementation.Attributes.Add("class", "");
            ViewState["Type"] = "Process";

            BindData();

            int chknumber = Convert.ToInt32(grdProcessAudits.PageIndex);

            if (chknumber > 0)
            {
                chknumber = chknumber + 1;
                DropDownListPageNo.SelectedValue = (chknumber).ToString();
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void export_Click(object sender, EventArgs e)
        {
            #region Word Report             
            System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();
            System.Text.StringBuilder stringbuilderTOPSecond = new System.Text.StringBuilder();
            string AditHeadName = string.Empty;
            string BranchName = string.Empty;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var itemlist = (from row in entities.SPGETObservationDetails(15475, "2020-2021", 1017, "", 43)
                                select row).ToList();

                #region first Report
                stringbuilderTOP.Append(@"<style>.break { page-break-after: always;}</style>");
                stringbuilderTOP.Append(@"<p style='margin-top:1%; font-family:Cambria; font-size:12pt; text-align: center;'><b>"
                + " Area: Review of XYZ Program</b></p>");

                stringbuilderTOP.Append(@"<p style='font-family:Cambria; font-size:12pt; text-align: center;'><b>"
               + " Location: ALCOB</b></p>");

                stringbuilderTOP.Append(@"<p style='font-family:Cambria; font-size:12pt; text-align: center;'><b>"
               + "Date of Report: " + DateTime.Now.ToString("MMM dd, yyyy") + "</b></p>");

                stringbuilderTOP.Append(@"<p style='font-family:Cambria; font-size:12pt; text-align: center;'><b><u>"
               + "Part B: Internal Audit report </u></b></p>");

                stringbuilderTOP.Append(@"<table style='width:100%; font-family:Cambria; border-collapse: collapse; border: 1px solid black;'>" +
                "<tr><td style='width:50%;border: 1px solid black; padding:10px;'><b>Distribution:</b></td><td rowspan='4' style='width:50%;border: 1px solid black; padding:10px;'>" +
                "<table style='width:100%; font-family:Cambria;'>" +
                "<tr><td><b>Audit duration:</b><br/><br/><br/></td></tr>" +
                "<tr><td><b>Closing meeting:</b><br/><br/><br/></td></tr>" +
                "<tr><td><b>Draft report:</b><br/><br/><br/></td></tr> " +
                "<tr><td><b>Management response:</b><br/><br/><br/></td></tr>" +
                "<tr><td><b>Audit team member(s):</b><br/><br/><br/></td></tr>" +
                "<tr><td><b>Report reference:</b><br/><br/><br/></td></tr>" +
                "</table>" +
                "</td>" +
                "</tr>" +
                "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head -" + BranchName + ":" + AditHeadName + "</td></tr>" +
                "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + BranchName + ":" + AditHeadName + "</td></tr>" +
                "<tr><td style='width:50%;border: 1px solid black; padding:10px;'>Head - " + BranchName + ":" + AditHeadName + "</td></tr></table>");

                stringbuilderTOP.Append(@"<br clear=all style ='mso-special-character:line-break;page-break-before:always'>");

                stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Cambria; font-size:10pt; text-align: center;'><b> Table of Contents </b><br/><br/><br/><br/>");

                stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Cambria; font-size:10pt; text-align: left;'><b> 1. Background: </b><br/><br/><br/><br/></b><br/>");
                stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Cambria; font-size:10pt; text-align: left;'><b> 2. Audit Scope and Objective: </b><br/><br/><br/><br/></b><br/></b><br/>");
                stringbuilderTOP.Append(@"<p style='margin-left:15px;margin-top:1%;margin-bottom:1%; font-family:Cambria; font-size:10pt; text-align: left;'><b> Objective(s) of the review:  </b><br/><br/><br/><br/></b><br/></b><br/>");
                stringbuilderTOP.Append(@"<p style='margin-top:1%;margin-bottom:1%; font-family:Cambria; font-size:12pt; text-align: left;'><b> 3. Detailed Findings: </b><br/><br/><br/><br/></b><br/></b><br/>");
                stringbuilderTOP.Append(@"<style>.break { page-break-after: always;}</style>");

                int SrNo = 01;
                stringbuilderTOP.Append(@"<table style='width:100%; font-family:Cambria; border-collapse: collapse; border-top: 1px solid black;'>");
                foreach (var item in itemlist)
                {
                    string ColorCode = string.Empty;

                    stringbuilderTOP.Append(@"<tr> 
                     <td colspan='2' style='border-top: 1px solid black; background-color:#F3F2F2; font-weight:bold'>Issue # " + SrNo + " : " + item.ObservationTitle + "</td>");
                    if (item.ObservationRating == 1)
                    {
                        stringbuilderTOP.Append(@"<td style='border-top: 1px solid black; background-color:#FDB924; font-weight:bold'>High/" + item.ObservationCategoryName + "</td></tr>");
                    }
                    else if (item.ObservationRating == 2)
                    {
                        stringbuilderTOP.Append(@"<td style='border-top: 1px solid black; background-color:#FFFF00; font-weight:bold'>Medium/" + item.ObservationCategoryName + "</td></tr>");
                    }
                    else if (item.ObservationRating == 3)
                    {
                        stringbuilderTOP.Append(@"<td style='border-top: 1px solid black; background-color:#92D050; font-weight:bold'>Low/" + item.ObservationCategoryName + "</td></tr>");
                    }
                    stringbuilderTOP.Append(@"<tr><td colspan='3' style='font-family:Cambria; font-size:11pt;'>" + item.ObjBackground + "</td ></tr>" +
                    "<tr><td style='border-top: 1px solid black; font-weight:bold'>Finding(s)</td><td style='border-top: 1px solid black;font-weight:bold'>Probable Cause(s)</td>" +
                    "<td style='border-top: 1px solid black;font-weight:bold'>Recommendation(s)</td></tr>" +
                    "<tr><td style='border-top: 1px solid black;'>" + item.Observation + "</td><td style='border-top: 1px solid black;'>" + item.RootCost + "</td><td style='border-top: 1px solid black;'>" + item.Recomendation + "</td></tr>");

                    stringbuilderTOP.Append(@"<tr><td></td><td style='font-weight:bold'>Impact/Potential Risk(s)</td><td style='font-weight:bold'>Management Action Plan(s)</td></tr>");

                    stringbuilderTOP.Append(@"<tr><td></td><td style='border-bottom:1px solid black;'>" + item.FinancialImpact + "</td><td style='border-bottom:1px solid black;'>" + item.ManagementResponse + "</td></tr>");
                    stringbuilderTOP.Append(@"<tr><td></td><td></td><td>Responsibility: " + item.PersonResponsibleName + "</td></tr>");
                    stringbuilderTOP.Append(@"<tr><td></td><td></td><td>Timeline:" + item.TimeLine != null ? item.TimeLine.Value.ToString("dd-MMM-yyyy") : null + "</td></tr><br/><br/>");

                    //Image
                    //stringbuilderTOP.Append(@"<tr>" +
                    //"<td style='width:8%;vertical-align: top;padding:10px;'></td>" +
                    //"<td style='width:92%;vertical-align: top;padding:10px;'><div style='width:250px'> ");


                    //List<ObservationImage> AllinOneDocumentList = RiskCategoryManagement.GetObservationImageFileList(AuditID, item.ATBDId);
                    //if (AllinOneDocumentList.Count > 0)
                    //{
                    //    foreach (var ObjImageitem in AllinOneDocumentList)
                    //    {
                    //        string filePath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                    //        if (ObjImageitem.ImagePath != null && File.Exists(filePath))
                    //        {
                    //            DocumentPath = Path.Combine(Server.MapPath(ObjImageitem.ImagePath), ObjImageitem.ImageName);
                    //            // DocumentPath = filePath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                    //            stringbuilderTOP.Append("<img src ='" + System.Configuration.ConfigurationManager.AppSettings["LoginURL"].ToString() + "/ObservationImages/" + CustomerId + "/"
                    //            + item.Customerbranchid + "/" + item.VerticalID + "/" + item.ProcessId + "/"
                    //            + item.FinancialYear + "/" + item.ForPerid + "/"
                    //            + AuditID + "/" +
                    //            +item.ATBDId + "/1.0/" + ObjImageitem.ImageName + "' alt ='d'></img >" + "<br>");
                    //        }
                    //    }
                    //}
                    //stringbuilderTOP.Append(@"</div></td><td></td></tr>");
                    SrNo++;
                }


                stringbuilderTOP.Append(@"</table>");
                stringbuilderTOP.Append(@"</table>");
                ProcessRequestWord(stringbuilderTOP.ToString(), "", null, null, null);
                #endregion                
            }
            #endregion
        }

        private void BindCustomerList()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDLForMaster(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
                ddlCustomer.SelectedIndex = 2;
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    BindLegalEntityData();
                    BindData();
                }
                else
                {
                    ddlLegalEntity.DataSource = null;
                    ddlLegalEntity.DataBind();

                    grdProcessAudits.DataSource = null;
                    grdProcessAudits.DataBind();

                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                }
            }
        }

    }
}