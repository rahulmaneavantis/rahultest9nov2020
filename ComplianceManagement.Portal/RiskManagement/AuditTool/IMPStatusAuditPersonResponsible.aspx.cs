﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class IMPStatusAuditPersonResponsible : System.Web.UI.Page
    {
        protected string FinYear;
        protected string Period;
        protected int BranchId;
        protected int ResultID;
        protected int VerticalID;
        protected int ScheduledOnId;
        protected static string IsAfterPerformer;
        protected int AuditID;
        protected static int CustomerId = 0;
        protected int AuditStatusID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerId"]))
                {
                    CustomerId = Convert.ToInt32(Request.QueryString["CustomerId"].ToString());
                    Session["CustomerIdFromQueryString"] = Request.QueryString["CustomerId"].ToString();
                }
              
                BindUsersNEWIMP();
                BindCustomer();
                BindFinancialYear();
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Request.QueryString["FinYear"];
                    ViewState["FinYear"] = Request.QueryString["FinYear"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchId = Convert.ToInt32(Request.QueryString["BID"]);
                    ViewState["BID"] = Request.QueryString["BID"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
                    ViewState["ResultID"] = Request.QueryString["ResultID"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                    ViewState["ForMonth"] = Request.QueryString["ForMonth"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                    ViewState["VID"] = Request.QueryString["VID"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                    ViewState["scheduledonid"] = Request.QueryString["scheduledonid"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditStatusID"]))
                {
                    AuditStatusID = Convert.ToInt32(Request.QueryString["AuditStatusID"]);
                    ViewState["AuditStatusID"] = Request.QueryString["AuditStatusID"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                    ViewState["AuditID"] = Request.QueryString["AuditID"];
                }
                OpenTransactionPageIMP(ResultID, BranchId, FinYear, Period, VerticalID, ScheduledOnId, AuditID);
            }
            DateTime date = DateTime.MinValue;
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker2", string.Format("initializeConfirmDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
        }

        public void BindCustomer()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(Session["CustomerIdFromQueryString"].ToString());
            }
            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(CustomerId);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void BindUsersNEWIMP()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(Session["CustomerIdFromQueryString"].ToString());
            }
            ddlIMPNewPersonresponsible.Items.Clear();
            ddlIMPNewPersonresponsible.DataTextField = "Name";
            ddlIMPNewPersonresponsible.DataValueField = "ID";
            ddlIMPNewPersonresponsible.DataSource = RiskCategoryManagement.FillUsers(CustomerId);
            ddlIMPNewPersonresponsible.DataBind();
            ddlIMPNewPersonresponsible.Items.Insert(0, new ListItem("< Select Person Responsible >", "-1"));
        }

        private void BindTransactionsIMP(int ScheduledOnID, int ResultID, int Auditid)
        {
            try
            {
                grdTransactionIMPHistory.DataSource = Business.DashboardManagementRisk.GetAllImplementationAuditTransactionViews(ScheduledOnID, ResultID, Auditid);
                grdTransactionIMPHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRemarksIMP(string ForPeriod, string FinancialYear, int ScheduledOnID, int ResultID, long AuditID)
        {
            try
            {
                GrdIMPRemark.DataSource = Business.DashboardManagementRisk.GetImplementationReviewAllRemarks(ForPeriod, FinancialYear, ScheduledOnID, ResultID, AuditID);
                GrdIMPRemark.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindDocumentIMP(int ScheduledOnID, int resultID, long AuditID)
        {
            try
            {
                List<GetImplementationAuditDocumentsView> ComplianceDocument = new List<GetImplementationAuditDocumentsView>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetImplementationAuditDocumentsView(ScheduledOnID, resultID, AuditID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptIMPComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptIMPComplianceDocumnets.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlIMPFinalStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlIMPFinalStatus.SelectedValue))
            {
                if (ddlIMPFinalStatus.SelectedValue != "-1")
                {
                    if (ddlIMPFinalStatus.SelectedValue == "5")
                    {
                        TimelineTr.Visible = true;
                        RequiereValidateTimeline.Enabled = true;
                        personresponsile.Visible = false;
                        lblTimeline.InnerText = "Date of Implementation";
                        RequiereValidateTimeline.Enabled = true;
                        RequiereValidateTimeline.ErrorMessage = "Date of Implementation can not be empty.";
                    }
                    else if (ddlIMPFinalStatus.SelectedValue == "3")
                    {
                        TimelineTr.Visible = true;
                        RequiereValidateTimeline.Enabled = true;
                        personresponsile.Visible = false;
                        lblTimeline.InnerText = "Revised Date of Implementation";
                        RequiereValidateTimeline.Enabled = true;
                        RequiereValidateTimeline.ErrorMessage = "Revised Date of Implementation can not be empty.";
                    }
                    else if (ddlIMPFinalStatus.SelectedValue == "4")
                    {
                        TimelineTr.Visible = false;
                        RequiereValidateTimeline.Enabled = false;
                    }
                    else
                    {
                        lblTimeline.InnerText = "TimeLine";
                        RequiereValidateTimeline.Enabled = false;
                        RequiereValidateTimeline.ErrorMessage = "Time Line can not be empty.";
                        TimelineTr.Visible = true;
                        RequiereValidateTimeline.Enabled = true;
                    }
                }
                else
                {
                    lblTimeline.InnerText = "TimeLine";
                    RequiereValidateTimeline.Enabled = true;
                    RequiereValidateTimeline.ErrorMessage = "Time Line can not be empty.";
                }
            }
        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                        {
                            AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                            int count = 0;
                            count = UserManagementRisk.GetPhaseCountNew(Convert.ToInt32(ddlFilterLocation.SelectedValue), AuditID);
                            BindAuditSchedule("P", count);
                        }
                    }
                }
            }
        }

        public void OpenTransactionPageIMP(int resultID, int custbranchid, string FinancialYear, string Forperiod, int VerticalID, int ScheduledonID, long AuditID)
        {
            try
            {
                var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(FinancialYear, Forperiod, custbranchid, VerticalID, ScheduledonID, AuditID);
                var RecentComplianceTransaction = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceIDIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), Convert.ToInt32(resultID));
                if (getauditimplementationscheduledetails != null)
                {
                    txtIMPNewTimeLine.Text = string.Empty;
                    txtIMPNewManagementResponse.Text = string.Empty;
                    txtIMPFinalStatusRemark.Text = string.Empty;
                    var MstRiskResultPrevious = RiskCategoryManagement.GetInternalControlAuditResultbyIMPlementationID(resultID, VerticalID, AuditID);

                    var MstRiskResult = RiskCategoryManagement.GetImplementationAuditResultIMPlementationID(resultID, VerticalID, Convert.ToInt32(getauditimplementationscheduledetails.Id), AuditID);
                    if (MstRiskResult != null)
                    {
                        txtIMPFinalStatusRemark.Text = MstRiskResult.FixRemark;
                        txtIMPNewManagementResponse.Text = MstRiskResult.ManagementResponse;
                        txtIMPNewTimeLine.Text = MstRiskResult.TimeLine != null ? MstRiskResult.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.PersonResponsible)))
                        {
                            ddlIMPNewPersonresponsible.SelectedValue = Convert.ToString(MstRiskResult.PersonResponsible);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ImplementationStatus)))
                        {
                            if (MstRiskResult.ImplementationStatus != 0)
                            {
                                ddlIMPFinalStatus.SelectedValue = Convert.ToString(MstRiskResult.ImplementationStatus);
                                if (ddlIMPFinalStatus.SelectedValue == "4" || ddlIMPFinalStatus.SelectedValue == "5")
                                {
                                    ddlIMPFinalStatus_SelectedIndexChanged(null, null);
                                }
                            }
                        }
                        AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                        {
                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                            ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                            FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                            CustomerBranchId = custbranchid,
                            ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                            ForPeriod = getauditimplementationscheduledetails.ForMonth,
                            ResultID = ResultID,
                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            VerticalID = VerticalID,
                            ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                            AuditID = AuditID,
                        };
                        var AuditteeStatus = RiskCategoryManagement.GetIMPLatestAuditteeStatus(transaction);
                        if (AuditteeStatus != null)
                        {
                            if (AuditteeStatus != "PA")
                            {
                                btnIMPSave.Enabled = false;
                                tbxDateIMP.Enabled = false;
                                txtIMPReviewRemark.Enabled = false;
                                GrdIMPRemark.Enabled = false;
                                btnIMPNext3.Enabled = false;
                                txtIMPNewTimeLine.Enabled = false;
                                ddlIMPNewPersonresponsible.Enabled = false;
                                ddlIMPFinalStatus.Enabled = false;
                            }
                        }
                        if (RecentComplianceTransaction.AuditStatusID == 3)
                        {
                            if (RecentComplianceTransaction.ImplementationAuditStatusID == 3 || RecentComplianceTransaction.ImplementationAuditStatusID == 2)
                            {
                                btnIMPSave.Enabled = true;
                                tbxDateIMP.Enabled = true;
                                txtIMPReviewRemark.Enabled = true;
                                GrdIMPRemark.Enabled = true;
                                btnIMPNext3.Enabled = true;
                                txtIMPNewTimeLine.Enabled = true;
                                ddlIMPNewPersonresponsible.Enabled = true;
                                ddlIMPFinalStatus.Enabled = true;
                                txtIMPNewManagementResponse.Text = string.Empty;
                                txtIMPNewTimeLine.Text = string.Empty;
                                ddlIMPFinalStatus.SelectedValue = "-1";
                            }
                        }
                        if (RecentComplianceTransaction != null)
                        {
                            tbxDateIMP.Text = RecentComplianceTransaction.StatusChangedOn != null ? RecentComplianceTransaction.StatusChangedOn.Value.ToString("dd-MM-yyyy") : " ";
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.PersonResponsible)))
                        {
                            var TimeLineHistory = RiskCategoryManagement.GetIMPTimeLineHistory(ResultID, Convert.ToInt32(AuditID), Convert.ToInt32(MstRiskResult.PersonResponsible));
                            grdTimelineHistory.DataSource = TimeLineHistory;
                            grdTimelineHistory.DataBind();
                        }
                        liReviewHistory.Visible = true;
                        btnIMPNext3.Text = "Next";
                    }
                    else
                    {
                        liReviewHistory.Visible = false;
                        btnIMPNext3.Text = "Submit";
                    }
                    var getDated = RiskCategoryManagement.GetInternalAuditTransactionbyIDIMP(getauditimplementationscheduledetails.Id, Convert.ToInt32(resultID), FinancialYear, Forperiod, 4, VerticalID, AuditID);
                    if (getDated != null)
                    {
                        tbxDateIMP.Text = getDated.Dated != null ? getDated.Dated.Value.ToString("dd-MM-yyyy") : null;
                    }
                    BindDocumentIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), Convert.ToInt32(resultID), AuditID);
                    BindTransactionsIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), Convert.ToInt32(resultID), Convert.ToInt32(AuditID));
                    if (!string.IsNullOrEmpty(FinancialYear))
                    {
                        BindRemarksIMP(Forperiod, FinancialYear, Convert.ToInt32(getauditimplementationscheduledetails.Id), Convert.ToInt32(resultID), AuditID);
                    }
                    if (GrdIMPRemark.Rows.Count != 0)
                    {
                        txtIMPFinalStatusRemark.Enabled = false;
                    }
                    if (RecentComplianceTransaction != null)
                    {
                        tbxDateIMP.Text = RecentComplianceTransaction.StatusChangedOn != null ? RecentComplianceTransaction.StatusChangedOn.Value.ToString("dd-MM-yyyy") : " ";
                    }

                    if (GrdIMPRemark.Rows.Count != 0)
                    {
                        txtIMPFinalStatusRemark.Enabled = false;
                    }
                    btnObservationHistory.Enabled = true;
                    btnActualTestingWorkDone.Enabled = true;
                    btnFinalStatus.Enabled = true;
                    btnReviewHistory.Enabled = true;
                    btnFinalStatus_Click(null, null);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divComplianceDetailsDialogIMP\").dialog('open');", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<ImplementationReviewHistory> GetFileDataIMP(int id, int ImplementationInstance, int ImplementationScheduleOnID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.ImplementationReviewHistories
                                where row.ID == id && row.ImplementationInstance == ImplementationInstance
                                && row.ImplementationScheduleOnID == ImplementationScheduleOnID
                                && row.AuditID == AuditID
                                select row).ToList();

                return fileData;
            }
        }
        protected void DownLoadClickIMP(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblID = (Label)gvr.FindControl("lblID");
                Label lblriskID = (Label)gvr.FindControl("lblriskID");
                Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<ImplementationReviewHistory> fileData = GetFileDataIMP(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblriskID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text), AuditID);
                    int i = 0;
                    string directoryName = "abc";
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=TestingDocument.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void btnObservationHistory_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "");
            liFinalStatus.Attributes.Add("class", "active");
            liReviewHistory.Attributes.Add("class", "");
            ImplementationView.ActiveViewIndex = 0;
        }
        protected void btnActualTestingWorkDone_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "active");
            liFinalStatus.Attributes.Add("class", "");
            liReviewHistory.Attributes.Add("class", "");
            ImplementationView.ActiveViewIndex = 1;
        }
        protected void btnFinalStatus_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "");
            liFinalStatus.Attributes.Add("class", "active");
            liReviewHistory.Attributes.Add("class", "");
            ImplementationView.ActiveViewIndex = 0;
        }
        protected void btnReviewHistory_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "");
            liFinalStatus.Attributes.Add("class", "");
            liReviewHistory.Attributes.Add("class", "active");
            ImplementationView.ActiveViewIndex = 1;
        }
        protected void btnIMPNext2_Click(object sender, EventArgs e)
        {
            try
            {
                long roleid = 3;
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchId = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    BranchId = Convert.ToInt32(ViewState["BID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(FinYear, Period, BranchId, VerticalID, ScheduledOnId, AuditID);
                if (getauditimplementationscheduledetails != null)
                {
                    ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                    {
                        AuditScheduleOnID = getauditimplementationscheduledetails.Id,
                        FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                        ForPeriod = getauditimplementationscheduledetails.ForMonth,
                        CustomerBranchId = BranchId,
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ResultID = ResultID,
                        RoleID = roleid,
                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                        AuditID = AuditID,
                    };

                    //if (txtIMPProcessWalkthrough.Text.Trim() == "")
                    //    MstRiskResult.ProcessWalkthrough = "";
                    //else
                    //    MstRiskResult.ProcessWalkthrough = txtIMPProcessWalkthrough.Text.Trim();

                    //if (txtIMPActualWorkDone.Text.Trim() == "")
                    //    MstRiskResult.ActualWorkDone = "";
                    //else
                    //    MstRiskResult.ActualWorkDone = txtIMPActualWorkDone.Text.Trim();

                    //if (txtIMPPopulation.Text.Trim() == "")
                    //    MstRiskResult.Population = "";
                    //else
                    //    MstRiskResult.Population = txtIMPPopulation.Text.Trim();

                    //if (txtIMPSample.Text.Trim() == "")
                    //    MstRiskResult.Sample = "";
                    //else
                    //    MstRiskResult.Sample = txtIMPSample.Text.Trim();


                    if (txtIMPFinalStatusRemark.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = txtIMPFinalStatusRemark.Text.Trim();

                    if (txtIMPNewManagementResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtIMPNewManagementResponse.Text.Trim();

                    if (ddlIMPNewPersonresponsible.SelectedValue == "-1")
                    {
                        MstRiskResult.PersonResponsible = null;
                    }
                    else
                    {
                        MstRiskResult.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                    }

                    if (ddlIMPFinalStatus.SelectedValue == "-1")
                    {
                        MstRiskResult.ImplementationStatus = null;
                    }
                    else
                    {
                        MstRiskResult.ImplementationStatus = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                    }
                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtIMPNewTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;

                    bool Success1 = false;
                    if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                    {
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                    }

                    if (Success1)
                    {
                        btnFinalStatus_Click(sender, e);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnIMPNext3_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchId = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    BranchId = Convert.ToInt32(ViewState["BID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                long roleid = 3;
                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(FinYear, Period, BranchId, VerticalID, ScheduledOnId, AuditID);
                if (getauditimplementationscheduledetails != null)
                {
                    ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                    {
                        AuditScheduleOnID = getauditimplementationscheduledetails.Id,
                        FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                        ForPeriod = getauditimplementationscheduledetails.ForMonth,
                        CustomerBranchId = BranchId,
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ResultID = ResultID,
                        RoleID = roleid,
                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                        AuditID = AuditID,
                    };
                    AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                    {
                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                        ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                        FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                        CustomerBranchId = BranchId,
                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                        ForPeriod = getauditimplementationscheduledetails.ForMonth,
                        ResultID = ResultID,
                        RoleID = roleid,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                        AuditID = AuditID,
                        StatusChangedOn = GetDate(b.ToString("dd/MM/yyyy"))
                    };
                    if (txtIMPNewManagementResponse.Text.Trim() == "")
                    {
                        MstRiskResult.ManagementResponse = "";
                    }
                    else
                    {
                        MstRiskResult.ManagementResponse = txtIMPNewManagementResponse.Text.Trim();
                    }
                    if (txtIMPFinalStatusRemark.Text.Trim() == "")
                    {
                        MstRiskResult.FixRemark = "";
                        transaction.AuditorRemark = "";
                    }
                    else
                    {
                        MstRiskResult.FixRemark = txtIMPFinalStatusRemark.Text.Trim();
                        transaction.AuditorRemark = txtIMPFinalStatusRemark.Text.Trim();
                    }

                    MstRiskResult.PersonResponsible = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                    transaction.PersonResponsible = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                    if (ddlIMPFinalStatus.SelectedValue == "-1")
                    {
                        MstRiskResult.ImplementationStatus = null;
                        transaction.ImplementationStatusId = null;
                    }
                    else
                    {
                        MstRiskResult.ImplementationStatus = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                        transaction.ImplementationStatusId = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                    }

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtIMPNewTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                    {
                        MstRiskResult.TimeLine = null;
                    }

                    var CheckHistory = RiskCategoryManagement.GetImplementationAuditResultIMPlementationID(ResultID, VerticalID, Convert.ToInt32(getauditimplementationscheduledetails.Id), AuditID);
                    if (CheckHistory == null)
                    {
                        transaction.AuditeeResponse = "AS";
                    }
                    else
                    {
                        var ResponstStatus = RiskCategoryManagement.GetLatestImplementationAuditteeResponse(ResultID, VerticalID, AuditID);
                        if (ResponstStatus != null)
                        {
                            transaction.AuditeeResponse = ResponstStatus.AuditeeResponse;
                        }
                    }

                    bool Success1 = false;
                    bool Success2 = false;

                    if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                    {
                        MstRiskResult.Status = 6;
                        transaction.StatusId = 6;
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                    }
                    else
                    {
                        MstRiskResult.Status = 6;
                        transaction.StatusId = 6;
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                    }

                    transaction.StatusId = 6;
                    transaction.Remarks = "Implementation Step Under Auditee Review.";
                    transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    transaction.CreatedOn = DateTime.Now;

                    if (Success1)
                    {
                        BindRemarksIMP(Convert.ToString(getauditimplementationscheduledetails.ForMonth), FinYear, Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, AuditID);
                        BindTransactionsIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, Convert.ToInt32(AuditID));
                        Success1 = CreateAuditImplementationTransaction(getauditimplementationscheduledetails, transaction, CustomerId, AuditID);
                        BindDocumentIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, AuditID);
                        if (CheckHistory == null)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Implementation Step submitted successfully";
                        }
                        else
                        {
                            btnReviewHistory_Click(sender, e);
                        }
                        var TimeLineHistory = RiskCategoryManagement.GetIMPTimeLineHistory(ResultID, Convert.ToInt32(AuditID), Convert.ToInt32(MstRiskResult.PersonResponsible));
                        grdTimelineHistory.DataSource = TimeLineHistory;
                        grdTimelineHistory.DataBind();
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool CreateAuditImplementationTransaction(AuditImpementationScheduleOn ImpementationScheduleOnDetails, AuditImplementationTransaction transaction, long customerID, long AuditID)
        {
            bool flag = false;
            if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
            {
                ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
            }
            else
            {
                ResultID = Convert.ToInt32(ViewState["ResultID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
            {
                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
            }
            else
            {
                VerticalID = Convert.ToInt32(ViewState["VID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
            {
                BranchId = Convert.ToInt32(Request.QueryString["BID"]);
            }
            else
            {
                BranchId = Convert.ToInt32(ViewState["BID"]);
            }
            if (ImpementationScheduleOnDetails != null)
            {
                if (fuIMPWorkingUpload.HasFile)
                {
                    HttpFileCollection fileCollection1 = Request.Files;
                    if (fileCollection1.Count > 0)
                    {
                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                        List<ImplementationFileData_Risk> files = new List<ImplementationFileData_Risk>();
                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                        var InstanceData = RiskCategoryManagement.GetAuditImplementationInstanceData(ImpementationScheduleOnDetails.ForMonth, Convert.ToInt32(ImpementationScheduleOnDetails.ImplementationInstance), BranchId, VerticalID, AuditID);
                        string directoryPath = "";
                        if (!string.IsNullOrEmpty(InstanceData.ForPeriod) && InstanceData.CustomerBranchID != -1)
                        {
                            directoryPath = Server.MapPath("~/ImplementationWorkingDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + ddlFinancialYear.SelectedItem.Text + "/" + InstanceData.ForPeriod.ToString() + "/" + ImpementationScheduleOnDetails.Id + "/1.0");
                        }
                        DocumentManagement.CreateDirectory(directoryPath);
                        for (int i = 0; i < fileCollection1.Count; i++)
                        {
                            HttpPostedFile uploadfile = fileCollection1[i];
                            string[] keys = fileCollection1.Keys[i].Split('$');
                            string fileName = "";
                            if (keys[keys.Count() - 1].Equals("fuIMPWorkingUpload"))
                            {
                                fileName = uploadfile.FileName;
                            }
                            Guid fileKey = Guid.NewGuid();
                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                            Stream fs = uploadfile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                            if (uploadfile.ContentLength > 0)
                            {
                                ImplementationFileData_Risk file = new ImplementationFileData_Risk()
                                {
                                    Name = fileName,
                                    FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                    FileKey = fileKey.ToString(),
                                    Version = "1.0",
                                    VersionDate = DateTime.UtcNow,
                                    ForPeriod = ImpementationScheduleOnDetails.ForMonth,
                                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                    FinancialYear = ImpementationScheduleOnDetails.FinancialYear,
                                    IsDeleted = false,
                                    ResultId = ResultID,
                                    VerticalID = VerticalID,
                                    AuditID = AuditID,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };
                                files.Add(file);
                            }
                        }//For Loop End  
                        if (Filelist != null && list != null)
                            flag = UserManagementRisk.CreateAuditImplementationTransaction(transaction, files, list, Filelist);
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                        }
                    }//FileCollection Count End 
                    else
                    {
                        RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                    }
                }//HasUpload File End
                else
                {
                    RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                }
            }//ImpementationScheduleOnDetails Is null End
            return flag;
        }
        protected void btnIMPSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchId = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    BranchId = Convert.ToInt32(ViewState["BID"]);
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(Session["CustomerIdFromQueryString"].ToString());
                }
                if (ddlIMPFinalStatus.SelectedValue == "-1")
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Implementation Status.";
                    ImplementationView.ActiveViewIndex = 0;
                    return;
                }
                if (string.IsNullOrEmpty(txtIMPNewManagementResponse.Text.Trim()))
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Enter Management Response.";
                    ImplementationView.ActiveViewIndex = 1;
                    txtIMPNewManagementResponse.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                {
                    if (ddlIMPFinalStatus.SelectedValue == "4")
                    {
                        TimelineTr.Visible = false;
                        RequiereValidateTimeline.Enabled = false;
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Enter TimeLine.";
                        ImplementationView.ActiveViewIndex = 0;
                        txtIMPNewTimeLine.Focus();
                        return;
                    }
                }
                bool Flag = false;
                long roleid = 4;
                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(FinYear, Period, BranchId, VerticalID, ScheduledOnId, AuditID);
                if (getauditimplementationscheduledetails != null)
                {
                    #region
                    ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                    {
                        AuditScheduleOnID = getauditimplementationscheduledetails.Id,
                        FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                        ForPeriod = getauditimplementationscheduledetails.ForMonth,
                        CustomerBranchId = BranchId,
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ResultID = ResultID,
                        RoleID = roleid,
                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                        AuditID = AuditID,
                    };
                    AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                    {
                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                        StatusChangedOn = GetDate(b.ToString("dd/MM/yyyy")),
                        ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                        FinancialYear = FinYear,
                        CustomerBranchId = BranchId,
                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                        ForPeriod = Period,
                        ResultID = ResultID,
                        RoleID = roleid,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                        AuditID = AuditID,
                    };
                    #endregion

                    #region
                    string remark = string.Empty;
                    transaction.StatusId = 6;
                    MstRiskResult.Status = 6;
                    remark = "Implementation Step Under Auditee Review.";
                    if (ddlIMPFinalStatus.SelectedValue == "-1")
                    {
                        MstRiskResult.ImplementationStatus = null;
                        transaction.ImplementationStatusId = null;
                    }
                    else
                    {
                        MstRiskResult.ImplementationStatus = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                        transaction.ImplementationStatusId = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                    }

                    if (txtIMPNewManagementResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtIMPNewManagementResponse.Text.Trim();

                    if (txtIMPFinalStatusRemark.Text.Trim() == "")
                    {
                        MstRiskResult.FixRemark = "";
                        transaction.AuditorRemark = "";
                    }
                    else
                    {
                        MstRiskResult.FixRemark = txtIMPFinalStatusRemark.Text.Trim();
                        transaction.AuditorRemark = txtIMPFinalStatusRemark.Text.Trim();
                    }

                    if (ddlIMPNewPersonresponsible.SelectedValue == "-1")
                    {
                        MstRiskResult.PersonResponsible = null;
                        transaction.PersonResponsible = null;
                    }
                    else
                    {
                        MstRiskResult.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                        transaction.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                    }
                    MstRiskResult.PersonResponsible = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                    transaction.PersonResponsible = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
                    transaction.Remarks = remark.Trim();
                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtIMPNewTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;
                    #endregion

                    bool Success1 = false;
                    bool Success2 = false;
                    var AuditteeStatus = RiskCategoryManagement.GetIMPLatestAuditteeStatus(transaction);
                    if (AuditteeStatus != null)
                    {
                        if (AuditteeStatus == "PA")
                        {
                            transaction.AuditeeResponse = "AR";
                        }
                        else
                        {
                            transaction.AuditeeResponse = "AS";
                        }
                    }
                    else
                    {
                        transaction.AuditeeResponse = "AS";
                    }

                    if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                    {
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                    }
                    if (RiskCategoryManagement.AuditImplementationTransactionExists(transaction))
                    {
                        transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        transaction.CreatedOn = DateTime.Now;
                        Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                    }
                    else
                    {
                        transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        transaction.CreatedOn = DateTime.Now;
                        Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                    }

                    string Testremark = "";
                    if (string.IsNullOrEmpty(txtIMPReviewRemark.Text))
                    {
                        Testremark = "NA";
                    }
                    else
                    {
                        Testremark = txtIMPReviewRemark.Text.Trim();
                    }
                    List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                    HttpFileCollection fileCollection1 = Request.Files;
                    if (fileCollection1.Count > 0)
                    {
                        var InstanceData = RiskCategoryManagement.GetAuditImplementationInstanceData(getauditimplementationscheduledetails.ForMonth, Convert.ToInt32(getauditimplementationscheduledetails.ImplementationInstance), BranchId, VerticalID, AuditID);
                        string directoryPath = "";
                        if (!string.IsNullOrEmpty(InstanceData.ForPeriod) && InstanceData.CustomerBranchID != -1)
                        {
                            directoryPath = Server.MapPath("~/ImplementationAdditionalDocument/" + CustomerId + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + FinYear + "/" + InstanceData.ForPeriod.ToString() + "/" + getauditimplementationscheduledetails.Id + "/1.0");
                        }
                        DocumentManagement.CreateDirectory(directoryPath);
                        for (int i = 0; i < fileCollection1.Count; i++)
                        {
                            HttpPostedFile uploadfile1 = fileCollection1[i];
                            string[] keys1 = fileCollection1.Keys[i].Split('$');
                            String fileName1 = "";
                            if (keys1[keys1.Count() - 1].Equals("ReviewFileUpload"))
                            {
                                fileName1 = uploadfile1.FileName;
                            }
                            Guid fileKey1 = Guid.NewGuid();
                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadfile1.FileName));
                            Stream fs = uploadfile1.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                            if (uploadfile1.ContentLength > 0)
                            {
                                ImplementationReviewHistory RH = new ImplementationReviewHistory()
                                {
                                    ResultID = ResultID,
                                    ForMonth = Convert.ToString(getauditimplementationscheduledetails.ForMonth),
                                    ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                    Dated = DateTime.Now,
                                    Remarks = Testremark,
                                    ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                                    FinancialYear = FinYear,
                                    CustomerBranchId = BranchId,
                                    Name = fileName1,
                                    FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                    FileKey = fileKey1.ToString(),
                                    Version = "1.0",
                                    VersionDate = DateTime.UtcNow,
                                    FixRemark = txtIMPFinalStatusRemark.Text.Trim(),
                                    VerticalID = VerticalID,
                                    ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                    AuditID = AuditID,
                                    CreatedOn = DateTime.Now,
                                };
                                Flag = RiskCategoryManagement.CreateImplementationReviewHistoryRemark(RH);
                                DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                Label2.Text = "";
                                Label1.Text = "";
                            }
                            else
                            {
                                ImplementationReviewHistory RH = new ImplementationReviewHistory()
                                {
                                    ForMonth = Convert.ToString(getauditimplementationscheduledetails.ForMonth),
                                    ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                    Dated = DateTime.Now,
                                    Remarks = Testremark,
                                    ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                                    FinancialYear = FinYear,
                                    CustomerBranchId = BranchId,
                                    ResultID = ResultID,
                                    FixRemark = txtIMPFinalStatusRemark.Text.Trim(),
                                    VerticalID = VerticalID,
                                    ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                    AuditID = AuditID,
                                    CreatedOn = DateTime.Now,
                                };
                                Flag = RiskCategoryManagement.CreateImplementationReviewHistoryRemark(RH);
                            }
                        }
                    }
                    else
                    {
                        ImplementationReviewHistory RH = new ImplementationReviewHistory()
                        {
                            ForMonth = Convert.ToString(getauditimplementationscheduledetails.ForMonth),
                            ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                            Dated = DateTime.Now,
                            Remarks = Testremark,
                            ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                            FinancialYear = FinYear,
                            CustomerBranchId = BranchId,
                            ResultID = ResultID,
                            FixRemark = txtIMPFinalStatusRemark.Text.Trim(),
                            VerticalID = VerticalID,
                            ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                            AuditID = AuditID,
                            CreatedOn = DateTime.Now,
                        };
                        Flag = RiskCategoryManagement.CreateImplementationReviewHistoryRemark(RH);
                    }
                    BindRemarksIMP(Convert.ToString(getauditimplementationscheduledetails.ForMonth), FinYear, Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, AuditID);
                    if (Success1 == true && Success2 == true && Flag == true)
                    {
                        BindTransactionsIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, Convert.ToInt32(AuditID));
                        txtIMPReviewRemark.Text = string.Empty;
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Implementation Step submitted successfully";
                        btnIMPSave.Enabled = false;
                        tbxDateIMP.Enabled = false;
                        txtIMPReviewRemark.Enabled = false;
                        GrdIMPRemark.Enabled = false;
                        btnIMPNext3.Enabled = false;
                        txtIMPNewTimeLine.Enabled = false;
                        ddlIMPNewPersonresponsible.Enabled = false;
                        ddlIMPFinalStatus.Enabled = false;
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void rptIMPComplianceDocumnets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (e.CommandName.Equals("DownloadIMP"))
                {
                    DownloadFileIMP(Convert.ToInt32(e.CommandArgument), AuditID);
                }
                else if (e.CommandName.Equals("DeleteIMP"))
                {
                    DeleteFileIMP(Convert.ToInt32(e.CommandArgument));
                    BindDocumentIMP(ScheduledOnId, ResultID, AuditID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptIMPComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnetsIMP");
            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocbutton = (LinkButton)e.Row.FindControl("lbtLinkDocbuttonIMP");
            if (lbtLinkDocbutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
            }
        }
        public void DeleteFileIMP(int fileId)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                var file = Business.RiskCategoryManagement.GetFileImplementationFileData_Risk(fileId, AuditID);
                if (file != null)
                {
                    string path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    DashboardManagementRisk.DeleteFileIMP(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void DownloadFileIMP(int fileId, long AuditID)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileImplementationFileData_Risk(fileId, AuditID);
                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void grdTransactionIMPHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                grdTransactionIMPHistory.PageIndex = e.NewPageIndex;
                BindTransactionsIMP(ScheduledOnId, ResultID, Convert.ToInt32(AuditID));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void GrdIMPRemark_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                GrdIMPRemark.PageIndex = e.NewPageIndex;
                BindRemarksIMP(Period, FinYear, ScheduledOnId, ResultID, AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnDownloadIMP_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                var file = Business.RiskCategoryManagement.GetFileImplementationFileData_Risk(Convert.ToInt32(hdlSelectedDocumentIDIMP.Value), AuditID);
                Response.Buffer = true;
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploaded";
            }
            return processnonprocess;
        }

        public string ShowSampleDocumentNameView(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "/View";
            }
            return processnonprocess;
        }

        protected void grdTimelineHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void lblViewFile_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;
            Label lblID = (Label)gvr.FindControl("lblID");
            Label lblriskID = (Label)gvr.FindControl("lblriskID");
            Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
            Label lblATBDId = (Label)gvr.FindControl("lblATBDId");

            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            List<ImplementationReviewHistory> fileData = GetFileDataIMP(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblriskID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text), AuditID);

            foreach (var file in fileData)
            {
                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                if (file.FilePath != null && File.Exists(filePath))
                {
                    string Folder = "~/TempFiles";
                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    string DateFolder = Folder + "/" + FileData;

                    string extension = System.IO.Path.GetExtension(filePath);

                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                    if (!Directory.Exists(DateFolder))
                    {
                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                    }

                    string customerID = Convert.ToString(Session["CustomerIdFromQueryString"].ToString());

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                    string FileName = DateFolder + "/" + User + "" + extension;

                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    if (file.EnType == "M")
                    {
                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    else
                    {
                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    bw.Close();

                    string CompDocReviewPath = FileName;
                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestViewIMPPerformer('" + CompDocReviewPath + "');", true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                }
                break;
            }
        }
    }
}