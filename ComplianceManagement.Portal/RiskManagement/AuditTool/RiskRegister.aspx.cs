﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using Saplin.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;


namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class RiskRegister : System.Web.UI.Page
    {
        public static List<int> IndustryIdProcess = new List<int>();
        public static List<int> RiskIdProcess = new List<int>();
        public static List<int> AssertionsIdProcess = new List<int>();
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "ActivityDescription";
                BindLegalEntityData();                
                BindLocationType();
                BindLocationTypePopPup();
                this.countrybindRisk(ddlRiskCategory);
                this.countrybindAssertions(ddlAssertions);
                this.countrybindIndustory(ddlIndultory, customerID);
                GetPageDisplaySummary();
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindData("P");
                }
                else
                {
                    BindData("N");
                }
            }
        }


        public void BindGrid()
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
            {
                BindData("N");
            }
        }

        #region Comman
        private void BindProcess(string flag)
        {
            try
            {
                int CustomerBranchId = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (flag == "P")
                {
                    ddlFilterProcess.Items.Clear();
                    ddlFilterProcess.DataTextField = "Name";
                    ddlFilterProcess.DataValueField = "Id";
                    ddlFilterProcess.Items.Clear();
                    ddlFilterProcess.DataSource = ProcessManagement.FillProcess(flag, Convert.ToInt32(CustomerBranchId));
                    ddlFilterProcess.DataBind();
                    ddlFilterProcess.Items.Insert(0, new ListItem("Process", "-1"));

                    if (ddlFilterProcess.SelectedValue != "")
                    {
                        BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "P");
                    }
                }
                else
                {
                    ddlFilterProcess.Items.Clear();
                    ddlFilterProcess.DataTextField = "Name";
                    ddlFilterProcess.DataValueField = "Id";
                    ddlFilterProcess.Items.Clear();
                    ddlFilterProcess.DataSource = ProcessManagement.FillProcess(flag, Convert.ToInt32(CustomerBranchId));
                    ddlFilterProcess.DataBind();
                    ddlFilterProcess.Items.Insert(0, new ListItem("Process", "-1"));
                    if (ddlFilterProcess.SelectedValue != "")
                    {
                        BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "N");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
                else
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindProcessPopPup(string flag)
        {
            try
            {
                if (flag == "P")
                {
                    var process = ProcessManagement.FillProcess("P", Convert.ToInt32(ddlClientProcess.SelectedValue));

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = process;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("All", "-1"));
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcessPoPup(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                    }
                }
                else
                {
                    var process = ProcessManagement.FillProcess("N", Convert.ToInt32(ddlClientProcess.SelectedValue));

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = process;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("All", "-1"));
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcessPoPup(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindProcessPopPup(int Branchid, int Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    var process = ProcessManagement.FillProcess("P", Convert.ToInt32(Branchid));

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = process;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("All", "-1"));

                    BindSubProcessPoPup(Convert.ToInt32(Processid), "P");

                }
                else
                {
                    var process = ProcessManagement.FillProcess("N", Convert.ToInt32(Branchid));

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = process;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("All", "-1"));
                    BindSubProcessPoPup(Convert.ToInt32(Processid), "N");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindSubProcessPoPup(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("All", "-1"));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("All", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindLocationTypePopPup()
        {
            long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
            ddlLocationType.DataTextField = "Name";
            ddlLocationType.DataValueField = "ID";
            ddlLocationType.Items.Clear();
            ddlLocationType.DataSource = ProcessManagement.FillLocationType(CustomerID);
            ddlLocationType.DataBind();
            ddlLocationType.Items.Insert(0, new ListItem(" Select Location Type ", "-1"));
        }
        private void BindDataExport(string Flag, string Flag2)
        {
            try
            {

                int customerID = -1;
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                int CustomerBranchId = -1;
                int processid = -1;
                int subprocessid = -1;
                int LocationType = -1;
                int RiskCategory = -1;
                int Industry = -1;
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                {
                    if (ddlSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocationType.SelectedValue))
                {
                    if (ddlFilterLocationType.SelectedValue != "-1")
                    {
                        LocationType = Convert.ToInt32(ddlFilterLocationType.SelectedValue);
                    }
                }
                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                var Branchlistloop = Branchlist.ToList();
                if (Flag == "P")
                {

                    var Riskcategorymanagementlist = RiskControlMatrixClass.GetAllRiskCategoryCreation1(processid, subprocessid, Branchlist.ToList(), LocationType, RiskCategory, Industry, customerID, "P", "All");
                    GridExportExcel.DataSource = Riskcategorymanagementlist;
                    GridExportExcel.DataBind();

                }
                else
                {

                    var Riskcategorymanagementlist = RiskControlMatrixClass.GetAllRiskCategoryCreation1(processid, subprocessid, Branchlist.ToList(), LocationType, RiskCategory, Industry, customerID, "N", "All");
                    GridExportExcel.DataSource = Riskcategorymanagementlist;
                    GridExportExcel.DataBind();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        private void BindData(string Flag)
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                int CustomerBranchId = -1;
                int processid = -1;
                int subprocessid = -1;
                int LocationType = -1;
                int RiskCategory = -1;
                int Industry = -1;               
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);                       
                    }                  
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);                        
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);                       
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);                       
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);                       
                    }
                }
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);                        
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                {
                    if (ddlSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlSubProcess.SelectedValue);                        
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocationType.SelectedValue))
                {
                    if (ddlFilterLocationType.SelectedValue != "-1")
                    {
                        LocationType = Convert.ToInt32(ddlFilterLocationType.SelectedValue);                        
                    }
                }
                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                var Branchlistloop = Branchlist.ToList();
                if (Flag == "P")
                {

                    var Riskcategorymanagementlist = RiskControlMatrixClass.GetAllRiskCategoryCreation1(processid, subprocessid, Branchlist.ToList(), LocationType, RiskCategory, Industry, customerID, "P", "All");
                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    grdRiskActivityMatrix.DataBind();

                    Saplin.Controls.DropDownCheckBoxes DrpIndustry = (DropDownCheckBoxes) grdRiskActivityMatrix.FindControl("ddlIndultory");
                    if (DrpIndustry != null)
                    {
                        this.countrybindIndustory(DrpIndustry, customerID);
                    }
                    Saplin.Controls.DropDownCheckBoxes DrpRiskCategoryProcess = (DropDownCheckBoxes) grdRiskActivityMatrix.FindControl("ddlRiskCategory");
                    if (DrpRiskCategoryProcess != null)
                    {
                        this.countrybindRisk(DrpRiskCategoryProcess);
                    }
                    Saplin.Controls.DropDownCheckBoxes ddlAssertionsProcess = (DropDownCheckBoxes) grdRiskActivityMatrix.FindControl("ddlAssertions");
                    if (ddlAssertionsProcess != null)
                    {
                        this.countrybindAssertions(ddlAssertionsProcess);
                    }
                }
                else
                {
                    var Riskcategorymanagementlist = RiskControlMatrixClass.GetAllRiskCategoryCreation1(processid, subprocessid, Branchlist.ToList(), LocationType, RiskCategory, Industry, customerID, "N", "All");
                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    grdRiskActivityMatrix.DataBind();
                }
                GetPageDisplaySummary();
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=RiskCategoryTransaction.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                using (StringWriter sw = new StringWriter())
                {
                    HtmlTextWriter hw = new HtmlTextWriter(sw);
                    //To ExportAllpages
                    GridExportExcel.AllowPaging = false;
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        this.BindDataExport("P", "A");
                    }
                    else
                    {
                        this.BindDataExport("N", "A");
                    }
                    if (GridExportExcel.Rows.Count > 0)
                    {
                        GridExportExcel.HeaderRow.BackColor = Color.Blue;
                        foreach (TableCell cell in GridExportExcel.HeaderRow.Cells)
                        {
                            cell.BackColor = GridExportExcel.HeaderStyle.BackColor;
                        }
                        GridExportExcel.RenderControl(hw);
                    }


                    string style = @"<style> .textmode { } </style>";
                    Response.Write(style);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public string ShowLocationType(int locationtypeid)
        {
            long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
            string LocationType = "";
            LocationType = ProcessManagement.GetLocationTypeName(locationtypeid, CustomerID);
            return LocationType;
        }

        public string ShowProcessName(long processId)
        {
            int customerID = -1;
            customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetProcessName(processId, customerID);
            return processnonprocess;
        }

        public string ShowSubProcessName(long processId, long SubprocessId)
        {
            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetSubProcessName(processId, SubprocessId);
            return processnonprocess;
        }

        public string ShowRiskCategoryName(long categoryid)
        {
            List<RiskCategoryName_Result> a = new List<RiskCategoryName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetRiskCategoryNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }

        public string ShowIndustryName(long categoryid)
        {
            List<IndustryName_Result> a = new List<IndustryName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetIndustryNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }

        public void SplitString(string StrToSplit, Saplin.Controls.DropDownCheckBoxes DrpToFill)
        {
            string[] commandArgs = StrToSplit.Split(new char[] { ',' });
            string a = string.Empty;
            if (commandArgs.Length > 0)
            {
                for (int i = 0; i < commandArgs.Length; i++)
                {
                    a = commandArgs[i];
                    if (!string.IsNullOrEmpty(a))
                    {
                        DrpToFill.Items.FindByText(a.Trim()).Selected = true;
                    }
                }
            }
        }

        public void SetddlLocationType(string StrToSet, DropDownList ddlLocationType)
        {

            ddlLocationType.Items.FindByText(StrToSet.Trim()).Selected = true;

        }


        public string ShowCustomerBranchName(long categoryid)
        {
            List<CustomerBranchName_Result> a = new List<CustomerBranchName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetCustomerBranchNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowAssertionsName(long categoryid)
        {
            List<AssertionsName_Result> a = new List<AssertionsName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetAssertionsNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                //  ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("P");
                BindData("P");
            }
            else
            {
                BindProcess("N");
                BindData("N");
            }
        }       
        public void BindPopPupCustomer()
        {
            int customerID = -1;
            customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            if (ddlClientProcess != null)
            {
                ddlClientProcess.DataTextField = "Name";
                ddlClientProcess.DataValueField = "ID";
                ddlClientProcess.DataSource = UserManagementRisk.FillCustomerNew(customerID);
                ddlClientProcess.DataBind();
                ddlClientProcess.Items.Insert(0, new ListItem(" Select Branch ", "-1"));
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomerBranchName"])))
                {
                    ddlClientProcess.Items.FindByText(ViewState["CustomerBranchName"].ToString().Trim()).Selected = true;
                }
            }
        }

        public void BindLocationType()
        {
            long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
            ddlFilterLocationType.DataTextField = "Name";
            ddlFilterLocationType.DataValueField = "ID";
            ddlFilterLocationType.Items.Clear();
            ddlFilterLocationType.DataSource = ProcessManagement.FillLocationType(CustomerID);
            ddlFilterLocationType.DataBind();
            ddlFilterLocationType.Items.Insert(0, new ListItem("All", "-1"));
        }

        private void countrybindIndustory(Saplin.Controls.DropDownCheckBoxes DrpIndustry,int customerID)
        {
            DrpIndustry.DataTextField = "Name";
            DrpIndustry.DataValueField = "Id";
            DrpIndustry.DataSource = ProcessManagement.FilliNDUSTRY(customerID);
            DrpIndustry.DataBind();
        }
        private void countrybindRisk(Saplin.Controls.DropDownCheckBoxes Drpriskcategory)
        {

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {

                Drpriskcategory.DataTextField = "Name";
                Drpriskcategory.DataValueField = "ID";
                Drpriskcategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("P");
                Drpriskcategory.DataBind();
            }
            else
            {
                Drpriskcategory.DataTextField = "Name";
                Drpriskcategory.DataValueField = "ID";
                Drpriskcategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("N");
                Drpriskcategory.DataBind();
            }

        }
        private void countrybindClient(Saplin.Controls.DropDownCheckBoxes DrpClient, long customerid)
        {
            if (customerid != -1)
            {
                DrpClient.DataTextField = "Name";
                DrpClient.DataValueField = "ID";
                DrpClient.DataSource = RiskCategoryManagement.FillCustomerBranch(customerid);
                DrpClient.DataBind();
            }
        }
        private void countrybindAssertions(Saplin.Controls.DropDownCheckBoxes DrpAssertions)
        {
            DrpAssertions.DataTextField = "Name";
            DrpAssertions.DataValueField = "ID";
            DrpAssertions.DataSource = RiskCategoryManagement.FillAssertions();
            DrpAssertions.DataBind();
        }
        #endregion

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindProcess("P");
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
                else
                {
                    BindProcess("N");
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindProcess("P");
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
                else
                {
                    BindProcess("N");
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindProcess("P");
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
                else
                {
                    BindProcess("N");
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindProcess("P");
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
                else
                {
                    BindProcess("N");
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindProcess("P");
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
            else
            {
                BindProcess("N");
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        #region Process

        protected void ddlFilterRiskCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P");
            }
            else
            {
                this.BindData("N");
            }
        }
        protected void ddlFilterIndustry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P");
            }
            else
            {
                this.BindData("N");
            }
        }
        protected void ddlFilterLocationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        
        protected void ddlFilterProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    if (ddlFilterProcess.SelectedValue != "" || ddlFilterProcess.SelectedValue != null || ddlFilterProcess.SelectedValue != "Process")
                    {
                        BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "P");
                        ViewState["ProcessIdForAddNew"] = null;
                        ViewState["ProcessIdForAddNew"] = ddlFilterProcess.SelectedValue;
                    }
                    this.BindData("P");
                }
                else
                {
                    if (ddlFilterProcess.SelectedValue != "" || ddlFilterProcess.SelectedValue != null || ddlFilterProcess.SelectedValue != "Process")
                    {
                        BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "N");
                        ViewState["ProcessIdForAddNew"] = null;
                        ViewState["ProcessIdForAddNew"] = ddlFilterProcess.SelectedValue;
                    }
                    this.BindData("N");

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
            {
                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedItem.Text != "All")
                    {
                        if (ddlFilterSubProcess.SelectedItem.Text != "All")
                        {
                            ViewState["SubProcessIdForAddNew"] = null;
                            ViewState["SubProcessIdForAddNew"] = ddlFilterSubProcess.SelectedValue;
                        }
                    }
                }
            }
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P");
            }
            else
            {
                this.BindData("N");
            }
        }
        
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    if (ddlProcess.SelectedValue != "" || ddlProcess.SelectedValue != null || ddlProcess.SelectedValue != " Select Process ")
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                        ViewState["ProcessIdForAddNew"] = null;
                        ViewState["ProcessIdForAddNew"] = ddlProcess.SelectedValue;
                    }
                    this.BindData("P");
                }
                else
                {
                    if (ddlProcess.SelectedValue != "" || ddlProcess.SelectedValue != null || ddlProcess.SelectedValue != " Select Process ")
                    {
                        BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                        ViewState["ProcessIdForAddNew"] = null;
                        ViewState["ProcessIdForAddNew"] = ddlProcess.SelectedValue;
                    }
                    this.BindData("N");

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedItem.Text != "All")
                    {
                        if (ddlSubProcess.SelectedItem.Text != "All")
                        {
                            ViewState["SubProcessIdForAddNew"] = null;
                            ViewState["SubProcessIdForAddNew"] = ddlSubProcess.SelectedValue;
                        }
                    }
                }
            }
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P");
            }
            else
            {
                this.BindData("N");
            }
        }

        //protected void Add(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Control control = null;
        //        if (grdRiskActivityMatrix.FooterRow != null)
        //        {
        //            control = grdRiskActivityMatrix.FooterRow;
        //        }
        //        else
        //        {
        //            control = grdRiskActivityMatrix.Controls[0].Controls[0];
        //        }
        //        string ControlNo = (control.FindControl("txtControlNo") as TextBox).Text;
        //        string ActivityDescription = (control.FindControl("txtRiskActivityDescription") as TextBox).Text;
        //        string controlObjective = (control.FindControl("txtControlObjective") as TextBox).Text;
        //        string locationtype = (control.FindControl("ddlLocationTypeProcess") as DropDownList).SelectedItem.Value;

        //        string customerbranchidID = (control.FindControl("ddlClientProcess") as DropDownList).SelectedItem.Value;                
        //        int customerID = -1;
        //        if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "CADMN")
        //        {
        //            customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
        //        }
        //        long pid = 0;
        //        long psid = 0;
        //        if (ddlProcess.SelectedValue != "")
        //        {
        //            if (ddlProcess.SelectedValue != "All")
        //            {
        //                pid = Convert.ToInt32(ddlProcess.SelectedValue);
        //            }
        //        }
        //        if (ddlSubProcess.SelectedValue != "")
        //        {
        //            if (ddlSubProcess.SelectedValue != "All")
        //            {
        //                psid = Convert.ToInt32(ddlSubProcess.SelectedValue);
        //            }
        //        }
        //        if (ActivityDescription != "" || ActivityDescription != null)
        //        {
        //            if (controlObjective != "" || controlObjective != null)
        //            {
        //                RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation()
        //                {
        //                    ControlNo = ControlNo,
        //                    ActivityDescription = ActivityDescription,
        //                    ControlObjective = controlObjective,
        //                    ProcessId = pid,
        //                    SubProcessId = psid,
        //                    LocationType = Convert.ToInt32(locationtype),
        //                    IsInternalAudit = "N",
        //                    CustomerId = customerID,
        //                    CustomerBranchId = Convert.ToInt32(customerbranchidID)
        //                };
        //                if (RiskCategoryManagement.Exists(riskcategorycreation))
        //                {
        //                    cvDuplicateEntry.ErrorMessage = "Risk Category already exists.";
        //                    cvDuplicateEntry.IsValid = false;
        //                    return;
        //                }
        //                using (AuditControlEntities entities = new AuditControlEntities())
        //                {
        //                    entities.RiskCategoryCreations.Add(riskcategorycreation);
        //                    entities.SaveChanges();
        //                }
        //                if (IndustryIdProcess.Count > 0)
        //                {
        //                    foreach (var aItem in IndustryIdProcess)
        //                    {
        //                        IndustryMapping IndustryMapping = new IndustryMapping()
        //                        {
        //                            RiskCategoryCreationId = riskcategorycreation.Id,
        //                            IndustryID = Convert.ToInt32(aItem),
        //                            IsActive = true,
        //                            EditedDate = DateTime.UtcNow,
        //                            EditedBy = Convert.ToInt32(Session["userID"]),
        //                            ProcessId = pid,
        //                            SubProcessId = psid,
        //                        };
        //                        RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
        //                    }
        //                    IndustryIdProcess.Clear();
        //                }
        //                if (RiskIdProcess.Count > 0)
        //                {
        //                    foreach (var aItem in RiskIdProcess)
        //                    {
        //                        RiskCategoryMapping RiskCategoryMapping = new RiskCategoryMapping()
        //                        {
        //                            RiskCategoryCreationId = riskcategorycreation.Id,
        //                            RiskCategoryId = Convert.ToInt32(aItem),
        //                            ProcessId = pid,
        //                            IsActive = true,
        //                            SubProcessId = psid,
        //                        };
        //                        RiskCategoryManagement.CreateRiskCategoryMapping(RiskCategoryMapping);
        //                    }
        //                    RiskIdProcess.Clear();
        //                }

        //                if (AssertionsIdProcess.Count > 0)
        //                {
        //                    foreach (var aItem in AssertionsIdProcess)
        //                    {
        //                        AssertionsMapping AssertionsMapping = new AssertionsMapping()
        //                        {
        //                            RiskCategoryCreationId = riskcategorycreation.Id,
        //                            AssertionId = Convert.ToInt32(aItem),
        //                            ProcessId = pid,
        //                            IsActive = true,
        //                            SubProcessId = psid,
        //                        };
        //                        RiskCategoryManagement.CreateAssertionsMapping(AssertionsMapping);
        //                    }
        //                    AssertionsIdProcess.Clear();
        //                }
        //            }
        //        }
        //        if (rdRiskActivityProcess.SelectedItem.Text == "Process")
        //        {
        //            this.BindData("P");
        //        }
        //        else
        //        {
        //            this.BindData("N");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //protected void Edit(object sender, GridViewEditEventArgs e)
        //{
        //    ViewState["Location"] = ((Label)grdRiskActivityMatrix.Rows[e.NewEditIndex].FindControl("lblLocationType")).Text;
        //    grdRiskActivityMatrix.EditIndex = e.NewEditIndex;

        //    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
        //    {
        //        BindData("P");
        //    }
        //    else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
        //    {
        //        BindData("N");
        //    }          
        //}

        //protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
        //{
        //    grdRiskActivityMatrix.EditIndex = -1;

        //    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
        //    {
        //        BindData("P");
        //    }
        //    else
        //    {
        //        BindData("N");
        //    }
        //}


        //protected void UpdateRisk(object sender, GridViewUpdateEventArgs e)
        //{
        //    long pid = 0;
        //    long psid = 0;

        //    try
        //    {
        //        if (ddlProcess.SelectedValue != "-1" && ddlSubProcess.SelectedValue != "-1")
        //        {

        //            if (ddlProcess.SelectedItem.Text != "All")
        //            {
        //                pid = Convert.ToInt32(ddlProcess.SelectedValue);
        //            }


        //            if (ddlSubProcess.SelectedItem.Text != "All")
        //            {
        //                psid = Convert.ToInt32(ddlSubProcess.SelectedValue);
        //            }



        //            long RiskId = Convert.ToInt64(grdRiskActivityMatrix.DataKeys[e.RowIndex].Value);

        //            RiskCategoryCreation RiskData = new RiskCategoryCreation()
        //            {
        //                Id = RiskId,
        //                ControlNo = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtControlNoEdit")).Text,
        //                ActivityDescription = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtRiskActivityDescription")).Text,
        //                ControlObjective = ((TextBox)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("txtControlObjective")).Text,
        //                LocationType = Convert.ToInt32((grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlLocationTypeProcessEdit") as DropDownList).SelectedItem.Value),
        //                CustomerBranchId = Convert.ToInt32((grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlClientProcessEdit") as DropDownList).SelectedItem.Value)
        //            };

        //            RiskCategoryManagement.RiskUpdate(RiskData);

        //            RiskCategoryManagement.UpdateRiskIndustryMapping(RiskId);
        //            RiskCategoryManagement.UpdateRiskCategoryMapping(RiskId);
        //            RiskCategoryManagement.UpdateAssertionsMapping(RiskId);
        //            //RiskCategoryManagement.UpdateCustomerBranchMapping(RiskId);

        //            if (grdRiskActivityMatrix.Rows.Count >= 0)
        //            {
        //                Saplin.Controls.DropDownCheckBoxes DrpRiskCategoryProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlRiskCategoryProcessEdit");
        //                RiskIdProcess.Clear();
        //                for (int i = 0; i < DrpRiskCategoryProcess.Items.Count; i++)
        //                {
        //                    if (DrpRiskCategoryProcess.Items[i].Selected)
        //                    {
        //                        RiskIdProcess.Add(Convert.ToInt32(DrpRiskCategoryProcess.Items[i].Value));
        //                    }
        //                }

        //                Saplin.Controls.DropDownCheckBoxes DrpIndustryProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("DropDownCheckBoxesProcessEdit");
        //                IndustryIdProcess.Clear();
        //                for (int i = 0; i < DrpIndustryProcess.Items.Count; i++)
        //                {
        //                    if (DrpIndustryProcess.Items[i].Selected)
        //                    {
        //                        IndustryIdProcess.Add(Convert.ToInt32(DrpIndustryProcess.Items[i].Value));
        //                    }
        //                }

        //                //Saplin.Controls.DropDownCheckBoxes ddlClientProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlClientProcessEdit");
        //                //ClientIdProcess.Clear();
        //                //for (int i = 0; i < ddlClientProcess.Items.Count; i++)
        //                //{
        //                //    if (ddlClientProcess.Items[i].Selected)
        //                //    {
        //                //        ClientIdProcess.Add(Convert.ToInt32(ddlClientProcess.Items[i].Value));
        //                //    }
        //                //}

        //                Saplin.Controls.DropDownCheckBoxes ddlAssertionsProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.Rows[e.RowIndex].FindControl("ddlAssertionsProcessEdit");
        //                AssertionsIdProcess.Clear();
        //                for (int i = 0; i < ddlAssertionsProcess.Items.Count; i++)
        //                {
        //                    if (ddlAssertionsProcess.Items[i].Selected)
        //                    {
        //                        AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertionsProcess.Items[i].Value));
        //                    }
        //                }
        //            }

        //            if (IndustryIdProcess.Count > 0)
        //            {
        //                foreach (var aItem in IndustryIdProcess)
        //                {
        //                    if (RiskCategoryManagement.RiskIndustryMappingExists(Convert.ToInt64(aItem), RiskId))
        //                    {
        //                        RiskCategoryManagement.EditRiskIndustryMapping(aItem, RiskId);
        //                    }
        //                    else
        //                    {
        //                        IndustryMapping IndustryMapping = new IndustryMapping()
        //                        {
        //                            RiskCategoryCreationId = RiskId,
        //                            IndustryID = Convert.ToInt32(aItem),
        //                            IsActive = true,
        //                            EditedDate = DateTime.UtcNow,
        //                            EditedBy = Convert.ToInt32(Session["userID"]),
        //                            ProcessId = pid,
        //                            SubProcessId = psid,
        //                        };
        //                        RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
        //                    }
        //                }
        //                IndustryIdProcess.Clear();
        //            }

        //            if (RiskIdProcess.Count > 0)
        //            {
        //                foreach (var aItem in RiskIdProcess)
        //                {
        //                    if (RiskCategoryManagement.RiskCategoryMappingExists(Convert.ToInt64(aItem), RiskId))
        //                    {
        //                        RiskCategoryManagement.EditRiskCategoryMapping(aItem, RiskId);
        //                    }
        //                    else
        //                    {
        //                        RiskCategoryMapping RiskCategoryMapping = new RiskCategoryMapping()
        //                        {
        //                            RiskCategoryCreationId = RiskId,
        //                            RiskCategoryId = Convert.ToInt32(aItem),
        //                            ProcessId = pid,
        //                            SubProcessId = psid,
        //                        };
        //                        RiskCategoryManagement.CreateRiskCategoryMapping(RiskCategoryMapping);
        //                    }
        //                }
        //                RiskIdProcess.Clear();
        //            }

        //            //if (ClientIdProcess.Count > 0)
        //            //{
        //            //    foreach (var aItem in ClientIdProcess)
        //            //    {
        //            //        if (RiskCategoryManagement.CustomerBranchMappingExists(Convert.ToInt64(aItem), RiskId))
        //            //        {
        //            //            RiskCategoryManagement.EditCustomerBranchMapping(aItem, RiskId);
        //            //        }
        //            //        else
        //            //        {
        //            //            CustomerBranchMapping CustomerBranchMapping = new CustomerBranchMapping()
        //            //            {
        //            //                RiskCategoryCreationId = RiskId,
        //            //                BranchId = Convert.ToInt32(aItem),
        //            //                ProcessId = pid,
        //            //                SubProcessId = psid,
        //            //            };
        //            //            RiskCategoryManagement.CreateCustomerBranchMapping(CustomerBranchMapping);
        //            //        }
        //            //    }
        //            //    ClientIdProcess.Clear();
        //            //}


        //            if (AssertionsIdProcess.Count > 0)
        //            {
        //                foreach (var aItem in AssertionsIdProcess)
        //                {
        //                    if (RiskCategoryManagement.AssertionMappingExists(Convert.ToInt64(aItem), RiskId))
        //                    {
        //                        RiskCategoryManagement.EditAssertionsMapping(aItem, RiskId);
        //                    }
        //                    else
        //                    {
        //                        AssertionsMapping AssertionsMapping = new AssertionsMapping()
        //                        {
        //                            RiskCategoryCreationId = RiskId,
        //                            AssertionId = Convert.ToInt32(aItem),
        //                            ProcessId = pid,
        //                            SubProcessId = psid,
        //                        };
        //                        RiskCategoryManagement.CreateAssertionsMapping(AssertionsMapping);
        //                    }
        //                }
        //                AssertionsIdProcess.Clear();
        //            }
        //            grdRiskActivityMatrix.EditIndex = -1;
        //            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
        //            {
        //                this.BindData("P");
        //            }
        //            else
        //            {
        //                this.BindData("N");
        //            }
        //        }
        //        else
        //        {
        //            //cvProcess.IsValid = false;
        //            // cvSubProcess.IsValid = false;
        //            cvDuplicateEntry.IsValid = false;
        //            // cvDuplicateEntry.ErrorMessage = cvProcess.ErrorMessage + cvSubProcess.ErrorMessage;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }

        //}

        //protected void Delete(object sender, EventArgs e)
        //{
        //    Button btnRemove = (Button)sender;
        //    GridViewRow objGvRow = (GridViewRow)btnRemove.NamingContainer;
        //    string RequestId = Convert.ToString(grdRiskActivityMatrix.DataKeys[objGvRow.RowIndex].Value);
        //    RiskCategoryManagement.Delete(Convert.ToInt32(RequestId));
        //    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
        //    {
        //        this.BindData("P");
        //    }
        //    else
        //    {
        //        this.BindData("N");
        //    }
        //}

        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdRiskActivityMatrix.PageIndex = e.NewPageIndex;
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P");
            }
            else
            {
                this.BindData("N");
            }
        }
        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxControl.Text = string.Empty;
                tbxRiskDesc.Text = string.Empty;
                tbxControlObj.Text = string.Empty;
                ddlRiskCategory.ClearSelection();
                ddlAssertions.ClearSelection();
                ddlClientProcess.ClearSelection();
                //if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                //{
                //    BindProcessPopPup("P");
                //}
                //else
                //{
                //    BindProcessPopPup("N");
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSavePop_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                if ((int)ViewState["Mode"] == 0)
                {
                    #region  Creation    
                    string Controno = tbxControl.Text;
                    string ActivityDescription = tbxRiskDesc.Text;
                    string controlObjective = tbxControlObj.Text;
                    string ClientProcess = ddlClientProcess.SelectedValue;
                    long pid = -1;
                    long psid = -1;

                    if (ddlProcess.SelectedValue != "")
                    {
                        if (ddlProcess.SelectedValue != "All")
                        {
                            pid = Convert.ToInt32(ddlProcess.SelectedValue);
                        }
                    }
                    if (ddlSubProcess.SelectedValue != "")
                    {
                        if (ddlSubProcess.SelectedValue != "All")
                        {
                            psid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                        }
                    }

                    if (ActivityDescription != "" || ActivityDescription != null)
                    {
                        if (controlObjective != "" || controlObjective != null)
                        {
                            RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation()
                            {
                                ControlNo = Controno,
                                ActivityDescription = ActivityDescription,
                                ControlObjective = controlObjective,
                                ProcessId = pid,
                                SubProcessId = psid,
                                IsInternalAudit = "N",
                                CustomerId = customerID,
                                CustomerBranchId = Convert.ToInt32(ddlClientProcess.SelectedValue),
                                LocationType = Convert.ToInt32(ddlLocationType.SelectedValue),
                            };
                            if (RiskCategoryManagement.Exists(riskcategorycreation))
                            {
                                cvDuplicateEntry.ErrorMessage = "Risk Category already exists.";
                                cvDuplicateEntry.IsValid = false;
                                return;
                            }
                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                entities.RiskCategoryCreations.Add(riskcategorycreation);
                                entities.SaveChanges();
                            }
                            RiskIdProcess.Clear();
                            for (int i = 0; i < ddlRiskCategory.Items.Count; i++)
                            {
                                if (ddlRiskCategory.Items[i].Selected)
                                {
                                    RiskIdProcess.Add(Convert.ToInt32(ddlRiskCategory.Items[i].Value));
                                }
                            }
                            AssertionsIdProcess.Clear();
                            for (int i = 0; i < ddlAssertions.Items.Count; i++)
                            {
                                if (ddlAssertions.Items[i].Selected)
                                {
                                    AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertions.Items[i].Value));
                                }
                            }
                            IndustryIdProcess.Clear();
                            for (int i = 0; i < ddlIndultory.Items.Count; i++)
                            {
                                if (ddlIndultory.Items[i].Selected)
                                {
                                    IndustryIdProcess.Add(Convert.ToInt32(ddlIndultory.Items[i].Value));
                                }
                            }

                            if (IndustryIdProcess.Count > 0)
                            {
                                foreach (var aItem in IndustryIdProcess)
                                {
                                    IndustryMapping_Risk IndustryMapping = new IndustryMapping_Risk()
                                    {
                                        RiskCategoryCreationId = riskcategorycreation.Id,
                                        IndustryID = Convert.ToInt32(aItem),
                                        IsActive = true,
                                        EditedDate = DateTime.Now,
                                        EditedBy = Convert.ToInt32(Session["userID"]),
                                        ProcessId = pid,
                                        SubProcessId = psid,
                                    };
                                    RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                }
                                IndustryIdProcess.Clear();
                            }
                            if (RiskIdProcess.Count > 0)
                            {
                                foreach (var aItem in RiskIdProcess)
                                {
                                    RiskCategoryMapping RiskCategoryMapping = new RiskCategoryMapping()
                                    {
                                        RiskCategoryCreationId = riskcategorycreation.Id,
                                        RiskCategoryId = Convert.ToInt32(aItem),
                                        ProcessId = pid,
                                        SubProcessId = psid,
                                        IsActive = true,
                                    };
                                    RiskCategoryManagement.CreateRiskCategoryMapping(RiskCategoryMapping);
                                }
                                RiskIdProcess.Clear();
                            }
                            if (AssertionsIdProcess.Count > 0)
                            {
                                foreach (var aItem in AssertionsIdProcess)
                                {
                                    AssertionsMapping AssertionsMapping = new AssertionsMapping()
                                    {
                                        RiskCategoryCreationId = riskcategorycreation.Id,
                                        AssertionId = Convert.ToInt32(aItem),
                                        ProcessId = pid,
                                        SubProcessId = psid,
                                        IsActive = true,
                                    };
                                    RiskCategoryManagement.CreateAssertionsMapping(AssertionsMapping);
                                }
                                AssertionsIdProcess.Clear();
                            }
                        }
                    }
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        this.BindData("P");
                    }
                    else
                    {
                        this.BindData("N");
                    }
                    #endregion
                }
                else
                {
                    long pid = -1;
                    long psid = -1;
                    try
                    {
                        if (ddlProcess.SelectedValue != "-1" && ddlSubProcess.SelectedValue != "-1")
                        {
                            if (ddlProcess.SelectedItem.Text != "All")
                            {
                                pid = Convert.ToInt32(ddlProcess.SelectedValue);
                            }
                            if (ddlSubProcess.SelectedItem.Text != "All")
                            {
                                psid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                            }
                            long RiskCreationId = Convert.ToInt32(ViewState["RRID"]);
                            RiskCategoryCreation RiskData = new RiskCategoryCreation()
                            {
                                Id = RiskCreationId,
                                ActivityDescription = tbxRiskDesc.Text,
                                ControlObjective = tbxControlObj.Text,
                                LocationType = Convert.ToInt32(ddlLocationType.SelectedValue),
                            };
                            RiskCategoryManagement.RiskUpdateRCM(RiskData);
                            RiskCategoryManagement.UpdateRiskCategoryMapping(RiskCreationId);
                            RiskCategoryManagement.UpdateAssertionsMapping(RiskCreationId);
                            RiskCategoryManagement.UpdateRiskIndustryMapping(RiskCreationId);
                            RiskIdProcess.Clear();
                            for (int i = 0; i < ddlRiskCategory.Items.Count; i++)
                            {
                                if (ddlRiskCategory.Items[i].Selected)
                                {
                                    RiskIdProcess.Add(Convert.ToInt32(ddlRiskCategory.Items[i].Value));
                                }
                            }
                            AssertionsIdProcess.Clear();
                            for (int i = 0; i < ddlAssertions.Items.Count; i++)
                            {
                                if (ddlAssertions.Items[i].Selected)
                                {
                                    AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertions.Items[i].Value));
                                }
                            }
                            IndustryIdProcess.Clear();
                            for (int i = 0; i < ddlIndultory.Items.Count; i++)
                            {
                                if (ddlIndultory.Items[i].Selected)
                                {
                                    IndustryIdProcess.Add(Convert.ToInt32(ddlIndultory.Items[i].Value));
                                }
                            }
                            if (IndustryIdProcess.Count > 0)
                            {
                                foreach (var aItem in IndustryIdProcess)
                                {
                                    IndustryMapping_Risk IndustryMapping = new IndustryMapping_Risk()
                                    {
                                        RiskCategoryCreationId = RiskCreationId,
                                        IndustryID = Convert.ToInt32(aItem),
                                        IsActive = true,
                                        EditedDate = DateTime.Now,
                                        EditedBy = Convert.ToInt32(Session["userID"]),
                                        ProcessId = pid,
                                        SubProcessId = psid,
                                    };
                                    RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                }
                                IndustryIdProcess.Clear();
                            }
                            if (RiskIdProcess.Count > 0)
                            {
                                foreach (var aItem in RiskIdProcess)
                                {
                                    if (RiskCategoryManagement.RiskCategoryMappingExists(Convert.ToInt64(aItem), RiskCreationId))
                                    {
                                        RiskCategoryManagement.EditRiskCategoryMapping(aItem, RiskCreationId);
                                    }
                                    else
                                    {
                                        if (pid != -1 && psid != -1)
                                        {
                                            RiskCategoryMapping RiskCategoryMapping = new RiskCategoryMapping()
                                            {
                                                RiskCategoryCreationId = RiskCreationId,
                                                RiskCategoryId = Convert.ToInt32(aItem),
                                                ProcessId = pid,
                                                SubProcessId = psid,
                                                IsActive = true,
                                            };
                                            RiskCategoryManagement.CreateRiskCategoryMapping(RiskCategoryMapping);
                                        }
                                    }
                                }
                                RiskIdProcess.Clear();
                            }
                            if (AssertionsIdProcess.Count > 0)
                            {
                                foreach (var aItem in AssertionsIdProcess)
                                {
                                    if (RiskCategoryManagement.AssertionMappingExists(Convert.ToInt64(aItem), RiskCreationId))
                                    {
                                        RiskCategoryManagement.EditAssertionsMapping(aItem, RiskCreationId);
                                    }
                                    else
                                    {
                                        if (pid != -1 && psid != -1)
                                        {
                                            AssertionsMapping AssertionsMapping = new AssertionsMapping()
                                            {
                                                RiskCategoryCreationId = RiskCreationId,
                                                AssertionId = Convert.ToInt32(aItem),
                                                ProcessId = pid,
                                                SubProcessId = psid,
                                                IsActive = true,
                                            };
                                            RiskCategoryManagement.CreateAssertionsMapping(AssertionsMapping);
                                        }
                                    }
                                }
                                AssertionsIdProcess.Clear();
                            }


                            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                            {
                                BindData("P");

                            }
                            else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
                            {
                                BindData("N");
                            }
                        }
                        else
                        {
                            rfvProcess.IsValid = false;
                            rfvSubProcess.IsValid = false;
                            cvDuplicateEntry.IsValid = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlClientProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                //BindProcessPopPup("P");
            }
            else
            {
                //BindProcessPopPup("N");
            }
        }
        public static FilterViewRiskRegister GetRiskRegisterByRRID(int RRID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var FilterRiskControl = (from row in entities.FilterViewRiskRegisters
                                         where row.RiskCreationId == RRID
                                         select row).FirstOrDefault();

                return FilterRiskControl;
            }
        }
        protected void grdRiskActivityMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_RISKREGISTER"))
                {
                    int customerID = -1;
                    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    int RRID = Convert.ToInt32(e.CommandArgument.ToString());
                    // ViewState["RCMID"] = RCMID;
                    ViewState["RRID"] = RRID;
                    ViewState["Mode"] = 1;
                    var RRInfo = GetRiskRegisterByRRID(RRID);
                    if (RRInfo != null)
                    {
                        BindPopPupCustomer();
                        if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                        {
                            BindProcessPopPup(Convert.ToInt32(RRInfo.BranchId), Convert.ToInt32(RRInfo.ProcessId), "P");
                        }
                        else
                        {
                            BindProcessPopPup(Convert.ToInt32(RRInfo.BranchId), Convert.ToInt32(RRInfo.ProcessId), "N");
                        }
                        tbxControl.Text = RRInfo.ControlNo;
                        tbxRiskDesc.Text = RRInfo.ActivityDescription;
                        tbxControlObj.Text = RRInfo.ControlObjective;
                        SplitString(ShowRiskCategoryName(RRID), ddlRiskCategory);
                        SplitString(ShowAssertionsName(RRID), ddlAssertions);
                        SplitString(ShowIndustryName(RRID), ddlIndultory);
                        ddlClientProcess.SelectedValue = Convert.ToString(RRInfo.BranchId);
                        ddlProcess.SelectedValue = Convert.ToString(RRInfo.ProcessId);
                        ddlSubProcess.SelectedValue = Convert.ToString(RRInfo.SubProcessId);
                        ddlLocationType.SelectedValue = Convert.ToString(RRInfo.LocationType);
                        upComplianceDetails.Update();
                    }
                }
                else if (e.CommandName.Equals("DELETE_RISKREGISTER"))
                {
                    int RiskCreationId = Convert.ToInt32(e.CommandArgument);
                    RiskCategoryManagement.Delete(Convert.ToInt32(RiskCreationId));
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        this.BindData("P");
                    }
                    else
                    {
                        this.BindData("N");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdRiskActivityMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
        }
        protected void checkBoxesProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (grdRiskActivityMatrix.Rows.Count >= 0)
                {
                    Saplin.Controls.DropDownCheckBoxes DrpIndustryProcess = (DropDownCheckBoxes)sender;
                    IndustryIdProcess.Clear();
                    for (int i = 0; i < DrpIndustryProcess.Items.Count; i++)
                    {
                        if (DrpIndustryProcess.Items[i].Selected)
                        {
                            IndustryIdProcess.Add(Convert.ToInt32(DrpIndustryProcess.Items[i].Value));
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void checkBoxesRiskCategoryProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            if (grdRiskActivityMatrix.Rows.Count >= 0)
            {
                Saplin.Controls.DropDownCheckBoxes DrpRiskCategoryProcess = (DropDownCheckBoxes)sender;
                RiskIdProcess.Clear();
                for (int i = 0; i < DrpRiskCategoryProcess.Items.Count; i++)
                {
                    if (DrpRiskCategoryProcess.Items[i].Selected)
                    {
                        RiskIdProcess.Add(Convert.ToInt32(DrpRiskCategoryProcess.Items[i].Value));
                    }
                }
            }
        }      
        protected void checkBoxesAssertionsProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            if (grdRiskActivityMatrix.Rows.Count >= 0)
            {
                Saplin.Controls.DropDownCheckBoxes ddlAssertionsProcess = (DropDownCheckBoxes)sender;
                AssertionsIdProcess.Clear();
                for (int i = 0; i < ddlAssertionsProcess.Items.Count; i++)
                {
                    if (ddlAssertionsProcess.Items[i].Selected)
                    {
                        AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertionsProcess.Items[i].Value));
                    }
                }
            }
        }
        #endregion

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindGrid();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindGrid();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {
                }
                //Reload the Grid
                BindGrid();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }


    }
}