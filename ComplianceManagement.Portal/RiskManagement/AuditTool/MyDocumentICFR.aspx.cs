﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class MyDocumentICFR : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        static bool auditHeadFlag;
        public static string linkclick;
        protected static string AuditHeadOrManagerReport;
        public static bool ApplyFilter;
        protected bool personresponsibleapplicable = false;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesICFR(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            personresponsibleapplicable = UserManagementRisk.PersonResponsibleExists(Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                // CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindCustomerList();
                BindLegalEntityData();
                BindFinancialYear();

                string FinancialYear = DateTimeExtensions.GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }

                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        auditHeadFlag = true;
                        ShowAuditHead(sender, e);
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                }
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
        }


        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdMyDocumentICFR.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdMyDocumentICFR.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindGrid();
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liPerformer.Attributes.Add("class", "active");
            liReviewer.Attributes.Add("class", "");
            liAuditHead.Attributes.Add("class", "");

            PerformerFlag = true;
            ReviewerFlag = false;
            auditHeadFlag = false;

            BindGrid();
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liPerformer.Attributes.Add("class", "");
            liReviewer.Attributes.Add("class", "active");
            liAuditHead.Attributes.Add("class", "");

            PerformerFlag = false;
            ReviewerFlag = true;
            auditHeadFlag = false;

            BindGrid();
        }

        protected void ShowAuditHead(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "");
            liAuditHead.Attributes.Add("class", "active");

            ReviewerFlag = false;
            PerformerFlag = false;
            auditHeadFlag = true;

            BindGrid();
        }

        public void BindGrid()
        {
            try
            {
                string FinancialYear = string.Empty;
                string ForMonth = string.Empty;
                string auditHeadManagerFlag = string.Empty;

                int CustBranchID = -1;
                int UserID = -1;
                int Processid = -1;
                int SubProcessID = -1;
                int RoleID = -1;

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                UserID = Portal.Common.AuthenticationHelper.UserID;

                if (PerformerFlag)
                    RoleID = 3;
                if (ReviewerFlag)
                    RoleID = 4;
                if (auditHeadFlag)
                {
                    RoleID = 4;
                    auditHeadManagerFlag = AuditHeadOrManagerReport;
                }

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        ForMonth = ddlPeriod.SelectedItem.Text;
                    }
                }
                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        Processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                {
                    if (ddlSubProcess.SelectedValue != "-1")
                    {
                        SubProcessID = Convert.ToInt32(ddlSubProcess.SelectedValue);
                    }
                }

                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(CustomerId, CustBranchID);
                var Branchlistloop = Branchlist.ToList();

                var RowData = UserManagementRisk.GetAllIFCDocuments(Branchlist, FinancialYear, ForMonth, Processid, SubProcessID, UserID, RoleID, auditHeadManagerFlag, CustomerId);
                grdMyDocumentICFR.DataSource = RowData;
                grdMyDocumentICFR.DataBind();

                Session["TotalRows"] = RowData.Count;

                bindPageNumber();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public void BindLegalEntityData()
        {
            //if (CustomerId == 0)
            //{
            //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //}
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        //public void BindProcess(int Branchid)
        //{
        //    if (CustomerId == 0)
        //    {
        //        CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
        //    }
        //    ddlProcess.Items.Clear();
        //    ddlProcess.DataTextField = "Name";
        //    ddlProcess.DataValueField = "Id";
        //    ddlProcess.DataSource = ProcessManagement.FillProcess(CustomerId, Branchid);
        //    ddlProcess.DataBind();
        //    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
        //}
        public void BindProcess(int Branchid)
        {
            //if (CustomerId == 0)
            //{
            //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //}
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            ddlProcess.Items.Clear();
            ddlProcess.DataTextField = "Name";
            ddlProcess.DataValueField = "Id";
            if (AuditHeadOrManagerReport == null)
            {
                AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            }
            if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                ddlProcess.DataSource = ProcessManagement.FillProcessDropdownAuditManager(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                ddlProcess.DataBind();
                ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
            }
            else
            {
                ddlProcess.DataSource = ProcessManagement.FillProcessDropdownPerformerAndReviewer(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                ddlProcess.DataBind();
                ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
            }
        }
        public void BindSubProcess(int DDLProcessId)
        {
            ddlSubProcess.Items.Clear();
            ddlSubProcess.DataTextField = "Name";
            ddlSubProcess.DataValueField = "Id";
            ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(DDLProcessId);
            ddlSubProcess.DataBind();
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            //if (CustomerId == 0)
            //{
            //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //}
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (personresponsibleapplicable)
            {
                DRP.DataSource = AuditKickOff_NewDetails.PersonResponsibleFillSubEntityDataICFR(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (roles.Contains(3) || roles.Contains(4))
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityDataICFR(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindProcess(Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }

            BindGrid();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdMyDocumentICFR.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                BindProcess(Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }

            BindGrid();
            //bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdMyDocumentICFR.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                BindProcess(Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }

            BindGrid();
            //bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdMyDocumentICFR.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                BindProcess(Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindGrid();
            //bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdMyDocumentICFR.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess(Convert.ToInt32(ddlFilterLocation.SelectedValue));
            BindGrid();
            //bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdMyDocumentICFR.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdMyDocumentICFR.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);


                //Reload the Grid
                BindGrid();
                //bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdMyDocumentICFR.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }



        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            //bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdMyDocumentICFR.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSubProcess(Convert.ToInt32(ddlProcess.SelectedValue));
            BindGrid();
            //bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdMyDocumentICFR.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            //bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdMyDocumentICFR.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            //bindPageNumber();
            int count = Convert.ToInt32(GetTotalPagesCount());
            if (count > 0)
            {
                int gridindex = grdMyDocumentICFR.PageIndex;
                string chkcindition = (gridindex + 1).ToString();
                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
            }
        }

        protected void lbOriDocDownload_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btndetails = sender as LinkButton;
                GridViewRow gvrow = (GridViewRow)btndetails.NamingContainer;
                string[] arguments = btndetails.CommandArgument.ToString().Split(new char[] { ',' });
                if (arguments.Length > 0)
                {
                    int CustomerBranchID = -1;
                    string ForMonth = string.Empty;
                    string FinancialYear = string.Empty;
                    int UserID = -1;
                    int RiskCreationID = -1;
                    int RoleID = -1;
                    if (Convert.ToInt32(arguments[0]) != -1)
                    {
                        CustomerBranchID = Convert.ToInt32(arguments[0]);
                    }
                    if (!string.IsNullOrEmpty(arguments[1]))
                    {
                        ForMonth = Convert.ToString(arguments[1]);
                    }
                    if (!string.IsNullOrEmpty(arguments[2]))
                    {
                        FinancialYear = Convert.ToString(arguments[2]);
                    }
                    if (Convert.ToInt32(arguments[3]) != -1)
                    {
                        UserID = Convert.ToInt32(arguments[3]);
                    }
                    if (Convert.ToInt32(arguments[4]) != -1)
                    {
                        RiskCreationID = Convert.ToInt32(arguments[4]);
                    }
                    if (Convert.ToInt32(arguments[5]) != -1)
                    {
                        RoleID = Convert.ToInt32(arguments[5]);
                    }

                    using (ZipFile ICFRZip = new ZipFile())
                    {
                        //if (CustomerId == 0)
                        //{
                        //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        //}
                        if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                        {
                            if (ddlCustomer.SelectedValue != "-1")
                            {
                                CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                            }
                        }
                        var ICFRData = UserManagementRisk.GetICFROrignalFilesData(CustomerId, CustomerBranchID, UserID, RoleID, RiskCreationID, FinancialYear, ForMonth, AuditHeadOrManagerReport);
                        ICFRZip.AddDirectoryByName((arguments[2]) + "/" + arguments[1]);

                        if (ICFRData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ICFRData)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    //string[] filename = file.Name.Split('.');
                                    //string str = filename[0] + i + "." + filename[1];

                                    int idx = file.Name.LastIndexOf('.');
                                    string str = file.Name.Substring(0, idx) + "_" + i + "." + file.Name.Substring(idx + 1);

                                    if (!ICFRZip.ContainsEntry(arguments[2] + "/" + arguments[1] + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            ICFRZip.AddEntry(arguments[2] + "/" + arguments[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ICFRZip.AddEntry(arguments[2] + "/" + arguments[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                    i++;
                                }
                            }

                            var zipMs = new MemoryStream();
                            ICFRZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TestingDocuments.zip");
                            Response.BinaryWrite(Filedata);
                            Response.Flush();
                            //Response.End();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "There is no document available for download.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw;
            }
        }

        protected void lbReviewDocDownload_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btndetails = sender as LinkButton;
                GridViewRow gvrow = (GridViewRow)btndetails.NamingContainer;
                string[] arguments = btndetails.CommandArgument.ToString().Split(new char[] { ',' });
                if (arguments.Length > 0)
                {
                    int CustomerBranchID = -1;
                    string ForMonth = string.Empty;
                    string FinancialYear = string.Empty;
                    int UserID = -1;
                    int RiskCreationID = -1;
                    int ScheduledOnID = -1;
                    if (Convert.ToInt32(arguments[0]) != -1)
                    {
                        CustomerBranchID = Convert.ToInt32(arguments[0]);
                    }
                    if (!string.IsNullOrEmpty(arguments[1]))
                    {
                        ForMonth = Convert.ToString(arguments[1]);
                    }
                    if (!string.IsNullOrEmpty(arguments[2]))
                    {
                        FinancialYear = Convert.ToString(arguments[2]);
                    }
                    if (Convert.ToInt32(arguments[3]) != -1)
                    {
                        UserID = Convert.ToInt32(arguments[3]);
                    }
                    if (Convert.ToInt32(arguments[4]) != -1)
                    {
                        RiskCreationID = Convert.ToInt32(arguments[4]);
                    }
                    if (Convert.ToInt32(arguments[5]) != -1)
                    {
                        ScheduledOnID = Convert.ToInt32(arguments[5]);
                    }
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        ComplianceZip.AddDirectoryByName((arguments[2]) + "/" + arguments[1]);
                        var ReviewFiledata = UserManagementRisk.GetReveiwFilesData(CustomerBranchID, UserID, RiskCreationID, FinancialYear, ForMonth, ScheduledOnID);//UserID

                        if (ReviewFiledata.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ReviewFiledata)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    //string[] filename = file.Name.Split('.');
                                    //string str = filename[0] + i + "." + filename[1];

                                    int idx = file.Name.LastIndexOf('.');
                                    string str = file.Name.Substring(0, idx) + "_" + i + "." + file.Name.Substring(idx + 1);
                                    if (!ComplianceZip.ContainsEntry(file.ForMonth + "/" + arguments[1] + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(file.ForMonth + "/" + arguments[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(file.ForMonth + "/" + arguments[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                    i++;
                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=ReviewDocument.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            //Response.End();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "There is no document available for download.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw;
            }
        }

        private void BindCustomerList()
        {
            long userId = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            var customerList = RiskCategoryManagement.GetAllMappedCustomerForIFCPerformer(userId);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Customer", "-1"));
                ddlCustomer.SelectedIndex = 2;
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (ddlProcess.Items.Count > 0)
                ddlProcess.Items.Clear();

            if (ddlSubProcess.Items.Count > 0)
                ddlSubProcess.Items.Clear();


            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    BindLegalEntityData();
                    BindGrid();
                }
                else
                {
                    ddlSubEntity1.DataSource = null;
                    ddlSubEntity1.DataBind();
                    ddlSubEntity1.Items.Clear();

                    ddlSubEntity2.DataSource = null;
                    ddlSubEntity2.DataBind();
                    ddlSubEntity2.Items.Clear();

                    ddlSubEntity3.DataSource = null;
                    ddlSubEntity3.DataBind();
                    ddlSubEntity3.Items.Clear();

                    ddlFilterLocation.DataSource = null;
                    ddlFilterLocation.DataBind();
                    ddlFilterLocation.Items.Clear();

                    ddlProcess.DataSource = null;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Clear();

                    ddlSubProcess.DataSource = null;
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Clear();

                    ddlLegalEntity.DataSource = null;
                    ddlLegalEntity.DataBind();
                    ddlLegalEntity.Items.Clear();

                    grdMyDocumentICFR.DataSource = null;
                    grdMyDocumentICFR.DataBind();

                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Customer.";
                }
            }

        }
    }
}