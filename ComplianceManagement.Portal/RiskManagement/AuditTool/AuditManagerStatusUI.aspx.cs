﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Saplin.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class AuditManagerStatusUI : System.Web.UI.Page
    {
        int Statusid;
        protected List<Int32> roles;
        public static List<long> Branchlist = new List<long>();
        protected int CustomerId = 0;
        public static List<sp_PerformerReviewView_Result> PerformerReviewViewList = new List<sp_PerformerReviewView_Result>();
        protected bool DepartmentHead = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                BindCustomerList();
                string customerIdCommaSeparatedList = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerId"]))
                {
                    customerIdCommaSeparatedList = Request.QueryString["CustomerId"].ToString();
                }
                if (!string.IsNullOrEmpty(customerIdCommaSeparatedList))
                {
                    List<string> customerIdList = customerIdCommaSeparatedList.Split(',').ToList();
                    if (customerIdList.Count > 0)
                    {
                        for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                        {
                            foreach (string cId in customerIdList)
                            {
                                if (ddlCustomerMultiSelect.Items[i].Value == cId)
                                {
                                    ddlCustomerMultiSelect.Items[i].Selected = true;
                                }
                            }
                        }
                        Session["CustIdFromQueryString"] = customerIdCommaSeparatedList;
                    }
                }

                BindFinancialYear();
                string financialYearCommaSeparatedList = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["FY"]))
                {
                    financialYearCommaSeparatedList = Request.QueryString["FY"].ToString();
                }
                if (!string.IsNullOrEmpty(financialYearCommaSeparatedList))
                {
                    List<string> finYearsList = financialYearCommaSeparatedList.Split(',').ToList();
                    if (finYearsList.Count > 0)
                    {
                        for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                        {
                            foreach (string fy in finYearsList)
                            {
                                if (ddlFinancialYearMultiSelect.Items[i].Text == fy)
                                {
                                    ddlFinancialYearMultiSelect.Items[i].Selected = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                    if (FinancialYear != null)
                    {
                        for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                        {
                            if (ddlFinancialYearMultiSelect.Items[i].Text == FinancialYear)
                            {
                                ddlFinancialYearMultiSelect.Items[i].Selected = true;
                            }
                        }
                    }
                }


                BindLegalEntityData();
                BindVertical();
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    if (Request.QueryString["Status"] == "Open")
                    {
                        ViewState["Status"] = 1;
                    }
                    else if (Request.QueryString["Status"] == "Closed")
                    {
                        ViewState["Status"] = 3;
                    }
                }
                else
                {
                    ViewState["Status"] = 1;
                }
                PerformerReviewViewList = DashboardManagementRisk.sp_GetPerFormerReviewer();
                if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    if (Request.QueryString["Type"] == "Process")
                    {
                        ShowProcessGrid(sender, e);
                    }
                    else if (Request.QueryString["Type"] == "Implementation")
                    {
                        ShowImplementationGrid(sender, e);
                    }
                    else
                    {
                        ShowProcessGrid(sender, e);
                    }
                }
                string subEntityBranchIdCommanSeparatedList = string.Empty;
                if (!String.IsNullOrEmpty(Request.QueryString["IsHiddenSubEntity"]))
                {
                    subEntityBranchIdCommanSeparatedList = Request.QueryString["IsHiddenSubEntity"].ToString();
                }
                List<long> CHKBranchlist = new List<long>();
                if (!string.IsNullOrEmpty(subEntityBranchIdCommanSeparatedList) && subEntityBranchIdCommanSeparatedList != "-1")
                {
                    List<string> branchIdsList = subEntityBranchIdCommanSeparatedList.Split(',').ToList();
                    if (branchIdsList.Count > 0)
                    {
                        for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                        {
                            foreach (string bId in branchIdsList)
                            {
                                if (ddlLegalEntityMultiSelect.Items[i].Value == bId)
                                {
                                    ddlLegalEntityMultiSelect.Items[i].Selected = true;
                                    CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                                }
                            }
                        }
                    }
                }
                List<long> SelectedSubEnityOneListId = new List<long>();
                if (CHKBranchlist.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity1MultiSelect, CHKBranchlist);
                    string subEntityOneIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["IsHiddenSubEntityOne"]))
                    {
                        subEntityOneIdCommaSeparatedList = Request.QueryString["IsHiddenSubEntityOne"].ToString();
                        if (!string.IsNullOrEmpty(subEntityOneIdCommaSeparatedList) && subEntityOneIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = subEntityOneIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlSubEntity1MultiSelect.Items[i].Value == bId)
                                        {
                                            ddlSubEntity1MultiSelect.Items[i].Selected = true;
                                            SelectedSubEnityOneListId.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                List<long> SelectedSubEnityTwoListId = new List<long>();
                if (SelectedSubEnityOneListId.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity2MultiSelect, SelectedSubEnityOneListId);
                    string subEntityTwoIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["IsHiddenSubEntityTwo"]))
                    {
                        subEntityTwoIdCommaSeparatedList = Request.QueryString["IsHiddenSubEntityTwo"].ToString();
                        if (!string.IsNullOrEmpty(subEntityTwoIdCommaSeparatedList) && subEntityTwoIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = subEntityTwoIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlSubEntity2MultiSelect.Items[i].Value == bId)
                                        {
                                            ddlSubEntity2MultiSelect.Items[i].Selected = true;
                                            SelectedSubEnityTwoListId.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                List<long> SelectedSubEnityThreeListId = new List<long>();
                if (SelectedSubEnityTwoListId.Count > 0)
                {
                    BindSubEntityData(ddlSubEntity3MultiSelect, SelectedSubEnityTwoListId);

                    string subEntityThreeIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["IsHiddenSubEntityThree"]))
                    {
                        subEntityThreeIdCommaSeparatedList = Request.QueryString["IsHiddenSubEntityThree"].ToString();
                        if (!string.IsNullOrEmpty(subEntityThreeIdCommaSeparatedList) && subEntityThreeIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = subEntityThreeIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlSubEntity3MultiSelect.Items[i].Value == bId)
                                        {
                                            ddlSubEntity3MultiSelect.Items[i].Selected = true;
                                            SelectedSubEnityThreeListId.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (SelectedSubEnityTwoListId.Count > 0)
                {
                    BindSubEntityData(ddlFilterLocationMultiSelect, SelectedSubEnityThreeListId);

                    string filterLocationIdCommaSeparatedList = string.Empty;
                    if (!string.IsNullOrEmpty(Request.QueryString["IsHiddenSubEntityFour"]))
                    {
                        filterLocationIdCommaSeparatedList = Request.QueryString["IsHiddenSubEntityFour"].ToString();
                        if (!string.IsNullOrEmpty(filterLocationIdCommaSeparatedList) && filterLocationIdCommaSeparatedList != "-1")
                        {
                            List<string> branchIdsList = filterLocationIdCommaSeparatedList.Split(',').ToList();
                            if (branchIdsList.Count > 0)
                            {
                                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                                {
                                    foreach (string bId in branchIdsList)
                                    {
                                        if (ddlFilterLocationMultiSelect.Items[i].Value == bId)
                                        {
                                            ddlFilterLocationMultiSelect.Items[i].Selected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                BindSchedulingType();
                for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
                {
                    ddlSchedulingTypeMultiSelect.Items[i].Selected = true;
                }
                BindData();
                bindPageNumber();
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(List<int> customerIDList, List<int> customerBranchIdList)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && customerIDList.Contains(row.CustomerID)
                             && customerBranchIdList.Contains(row.ID)
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerIDList, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(List<int> customerIdList, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && customerIdList.Contains(row.CustomerID)
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerIdList, item, false, entities);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
            {
                if (ViewState["Type"].ToString() == "Process")
                {
                    grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdProcessAudits.PageIndex = chkSelectedPage - 1;
                }
                else if (ViewState["Type"].ToString() == "Implementation")
                {
                    grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdImplementationAudits.PageIndex = chkSelectedPage - 1;
                }
            }
            BindData();
        }

        public void BindFinancialYear()
        {
            ddlFinancialYearMultiSelect.DataTextField = "Name";
            ddlFinancialYearMultiSelect.DataValueField = "ID";
            ddlFinancialYearMultiSelect.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYearMultiSelect.DataBind();
        }

        public void BindLegalEntityData()
        {
            List<int?> CustomerIdList = new List<int?>();
            for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
            {
                if (ddlCustomerMultiSelect.Items[i].Selected)
                {
                    CustomerIdList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                }
            }
            if (CustomerIdList.Count > 0)
            {
                ddlLegalEntityMultiSelect.DataTextField = "Name";
                ddlLegalEntityMultiSelect.DataValueField = "ID";
                ddlLegalEntity.Items.Clear();
                ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataForDashBoard(CustomerIdList);
                ddlLegalEntityMultiSelect.DataBind();
            }
        }

        public void BindSubEntityData(DropDownCheckBoxes DRP, List<long> ParentIdList)
        {
            List<int> CustomerIdList = new List<int>();
            for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
            {
                if (ddlCustomerMultiSelect.Items[i].Selected)
                {
                    CustomerIdList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                }
            }
            if (CustomerIdList.Count == 0)
            {
                CustomerIdList.Add(-1);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityDataListForDashBoard(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CustomerIdList, ParentIdList);
            DRP.DataBind();
        }

        public void BindAuditSchedule(List<string> flagList, List<int?> countList)
        {
            List<string> FlagList = new List<string>();
            try
            {
                foreach (var flag in flagList)
                {
                    if (flag == "A")
                    {
                        FlagList.Add("Annually");
                    }
                    else if (flag == "H")
                    {
                        FlagList.Add("Apr-Sep");
                        FlagList.Add("Oct-Mar");
                    }
                    else if (flag == "Q")
                    {
                        FlagList.Add("Apr-Jun");
                        FlagList.Add("Jul-Sep");
                        FlagList.Add("Oct-Dec");
                        FlagList.Add("Jan-Mar");
                    }
                    else if (flag == "M")
                    {
                        FlagList.Add("Apr");
                        FlagList.Add("May");
                        FlagList.Add("Jun");
                        FlagList.Add("Jul");
                        FlagList.Add("Aug");
                        FlagList.Add("Sep");
                        FlagList.Add("Oct");
                        FlagList.Add("Nov");
                        FlagList.Add("Dec");
                        FlagList.Add("Jan");
                        FlagList.Add("Feb");
                        FlagList.Add("Mar");
                    }
                    else if (flag == "S")
                    {
                        FlagList.Add("Select Period");
                        FlagList.Add("Special Audit");
                    }
                    else
                    {
                        int count = countList.Count;
                        if (count == 1)
                        {
                            FlagList.Add("Phase1");
                        }
                        else if (count == 2)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                        }
                        else if (count == 3)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                        }
                        else if (count == 4)
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                            FlagList.Add("Phase4");
                        }
                        else
                        {
                            FlagList.Add("Phase1");
                            FlagList.Add("Phase2");
                            FlagList.Add("Phase3");
                            FlagList.Add("Phase4");
                            FlagList.Add("Phase5");
                        }
                    }
                }
                if (FlagList.Count > 0)
                {
                    int setIndex = 0;
                    ddlPeriodMultiSelect.Items.Clear();
                    foreach (string item in FlagList)
                    {
                        ddlPeriodMultiSelect.Items.Insert(setIndex, item);
                        setIndex = setIndex + 1;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public void BindSchedulingType()
        {
            List<int> branchidList = new List<int>();
            bool clearLegalBranchList = false;
            bool clearSubEntity1List = false;
            bool clearSubEntity2List = false;
            bool clearSubEntity3List = false;
            bool clearFilterLocation = false;

            for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
            {
                if (ddlLegalEntityMultiSelect.Items[i].Selected)
                {
                    clearLegalBranchList = true;
                    branchidList.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity1MultiSelect.Items[i].Selected)
                {
                    if (clearLegalBranchList && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearLegalBranchList = false;
                    }

                    clearSubEntity1List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity2MultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity1List && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearSubEntity1List = false;
                    }
                    clearSubEntity2List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity3MultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity2List && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearSubEntity2List = false;
                    }
                    clearSubEntity3List = true;
                    branchidList.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                }
            }

            for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
            {
                if (ddlFilterLocationMultiSelect.Items[i].Selected)
                {
                    if (clearSubEntity3List && branchidList.Count > 0)
                    {
                        branchidList.Clear();
                        clearSubEntity3List = false;
                    }
                    clearFilterLocation = true;
                    branchidList.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                }
            }
            ddlSchedulingTypeMultiSelect.DataTextField = "Name";
            ddlSchedulingTypeMultiSelect.DataValueField = "ID";
            ddlSchedulingTypeMultiSelect.DataSource = UserManagementRisk.FillSchedulingTypeForAuditHeadDashbaord(branchidList);
            ddlSchedulingTypeMultiSelect.DataBind();
        }
        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        if (ddlCustomer.SelectedValue != "-1")
                        {
                            CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                            branchid = CustomerId;
                        }
                    }

                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                //ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindData()
        {
            try
            {
                string Flag = String.Empty;
                List<string> FinancialYearList = new List<string>();
                List<string> PeriodList = new List<string>();
                int VerticalID = -1;
                string departmentheadFlag = string.Empty;
                if (DepartmentHead)
                {
                    departmentheadFlag = "DH";
                }
                List<int> CustomerIdList = new List<int>();
                List<int?> CustomerIdList_1 = new List<int?>();
                for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                {
                    if (ddlCustomerMultiSelect.Items[i].Selected)
                    {
                        CustomerIdList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                        CustomerIdList_1.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                    }
                }
                if (CustomerIdList.Count == 0)
                {
                    CustomerIdList.Add(-1);
                }
                bool clearLegalBranchList = false;
                bool clearSubEntity1List = false;
                bool clearSubEntity2List = false;
                bool clearSubEntity3List = false;
                bool clearFilterLocation = false;
                List<int?> CHKBranchlist = new List<int?>();
                List<int> VerticalIDList = new List<int>();
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity1MultiSelect.Items[i].Selected)
                    {
                        if (clearLegalBranchList && CHKBranchlist.Count > 0)
                        {
                            CHKBranchlist.Clear();
                            clearLegalBranchList = false;
                        }
                        clearSubEntity1List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                    }

                }

                for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity2MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity1List && CHKBranchlist.Count > 0)
                        {
                            CHKBranchlist.Clear();
                            clearSubEntity1List = false;
                        }
                        clearSubEntity2List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity3MultiSelect.Items.Count; i++)
                {
                    if (ddlSubEntity3MultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity2List && CHKBranchlist.Count > 0)
                        {
                            CHKBranchlist.Clear();
                            clearSubEntity2List = false;
                        }
                        clearSubEntity3List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlFilterLocationMultiSelect.Items.Count; i++)
                {

                    if (ddlFilterLocationMultiSelect.Items[i].Selected)
                    {
                        if (clearSubEntity3List && CHKBranchlist.Count > 0)
                        {
                            CHKBranchlist.Clear();
                            clearSubEntity3List = false;
                        }
                        clearFilterLocation = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocationMultiSelect.Items[i].Value));
                    }

                }

                for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                {
                    if (ddlFinancialYearMultiSelect.Items[i].Selected)
                    {
                        FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                    }
                }

                for (int i = 0; i < ddlPeriodMultiSelect.Items.Count; i++)
                {
                    if (ddlPeriodMultiSelect.Items[i].Selected)
                    {
                        PeriodList.Add(ddlPeriodMultiSelect.Items[i].Text);
                    }
                }
                if (!String.IsNullOrEmpty(ViewState["Status"].ToString()))
                {
                    Statusid = Convert.ToInt32(ViewState["Status"]);
                }
                if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
                {
                    Flag = Convert.ToString(ViewState["Type"]);
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    List<int> verticalIdsList = UserManagementRisk.VerticalgetBycustomeridListForDashboard(CustomerIdList);
                    if (verticalIdsList.Count > 0)
                    {
                        foreach (var vId in verticalIdsList)
                        {
                            VerticalIDList.Add(Convert.ToInt32(vId));
                        }
                    }

                    //if (VerticalIDList.Count > 0)
                    //{
                    //    verticalIDCommaSeparatedList = string.Join(",", VerticalIDList);
                    //}
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }
                if (FinancialYearList.Count == 0)
                    FinancialYearList.Add(GetCurrentFinancialYear(DateTime.Now.Date));

                Branchlist.Clear();
                List<int> branchIdList = new List<int>();
                if (CHKBranchlist.Count > 0)
                {
                    foreach (var branchId in CHKBranchlist)
                    {
                        branchIdList.Add(Convert.ToInt32(branchId));
                    }
                }
                GetAllHierarchy(CustomerIdList, branchIdList);
                Branchlist.ToList();
                List<int?> CustomerBranchIds = new List<int?>();
                if (Branchlist.Count > 0)
                {
                    
                    foreach (var item in Branchlist)
                    {
                        CustomerBranchIds.Add(Convert.ToInt32(item));
                    }
                }
                if (Flag == "Process")
                {
                    grdProcessAudits.DataSource = null;
                    grdProcessAudits.DataBind();

                    grdImplementationAudits.Visible = false;
                    grdProcessAudits.Visible = true;
                    var AuditLists = DashboardManagementRisk.GetAuditManagerAuditsForDashbaord(FinancialYearList, PeriodList, CustomerIdList_1, Branchlist, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, -1, 4, departmentheadFlag);

                    grdProcessAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdProcessAudits.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }
                else if (Flag == "Implementation")
                {
                    grdProcessAudits.DataSource = null;
                    grdProcessAudits.DataBind();

                    grdImplementationAudits.Visible = true;
                    grdProcessAudits.Visible = false;

                    var AuditLists = DashboardManagementRisk.GetAuditManagerAuditsIMPForDashBoard(FinancialYearList, PeriodList, CustomerIdList_1, CustomerBranchIds, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, Statusid, VerticalIDList);
                    grdImplementationAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdImplementationAudits.DataBind();

                    AuditLists.Clear();
                    AuditLists = null;
                }

                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    // BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    //BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    //BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    // BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }

            BindSchedulingType();
            BindData();
            BindVertical();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                {
                    BindSchedulingType();
                    BindVertical();
                    BindData();
                }
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlSchedulingType.SelectedItem.Text == "Annually")
            //{
            //    BindAuditSchedule("A", 0);
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            //{
            //    BindAuditSchedule("H", 0);
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            //{
            //    BindAuditSchedule("Q", 0);
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            //{
            //    BindAuditSchedule("M", 0);
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            //{
            //    BindAuditSchedule("S", 0);
            //}
            //else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            //{
            //    if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            //    {
            //        if (ddlFilterLocation.SelectedValue != "-1")
            //        {
            //            int count = 0;
            //            count = UserManagementRisk.GetPhaseCount(Convert.ToInt32(ddlFilterLocation.SelectedValue));
            //            BindAuditSchedule("P", count);
            //        }
            //    }
            //}
            //bindPageNumber();
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
                {
                    if (ViewState["Type"].ToString() == "Process")
                    {
                        grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    }
                    else if (ViewState["Type"].ToString() == "Implementation")
                    {
                        grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    }
                }

                BindData();
                if (ViewState["Type"].ToString() == "Process")
                {
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdProcessAudits.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
                else if (ViewState["Type"].ToString() == "Implementation")
                {
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdImplementationAudits.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void grdProcessAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    String Args = e.CommandArgument.ToString();

                    string[] arg = Args.ToString().Split(',');

                    string URLStr = string.Empty;

                    URLStr = "~/RiskManagement/InternalAuditTool/AuditManagerMainUI.aspx?Status=" + arg[0] + "&CustBranchID=" + arg[1] + "&peroid=" + arg[2] + "&FY=" + arg[3] + "&VID=" + arg[4] + "&AuditID=" + arg[5] + "&ID=0" + "&SID=0" + "&PID=0" + "&SPID=0" + "&AH=True";

                    if (Args != "")
                        Response.Redirect(URLStr, false);

                }
                else if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string custId = string.Empty;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                    string ForMonth = Convert.ToString(commandArgs[1]);
                    string FinancialYear = Convert.ToString(commandArgs[2]);
                    int Verticalid = Convert.ToInt32(commandArgs[3]);
                    var AuditID = Convert.ToInt32(commandArgs[4]);
                    var AuditDetail = UserManagementRisk.GetAuditDatail(AuditID);
                    if (AuditDetail != null)
                    {
                        custId= Convert.ToString(AuditDetail.CustomerID);
                    }
                    
                    //if (!string.IsNullOrEmpty(Convert.ToString(Session["CustIdFromQueryString"])))
                    //{
                    //    custId = Convert.ToString(Session["CustIdFromQueryString"]);
                    //}

                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerStatusSummary.aspx?AuditID=" + AuditID + "&FY=" + FinancialYear + "&Type=Process" + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString() + "&CustomerId=" + custId, false);
                    }
                    else
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerStatusSummary.aspx?AuditID=" + AuditID + "&FY=" + FinancialYear + "&Type=Process" + "&ReturnUrl1=" + "&CustomerId=" + custId, false);
                    }
                    //}
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdImplementationAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                    string ForMonth = Convert.ToString(commandArgs[1]);
                    string FinancialYear = Convert.ToString(commandArgs[2]);
                    int Verticalid = Convert.ToInt32(commandArgs[3]);
                    long AuditID = Convert.ToInt64(commandArgs[4]);
                    string custId = string.Empty;
                    var AuditDetail = UserManagementRisk.GetAuditDatail(AuditID);
                    if (AuditDetail != null)
                    {
                        custId = Convert.ToString(AuditDetail.CustomerID);
                    }
                    //if (!string.IsNullOrEmpty(Convert.ToString(Session["CustIdFromQueryString"])))
                    //{
                    //    custId = Convert.ToString(Session["CustIdFromQueryString"]);
                    //}
                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString() + "&AuditID=" + AuditID + "&FY=" + FinancialYear + "&CustomerId=" + custId, false);
                    }
                    else
                    {
                        Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=" + "&AuditID=" + AuditID + "&FY=" + FinancialYear + "&CustomerId=" + custId, false);
                    }
                }
                else if (e.CommandName.Equals("DRILLDOWNIMP"))
                {
                    string custId = string.Empty;
                    string[] arg = e.CommandArgument.ToString().Split(new char[] { ',' });
                    Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerMainUI_IMP.aspx?Status=" + "FinalReview" + "&ID=" + -1 + "&SID=" + -1 + "&CustBranchID=" + arg[0] + "&FinYear=" + arg[2] + "&Period=" + arg[1] + "&VID=" + arg[3] + "&AuditID=" + arg[4] + "&DrilDown=Yes" + "&CustomerId=" + custId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ShowImplementationGrid(object sender, EventArgs e)
        {
            liImplementation.Attributes.Add("class", "active");
            liProcess.Attributes.Add("class", "");
            ViewState["Type"] = "Implementation";

            BindData();
            int chknumber = Convert.ToInt32(grdProcessAudits.PageIndex);

            if (chknumber > 0)
            {
                chknumber = chknumber + 1;
                DropDownListPageNo.SelectedValue = (chknumber).ToString();
            }
        }

        protected void ShowProcessGrid(object sender, EventArgs e)
        {
            liProcess.Attributes.Add("class", "active");
            liImplementation.Attributes.Add("class", "");
            ViewState["Type"] = "Process";

            BindData();

            int chknumber = Convert.ToInt32(grdProcessAudits.PageIndex);

            if (chknumber > 0)
            {
                chknumber = chknumber + 1;
                DropDownListPageNo.SelectedValue = (chknumber).ToString();
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void grdProcessAudits_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkAuditDetails = (LinkButton)e.Row.FindControl("lnkAuditDetails");
                LinkButton lblFinalReview = (LinkButton)e.Row.FindControl("lblFinalReview");
                string Args = lnkAuditDetails.CommandArgument;
                int custbranchid = -1;
                int verticalid = -1;
                long AuditID = -1;

                if (!string.IsNullOrEmpty(Args))
                {
                    string[] arg = Args.ToString().Split(',');
                    custbranchid = Convert.ToInt32(arg[0]);
                    verticalid = Convert.ToInt32(arg[3]);
                    AuditID = Convert.ToInt32(arg[4]);

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var AuditSummaryMasterRecords = (from row in entities.SP_AuditStatusSummary(custbranchid, verticalid, AuditID)
                                                         select row).ToList();

                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.RoleID == 4 && row.AuditID == AuditID
                                                   select row).ToList();
                        //select row).ToList().GroupBy(entity => entity.ATBDID).ToList();

                        //var AuditCount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();
                        var AuditCount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();
                        if (AuditCount > 0)
                        {
                            lblFinalReview.Text = Convert.ToString(AuditCount);
                        }
                        else
                        {
                            lblFinalReview.Text = string.Empty;
                        }
                    }
                }
                //Label AuditId = (Label)e.Row.FindControl("AuditId");
                //Label lblProcess = (Label)e.Row.FindControl("lblProcess");
                //Label lblPerformer = (Label)e.Row.FindControl("lblPerformer");
                //long cbranchId = Convert.ToInt64(AuditId.Text);

                //int roleid = 3;
                //List<sp_PerformerReviewView_Result> Rowss = (from C in PerformerReviewViewList
                //                                             where C.AuditID == cbranchId
                //                                             select C).Distinct().ToList();

                //var ProcessName = (from C in Rowss
                //                   where C.Roleid == roleid
                //                   select C.ProcessName).Distinct().ToList();

                //string Process = "";
                //foreach (var item in ProcessName)
                //{
                //    Process += item + ",";
                //}
                //lblProcess.Text = Process.TrimEnd(',');
                //lblProcess.Attributes.Add("tooltip", lblProcess.Text);
            }
        }

        protected void grdImplementationAudits_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkAuditDetails = (LinkButton)e.Row.FindControl("lnkAuditDetails");
                LinkButton lblFinalReviewIMP = (LinkButton)e.Row.FindControl("lblFinalReviewIMP");
                string Args = lnkAuditDetails.CommandArgument;
                int UserID = Portal.Common.AuthenticationHelper.UserID;
                int custbranchid = -1;
                int verticalid = -1;
                long AuditID = -1;

                if (!string.IsNullOrEmpty(Args))
                {
                    string[] arg = Args.ToString().Split(',');
                    custbranchid = Convert.ToInt32(arg[0]);
                    verticalid = Convert.ToInt32(arg[3]);
                    AuditID = Convert.ToInt32(arg[4]);

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        var AuditSummaryMasterRecords = (from row in entities.ImplementationAuditSummaryCountViews
                                                         where row.AuditID == AuditID
                                                         //&& row.UserID== UserID
                                                         && row.RoleID == 4
                                                         select row).ToList();

                        var AuditCount = AuditSummaryMasterRecords.Where(entity => entity.AuditStatusID == 5).Count();
                        if (AuditCount > 0)
                        {
                            lblFinalReviewIMP.Text = Convert.ToString(AuditCount);
                        }
                        else
                        {
                            lblFinalReviewIMP.Text = string.Empty;
                        }
                    }
                }
            }
        }

        private void BindCustomerList()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDL(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomerMultiSelect.DataTextField = "CustomerName";
                ddlCustomerMultiSelect.DataValueField = "CustomerId";
                ddlCustomerMultiSelect.DataSource = customerList;
                ddlCustomerMultiSelect.DataBind();
                if (String.IsNullOrEmpty(Request.QueryString["CustomerId"]))
                {
                    for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                    {
                        ddlCustomerMultiSelect.Items[i].Selected = true;
                    }
                }
                else
                {
                    ddlCustomerMultiSelect.SelectedValue = Convert.ToString(Request.QueryString["CustomerId"]);
                }
            }
            else
            {
                ddlCustomerMultiSelect.DataSource = null;
                ddlCustomerMultiSelect.DataBind();
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    Session["CustIdFromQueryString"] = ddlCustomer.SelectedValue;
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                    //Session["CustIdFromQueryString"] = ddlCustomer.SelectedValue;
                }
            }
            else
            {
                Session["CustIdFromQueryString"] = 0;
            }
            BindLegalEntityData();
            BindData();
        }

        protected void ddlCustomerMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<int?> customerIdsList = new List<int?>();
            customerIdsList.Clear();
            for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
            {
                if (ddlCustomerMultiSelect.Items[i].Selected)
                {
                    customerIdsList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                }
            }
            if (customerIdsList.Count > 0)
            {
                Session["CustIdFromQueryString"] = string.Join(",", customerIdsList);
                BindLegalEntityData();
                BindData();
            }
            else
            {
                Session["CustIdFromQueryString"] = 0;
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
            }
        }

        protected void ddlLegalEntityMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> CHKBranchlist = new List<long>();

            for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
            {
                if (ddlLegalEntityMultiSelect.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                }
            }

            if (CHKBranchlist.Count > 0)
            {
                BindSubEntityData(ddlSubEntity1MultiSelect, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity1MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity1MultiSelect.Items.Clear();
                }
                if (ddlSubEntity2MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity2MultiSelect.Items.Clear();
                }
                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();

                }
            }
            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity1MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> parentIdsList = new List<long>();
            parentIdsList.Clear();
            for (int i = 0; i < ddlSubEntity1MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity1MultiSelect.Items[i].Selected)
                {
                    parentIdsList.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[i].Value));
                }
            }
            if (parentIdsList.Count > 0)
            {
                BindSubEntityData(ddlSubEntity2MultiSelect, parentIdsList);
            }
            else
            {
                if (ddlSubEntity2MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity2MultiSelect.Items.Clear();
                }
                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity2MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> parentIdsList = new List<long>();
            parentIdsList.Clear();
            for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity2MultiSelect.Items[i].Selected)
                {
                    parentIdsList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                }
            }
            if (parentIdsList.Count > 0)
            {
                BindSubEntityData(ddlSubEntity3MultiSelect, parentIdsList);
            }
            else
            {
                if (ddlSubEntity3MultiSelect.Items.Count > 0)
                {
                    ddlSubEntity3MultiSelect.Items.Clear();
                }
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSubEntity3MultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<long> parentIdsList = new List<long>();
            parentIdsList.Clear();
            for (int i = 0; i < ddlSubEntity2MultiSelect.Items.Count; i++)
            {
                if (ddlSubEntity2MultiSelect.Items[i].Selected)
                {
                    parentIdsList.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[i].Value));
                }
            }
            if (parentIdsList.Count > 0)
            {
                BindSubEntityData(ddlFilterLocationMultiSelect, parentIdsList);
            }
            else
            {
                if (ddlFilterLocationMultiSelect.Items.Count > 0)
                {
                    ddlFilterLocationMultiSelect.Items.Clear();
                }
            }
            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlFilterLocationMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSchedulingType();
            BindVertical();
            BindData();
        }

        protected void ddlSchedulingTypeMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> FlagList = new List<string>();
            List<int> CHKBranchlist = new List<int>();
            for (int i = 0; i < ddlSchedulingTypeMultiSelect.Items.Count; i++)
            {
                if (ddlSchedulingTypeMultiSelect.Items[i].Selected)
                {
                    if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Annually")
                    {
                        FlagList.Add("A");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Half Yearly")
                    {
                        FlagList.Add("H");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Quarterly")
                    {
                        FlagList.Add("Q");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Monthly")
                    {
                        FlagList.Add("M");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Special Audit")
                    {
                        FlagList.Add("S");
                    }
                    else if (ddlSchedulingTypeMultiSelect.Items[i].Text == "Phase")
                    {
                        bool clearLegalBranchList = false;
                        bool clearSubEntity1List = false;
                        bool clearSubEntity2List = false;
                        bool clearSubEntity3List = false;
                        bool clearFilterLocation = false;

                        CHKBranchlist.Clear();
                        for (int j = 0; j < ddlLegalEntityMultiSelect.Items.Count; j++)
                        {
                            if (ddlLegalEntityMultiSelect.Items[j].Selected)
                            {
                                clearLegalBranchList = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[j].Value));
                            }
                        }

                        for (int k = 0; k < ddlSubEntity1MultiSelect.Items.Count; k++)
                        {
                            if (ddlSubEntity1MultiSelect.Items[k].Selected)
                            {
                                if (clearLegalBranchList && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearLegalBranchList = false;
                                }
                                clearSubEntity1List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1MultiSelect.Items[k].Value));
                            }

                        }

                        for (int l = 0; l < ddlSubEntity2MultiSelect.Items.Count; l++)
                        {
                            if (ddlSubEntity2MultiSelect.Items[l].Selected)
                            {
                                if (clearSubEntity1List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity1List = false;
                                }
                                clearSubEntity2List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2MultiSelect.Items[l].Value));
                            }
                        }

                        for (int m = 0; m < ddlSubEntity3.Items.Count; m++)
                        {
                            if (ddlSubEntity3MultiSelect.Items[m].Selected)
                            {
                                if (clearSubEntity2List && CHKBranchlist.Count > 0)
                                {
                                    CHKBranchlist.Clear();
                                    clearSubEntity2List = false;
                                }
                                clearSubEntity3List = true;
                                CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3MultiSelect.Items[m].Value));
                            }
                        }
                    }
                    List<int?> recordCount = new List<int?>(); ;
                    recordCount = UserManagementRisk.GetPhaseCountForAuditHeadDashboard(CHKBranchlist);
                    if (recordCount.Count > 0)
                    {
                        FlagList.Add("P");
                    }
                    BindAuditSchedule(FlagList, recordCount);
                }
            }
        }


        protected void ddlPeriodMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlFinancialYearMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}