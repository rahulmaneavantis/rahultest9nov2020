﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class TestingUI : System.Web.UI.Page
    {
        protected List<Int32> roles;
        protected string FinYear;
        protected string Period;
        protected int branchid;
        protected static int statusid;
        protected String status;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        protected void Page_Load(object sender, EventArgs e)
        {
            roles = CustomerManagementRisk.GetAssignedRolesICFR(Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                BindFinancialYear();
                if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    var AuditScheduleDetails = InternalControlManagementDashboardRisk.GetInternalAuditScheduleOnDetails(Convert.ToInt32(Request.QueryString["SID"]));
                    if (AuditScheduleDetails != null)
                    {
                        FinYear = AuditScheduleDetails.FinancialYear;

                        if (FinYear != "")
                        {
                            ddlFilterFinancial.ClearSelection();
                            ddlFilterFinancial.Items.FindByText(FinYear).Selected = true;
                        }
                        Period = AuditScheduleDetails.ForMonth;
                        if (Period != "")
                        {
                            ddlQuarter.ClearSelection();
                            ddlQuarter.Items.FindByText(Period).Selected = true;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    status = Convert.ToString(Request.QueryString["Status"]);

                    if (status != "")
                    {
                        ddlStatus.ClearSelection();

                        if (status != "ReviewComment")
                            ddlStatus.Items.FindByText(status.ToString()).Selected = true;
                        else
                        {
                            status = "Review Comment";
                            ddlStatus.Items.FindByText(status.ToString()).Selected = true;
                        }
                    }
                }
                if (!String.IsNullOrEmpty(Request.QueryString["returnUrl2"]))
                {
                    ViewState["returnUrl2"] = Request.QueryString["returnUrl2"];
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Period"]))
                {
                    ViewState["Period"] = Request.QueryString["Period"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["RoleID"]))
                {
                    if (Request.QueryString["RoleID"] == "3")
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (Request.QueryString["RoleID"] == "4")
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
                //Page Title
                int customerID = -1;
                string PName = string.Empty;
                string SPname = string.Empty;
                //customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    customerID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                }
                var Period1 = string.Empty;
                var FY = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["FinancialYear"])))
                {
                    FY = "/" + Convert.ToString(Request.QueryString["FinancialYear"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["Period"])))
                {
                    Period1 = "/" + Convert.ToString(Request.QueryString["Period"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["PID"])))
                {
                    if (Convert.ToString(Request.QueryString["PID"]) != "-1")
                    {
                        PName = "/" + ProcessManagement.GetProcessName(Convert.ToInt64(Request.QueryString["PID"]), customerID);
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["SPID"])))
                {
                    if (Convert.ToString(Request.QueryString["SPID"]) != "-1")
                    {
                        SPname = "/" + ProcessManagement.GetSubProcessName(Convert.ToInt32(Request.QueryString["SPID"]));
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["CustBranchID"])))
                {
                    int CBranchID = Convert.ToInt32(Request.QueryString["CustBranchID"]);
                    var BrachName = CustomerBranchManagement.GetByID(CBranchID).Name;
                    LblPageDetails.InnerText = BrachName + FY + Period1 + PName + SPname;// + "/" + VerticalName;
                }
                //End                                                        
            }
        }
        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
            BindAuditTransactions();
        }
        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
            BindAuditTransactions();
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdComplianceTransactions.PageIndex = chkSelectedPage - 1;
            grdComplianceTransactions.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindAuditTransactions();
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {

            string url3 = "";
            string url4 = "";
            int PID = -1;
            int SPID = -1;
            int PageSize = -1;
            int gridpagesize = -1;
            int BoPSize = 0;
            string period = "";
            string CustomerID = string.Empty;

            if (!String.IsNullOrEmpty(Request.QueryString["returnUrl2"]))
            {
                url3 = Request.QueryString["returnUrl2"];

                if (!String.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    PID = Convert.ToInt32(Request.QueryString["PID"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["SPID"]))
                {
                    SPID = Convert.ToInt32(Request.QueryString["SPID"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["PageSize"]))
                {
                    PageSize = Convert.ToInt32(Request.QueryString["PageSize"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["gridpagesize"]))
                {
                    gridpagesize = Convert.ToInt32(Request.QueryString["gridpagesize"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["BoPSize"]))
                {
                    BoPSize = Convert.ToInt32(Request.QueryString["BoPSize"]);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Period"]))
                {
                    period = Request.QueryString["Period"];
                }
                if (!String.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    CustomerID = Request.QueryString["CustomerID"];
                }

                if (!String.IsNullOrEmpty(Request.QueryString["returnUrl1"]))
                {
                    url4 = "&returnUrl1=" + Request.QueryString["returnUrl1"] + "&PID=" + PID + "&SPID=" + SPID + "&PageSize=" + PageSize + "&gridpagesize=" + gridpagesize + "&BoPSize=" + BoPSize + "&Period=" + period + "&CustomerID=" + CustomerID;
                }

                Response.Redirect("~/RiskManagement/InternalAuditTool/InternalAuditStatusSummary.aspx?" + url3.Replace("@", "=") + url4);
            }
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {

        }
        protected void ddlQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
        }
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
        }
        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditTransactions();
        }
        public void BindFinancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.Items.Clear();
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
        }
        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int? ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int RiskCreationId = Convert.ToInt32(commandArgs[1]);
                string FinancialYear = Convert.ToString(commandArgs[2]);
                int CustomerBranchID = Convert.ToInt32(commandArgs[3]);
                string Period = Convert.ToString(commandArgs[4]);
                int RoleID = Convert.ToInt32(commandArgs[5]);
                string CustomerID = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    CustomerID = Request.QueryString["CustomerID"];
                }
                if (RoleID == 3)
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ShowDialog('" + ScheduledOnID + "','" + RiskCreationId + "','" + FinancialYear + "','" + CustomerBranchID + "','" + Period + "','" + CustomerID + "');", true);
                else if (RoleID == 4)
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ShowReviewerDialog('" + ScheduledOnID + "','" + RiskCreationId + "','" + FinancialYear + "','" + CustomerBranchID + "','" + Period + "','" + CustomerID + "');", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                BindAuditTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected string GetReviewer(int AuditStatusID, long? scheduledonid, long RiskCreationId)
        {
            try
            {
                string result = "";
                result = DashboardManagementRisk.GetUserName(AuditStatusID, scheduledonid, RiskCreationId, 4);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }
        }
        public string ShowFrequencyName(long? id)
        {
            List<FrequencyName_Result> a = new List<FrequencyName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetFrequencyNameProcedure(Convert.ToInt32(id)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        protected void BindAuditTransactions()
        {
            try
            {
                int customerID = -1;
                //customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                List<AuditInstanceTransactionView> assignmentList = null;
                int roleid = -1;
                long pid = -1;
                long psid = -1;
                string finacialyear = "";
                string Period = "";
                string KeyNonKey = "";
                long AuditStatus = -1;
                string Flag = "A";
                long CustomerBranchId = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    customerID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    pid = Convert.ToInt32(Request.QueryString["PID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SPID"]))
                {
                    psid = Convert.ToInt32(Request.QueryString["SPID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["CustBranchID"]))
                {
                    CustomerBranchId = Convert.ToInt32(Request.QueryString["CustBranchID"]);
                }
                if (!string.IsNullOrEmpty(ddlQuarter.SelectedValue))
                {
                    if (ddlQuarter.SelectedValue != "-1")
                    {
                        Period = Convert.ToString(ddlQuarter.SelectedItem.Text);
                        Flag = "PE";
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                {
                    if (ddlFilterFinancial.SelectedItem.Text != "All")
                    {
                        finacialyear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                        Flag = "F";
                    }
                }
                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    if (ddlStatus.SelectedValue != "-1")
                    {
                        AuditStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                        Flag = "S";
                    }
                }
                if (PerformerFlag)
                    roleid = 3;

                else if (ReviewerFlag)
                    roleid = 4;

                assignmentList = DashboardManagementRisk.DashboardData(Portal.Common.AuthenticationHelper.UserID, roleid, "All", CustomerBranchId, pid, psid, finacialyear, KeyNonKey, Period, AuditStatus, customerID);
                grdComplianceTransactions.DataSource = assignmentList;
                Session["TotalRows"] = assignmentList.Count;
                grdComplianceTransactions.DataBind();

                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdComplianceTransactions.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == Portal.Common.AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6 || statusID == 10 || statusID == 4;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindAuditTransactions();
            }
            catch (Exception ex)
            {
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        string statusidasd = Request.QueryString["Status"].ToString();
                        if (statusidasd == "TeamReview")
                        {
                            Label lblRiskCreationId = (Label)e.Row.FindControl("lblRiskCreationId");
                            Label lblScheduledOnID = (Label)e.Row.FindControl("lblScheduledOnID");
                            Label lblCustomerBranchID = (Label)e.Row.FindControl("lblCustomerBranchID");
                            Label lblcolor = (Label)e.Row.FindControl("lblActivityDescription");
                            Label lblCreatedBy = (Label)e.Row.FindControl("lblCreatedBy");

                            int bid = Convert.ToInt32(lblCustomerBranchID.Text);
                            int rkid = Convert.ToInt32(lblRiskCreationId.Text);
                            int sid = Convert.ToInt32(lblScheduledOnID.Text);

                            var MasterInternalControlAuditAssignment = (from row in entities.AuditAssignments
                                                                        where row.CustomerBranchID == bid
                                                                        && row.RiskCreationId == rkid
                                                                        select row).OrderByDescending(entry => entry.CreatedOn).ToList();
                            if (MasterInternalControlAuditAssignment != null)
                            {
                                var User = (from row in entities.RecentAuditTransactionViews
                                            where row.AuditScheduleOnID == sid
                                            && row.CustomerBranchId == bid
                                            && row.RiskCreationID == rkid
                                            select row).OrderByDescending(el => el.Dated).FirstOrDefault();

                                var ListPerformer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 3 select row.UserID).FirstOrDefault();
                                var ListReviewer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 4 select row.UserID).FirstOrDefault();
                                if (User != null)
                                {
                                    if (ListPerformer == User.CreatedBy)
                                    {
                                        lblcolor.Style.Add("color", "black");
                                    }
                                    else if (ListReviewer == User.CreatedBy)
                                    {
                                        lblcolor.Style.Add("color", "red");
                                    }
                                }
                            }
                        }
                        else if (statusidasd == "AuditeeReview")
                        {
                            Label lblRiskCreationId = (Label)e.Row.FindControl("lblRiskCreationId");
                            Label lblScheduledOnID = (Label)e.Row.FindControl("lblScheduledOnID");
                            Label lblCustomerBranchID = (Label)e.Row.FindControl("lblCustomerBranchID");
                            Label lblcolor = (Label)e.Row.FindControl("lblActivityDescription");
                            Label lblCreatedBy = (Label)e.Row.FindControl("lblCreatedBy");

                            int bid = Convert.ToInt32(lblCustomerBranchID.Text);
                            int rkid = Convert.ToInt32(lblRiskCreationId.Text);
                            int sid = Convert.ToInt32(lblScheduledOnID.Text);

                            var MasterInternalControlAuditAssignment = (from row in entities.AuditAssignments
                                                                        where row.CustomerBranchID == bid
                                                                        && row.RiskCreationId == rkid
                                                                        //  && row.UserID == Portal.Common.AuthenticationHelper.UserID
                                                                        select row).ToList();

                            var User = (from row in entities.RecentAuditTransactionViews
                                        where row.AuditScheduleOnID == sid
                                            && row.CustomerBranchId == bid
                                            && row.RiskCreationID == rkid
                                        select row).OrderByDescending(el => el.Dated).FirstOrDefault();

                            var ListReviewer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 4 select row.UserID).FirstOrDefault();
                            if (User != null)
                            {
                                if (ListReviewer == User.CreatedBy)
                                {
                                    lblcolor.Style.Add("color", "blue");
                                }
                                else
                                {
                                    lblcolor.Style.Add("color", "black");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}