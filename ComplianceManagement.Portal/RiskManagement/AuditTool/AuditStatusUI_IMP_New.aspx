﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditStatusUI_IMP_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.AuditStatusUI_IMP_New" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style>
        span#ContentPlaceHolder1_rdRiskActivityProcess > Label {
            margin-left: 5px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }

        input#ContentPlaceHolder1_rdRiskActivityProcess_1 {
            margin-left: 12px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

         .ddlMultiSelectCustomer {
            height: 32px !important;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            var filterbytype = ReadQuerySt('Status');
            if (filterbytype == '') {
                fhead('My Workspace');
            } else {
                //$('#pagetype').css("font-size", "20px")
                if (filterbytype == 'Open') {
                    filterbytype = 'Open Audits';
                } else if (filterbytype == 'Closed') {
                    filterbytype = 'Closed Audits';
                }
                fhead('My Workspace/ Implementation / ' + filterbytype);
                $('#pagetype').css('font-size', '21px');
            }
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            // JQUERY DATE PICKER.
            
            $(function () {
                $('input[id*=txtStartDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                });
            });

            $(function () {
                $('input[id*=txtEndDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                });
            });
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                              
                             <div class="clearfix"></div> 
                             <div class="panel-body">

                                 <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                           <%if (roles.Contains(3))%>
                                           <%{%>
                                        <li class="active" id="liPerformer" runat="server">
                                            <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" runat="server">Performer</asp:LinkButton>                                           
                                        </li>
                                           <%}%>
                                            <%if (roles.Contains(4) || roles.Contains(5))%>
                                           <%{%>
                                        <li class=""  id="liReviewer" runat="server">
                                            <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer" runat="server">Reviewer</asp:LinkButton>                                        
                                        </li>
                                          <%}%>
                                    </ul>
                                </header>   
                                 <div class="clearfix"></div> 
                                 <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="ComplianceInstanceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>

                                 <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5"/>
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" Selected="True" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                    </div>  
                                    <div class="col-md-3 colpadding0" style="color: #999; padding-top: 9px;width: 24%; display:none;" >
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" ForeColor="Black"
                                        RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged">
                                        <asp:ListItem Text="Process"  Value="Process" Selected="True" />
                                        <asp:ListItem Text="Non Process" Value="Non Process" />
                                    </asp:RadioButtonList>
                                </div>                         
                                 </div>   
                                                           
                                <div class="clearfix"></div>

                                 <div class="col-md-12 colpadding0">
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;display:none;"> 
                                         <asp:DropDownListChosen ID="ddlCustomer" runat="server" DataPlaceHolder="Customer" Width="90%"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15 select_location"  AutoPostBack="true"
                                          OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"></asp:DropDownListChosen>
                                     </div>
                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%;">
                                   <asp:DropDownCheckBoxes ID="ddlCustomerMultiSelect" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlCustomerMultiSelect_SelectedIndexChanged"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 95%; height: 50px;">
                                                <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer" DropDownBoxCssClass="MarginTop"/>
                                                <Texts SelectBoxCaption="Select Customer" />
                                            </asp:DropDownCheckBoxes>
                                   </div>
                               <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;display:none;">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Entity"  class="form-control m-bot15" Width="90%" Height="32px"
                                         AllowSingleDeselect="false" DisableSearchThreshold="3"    AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                </div>
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%;">
                                   <asp:DropDownCheckBoxes ID="ddlLegalEntityMultiSelect" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlLegalEntityMultiSelect_SelectedIndexChanged"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 95%; height: 50px;">
                                                <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer"  DropDownBoxCssClass="MarginTop"/>
                                                <Texts SelectBoxCaption="Select Unit" />
                                            </asp:DropDownCheckBoxes>
                                   </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;display:none">
                                    
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Entity" class="form-control m-bot15" Width="90%" Height="32px"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"   AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div>
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                          <asp:DropDownCheckBoxes ID="ddlSubEntity1MultiSelect" runat="server" AutoPostBack="true" Visible="true" 
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlSubEntity1MultiSelect_SelectedIndexChanged"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                             Style="padding: 0px; margin: 0px; width: 80%; height: 50px;">
                                            <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer"  DropDownBoxCssClass="MarginTop"/>
                                            <Texts SelectBoxCaption="Select Sub Unit" />
                                        </asp:DropDownCheckBoxes>
                                                                 
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;display:none">                                   
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Entity" class="form-control m-bot15" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"     AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                      
                                </div>                          
                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                          <asp:DropDownCheckBoxes ID="ddlSubEntity2MultiSelect" runat="server" AutoPostBack="true" Visible="true" 
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlSubEntity2MultiSelect_SelectedIndexChanged"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                             Style="padding: 0px; margin: 0px; width: 80%; height: 50px;">
                                            <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer"  DropDownBoxCssClass="MarginTop"/>
                                            <Texts SelectBoxCaption="Select Sub Unit" />
                                        </asp:DropDownCheckBoxes>
                                                                 
                                </div>                       
                                     </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12 colpadding0">    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;display:none;">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px" DataPlaceHolder="Sub Entity"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div>        
                                    
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                          <asp:DropDownCheckBoxes ID="ddlSubEntity3MultiSelect" runat="server" AutoPostBack="true" Visible="true" 
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlSubEntity3MultiSelect_SelectedIndexChanged"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                             Style="padding: 0px; margin: 0px; width: 80%; height: 50px;">
                                            <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer"  DropDownBoxCssClass="MarginTop"/>
                                            <Texts SelectBoxCaption="Select Sub Unit" />
                                        </asp:DropDownCheckBoxes>
                                                                 
                                </div>                                
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;display:none;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Sub Entity"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%;">
                                          <asp:DropDownCheckBoxes ID="ddlFilterLocationMultiSelect" runat="server" AutoPostBack="true" Visible="true" 
                                            CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlFilterLocationMultiSelect_SelectedIndexChanged"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                             Style="padding: 0px; margin: 0px; width: 80%; height: 50px;">
                                            <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer"  DropDownBoxCssClass="MarginTop"/>
                                            <Texts SelectBoxCaption="Select Sub Unit" />
                                        </asp:DropDownCheckBoxes>
                                                                 
                                </div>
                                     <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                     <%{%>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                       <asp:DropDownListChosen runat="server" ID="ddlVertical" AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" DataPlaceHolder="Vertical" class="form-control m-bot15" Width="90%" Height="32px" OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                                        
                                    </div>   
                                    <%}%>              
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;display:none">                     
                                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true" DataPlaceHolder="Financial Year" class="form-control m-bot15" Width="90%" Height="32px"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:CompareValidator ErrorMessage="Please Select Financial Year." ControlToValidate="ddlFinancialYear"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" Display="None" />                    
                                    </div> 
                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%;">
                                           <asp:DropDownCheckBoxes ID="ddlFinancialYearMultiSelect" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"  OnSelectedIndexChanged="ddlFinancialYearMultiSelect_SelectedIndexChanged"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer" DropDownBoxCssClass="MarginTop"/>
                                                <Texts SelectBoxCaption="Select Financial Year" />
                                            </asp:DropDownCheckBoxes>
                                   </div>
                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;display:none;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true" DataPlaceHolder="Schedule"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%;">
                                           <asp:DropDownCheckBoxes ID="ddlSchedulingTypeMultiSelect" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15" OnSelectedIndexChanged="ddlSchedulingTypeMultiSelect_SelectedIndexChanged"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer"  DropDownBoxCssClass="MarginTop"/>
                                                <Texts SelectBoxCaption="Select Scheduling Type" />
                                            </asp:DropDownCheckBoxes>
                                   </div>
                                    </div> 
                                 <div class="clearfix"></div> 
                                 <div class="col-md-12 colpadding0"> 
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;display:none;">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true" DataPlaceHolder="Period"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" class="form-control m-bot15" Width="90%" Height="32px">
                                        </asp:DropDownListChosen>
                                    </div>     
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%;">
                                           <asp:DropDownCheckBoxes ID="ddlPeriodMultiSelect" runat="server" AutoPostBack="true" Visible="true"
                                                CssClass="form-control m-bot15"  OnSelectedIndexChanged="ddlPeriodMultiSelect_SelectedIndexChanged"
                                                AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                                Style="padding: 0px; margin: 0px; width: 90%; height: 50px;">
                                                <Style SelectBoxWidth="200" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" SelectBoxCssClass="ddlMultiSelectCustomer"  DropDownBoxCssClass="MarginTop"/>
                                                <Texts SelectBoxCaption="Select Period" />
                                            </asp:DropDownCheckBoxes>
                                   </div>               
                                 </div>
                                                                     
                                <div class="clearfix"></div>  
                                                                 
                                <div style="margin-bottom: 4px">
                                    &nbsp;       
                                    <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false" 
                                    OnSorting="grdRiskActivityMatrix_Sorting" OnRowDataBound="grdRiskActivityMatrix_RowDataBound" 
                                    PageSize="20" AllowPaging="true" AutoPostBack="true" ShowHeaderWhenEmpty="true"
                                    CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                    OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging" ShowFooter="true" OnRowCommand="grdRiskActivityMatrix_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Audit Name">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; width: 200px;">
                                                <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("AuditName") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AuditName") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="Total Observation" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskTotal" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Total;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ null %>'
                                                    Text='<%# Eval("Total") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Open Observations" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskNotDone" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "NotDone;"+ Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ null %>'
                                                    Text='<%# Eval("NotDone") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Submitted" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblAuditeeReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "AuditeeSubmit;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+"AS"  %>'
                                                    Text='<%# Eval("AuditeeSubmit") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Review Comment" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblAuditeeSubmit" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "AuditeeReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+"PA" %>'
                                                    Text='<%# Eval("AuditeeReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Re-submitted" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblAuditeeReviewComment" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "ReviewComment;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+"AR" %>'
                                                    Text='<%# Eval("ReviewComment") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Review 2" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskCompleted" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Submitted;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ "PS" %>'
                                                    Text='<%# Eval("Submited") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                           
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Team Review" ItemStyle-HorizontalAlign="Center" Visible="false" HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskTeamReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "TeamReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ null %>'
                                                    Text='<%# Eval("TeamReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Review 3" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblFinalReview" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "FinalReview;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ "RS" %>'
                                                    Text='<%# Eval("FinalReview") %>' CausesValidation="false"></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Closed" ItemStyle-HorizontalAlign="Center"  HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblRiskClosed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Closed;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("CustomerBranchID")+";"+Eval("FinancialYear")+";"+Eval("ForMonth")+";"+Eval("RoleID")+";"+Eval("VerticalID")+";"+Eval("AuditID")+";"+ "AC" %>'
                                                    Text='<%# Eval("Closed") %>' ></asp:LinkButton>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" /> 
                                     <HeaderStyle BackColor="#ECF0F1" /> 
                                        <PagerSettings Visible="false" />                     
                                    <PagerTemplate>                                        
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                          No Records Found.
                                     </EmptyDataTemplate> 
                                </asp:GridView>
                                                 <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                                </div>

                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">                                    
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page
                                          
                                        </p>
                                    </div>                                    
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>           
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
