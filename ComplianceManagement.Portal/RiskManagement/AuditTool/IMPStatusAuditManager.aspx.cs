﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class IMPStatusAuditManager : System.Web.UI.Page
    {

        protected string FinYear;
        protected string Period;
        protected int BranchId;
        protected int ResultID;
        protected int StatusId;
        protected int VerticalID;
        protected int ScheduledOnId;
        protected static string IsAfterPerformer;
        protected int AuditID;
        protected static int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomer();
                BindUsersIMP();
                BindUsersNEWIMP();
                BindObservationCategoryIMP();
                BindFinancialYear();

                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Request.QueryString["FinYear"];
                    ViewState["FinYear"] = Request.QueryString["FinYear"];
                    if (FinYear != "")
                    {
                        ddlFinancialYear.ClearSelection();
                        ddlFinancialYear.Items.FindByText(FinYear).Selected = true;
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchId = Convert.ToInt32(Request.QueryString["BID"]);
                    ViewState["BID"] = Request.QueryString["BID"];
                    if (BranchId != 0)
                    {
                        ddlFilterLocation.ClearSelection();
                        ddlFilterLocation.Items.FindByValue(BranchId.ToString()).Selected = true;
                        BindSchedulingType(BranchId);
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    ViewState["VID"] = Request.QueryString["VID"];
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    ViewState["AuditID"] = Request.QueryString["AuditID"];
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                    ViewState["ForMonth"] = Request.QueryString["ForMonth"];
                    String SType = GetSchedulingType(BranchId, FinYear, Period, AuditID);

                    if (SType != "")
                    {
                        ddlSchedulingType.ClearSelection();

                        if (SType == "A")
                            ddlSchedulingType.Items.FindByText("Annually").Selected = true;
                        else if (SType == "H")
                            ddlSchedulingType.Items.FindByText("Half Yearly").Selected = true;
                        else if (SType == "Q")
                            ddlSchedulingType.Items.FindByText("Quarterly").Selected = true;
                        else if (SType == "M")
                            ddlSchedulingType.Items.FindByText("Monthly").Selected = true;
                        else if (SType == "P")
                            ddlSchedulingType.Items.FindByText("Phase").Selected = true;
                        else if (SType == "S")
                            ddlSchedulingType.Items.FindByText("Special Audit").Selected = true;
                        ddlSchedulingType_SelectedIndexChanged(null, null);

                        ddlPeriod.ClearSelection();
                        ddlPeriod.Items.FindByText(Period).Selected = true;
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
                {
                    ViewState["ResultID"] = Request.QueryString["ResultID"];
                    ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ViewState["scheduledonid"] = Request.QueryString["scheduledonid"];
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                {
                    ViewState["SID"] = Request.QueryString["SID"];
                    StatusId = Convert.ToInt32(Request.QueryString["SID"]);

                    if (StatusId != 0)
                    {
                        ddlFilterStatus.ClearSelection();
                        ddlFilterStatus.Items.FindByValue(StatusId.ToString()).Selected = true;
                    }
                }
                tvImplementation_SelectedNodeChanged(sender, e);
                btnFinalStatus_Click(sender, e);
            }
            DateTime date = DateTime.MinValue;
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker2", string.Format("initializeConfirmDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
        }
        public void BindCustomer()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(CustomerId);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem("< Select Location >", "-1"));
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("< Select Financial Year >", "-1"));
        }
        public static String GetSchedulingType(int CustBranchID, String FinYear, String Period, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Record = (from row in entities.InternalAuditSchedulings
                              where row.CustomerBranchId == CustBranchID
                              && row.FinancialYear == FinYear
                              && row.TermName == Period
                              && row.AuditID == AuditID
                              select row.ISAHQMP).FirstOrDefault();

                if (Record != "")
                    return Record;
                else
                    return "";
            }
        }

        public void BindSchedulingType(int BranchId)
        {
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(BranchId);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("< Select Scheduling Type >", "-1"));
        }
        #region Implementation Status

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            {
                BindAuditSchedule("S", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                        {
                            AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                            int count = 0;
                            count = UserManagementRisk.GetPhaseCountNew(Convert.ToInt32(ddlFilterLocation.SelectedValue), AuditID);
                            BindAuditSchedule("P", count);
                        }
                    }
                }
            }
        }

        protected void ddlObservationCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(ddlIMPObservationCategory.SelectedValue))
            //{
            //    if (ddlIMPObservationCategory.SelectedValue != "-1")
            //    {
            //        BindObservationSubCategory(Convert.ToInt32(ddlIMPObservationCategory.SelectedValue));
            //    }
            //}
        }

        public void BindObservationSubCategory(int ObservationId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            //ddlIMPObservationSubCategory.DataTextField = "Name";
            //ddlIMPObservationSubCategory.DataValueField = "ID";
            //ddlIMPObservationSubCategory.Items.Clear();
            //ddlIMPObservationSubCategory.DataSource = ObservationSubcategory.FillObservationSubCategory(ObservationId, CustomerId);
            //ddlIMPObservationSubCategory.DataBind();
            //ddlIMPObservationSubCategory.Items.Insert(0, new ListItem("Observation Sub-Category", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void BindUsersIMP()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            //ddlIMPPersonresponsible.Items.Clear();
            //ddlIMPPersonresponsible.DataTextField = "Name";
            //ddlIMPPersonresponsible.DataValueField = "ID";
            //ddlIMPPersonresponsible.DataSource = RiskCategoryManagement.FillUsers(CustomerId);
            //ddlIMPPersonresponsible.DataBind();
            //ddlIMPPersonresponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
        }

        public void BindUsersNEWIMP()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlIMPNewPersonresponsible.Items.Clear();
            ddlIMPNewPersonresponsible.DataTextField = "Name";
            ddlIMPNewPersonresponsible.DataValueField = "ID";
            ddlIMPNewPersonresponsible.DataSource = RiskCategoryManagement.FillUsers(CustomerId);
            ddlIMPNewPersonresponsible.DataBind();
            ddlIMPNewPersonresponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
        }

        public void BindObservationCategoryIMP()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            //ddlIMPObservationCategory.DataTextField = "Name";
            //ddlIMPObservationCategory.DataValueField = "ID";
            //ddlIMPObservationCategory.Items.Clear();
            //ddlIMPObservationCategory.DataSource = ObservationSubcategory.FillObservationCategory(CustomerId);
            //ddlIMPObservationCategory.DataBind();
            //ddlIMPObservationCategory.Items.Insert(0, new ListItem("Select Observation Category", "-1"));
        }
        public static List<ImplementationReviewHistory> GetFileDataIMP(int id, int ImplementationInstance, int ImplementationScheduleOnID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fileData = (from row in entities.ImplementationReviewHistories
                                where row.ID == id && row.ImplementationInstance == ImplementationInstance
                                && row.ImplementationScheduleOnID == ImplementationScheduleOnID
                                && row.AuditID == AuditID
                                select row).ToList();

                return fileData;
            }
        }
        protected void DownLoadClickIMP(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label lblID = (Label)gvr.FindControl("lblID");
                Label lblriskID = (Label)gvr.FindControl("lblriskID");
                Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }

                using (ZipFile ComplianceZip = new ZipFile())
                {
                    List<ImplementationReviewHistory> fileData = GetFileDataIMP(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblriskID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text), AuditID);
                    int i = 0;
                    string directoryName = "abc";
                    string version = "1";
                    foreach (var file in fileData)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];                            
                            if (file.EnType == "M")
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            i++;
                        }
                    }
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = zipMs.Length;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=TestingDocument.zip");
                    Response.BinaryWrite(data);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteFileIMP(int fileId)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                var file = Business.RiskCategoryManagement.GetFileImplementationFileData_Risk(fileId, AuditID);
                if (file != null)
                {
                    string path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    DashboardManagementRisk.DeleteFileIMP(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DownloadFileIMP(int fileId, long AuditID)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileImplementationFileData_Risk(fileId, AuditID);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);                        
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        Response.End();

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void BindDocumentIMP(int ScheduledOnID, int resultID, long AuditID)
        {
            try
            {
                List<GetImplementationAuditDocumentsView> ComplianceDocument = new List<GetImplementationAuditDocumentsView>();
                ComplianceDocument = DashboardManagementRisk.GetFileDataGetImplementationAuditDocumentsView(ScheduledOnID, resultID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptIMPComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptIMPComplianceDocumnets.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStatusListIMP(int statusID)
        {
            try
            {
                rdbtnStatusNEW.Items.Clear();
                var statusList = AuditStatusManagement.GetInternalStatusList();
                List<InternalAuditStatu> allowedStatusList = null;
                List<InternalAuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetInternalAuditStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();
                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();
                if (statusID == 7)
                {
                    if (ddlFilterStatus.SelectedValue == "6")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {

                            if ((st.ID == 4) || (st.ID == 6))
                            {
                            }
                            else
                            {
                                rdbtnStatusNEW.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                            }
                        }
                    }
                    else if (ddlFilterStatus.SelectedValue == "5")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((st.ID == 4) || (st.ID == 6))
                            {
                            }
                            else
                            {
                                rdbtnStatusNEW.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                            }

                        }
                    }
                    else if (ddlFilterStatus.SelectedValue == "4")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (!(st.ID == 5))
                            {
                                rdbtnStatusNEW.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                            }
                        }
                    }
                    else
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (!(st.ID == 5))
                            {
                                rdbtnStatusNEW.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                            }
                        }
                    }
                }
                else
                {
                    if (ddlFilterStatus.SelectedValue == "5")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (!(st.ID == 4))
                            {
                                rdbtnStatusNEW.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                            }
                        }
                    }
                    else
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (!(st.ID == 7))
                            {
                                rdbtnStatusNEW.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                            }
                        }
                    }
                }
                lblStatusIMP.Visible = allowedStatusList.Count > 0 ? true : false;
                divDatedIMP.Visible = allowedStatusList.Count > 0 ? true : false;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactionsIMP(int ScheduledOnID, int ResultID, int AuditID)
        {
            try
            {
                grdTransactionIMPHistory.DataSource = Business.DashboardManagementRisk.GetAllImplementationAuditTransactionViews(ScheduledOnID, ResultID, AuditID);
                grdTransactionIMPHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindRemarksIMP(string ForPeriod, string FinancialYear, int ScheduledOnID, int ResultID, long AuditID)
        {
            try
            {
                GrdIMPRemark.DataSource = Business.DashboardManagementRisk.GetImplementationReviewAllRemarks(ForPeriod, FinancialYear, ScheduledOnID, ResultID, AuditID);
                GrdIMPRemark.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlIMPFinalStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlIMPFinalStatus.SelectedValue))
            {
                if (ddlIMPFinalStatus.SelectedValue != "-1")
                {
                    if (ddlIMPFinalStatus.SelectedValue == "5")
                    {
                        personresponsile.Visible = false;
                        ManagementResponce.Visible = false;
                        //TimeLine.Visible = false;
                    }
                    else if (ddlIMPFinalStatus.SelectedValue == "4")
                    {
                        personresponsile.Visible = false;
                        ManagementResponce.Visible = true;
                        //TimeLine.Visible = false;
                    }
                    else
                    {
                        personresponsile.Visible = true;
                        ManagementResponce.Visible = true;
                        //TimeLine.Visible = true;
                    }
                }

            }
        }

        private void BindTransactionDetailsIMP(int ScheduledOnID, int ImplementationInstanceID, int resultID, string ForPeriod, int CustomerBranchId, string FinancialYear, int VerticalID, int Processid, int AuditID)
        {
            try
            {
                //txtIMPObservationNumber.Enabled = false;
                //txtIMPObservationTitile.Enabled = false;
                //txtIMPObservation.Enabled = false;
                //txtIMPRisk.Enabled = false;
                //txtIMPRootCost.Enabled = false;
                //txtIMPfinancialImpact.Enabled = false;
                //txtIMPRecommendation.Enabled = false;
                //txtIMPManagementResponse.Enabled = false;
                //txtIMPTimeLine.Enabled = false;
                //ddlIMPPersonresponsible.Enabled = false;
                //ddlIMPobservationRating.Enabled = false;
                //ddlIMPObservationCategory.Enabled = false;
                //ddlIMPObservationSubCategory.Enabled = false;

                ViewState["ScheduledOnIDIMPR"] = null;
                ViewState["ScheduledOnIDIMPR"] = ScheduledOnID;
                //txtIMPObservationNumber.Text = string.Empty;
                //txtIMPObservationTitile.Text = string.Empty;
                //txtIMPObservation.Text = string.Empty;
                //txtIMPRisk.Text = string.Empty;
                //txtIMPRootCost.Text = string.Empty;
                //txtIMPfinancialImpact.Text = string.Empty;
                //txtIMPRecommendation.Text = string.Empty;
                //txtIMPManagementResponse.Text = string.Empty;
                //ddlIMPPersonresponsible.SelectedValue = "-1";
                //ddlIMPobservationRating.SelectedValue = "-1";
                //ddlIMPObservationCategory.SelectedValue = "-1";
                txtIMPNewTimeLine.Text = string.Empty;
                txtIMPNewManagementResponse.Text = string.Empty;
                txtIMPFinalStatusRemark.Text = string.Empty;

                if (ddlFilterStatus.SelectedValue != "1")
                {
                    //var MstRiskResultPrevious = RiskCategoryManagement.GetInternalControlAuditResultbyIMPlementationID(resultID, VerticalID, AuditID);
                    var RecentComplianceTransaction = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceIDIMP(Convert.ToInt32(ScheduledOnID), Convert.ToInt32(resultID));
                    //if (MstRiskResultPrevious != null)
                    //{
                       // txtIMPfinancialImpact.Text = Convert.ToString(MstRiskResultPrevious.FinancialImpact);

                        //var clouserdetails = RiskCategoryManagement.GetAuditClosureResult(ForPeriod, CustomerBranchId, FinancialYear, VerticalID, Processid, MstRiskResultPrevious.ATBDId, AuditID);
                        //if (clouserdetails != null)
                        //{
                            //txtIMPObservationNumber.Text = clouserdetails.ObservationNumber;
                            //txtIMPObservationTitile.Text = clouserdetails.ObservationTitle;
                            //txtIMPObservation.Text = clouserdetails.Observation;
                            //txtIMPRisk.Text = clouserdetails.Risk;
                            //txtIMPRootCost.Text = Convert.ToString(clouserdetails.RootCost);

                            //txtIMPRecommendation.Text = clouserdetails.Recomendation;
                            //txtIMPManagementResponse.Text = clouserdetails.ManagementResponse;
                            //txtIMPTimeLine.Text = clouserdetails.TimeLine != null ? clouserdetails.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                            //if (!string.IsNullOrEmpty(Convert.ToString(clouserdetails.PersonResponsible)))
                            //{
                            //    ddlIMPPersonresponsible.SelectedValue = Convert.ToString(clouserdetails.PersonResponsible);
                            //}
                            //if (!string.IsNullOrEmpty(Convert.ToString(clouserdetails.ObservationRating)))
                            //{
                            //    ddlIMPobservationRating.SelectedValue = Convert.ToString(clouserdetails.ObservationRating);
                            //}
                            //if (!string.IsNullOrEmpty(Convert.ToString(clouserdetails.ObservationCategory)))
                            //{
                            //    ddlIMPObservationCategory.SelectedValue = Convert.ToString(clouserdetails.ObservationCategory);
                            //    ddlObservationCategory_SelectedIndexChanged(null, null);
                            //}
                            //if (!string.IsNullOrEmpty(Convert.ToString(clouserdetails.ObservationSubCategory)))
                            //{
                            //    ddlIMPObservationSubCategory.SelectedValue = Convert.ToString(clouserdetails.ObservationSubCategory);
                            //}
                        //}
                    //}
                    var MstRiskResult = RiskCategoryManagement.GetImplementationAuditResultIMPlementationID(resultID, VerticalID, Convert.ToInt32(ScheduledOnID), AuditID);
                    if (MstRiskResult != null)
                    {
                        txtIMPFinalStatusRemark.Text = MstRiskResult.FixRemark;
                        txtIMPNewManagementResponse.Text = MstRiskResult.ManagementResponse;
                        txtIMPNewTimeLine.Text = MstRiskResult.TimeLine != null ? MstRiskResult.TimeLine.Value.ToString("dd-MM-yyyy") : null;
                        //txtIMPProcessWalkthrough.Text = MstRiskResult.ProcessWalkthrough;
                        //txtIMPActualWorkDone.Text = MstRiskResult.ActualWorkDone;
                        //txtIMPPopulation.Text = MstRiskResult.Population;
                        //txtIMPSample.Text = MstRiskResult.Sample;
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.PersonResponsible)))
                        {
                            ddlIMPNewPersonresponsible.SelectedValue = Convert.ToString(MstRiskResult.PersonResponsible);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.ImplementationStatus)))
                        {
                            if (MstRiskResult.ImplementationStatus != 0)
                            {
                                ddlIMPFinalStatus.SelectedValue = Convert.ToString(MstRiskResult.ImplementationStatus);
                                if (ddlIMPFinalStatus.SelectedValue == "4" || ddlIMPFinalStatus.SelectedValue == "5")
                                {
                                    ddlIMPFinalStatus_SelectedIndexChanged(null, null);
                                }
                            }
                        }
                    }
                    var getDated = RiskCategoryManagement.GetInternalAuditTransactionbyIDIMP(ScheduledOnID, Convert.ToInt32(resultID), ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, 4, VerticalID, AuditID);
                    if (getDated != null)
                    {
                        tbxDateIMP.Text = getDated.Dated != null ? getDated.Dated.Value.ToString("dd-MM-yyyy") : null;
                    }
                    BindDocumentIMP(ScheduledOnID, Convert.ToInt32(resultID), AuditID);
                    BindStatusListIMP(5);
                    BindTransactionsIMP(ScheduledOnID, Convert.ToInt32(resultID), AuditID);
                    if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                    {
                        BindRemarksIMP(ForPeriod, ddlFinancialYear.SelectedItem.Text, ScheduledOnID, Convert.ToInt32(resultID), AuditID);
                    }
                    if (GrdIMPRemark.Rows.Count != 0)
                    {
                        txtIMPFinalStatusRemark.Enabled = false;

                    }
                    tbxDateIMP.Text = RecentComplianceTransaction.StatusChangedOn != null ? RecentComplianceTransaction.StatusChangedOn.Value.ToString("dd-MM-yyyy") : " ";
                    var TimeLineHistory = RiskCategoryManagement.GetIMPTimeLineHistory(ResultID, Convert.ToInt32(AuditID), Convert.ToInt32(MstRiskResult.PersonResponsible));
                    grdTimelineHistory.DataSource = TimeLineHistory;
                    grdTimelineHistory.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownloadIMP_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.RiskCategoryManagement.GetFileImplementationFileData_Risk(Convert.ToInt32(hdlSelectedDocumentIDIMP.Value), -1);
                Response.Buffer = true;
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnObservationHistory_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "");
            liFinalStatus.Attributes.Add("class", "");
            liReviewHistory.Attributes.Add("class", "");
            ImplementationView.ActiveViewIndex = 0;
        }
        protected void btnActualTestingWorkDone_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "active");
            liFinalStatus.Attributes.Add("class", "");
            liReviewHistory.Attributes.Add("class", "");
            ImplementationView.ActiveViewIndex = 1;
        }
        protected void btnFinalStatus_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "");
            liFinalStatus.Attributes.Add("class", "active");
            liReviewHistory.Attributes.Add("class", "");
            ImplementationView.ActiveViewIndex = 0;
        }
        protected void btnReviewHistory_Click(object sender, EventArgs e)
        {
            liObservationHistory.Attributes.Add("class", "");
            liActualTestingWorkDone.Attributes.Add("class", "");
            liFinalStatus.Attributes.Add("class", "");
            liReviewHistory.Attributes.Add("class", "active");
            ImplementationView.ActiveViewIndex = 1;
        }
        protected void EnableDisableControlsIMP(bool Flag)
        {
            //txtIMPProcessWalkthrough.Enabled = Flag;
            //txtIMPActualWorkDone.Enabled = Flag;
            //txtIMPPopulation.Enabled = Flag;
            //txtIMPSample.Enabled = Flag;

            txtIMPNewManagementResponse.Enabled = Flag;
            txtIMPFinalStatusRemark.Enabled = Flag;
            ddlIMPNewPersonresponsible.Enabled = Flag;
            //btnIMPNext2.Enabled = Flag;

            rptIMPComplianceDocumnets.Enabled = Flag;
            txtIMPFinalStatusRemark.Enabled = Flag;
            ddlIMPFinalStatus.Enabled = Flag;
            txtIMPNewTimeLine.Enabled = Flag;

            btnIMPNext3.Enabled = Flag;
            btnIMPSave.Enabled = Flag;
            txtIMPReviewRemark.Enabled = Flag;
            ReviewFileUpload.Enabled = Flag;
            rdbtnStatusNEW.Enabled = Flag;
        }
        protected void tvImplementation_SelectedNodeChanged(object sender, EventArgs e)
        {
            btnFinalStatus_Click(sender, e);
            ImplementationView.ActiveViewIndex = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
            {
                ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
            }
            else
            {
                ResultID = Convert.ToInt32(ViewState["resultID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
            {
                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
            }
            else
            {
                VerticalID = Convert.ToInt32(ViewState["VID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
            {
                ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
            }
            else
            {
                ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
            }
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (!string.IsNullOrEmpty(ddlPeriod.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            if (ddlFinancialYear.SelectedValue != "-1")
                            {
                                if (ddlPeriod.SelectedValue != "0")
                                {
                                    var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, Convert.ToInt32(ScheduledOnId), AuditID);
                                    if (getauditimplementationscheduledetails != null)
                                    {
                                        BindTransactionDetailsIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), Convert.ToInt32(getauditimplementationscheduledetails.ImplementationInstance), Convert.ToInt32(ResultID), Convert.ToString(getauditimplementationscheduledetails.ForMonth), Convert.ToInt32(ddlFilterLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text, VerticalID, Convert.ToInt32(getauditimplementationscheduledetails.ProcessId), Convert.ToInt32(AuditID));
                                    }

                                    TDIMPTab.Visible = true;
                                    btnFinalStatus_Click(sender, e);
                                    if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 2
                                        || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4
                                        || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 5)
                                    {
                                        EnableDisableControlsIMP(true);
                                    }
                                    else
                                    {
                                        EnableDisableControlsIMP(false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        protected void btnIMPNext2_Click(object sender, EventArgs e)
        {
            try
            {
                long roleid = 3;
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchId = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    BranchId = Convert.ToInt32(ViewState["BID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(FinYear, Period, BranchId, VerticalID, ScheduledOnId, AuditID);
                if (getauditimplementationscheduledetails != null)
                {
                    ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                    {
                        AuditScheduleOnID = getauditimplementationscheduledetails.Id,
                        FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                        ForPeriod = getauditimplementationscheduledetails.ForMonth,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ResultID = ResultID,
                        RoleID = roleid,
                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                        AuditID = AuditID,
                    };

                    //if (txtIMPProcessWalkthrough.Text.Trim() == "")
                    //    MstRiskResult.ProcessWalkthrough = "";
                    //else
                    //    MstRiskResult.ProcessWalkthrough = txtIMPProcessWalkthrough.Text.Trim();

                    //if (txtIMPActualWorkDone.Text.Trim() == "")
                    //    MstRiskResult.ActualWorkDone = "";
                    //else
                    //    MstRiskResult.ActualWorkDone = txtIMPActualWorkDone.Text.Trim();

                    //if (txtIMPPopulation.Text.Trim() == "")
                    //    MstRiskResult.Population = "";
                    //else
                    //    MstRiskResult.Population = txtIMPPopulation.Text.Trim();

                    //if (txtIMPSample.Text.Trim() == "")
                    //    MstRiskResult.Sample = "";
                    //else
                    //    MstRiskResult.Sample = txtIMPSample.Text.Trim();


                    if (txtIMPFinalStatusRemark.Text.Trim() == "")
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = txtIMPFinalStatusRemark.Text.Trim();

                    if (txtIMPNewManagementResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtIMPNewManagementResponse.Text.Trim();

                    if (ddlIMPNewPersonresponsible.SelectedValue == "-1")
                    {
                        MstRiskResult.PersonResponsible = null;
                    }
                    else
                    {
                        MstRiskResult.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                    }

                    if (ddlIMPFinalStatus.SelectedValue == "-1")
                    {
                        MstRiskResult.ImplementationStatus = null;
                    }
                    else
                    {
                        MstRiskResult.ImplementationStatus = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                    }
                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtIMPNewTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;

                    bool Success1 = false;
                    if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                    {
                        if (!string.IsNullOrEmpty(ddlFilterStatus.SelectedValue))
                        {
                            if (ddlFilterStatus.SelectedValue == "4")
                            {
                                MstRiskResult.Status = 4;
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.CreatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                            }
                            else if (ddlFilterStatus.SelectedValue == "6")
                            {
                                MstRiskResult.Status = 6;
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.CreatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                            }
                            else if (ddlFilterStatus.SelectedValue == "5")
                            {
                                MstRiskResult.Status = 5;
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.CreatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                            }
                            else
                            {
                                int checkStatusId = -1;
                                if (ddlFilterStatus.SelectedValue != "")
                                {
                                    if (ddlFilterStatus.SelectedValue == "4")
                                    {
                                        checkStatusId = 4;
                                    }
                                    else if (ddlFilterStatus.SelectedValue == "5")
                                    {
                                        checkStatusId = 5;
                                    }
                                    else if (ddlFilterStatus.SelectedValue == "6")
                                    {
                                        checkStatusId = 6;
                                    }
                                }
                                if (checkStatusId != -1)
                                {
                                    MstRiskResult.Status = checkStatusId;
                                }
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                            }
                        }
                        else
                        {
                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                        }
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                    }

                    if (Success1)
                    {
                        btnFinalStatus_Click(sender, e);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnIMPNext3_Click(object sender, EventArgs e)
        {
            try
            {
                long roleid = 3;
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchId = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    BranchId = Convert.ToInt32(ViewState["BID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(FinYear, Period, BranchId, VerticalID, ScheduledOnId, AuditID);
                if (getauditimplementationscheduledetails != null)
                {
                    ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                    {
                        AuditScheduleOnID = getauditimplementationscheduledetails.Id,
                        FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                        ForPeriod = getauditimplementationscheduledetails.ForMonth,
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                        IsDeleted = false,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        ResultID = ResultID,
                        RoleID = roleid,
                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                        AuditID = AuditID,
                    };

                    AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                    {
                        CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                        ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                        FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                        CustomerBranchId = BranchId,
                        ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                        ForPeriod = getauditimplementationscheduledetails.ForMonth,
                        ResultID = ResultID,
                        RoleID = roleid,
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                        VerticalID = VerticalID,
                        ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                        AuditID = AuditID,
                    };
                    //if (txtIMPProcessWalkthrough.Text.Trim() == "")
                    //    MstRiskResult.ProcessWalkthrough = "";
                    //else
                    //    MstRiskResult.ProcessWalkthrough = txtIMPProcessWalkthrough.Text.Trim();

                    //if (txtIMPActualWorkDone.Text.Trim() == "")
                    //    MstRiskResult.ActualWorkDone = "";
                    //else
                    //    MstRiskResult.ActualWorkDone = txtIMPActualWorkDone.Text.Trim();

                    //if (txtIMPPopulation.Text.Trim() == "")
                    //    MstRiskResult.Population = "";
                    //else
                    //    MstRiskResult.Population = txtIMPPopulation.Text.Trim();

                    //if (txtIMPSample.Text.Trim() == "")
                    //    MstRiskResult.Sample = "";
                    //else
                    //    MstRiskResult.Sample = txtIMPSample.Text.Trim();

                    if (txtIMPNewManagementResponse.Text.Trim() == "")
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = txtIMPNewManagementResponse.Text.Trim();
                    
                    if (txtIMPFinalStatusRemark.Text.Trim() == "")
                    {
                        MstRiskResult.FixRemark = "";
                        transaction.AuditorRemark = "";
                    }
                    else
                    {
                        MstRiskResult.FixRemark = txtIMPFinalStatusRemark.Text.Trim();
                        transaction.AuditorRemark = txtIMPFinalStatusRemark.Text.Trim();
                    }
                    if (ddlIMPNewPersonresponsible.SelectedValue == "-1")
                    {
                        MstRiskResult.PersonResponsible = null;
                    }
                    else
                    {
                        MstRiskResult.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                    }
                    if (ddlIMPFinalStatus.SelectedValue == "-1")
                    {
                        MstRiskResult.ImplementationStatus = null;
                        transaction.ImplementationStatusId = null;
                    }
                    else
                    {
                        MstRiskResult.ImplementationStatus = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                        transaction.ImplementationStatusId = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                    }

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                    {
                        dt = DateTime.ParseExact(txtIMPNewTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        MstRiskResult.TimeLine = dt.Date;
                    }
                    else
                        MstRiskResult.TimeLine = null;
                    
                    var ResponstStatus = RiskCategoryManagement.GetLatestImplementationAuditteeResponse(ResultID, VerticalID, AuditID);
                    if (ResponstStatus != null)
                    {
                        transaction.AuditeeResponse = ResponstStatus.AuditeeResponse;
                    }

                    bool Success1 = false;
                    bool Success2 = false;
                    BindRemarksIMP(Convert.ToString(getauditimplementationscheduledetails.ForMonth), FinYear, Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, AuditID);

                    if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                    {
                        if (!string.IsNullOrEmpty(ddlFilterStatus.SelectedValue))
                        {
                            if (ddlFilterStatus.SelectedValue == "4")
                            {
                                MstRiskResult.Status = 4;
                                MstRiskResult.Status = 4;
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                            }
                            else if (ddlFilterStatus.SelectedValue == "6")
                            {
                                MstRiskResult.Status = 6;
                                transaction.StatusId = 6;
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                            }
                            else if (ddlFilterStatus.SelectedValue == "5")
                            {
                                MstRiskResult.Status = 5;
                                transaction.StatusId = 5;
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                            }
                            else
                            {
                                if (ResponstStatus != null)
                                {
                                    if (ResponstStatus.AuditeeResponse == "RS" || ResponstStatus.AuditeeResponse == "2R")
                                    {
                                        MstRiskResult.Status = 5;
                                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        transaction.CreatedOn = DateTime.Now;
                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                    }
                                    else
                                    {
                                        MstRiskResult.Status = 2;
                                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        transaction.CreatedOn = DateTime.Now;
                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                    }
                                }
                                Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                            }
                        }
                        else
                        {
                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                        }
                    }
                    else
                    {
                        if (ResponstStatus != null)
                        {
                            if (ResponstStatus.AuditeeResponse == "RS" || ResponstStatus.AuditeeResponse == "2R")
                            {
                                MstRiskResult.Status = 5;
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.CreatedOn = DateTime.Now;
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                            }
                            else
                            {
                                MstRiskResult.Status = 2;
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.CreatedOn = DateTime.Now;
                                MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.UpdatedOn = DateTime.Now;
                            }
                        }
                        Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                    }
                    if (RiskCategoryManagement.AuditImplementationTransactionExists(transaction))
                    {
                        if (ResponstStatus != null)
                        {
                            if (ResponstStatus.AuditeeResponse == "RS" || ResponstStatus.AuditeeResponse == "2R")
                            {
                                transaction.StatusId = 5;
                                transaction.Remarks = "Implementation Step Under Final Review.";
                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.CreatedOn = DateTime.Now;
                            }
                            else
                            {
                                transaction.StatusId = 2;
                                transaction.Remarks = "Implementation Step Submitted.";
                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.CreatedOn = DateTime.Now;
                            }
                        }
                        Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                    }
                    else
                    {
                        if (ResponstStatus != null)
                        {
                            if (ResponstStatus.AuditeeResponse == "RS" || ResponstStatus.AuditeeResponse == "2R")
                            {
                                transaction.StatusId = 5;
                                transaction.Remarks = "Implementation Step Under Final Review.";
                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.CreatedOn = DateTime.Now;
                            }
                            else
                            {
                                transaction.StatusId = 2;
                                transaction.Remarks = "Implementation Step Submitted.";
                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.CreatedOn = DateTime.Now;
                            }
                        }
                        Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                    }

                    if (Success1 && Success2)
                    {
                        CreateAuditImplementationTransaction(getauditimplementationscheduledetails, transaction, CustomerId, AuditID);
                        BindTransactionsIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), Convert.ToInt32(ResultID), Convert.ToInt32(AuditID));
                        btnReviewHistory_Click(sender, e);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnIMPSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                else
                {
                    FinYear = Convert.ToString(ViewState["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["BID"]))
                {
                    BranchId = Convert.ToInt32(Request.QueryString["BID"]);
                }
                else
                {
                    BranchId = Convert.ToInt32(ViewState["BID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                else
                {
                    VerticalID = Convert.ToInt32(ViewState["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                if (string.IsNullOrEmpty(txtIMPNewManagementResponse.Text.Trim()))
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Enter Management Response.";
                    ImplementationView.ActiveViewIndex = 0;
                    txtIMPNewManagementResponse.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Enter TimeLine.";
                    ImplementationView.ActiveViewIndex = 0;
                    txtIMPNewTimeLine.Focus();
                    return;
                }
                if (ddlIMPFinalStatus.SelectedValue == "-1")
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select  implementation Status";
                    ImplementationView.ActiveViewIndex = 0;
                    return;
                }
                if (ddlIMPNewPersonresponsible.SelectedValue == "-1")
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select  Person Responsible";
                    ImplementationView.ActiveViewIndex = 0;
                    return;
                }
                bool Flag = false;
                long roleid = 4;
                if (ddlIMPFinalStatus.SelectedValue != "-1")
                {

                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    var getauditimplementationscheduledetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(FinYear, Period, Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, ScheduledOnId, AuditID);
                    if (getauditimplementationscheduledetails != null)
                    {
                        if (rdbtnStatusNEW.SelectedValue != "")
                        {
                            int StatusID = 0;
                            if (lblStatusIMP.Visible)
                                StatusID = Convert.ToInt32(rdbtnStatusNEW.SelectedValue);
                            else
                                StatusID = Convert.ToInt32(ComplianceManagement.Business.RiskCategoryManagement.GetClosedTransaction(Convert.ToInt32(getauditimplementationscheduledetails.Id)).AuditStatusID);

                            ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                            {
                                AuditScheduleOnID = getauditimplementationscheduledetails.Id,
                                FinancialYear = getauditimplementationscheduledetails.FinancialYear,
                                ForPeriod = getauditimplementationscheduledetails.ForMonth,
                                CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                IsDeleted = false,
                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                ResultID = ResultID,
                                RoleID = roleid,
                                ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                VerticalID = VerticalID,
                                ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                AuditID = AuditID,
                            };

                            AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                            {
                                CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                StatusChangedOn = GetDate(b.ToString("dd/MM/yyyy")),
                                ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                                FinancialYear = ddlFinancialYear.SelectedItem.Text,
                                CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                ForPeriod = ddlPeriod.SelectedItem.Text,
                                ResultID = ResultID,
                                RoleID = roleid,
                                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                VerticalID = VerticalID,
                                ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                AuditID = AuditID,
                            };

                            int AuditteeResponse = CustomerManagementRisk.GetLatestAuditteeStatus(AuditID, ResultID);
                            var ResponstStatus = RiskCategoryManagement.GetLatestImplementationAuditteeResponse(ResultID, VerticalID, AuditID);
                            string remark = string.Empty;
                            if (rdbtnStatusNEW.SelectedItem.Text == "Closed")
                            {
                                transaction.StatusId = 3;
                                MstRiskResult.Status = 3;
                                transaction.AuditeeResponse = "AC";
                                remark = "Implementation Step Closed.";
                            }
                            else if (rdbtnStatusNEW.SelectedItem.Text == "Team Review")
                            {
                                transaction.StatusId = 4;
                                MstRiskResult.Status = 4;
                                remark = "Implementation Step Under Team Review.";
                            }
                            else if (rdbtnStatusNEW.SelectedItem.Text == "Final Review")
                            {
                                transaction.StatusId = 5;
                                MstRiskResult.Status = 5;
                                remark = "Implementation Step Under Final Review.";
                                if (ResponstStatus != null)
                                {
                                    if (ResponstStatus.AuditeeResponse == "PS" || ResponstStatus.AuditeeResponse == "2R")
                                    {
                                        transaction.AuditeeResponse = "RS";
                                    }
                                    else
                                    {
                                        transaction.AuditeeResponse = "3R";
                                    }
                                }
                            }
                            else if (rdbtnStatusNEW.SelectedItem.Text == "Auditee Review")
                            {
                                transaction.StatusId = 6;
                                MstRiskResult.Status = 6;
                                remark = "Implementation Step Under Auditee Review.";
                            }

                            if (ddlIMPFinalStatus.SelectedValue == "-1")
                            {
                                MstRiskResult.ImplementationStatus = null;
                                transaction.ImplementationStatusId = null;
                            }
                            else
                            {
                                MstRiskResult.ImplementationStatus = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                                transaction.ImplementationStatusId = Convert.ToInt32(ddlIMPFinalStatus.SelectedValue);
                            }

                            //if (txtIMPProcessWalkthrough.Text.Trim() == "")
                            //    MstRiskResult.ProcessWalkthrough = "";
                            //else
                            //    MstRiskResult.ProcessWalkthrough = txtIMPProcessWalkthrough.Text.Trim();

                            //if (txtIMPActualWorkDone.Text.Trim() == "")
                            //    MstRiskResult.ActualWorkDone = "";
                            //else
                            //    MstRiskResult.ActualWorkDone = txtIMPActualWorkDone.Text.Trim();

                            //if (txtIMPPopulation.Text.Trim() == "")
                            //    MstRiskResult.Population = "";
                            //else
                            //    MstRiskResult.Population = txtIMPPopulation.Text.Trim();

                            //if (txtIMPSample.Text.Trim() == "")
                            //    MstRiskResult.Sample = "";
                            //else
                            //    MstRiskResult.Sample = txtIMPSample.Text.Trim();

                            if (txtIMPNewManagementResponse.Text.Trim() == "")
                                MstRiskResult.ManagementResponse = "";
                            else
                                MstRiskResult.ManagementResponse = txtIMPNewManagementResponse.Text.Trim();

                            if (txtIMPFinalStatusRemark.Text.Trim() == "")
                                MstRiskResult.FixRemark = "";
                            else
                                MstRiskResult.FixRemark = txtIMPFinalStatusRemark.Text.Trim();

                            if (ddlIMPNewPersonresponsible.SelectedValue == "-1")
                            {
                                MstRiskResult.PersonResponsible = null;
                                transaction.PersonResponsible = null;
                            }
                            else
                            {
                                MstRiskResult.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                                transaction.PersonResponsible = Convert.ToInt32(ddlIMPNewPersonresponsible.SelectedValue);
                            }

                            transaction.Remarks = remark.Trim();

                            DateTime dt = new DateTime();
                            if (!string.IsNullOrEmpty(txtIMPNewTimeLine.Text.Trim()))
                            {
                                dt = DateTime.ParseExact(txtIMPNewTimeLine.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                MstRiskResult.TimeLine = dt.Date;
                            }
                            else
                                MstRiskResult.TimeLine = null;

                            bool Success1 = false;
                            bool Success2 = false;

                            if (rdbtnStatusNEW.SelectedValue == "3")
                            {
                                transaction.AuditeeResponse = "AC";
                            }

                            if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                            {
                                if (!string.IsNullOrEmpty(ddlFilterStatus.SelectedValue))
                                {
                                    if (ddlFilterStatus.SelectedValue == "5")
                                    {
                                        if (RiskCategoryManagement.ImplementationAuditResultExistsWithStatus(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateImplementationAuditResultResult(MstRiskResult, 5);
                                        }
                                        else
                                        {
                                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.CreatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                        }
                                    }
                                    else
                                    {
                                        if (RiskCategoryManagement.ImplementationAuditResultExistsWithStatus(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                                        }
                                        else
                                        {
                                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.CreatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                        }
                                    }
                                }
                                else
                                {
                                    if (RiskCategoryManagement.ImplementationAuditResultExistsWithStatus(MstRiskResult))
                                    {
                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                                    }
                                    else
                                    {
                                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                        MstRiskResult.CreatedOn = DateTime.Now;
                                        Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                    }
                                }
                            }
                            else
                            {
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.CreatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                            }
                            if (RiskCategoryManagement.AuditImplementationTransactionExists(transaction))
                            {
                                if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 5 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 6)
                                {
                                    transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    transaction.CreatedOn = DateTime.Now;
                                    Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                                }
                                else
                                {
                                    transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    transaction.UpdatedOn = DateTime.Now;
                                    Success2 = RiskCategoryManagement.UpdateAuditImplementationTransactionTxnStatusReviewer(transaction);
                                }
                            }
                            else
                            {
                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                transaction.CreatedOn = DateTime.Now;
                                Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                            }
                            string Testremark = "";
                            if (string.IsNullOrEmpty(txtIMPReviewRemark.Text))
                            {
                                Testremark = "NA";
                            }
                            else
                            {
                                Testremark = txtIMPReviewRemark.Text.Trim();
                            }
                            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
                            HttpFileCollection fileCollection1 = Request.Files;

                            if (fileCollection1.Count > 0)
                            {

                                var InstanceData = RiskCategoryManagement.GetAuditImplementationInstanceData(getauditimplementationscheduledetails.ForMonth, Convert.ToInt32(getauditimplementationscheduledetails.ImplementationInstance), Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, AuditID);

                                string directoryPath = "";
                                if (!string.IsNullOrEmpty(InstanceData.ForPeriod) && InstanceData.CustomerBranchID != -1)
                                {
                                    directoryPath = Server.MapPath("~/ImplementationAdditionalDocument/" + CustomerId + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + ddlFinancialYear.SelectedItem.Text + "/" + InstanceData.ForPeriod.ToString() + "/" + getauditimplementationscheduledetails.Id + "/1.0");
                                }
                                DocumentManagement.CreateDirectory(directoryPath);
                                for (int i = 0; i < fileCollection1.Count; i++)
                                {
                                    HttpPostedFile uploadfile1 = fileCollection1[i];
                                    string[] keys1 = fileCollection1.Keys[i].Split('$');
                                    String fileName1 = "";
                                    if (keys1[keys1.Count() - 1].Equals("ReviewFileUpload"))
                                    {
                                        fileName1 = uploadfile1.FileName;
                                    }
                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadfile1.FileName));
                                    Stream fs = uploadfile1.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                    Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                                    if (uploadfile1.ContentLength > 0)
                                    {
                                        ImplementationReviewHistory RH = new ImplementationReviewHistory()
                                        {
                                            ResultID = ResultID,
                                            ForMonth = Convert.ToString(getauditimplementationscheduledetails.ForMonth),
                                            ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                            Dated = DateTime.Now,
                                            Remarks = Testremark,
                                            ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                                            FinancialYear = FinYear,
                                            CustomerBranchId = BranchId,
                                            Name = fileName1,
                                            FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                            FileKey = fileKey1.ToString(),
                                            Version = "1.0",
                                            VersionDate = DateTime.UtcNow,
                                            FixRemark = txtIMPFinalStatusRemark.Text.Trim(),
                                            VerticalID = VerticalID,
                                            ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                            AuditID = AuditID,
                                        };
                                        Flag = RiskCategoryManagement.CreateImplementationReviewHistoryRemark(RH);
                                        DocumentManagement.Audit_SaveDocFiles(Filelist1);
                                        Label2.Text = "";
                                        Label1.Text = "";
                                    }
                                    else
                                    {
                                        ImplementationReviewHistory RH = new ImplementationReviewHistory()
                                        {
                                            ForMonth = Convert.ToString(getauditimplementationscheduledetails.ForMonth),
                                            ImplementationInstance = getauditimplementationscheduledetails.ImplementationInstance,
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                            Dated = DateTime.Now,
                                            Remarks = Testremark,
                                            ImplementationScheduleOnID = getauditimplementationscheduledetails.Id,
                                            FinancialYear = FinYear,
                                            CustomerBranchId = BranchId,
                                            ResultID = ResultID,
                                            FixRemark = txtIMPFinalStatusRemark.Text.Trim(),
                                            VerticalID = VerticalID,
                                            ProcessId = Convert.ToInt32(getauditimplementationscheduledetails.ProcessId),
                                            AuditID = AuditID,
                                        };
                                        Flag = RiskCategoryManagement.CreateImplementationReviewHistoryRemark(RH);
                                    }
                                }
                            }
                            BindRemarksIMP(Convert.ToString(getauditimplementationscheduledetails.ForMonth), FinYear, Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, AuditID);
                            if (Success1 == true && Success2 == true && Flag == true)
                            {
                                BindTransactionsIMP(Convert.ToInt32(getauditimplementationscheduledetails.Id), ResultID, Convert.ToInt32(AuditID));
                                txtIMPReviewRemark.Text = string.Empty;

                                if (ddlIMPFinalStatus.SelectedValue == "3")
                                {
                                    RiskCategoryManagement.UpdateImplementationAuditResultISACTIVE(ResultID, AuditID);
                                    RiskCategoryManagement.UpdateAuditImplementationTransactionISACTIVE(ResultID, AuditID);
                                }
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Implementation Step Submitted Successfully";
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Status";
                        }
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Final Implementation Status";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool CreateAuditImplementationTransaction(AuditImpementationScheduleOn ImpementationScheduleOnDetails, AuditImplementationTransaction transaction, long customerID, long AuditID)
        {
            bool flag = false;
            if (!string.IsNullOrEmpty(Request.QueryString["ResultID"]))
            {
                ResultID = Convert.ToInt32(Request.QueryString["ResultID"]);
            }
            else
            {
                ResultID = Convert.ToInt32(ViewState["ResultID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
            {
                VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
            }
            else
            {
                VerticalID = Convert.ToInt32(ViewState["VID"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            if (ImpementationScheduleOnDetails != null)
            {
                if (fuIMPWorkingUpload.HasFile)
                {
                    HttpFileCollection fileCollection1 = Request.Files;
                    if (fileCollection1.Count > 0)
                    {
                        List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                        List<ImplementationFileData_Risk> files = new List<ImplementationFileData_Risk>();
                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                        var InstanceData = RiskCategoryManagement.GetAuditImplementationInstanceData(ImpementationScheduleOnDetails.ForMonth, Convert.ToInt32(ImpementationScheduleOnDetails.ImplementationInstance), Convert.ToInt32(ddlFilterLocation.SelectedValue), VerticalID, AuditID);
                        string directoryPath = "";
                        if (!string.IsNullOrEmpty(InstanceData.ForPeriod) && InstanceData.CustomerBranchID != -1)
                        {
                            directoryPath = Server.MapPath("~/ImplementationWorkingDocument/" + customerID + "/" + InstanceData.CustomerBranchID.ToString() + "/" + InstanceData.VerticalID.ToString() + "/" + ddlFinancialYear.SelectedItem.Text + "/" + InstanceData.ForPeriod.ToString() + "/" + ImpementationScheduleOnDetails.Id + "/1.0");
                        }
                        DocumentManagement.CreateDirectory(directoryPath);
                        for (int i = 0; i < fileCollection1.Count; i++)
                        {
                            HttpPostedFile uploadfile = fileCollection1[i];
                            string[] keys = fileCollection1.Keys[i].Split('$');
                            string fileName = "";
                            if (keys[keys.Count() - 1].Equals("fuIMPWorkingUpload"))
                            {
                                fileName = uploadfile.FileName;
                            }
                            Guid fileKey = Guid.NewGuid();
                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));
                            Stream fs = uploadfile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));
                            if (uploadfile.ContentLength > 0)
                            {
                                ImplementationFileData_Risk file = new ImplementationFileData_Risk()
                                {
                                    Name = fileName,
                                    FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                    FileKey = fileKey.ToString(),
                                    Version = "1.0",
                                    VersionDate = DateTime.UtcNow,
                                    ForPeriod = ImpementationScheduleOnDetails.ForMonth,
                                    CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
                                    FinancialYear = ImpementationScheduleOnDetails.FinancialYear,
                                    IsDeleted = false,
                                    ResultId = ResultID,
                                    VerticalID = VerticalID,
                                    AuditID = AuditID,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };
                                files.Add(file);
                            }
                        }//For Loop End  
                        if (Filelist != null && list != null)
                            flag = UserManagementRisk.CreateAuditImplementationTransaction(transaction, files, list, Filelist);
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                        }
                    }//FileCollection Count End                    
                }//HasUpload File End
            }//ImpementationScheduleOnDetails Is null End
            return flag;
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void rptIMPComplianceDocumnets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (e.CommandName.Equals("DownloadIMP"))
                {
                    DownloadFileIMP(Convert.ToInt32(e.CommandArgument), AuditID);
                }
                else if (e.CommandName.Equals("DeleteIMP"))
                {
                    DeleteFileIMP(Convert.ToInt32(e.CommandArgument));
                    BindDocumentIMP(ScheduledOnId, ResultID, AuditID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptIMPComplianceDocumnets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lblDownLoadfile = (LinkButton)e.Row.FindControl("btnComplianceDocumnetsIMP");

            if (lblDownLoadfile != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
            LinkButton lbtLinkDocbutton = (LinkButton)e.Row.FindControl("lbtLinkDocbuttonIMP");
            if (lbtLinkDocbutton != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
            }
        }

        protected void grdTransactionIMPHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                grdTransactionIMPHistory.PageIndex = e.NewPageIndex;
                BindTransactionsIMP(ScheduledOnId, ResultID, Convert.ToInt32(AuditID));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void GrdIMPRemark_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ForMonth"]))
                {
                    Period = Request.QueryString["ForMonth"];
                }
                else
                {
                    Period = Convert.ToString(ViewState["ForMonth"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
                {
                    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
                }
                else
                {
                    AuditID = Convert.ToInt32(ViewState["AuditID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["resultID"]))
                {
                    ResultID = Convert.ToInt32(Request.QueryString["resultID"]);
                }
                else
                {
                    ResultID = Convert.ToInt32(ViewState["resultID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["scheduledonid"]))
                {
                    ScheduledOnId = Convert.ToInt32(Request.QueryString["scheduledonid"]);
                }
                else
                {
                    ScheduledOnId = Convert.ToInt32(ViewState["scheduledonid"]);
                }
                GrdIMPRemark.PageIndex = e.NewPageIndex;
                BindRemarksIMP(Period, ddlFinancialYear.SelectedItem.Text, ScheduledOnId, ResultID, AuditID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploaded";
            }
            return processnonprocess;
        }

        #endregion
        public string ShowSampleDocumentNameView(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "/View";
            }
            return processnonprocess;
        }

        protected void lblViewFile_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;
            Label lblID = (Label)gvr.FindControl("lblID");
            Label lblriskID = (Label)gvr.FindControl("lblriskID");
            Label lblAuditScheduleOnId = (Label)gvr.FindControl("lblAuditScheduleOnId");
            Label lblATBDId = (Label)gvr.FindControl("lblATBDId");
            if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            }
            else
            {
                AuditID = Convert.ToInt32(ViewState["AuditID"]);
            }
            List<ImplementationReviewHistory> fileData = GetFileDataIMP(Convert.ToInt32(lblID.Text), Convert.ToInt32(lblriskID.Text), Convert.ToInt32(lblAuditScheduleOnId.Text), AuditID);

            foreach (var file in fileData)
            {
                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                if (file.FilePath != null && File.Exists(filePath))
                {
                    string Folder = "~/TempFiles";
                    string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    string DateFolder = Folder + "/" + FileData;

                    string extension = System.IO.Path.GetExtension(filePath);

                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                    if (!Directory.Exists(DateFolder))
                    {
                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                    }

                    string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    string User = Portal.Common.AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                    string FileName = DateFolder + "/" + User + "" + extension;

                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);                    
                    if (file.EnType == "M")
                    {
                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    else
                    {
                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    }
                    bw.Close();

                    string CompDocReviewPath = FileName;
                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "ConfirmtestViewIMPAM('" + CompDocReviewPath + "');", true);
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                }
                break;
            }
        }
    }
}