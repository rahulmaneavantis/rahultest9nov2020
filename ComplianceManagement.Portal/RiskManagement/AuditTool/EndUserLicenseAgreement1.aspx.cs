﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class EndUserLicenseAgreement1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod()]
        public static void Update(string approve)
        {
            if (!string.IsNullOrEmpty(approve) && approve == "Yes")
            {
                bool result = false;
                int UserID = Portal.Common.AuthenticationHelper.UserID;
                User user = UserManagement.GetByID(UserID);
                mst_User mstUser = UserManagementRisk.GetByID(UserID);
                if (mstUser != null)
                {
                    mstUser.AcceptEndUserLicenseAgreement = true;
                }

                if (user != null)
                {
                    user.AcceptEndUserLicenseAgreement = true;
                }

                result = UserManagement.UpdateAcceptEndUserLicenseAgreementFlag(user);
                result = UserManagementRisk.UpdateAcceptEndUserLicenseAgreementFlag(mstUser);
                if (result)
                {
                    //Response.Write("<script>alert('Please Select Customer.');</script>");
                }
            }
        }
    }
}