﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckListPreReqsite.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.CheckListPreReqsite" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/responsive-calendar.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- full calendar css-->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css" />
    <!-- owl carousel -->
    <link rel="stylesheet" href="https://avacdn.azureedge.net/newcss/owl.carousel.css" type="text/css" />
    <!-- Custom styles -->
    <link rel="stylesheet" href="https://avacdn.azureedge.net/newcss/fullcalendar.css" type="text/css" />
    <%--<link href="https://avacdn.azureedge.net/newcss/stylenew.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/jquery-ui-1.10.7.min.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>


    <%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .table-paging-text p {
            color: #555;
            text-align: center;
            font-size: 14px;
            padding-top: 7px;
        }
    </style>

    <script type="text/javascript">
        function ShowDOCDialog(auID, atbdID, checklistID) {
            $('#divPREDOCShowDialog').modal('show');
            $('#PreDOCshowdetails').attr('width', '98%');
            $('#PreDOCshowdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#PreDOCshowdetails').attr('src', "../AuditTool/CheckListPreReqsiteDocuments.aspx?AuditID=" + auID + "&ATID=" + atbdID + "&CHID=" + checklistID);
        };

        function ShowAddCheckList() {
            $('#divCheckListAdd').modal('show');
            $('.modal-dialog').css('width', '50%');
        };

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }
        // added by Sagar More on 19-12-2019
        function setTimeLine() {
            var sDate = new Date();
            $('input[id*=txtStartDateKickoff]').datepicker(
                {
                    dateFormat: 'dd-mm-yy',
                    minDate: sDate,
                });

            $('input[id*=txtStartDate]').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: sDate,
            });
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        // code change by Sagar more on 19-12-2019
        $(document).ready(function () {
            $(function () {
                var startDate = new Date();
                $('input[id*=txtStartDateKickoff]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                    });

                $('input[id*=txtStartDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: startDate,
                });
            });
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $(function () {
                    var startDate = new Date();
                    $('input[id*=txtStartDateKickoff]').datepicker(
                        {
                            dateFormat: 'dd-mm-yy',
                            minDate: startDate,
                        });
                    $('input[id*=txtStartDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                    });
                });
            }
        });

        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        $('.btn-search').on('click', function () {

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        });
    </script>

    <script>
        function Resetbuttonminisize() {
            $("#collapseActDetails").removeClass("collapse").addClass("in");
            return true;
        }

        function OpenUserPopup() {
            $('#AddUserModelPopup').modal('show');
            $('#IUser').attr('width', '100%');
            $('#IUser').attr('height', '400px');
            $('.modal-dialog').css('width', '60%');
            $('.modal-dialog').css('height', '500px');
            var CustomerID = <%= Session["CustId"] %>;
            console.log(CustomerID);
            $('#IUser').attr('src', "../../RiskManagement/AuditTool/AuditUser.aspx?CustomerId="+ CustomerID);
        }
    </script>

    <script type="text/javascript">
        function REcheckAll(objRef) {
            for (var i = 0; i < $('span.classrem > input').length; i++) {
                if ($('#grdRiskActivityMatrix_remindercheckAll').is(':checked')) {
                    $($('span.classrem > input')[i]).prop("checked", true);
                    $('#btnReminderEscalation').show();

                } else {
                    $($('span.classrem > input')[i]).prop("checked", false);
                    $('#btnReminderEscalation').hide();
                }
            }            
        }

        function REchildcheckAll(objRef) {
            for (var i = 0; i < $('span.classrem > input').length; i++) {
                if ($($('span.classrem > input')[i]).is(':checked')) {
                    $($('span.classrem > input')[i]).prop("checked", true);
                    $('#btnReminderEscalation').show();
                    break;
                }
                else {
                    $($('span.classrem > input')[i]).prop("checked", false);
                    $('#btnReminderEscalation').hide();
                }
            }
        }

        function EScheckAll(objRef) {           
            for (var i = 0; i < $('span.classescal > input').length; i++) {
                if ($('#grdRiskActivityMatrix_escalationcheckAll').is(':checked'))
                {
                    $($('span.classescal > input')[i]).prop("checked", true);
                    $('#btnReminderEscalation').show();
                    
                } else {                    
                    $($('span.classescal > input')[i]).prop("checked", false);
                    $('#btnReminderEscalation').hide();
                }
            }
        }


        function ESchildcheckAll(objRef) {
            for (var i = 0; i < $('span.classescal > input').length; i++) {
                if ($($('span.classescal > input')[i]).is(':checked')) {
                    $($('span.classescal > input')[i]).prop("checked", true);
                    $('#btnReminderEscalation').show();
                    break;
                }
                else {
                    $($('span.classescal > input')[i]).prop("checked", false);
                    $('#btnReminderEscalation').hide();
                }
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upPR">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <asp:UpdatePanel ID="upPR" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12 colpadding0">
                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" class="alert alert-block alert-danger fade in"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" class="alert alert-block alert-danger fade in" />
                        <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                    </div>
                    <div id="ActDetails" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails">
                                                <h2>Audit Customer data Request Summary</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="collapseActDetails" class="collapse">
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                            <div class="col-md-12 colpadding0">
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                    <asp:DropDownListChosen runat="server" ID="ddlProcess" DataPlaceHolder="Select Process" Width="95%"
                                                        AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15 select_location"
                                                        Style="float: left; width: 90%;" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                                    </asp:DropDownListChosen>
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                    <asp:DropDownListChosen runat="server" ID="ddlSubProcess" DataPlaceHolder="Select Sub Process" Width="95%"
                                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                        class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged">
                                                    </asp:DropDownListChosen>
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                &nbsp;
                                                <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false"
                                                    PageSize="20"
                                                    AllowPaging="true" AutoPostBack="true" ShowHeaderWhenEmpty="true"
                                                    CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                                    OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging" ShowFooter="true"
                                                    OnRowDataBound="grdRiskActivityMatrix_RowDataBound"
                                                    OnRowCommand="grdRiskActivityMatrix_RowCommand">
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="auditiD" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAuditID" runat="server" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                                <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ProcessID") %>'></asp:Label>
                                                                <asp:Label ID="lblsubprocessID" runat="server" Text='<%# Eval("SubprocessID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Process Name" ItemStyle-Width="170">
                                                            <ItemTemplate>
                                                                <div class="text_NlinesusingCSS" style="width: 220px;">
                                                                    <asp:Label ID="lblProcessName" runat="server" Text='<%# Eval("ProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Sub Process Name" ItemStyle-Width="170">
                                                            <ItemTemplate>
                                                                <div class="text_NlinesusingCSS" style="width: 220px;">
                                                                    <asp:Label ID="lblSubProcessName" runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Document Required" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="130">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblDocumentRequired" runat="server" Text='<%# Eval("Document_Required") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Document_Required") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Document Recevied" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="140">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblDocumentRecevied" runat="server" Text='<%# Eval("Document_Recevied") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Document_Recevied") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="&nbsp;&nbsp;&nbsp;&nbsp;Pending" HeaderStyle-HorizontalAlign="Justify" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblPending" runat="server" Text='<%# Eval("Pending") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Pending") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="&nbsp;&nbsp;&nbsp;&nbsp;Reminder" ItemStyle-Width="50">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="remindercheckAll" Text="Reminder" runat="server" onclick="REcheckAll(this);" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="rechild" runat="server" CssClass="classrem" onclick="REchildcheckAll(this);" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="&nbsp;&nbsp;&nbsp;&nbsp;Escalation" ItemStyle-Width="50">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="escalationcheckAll" Text="Escalation" runat="server" onclick="EScheckAll(this);" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="eschild" CssClass="classescal" runat="server" onclick="ESchildcheckAll(this);" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle CssClass="clsROWgrid" />
                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                    <HeaderStyle BackColor="#ECF0F1" />
                                                    <PagerSettings Visible="false" />
                                                    <PagerTemplate>
                                                    </PagerTemplate>
                                                    <EmptyDataTemplate>
                                                        No Records Found.
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </div>

                                            <div class="col-md-12 colpadding0">
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 30%;">
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 30%;">
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%; text-align: right;">
                                                    <asp:Button Text="Send Mail" runat="server" ID="btnReminderEscalation" Style="display: none;" OnClick="btnReminderEscalation_Click"
                                                        CssClass="btn btn-primary" />
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="ComplianceDetails" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">

                                    <div class="panel panel-default" style="margin-bottom: 1px;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails">
                                                <h2>Audit Customer data Request Details</h2>

                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="collapseComplianceDetails" class="panel-collapse collapse in">
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                            <div class="col-md-12 colpadding0">
                                                <div class="col-md-1 colpadding0" style="margin-right: 5px;">
                                                    <p style="color: #999; margin-top: 5px; margin-right: 5px;">Audit Name : </p>
                                                </div>
                                                <div class="col-md-8 colpadding0" style="margin-right: 5px;">
                                                    <p style="color: #999; margin-top: 5px; margin-right: 5px;">
                                                        <asp:Label ID="lblaname" runat="server" Text="Label"></asp:Label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 colpadding0">
                                                <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 11%">
                                                    <div class="col-md-2 colpadding0" style="margin-right: 24px;">
                                                        <p style="color: #999; margin-top: 5px; margin-right: 5px;">Show </p>
                                                    </div>
                                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; height: 32px; float: left"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                        <asp:ListItem Text="5" />
                                                        <asp:ListItem Text="10" />
                                                        <asp:ListItem Text="20" Selected="True" />
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 17%;">
                                                    <asp:DropDownListChosen runat="server" ID="ddlProcess1" DataPlaceHolder="Select Process" Width="93%"
                                                        AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15 select_location"
                                                        Style="float: left; width: 90%;" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddlProcess1_SelectedIndexChanged">
                                                    </asp:DropDownListChosen>
                                                </div>

                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 16%;">
                                                    <asp:DropDownListChosen runat="server" ID="ddlSubProcess1" DataPlaceHolder="Select Sub Process" Width="93%"
                                                        AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                        class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlSubProcess1_SelectedIndexChanged">
                                                    </asp:DropDownListChosen>
                                                </div>

                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 17%">
                                                    <asp:DropDownListChosen runat="server" ID="ddlPerformerFilter" class="form-control m-bot15"
                                                        Width="93%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                        AutoPostBack="true" DataPlaceHolder="Auditee"
                                                        OnSelectedIndexChanged="ddlPerformerFilter_SelectedIndexChanged">
                                                    </asp:DropDownListChosen>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please select auditee"
                                                        ControlToValidate="ddlPerformerFilter" runat="server"
                                                        ValidationGroup="ComplianceValidationGroup1"
                                                        Display="None" />
                                                    <asp:CompareValidator ID="CompareValidator15" ErrorMessage="Please select auditee"
                                                        ControlToValidate="ddlPerformerFilter" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                        ValidationGroup="ComplianceValidationGroup1" Display="None" />

                                                </div>

                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 17%">
                                                    <asp:DropDownListChosen runat="server" ID="ddlReviewerFilter" class="form-control m-bot15"
                                                        Width="92%" Height="32px"
                                                        AutoPostBack="true" DataPlaceHolder="Reporting Manager" AllowSingleDeselect="false"
                                                        DisableSearchThreshold="3"
                                                        OnSelectedIndexChanged="ddlReviewerFilter_SelectedIndexChanged">
                                                    </asp:DropDownListChosen>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please select reporting manager"
                                                        ControlToValidate="ddlReviewerFilter" runat="server"
                                                        ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                    <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select reporting manager"
                                                        ControlToValidate="ddlReviewerFilter" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                                        ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                </div>

                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%">
                                                    <asp:TextBox runat="server" ID="txtStartDateKickoff" AutoComplete="Off"
                                                        AutoPostBack="true" class="form-control" Style="width: 60%;"
                                                        placeholder="Timeline" OnTextChanged="txtStartDateKickoff_TextChanged" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please enter timeline."
                                                        ControlToValidate="txtStartDateKickoff"
                                                        runat="server" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                </div>

                                                <div class="col-md-2 colpadding0 entrycount" style="margin-top: 10px; width: 3%">
                                                    <img id="lnkAddNewUser" style="float: right;" src="../../Images/add_icon_new.png"
                                                        onclick="OpenUserPopup()" data-toggle="tooltip" data-placement="top" alt="Add New User" title="Add New User" />
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 colpadding0">
                                                &nbsp;
                                                <asp:GridView runat="server" ID="grdProcessAudit" AutoGenerateColumns="false" GridLines="None" PageSize="20"
                                                    AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                                    OnRowDataBound="grdProcessAudit_RowDataBound"
                                                    OnRowCommand="grdProcessAudit_RowCommand" ShowHeaderWhenEmpty="true">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="AuditID" Visible="false">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                                    <asp:Label runat="server" ID="lblAuditID" Style="display: none;" Text='<%#Eval("AuditID") %>'></asp:Label>
                                                                    <asp:Label runat="server" ID="lblatbdID" Style="display: none;" Text='<%#Eval("ATBDID") %>'></asp:Label>
                                                                    <asp:Label runat="server" ID="lblChecklistid" Style="display: none;" Text='<%#Eval("ChecklistID") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Audit Step">
                                                            <ItemTemplate>
                                                                <div class="text_NlinesusingCSS" style="width: 200px;">
                                                                    <asp:Label runat="server" ID="lblActivityTobeDone" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ActivityTobeDone") %>' ToolTip='<%# Eval("ActivityTobeDone") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Document Requried">
                                                            <ItemTemplate>
                                                                <div class="text_NlinesusingCSS" style="width: 200px;">
                                                                    <asp:Label runat="server" ID="lblChecklistDocument" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ChecklistDocument") %>' ToolTip='<%# Eval("ChecklistDocument") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Auditee">

                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlPerformer" class="form-control m-bot15" Width="180"
                                                                    DataPlaceHolder="Auditee" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblPerformerName" runat="server" Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Reporting Manager">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlReviewer" runat="server" Width="180px"
                                                                    class="form-control m-bot15" DataPlaceHolder="Reporting Manager">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblReviewerName" runat="server" Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="TimeLine">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtStartDate" class="form-control" Style="width: 110px; text-align: center;" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStatus" runat="server" Text='<%# getstatus((long)Eval("AuditID"),(long)Eval("ATBDID"),(int)Eval("ChecklistID")) %>' Width="25px" Height="23px" BackColor=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_SCHEDULE"
                                                                    CommandArgument='<%# Eval("AuditID") +"," + Eval("ATBDID")+","+Eval("ChecklistID")  %>'
                                                                    Visible='<%# CheckActionButtonEnable((long)Eval("AuditID"),(long)Eval("ATBDID"),(int)Eval("ChecklistID")) %>'>                                                           
                                                                    <img src="../../Images/view-icon-new.png" data-toggle="tooltip" data-placement="top"  title="View Documents" 
                                                                        /></asp:LinkButton>

                                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Add_CheckList"
                                                                    CommandArgument='<%# Eval("AuditID") +"," + Eval("ATBDID")+","+Eval("ChecklistID")  %>'>                                                           
                                                                    <img src="../../Images/add_icon_new.png" data-toggle="tooltip" data-placement="top"  title="Add Document Name" /></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                    <RowStyle CssClass="clsROWgrid" />
                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                    <HeaderStyle BackColor="#ECF0F1" />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                    <EmptyDataTemplate>
                                                        No Record Found
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </div>

                                            <div class="col-md-12 colpadding0">

                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 40%">
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%">
                                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click"
                                                        ValidationGroup="ComplianceValidationGroup1" CssClass="btn btn-primary" />
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 31%">
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 4%">
                                                    <p style="color: #999; margin-top: 5px; margin-right: 5px;">Page </p>
                                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 5%">
                                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" AllowSingleDeselect="false"
                                                        class="form-control m-bot15" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                                    </asp:DropDownListChosen>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 colpadding0">
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 40%">
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%">
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 25%">
                                                </div>
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%">
                                                    <div style="background-color: orange; border: 1px solid #4a8407; width: 50px; height: 20px; float: left" data-toggle="tooltip" data-placement="top" title="Resubmision of rejected "></div>
                                                    <div style="background-color: red; border: 1px solid #ba8b00; width: 50px; margin-left: 10px; height: 20px; float: left;" data-toggle="tooltip" data-placement="top" title="Rejected"></div>
                                                    <div style="background-color: green; border: 1px solid #b65454; width: 50px; margin-left: 10px; height: 20px; float: left;" data-toggle="tooltip" data-placement="top" title="Approved"></div>
                                                </div>
                                            </div>
                                            <div style="display: none">
                                                <asp:LinkButton ID="btnHiddenUpdatUser" runat="server" OnClick="btnHiddenUpdatUser_Click" />
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <%--<asp:PostBackTrigger ControlID="btnHiddenUpdatUser" />--%>
                </Triggers>
            </asp:UpdatePanel>
        </div>


        <div class="modal fade" id="divPREDOCShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="PreDOCshowdetails" src="about:blank" width="95%" height="100%" frameborder="0" style="margin-left: 25px;"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="AddUserModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog p0" style="width: 50%; overflow: hidden;">
                <div class="modal-content">
                    <div class="modal-header" style="padding: 30px !important;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="modal-header-custom" id="Usermmodel">
                            Add User</label>
                    </div>
                    <div class="modal-body Dashboard-white-widget" style="width: 100%; margin-top: -2px;">
                        <iframe src="about:blank" id="IUser" frameborder="0" runat="server" width="100%" height="400px"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="divCheckListAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-md-12 colpadding0">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ComplianceValidationGroup1" />
                                    <asp:CustomValidator ID="cvChecklist" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup1" Display="None" class="alert alert-block alert-danger fade in" />
                                    <asp:Label ID="Label1" runat="server" Style="color: Red"></asp:Label>
                                </div>
                                <div class="row">
                                    <asp:Label runat="server" Text="Document Name" Style="float: left; width: 20%; color: #999;"></asp:Label>
                                    <asp:TextBox runat="server" ID="tbxCheckListName" Style="float: right; width: 75%" CssClass="form-control" />
                                </div>
                                <div class="row">
                                    <asp:Button runat="server" ID="btnAddCheckList" CssClass="btn btn-primary" Text="Save" AutoPostback="false" OnClick="btnAddCheckList_Click" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            function CloseUserPage() {                
                $('#AddUserModelPopup').modal('hide');
                $(document).ready(() => {
                    document.getElementById('<%= btnHiddenUpdatUser.ClientID %>').click();
                })            
            }
        </script>
    </form>
</body>
</html>
