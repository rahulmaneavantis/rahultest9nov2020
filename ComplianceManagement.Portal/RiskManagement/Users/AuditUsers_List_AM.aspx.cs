﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using System.ComponentModel;
using System.Collections;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Users
{
    public partial class AuditUsers_List_AM : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomers();
                BindUsers();
                Session["CurrentRole"] = AuthenticationHelper.Role;
                Session["CurrentUserId"] = AuthenticationHelper.UserID;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    divCustomerfilter.Visible = false;
                //}
                //else
                //{
                //    divCustomerfilter.Visible = true;
                //}
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    btnAddUser.Visible = false;
                }
                ViewState["AssignedCompliancesID"] = null;
                //bindPageNumber(); //DisableAddbutton();
            }
            udcInputForm_AM.OnSaved += (inputForm, args) => { BindUsers(); };
        }

        public void DisableAddbutton()
        {
            if (AuthenticationHelper.Role.Equals("CADMN"))
            {
                bool Result = ICIAManagement.ServiceProviderUserLimit(AuthenticationHelper.ServiceProviderID, AuthenticationHelper.CustomerID);

                if (!Result)
                {
                    UserTooltip.Visible = true;
                    btnAddUser.Visible = false;
                    //btnAddUser.Attributes["disabled"] = "disabled";
                    //btnAddUser.ToolTip = "Facility to create more users is not available in the Free version. Kindly contact Avantis to activate the same.";
                }
                else
                {
                    UserTooltip.Visible = false;
                    btnAddUser.Visible = true;
                    //btnAddUser.Attributes.Remove("disabled");
                }
            }
            else
            {
                UserTooltip.Visible = false;
            }
        }


        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdUser.PageIndex = chkSelectedPage - 1;
            grdUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindUsers();
        }

        #region user Detail

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                string Type = CustomerManagementRisk.GetAuditHeadOrManagerid(AuthenticationHelper.UserID);
                if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                {
                    if (ddlCustomerList.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                    }
                }
                if (customerID != -1)
                {
                    if (Type == "AH" || Type == "AM")
                    {
                        // var uselist = UserManagementRisk.GetAllUserCustomerWise(customerID, Convert.ToInt32(AuthenticationHelper.UserID), tbxFilter.Text);
                        var uselist = UserManagement.GetAllUser(customerID, tbxFilter.Text);
                        grdUser.DataSource = uselist;
                        Session["TotalRows"] = uselist.Count;
                        grdUser.DataBind();
                    }
                    else
                    {
                        if (UserManagementRisk.GetUsrIsCompnyAdmin(AuthenticationHelper.UserID))
                        {
                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var uselist = UserManagement.GetAllUser(customerID, tbxFilter.Text);
                            //uselist = uselist.Where(e => e.ServiceProviderID == AuthenticationHelper.ServiceProviderID && e.IsServiceProvider == true).ToList();
                            grdUser.DataSource = uselist;
                            Session["TotalRows"] = uselist.Count;
                            grdUser.DataBind();
                            ddlCustomerList.Visible = false;
                        }
                    }
                }
                else
                {
                    grdUser.DataSource = null;
                    Session["TotalRows"] = 0;
                    grdUser.DataBind();
                }
                bindPageNumber();
                upUserList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdUser_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string customerName = string.Empty;
                int customerId = -1;
                if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                {
                    if (ddlCustomerList.SelectedValue != "-1")
                    {
                        customerName = ddlCustomerList.SelectedItem.Text.ToString();
                        customerId = Convert.ToInt32(ddlCustomerList.SelectedValue);
                    }
                }
                int userID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_USER"))
                {
                    udcInputForm_AM.EditUserInformation(userID, customerName, customerId);
                }
                else if (e.CommandName.Equals("DELETE_USER"))
                {
                    bool checkAssignedOrNot = UserManagementRisk.CheckUserAssignOrNot(userID);
                    if (!checkAssignedOrNot)
                    {
                        if (UserManagementRisk.CheckAssignedUserCount(userID))
                        {
                            UserManagement.Delete(userID);
                            UserManagementRisk.Delete(userID);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Audits are assigned to user, please re-assign to other user.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Audits are assigned to user, please re-assign to other user.');", true);
                    }
                    BindUsers();
                }
                else if (e.CommandName.Equals("CHANGE_STATUS"))
                {
                    if (UserManagement.IsActive(userID) && UserManagement.HasCompliancesAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deactivated. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                    else
                    {
                        UserManagement.ToggleStatus(userID);
                        UserManagementRisk.ToggleStatus(userID);

                    }
                    BindUsers();
                }
                else if (e.CommandName.Equals("RESET_PASSWORD"))
                {
                    User user = UserManagement.GetByID(userID);
                    mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                    string passwordText = Util.CreateRandomPassword(10);
                    user.Password = Util.CalculateMD5Hash(passwordText);
                    mstuser.Password = Util.CalculateMD5Hash(passwordText);

                    // added by sudarshan for comman notification
                    int customerID = -1;
                    string ReplyEmailAddressName = "";
                    if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                    {
                        ReplyEmailAddressName = "Avantis";
                    }
                    else
                    {
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                    }
                    string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]), ReplyEmailAddressName);
                    bool result = UserManagement.ChangePassword(user, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                    bool result1 = UserManagementRisk.ChangePassword(mstuser, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                    if (result && result1)
                    {
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<string>(new string[] { user.Email }), null, null, "Avantis Audit Product - account password changed", message);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Password reset successfully.');", true);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
                else if (e.CommandName.Equals("MODIFY_AUDIT_ASSIGNMENT"))
                {
                    lblReAssign.Text = string.Empty;
                    ViewState["ReAssignID"] = userID;
                    User user = UserManagement.GetByID(userID);
                    lblReAssign.Text = user.FirstName + " " + user.LastName;
                    BindNewUserList(user.CustomerID ?? -1, ddlREAssignUser);
                    ReAssign.Update();
                    BindReAssingGrid();
                    GetPageDisplaySummaryReAssign();
                    BindProcess();
                }
                else if (e.CommandName.Equals("UNLOCK_USER"))
                {
                    UserManagement.WrongAttemptCountUpdate(userID);
                    UserManagementRisk.WrongAttemptCountUpdate(userID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "Unloack", "alert('User unlocked successfully.');", true);
                    BindUsers();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindNewUserList(int customerID, DropDownList ddllist)
        {
            try
            {
                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";
                ddllist.DataSource = UserManagement.GetAllByCustomerID(customerID, null);
                ddllist.DataBind();
                ddllist.Items.Insert(0, new ListItem("Select user", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUser.PageIndex = e.NewPageIndex;
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upUserList_Load(object sender, EventArgs e)
        {
            try
            {
                // ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComboboxForAssignmentsDialog", "initializeComboboxForAssignmentsDialog();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdUser_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                var users = UserManagement.GetAllUser(customerID, tbxFilter.Text);
                grdUser.DataSource = users;
                Session["TotalRows"] = users.Count;
                grdUser.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int id = Convert.ToInt32(((GridView)sender).DataKeys[e.Row.RowIndex].Value);
                    LinkButton lbtnDelete = (LinkButton)e.Row.FindControl("lbtnDelete");
                    LinkButton lbtnChangeStatus = (LinkButton)e.Row.FindControl("lbtnChangeStatus");
                    LinkButton lbtnReassignment = (LinkButton)e.Row.FindControl("lbtnReassignment");

                    if (UserManagement.GetByID(id).RoleID == 1)
                    {
                        lbtnDelete.Visible = false;
                        lbtnChangeStatus.Enabled = false;
                        lbtnChangeStatus.Attributes.Add("href", "#");
                        lbtnChangeStatus.OnClientClick = string.Empty;
                    }

                    if (id == AuthenticationHelper.UserID)
                    {
                        lbtnChangeStatus.Enabled = false;
                        lbtnChangeStatus.Attributes.Add("href", "#");
                        lbtnChangeStatus.OnClientClick = string.Empty;
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "removeclass", "javascript:remoDisabledclass();", true);
                    }
                    var x = 0;
                    var y = 0;
                    var z = 0;

                    x = UserManagementRisk.GetAllProcessID(id).Count;
                    y = UserManagementRisk.GetAllImplementationID(id).Count;
                    z = UserManagementRisk.GetAllICFRID(id).Count;

                    if (x <= 0 && y <= 0 && z <= 0)
                    {
                        lbtnReassignment.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUser_RowCreated(object sender, GridViewRowEventArgs e)
        {
        }
        #endregion
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            string customerName = string.Empty;
            int customerId = -1;

            if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
            {
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerName = ddlCustomerList.SelectedItem.Text.ToString();
                    customerId = Convert.ToInt32(ddlCustomerList.SelectedValue);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myModel", "openModalControl();", true);
                    udcInputForm_AM.AddNewUser(customerId, customerName);
                }

                //else
                //{
                //    int customerID = -1;
                //    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                //    udcInputForm.AddNewUser(customerID);
                //}
            }
            else
            {
                #region code comment and new code added by sagar on 09-sept-2020
                //int customerID = -1;
                //customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                //customerName = UserManagementRisk.GetCustomerName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                //udcInputForm_AM.AddNewUser(customerID, customerName);

                Response.Write("<script>alert('Please assign the Customer');</script>");
                #endregion
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upModifyAssignment_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "removeclass", "javascript:remoDisabledclass()", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "CADMN")
                //{
                if (ddlCustomerList.SelectedItem.Text == "Select Customer")
                {
                    btnAddUser.Visible = false;
                }
                else
                {
                    btnAddUser.Visible = true;
                }
                //}
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private void BindCustomers()
        {
            try
            {
                long userId = Portal.Common.AuthenticationHelper.UserID;
                int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
                int RoleID = 0;
                //if (!Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
                //{
                var customerList = RiskCategoryManagement.GetCustomerListForDDLForMaster(userId, ServiceProviderID, RoleID);
                if (customerList.Count > 0)
                {
                    ddlCustomerList.DataTextField = "CustomerName";
                    ddlCustomerList.DataValueField = "CustomerId";
                    ddlCustomerList.DataSource = customerList;
                    ddlCustomerList.DataBind();
                    ddlCustomerList.Items.Insert(0, new ListItem("Select Customer", "-1"));

                    if (!string.IsNullOrEmpty(Convert.ToString(Session["customerId"])))
                    {
                        ddlCustomerList.SelectedValue = Convert.ToString(Session["customerId"]);
                    }
                    else
                    {
                        ddlCustomerList.SelectedIndex = 1;
                    }
                    Session["customerId"] = null;
                }
                else
                {
                    ddlCustomerList.DataSource = null;
                    ddlCustomerList.DataBind();
                    btnAddUser.Visible = false;
                    //cvDuplicateEntry.IsValid = false;
                    //cvDuplicateEntry.ErrorMessage = "You need to assign customer first.";
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('You need to assign customer first.')", true);
                }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool IsLocked(string emailID)
        {
            try
            {
                if (UserManagement.WrongAttemptCount(emailID.Trim()) >= 3)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                ////Reload the Grid
                BindUsers();
                //bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdUser.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void ddlPageSizeReAssignment_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        protected void grdReassign_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        protected void grdReassign_RowCreated(object sender, GridViewRowEventArgs e)
        {
        }
        protected void grdReassign_Sorting(object sender, GridViewSortEventArgs e)
        {
        }
        protected void ReAssign_Load(object sender, EventArgs e)
        {

            int UserID = -1;
            if (ViewState["ReAssignID"] != null)
            {
                UserID = Convert.ToInt32(ViewState["ReAssignID"]);
            }
            BindProcess(); BindReAssingGrid();
            lblReAssign.Text = string.Empty;
            User user = UserManagement.GetByID(UserID);
            lblReAssign.Text = user.FirstName + " " + user.LastName;
            BindNewUserList(user.CustomerID ?? -1, ddlREAssignUser);
            GetPageDisplaySummaryReAssign();
        }
        public void BindReAssingGrid()
        {
            int UserID = -1;
            if (ViewState["ReAssignID"] != null)
            {
                UserID = Convert.ToInt32(ViewState["ReAssignID"]);
            }
            int processid = -1;
            string processName = string.Empty;
            string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
            if (action.Equals("Process"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignData(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignData(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
            if (action.Equals("Implementation"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignDataImplement(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignDataImplement(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
            if (action.Equals("ICFR"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
        }
        protected void txtReAssignFilter_TextChanged(object sender, EventArgs e)
        {
            int UserID = -1;
            if (ViewState["ReAssignID"] != null)
            {
                UserID = Convert.ToInt32(ViewState["ReAssignID"]);
            }
            int processid = -1;
            string processName = string.Empty;
            processName = txtReAssignFilter.Text;
            string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
            if (action.Equals("Process"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignData(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignData(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
            if (action.Equals("Implementation"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignDataImplement(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignDataImplement(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
            if (action.Equals("ICFR"))
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    processid = Convert.ToInt32(ddlProcess.SelectedValue);
                }
                Session["TotalRowsReAssging"] = null;
                grdReassign.DataSource = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName);
                Session["TotalRowsReAssging"] = UserManagementRisk.GetAllReassignDataICFR(UserID, processid, processName).Count;
                grdReassign.DataBind();
            }
        }
        public void BindProcess()
        {
            try
            {
                int UserID = -1;
                if (ViewState["ReAssignID"] != null)
                {
                    UserID = Convert.ToInt32(ViewState["ReAssignID"]);
                }
                string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
                if (action.Equals("Audit"))
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ProcessId";
                    var ProcessList = UserManagementRisk.GetddlProcessDataProc(UserID);
                    ddlProcess.DataSource = ProcessList;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }
                if (action.Equals("Implementation"))
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ProcessId";
                    var ProcessList = UserManagementRisk.GetddlProcessDataImp(UserID);
                    ddlProcess.DataSource = ProcessList;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }
                if (action.Equals("ICFR"))
                {
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "ProcessId";
                    var ProcessList = UserManagementRisk.GetddlProcessDataICFR(UserID);
                    ddlProcess.DataSource = ProcessList;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindReAssingGrid();
        }
        protected void rbtEventReAssignment_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess(); BindReAssingGrid();
        }
        private void ReAssignCheckBoxValueSaved()
        {
            List<Tuple<int, int>> chkList = new List<Tuple<int, int>>();
            int ProceID = -1;
            foreach (GridViewRow gvrow in grdReassign.Rows)
            {
                ProceID = Convert.ToInt32(grdReassign.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkEvent")).Checked;
                int BranchID = Convert.ToInt32(((Label)gvrow.FindControl("lblBrachID")).Text);

                //Tuple<int, string>  = new Tuple<int, string>();
                if (ViewState["ReAssignedEventID"] != null)
                    chkList = (List<Tuple<int, int>>)ViewState["ReAssignedEventID"];

                var checkedData = chkList.Where(entry => entry.Item1 == ProceID && entry.Item2 == BranchID).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<int, int>(ProceID, BranchID));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["ReAssignedEventID"] = chkList;
        }
        protected void btnReassign_Click(object sender, EventArgs e)
        {
            try
            {
                string[] confirmValue = Request.Form["confirm_Event_value"].Split(',');

                if (confirmValue[confirmValue.Length - 1] == "Yes")
                {
                    ReAssignCheckBoxValueSaved();
                    List<Tuple<int, int>> chkList = (List<Tuple<int, int>>)ViewState["ReAssignedEventID"];
                    if (chkList != null)
                    {
                        string action = Convert.ToString(rbtEventReAssignment.SelectedValue);
                        if (action.Equals("Process"))
                        {
                            UserManagementRisk.UpdateReAssignedProcess(Convert.ToInt32(ViewState["ReAssignID"]), Convert.ToInt32(ddlREAssignUser.SelectedValue), chkList);
                        }
                        if (action.Equals("Implementation"))
                        {
                            UserManagementRisk.UpdateReAssignedImplementation(Convert.ToInt32(ViewState["ReAssignID"]), Convert.ToInt32(ddlREAssignUser.SelectedValue), chkList);
                        }
                        if (action.Equals("ICFR"))
                        {
                            UserManagementRisk.UpdateReAssignedICFR(Convert.ToInt32(ViewState["ReAssignID"]), Convert.ToInt32(ddlREAssignUser.SelectedValue), chkList);
                        }
                        BindReAssingGrid();
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divReAssign\").dialog('close')", true);
                        ViewState["ReAssignedEventID"] = null;
                    }
                    else
                    {
                        CustomModifyAsignment.IsValid = false;
                        CustomModifyAsignment.ErrorMessage = "Please select at list one event for proceed.";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAssignEvent\").dialog('close')", true);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lBPreviousReAssign_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(SelectedPageNoReAssign.Text) > 1)
                {
                    SelectedPageNoReAssign.Text = (Convert.ToInt32(SelectedPageNoReAssign.Text) - 1).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindReAssingGrid();
                GetPageDisplaySummaryReAssign();
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNextReAssign_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNoReAssign.Text);

                if (currentPageNo < GetTotalPagesCountReAssign())
                {
                    SelectedPageNoReAssign.Text = (currentPageNo + 1).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                else
                {
                }
                BindReAssingGrid();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private void GetPageDisplaySummaryReAssign()
        {
            try
            {
                lTotalCountReAssign.Text = GetTotalPagesCountReAssign().ToString();

                if (lTotalCountReAssign.Text != "0")
                {
                    if (SelectedPageNoReAssign.Text == "")
                        SelectedPageNoReAssign.Text = "1";

                    if (SelectedPageNoReAssign.Text == "0")
                        SelectedPageNoReAssign.Text = "1";
                }
                else if (lTotalCountReAssign.Text == "0")
                {
                    SelectedPageNoReAssign.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetTotalPagesCountReAssign()
        {
            try
            {
                TotalRowsReAssign.Value = Session["TotalRowsReAssging"].ToString();
                int totalPages = Convert.ToInt32(TotalRowsReAssign.Value) / Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRowsReAssign.Value) % Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void ddlPageSizeReAssign_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNoReAssign.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNoReAssign.Text);

                if (currentPageNo <= GetTotalPagesCountReAssign())
                {
                    //SelectedPageNo.Text = (currentPageNo).ToString();
                    grdReassign.PageSize = Convert.ToInt32(ddlPageSizeReAssign.SelectedValue);
                    grdReassign.PageIndex = Convert.ToInt32(SelectedPageNoReAssign.Text) - 1;
                }
                //Reload the Grid
                BindReAssingGrid();
                GetPageDisplaySummaryReAssign();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }


    }
}