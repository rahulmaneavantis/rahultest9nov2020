﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
//using AuditManagement.Business;
//using AuditManagement.Business.Data;
//using AuditManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Users 
{
    public partial class UserList : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindCustomersADD();
                BindCustomersUpdate();               
                BindUsers();
                BindRoles();
                BindDepartment();
                Session["CurrentRole"] = AuthenticationHelper.Role;
                Session["CurrentUserId"] = AuthenticationHelper.UserID;              
                ViewState["AssignedCompliancesID"] = null;

            }
            //udcInputForm.OnSaved += (inputForm, args) => { BindUsers(); };
        }

        #region user Detail

        public void BindUsers()
        {
            try
            {
                //int customerID = -1;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                //}

                //if (ddlCustomerList.SelectedValue != "-1")
                //{
                //    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                //}

                //var uselist = UserManagement.GetAllUser(customerID, tbxFilter.Text);

                //if (ViewState["SortOrder"].ToString() == "Asc")
                //{
                //    if (ViewState["SortExpression"].ToString() == "FirstName")
                //    {
                //        uselist = uselist.OrderBy(entry => entry.FirstName).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "LastName")
                //    {
                //        uselist = uselist.OrderBy(entry => entry.LastName).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "Email")
                //    {
                //        uselist = uselist.OrderBy(entry => entry.Email).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "ContactNumber")
                //    {
                //        uselist = uselist.OrderBy(entry => entry.ContactNumber).ToList();
                //    }
                //    direction = SortDirection.Descending;
                //}
                //else
                //{
                //    if (ViewState["SortExpression"].ToString() == "FirstName")
                //    {
                //        uselist = uselist.OrderByDescending(entry => entry.FirstName).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "LastName")
                //    {
                //        uselist = uselist.OrderByDescending(entry => entry.LastName).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "Email")
                //    {
                //        uselist = uselist.OrderByDescending(entry => entry.Email).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "ContactNumber")
                //    {
                //        uselist = uselist.OrderByDescending(entry => entry.ContactNumber).ToList();
                //    }
                //    direction = SortDirection.Ascending;
                //}

                //List<object> dataSource = new List<object>();
                //foreach (mst_User user in uselist)
                //{
                //    dataSource.Add(new
                //    {
                //        user.ID,
                //        user.FirstName,
                //        user.LastName,
                //        user.Email,
                //        user.ContactNumber,
                //        Role = RoleManagement.GetByID(user.RoleID).Name,
                //        user.IsActive
                //    });
                //}
                //grdUser.DataSource = dataSource;
                //grdUser.DataBind();
                //upUserList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUser_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_USER"))
                {
                   EditUserInformation(userID);
                }
                else if (e.CommandName.Equals("DELETE_USER"))
                {
                    
                        //UserManagement.Delete(userID);
                    
                    BindUsers();
                }
                else if (e.CommandName.Equals("CHANGE_STATUS"))
                {
                    //if (UserManagement.IsActive(userID))
                    //{
                    //    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deactivated. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    //}
                    //else
                    //{
                    //    UserManagement.ToggleStatus(userID);
                    //}
                    BindUsers();
                }
                else if (e.CommandName.Equals("RESET_PASSWORD"))
                {
                    //mst_User user = UserManagement.GetByID(userID);
                    //string passwordText = Util.CreateRandomPassword(10);
                    //user.Password = Util.CalculateMD5Hash(passwordText);


                    // added by sudarshan for comman notification
                    //int customerID = -1;
                    //string ReplyEmailAddressName = "";
                    //if (AuthenticationHelper.Role == "SADMN")
                    //{
                    //    ReplyEmailAddressName = "Avantis";
                    //}
                    //else
                    //{
                    //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    //    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                    //}

                    //string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]), ReplyEmailAddressName);

                    //bool result = UserManagement.ChangePassword(user, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);

                    //if (result)
                    //{
                    //    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Password reset successfully.');", true);
                    //}
                    //else
                    //{
                    //    cvDuplicateEntry.IsValid = false;
                    //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    //}
                }                
                else if (e.CommandName.Equals("UNLOCK_USER"))
                {
                    //UserManagement.WrongAttemptCountUpdate(userID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "Unloack", "alert('User unlocked successfully.');", true);
                    BindUsers();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindNewUserList(int customerID, DropDownList ddllist)
        {
            //try
            //{
            //    ddllist.DataTextField = "Name";
            //    ddllist.DataValueField = "ID";

            //    ddllist.DataSource = UserManagement.GetAllByCustomerID(customerID, null);
            //    ddllist.DataBind();

            //    ddllist.Items.Insert(0, new ListItem("< Select user >", "-1"));
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }

        protected void grdUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUser.PageIndex = e.NewPageIndex;
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

     
        protected void grdUser_Sorting(object sender, GridViewSortEventArgs e)
        {
            //try
            //{

            //    int customerID = -1;
            //    if (AuthenticationHelper.Role == "CADMN")
            //    {
            //        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
            //    }

            //    if (ddlCustomerList.SelectedValue != "-1")
            //    {
            //        customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
            //    }

            //    var users = UserManagement.GetAllUser(customerID, tbxFilter.Text);
            //    List<object> dataSource = new List<object>();
            //    foreach (User user in users)
            //    {
            //        dataSource.Add(new
            //        {
            //            user.ID,
            //            user.FirstName,
            //            user.LastName,
            //            user.Email,
            //            user.ContactNumber1,
            //            Role = RoleManagement.GetByID(user.RoleID).Name,
            //            user.IsActive

            //        });
            //    }
            //    if (direction == SortDirection.Ascending)
            //    {
            //        dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
            //        direction = SortDirection.Descending;
            //        ViewState["SortOrder"] = "Asc";
            //        ViewState["SortExpression"] = e.SortExpression.ToString();
            //    }
            //    else
            //    {
            //        dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
            //        direction = SortDirection.Ascending;
            //        ViewState["SortOrder"] = "Desc";
            //        ViewState["SortExpression"] = e.SortExpression.ToString();
            //    }

            //    foreach (DataControlField field in grdUser.Columns)
            //    {
            //        if (field.SortExpression == e.SortExpression)
            //        {
            //            ViewState["SortIndex"] = grdUser.Columns.IndexOf(field);
            //        }
            //    }

            //    grdUser.DataSource = dataSource;
            //    grdUser.DataBind();


            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }

        protected void grdUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //int id = Convert.ToInt32(((GridView)sender).DataKeys[e.Row.RowIndex].Value);

                    //LinkButton lbtnDelete = (LinkButton)e.Row.FindControl("lbtnDelete");                    
                    //LinkButton lbtnChangeStatus = (LinkButton)e.Row.FindControl("lbtnChangeStatus");
                    

                    //if (UserManagement.GetByID(id).RoleID == 1)
                    //{
                    //    lbtnDelete.Visible = false;                        
                    //    lbtnChangeStatus.Enabled = false;
                    //    lbtnChangeStatus.Attributes.Add("href", "#");
                    //    lbtnChangeStatus.OnClientClick = string.Empty;
                    //}
                    //if (id == AuthenticationHelper.UserID)
                    //{
                    //    lbtnChangeStatus.Enabled = false;
                    //    lbtnChangeStatus.Attributes.Add("href", "#");
                    //    lbtnChangeStatus.OnClientClick = string.Empty;
                    //}                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUser_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        #endregion

        
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            AddNewUser();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open')", true);
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        
        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomersADD()
        {
            try
            {
                //ddlCustomer.DataTextField = "Name";
                //ddlCustomer.DataValueField = "ID";

                //int customerID = -1;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                //}

                //ddlCustomer.DataSource = CustomerManagement.GetAll(customerID);
                //ddlCustomer.DataBind();

                //if (AuthenticationHelper.Role != "CADMN")
                //    ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomersUpdate()
        {
            try
            {
                //ddlCustomerList.DataTextField = "Name";
                //ddlCustomerList.DataValueField = "ID";

                //ddlCustomerList.DataSource = CustomerManagement.GetAll();
                //ddlCustomerList.DataBind();

                //ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //try
            //{
            //    CheckBoxValueSaved();
            //    grdComplianceInstances.PageIndex = e.NewPageIndex;
            //    BindComplianceInstances(Convert.ToInt32(ViewState["UserID"]));
            //    AssignCheckBoxexValue();
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdComplianceInstances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

               
                //var assignmentList = Business.ComplianceManagement.GetAllAssignedInstances(userID: Convert.ToInt32(ViewState["UserID"]), filter: txtAssigmnetFilter.Text).OrderBy(entry => entry.Branch).ToList();
                //if (direction == SortDirection.Ascending)
                //{
                //    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Descending;
                //    ViewState["SortOrder"] = "Asc";
                //    ViewState["SortExpression"] = e.SortExpression.ToString();
                //}
                //else
                //{
                //    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Ascending;
                //    ViewState["SortOrder"] = "Desc";
                //    ViewState["SortExpression"] = e.SortExpression.ToString();
                //}

                //foreach (DataControlField field in grdComplianceInstances.Columns)
                //{
                //    if (field.SortExpression == e.SortExpression)
                //    {
                //        ViewState["InstancesSortIndex"] = grdComplianceInstances.Columns.IndexOf(field);
                //    }
                //}

                //grdComplianceInstances.DataSource = assignmentList;
                //grdComplianceInstances.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceInstances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["InstancesSortIndex"]);
                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void CheckBoxValueSaved()
        {
            //List<Tuple<int, string>> chkList = new List<Tuple<int, string>>();
            //int index = -1;
            //foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
            //{
            //    index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
            //    bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;
            //    string role = ((Label)gvrow.FindControl("lblRole")).Text;

            //    //Tuple<int, string>  = new Tuple<int, string>();
            //    if (ViewState["AssignedCompliancesID"] != null)
            //        chkList = (List<Tuple<int, string>>)ViewState["AssignedCompliancesID"];

            //    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
            //    if (result)
            //    {
            //        if (checkedData == null)
            //            chkList.Add(new Tuple<int, string>(index, role));
            //    }
            //    else
            //        chkList.Remove(checkedData);
            //}
            //if (chkList != null && chkList.Count > 0)
            //    ViewState["AssignedCompliancesID"] = chkList;
        }

        private void AssignCheckBoxexValue()
        {

            //List<Tuple<int, string>> chkList = (List<Tuple<int, string>>)ViewState["AssignedCompliancesID"];
            //if (chkList != null && chkList.Count > 0)
            //{
            //    foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
            //    {
            //        int index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
            //        string role = ((Label)gvrow.FindControl("lblRole")).Text;
            //        var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
            //        if (chkList.Contains(checkedData))
            //        {
            //            CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
            //            myCheckBox.Checked = true;
            //        }
            //    }
            //}
        }

        protected void rbtModifyAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    string action = Convert.ToString(rbtModifyAction.SelectedValue);
            //    if (action.Equals("Delete"))
            //    {
            //        divNewUser.Visible = false;
            //    }
            //    else
            //    {
            //        divNewUser.Visible = true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //}
        }

        protected void txtAssigmnetFilter_TextChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    grdUser.PageIndex = 0;
            //    int userID = Convert.ToInt32(ViewState["UserID"].ToString());
            //    //BindComplianceInstances(userID);
            //    if (rbtSelectionType.SelectedValue == "0")
            //    {
            //        BindComplianceInstances(userID);
            //    }
            //    else
            //    {
            //        BindInternalComplianceInstances(userID);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }

        private void BindEventInstances(int userID)
        {
            try
            {
                
                //var eMgmt = EventManagement.GetAllAssignedInstancesByUser(userID, filter: txtAssignEventUser.Text).OrderBy(entry => entry.CustomerBranchName).ToList();

                //if (ViewState["SortOrder"].ToString() == "Asc")
                //{
                //    if (ViewState["SortExpression"].ToString() == "CustomerBranchName")
                //    {
                //        eMgmt = eMgmt.OrderBy(entry => entry.CustomerBranchName).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "Name")
                //    {
                //        eMgmt = eMgmt.OrderBy(entry => entry.Name).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "Role")
                //    {
                //        eMgmt = eMgmt.OrderBy(entry => entry.Role).ToList();
                //    }
                //    direction = SortDirection.Descending;
                //}
                //else
                //{

                //    if (ViewState["SortExpression"].ToString() == "CustomerBranchName")
                //    {
                //        eMgmt = eMgmt.OrderByDescending(entry => entry.CustomerBranchName).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "Name")
                //    {
                //        eMgmt = eMgmt.OrderByDescending(entry => entry.Name).ToList();
                //    }
                //    if (ViewState["SortExpression"].ToString() == "Role")
                //    {
                //        eMgmt = eMgmt.OrderByDescending(entry => entry.Role).ToList();
                //    }
                //    direction = SortDirection.Ascending;
                //}

                //grdAssignedEventInstance.DataSource = eMgmt;
                //grdAssignedEventInstance.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

       

        protected void grdAssignedEventInstance_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                //var assignmentList = EventManagement.GetAllAssignedInstancesByUser(Convert.ToInt32(ViewState["UserID"])).OrderBy(entry => entry.CustomerBranchName).ToList();
                //if (direction == SortDirection.Ascending)
                //{
                //    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Descending;
                //    ViewState["SortOrder"] = "Asc";
                //    ViewState["SortExpression"] = e.SortExpression.ToString();
                //}
                //else
                //{
                //    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Ascending;
                //    ViewState["SortOrder"] = "Desc";
                //    ViewState["SortExpression"] = e.SortExpression.ToString();
                //}

                //foreach (DataControlField field in grdAssignedEventInstance.Columns)
                //{
                //    if (field.SortExpression == e.SortExpression)
                //    {
                //        ViewState["InstancesSortIndex"] = grdAssignedEventInstance.Columns.IndexOf(field);
                //    }
                //}

                //grdAssignedEventInstance.DataSource = assignmentList;
                //grdAssignedEventInstance.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAssignedEventInstance_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["InstancesSortIndex"]);
                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAssignedEventInstance_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //try
            //{
            //    EventCheckBoxValueSaved();
            //    grdAssignedEventInstance.PageIndex = e.NewPageIndex;
            //    BindEventInstances(Convert.ToInt32(ViewState["UserID"]));
            //    EventSavedCheckBoxexValue();
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }

        private void EventCheckBoxValueSaved()
        {
            //List<Tuple<int, int>> chkList = new List<Tuple<int, int>>();
            //int index = -1;
            //foreach (GridViewRow gvrow in grdAssignedEventInstance.Rows)
            //{
            //    index = Convert.ToInt32(grdAssignedEventInstance.DataKeys[gvrow.RowIndex].Value);
            //    bool result = ((CheckBox)gvrow.FindControl("chkEvent")).Checked;
            //    int role = Convert.ToInt32(((Label)gvrow.FindControl("lblRoleId")).Text);

            //    //Tuple<int, string>  = new Tuple<int, string>();
            //    if (ViewState["AssignedEventID"] != null)
            //        chkList = (List<Tuple<int, int>>)ViewState["AssignedEventID"];

            //    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2 == role).FirstOrDefault();
            //    if (result)
            //    {
            //        if (checkedData == null)
            //            chkList.Add(new Tuple<int, int>(index, role));
            //    }
            //    else
            //        chkList.Remove(checkedData);
            //}
            //if (chkList != null && chkList.Count > 0)
            //    ViewState["AssignedEventID"] = chkList;
        }

        private void EventSavedCheckBoxexValue()
        {

            //List<Tuple<int, int>> chkList = (List<Tuple<int, int>>)ViewState["AssignedEventID"];
            //if (chkList != null && chkList.Count > 0)
            //{
            //    foreach (GridViewRow gvrow in grdAssignedEventInstance.Rows)
            //    {
            //        int index = Convert.ToInt32(grdAssignedEventInstance.DataKeys[gvrow.RowIndex].Value);
            //        int role = Convert.ToInt32(((Label)gvrow.FindControl("lblRoleId")).Text);
            //        var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2 == role).FirstOrDefault();
            //        if (chkList.Contains(checkedData))
            //        {
            //            CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkEvent");
            //            myCheckBox.Checked = true;
            //        }
            //    }
            //}
        }

        protected void btnAssignEvents_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    string[] confirmValue = Request.Form["confirm_Event_value"].Split(',');

            //    if (confirmValue[confirmValue.Length - 1] == "Yes")
            //    {
            //        EventCheckBoxValueSaved();
            //        List<Tuple<int, int>> chkList = (List<Tuple<int, int>>)ViewState["AssignedEventID"];
            //        if (chkList != null)
            //        {
            //            string action = Convert.ToString(rbtEventModifyAssignment.SelectedValue);
            //            if (action.Equals("Delete"))
            //            {

            //                Business.EventManagement.DeleteEventAssignment(Convert.ToInt32(ViewState["UserID"]), chkList);
            //                var oldUser = UserManagement.GetByID(Convert.ToInt32(ViewState["UserID"]));
            //                if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
            //                {
            //                    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
            //                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ExcludeEvents
            //                                        .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
            //                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
            //                                        .Replace("@From", ReplyEmailAddressName);

            //                    new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for excluding events.", message); }).Start();
            //                }


            //            }
            //            else
            //            {
            //                EventManagement.ReassignEvents(Convert.ToInt32(ViewState["UserID"]), Convert.ToInt32(ddlAllUsers.SelectedValue), chkList, AuthenticationHelper.UserID);
            //                var newUser = UserManagement.GetByID(Convert.ToInt32(ddlAllUsers.SelectedValue));
            //                var oldUser = UserManagement.GetByID(Convert.ToInt32(ViewState["UserID"]));
            //                if (CustomerManagement.IsCustomerActive(Convert.ToInt64(newUser.CustomerID)))
            //                {
            //                    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(newUser.CustomerID)).Name;
            //                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignEventNewOwner
            //                                        .Replace("@User", newUser.FirstName + " " + newUser.LastName)
            //                                        .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
            //                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
            //                                        .Replace("@From", ReplyEmailAddressName);

            //                    new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning events.", message); }).Start();
            //                }


            //                if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
            //                {
            //                    string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
            //                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignEventOldOwner
            //                                        .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
            //                                        .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
            //                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
            //                                        .Replace("@From", ReplyEmailAddressName);

            //                    new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning events.", message); }).Start();
            //                }
            //            }

            //            BindUsers();
            //            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAssignEvent\").dialog('close')", true);
            //            ViewState["AssignedEventID"] = null;
            //        }
            //        else
            //        {
            //            CustomModifyAsignment.IsValid = false;
            //            CustomModifyAsignment.ErrorMessage = "Please select at list one event for proceed.";
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAssignEvent\").dialog('close')", true);
            //    }

            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }

        protected void txtAssignEventsFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                int userID = Convert.ToInt32(ViewState["UserID"].ToString());
                BindEventInstances(userID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool IsLocked(string emailID)
        {
            //try
            //{
            //    if (UserManagement.WrongAttemptCount(emailID.Trim()) >= 3)
            //        return true;
            //    else
            //        return false;
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
            return false;
        }

        protected void rbtEventModifyAssignment_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    string action = Convert.ToString(rbtEventModifyAssignment.SelectedValue);
            //    if (action.Equals("Delete"))
            //    {
            //        divEventNewUser.Visible = false;
            //    }
            //    else
            //    {
            //        divEventNewUser.Visible = true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //}
        }
        protected void rbtSelectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int userID = Convert.ToInt32(ViewState["UserID"].ToString());
            //if (rbtSelectionType.SelectedValue == "0")
            //{
            //    BindComplianceInstances(userID);
            //}
            //else
            //{
            //    BindInternalComplianceInstances(userID);
            //}
        }

        //internal

        private void InternalCheckBoxValueSaved()
        {
            //List<Tuple<int, string>> chkIList = new List<Tuple<int, string>>();
            //int index = -1;
            //foreach (GridViewRow gvrow in grdInternalComplianceInstances.Rows)
            //{
            //    index = Convert.ToInt32(grdInternalComplianceInstances.DataKeys[gvrow.RowIndex].Value);
            //    bool result = ((CheckBox)gvrow.FindControl("chkICompliances")).Checked;
            //    string role = ((Label)gvrow.FindControl("lblIRole")).Text;


            //    if (ViewState["AssignedInternalCompliancesID"] != null)
            //        chkIList = (List<Tuple<int, string>>)ViewState["AssignedInternalCompliancesID"];

            //    var checkedData = chkIList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
            //    if (result)
            //    {
            //        if (checkedData == null)
            //            chkIList.Add(new Tuple<int, string>(index, role));
            //    }
            //    else
            //        chkIList.Remove(checkedData);
            //}
            //if (chkIList != null && chkIList.Count > 0)
            //    ViewState["AssignedInternalCompliancesID"] = chkIList;
        }

        private void InternalAssignCheckBoxexValue()
        {

            //List<Tuple<int, string>> chkIList = (List<Tuple<int, string>>)ViewState["AssignedInternalCompliancesID"];
            //if (chkIList != null && chkIList.Count > 0)
            //{
            //    foreach (GridViewRow gvrow in grdInternalComplianceInstances.Rows)
            //    {
            //        int index = Convert.ToInt32(grdInternalComplianceInstances.DataKeys[gvrow.RowIndex].Value);
            //        string role = ((Label)gvrow.FindControl("lblIRole")).Text;
            //        var checkedIData = chkIList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role)).FirstOrDefault();
            //        if (chkIList.Contains(checkedIData))
            //        {
            //            CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkICompliances");
            //            myCheckBox.Checked = true;
            //        }
            //    }
            //}
        }

        protected void grdInternalComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //try
            //{
            //    InternalCheckBoxValueSaved();
            //    grdInternalComplianceInstances.PageIndex = e.NewPageIndex;
            //    //BindComplianceInstances(Convert.ToInt32(ViewState["UserID"]));
            //    BindInternalComplianceInstances(Convert.ToInt32(ViewState["UserID"]));
            //    InternalAssignCheckBoxexValue();
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }

        protected void grdInternalComplianceInstances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    //int sortColumnIndex = Convert.ToInt32(ViewState["InstancesSortIndex"]);
                    int sortColumnIndex = Convert.ToInt32(ViewState["InternalInstancesSortIndex"]);

                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdInternalComplianceInstances_Sorting(object sender, GridViewSortEventArgs e)
        {
            //try
            //{

            //    //var assignmentList = Business.ComplianceManagement.GetAllInstances(userID: Convert.ToInt32(ViewState["UserID"]), filter: txtAssigmnetFilter.Text).OrderBy(entry => entry.Branch).ToList();
            //    //var assignmentList = Business.ComplianceManagement.GetAllAssignedInstances(userID: Convert.ToInt32(ViewState["UserID"]), filter: txtAssigmnetFilter.Text).OrderBy(entry => entry.Branch).ToList();
            //    var InternalassignmentList = Business.InternalComplianceManagement.GetAllAssignedInstances(userID: Convert.ToInt32(ViewState["UserID"]), filter: txtAssigmnetFilter.Text).OrderBy(entry => entry.Branch).ToList();
            //    if (direction == SortDirection.Ascending)
            //    {
            //        InternalassignmentList = InternalassignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
            //        direction = SortDirection.Descending;
            //        ViewState["SortOrder"] = "Asc";
            //        ViewState["SortExpression"] = e.SortExpression.ToString();
            //    }
            //    else
            //    {
            //        InternalassignmentList = InternalassignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
            //        direction = SortDirection.Ascending;
            //        ViewState["SortOrder"] = "Desc";
            //        ViewState["SortExpression"] = e.SortExpression.ToString();
            //    }

            //    foreach (DataControlField field in grdInternalComplianceInstances.Columns)
            //    {
            //        if (field.SortExpression == e.SortExpression)
            //        {
            //            ViewState["InternalInstancesSortIndex"] = grdInternalComplianceInstances.Columns.IndexOf(field);
            //        }
            //    }

            //    grdInternalComplianceInstances.DataSource = InternalassignmentList;
            //    grdInternalComplianceInstances.DataBind();


            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }
        protected void upUsers_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox(1);", true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewEdit", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindRoles()
        {
            try
            {
                //ddlRole.DataTextField = "Name";
                //ddlRole.DataValueField = "ID";

                //var roles = RoleManagement.GetAll(false);
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    roles = roles.Where(entry => !entry.Code.Equals("SADMN")).ToList();
                //}
                //ddlRole.DataSource = roles.OrderBy(entry => entry.Name);
                //ddlRole.DataBind();

                //ddlRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //string roleCode = string.Empty;
                //var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                //if (role != null)
                //{
                //    roleCode = role.Code;
                //}
              
                //divCustomerBranch.Visible = roleCode.Equals("EXCT");
                //if (divCustomer.Visible && AuthenticationHelper.Role != "CADMN")
                //{
                //    ddlCustomer.SelectedValue = "-1";
                //    ddlCustomer_SelectedIndexChanged(null, null);
                //}
                //if (divCustomerBranch.Visible)
                //{
                //    ddlCustomer_SelectedIndexChanged(null, null);
                //    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewRoleChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                //}

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = string.Empty;
                BindCustomerBranches();
                BindDepartment();
                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewCustomerChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDepartment()
        {
            try
            {
                if (ddlCustomer.SelectedValue != "" && ddlCustomer.SelectedValue != null)
                {
                    ddlDepartment.DataTextField = "Name";
                    ddlDepartment.DataValueField = "ID";

                 
                    //ddlDepartment.DataSource = UserManagement.FillDepartment();
                    //ddlDepartment.DataBind();
                    //ddlDepartment.Items.Insert(0, new ListItem("< Select Department >", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Branch >";
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                if (ddlCustomer.SelectedValue != "" && ddlCustomer.SelectedValue != null)
                {


                    //tvBranches.Nodes.Clear();
                    //NameValueHierarchy branch = null;
                    //var branchs = CustomerBranchManagement.GetAllHierarchy(Convert.ToInt32(ddlCustomer.SelectedValue));
                    //if (branchs.Count > 0)
                    //{
                    //    branch = branchs[0];
                    //}
                    //tbxBranch.Text = "< Select Location >";
                    //List<TreeNode> nodes = new List<TreeNode>();
                    //BindBranchesHierarchy(null, branch, nodes);
                    //foreach (TreeNode item in nodes)
                    //{
                    //    tvBranches.Nodes.Add(item);
                    //}

                    //tvBranches.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        //{
        //    try
        //    {
        //        if (nvp != null)
        //        {
        //            foreach (var item in nvp.Children)
        //            {
        //                TreeNode node = new TreeNode(item.Name, item.ID.ToString());
        //                BindBranchesHierarchy(node, item, nodes);
        //                if (parent == null)
        //                {
        //                    nodes.Add(node);
        //                }
        //                else
        //                {
        //                    parent.ChildNodes.Add(node);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvEmailError.IsValid = false;
        //        cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //private void BindParameters(List<UserParameterValueInfo> userParameterValues = null)
        //{
        //    try
        //    {
        //        if (userParameterValues == null)
        //        {
        //            userParameterValues = new List<UserParameterValueInfo>();
        //            var userParameters = UserManagement.GetAllUserParameters();
        //            userParameters.ForEach(entry =>
        //            {
        //                userParameterValues.Add(new UserParameterValueInfo()
        //                {
        //                    UserID = -1,
        //                    ParameterID = entry.ID,
        //                    ValueID = -1,
        //                    Name = entry.Name,
        //                    DataType = (DataType)entry.DataType,
        //                    Length = (int)entry.Length,
        //                    Value = string.Empty
        //                });
        //            });
        //        }

        //        repParameters.DataSource = userParameterValues;
        //        repParameters.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvEmailError.IsValid = false;
        //        cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        public void AddNewUser()
        {
            try
            {
                ViewState["Mode"] = 0;

                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;                
                tbxEmail.Enabled = true;
                ddlCustomer.Enabled = true;

                ddlRole.SelectedValue = "-1";
                ddlRole_SelectedIndexChanged(null, null);

                //BindParameters();
                upUsers.Update();

                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open');", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void EditUserInformation(int userID)
        {
            try
            {

                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox(0);", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "disableCombobox", "disableCombobox();", true);
                ViewState["Mode"] = 1;
                ViewState["UserID"] = userID;

                //mst_User user = UserManagement.GetByID(userID);
                //List<UserParameterValueInfo> userParameterValues = UserManagement.GetParameterValuesByUserID(userID);

                //tbxFirstName.Text = user.FirstName;
                //tbxLastName.Text = user.LastName;
                //tbxDesignation.Text = user.Designation;
                //tbxEmail.Text = user.Email;
                //tbxContactNo.Text = user.ContactNumber;
                ////tbxUsername.Text = user.Username;
                //tbxAddress.Text = user.Address;

                ////tbxUsername.Enabled = false;
                //tbxEmail.Enabled = false;
                //ddlRole.Enabled = false;
                //tbxEmail.Enabled = true;

                //ddlDepartment.SelectedValue = user.DepartmentID.ToString();
                //ddlRole.SelectedValue = user.RoleID.ToString();
                //ddlRole_SelectedIndexChanged(null, null);
                //ddlRole.Attributes.Add("disabled", "disabled");
                //if (divCustomer.Visible && user.CustomerID.HasValue)
                //{
                //    ddlCustomer.SelectedValue = user.CustomerID.Value.ToString();
                //    ddlCustomer_SelectedIndexChanged(null, null);
                //}

              
                //if (divCustomerBranch.Visible && user.CustomerBranchID.HasValue)
                //{
                //    Queue<TreeNode> queue = new Queue<TreeNode>();
                //    foreach (TreeNode node in tvBranches.Nodes)
                //    {
                //        queue.Enqueue(node);
                //    }
                //    while (queue.Count > 0)
                //    {
                //        TreeNode node = queue.Dequeue();
                //        if (node.Value == user.CustomerBranchID.Value.ToString())
                //        {
                //            node.Selected = true;
                //            break;
                //        }
                //        else
                //        {
                //            foreach (TreeNode child in node.ChildNodes)
                //            {
                //                queue.Enqueue(child);
                //            }
                //        }
                //    }
                //    tvBranches_SelectedNodeChanged(null, null);
                //}

                //BindParameters(userParameterValues);
                upUsers.Update();
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open')", true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewEdit", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                mst_User user = new mst_User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    Address = tbxAddress.Text,
                    RoleID = Convert.ToInt32(ddlRole.SelectedValue),
                    DepartmentID=Convert.ToInt32(ddlDepartment.SelectedValue)

                };

                if (divCustomer.Visible)
                {
                    user.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }

                if (divCustomerBranch.Visible)
                {
                    user.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                }
                List<UserParameterValue_Risk> parameters = new List<UserParameterValue_Risk>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                    parameters.Add(new UserParameterValue_Risk()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }

                if ((int)ViewState["Mode"] == 1)
                {
                    user.ID = Convert.ToInt32(ViewState["UserID"]);
                }

                //bool emailExists;
                ////UserManagement.Exists(user, out emailExists);
                //if (emailExists)
                //{
                //    cvEmailError.IsValid = !emailExists;
                //    return;
                //}

                bool result = false;
                if ((int)ViewState["Mode"] == 0)
                {
              
                    //user.CreatedBy = 11;
                    //user.CreatedByText = "rahul";
                    //string passwordText = Util.CreateRandomPassword(10);
                    //user.Password = Util.CalculateMD5Hash(passwordText);
                    //string message = SendNotificationEmail(user, passwordText);

                    //result = UserManagement.Create(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                }
                else if ((int)ViewState["Mode"] == 1)
                {


                    //mst_User User = UserManagement.GetByID(Convert.ToInt32(user.ID));
                    //if (tbxEmail.Text.Trim() != User.Email)
                    //{
                    //    string message = SendNotificationEmailChanged(user);

                    //    string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                    //    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                    //}

                    //result = UserManagement.Update(user, parameters);
                }

                if (result)
                {
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "CloseDialog", "$(\"#divUsersDialog\").dialog('close')", true);
                }
                else
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }              
                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private string SendNotificationEmail(mst_User user, string passwordText)
        {
            //try
            //{
            //    int customerID = -1;
            //    string ReplyEmailAddressName = "";
            //    if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
            //    {
            //        ReplyEmailAddressName = "Avantis";
            //    }
            //    else
            //    {
            //        customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
            //        ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
            //    }

            //    string username = string.Format("{0} {1}", user.FirstName, user.LastName);
            //    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
            //                            .Replace("@Username", user.Email)
            //                            .Replace("@User", username)
            //                            .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
            //                            .Replace("@Password", passwordText)
            //                            .Replace("@From", ReplyEmailAddressName)
            //                            .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
            //                        ;

            //    return message;

            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvEmailError.IsValid = false;
            //    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            //}
            return null;
        }

        private string SendNotificationEmailChanged(mst_User user)
        {
            try
            {
                int customerID = -1;
                string message = "";
                //string ReplyEmailAddressName = "";
                //if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                //{
                //    ReplyEmailAddressName = "Avantis";
                //}
                //else
                //{
                //    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                //    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                //}

                //string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //    //.Replace("@Password", passwordText)
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                    ;
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        public event EventHandler OnSaved;
    }
}