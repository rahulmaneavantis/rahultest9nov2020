﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Configuration;
using System.Threading;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Users;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Users
{
    public partial class AuditUserDetailsControl_AM : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rblAuditRole.Enabled = true;
                //BindDepartment();
                UserImageUpload.Attributes["onchange"] = "UploadFile(this)";
                ddlRole.Enabled = true;
                DivAuditHeadRole.Visible = true;
                CampareValAuditHeadRole.Enabled = true;
                divRiskRole.Visible = false;
                CompareValidatorRoleAdmin.Enabled = false;
                CompareValidatorRoleAdmin.Enabled = false;
                DivCustomerName.Visible = true;
                ViewState["Mode"] = 0;
            }
        }

        protected void Upload(object sender, EventArgs e)
        {
            if (UserImageUpload.HasFile)
            {
                string[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg" };

                string ext = System.IO.Path.GetExtension(UserImageUpload.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Invalid File. Please upload a File with extension " + string.Join(",", validFileTypes);
                }

            }
        }

        protected void upUsers_Load(object sender, EventArgs e)
        {
        }
        protected void rblAuditRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRole.SelectedIndex = -1;
            ddlRiskRole.SelectedIndex = -1;

            if (rblAuditRole.SelectedItem.Text == "Is Audit Head" || rblAuditRole.SelectedItem.Text == "Is Audit Manager")
            {
                ddlRole.Enabled = false;
                ddlRole.Items.FindByText("Auditor-Auditee").Selected = true;

                ddlRiskRole.Enabled = false;
                ddlRiskRole.Items.FindByText("Auditor-Auditee").Selected = true;
            }
        }
        
        private void BindRoles()
        {
            try
            {
                ddlRole.DataTextField = "Name";
                ddlRole.DataValueField = "ID";

                var roles = RoleManagement.GetAll(false);
                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN") && !entry.Code.Equals("IMPT") && !entry.Code.Equals("CADMN")).ToList();
                }
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("IMPT")).ToList();
                }
                ddlRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("Select Role", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRolesRisk()
        {
            try
            {
                ddlRiskRole.DataTextField = "Name";
                ddlRiskRole.DataValueField = "ID";

                var roles = RoleManagement.GetLimitedRole(false);
                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN")).ToList();
                }

                ddlRiskRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlRiskRole.DataBind();

                ddlRiskRole.Items.Insert(0, new ListItem("Select Role", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlRiskRole_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        private void BindParameters(List<UserParameterValueInfo> userParameterValues = null)
        {
            try
            {
                if (userParameterValues == null)
                {
                    userParameterValues = new List<UserParameterValueInfo>();
                    var userParameters = UserManagement.GetAllUserParameters();
                    userParameters.ForEach(entry =>
                    {
                        userParameterValues.Add(new UserParameterValueInfo()
                        {
                            UserID = -1,
                            ParameterID = entry.ID,
                            ValueID = -1,
                            Name = entry.Name,
                            DataType = (DataType)entry.DataType,
                            Length = (int)entry.Length,
                            Value = string.Empty
                        });
                    });
                }

                repParameters.DataSource = userParameterValues;
                repParameters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void AddNewUser(int CustomerID, string CustomerName)
        {
            try
            {
                tbxFirstName.Text = string.Empty;
                tbxLastName.Text = string.Empty;
                tbxDesignation.Text = string.Empty;
                tbxEmail.Text = string.Empty;
                tbxContactNo.Text = string.Empty;
                if (ddlRole.Items.Count > 0)
                {
                    ddlRole.SelectedValue = "-1";
                }
                if (ddlRiskRole.Items.Count > 0)
                {
                    ddlRiskRole.SelectedValue = "-1";
                }
                tbxAddress.Text = string.Empty;
                cvEmailError.Attributes["class"] = "hidden";
                cvEmailError.ErrorMessage = string.Empty;
                upUsers.Update();
                ViewState["Mode"] = 0;
                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                if (!string.IsNullOrEmpty(CustomerName))
                {
                    lblCustomerName.InnerText = CustomerName;
                }
                Session["customerId"] = CustomerID;
                tbxEmail.Enabled = true;
                rblAuditRole.Enabled = true;
                rblAuditRole.ClearSelection();
                ddlRiskRole.ClearSelection();
                ddlRole.ClearSelection();
                ddlRole.SelectedValue = "-1";
                //ddlRole_SelectedIndexChanged(null, null);

                BindParameters();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool ac = UserManagementRisk.CheckProductmapping(CustomerID, 1);
                    if (ac == true)
                    {
                        divComplianceRole.Visible = true;
                        ddlRole.Enabled = true;
                    }
                    else
                    {
                        divComplianceRole.Visible = false;
                        ddlRole.Enabled = false;
                    }
                }
                upUsers.Update(); upUsers_Load(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void EditUserInformation(int userID, string customerName, int customerId)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "disableCombobox", "disableCombobox();", true);
                ViewState["Mode"] = 1;
                ViewState["UserID"] = userID;
                Session["MappedUserId"] = userID;
                User user = UserManagement.GetByID(userID);
                List<UserParameterValueInfo> userParameterValues = UserManagement.GetParameterValuesByUserID(userID);
                if (user.RoleID == 1 || user.RoleID == 2 || user.RoleID == 8 || user.RoleID == 12)
                {
                    rblAuditRole.Enabled = false;
                }
                if (!string.IsNullOrEmpty(customerName))
                {
                    lblCustomerName.InnerText = customerName;
                }
                Session["customerId"] = customerId;
                tbxFirstName.Text = user.FirstName;
                tbxLastName.Text = user.LastName;
                tbxDesignation.Text = user.Designation;
                tbxEmail.Text = user.Email;
                tbxContactNo.Text = user.ContactNumber;
                //tbxUsername.Text = user.Username;
                tbxAddress.Text = user.Address;
                
                mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                if (mstuser != null)
                {
                    if (mstuser.IsHead == true)
                    {
                        ddlroleAH.SelectedValue = "3";
                    }
                    else if (mstuser.RoleID == 8)
                    {
                        ddlroleAH.SelectedValue = "2";
                    }
                    else
                    {
                        ddlroleAH.SelectedValue = "1";
                    }
                }
                upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int UserID = 0;
                int customerID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(Session["customerId"])))
                {
                    customerID = Convert.ToInt32(Session["customerId"]);
                }
                string RoleNameVal = string.Empty;
                #region Compliance User
                User user = new User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    Address = tbxAddress.Text,
                    DepartmentID = -1,
                    IsExternal = false
                };

                List<UserParameterValue> parameters = new List<UserParameterValue>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                    parameters.Add(new UserParameterValue()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }
                #endregion

                #region Risk User

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    Address = tbxAddress.Text,
                    DepartmentID = -1,
                    IsExternal = false
                };

                List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk> parametersRisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                    parametersRisk.Add(new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }
                #endregion

                if (ddlroleAH.SelectedIndex != -1)
                {
                    if (ddlroleAH.SelectedItem.Text == "Auditee")
                    {
                        user.RoleID = 7;
                        mstUser.RoleID = 7;
                    }
                    else if (ddlroleAH.SelectedItem.Text == "Management")
                    {
                        user.RoleID = 8;
                        mstUser.RoleID = 8;
                    }
                    else if (ddlroleAH.SelectedItem.Text == "Department Head")
                    {
                        user.RoleID = 7;
                        mstUser.RoleID = 7;
                        user.IsHead = true;
                        mstUser.IsHead = true;
                    }
                }
                user.CustomerID = customerID;
                mstUser.CustomerID = customerID;

                if ((int)ViewState["Mode"] == 1)
                {
                    user.ID = Convert.ToInt32(ViewState["UserID"]);
                    mstUser.ID = Convert.ToInt32(ViewState["UserID"]);
                }

                bool emailExists;
                UserManagement.Exists(user, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                UserManagementRisk.Exists(mstUser, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                bool result = false;
                int CompUserID = 0;
                if ((int)ViewState["Mode"] == 0)
                {
                    bool Result = true;
                    if (Result)
                    {
                        user.CreatedBy = AuthenticationHelper.UserID;
                        user.CreatedByText = AuthenticationHelper.User;
                        string passwordText = Util.CreateRandomPassword(10);
                        user.Password = Util.CalculateAESHash(passwordText);
                        string message = SendNotificationEmail(user, passwordText);

                        mstUser.CreatedBy = AuthenticationHelper.UserID;
                        mstUser.CreatedByText = AuthenticationHelper.User;
                        mstUser.Password = Util.CalculateAESHash(passwordText);
                        CompUserID = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        if (CompUserID > 0)
                        {
                            UserID = CompUserID;
                            result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                            if (result == false)
                            {
                                UserManagement.deleteUser(CompUserID);
                            }
                        }
                        UserCustomerMapping objUserCustMapping = new UserCustomerMapping()
                        {
                            UserID = CompUserID,
                            CustomerID = customerID,
                            ProductID = 4,
                            IsActive = true,
                            CreatedOn = DateTime.Now,
                        };
                        UserManagementRisk.CreateUserCustomerMapping(objUserCustMapping);                        
                        cvEmailError.IsValid = false;
                        cvEmailError.ErrorMessage = "User Created Successfully.";
                    }
                    else
                    {
                        cvEmailError.IsValid = false;
                        cvEmailError.ErrorMessage = "Facility to create more users is not available in the Free version. Kindly contact Avantis to activate the same.";
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    int UpdateUserID = 0;
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["MappedUserId"])))
                    {
                        UpdateUserID = Convert.ToInt32(Session["MappedUserId"]);
                    }
                    UserID = Convert.ToInt32(ViewState["UserID"]);//for Unit assignment
                    //Get Existing Users Auditor ID and Compliance-Audit Product RoleID
                    User complianceUser = UserManagement.GetByID(UserID);
                    mst_User auditUser = UserManagementRisk.GetByID(UserID);

                    if (complianceUser != null)
                    {
                        user.AuditorID = complianceUser.AuditorID;
                    }

                    if (auditUser != null)
                    {
                        mstUser.AuditorID = auditUser.AuditorID;
                    }

                    User User = UserManagement.GetByID(Convert.ToInt32(user.ID));
                    if (tbxEmail.Text.Trim() != User.Email)
                    {
                        string message = SendNotificationEmailChanged(user);
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                    }
                    result = UserManagement.Update(user, parameters);
                    result = UserManagementRisk.Update(mstUser, parametersRisk);
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User Updated Successfully.";
                }
                else
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }
                
                if (result)
                {
                    if (UserImageUpload.HasFile)
                    {
                        string fileName = user.ID + "-" + user.FirstName + " " + user.LastName + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                        UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);

                        string filepath = "~/UserPhotos/" + fileName;

                        UserManagement.UpdateUserPhoto(Convert.ToInt32(user.ID), filepath, fileName);
                        UserManagementRisk.UpdateUserPhoto(Convert.ToInt32(mstUser.ID), filepath, fileName);
                    }
                }
                
                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    ;

                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        public event EventHandler OnSaved;
    }
}