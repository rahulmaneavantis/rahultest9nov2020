﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Users
{
    public partial class AddUserCompanyAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void BindCustomersList()
        {
            try
            {
                long userId = Portal.Common.AuthenticationHelper.UserID;
                int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
                int RoleID = 0;
                if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
                {
                    RoleID = 2;
                }
                var customerList = RiskCategoryManagement.GetCustomerListForDDL(userId, ServiceProviderID, RoleID);
                if (customerList.Count > 0)
                {
                    ddlCustomer.DataTextField = "CustomerName";
                    ddlCustomer.DataValueField = "CustomerId";
                    ddlCustomer.DataSource = customerList;
                    ddlCustomer.DataBind();
                    ddlCustomer.Items.Insert(0, new ListItem("Customer", "-1"));
                    //ddlCustomer.SelectedIndex = 1;
                }
                else
                {
                    ddlCustomer.DataSource = null;
                    ddlCustomer.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upUsers_Load(object sender, EventArgs e)
        {

        }

        protected void ddlRiskRole_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool a = false;
                int customerID = -1;
                customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                a = UserManagementRisk.CheckProductmapping(customerID);

                int getproductrisk = -1;
                getproductrisk = Convert.ToInt32(ddlRiskRole.SelectedValue);

                #region Compliance User
                User user = new User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    Address = tbxAddress.Text,
                    RoleID = getproductrisk,
                    DepartmentID = -1,
                    IsExternal = false,
                    IsHead = false,
                    CustomerID = (int)AuthenticationHelper.CustomerID
                };
                #endregion

                #region Risk User

                Business.DataRisk.mst_User mstUser = new Business.DataRisk.mst_User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    Address = tbxAddress.Text,
                    RoleID = getproductrisk,
                    DepartmentID = -1,
                    IsExternal = false,
                    IsHead = false,
                    CustomerID = (int)AuthenticationHelper.CustomerID
                };
                #endregion

                if (ddlReportingTo.SelectedValue != "-1" && ddlReportingTo.SelectedValue != "")
                {
                    user.ReportingToID = Convert.ToInt64(ddlReportingTo.SelectedValue);
                    mstUser.ReportingToID=Convert.ToInt64(ddlReportingTo.SelectedValue);
                }

                if (ddlRiskRole.SelectedIndex != -1)
                {
                    if (ddlRiskRole.SelectedItem.Text == "Audit Head")
                    {
                        user.IsAuditHeadOrMgr= "AH";
                        mstUser.IsAuditHeadOrMgr = "AH";
                    }
                    else if (ddlRiskRole.SelectedItem.Text == "Audit Manager")
                    {
                        user.IsAuditHeadOrMgr = "AM";
                        mstUser.IsAuditHeadOrMgr = "AM";
                    }
                }
                
                if ((int)ViewState["Mode"] == 1)
                {
                    user.ID = Convert.ToInt32(ViewState["UserID"]);
                    mstUser.ID = Convert.ToInt32(ViewState["UserID"]);
                }

                bool emailExists;
                UserManagement.Exists(user, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                UserManagementRisk.Exists(mstUser, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                bool result = false;
                int resultValue = 0;
                if ((int)ViewState["Mode"] == 0)
                {
                    user.CreatedBy = AuthenticationHelper.UserID;
                    user.CreatedByText = AuthenticationHelper.User;
                    string passwordText = Util.CreateRandomPassword(10);
                    user.Password = Util.CalculateMD5Hash(passwordText);
                    string message = SendNotificationEmail(user, passwordText);

                    mstUser.CreatedBy = AuthenticationHelper.UserID;
                    mstUser.CreatedByText = AuthenticationHelper.User;
                    mstUser.Password = Util.CalculateMD5Hash(passwordText);
                    resultValue = UserManagement.CreateNew(user, null, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                    if (resultValue > 0)
                    {
                        result = UserManagementRisk.Create(mstUser, null, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        if (result == false)
                        {
                            UserManagement.deleteUser(resultValue);
                        }
                    }
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User Created Successfully.";
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    int userID = -1;
                    userID = Convert.ToInt32(ViewState["UserID"]);

                    //Get Existing Users Auditor ID and Compliance-Audit Product RoleID
                    User complianceUser = UserManagement.GetByID(userID);
                    mst_User auditUser = UserManagementRisk.GetByID(userID);

                    if (complianceUser != null)
                    {
                        user.AuditorID = complianceUser.AuditorID;
                        //user.RoleID = complianceUser.RoleID;
                    }

                    if (auditUser != null)
                    {
                        mstUser.AuditorID = auditUser.AuditorID;
                        //mstUser.RoleID = auditUser.RoleID;
                    }

                    User User = UserManagement.GetByID(Convert.ToInt32(user.ID));
                    if (tbxEmail.Text.Trim() != User.Email)
                    {
                        string message = SendNotificationEmailChanged(user);
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                    }
                    result = UserManagement.Update(user, null);
                    result = UserManagementRisk.Update(mstUser, null);
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User Updated Successfully.";
                }
                else
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }

                if (result)
                {
                    if (UserImageUpload.HasFile)
                    {
                        string fileName = user.ID + "-" + user.FirstName + " " + user.LastName + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                        UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);

                        string filepath = "~/UserPhotos/" + fileName;

                        UserManagement.UpdateUserPhoto(Convert.ToInt32(user.ID), filepath, fileName);
                        UserManagementRisk.UpdateUserPhoto(Convert.ToInt32(mstUser.ID), filepath, fileName);
                    }
                }
                //if (OnSaved != null)
                //{
                //    OnSaved(this, null);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    ;

                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }
        
    }
}