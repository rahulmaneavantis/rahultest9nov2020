﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common
{
    public partial class OpenObservationDetails : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        public static bool ApplyFilter;
        protected string AuditHeadOrManagerReport = null;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // comment by sagar more on 29-01-2020
                //CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindCustomerList(); // added by sagar more on 28-01-2020
                BindProcess("P");
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData();// comment by sagar more on 29-01-2020
                BindData();
                bindPageNumber();
                ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
                ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                DropDownListPageNo.Items.Clear();
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;
            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindData();
        }
        private void BindProcess(string flag)
        {
            try
            {
                bool IsAuditManager = false;
                bool IsDepartmentHead = false;
                bool IsManagement = false;
                // code comment by sagar more on 29-01-2020
                //if (CustomerId == 0)
                //{
                //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //}

                // added by sagar more on 30-01-2020
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        IsDepartmentHead = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        IsManagement = true;
                    }
                }
                ddlProcess.Items.Clear();
                ddlProcess.DataTextField = "Name";
                ddlProcess.DataValueField = "Id";

                if (IsAuditManager)
                {
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownAuditManager(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
                }
                else if (IsDepartmentHead)
                {
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownDepartmentHead(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
                }
                else if (IsManagement)
                {
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownManagement(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));
                }

                //if (flag == "P")
                //{
                //    ddlProcess.DataSource = ProcessManagement.FillProcess("P", CustomerId);
                //    ddlProcess.DataBind();
                //    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                //}
                //else
                //{                    
                //    ddlProcess.DataSource = ProcessManagement.FillProcess("N", CustomerId);
                //    ddlProcess.DataBind();
                //    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindVertical()
        {
            try
            {
                int branchid = -1;
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
        }
        public void BindLegalEntityData()
        {
            //code comment by sagar more on 29-01-2020
            //if (CustomerId == 0)
            //{
            //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //}

            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            // code comment by sagar more on 29-01-2020
            //if (CustomerId == 0)
            //{
            //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //}

            // added by sagar more on 29-01-2020
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            bool IsAuditManager = false;
            bool IsDepartmentHead = false;
            bool IsManagement = false;
            if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
            {
                if (Convert.ToString(Request.QueryString["UType"]) == "AM" || Convert.ToString(Request.QueryString["UType"]) == "AH")
                {
                    IsAuditManager = true;
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                {
                    IsDepartmentHead = true;
                }
                else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                {
                    IsManagement = true;
                }
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (IsAuditManager)
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (IsDepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (IsManagement)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            //DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, CustomerId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }
        public void BindSchedulingType()
        {
            int branchid = -1;
            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(long customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(long customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        private void BindData()
        {
            try
            {
                // code comment by sagar more on 29-01-2020
                //if (CustomerId == 0)
                //{
                //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //}

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                ApplyFilter = false;
                int CustBranchID = -1;
                int VerticalID = -1;
                int ProcessID = -1;
                int UserID = -1;
                bool IsDepartmentHead = false;
                bool IsAuditManager = false;
                bool IsManagement = false;
                String FinancialYear = String.Empty;
                String Period = String.Empty;
                String Type = String.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    Type = Request.QueryString["Type"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                {
                    CustBranchID = Convert.ToInt32(Request.QueryString["branchid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    FinancialYear = Convert.ToString(Request.QueryString["FinYear"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                {
                    Period = Convert.ToString(Request.QueryString["Period"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        IsDepartmentHead = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        IsManagement = true;
                    }
                }
                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                List<int?> BranchList = new List<int?>();
                if (Session["BranchList"] != null)
                {
                    BranchList = (List<int?>)(Session["BranchList"]);
                }
                var detailView = InternalControlManagementDashboardRisk.GetOpenObservationsDetailView(Type, CustomerId, BranchList, FinancialYear, Period, ProcessID, VerticalID, UserID, IsAuditManager, IsManagement, IsDepartmentHead);
                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();
                Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<ObservationAging_DisplayView>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindDataFilter()
        {
            try
            {
                // comment by sagar more on 29-01-2020
                //if (CustomerId == 0)
                //{
                //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //}

                //added by sagar more on 29-01-2020
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                int CustBranchID = -1;
                int CatID = -1;
                int VerticalID = -1;
                int ProcessID = -1;
                int UserID = -1;
                bool IsAuditManager = false;
                bool IsDepartmentHead = false;
                bool IsManagement = false;
                String FinancialYear = String.Empty;
                String Period = String.Empty;
                String Type = String.Empty;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    Type = Request.QueryString["Type"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ObsID"]))
                {
                    CatID = Convert.ToInt32(Request.QueryString["ObsID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["PID"]))
                {
                    ProcessID = Convert.ToInt32(Request.QueryString["PID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        IsDepartmentHead = true;
                    }
                    else if (Convert.ToString(Request.QueryString["UType"]) == "MG")
                    {
                        IsManagement = true;
                    }
                }
                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        ProcessID = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    //int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);

                    int vid = UserManagementRisk.VerticalgetBycustomerid(CustomerId);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }
                List<int?> ListofBranch = new List<int?>();
                if (CustBranchID == -1)
                {
                    if (Session["BranchList"] != null)
                    {
                        ListofBranch = (List<int?>)(Session["BranchList"]);
                    }
                }
                else
                {
                    Branchlist.Clear();
                    GetAllHierarchy(CustomerId, CustBranchID);
                    if (Branchlist.Count > 0)
                    {
                        ListofBranch = Branchlist.Select(x => (int?)x).ToList();
                    }
                }
                List<int> ObsRating = new List<int>();
                if (Type.Trim().Equals("High"))
                    ObsRating.Add(1);
                else if (Type.Trim().Equals("Medium"))
                {
                    ObsRating.Add(2);
                }
                else if (Type.Trim().Equals("Low"))
                {
                    ObsRating.Add(3);
                }
                else
                {
                    ObsRating.Add(1);
                    ObsRating.Add(2);
                    ObsRating.Add(3);
                }
                var detailView = InternalControlManagementDashboardRisk.GetOpenObservationsDetailView(Type, CustomerId, ListofBranch, FinancialYear, Period, ProcessID, VerticalID, UserID, IsAuditManager, IsManagement, IsDepartmentHead);
                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();
                Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<ObservationStatus_DisplayView_New>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;
                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }
                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                if (ApplyFilter)
                {
                    BindDataFilter();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
                else
                {
                    BindData();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ApplyFilter = true;
                BindDataFilter();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    String FileName = String.Empty;
                    FileName = "Observation Report";
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                    DataTable ExcelData = null;
                    DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                    ExcelData = view.ToTable("Selected", false, "Branch", "FinancialYear", "VerticalName", "ForMonth", "ProcessName", "SubProcessName", "ActivityDescription", "Observation", "ManagementResponse", "Recomendation", "TimeLine", "ObservationCategoryName", "ObservatioRating");
                    ExcelData.Columns.Add("Rating");
                    // code comment by sagar more on 30-01-2020
                    //var customer = UserManagementRisk.GetCustomerName(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID));

                    //code added by sagar more on 30-01-2020
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        if (ddlCustomer.SelectedValue != "-1")
                        {
                            CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Customer";
                            return;
                        }
                    }

                    var customer = UserManagementRisk.GetCustomerName(CustomerId);

                    exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A2"].Value = customer;
                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                    foreach (DataRow item in ExcelData.Rows)
                    {
                        if (item["ObservatioRating"] != null)
                        {
                            if (item["ObservatioRating"].ToString() == "1")
                                item["Rating"] = "High";
                            else if (item["ObservatioRating"].ToString() == "2")
                                item["Rating"] = "Medium";
                            else if (item["ObservatioRating"].ToString() == "3")
                                item["Rating"] = "Low";
                        }
                    }

                    ExcelData.Columns.Remove("ObservatioRating");
                    exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                    //exWorkSheet.Cells["A3"].Value = lblDetailViewTitle.Text + " Report";
                    exWorkSheet.Cells["A3"].Value = "Observation Report";
                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A3"].AutoFitColumns(50);

                    exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A5"].Value = "Location";
                    exWorkSheet.Cells["A5"].AutoFitColumns(50);

                    exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B5"].Value = "Financial Year";
                    exWorkSheet.Cells["B5"].AutoFitColumns(15);

                    exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C5"].Value = "Vertical Name";
                    exWorkSheet.Cells["C5"].AutoFitColumns(15);

                    exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D5"].Value = "Period";
                    exWorkSheet.Cells["D5"].AutoFitColumns(15);

                    exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E5"].Value = "Process";
                    exWorkSheet.Cells["E5"].AutoFitColumns(25);

                    exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F5"].Value = "SubProcessName";
                    exWorkSheet.Cells["F5"].AutoFitColumns(50);

                    exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G5"].Value = "Activity Description";
                    exWorkSheet.Cells["G5"].AutoFitColumns(50);

                    exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H5"].Value = "Observation";
                    exWorkSheet.Cells["H5"].AutoFitColumns(50);

                    exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I5"].Value = "Management Response";
                    exWorkSheet.Cells["I5"].AutoFitColumns(50);

                    exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J5"].Value = "Recommandation";
                    exWorkSheet.Cells["J5"].AutoFitColumns(50);

                    exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K5"].Value = "Time Line";
                    exWorkSheet.Cells["K5"].AutoFitColumns(15);

                    exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["L5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["L5"].Value = "Observation Category";
                    exWorkSheet.Cells["L5"].AutoFitColumns(25);

                    exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["M5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["M5"].Value = "Observation Rating";
                    exWorkSheet.Cells["M5"].AutoFitColumns(20);

                    //Assign borders
                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 13])
                    {
                        col.Style.Numberformat.Format = "dd/MM/yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.WrapText = true;
                        //col.AutoFitColumns(20);
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        //col.Style.Border.Right.Styl
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    BindLegalEntityData();
                    BindDataFilter();
                    BindProcess("P");
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                }
            }
        }

        // added by sagar more on 28-01-2020
        private void BindCustomerList()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDL(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Customer", "-1"));
                ddlCustomer.SelectedIndex = 2;
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();
            }
        }
    }
}