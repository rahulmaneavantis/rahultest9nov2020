﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common
{
    public partial class DHPlannedActualAuditDetails : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        public static bool ApplyFilter;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindLegalEntityData();
                BindFinancialYear();               
                BindVertical();
                BindData();
                bindPageNumber();                                  
                ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListPageNo.SelectedItem.ToString() != "")
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;                
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);                
                BindData();
            }
        }
        

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

               

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID,CustomerId, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindData()
        {
            try
            {
                ApplyFilter = false;

                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                int CustBranchID = -1;
                int VerticalID = -1;                
                int UserID = -1;

                bool IsDepartmentHead = false;

                String FinancialYear = String.Empty;
                String Period = String.Empty;
                String Type = String.Empty;

                DateTime FromDate = new DateTime();
                DateTime ToDate = new DateTime();               

                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    Type = Request.QueryString["Type"];
                }                

                if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                {
                    CustBranchID = Convert.ToInt32(Request.QueryString["branchid"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FDate"]))
                {
                    FromDate = Convert.ToDateTime(Request.QueryString["FDate"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["TDate"]))
                {
                    ToDate = Convert.ToDateTime(Request.QueryString["TDate"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Period"]))
                {
                    Period = Convert.ToString(Request.QueryString["Period"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["VID"]))
                {
                    VerticalID = Convert.ToInt32(Request.QueryString["VID"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "DH")
                    {
                        IsDepartmentHead = true;
                        UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    }
                }

                List<int?> BranchList = new List<int?>();
                if (Session["BranchList"] != null)
                {
                    BranchList = (List<int?>)(Session["BranchList"]);
                }

                var detailView = InternalControlManagementDashboardRisk.GetAuditPlannedActualDisplaySP(Type, CustomerId, BranchList, FinancialYear, Period, FromDate, ToDate, VerticalID, UserID, IsDepartmentHead);

                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();

                Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<SP_AuditPlannedActualDetailDisplay_Result>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindDataFilter()
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                int CustBranchID = -1;
                int VerticalID = -1;
                int ProcessID = -1;
                int UserID = -1;

                bool IsAuditManager = false;

                String FinancialYear = String.Empty;
                String Period = String.Empty;
                String Type = String.Empty;

                DateTime FromDate = new DateTime();
                DateTime ToDate = new DateTime();

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    Type = Request.QueryString["Type"];
                }
                               
                if (!string.IsNullOrEmpty(Request.QueryString["FDate"]))
                {
                    FromDate = Convert.ToDateTime(Request.QueryString["FDate"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["TDate"]))
                {
                    ToDate = Convert.ToDateTime(Request.QueryString["TDate"]);
                }

                if (!String.IsNullOrEmpty(ddlVertical.SelectedValue))
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }

                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["UType"]))
                {
                    if (Convert.ToString(Request.QueryString["UType"]) == "AM")
                    {
                        IsAuditManager = true;                        
                    }
                }

                List<int?> ListofBranch = new List<int?>();

                if (CustBranchID == -1)
                {
                    if (Session["BranchList"] != null)
                    {
                        ListofBranch = (List<int?>)(Session["BranchList"]);
                    }
                }
                else
                {
                    Branchlist.Clear();
                    GetAllHierarchy(CustomerId, CustBranchID);

                    if (Branchlist.Count > 0)
                    {
                        ListofBranch = Branchlist.Select(x => (int?)x).ToList();
                    }
                }
                UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                var detailView = InternalControlManagementDashboardRisk.GetAuditPlannedActualDisplaySP(Type, CustomerId, ListofBranch, FinancialYear,Period, FromDate, ToDate, VerticalID,UserID, IsAuditManager);

                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();

                Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<SP_AuditPlannedActualDetailDisplay_Result>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
       
        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ApplyFilter = true;

                BindDataFilter();

                //GetPageDisplaySummary();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    String FileName = String.Empty;

                    FileName = "Audit Status Report";

                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                    DataTable ExcelData = null;

                    DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);

                    ExcelData = view.ToTable("Selected", false, "Branch", "VerticalName", "FinancialYear", "ForMonth", "StartDate", "EndDate", "ExpectedStartDate", "ExpectedEndDate");
                   
                    var customer = UserManagementRisk.GetCustomerName(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));

                    foreach (DataRow item in ExcelData.Rows)
                    {
                        if (item["StartDate"] != null && item["StartDate"] != DBNull.Value)  
                                item["StartDate"] = Convert.ToDateTime(item["StartDate"]).ToString("dd-MMM-yyyy");

                        if (item["EndDate"] != null && item["EndDate"] != DBNull.Value)
                            item["EndDate"] = Convert.ToDateTime(item["EndDate"]).ToString("dd-MMM-yyyy");

                        if (item["ExpectedStartDate"] != null && item["ExpectedStartDate"] != DBNull.Value)
                            item["ExpectedStartDate"] = Convert.ToDateTime(item["ExpectedStartDate"]).ToString("dd-MMM-yyyy");

                        if (item["ExpectedEndDate"] != null && item["ExpectedEndDate"] != DBNull.Value)
                            item["ExpectedEndDate"] = Convert.ToDateTime(item["ExpectedEndDate"]).ToString("dd-MMM-yyyy");  
                    }

                    exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A2"].Value = customer;
                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Size = 12; 
                    
                    exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                    //exWorkSheet.Cells["A3"].Value = lblDetailViewTitle.Text + " Report";
                    exWorkSheet.Cells["A3"].Value = "Audit Status Report";
                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A3"].AutoFitColumns(50);

                    exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A5"].Value = "Location";
                    exWorkSheet.Cells["A5"].AutoFitColumns(50);

                    exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B5"].Value = "Vertical";
                    exWorkSheet.Cells["B5"].AutoFitColumns(15);

                    exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C5"].Value = "Financial Year";
                    exWorkSheet.Cells["C5"].AutoFitColumns(15);

                    exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D5"].Value = "Period";
                    exWorkSheet.Cells["D5"].AutoFitColumns(15);

                    exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E5"].Value = "Audit Planned (From)";
                    exWorkSheet.Cells["E5"].AutoFitColumns(15);

                    exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F5"].Value = "Audit Planned (To)";
                    exWorkSheet.Cells["F5"].AutoFitColumns(15);

                    exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G5"].Value = "Audit Actual (From)";
                    exWorkSheet.Cells["G5"].AutoFitColumns(15);

                    exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H5"].Value = "Audit Actual (To)";
                    exWorkSheet.Cells["H5"].AutoFitColumns(15);                   

                    //Assign borders
                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 8])
                    {
                        col.Style.Numberformat.Format = "dd-MMM-yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.WrapText = true;
                        //col.AutoFitColumns(20);
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        //col.Style.Border.Right.Styl
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {           
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();                    
                }

                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();                    
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();                    
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }            
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }            
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }            
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {            
            BindVertical();
            BindSchedulingType();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
               
                //Reload the Grid
                if (ApplyFilter)
                    BindDataFilter();
                else
                    BindData();

                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }        
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }        
    }
}