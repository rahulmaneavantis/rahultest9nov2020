﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" MaintainScrollPositionOnPostback="true"  AutoEventWireup="true" CodeBehind="IFCAuditManagerDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common.IFCAuditManagerDashboard" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Offline-->
    <script type="text/javascript" src="../../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="../../avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="../../avantischarts/highcharts/js/modules/drilldown.js"></script>
    <%--<script type="text/javascript" src="../../avantischarts/highcharts/js/modules/exporting.js"></script>--%>
    <script type="text/javascript" src="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../newjs/spectrum.js"></script>

    <link href="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../../newcss/spectrum.css" rel="stylesheet" />

    <style type="text/css">
        #ContentPlaceHolder1_grdAuditTrackerSummary.table tr td {
            border: 1px solid white;
        }

        .colorPickerWidget {
            padding: 10px;
            margin: 10px;
            text-align: center;
            width: 360px;
            border-radius: 5px;
            background: #fafafa;
            border: 2px solid #ddd;
        }
    </style>
      <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <script type="text/javascript">

        function fpopulateddataprocess(keynonkey, type, customerid, branchid, FinYear, period, processid, subprocessid, IsMgmt) {
            $('#DivReports').modal('show');
            $('#showdetails').attr('width', '1150px');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#showdetails').attr('src', "../../Management/ICFRProcessObservationDetails.aspx?Type=" + type + "&keynonkey=" + keynonkey + "&customerid=" + customerid + "&branchid=" + branchid + "&FinYear=" + FinYear + "&period=" + period + "&processid=" + processid + "&subprocessid=" + subprocessid + "&ISMGMT=" + IsMgmt);
        }
        function fpopulateddataQuarter(keynonkey, type, customerid, branchid, FinYear, period, isqhya, IsMgmt) {
            $('#DivReports').modal('show');
            $('#showdetails').attr('width', '1150px');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#showdetails').attr('src', "../../Management/ICFRObservationDetails.aspx?Type=" + type + "&keynonkey=" + keynonkey + "&customerid=" + customerid + "&branchid=" + branchid + "&FinYear=" + FinYear + "&period=" + period + "&ISQHYA=" + isqhya + "&ISMGMT=" + IsMgmt);
        }


        function closeDiv(id) {
            document.getElementById(id + 'Checkbox').className = 'menucheckbox-notchecked';
            document.getElementById(id).style.display = 'none';
            // do something
        }

        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
        });

        var ObservationStatusColorScheme = {
            high: "<%=highcolor.Value %>",
            medium: "<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>"
        };


        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                <%=ProcessWiseObservationStatusChart%>
            });
        });

    </script>

    <script type="text/javascript">
        $(function () {
            // Chart Global options
            Highcharts.setOptions({
                credits: {
                    text: '',
                    href: 'https://www.avantis.co.in',
                },
                lang: {
                    drillUpText: "< Back",
                },
            });
            var perStatusPieChartQ1 = Highcharts.chart('ContentPlaceHolder1_perStatusPieChartDivAPRJUN', {                
                <%=perStatusPieChartAPRJUN%>
            });
            var perStatusPieChartQ2 = Highcharts.chart('ContentPlaceHolder1_perStatusPieChartDivJULSEP', {
                <%=perStatusPieChartJULSEP%>
            });
            var perStatusPieChartQ3 = Highcharts.chart('ContentPlaceHolder1_perStatusPieChartDivOCTDEC', {
                <%=perStatusPieChartOCTDEC%>
            });
            var perStatusPieChartQ4 = Highcharts.chart('ContentPlaceHolder1_perStatusPieChartDivJANMAR', {
                <%=perStatusPieChartJANMAR%>
            });

        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                //Quaterly Wise- Failed Control   

                var QuarterlyFailedControlChart = Highcharts.chart('QuarterlyFailedControlColumnChart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    <%=QuarterlyFailedControlStatusChart%>

                });
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                //Process Wise- Failed Control   

                var ProcessWiseFailedControlChart = Highcharts.chart('DivProcessWiseFailedControlColumnChart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },

                    <%=ProcessWiseFailedControlStatusChart%>

                });
            });
        });
    </script>

    <style type="text/css">
        .progressannually { width: 100%; height:300px; }

        .progressHF1 { width: 50%; height:300px; }
        .progressHF2 { width: 50%; height:300px; }

        .progressQ1 { width: 25%; height:300px; }
        .progressQ2 { width: 25%; height:300px; }
        .progressQ3 { width: 25%; height:300px; }
        .progressQ4 { width: 25%; height:300px; }        
        .progressno { display:none; }  
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section id="container" class="">
            <section class="wrapper">       
                
                 <asp:UpdatePanel ID="upDivFilters" runat="server" UpdateMode="Conditional">       
                          <ContentTemplate> 
                              
                        <% if (roles.Contains(3))%>
                        <% { %>
                            <div id="performersummary" class="col-lg-12 col-md-12 colpadding0">
                                <div class="panel panel-default" style="background:none;">
                                    <div class="panel-heading" style="background:none;">
                                        <h2>Performer Summary</h2>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformer"><i class="fa fa-chevron-up"></i></a>
                                            <a href="javascript:closeDiv('performersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                </div>   
                            </div>                        
                            <div id="collapsePerformer" class="panel-collapse collapse in">
                            <div class="row ">
                                 <% var performerGuage = GetPeformerAuditGuagePercentage(); %> 
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg" style="height:160px;important;">
                                        <div class="title">Open Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">           
                                                <a href="../InternalAuditTool/InternalAuditStatusUI.aspx?Status=Open&RoleID=3">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divPerformerOpenAuditCount">0</div>
                                                </a>                                               
                                                <div class="progress thin dashboardProgressbar">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=performerGuage%>%;">
                                                    </div>
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=performerGuage%>%;">
                                                    </div>
                                                </div>
                                            </div>                                                                                                            
                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                    </div>   
                                </div> 

                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg" style="height:160px;important;">
                                        <div class="title">Closed Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">        
                                                <a href="../InternalAuditTool/InternalAuditStatusUI.aspx?Status=Closed&RoleID=3">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divPerformerCloseAuditCount">0</div>
                                                </a>
                                                
                                                <div class="progress thin dashboardProgressbar">
                                                     <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-performerGuage%>%;">
                                                    </div>
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-performerGuage%>%;">
                                                    </div>
                                                </div>
                                            </div>                                                                                                                                                                                                                
                                    </div>   
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg">
                                        <div class="title">Failed Controls</div> 
                                         <% var performerDuefailedControlsGuage = GetPeformerFailedControlsGuagePercentage("D"); %>                                              
                                        <div class="col-md-6  borderright">  
                                               <a href="../../RiskManagement/AuditTool/FailedControlTesting.aspx?Status=D">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divperformerFailedControlDUE">0</div>
                                                </a>
                                            <div class="desc">Due</div>

                                               <div class="progress thin dashboardProgressbar">
                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:<%=performerDuefailedControlsGuage%>%;">
                                                    </div>
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:<%=performerDuefailedControlsGuage%>%;">
                                                    </div>
                                                </div>
                                        </div> 
                                      
                                        <div class="col-md-6">
                                            <a href="../../RiskManagement/AuditTool/FailedControlTesting.aspx?Status=ND">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divperformerFailedControlNotDUE">0</div>
                                                </a>  
                                             <% var performerNotDuefailedControlsGuage = GetPeformerFailedControlsGuagePercentage("ND"); %>                                     
                                                <div class="desc">Not Due</div>
                                                <div class="progress thin dashboardProgressbar">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:<%=performerNotDuefailedControlsGuage%>%;">
                                                    </div>
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:<%=performerNotDuefailedControlsGuage%>%;">
                                                    </div>
                                                </div>                                           
                                        </div> 
                                        <div class="clearfix"></div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                    </div>   
                                </div>
                           </div>
                      </div>                                            
                        <% } %>
                        <div class="clearfix"></div> 
                         <% if (roles.Contains(4))%>
                        <% { %>
                            <div id="Reviewersummary" class="col-lg-12 col-md-12 colpadding0">
                            <div class="panel panel-default" style="background:none;">
                                <div class="panel-heading" style="background:none;">
                                    <h2>Reviewer Summary</h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseReviewer"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('Reviewersummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>   
                            </div>

                            <div id="collapseReviewer" class="panel-collapse collapse in">
                            <div class="row">
                                 <% var reviewerGuage = GetReviewerAuditGuagePercentage(); %> 
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg" style="height:160px;important;">
                                        <div class="title">Open Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">           
                                                <a href="../InternalAuditTool/InternalAuditStatusUI.aspx?Status=Open&RoleID=4">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divReviewerOpenAuditCount">0</div>
                                                </a>                                               
                                                <div class="progress thin dashboardProgressbar">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:<%=reviewerGuage%>%;">
                                                    </div>
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:<%=reviewerGuage%>%;">
                                                    </div>
                                                </div>
                                            </div>                                                                                                            
                                           <div class="clearfix"></div>                                                                                                                                                                                                                        
                                    </div>   
                                </div> 

                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg" style="height:160px;important;">
                                        <div class="title">Closed Audits</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-12">        
                                                <a href="../InternalAuditTool/InternalAuditStatusUI.aspx?Status=Closed&RoleID=4">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divReviewerCloseAuditCount">0</div>                                                                                          
                                                </a>                                                
                                                <div class="progress thin dashboardProgressbar">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-reviewerGuage%>%;">
                                                    </div>
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=100-reviewerGuage%>%;">
                                                    </div>
                                                </div>
                                            </div>                                                                                                                                                                                                                
                                    </div>   
                                </div>

                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg" style="height:160px;important;">
                                        <div class="title">Failed Controls</div> 
                                        <% var reviewerfailedControlsGuage = GetReviewerFailedControlsGuagePercentage("D"); %>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                            <div class="col-md-6  borderright">        
                                                <a href="../../RiskManagement/AuditTool/FailedControlTesting.aspx?Status=D">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divReviewerFailedControlDUE">0</div>
                                                </a>
                                                <div class="desc">Due</div>
                                                <div class="progress thin dashboardProgressbar">
                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:<%=reviewerfailedControlsGuage%>%;">
                                                    </div>
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:<%=reviewerfailedControlsGuage%>%;">
                                                    </div>
                                                </div>
                                            </div>
                                         <% var reviewerNotDuefailedControlsGuage = GetReviewerFailedControlsGuagePercentage("ND"); %>     
                                        <div class="col-md-6">
                                            <a href="../../RiskManagement/AuditTool/FailedControlTesting.aspx?Status=ND">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divReviewerFailedControlNOTDUE">0</div>
                                                </a>
                                                <div class="desc">Not Due</div>
                                                <div class="progress thin dashboardProgressbar">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:<%=reviewerNotDuefailedControlsGuage%>%;">
                                                    </div>
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:<%=reviewerNotDuefailedControlsGuage%>%;">
                                                    </div>
                                                </div>
                                        </div>                                                                                                                                                                                                            
                                    </div>   
                                </div>
                           </div>
                      </div>                                              
                         <% } %>
                         <div class="clearfix"></div> 
                           <% if (personresponsibleapplicable)%>
                        <% { %>
                           <div id="Personresponsiblesummary" class="col-lg-12 col-md-12 colpadding0">
                            <div class="panel panel-default" style="background:none;">
                                <div class="panel-heading" style="background:none;">
                                    <h2>Person Responsible Summary</h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePersonresponsible"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('Personresponsiblesummary')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>   
                            </div>

                           <div id="collapsePersonresponsible" class="panel-collapse collapse in">
                            <div class="row ">
                               
                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" >
                                    <div class="info-box white-bg" style="height:160px;important;">
                                        <div class="title">Failed Controls</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                            <div class="col-md-6  borderright">         
                                                <a href="../../RiskManagement/AuditTool/FailedControlTestingPR.aspx?Status=D">                                                                                                                                                                                                                                                                                         
                                                    <div class="count" runat="server" id="divPersonresponsiblesummaryDUE">0</div>
                                                </a>
                                                 <div class="desc">Due</div>
                                                <div class="progress thin dashboardProgressbar">
                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:50%;">
                                                    </div>
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:50%;">
                                                    </div>
                                                </div>
                                            </div> 
                                        <div class="col-md-6">
                                            <a href="../../RiskManagement/AuditTool/FailedControlTestingPR.aspx?Status=ND">                                                                                                                                                                                                                                                                                         
                                            <div class="count" runat="server" id="divPersonresponsiblesummaryNOTDUE">0</div>
                                            </a>
                                            <div class="desc">Not Due</div>
                                            <div class="progress thin dashboardProgressbar">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:50%;">
                                                </div>
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:50%;">
                                                </div>
                                            </div>
                                        </div>                                                                                                                                                                                                               
                                    </div>   
                                </div>
                           </div>
                      </div>
                          <% } %>  

                        <div class="clearfix"></div>


                                   
                    <div id="DivFilters" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Filter </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#ContentPlaceHolder1_collapseDivFilters"><i class="fa fa-chevron-down"></i></a>
                                        <a href="javascript:closeDiv('DivFilters')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseDivFilters" class="panel-collapse collapse" runat="server">     
                                 <div class="panel-body">
                           <div class="col-md-12 colpadding0">

                               <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%">
                                   <asp:DropDownListChosen ID="ddlCustomer" runat="server" DataPlaceHolder="Select Customer"
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true"></asp:DropDownListChosen>
                               </div>
                               <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%"> 
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Entity" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">
                                  
                                    
                                       <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Entity" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                              AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                </asp:DropDownListChosen>                             
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                                   
                                 
                                      <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Entity" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                               AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                     
                                </div> 
                                                                                                
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                                    
                                       
                                       <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Entity" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                               AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                  
                                </div>
                          </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0"> 
                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                                                          
                                         <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" DataPlaceHolder="Sub Entity" AutoPostBack="true"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                    </asp:DropDownListChosen>
                                    </div>                         
                                               
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                     
                                      <%--  <asp:DropDownList ID="ddlFinancialYear" runat="server" AutoPostBack="true"
                                             OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged"
                                             class="form-control m-bot15 select_location" Style="float: left; width:90%;">                                         
                                        </asp:DropDownList>   
                                        --%>
                                           <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged" DataPlaceHolder="Financial Year">
                                        </asp:DropDownListChosen>                                                       
                                    </div>    
                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                     
                                       <asp:DropDownList runat="server" ID="ddlSchedulingType" 
                                         class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                            <asp:ListItem>Annually</asp:ListItem>
                                            <asp:ListItem>Half Yearly</asp:ListItem>
                                            <asp:ListItem Selected="True">Quarterly</asp:ListItem>
                                        </asp:DropDownList>

                                     

                                    </div>                                                                       
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                     
                                        <%--<asp:DropDownList runat="server" ID="ddlProcess" AutoPostBack="true" OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged" 
                                             class="form-control m-bot15 select_location" 
                                            Style="float: left; width:90%;">
                                        </asp:DropDownList>--%>
                                          <asp:DropDownListChosen runat="server" ID="ddlProcess" DataPlaceHolder="Process" AutoPostBack="true"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged" class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                    </asp:DropDownListChosen>

                                    </div> 
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                     
                                      <%--  <asp:DropDownList runat="server" ID="ddlSubProcess"  class="form-control m-bot15 select_location" 
                                        Style="float: left; width:90%;">
                                        </asp:DropDownList>--%>

                                          <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Process">
                                    </asp:DropDownListChosen>  
                                    </div> 
                                                                                             
                                </div> 
                                     <div class="col-md-12 colpadding0"> 
                                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; float:right;width: 13%;"> 
                                        <asp:Button ID="btnFilter" class="btn btn-search" runat="server" OnClick="btnTopSearch_Click" Text="Apply Filter" />                          
                                    </div>
                                         </div>
                             
                   <div class="clearfix"></div>                                    
                  
                    <div class="col-md-12 colpadding0" style="float: left;">

                        <div class="col-md-6 colpadding0 entrycount" style="display:none;">                         
                        <h5 style="font-weight:bold;">Select Your Risk Color Preference</h5>
                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                <input id="High" style="display: none;"/> 
                            </div>
                            <div class="col-md-3 colpadding0">
                                <h5 class="colpadding0">High</h5>
                            </div>

                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                              <input id="medium" style="display: none;"/> 
                            </div>
                            <div class="col-md-3 colpadding0">
                                <h5 class="colpadding0">Medium</h5>
                            </div>

                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                             <input id="low" style="display: none;"/> 
                            </div>
                            <div class="col-md-3 colpadding0">
                                <h5 class="colpadding0">Low</h5>
                            </div>
                         </div>

                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">  

                         </div>
                    </div>
                </div>    
                              </div>
                        </div>
                    </div>
                </div>     
                           </div>                            
                        </ContentTemplate>      
                     <Triggers>
                         <asp:PostBackTrigger ControlID="btnFilter" />
                         <asp:AsyncPostBackTrigger EventName="SelectedIndexChanged" ControlID="ddlLegalEntity" />
                     </Triggers>                    
                 </asp:UpdatePanel> 

                <div class="clearfix"></div>
                <asp:HiddenField ID="highcolor" runat="server" Value="#FF0000" /> <%-- #FF7473--%>
                <asp:HiddenField ID="mediumcolor" runat="server" Value="#FFFF00"/><%--#FFC952--%>
                <asp:HiddenField ID="lowcolor" runat="server" Value="#008000"/> <%--#1FD9E1--%> 
             

                 <!-- ObservationStatus Start -->
                <div id="ObservationStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Test Results </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseObsStatus"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('ObservationStatus')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseObsStatus" class="panel-collapse collapse in">     
                                 <div class="panel-body">
                                    <div class="col-md-12">
                                       
                                        <div id="perStatusPieChartDivAPRJUN" runat="server">                                            
                                        </div>

                                   <%--<div id="perStatusPieChartDivAPRJUN" runat="server" class="col-md-5" style="width:25%; height:300px;" >                                            
                                        </div>--%>

                                        <div id="perStatusPieChartDivJULSEP" runat="server">                                            
                                        </div>

                                        <div id="perStatusPieChartDivOCTDEC" runat="server">                                            
                                        </div>

                                        <div id="perStatusPieChartDivJANMAR" runat="server">                                            
                                        </div> 

                                    </div>                                   
                                </div>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>               
                 <!-- ObservationStatus End -->

                 <div class="clearfix" style="height:10px;"></div>
               
                 <!-- Quarterly TestResults Start -->
                <div id="QuarterlyFailedControlStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Failed Controls </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseQuarterlyFailedControlStatus"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('QuarterlyFailedControlStatus')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseQuarterlyFailedControlStatus" class="panel-collapse collapse in">     
                                 <div class="panel-body">
                                    <div class="col-md-12">
                                       
                                        <div id="QuarterlyFailedControlColumnChart" class="col-md-5" style="width:100%; height:300px;">      

                                    </div>                                   
                                </div>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>  
                     </div>               
                 <!--  Quarterly TestResults  End -->               

                 <div class="clearfix" style="height:10px;"></div>

                 <!-- Process Wise Observation Status Start -->
                <div id="ProcessWiseObservationStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Process Wise - Test Results </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseProcessObsStatus"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('ObservationStatus')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseProcessObsStatus" class="panel-collapse collapse in">     
                                 <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-2"></div>
                                        <div id="DivGraphProcessObsStatus" class="col-md-12" runat="server" style="height: 250px; float:left;">                                           
                                        </div>
                                    </div>                                   
                                </div>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>               
                  <!-- Process Wise Observation Status End -->     
                    
                    <div class="clearfix" style="height:10px;"></div>

                 <!-- Process Wise Failed Controls Status Start -->
                <div id="ProcessWiseFailedControlStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Process Wise - Failed Controls </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseProcessWiseFailedControlStatus"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('ProcessWiseFailedControlStatus')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseProcessWiseFailedControlStatus" class="panel-collapse collapse in">     
                                 <div class="panel-body">
                                    <div class="col-md-12">
                                        
                                        <div id="DivProcessWiseFailedControlColumnChart" style="height: 350px; margin: 0 auto"></div>
                                       <%-- <div id="DivProcessWiseFailedControlColumnChart" class="col-md-12" runat="server" style="height: 350px; overflow-y: auto;">                                           
                                        </div>--%>
                                    </div>                                   
                                </div>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>               
                  <!-- Process Wise Failed Controls Status End -->  
                                                       
                <div class="modal fade" id="DivReports" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                        <div class="modal-dialog" style="width:90%;">          
                            <div class="modal-content" style="width:100%;">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>

                                <div class="table-responsive" style="width:100%;">              
                                    <iframe id="showdetails" src="about:blank" width="1150px" height="100%" frameborder="0"></iframe>                                      
                                </div>
                            </div>
                        </div>
                   </div>

            </section>       
    </section>

</asp:Content>
