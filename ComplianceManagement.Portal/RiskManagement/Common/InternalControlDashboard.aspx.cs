﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common
{
    public partial class InternalControlDashboard : System.Web.UI.Page
    {
        protected bool prerqusite = false;
        protected List<Int32> roles;
        protected static string IsHiddenFY;
        protected static string IsHiddenCustomerId;
        protected void Page_Load(object sender, EventArgs e)
        {
            roles = CustomerManagementRisk.ARSGetAssignedRolesBOTH(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                BindCustomerList();
                List<int?> CustomerIdsList = new List<int?>();
                CustomerIdsList.Clear();
                List<int> CustomerIdsList1 = new List<int>();
                CustomerIdsList1.Clear();

                for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                {
                    if (ddlCustomerMultiSelect.Items[i].Selected)
                    {
                        CustomerIdsList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                        CustomerIdsList1.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                    }
                }
                if (CustomerIdsList.Count > 0)
                {
                    IsHiddenCustomerId = string.Join(",", CustomerIdsList);
                }
                BindFinancialYear();
                List<string> FinancialYearList = new List<string>();
                FinancialYearList.Clear();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (!string.IsNullOrEmpty(Request.QueryString["FinancialYear"]))
                {
                    string finYear = string.Empty;
                    finYear = Request.QueryString["FinancialYear"].ToString();
                    IsHiddenFY = Request.QueryString["FinancialYear"].ToString();
                    for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                    {
                        if (ddlFinancialYearMultiSelect.Items[i].Text == finYear)
                        {
                            ddlFinancialYearMultiSelect.Items[i].Selected = true;
                            FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                        }
                    }
                }
                else
                {
                    if (FinancialYear != null)
                    {
                        for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                        {
                            if (ddlFinancialYearMultiSelect.Items[i].Text == FinancialYear)
                            {
                                ddlFinancialYearMultiSelect.Items[i].Selected = true;
                                FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                            }
                        }
                    }
                }
                IsHiddenFY = string.Join(",", FinancialYearList);

                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                home.Value = "true";
                bindcount();
                if (prerqusite)
                {
                    if (roles.Count() <= 0)
                    {
                        ddlCustomer.Visible = false;
                    }
                }
            }
        }

        public void bindcount()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<string> FinancialYearList = new List<string>();
                for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                {
                    if (ddlFinancialYearMultiSelect.Items[i].Selected)
                    {
                        FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                    }
                }
                List<SP_PrequsiteCount_Result> Masterrecord = new List<SP_PrequsiteCount_Result>();

                List<int?> CustomerIdsList = new List<int?>();
                CustomerIdsList.Clear();
                List<long> CustomerIdsList1 = new List<long>();
                CustomerIdsList1.Clear();
                for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                {
                    if (ddlCustomerMultiSelect.Items[i].Selected)
                    {
                        CustomerIdsList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                        CustomerIdsList1.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                    }
                }
                var dataAuditSummary = (from c in entities.AuditCountView_Dashboard
                                        select c).ToList();
                dataAuditSummary = dataAuditSummary.Where(e => CustomerIdsList.Contains(e.CustomerID)).ToList();

                var dataImplimentInternal = (from c in entities.SP_ImplimentInternalAuditClosedCountViewForMultiSelectDDL()
                                             where CustomerIdsList.Contains(c.CustomerID)
                                             select c).ToList();


                entities.Database.CommandTimeout = 300;
                Masterrecord = (from row in entities.SP_PrequsiteCount(Portal.Common.AuthenticationHelper.UserID)
                                where FinancialYearList.Contains(row.FinancialYear)
                                select row).ToList();


                if (FinancialYearList.Count > 0)
                {
                    dataAuditSummary = dataAuditSummary.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                    dataImplimentInternal = dataImplimentInternal.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                    Masterrecord = Masterrecord.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();

                    divOpenAuditCount.InnerText = GetAuditCount(dataAuditSummary, Portal.Common.AuthenticationHelper.UserID, 1, CustomerIdsList1).ToString();
                    divCloseAuditCount.InnerText = GetAuditCount(dataAuditSummary, Portal.Common.AuthenticationHelper.UserID, 3, CustomerIdsList1).ToString();

                    divOpenAuditIMPCount.InnerText = GetAuditCountIMP(dataImplimentInternal, Portal.Common.AuthenticationHelper.UserID, 1).ToString();
                    divCloseAuditIMPCount.InnerText = GetAuditCountIMP(dataImplimentInternal, Portal.Common.AuthenticationHelper.UserID, 3).ToString();
                }
                else
                {
                    divOpenAuditCount.InnerText = "0";
                    divCloseAuditCount.InnerText = "0";
                    divOpenAuditIMPCount.InnerText = "0";
                    divCloseAuditIMPCount.InnerText = "0";
                }

               

                if (Masterrecord.Count > 0)
                {
                    prerqusite = true;
                    var opencount = Masterrecord.Where(entry => entry.DocStatus == "Open").ToList().Count;
                    var submittedcount = Masterrecord.Where(entry => entry.DocStatus == "Submitted" || entry.DocStatus == "Approved" || entry.DocStatus == "Re-Submitted").ToList().Count;
                    var Rejectedcount = Masterrecord.Where(entry => entry.DocStatus == "Rejected").ToList().Count;
                    divPreRequisiteOpenAuditCount.InnerText = Convert.ToString(opencount);
                    divPreRequisiteSubmittedAuditCount.InnerText = Convert.ToString(submittedcount);
                    divPreRequisiteRejectedCount.InnerText = Convert.ToString(Rejectedcount);
                }
                else
                {
                    prerqusite = false;
                    divPreRequisiteOpenAuditCount.InnerText = "0";
                    divPreRequisiteSubmittedAuditCount.InnerText = "0";
                    divPreRequisiteRejectedCount.InnerText = "0";
                }
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        public void BindFinancialYear()
        {
            ddlFinancialYearMultiSelect.DataTextField = "Name";
            ddlFinancialYearMultiSelect.DataValueField = "ID";
            ddlFinancialYearMultiSelect.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYearMultiSelect.DataBind();
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    IsHiddenFY = ddlFinancialYear.SelectedItem.Text;
                }
            }
            bindcount();
        }

        public static int GetAuditCountIMP(List<SP_ImplimentInternalAuditClosedCountViewForMultiSelectDDL_Result> ImplimentInternalAuditClosedRecords, int userid, int statusid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;

                if (statusid == 1)
                {
                    entities.Database.CommandTimeout = 300;
                    int flag = 0;
                    var OpenImplimentCount = (from row in ImplimentInternalAuditClosedRecords
                                              where row.UserID == userid
                                              && row.AllClosed != flag
                                              select row.AuditID).Distinct().ToList();

                    count = OpenImplimentCount.Count();
                }
                else if (statusid == 3)
                {
                    entities.Database.CommandTimeout = 300;
                    int flag = 0;
                    var closedImplimentCount = (from row in ImplimentInternalAuditClosedRecords
                                                where row.UserID == userid
                                                && row.AllClosed == flag
                                                select row.AuditID).Distinct().ToList();
                    count = closedImplimentCount.Count;
                }
                return count;
            }
        }
        public static int GetAuditCount(List<AuditCountView_Dashboard> AuditSummaryCountRecords, int userid, int statusid, List<long> customerIDList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();

                if (statusid == 1)
                {
                    entities.Database.CommandTimeout = 300;
                    record = (from C in AuditSummaryCountRecords
                              where C.UserID == userid
                              && C.ACCStatus == null
                              group C by new
                              {
                                  C.CustomerID,
                                  C.AuditID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  //C.RoleID,
                                  C.UserID,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  AuditID = GCS.Key.AuditID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  //RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                              }).ToList();
                    count = record.Count();

                }
                else if (statusid == 3)
                {
                    entities.Database.CommandTimeout = 300;
                    record = (from C in AuditSummaryCountRecords
                              where C.UserID == userid
                              //&& C.AuditStatusID == 3
                              && C.ACCStatus == 1
                              group C by new
                              {
                                  C.CustomerID,
                                  C.AuditID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  //C.RoleID,
                                  C.UserID,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  AuditID = GCS.Key.AuditID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  //RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                              }).ToList();


                    count = record.Count();
                }
                return count;
            }
        }


        public decimal GetImplementationPercentageAuditProcess()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditIMPCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditIMPCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }

        public decimal GetImplementationPercentageAuditProcessclose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditIMPCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditIMPCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }


        public decimal GetProcessPercentageAuditProcess()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }

        public decimal GetProcessPercentageAuditProcessclose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }

        private void BindCustomerList()
        {
            long userId = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            var customerList = RiskCategoryManagement.GetAllMappedCustomerForPerformer(userId);
            if (customerList.Count > 0)
            {
                ddlCustomerMultiSelect.DataTextField = "CustomerName";
                ddlCustomerMultiSelect.DataValueField = "CustomerId";
                ddlCustomerMultiSelect.Items.Clear();
                ddlCustomerMultiSelect.DataSource = customerList;
                ddlCustomerMultiSelect.DataBind();
                for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                {
                    ddlCustomerMultiSelect.Items[i].Selected = true;
                }
            }
            else
            {
                ddlCustomerMultiSelect.DataSource = null;
                ddlCustomerMultiSelect.DataBind();
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    IsHiddenCustomerId = ddlCustomer.SelectedValue.ToString();
                    bindcount();
                }
                else
                {
                    Response.Write("<script>alert('Please Select Customer.');</script>");
                    BindCustomerList();
                    bindcount();
                }
            }
        }

        #region added by sagar on 22-sept-2020
        protected void ddlCustomerMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<int> customerIdsList = new List<int>();
            for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
            {
                if (ddlCustomerMultiSelect.Items[i].Selected)
                {
                    customerIdsList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                }
            }
            if (customerIdsList.Count > 0)
            {
                IsHiddenCustomerId = string.Join(",", customerIdsList);
                bindcount();
            }
            else
            {
                Response.Write("<script>alert('Please Select Customer.');</script>");
                BindCustomerList();
                bindcount();
            }

        }

        protected void ddlFinancialYearMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> FinancialYearsList = new List<string>();
            for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
            {
                if (ddlFinancialYearMultiSelect.Items[i].Selected)
                {
                    FinancialYearsList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                }
            }
            if (FinancialYearsList.Count > 0)
            {
                IsHiddenFY = string.Join(",", FinancialYearsList);
            }
            else
            {
                Response.Write("<script>alert('Please Select Financial Year.');</script>");
            }
            bindcount();

        }
        #endregion

    }
}