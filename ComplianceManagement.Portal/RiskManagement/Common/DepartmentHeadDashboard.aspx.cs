﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Saplin.Controls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common
{
    public partial class DepartmentHeadDashboard : System.Web.UI.Page
    {
        protected bool checkPRFlag = false;
        protected List<long> CHKBranchlist = new List<long>();
        protected List<Int32> roles;
        protected static string ObservationStatusChart;
        protected static string ProcessWiseObservationStatusChart;
        protected static string ObservationProcessWiseAgingChart;
        protected static String[] items;
        public static List<long> Branchlist = new List<long>();
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            roles = CustomerManagementRisk.ARSGetAssignedRolesBOTH(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            items = new String[] { "Not Started", "In Progress", "Under Review", "Closed" };
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                HiddenField home = (HiddenField) Master.FindControl("Ishome");
                home.Value = "true";             
                using (AuditControlEntities entities = new AuditControlEntities())
                {                    
                    var branchid = CustomerManagementRisk.GetUsersApplicableBranch(Portal.Common.AuthenticationHelper.UserID);
                    checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(CustomerId, branchid);
                    
                    entities.Database.CommandTimeout = 300;
                    //var masterRecords = (from row in entities.PersonResponsibleAuditCountViews
                    //                     where row.CustomerID == CustomerId
                    //                     && row.PersonResponsible == Portal.Common.AuthenticationHelper.UserID
                    //                     select row).ToList();

                    var masterRecords = (from row in entities.sp_PersonResponsibleAuditCountView(Portal.Common.AuthenticationHelper.UserID)                                        
                                         select row).ToList();

                    var masterRecordsIMP = (from row in entities.ImplimentInternalPersonResponsibleAuditClosedCountViews
                                     where row.CustomerID == CustomerId && row.PersonResponsible == Portal.Common.AuthenticationHelper.UserID
                                            select row).ToList();

                    //Person Responsible Summary Count
                    divOpenAuditCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditCount(masterRecords,1).ToString();
                    divCloseAuditCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditCount(masterRecords, 3).ToString();

                    divOpenAuditIMPCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditCountIMP(masterRecordsIMP, 1).ToString();
                    divCloseAuditIMPCount.InnerText = DashboardManagementRisk.GetPersonResponsibleAuditCountIMP(masterRecordsIMP, 3).ToString();
                }
                // Dashboard Data              
                BindProcess("P");
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData();

                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                BindGraphData();             
                ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
            }
        }
        public decimal GetPercentageImpOpen()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditIMPCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditIMPCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageImpClose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditIMPCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditIMPCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageProcessClose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageProcessOpen()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        private void BindProcess(string flag)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ddlProcess.Items.Clear();
                ddlProcess.DataTextField = "Name";
                ddlProcess.DataValueField = "Id";
                ddlProcess.DataSource = ProcessManagement.FillProcessDropdownDepartmentHead(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                ddlProcess.DataBind();
                ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindVertical()
        {
            try
            {
                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa();
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityDataList(DropDownCheckBoxes DRP, List<long> ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityDataList(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(long customerID, List<long> customerbranchlist)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && customerbranchlist.Contains(row.ID)
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(long customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);

                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(long customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindGraphData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            CHKBranchlist.Clear();

            if (ddlLegalEntity.SelectedValue != "-1")
            {
                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindSubEntityDataList(ddlSubEntity1, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                }

                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //Region
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                {
                    if (ddlSubEntity1.Items[i].Selected)
                    {
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1.Items[i].Value));
                    }
                }
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            BindSubEntityDataList(ddlSubEntity2, CHKBranchlist);
            BindSchedulingType();
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //State
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                {
                    if (ddlSubEntity2.Items[i].Selected)
                    {
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2.Items[i].Value));
                    }
                }
                BindSubEntityDataList(ddlSubEntity3, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            BindSchedulingType();
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //Hub
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                {
                    if (ddlSubEntity3.Items[i].Selected)
                    {
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3.Items[i].Value));
                    }
                }
                BindSubEntityDataList(ddlFilterLocation, CHKBranchlist);
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            BindSchedulingType();

            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //Loction
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            CHKBranchlist.Clear();
            for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
            {
                if (ddlFilterLocation.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocation.Items[i].Value));
                }
            }
            BindSchedulingType();
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }

            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        public void BindGraphData()
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                int branchid = -1;
                int ProcessID = -1;
                int VerticalID = -1;
                string FinYear = string.Empty;
                string Period = string.Empty;
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    CHKBranchlist.Clear();
                    for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                    {
                        if (ddlSubEntity1.Items[i].Selected)
                        {
                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1.Items[i].Value));
                        }
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    CHKBranchlist.Clear();
                    for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                    {
                        if (ddlSubEntity2.Items[i].Selected)
                        {
                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2.Items[i].Value));
                        }
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    CHKBranchlist.Clear();
                    for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                    {
                        if (ddlSubEntity3.Items[i].Selected)
                        {
                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3.Items[i].Value));
                        }
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    CHKBranchlist.Clear();
                    for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
                    {
                        if (ddlFilterLocation.Items[i].Selected)
                        {
                            CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocation.Items[i].Value));
                        }
                    }
                }

                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }

                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        ProcessID = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlVertical.SelectedValue))
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }
                List<int?> ListofBranch = new List<int?>();
                if (CHKBranchlist.Count > 0)
                {
                    Branchlist.Clear();
                    GetAllHierarchy(CustomerId, CHKBranchlist);
                    Session["BranchList"] = null;
                    if (Branchlist.Count > 0)
                    {
                        ListofBranch = Branchlist.Select(x => (int?) x).ToList();
                        Session["BranchList"] = ListofBranch;
                    }
                }
                else
                {
                    Branchlist.Clear();
                    GetAllHierarchy(CustomerId, branchid);
                    Session["BranchList"] = null;
                    if (Branchlist.Count > 0)
                    {
                        ListofBranch = Branchlist.Select(x => (int?) x).ToList();
                        Session["BranchList"] = ListofBranch;
                    }
                }

                using (AuditControlEntities entities = new AuditControlEntities())
                {


                    entities.Database.CommandTimeout = 300;
                    var dataAuditSummary = (from c in entities.sp_GetDepartmentHeadDashboardAudits(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID)
                                            select c).ToList();

                    entities.Database.CommandTimeout = 300;


                    var dataObservationStatus = (from row in entities.ObservationStatus_DisplayView_New
                                                 join EADH in entities.EntitiesAssignmentDepartmentHeads
                                                 on row.DepartmentID equals (int)EADH.DepartmentID
                                                 join MSP in entities.Mst_Process
                                                 on row.ProcessId equals MSP.Id
                                                 where EADH.DepartmentID == MSP.DepartmentID
                                                 && row.CustomerBranchID == EADH.BranchID
                                                 && EADH.UserID == Portal.Common.AuthenticationHelper.UserID
                                                 && EADH.ISACTIVE == true
                                                 select row).Distinct().ToList();


                    entities.Database.CommandTimeout = 300;
                    var dataAuditPlanned = (from c in entities.SP_AuditPlannedActualDetailDisplay(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID)
                                            select c).ToList();

                    if (dataAuditPlanned.Count > 0)
                    {
                        dataAuditPlanned = dataAuditPlanned.GroupBy(a => a.AuditID).Select(a => a.First()).ToList();
                    }


                    //var dataObservationAging = (from C in entities.ObservationAging_DisplayView
                    //                             join EADH in entities.EntitiesAssignmentDepartmentHeads
                    //                             on C.CustomerBranchID equals (int)EADH.BranchID
                    //                             join ICAA in entities.InternalControlAuditAssignments
                    //                             on C.AuditId equals ICAA.AuditID
                    //                             join MP in entities.Mst_Process
                    //                             on EADH.DepartmentID equals (long)MP.DepartmentID
                    //                             where MP.Id == ICAA.ProcessId
                    //                             && C.DepartmentID == EADH.DepartmentID
                    //                             && C.CustomerID == CustomerId
                    //                             && EADH.UserID == Portal.Common.AuthenticationHelper.UserID
                    //                             && EADH.ISACTIVE == true
                    //                             select C).ToList();


                    var dataObservationAging = (from row in entities.ObservationAging_DisplayView
                                                join EADH in entities.EntitiesAssignmentDepartmentHeads
                                                on row.DepartmentID equals (int)EADH.DepartmentID
                                                join MSP in entities.Mst_Process
                                                on row.ProcessId equals MSP.Id
                                                where EADH.DepartmentID == MSP.DepartmentID
                                                && row.CustomerBranchID == EADH.BranchID
                                                && EADH.UserID == Portal.Common.AuthenticationHelper.UserID
                                                && EADH.ISACTIVE == true
                                                select row).Distinct().ToList();


                    GetObservationStatusDashboardCount(dataObservationStatus, CustomerId, ListofBranch, FinYear, Period, ProcessID, VerticalID);

                    GetProcessWiseObservationDashboardCount(dataObservationStatus, CustomerId, ListofBranch, FinYear, Period, ProcessID, VerticalID);

                    GetAuditTrackerSummary(dataAuditPlanned, CustomerId, ListofBranch, FinYear, Period, VerticalID);

                    GetAuditObservationAgingSummary(dataObservationAging, CustomerId, ListofBranch, FinYear, Period, ProcessID, VerticalID);

                    GetAuditStatusSummary(dataAuditSummary, CustomerId, ListofBranch, FinYear, Period, VerticalID);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<Sp_GetDepartmentHeadProcess_Result> GetProcess(int UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Sp_GetDepartmentHeadProcess_Result> query = new List<Sp_GetDepartmentHeadProcess_Result>();
                query = (from row in entities.Sp_GetDepartmentHeadProcess(UserID)
                         select row).Distinct().ToList();
                return query;
            }
        }
        //public static List<Mst_Process> GetProcess(string flag, long Customerid, List<long> Branchid, List<long> Departements)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        List<Mst_Process> query = new List<Mst_Process>();

        //        if (Branchid.Count > 0)
        //        {
        //            query = (from row in entities.RiskActivityTransactions
        //                     join row1 in entities.Mst_Process
        //                     on row.ProcessId equals row1.Id
        //                     where row1.IsDeleted == false
        //                     && row1.CustomerID == Customerid 
        //                     && Branchid.Contains(row.CustomerBranchId)
        //                     select row1).Distinct().ToList();

        //            query = query.Where(entry => Departements.Contains((long) entry.DepartmentID)).ToList();

        //            if (query.Count == 0)
        //            {
        //                query = (from row in entities.Mst_Process
        //                         where row.CustomerID == Customerid  && Departements.Contains((long) row.DepartmentID)
        //                         select row).Distinct().ToList();
        //            }
        //        }
        //        else
        //        {
        //            query = (from row in entities.Mst_Process
        //                     where row.CustomerID == Customerid && Departements.Contains((long) row.DepartmentID)
        //                     select row).Distinct().ToList();
        //        }
        //        if (flag == "N")
        //        {
        //            query = query.Where(entry => entry.IsProcessNonProcess == "N").ToList();
        //        }
        //        else
        //        {
        //            query = query.Where(entry => entry.IsProcessNonProcess == "P").ToList();
        //        }

        //        return query;
        //    }
        //}

        public static long CheckAuditStatusReviewWorkingManagementDashboard(List<Sp_InternalAuditInstanceTransactionView_Result> InternalAuditInstanceTransactionViewRecord, String FinYear, String Period, long Customerid, int CustBranchid, int VerticalID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int Result = 0;
                int TotalATBD = 0;

                List<int?> ReviewStatusList = new List<int?> { 3, 4, 5, 6 };

                var Records = (from row in InternalAuditInstanceTransactionViewRecord
                               where row.CustomerID == Customerid
                               && row.CustomerBranchID == CustBranchid
                               && row.VerticalID == VerticalID
                               && row.FinancialYear == FinYear
                               && row.ForMonth == Period
                               && row.AuditID == AuditID
                               && row.RoleID == 4
                               select row).ToList();

                var RecordsClose = (from row in InternalAuditInstanceTransactionViewRecord
                                    where row.CustomerID == Customerid
                                    && row.CustomerBranchID == CustBranchid
                                    && row.VerticalID == VerticalID
                                    && row.FinancialYear == FinYear
                                    && row.ForMonth == Period
                                     && row.AuditID == AuditID
                                    && row.RoleID == 4 && row.AuditStatusID == 3
                                    select row).ToList();

                var Records1 = (from row in InternalAuditInstanceTransactionViewRecord
                                where row.CustomerID == Customerid
                                && row.CustomerBranchID == CustBranchid
                                && row.VerticalID == VerticalID
                                && row.FinancialYear == FinYear
                                && row.ForMonth == Period
                                 && row.AuditID == AuditID
                                && row.RoleID == 4 && ReviewStatusList.Contains(row.AuditStatusID)
                                select row).ToList();

                var auditRecords = (from row in entities.AuditClosureDetails
                                    where row.ID == AuditID
                                    select row).FirstOrDefault();

                if (auditRecords != null)
                {
                    if (auditRecords.Total != null)
                    {
                        TotalATBD = Convert.ToInt32(auditRecords.Total);
                    }
                }

                if (Records.Count > 0)
                {

                    var ReviewedATBD2 = Records.Where(Entry => Entry.AuditStatusID == 2).Count();
                    var ReviewedATBD3 = RecordsClose.Count();
                    var ReviewedATBD4 = Records1.Count();


                    var reviewedPer = ((Convert.ToDecimal(ReviewedATBD4) / Convert.ToDecimal(TotalATBD)) * 100);
                    var closedPer = ((Convert.ToDecimal(ReviewedATBD3) / Convert.ToDecimal(TotalATBD)) * 100);

                    if (closedPer >= 100)
                        Result = 4;
                    else if (reviewedPer >= 75 && reviewedPer <= 100)
                        Result = 2;
                    else if (ReviewedATBD2 > 0) //Submitted Count
                        Result = 1;
                    else
                        Result = 0;
                }

                return Result;
            }
        }
        public static List<int> GetDistinctAuditsStatusManagementDashboard(List<sp_GetDepartmentHeadDashboardAudits_Result> AuditSummaryCountRecord, String FinYear, String Period, long Customerid, int CustBranchid, int VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditStatusList = new List<int>();

                var record = (from row in entities.InternalAuditInstanceTransactionViews
                              where row.CustomerID == Customerid
                              && row.CustomerBranchID == CustBranchid
                              && row.VerticalID == VerticalID
                              && row.FinancialYear == FinYear
                              && row.ForMonth == Period
                              select (int)row.AuditStatusID).Distinct().ToList();
              

                return record;
            }
        }
        public void GetObservationStatusDashboardCount(List<ObservationStatus_DisplayView_New> ObservationStatus_DisplayView_New, long CustomerID, List<int?> BranchList, String FinYear, String Period, int ProcessID, int VerticalID)
        {
            try
            {
                ObservationStatusChart = string.Empty;
                string ObservationStatusChartSeries = string.Empty;
                string ObservationStatusChartDrillDown = string.Empty;

                long highcount = 0;
                long mediumCount = 0;
                long lowcount = 0;
                long totalcount = 0;

                ObservationStatusChartSeries = "series: [{ name: 'Category', colorByPoint: true, data: [";
                ObservationStatusChartDrillDown = "drilldown: { series: [ ";

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var ObservationList = ObservationSubcategory.GetObservationCategoryAll(CustomerID);

                    var Records = (from row in ObservationStatus_DisplayView_New
                                   where row.CustomerID == CustomerID                                  
                                   select row).ToList();

                    if (BranchList.Count > 0)
                        Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                 
                    if (FinYear != "")
                        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                    else
                    {
                        FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                    }

                    if (Period != "")
                        Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                    if (ProcessID != -1)
                        Records = Records.Where(Entry => Entry.ProcessId == ProcessID).ToList();

                    if (VerticalID != -1)
                        Records = Records.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                    foreach (Mst_ObservationCategory EachObsCat in ObservationList)
                    {
                        highcount = (from row in Records
                                     where row.ObservatioRating == 1
                                      && row.ObservationCategory == EachObsCat.ID
                                     select row).Count(); /*entities.ObservationStatus_DisplayView*/

                        mediumCount = (from row in Records
                                       where row.ObservatioRating == 2
                                        && row.ObservationCategory == EachObsCat.ID
                                       select row).Count();

                        lowcount = (from row in Records
                                    where row.ObservatioRating == 3
                                     && row.ObservationCategory == EachObsCat.ID
                                    select row).Count();

                        totalcount = highcount + mediumCount + lowcount;

                        ObservationStatusChartSeries += "{ name: '" + EachObsCat.Name + "', y: " + totalcount + ", drilldown: '" + EachObsCat.Name + "' },";

                        ObservationStatusChartDrillDown += "{ name: '" + EachObsCat.Name + "', id: '" + EachObsCat.Name + "'," +
                           " data: [['High'," + highcount + "],['Medium', " + mediumCount + "],['Low'," + lowcount + "]]," +
                            " events: {" +
                                " click: function(e){" +
                                " ShowObservationDetails(e.point.name," + CustomerID + ",-1,'" + FinYear + "'," + EachObsCat.ID + "," + ProcessID + ",'DH')}" +
                                " }},";
                    }

                    ObservationStatusChartSeries = ObservationStatusChartSeries.Trim(',');
                    ObservationStatusChartDrillDown = ObservationStatusChartDrillDown.Trim(',');
                    ObservationStatusChartSeries += "] }],";
                    ObservationStatusChartDrillDown += "] } ";

                    ObservationStatusChart = ObservationStatusChartSeries + ObservationStatusChartDrillDown;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void GetProcessWiseObservationDashboardCount(List<ObservationStatus_DisplayView_New> ObservationStatus_DisplayView_New, long CustomerID, List<int?> BranchList, String FinYear, String Period, int ProcessID, int VerticalID)
        {
            try
            {
                ProcessWiseObservationStatusChart = String.Empty;

                String ProcesswiseObservationSeries = String.Empty;
                String ListofProcesses = String.Empty;

                String HighCountList = String.Empty;
                String MediumCountList = String.Empty;
                String LowCountList = String.Empty;

                long highcount = 0;
                long mediumCount = 0;
                long lowcount = 0;
                long totalcount = 0;

                String newDivName = String.Empty;

                int CustomerBranchID = -1;

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var ProcessList = GetProcess(Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID));

                   
                    if (ProcessID != -1)
                        ProcessList = ProcessList.Where(Entry => Entry.ProcessId == ProcessID).ToList();

                    if (ProcessList.Count > 0)
                    {                      
                        int Count = 1;
                        ProcessList.ForEach(EachProcess =>
                        {
                            var Records = (from row in ObservationStatus_DisplayView_New
                                           where row.CustomerID == CustomerID                                         
                                           && row.ProcessId == EachProcess.ProcessId
                                           select row).ToList();

                            if (BranchList.Count > 0)
                                Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                          
                            if (FinYear != "")
                                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                            else
                            {
                                FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                            }

                            if (Period != "")
                                Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                            if (VerticalID != -1)
                                Records = Records.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                            highcount = (from row in Records
                                         where row.ObservatioRating == 1
                                         select row).Count();

                            mediumCount = (from row in Records
                                           where row.ObservatioRating == 2
                                           select row).Count();

                            lowcount = (from row in Records
                                        where row.ObservatioRating == 3
                                        select row).Count();

                            totalcount = highcount + mediumCount + lowcount;

                            if (totalcount != 0)
                            {
                                ListofProcesses += "'" + EachProcess.Name + "',";
                               
                                System.Web.UI.HtmlControls.HtmlGenericControl newDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                                newDivName = "DivProcessObs" + Count;
                                newDiv.ID = newDivName;

                                String newDivClientID = newDiv.ClientID;
                                                           
                                newDiv.Style["height"] = "200px";
                                newDiv.Style["width"] = "20%";

                                newDiv.Style["float"] = "left";

                                DivGraphProcessObsStatus.Controls.Add(newDiv); /*format: '<b>{point.name}</b>: {y}',*/

                             
                                ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "', { chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie',marginBottom: 30, }," +
                            "title: {text: '" + EachProcess.Name + "',style: {fontSize: '12px' }},  legend: { itemDistance:0,itemStyle:{fontSize:'10px'}, }, tooltip: { pointFormat: '{series.name}: <b>{point.y}</b>'}," +
                            "plotOptions: {pie: {size: '100%', showInLegend: true, allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, format: '{y}', distance: 1," +
                            "style: { color: (Highcharts.theme &&     Highcharts.theme.contrastTextColor) || 'black'} } } }," +
                            "series: [{ name: '" + EachProcess.Name + "', colorByPoint: true, " +
                            "data: [{ name: 'High', color: '#f45b5b', y: " + highcount + ", events:{click: function(e) { ShowObservationDetails(e.point.name," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "',-1," + EachProcess.ProcessId + ",'DH') } } }," +
                            " { name: 'Medium', color: '#e4d354', y:" + mediumCount + ", events:{click: function(e) { ShowObservationDetails(e.point.name," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "',-1," + EachProcess.ProcessId + ",'DH') } } }," +
                            " { name: 'Low', color: '#90ed7d', y:" + lowcount + ", events:{click: function(e) { ShowObservationDetails(e.point.name," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "',-1," + EachProcess.ProcessId + ",'DH') } } }] }] });";

                                ProcessWiseObservationStatusChart += ProcesswiseObservationSeries;

                                Count++;
                            }
                        });                                                
                    }
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        public void GetAuditTrackerSummary(List<SP_AuditPlannedActualDetailDisplay_Result> AuditPlannedActualDetailDisplayRecord, long CustomerID, List<int?> BranchList, String FinYear, String Period, int VerticalID)
        {
            try
            {
                int TotalCount = 0;
                String PartialFromDate = String.Empty;
                String PartialToDate = String.Empty;
                String[] FinancialYear = new String[2];
                DataTable table = new DataTable();
                table.Columns.Add("Name", typeof(string));
                if (FinYear != "")
                {
                    FinancialYear = FinYear.Split('-');
                    PartialFromDate = "01/04/" + FinancialYear[0];
                    PartialToDate = "31/03/" + FinancialYear[1];
                }
                else
                {
                    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                    if (FinYear != "")
                    {
                        FinancialYear = FinYear.Split('-');
                        PartialFromDate = "01/04/" + FinancialYear[0];
                        PartialToDate = "31/03/" + FinancialYear[1];
                    }
                }
                DateTime FROMDate = Convert.ToDateTime(GetDate(PartialFromDate));
                DateTime TODate = Convert.ToDateTime(GetDate(PartialToDate));

                for (int i = 0; i < 12; i++)
                {
                    table.Columns.Add(FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy"), typeof(String));
                }
                table.Columns.Add("Total", typeof(string));
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    //Planned Audits
                    DataRow tableRow = table.NewRow();
                    tableRow["Name"] = "Planned";

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime MonthStartDate = FROMDate.AddMonths(i);
                        DateTime MonthEndDate = InternalControlManagementDashboardRisk.GetLastDayOfMonth(MonthStartDate);

                        var transactionsQuery = (from row in AuditPlannedActualDetailDisplayRecord
                                                 where MonthStartDate <= row.StartDate
                                                 && row.StartDate <= MonthEndDate                                                                             
                                                 select row).ToList();


                        if (BranchList.Count > 0)
                            transactionsQuery = transactionsQuery.Where(Entry => BranchList.Contains(Entry.CustomerBranchId)).ToList();

                       
                        if (Period != "")
                            transactionsQuery = transactionsQuery.Where(entry => entry.ForMonth == Period).ToList();

                      
                        if (VerticalID != -1)
                            transactionsQuery = transactionsQuery.Where(entry => entry.VerticalID == VerticalID).ToList();

                        tableRow[FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy")] = transactionsQuery.Count;

                        TotalCount += transactionsQuery.Count;
                    }

                    tableRow["Total"] = TotalCount;
                    table.Rows.Add(tableRow);


                    //Actual Audits
                    TotalCount = 0;
                    DataRow tableActualRow = table.NewRow();

                    tableActualRow["Name"] = "Actual";

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime MonthStartDate = FROMDate.AddMonths(i);
                        DateTime MonthEndDate = GetLastDayOfMonth(MonthStartDate);

                        var transactionsQuery = (from row in AuditPlannedActualDetailDisplayRecord
                                                 where MonthStartDate <= row.ExpectedStartDate
                                                  && row.ExpectedStartDate <= MonthEndDate                                                 
                                                 select row).ToList();



                        if (BranchList.Count > 0)
                            transactionsQuery = transactionsQuery.Where(Entry => BranchList.Contains(Entry.CustomerBranchId)).ToList();
                      
                        if (Period != "")
                            transactionsQuery = transactionsQuery.Where(entry => entry.ForMonth == Period).ToList();

                        if (VerticalID != -1)
                            transactionsQuery = transactionsQuery.Where(entry => entry.VerticalID == VerticalID).ToList();

                        tableActualRow[FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy")] = transactionsQuery.Count;

                        TotalCount += transactionsQuery.Count;
                    }

                    tableActualRow["Total"] = TotalCount;
                    table.Rows.Add(tableActualRow);

                    grdAuditTrackerSummary.DataSource = table;
                    grdAuditTrackerSummary.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        public static List<sp_GetDepartmentHeadDashboardAudits_Result> GetDepartmentHeadDashboardAudits(List<sp_GetDepartmentHeadDashboardAudits_Result> AuditSummaryCountRecord, List<int?> BranchList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<sp_GetDepartmentHeadDashboardAudits_Result> record = new List<sp_GetDepartmentHeadDashboardAudits_Result>();

                record = (from C in AuditSummaryCountRecord                                                   
                          select  C).ToList();

               
                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains((int)Entry.CustomerBranchID)).ToList();
             
                if (record.Count > 0)
                    record = record.OrderBy(entry => entry.Branch)
                        .ThenBy(entry => entry.VerticalName).ToList();

                return record;
            }
        }
              
        public void GetAuditStatusSummary(List<sp_GetDepartmentHeadDashboardAudits_Result> AuditSummaryCountRecord, int CustomerID, List<int?> BranchList, String FinYear, String Period, int VerticalID)
        {
            try
            {
                String newDivName = String.Empty;
                String newListName = String.Empty;
                String newLableName = String.Empty;

                int Count = 1;

                String[] FinancialYear = new String[2];

                if (FinYear == "")
                    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var AuditList = GetDepartmentHeadDashboardAudits(AuditSummaryCountRecord, BranchList);

                    var MasterRecords = (from row in entities.Sp_InternalAuditInstanceTransactionView(CustomerID, 4)
                                             //where row.CustomerID == CustomerID                                 
                                             //&& row.RoleID == 4
                                         select row).ToList();

                    if (AuditList.Count > 0)
                    {
                        AuditList.ForEach(EachAudit =>
                        {
                            //Create New Master Div To Contains Div that Contains Label and BulletedList 
                            System.Web.UI.HtmlControls.HtmlGenericControl newDivMaster = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                            newDivName = "DivAuditStatusMaster" + Count;
                            newDivMaster.ID = newDivName;

                            newDivMaster.Style["width"] = "100%";
                            newDivMaster.Style["float"] = "left";

                            //Create New Div To Contains Label 
                            System.Web.UI.HtmlControls.HtmlGenericControl newDivLabel = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                            newDivName = "DivAuditStatusLabel" + Count;
                            newDivLabel.ID = newDivName;
                            newDivLabel.Style["margin-top"] = "25px";
                            newDivLabel.Style["width"] = "50%";//"35%";
                            newDivLabel.Style["float"] = "left";

                            //Create New Label
                            System.Web.UI.WebControls.Label newLabel = new System.Web.UI.WebControls.Label();
                            newLableName = "Label" + Count;
                            newLabel.ID = newLableName;
                            newLabel.Font.Bold = true;

                            //Create New Div To Contains BulletedList
                            System.Web.UI.HtmlControls.HtmlGenericControl newDivBulletList = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                            newDivName = "DivAuditStatusBulletedList" + Count;
                            newDivBulletList.ID = newDivName;
                            newDivBulletList.Style["width"] = "50%"; //"65%";
                            newDivBulletList.Style["float"] = "left";

                            //Create New BulletedList
                            System.Web.UI.WebControls.BulletedList newBulletedList = new System.Web.UI.WebControls.BulletedList();
                            newListName = "BulletedList" + Count;
                            newBulletedList.ID = newListName;
                            newBulletedList.CssClass = "progtrckr";
                            newBulletedList.BulletStyle = BulletStyle.Numbered;

                            var ProgressResult = CheckAuditStatusReviewWorkingManagementDashboard(MasterRecords, EachAudit.FinancialYear, EachAudit.ForMonth, (int)EachAudit.CustomerID, (int)EachAudit.CustomerBranchID, (int)EachAudit.VerticalId, (int)EachAudit.AuditID);
                            newLabel.Text = EachAudit.Branch + "/" + EachAudit.VerticalName + "/" + EachAudit.FinancialYear + "/" + EachAudit.ForMonth;
                            AddSteps(items, newBulletedList);
                            SetProgress(ProgressResult, newBulletedList);
                            newDivLabel.Controls.Add(newLabel);
                            newDivBulletList.Controls.Add(newBulletedList);
                            newDivMaster.Controls.Add(newDivLabel);
                            newDivMaster.Controls.Add(newDivBulletList);
                            DivGraphAuditStatus.Controls.Add(newDivMaster);

                            Count++;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void GetAuditObservationAgingSummary(List<ObservationAging_DisplayView> ObservationAging_DisplayRecord, long CustomerID, List<int?> BranchList, String FinYear, String Period, int ProcessID, int VerticalID)
        {
            try
            {
                ObservationProcessWiseAgingChart = String.Empty;

                String ProcesswiseObservationAgingSeries = String.Empty;
                String ListofProcesses = String.Empty;

                String NotDueList = String.Empty;
                String LessThan45List = String.Empty;
                String GreaterThan45andLessThan90List = String.Empty;
                String GreaterThan90List = String.Empty;

                long NotDuecount = 0;
                long LessThan45Count = 0;
                long GreaterThan45andLessThan90count = 0;
                long GreaterThan90count = 0;

                String newDivName = String.Empty;
                int CustomerBranchID = -1;
                List<int?> ImplementedStatusList = new List<int?> { 2, 4, 5 };

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var ProcessList = GetProcess(Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID));

                    if (ProcessID != -1)
                        ProcessList = ProcessList.Where(Entry => Entry.ProcessId == ProcessID).ToList();

                    if (ProcessList.Count > 0)
                    {
                        int Count = 1;

                        ProcessList.ForEach(EachProcess =>
                        {
                            var Records = (from row in ObservationAging_DisplayRecord
                                           where row.CustomerID == CustomerID
                                           && row.ProcessId == EachProcess.ProcessId
                                           select row).ToList();


                            if (BranchList.Count > 0)
                                Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                            if (FinYear != "")
                                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                            else
                            {
                                FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                            }

                            if (Period != "")
                                Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                            if (VerticalID != -1)
                                Records = Records.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                            if (Records.Count > 0)
                            {

                                NotDuecount = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.Date && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                                LessThan45Count = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-45) && Entry.TimeLine < DateTime.Now.Date && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                                GreaterThan45andLessThan90count = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-90) && Entry.TimeLine < DateTime.Now.AddDays(-45) && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                                GreaterThan90count = Records.Where(Entry => Entry.TimeLine <= DateTime.Now.AddDays(-90) && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();


                                ListofProcesses += "'" + EachProcess.Name + "',";

                                System.Web.UI.HtmlControls.HtmlGenericControl newDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                                newDivName = "DivObsAging" + Count;
                                newDiv.ID = newDivName;

                                String newDivClientID = newDiv.ClientID;


                                newDiv.Style["height"] = "200px";
                                newDiv.Style["width"] = "20%";
                                newDiv.Style["float"] = "left";

                                DivObsAgingProcess.Controls.Add(newDiv); /*format: '<b>{point.name}</b>: {y}',*/


                                ProcesswiseObservationAgingSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "', { chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie',marginBottom: 30,  }," +
                                    "title: {text: '" + EachProcess.Name + "',style: {fontSize: '12px' }}, legend: { itemDistance:0,itemStyle:{fontSize:'10px'},}, tooltip: { pointFormat: '{series.name}: <b>{point.y}</b>'}," +
                                    "plotOptions: {pie: {size: '165%', showInLegend: true, allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, format: '{y}', distance: -25," +
                                    "style: { color: (Highcharts.theme &&     Highcharts.theme.contrastTextColor) || 'black'} }, startAngle: -90, endAngle: 90,center: ['50%', '75%'] } }," +
                                    "series: [{ type: 'pie', name: '" + EachProcess.Name + "', innerSize: '50%', colorByPoint: true, " +
                                    "data: [{ name: 'Not Due', 	color: '#7cb5ec', y: " + NotDuecount + ", events:{click: function(e) { ShowOpenObservationDetails('NotDue'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "'," + EachProcess.ProcessId + "," + VerticalID + ",'DH') } } }," +
                                    " { name: '0-45 Days', color: '#90ed7d', y:" + LessThan45Count + ", events:{click: function(e) { ShowOpenObservationDetails('LessThan45'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "'," + EachProcess.ProcessId + "," + VerticalID + ",'DH') } } }," +
                                    " { name: '46-90 Days', color: '#e4d354', y: " + GreaterThan45andLessThan90count + ", events: { click: function(e) { ShowOpenObservationDetails('GreaterThan45'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "'," + EachProcess.ProcessId + "," + VerticalID + ",'DH') } } }," +
                                    " { name: '>90 Days', color: '#f45b5b', y:" + GreaterThan90count + ", events:{click: function(e) { ShowOpenObservationDetails('GreaterThan90'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "'," + EachProcess.ProcessId + "," + VerticalID + ",'DH') } } }] }] });";

                                ObservationProcessWiseAgingChart += ProcesswiseObservationAgingSeries;

                                Count++;
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdAuditTrackerSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].Text = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int index = e.Row.RowIndex;
                    
                    int branchid = -1;
                    int VerticalID = -1;

                    String FinYear = String.Empty;
                    String Period = String.Empty;

                    String PartialFromDate = String.Empty;
                    String PartialToDate = String.Empty;
                    String[] FinancialYear = new String[2];

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                    {
                        if (ddlFinancialYear.SelectedValue != "-1")
                        {
                            FinYear = ddlFinancialYear.SelectedItem.Text;
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                    {
                        if (ddlPeriod.SelectedValue != "-1")
                        {
                            Period = ddlPeriod.SelectedItem.Text;
                        }
                    }

                    if (FinYear != "")
                    {
                        FinancialYear = FinYear.Split('-');

                        PartialFromDate = "01/04/" + FinancialYear[0];
                        PartialToDate = "31/03/" + FinancialYear[1];
                    }
                    else
                    {
                        FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                        if (FinYear != "")
                        {
                            FinancialYear = FinYear.Split('-');
                            PartialFromDate = "01/04/" + FinancialYear[0];
                            PartialToDate = "31/03/" + FinancialYear[1];
                        }
                    }

                    if (Period == "")
                        Period = "Period";

                    DateTime FROMDate = Convert.ToDateTime(GetDate(PartialFromDate));
                    DateTime TODate = Convert.ToDateTime(GetDate(PartialToDate));

                    for (int i = 1; i <= 12; i++)
                    {
                        DateTime MonthStartDate = FROMDate.AddMonths(i - 1);
                        DateTime MonthEndDate = GetLastDayOfMonth(MonthStartDate);
                        e.Row.Cells[i].Style.Add("font-weight", "500");
                        e.Row.Cells[i].Style.Add("cursor", "pointer");
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;

                        if (index == 0)
                            e.Row.Cells[i].Attributes.Add("onclick", "PlannedActualAuditDetails('Planned'," + branchid + ",'" + Period + "','" + MonthStartDate.Date + "','" + MonthEndDate.Date + "'," + VerticalID + ",'DH')");
                        else if (index == 1)
                            e.Row.Cells[i].Attributes.Add("onclick", "PlannedActualAuditDetails('Actual'," + branchid + ",'" + Period + "','" + MonthStartDate.Date + "','" + MonthEndDate.Date + "'," + VerticalID + ",'DH')");
                    }

                    if (index == 0)
                    {
                        e.Row.Cells[13].Style.Add("cursor", "pointer");
                        e.Row.Cells[13].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[13].Attributes.Add("onclick", "PlannedActualAuditDetails('Planned'," + branchid + ",'" + Period + "','" + FROMDate.Date + "','" + TODate.Date + "'," + VerticalID + ",'DH')");
                    }
                    else if (index == 1)
                    {
                        e.Row.Cells[13].Style.Add("cursor", "pointer");
                        e.Row.Cells[13].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[13].Attributes.Add("onclick", "PlannedActualAuditDetails('Actual'," + branchid + ",'" + Period + "','" + FROMDate.Date + "','" + TODate.Date + "'," + VerticalID + ",'DH')");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        public DateTime? GetDate(string date)
        {
            if (date != null && date != "")
            {
                string date1 = "";

                if (date.Contains("/"))
                {
                    date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains("-"))
                {
                    date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains(" "))
                {
                    date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
                }

                return Convert.ToDateTime(date1);
            }
            else
            {
                return null;
            }
        }

        public DateTime GetLastDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, DateTime.DaysInMonth(dateTime.Year, dateTime.Month));
        }     
        protected void AddSteps(string[] items, BulletedList ListName)
        {
            ListName.Items.Clear();
            ListName.Attributes["data-progtrckr-steps"] = items.Length.ToString();

            for (int i = 0; i < items.Length; i++)
            {
                ListName.Items.Add(new ListItem(items[i]));
            }
        }
        protected void SetProgress(long current, BulletedList ListName)
        {
            for (int i = 0; i < ListName.Items.Count; i++)
            {
                ListName.Items[i].Attributes["class"] =
                    (i < current) ? "progtrckr-done" : (i == current) ? "progtrckr-current" : "progtrckr-todo";

                if (i == 3 && i > current)
                    ListName.Items[i].Attributes["class"] = "progtrckr-todo-closed";
            }
            if (current == 4)
                ListName.Items[Convert.ToInt32(current) - 1].Attributes["class"] = "progtrckr-closed";
        }

    }
}