﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Saplin.Controls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class AuditManagerDashboard : System.Web.UI.Page
    {
        protected List<long> CHKBranchlist = new List<long>();
        protected List<Int32> roles;
        protected static string ObservationStatusChart;
        protected static string ProcessWiseObservationStatusChart;
        protected static string ObservationProcessWiseAgingChart;
        protected static String[] items;
        public static List<long> Branchlist = new List<long>();
        protected int CustomerId = 0;
        protected bool prerqusite = false;
        protected static string IsHiddenFY;
        protected static string IsHiddenCustomerId;
        protected static string IsHiddenSubEntity;
        protected static string IsHiddenSubEntityOne;
        protected static string IsHiddenSubEntityTwo;
        protected static string IsHiddenSubEntityThree;
        protected static string IsHiddenSubEntityFour;
        protected bool IsAuditHead;
        protected void Page_Load(object sender, EventArgs e)
        {
            roles = CustomerManagementRisk.ARSGetAssignedRolesBOTH(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            items = new String[] { "Not Started", "In Progress", "Under Review", "Closed" };
            if (!IsPostBack)
            {
                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                home.Value = "true";
                BindCustomerList();
                List<int?> CustomerIdsList = new List<int?>();
                CustomerIdsList.Clear();
                List<int> CustomerIdsList1 = new List<int>();
                CustomerIdsList1.Clear();
                List<string> FinancialYearList = new List<string>();
                FinancialYearList.Clear();
                for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                {
                    if (ddlCustomerMultiSelect.Items[i].Selected)
                    {
                        CustomerIdsList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                        CustomerIdsList1.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                    }
                }

                BindProcess("P", CustomerIdsList);
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData(CustomerIdsList);
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (!string.IsNullOrEmpty(Request.QueryString["FinancialYear"]))
                {
                    string finYear = string.Empty;
                    finYear = Request.QueryString["FinancialYear"].ToString();
                    for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                    {
                        if (ddlFinancialYearMultiSelect.Items[i].Text == finYear)
                        {
                            ddlFinancialYearMultiSelect.Items[i].Selected = true;
                            FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                        }
                    }
                }
                else
                {
                    if (FinancialYear != null)
                    {
                        for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                        {
                            if (ddlFinancialYearMultiSelect.Items[i].Text == FinancialYear)
                            {
                                ddlFinancialYearMultiSelect.Items[i].Selected = true;
                                FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                            }
                        }
                    }
                }
                IsHiddenFY = string.Join(",", FinancialYearList);
                List<string> custIdList = new List<string>();
                if (CustomerIdsList1.Count > 0)
                {
                    foreach (var item in CustomerIdsList1)
                    {
                        custIdList.Add(Convert.ToString(item));
                    }
                }
                IsHiddenCustomerId = string.Join(",", custIdList);
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;
                    List<AuditCountView_Dashboard> dataAuditSummary = new List<AuditCountView_Dashboard>();
                    List<AuditCountView_Dashboard> PRdataAuditSummary = new List<AuditCountView_Dashboard>();
                    List<ImplimentInternalAuditClosedCountView> dataImpliment = new List<ImplimentInternalAuditClosedCountView>();
                    List<ImplimentInternalAuditClosedCountView> PRdataImpliment = new List<ImplimentInternalAuditClosedCountView>();
                    //Process
                    var ProcessRecord = (from c in entities.AuditCountView_Dashboard
                                         where CustomerIdsList1.Contains(c.CustomerID)
                                         select c).ToList();

                    List<SP_PrequsiteCount_Result> Masterrecord = new List<SP_PrequsiteCount_Result>();
                    entities.Database.CommandTimeout = 300;
                    Masterrecord = (from row in entities.SP_PrequsiteCount(Portal.Common.AuthenticationHelper.UserID)
                                    select row).ToList();

                    if (ProcessRecord.Count > 0)
                    {
                        PRdataAuditSummary = ProcessRecord;
                        dataAuditSummary = (from C in ProcessRecord
                                            join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            where C.ProcessId == EAAMR.ProcessId
                                            && CustomerIdsList1.Contains(C.CustomerID)
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                            select C).ToList();
                    }
                    //Implementation
                    var ImplementationRecord = (from c in entities.ImplimentInternalAuditClosedCountViews
                                                select c).ToList();

                    if (ImplementationRecord.Count > 0)
                    {
                        PRdataImpliment = ImplementationRecord;
                        dataImpliment = (from C in ImplementationRecord
                                         join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                         on C.CustomerBranchID equals EAAMR.BranchID
                                         where C.ProcessID == EAAMR.ProcessId
                                         && CustomerIdsList1.Contains(C.CustomerID)
                                         && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                         && EAAMR.ISACTIVE == true
                                         select C).ToList();
                    }

                    if (FinancialYearList.Count > 0)
                    {
                        dataAuditSummary = dataAuditSummary.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        dataImpliment = dataImpliment.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        Masterrecord = Masterrecord.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        PRdataImpliment = PRdataImpliment.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        PRdataAuditSummary = PRdataAuditSummary.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                    }
                    bindcount(dataAuditSummary, PRdataAuditSummary, PRdataImpliment, dataImpliment, Masterrecord, CustomerIdsList1);
                    if (dataAuditSummary.Count > 0)
                    {
                        BindGraphData(dataAuditSummary, CustomerIdsList1, FinancialYearList);
                    }
                    else
                    {
                        ObservationProcessWiseAgingChart = String.Empty;
                        ObservationStatusChart = String.Empty;
                        ProcessWiseObservationStatusChart = String.Empty;
                    }
                }
            }
        }
        public void bindcount(List<AuditCountView_Dashboard> dataAuditSummary, List<AuditCountView_Dashboard> PRSummary, List<ImplimentInternalAuditClosedCountView> dataImpliment, List<ImplimentInternalAuditClosedCountView> PRImpliment, List<SP_PrequsiteCount_Result> Masterrecord, List<int> CustomerIdsList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (dataAuditSummary.Count > 0)
                {
                    divOpenAMCount.InnerText = GetAuditManagerAuditCount(dataAuditSummary, Portal.Common.AuthenticationHelper.UserID, 1, CustomerIdsList).ToString();
                    divCloseAMCount.InnerText = GetAuditManagerAuditCount(dataAuditSummary, Portal.Common.AuthenticationHelper.UserID, 3, CustomerIdsList).ToString();
                }
                else
                {
                    divOpenAMCount.InnerText = "0";
                    divCloseAMCount.InnerText = "0";
                }
                if (PRSummary.Count > 0)
                {
                    if (roles.Count > 0)
                    {
                        divOpenAuditCount.InnerText = GetAuditCount(PRSummary, Portal.Common.AuthenticationHelper.UserID, 1, CustomerIdsList).ToString();
                        divCloseAuditCount.InnerText = GetAuditCount(PRSummary, Portal.Common.AuthenticationHelper.UserID, 3, CustomerIdsList).ToString();
                    }
                    else
                    {
                        divOpenAuditCount.InnerText = "0";
                        divCloseAuditCount.InnerText = "0";
                    }
                }
                else
                {
                    divOpenAuditCount.InnerText = "0";
                    divCloseAuditCount.InnerText = "0";
                }
                if (dataImpliment.Count > 0)
                {
                    divOpenAMIMPCount.InnerText = GetAuditManagerAuditCountIMP(dataImpliment, Portal.Common.AuthenticationHelper.UserID, 1, CustomerIdsList).ToString();
                    divCloseAMIMPCount.InnerText = GetAuditManagerAuditCountIMP(dataImpliment, Portal.Common.AuthenticationHelper.UserID, 3, CustomerIdsList).ToString();

                }
                else
                {
                    divOpenAMIMPCount.InnerText = "0";
                    divCloseAMIMPCount.InnerText = "0";
                }
                if (PRImpliment.Count > 0)
                {
                    if (roles.Count > 0)
                    {
                        divOpenAuditIMPCount.InnerText = GetAuditCountIMP(PRImpliment, Portal.Common.AuthenticationHelper.UserID, 1, CustomerIdsList).ToString();
                        divCloseAuditIMPCount.InnerText = GetAuditCountIMP(PRImpliment, Portal.Common.AuthenticationHelper.UserID, 3, CustomerIdsList).ToString();
                    }
                    else
                    {
                        divOpenAuditIMPCount.InnerText = "0";
                        divCloseAuditIMPCount.InnerText = "0";
                    }
                }
                else
                {
                    divOpenAuditIMPCount.InnerText = "0";
                    divCloseAuditIMPCount.InnerText = "0";
                }
                if (Masterrecord.Count > 0)
                {
                    prerqusite = true;
                    var opencount = Masterrecord.Where(entry => entry.DocStatus == "Open").ToList().Count;
                    var submittedcount = Masterrecord.Where(entry => entry.DocStatus == "Submitted" || entry.DocStatus == "Approved" || entry.DocStatus == "Re-Submitted").ToList().Count;
                    var Rejectedcount = Masterrecord.Where(entry => entry.DocStatus == "Rejected").ToList().Count;
                    divPreRequisiteOpenAuditCount.InnerText = Convert.ToString(opencount);
                    divPreRequisiteSubmittedAuditCount.InnerText = Convert.ToString(submittedcount);
                    divPreRequisiteRejectedCount.InnerText = Convert.ToString(Rejectedcount);
                }
                else
                {
                    prerqusite = false;
                    divPreRequisiteOpenAuditCount.InnerText = "0";
                    divPreRequisiteSubmittedAuditCount.InnerText = "0";
                    divPreRequisiteRejectedCount.InnerText = "0";
                }
            }
        }
        public void BindGraphData(List<AuditCountView_Dashboard> dataAuditSummary, List<int> CustomerIdsList, List<string> FinancialYearList)
        {
            try
            {
                string FinYear = string.Empty;
                string Period = string.Empty;
                bool clearLegalBranchList = false;
                bool clearSubEntity1List = false;
                bool clearSubEntity2List = false;
                bool clearSubEntity3List = false;
                bool clearFilterLocation = false;
                List<int> ProcessIDList = new List<int>();
                List<int?> VerticalIDList = new List<int?>();

                CHKBranchlist.Clear();
                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                {
                    if (ddlSubEntity1.Items[i].Selected)
                    {
                        if (clearLegalBranchList && CHKBranchlist.Count > 0)
                        {
                            CHKBranchlist.Clear();
                            clearLegalBranchList = false;
                        }
                        clearSubEntity1List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1.Items[i].Value));
                    }

                }

                for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                {
                    if (ddlSubEntity2.Items[i].Selected)
                    {
                        if (clearSubEntity1List && CHKBranchlist.Count > 0)
                        {
                            CHKBranchlist.Clear();
                            clearSubEntity1List = false;
                        }
                        clearSubEntity2List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2.Items[i].Value));
                    }
                }

                for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                {
                    if (ddlSubEntity3.Items[i].Selected)
                    {
                        if (clearSubEntity2List && CHKBranchlist.Count > 0)
                        {
                            CHKBranchlist.Clear();
                            clearSubEntity2List = false;
                        }
                        clearSubEntity3List = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3.Items[i].Value));
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    CHKBranchlist.Clear();
                    for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
                    {
                        if (ddlFilterLocation.Items[i].Selected)
                        {
                            CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocation.Items[i].Value));
                        }
                    }
                }

                for (int i = 0; i < ddlProcessMultiSelect.Items.Count; i++)
                {
                    if (ddlProcessMultiSelect.Items[i].Selected)
                    {
                        ProcessIDList.Add(Convert.ToInt32(ddlProcessMultiSelect.Items[i].Value));
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    List<int> verticalIdsList = UserManagementRisk.VerticalgetBycustomeridListForDashboard(CustomerIdsList);
                    if (verticalIdsList.Count > 0)
                    {
                        foreach (var vId in verticalIdsList)
                        {
                            VerticalIDList.Add(Convert.ToInt32(vId));
                        }
                    }
                }
                List<int?> ListofBranch = new List<int?>();
                if (CHKBranchlist.Count > 0)
                {
                    Branchlist.Clear();
                    GetAllHierarchy(CustomerIdsList, CHKBranchlist);
                    Session["BranchList"] = null;
                    if (Branchlist.Count > 0)
                    {
                        ListofBranch = Branchlist.Select(x => (int?)x).ToList();
                        Session["BranchList"] = ListofBranch;
                    }
                }
                else
                {
                    Branchlist.Clear();
                    GetAllHierarchy(CustomerIdsList, CHKBranchlist);
                    Session["BranchList"] = null;
                    if (Branchlist.Count > 0)
                    {
                        ListofBranch = Branchlist.Select(x => (int?)x).ToList();
                        Session["BranchList"] = ListofBranch;
                    }
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    entities.Database.CommandTimeout = 300;

                    var dataObservationStatus = (from C in entities.ObservationStatus_DisplayView_New
                                                 join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                                 on C.CustomerBranchID equals EAAMR.BranchID
                                                 where C.ProcessId == EAAMR.ProcessId
                                                 && CustomerIdsList.Contains(C.CustomerID)
                                                 && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                                 && EAAMR.ISACTIVE == true
                                                 select C).ToList();


                    var dataAuditPlanned = (from row in entities.Sp_AuditPlannedActualDetailDisplayView(Portal.Common.AuthenticationHelper.UserID, "AM")
                                            where CustomerIdsList.Contains(row.CustomerID) && FinancialYearList.Contains(row.FinancialYear)
                                            select row).ToList();


                    dataAuditPlanned = dataAuditPlanned.GroupBy(a => a.AuditID).Select(a => a.First()).ToList();

                    var dataObservationAging = (from row1 in entities.ObservationAging_DisplayView
                                                join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                                on row1.CustomerBranchID equals EAAMR.BranchID
                                                where row1.ProcessId == EAAMR.ProcessId
                                                && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                                && EAAMR.ISACTIVE == true && CustomerIdsList.Contains(row1.CustomerID)
                                                select row1).ToList();

                    GetAuditTrackerSummary(dataAuditPlanned, CustomerIdsList, ListofBranch, FinancialYearList, Period, VerticalIDList);

                    GetAuditObservationAgingSummary(dataObservationAging, CustomerIdsList, ListofBranch, FinancialYearList, Period, ProcessIDList, VerticalIDList);

                    GetAuditStatusSummary(dataAuditSummary, CustomerIdsList, ListofBranch, FinancialYearList, Period, VerticalIDList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<long> CheckAuditManagerLocation(int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.EntitiesAssignmentAuditManagerRisks
                                         where row.UserID == userid
                                         select row.BranchID).Distinct().ToList();
                return transactionsQuery.ToList();
            }
        }
        public void BindVertical()
        {
            try
            {
                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa();
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindFinancialYear()
        {
            ddlFinancialYearMultiSelect.DataTextField = "Name";
            ddlFinancialYearMultiSelect.DataValueField = "ID";
            ddlFinancialYearMultiSelect.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYearMultiSelect.DataBind();
        }
        public void BindLegalEntityData(List<int?> customerIdsList)
        {
            if (customerIdsList.Count > 0)
            {
                ddlLegalEntityMultiSelect.DataTextField = "Name";
                ddlLegalEntityMultiSelect.DataValueField = "ID";
                ddlLegalEntityMultiSelect.Items.Clear();
                ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataForDashBoard(customerIdsList);
                ddlLegalEntityMultiSelect.DataBind();
            }
        }

        public void BindSubEntityDataList(DropDownCheckBoxes DRP, List<long> ParentId)
        {
            List<int> CustomerIdsList = new List<int>();
            CustomerIdsList.Clear();
            for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
            {
                if (ddlCustomerMultiSelect.Items[i].Selected)
                {
                    CustomerIdsList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                }
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityDataListForDashBoard(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CustomerIdsList, ParentId);
            DRP.DataBind();
        }
        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(List<int> customerIDList, List<long> customerbranchlist)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && customerIDList.Contains(row.CustomerID)
                             && customerbranchlist.Contains(row.ID)
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerIDList, item, true, entities);
                }
            }

            return hierarchy;
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(List<int> customerIDList, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && customerIDList.Contains(row.CustomerID)
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);

                    LoadSubEntities(customerIDList, item, true, entities);
                }
            }

            return hierarchy;
        }
        public static void LoadSubEntities(List<int> customerIDList, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && customerIDList.Contains(row.CustomerID)
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerIDList, item, false, entities);
            }
        }
        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<int> CustomerIdsList = new List<int>();
                    CustomerIdsList.Clear();
                    List<int> CustomerIdsList1 = new List<int>();
                    CustomerIdsList1.Clear();
                    for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                    {
                        if (ddlCustomerMultiSelect.Items[i].Selected)
                        {
                            CustomerIdsList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                            CustomerIdsList1.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                        }
                    }
                    if (CustomerIdsList.Count == 0)
                    {
                        Response.Write("<script>alert('Please Select Customer.');</script>");
                        return;
                    }

                    List<SP_PrequsiteCount_Result> Masterrecord = new List<SP_PrequsiteCount_Result>();
                    List<AuditCountView_Dashboard> dataAuditSummary = new List<AuditCountView_Dashboard>();
                    List<ImplimentInternalAuditClosedCountView> dataImpliment = new List<ImplimentInternalAuditClosedCountView>();
                    List<AuditCountView_Dashboard> PRdataAuditSummary = new List<AuditCountView_Dashboard>();
                    List<ImplimentInternalAuditClosedCountView> PRdataImpliment = new List<ImplimentInternalAuditClosedCountView>();

                    //Process
                    entities.Database.CommandTimeout = 300;
                    var ProcessRecord = (from c in entities.AuditCountView_Dashboard
                                         where CustomerIdsList.Contains(c.CustomerID)
                                         select c).ToList();


                    entities.Database.CommandTimeout = 300;
                    Masterrecord = (from row in entities.SP_PrequsiteCount(Portal.Common.AuthenticationHelper.UserID)
                                    select row).ToList();

                    if (ProcessRecord.Count > 0)
                    {
                        PRdataAuditSummary = ProcessRecord;
                        dataAuditSummary = (from C in ProcessRecord
                                            join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            where C.ProcessId == EAAMR.ProcessId
                                            && CustomerIdsList.Contains(C.CustomerID)
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                            select C).ToList();
                    }
                    //Implementation
                    entities.Database.CommandTimeout = 300;
                    var ImplementationRecord = (from c in entities.ImplimentInternalAuditClosedCountViews
                                                select c).ToList();

                    if (ImplementationRecord.Count > 0)
                    {
                        PRdataImpliment = ImplementationRecord;
                        dataImpliment = (from C in ImplementationRecord
                                         join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                         on C.CustomerBranchID equals EAAMR.BranchID
                                         where C.ProcessID == EAAMR.ProcessId
                                         && CustomerIdsList.Contains(C.CustomerID)
                                         && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                         && EAAMR.ISACTIVE == true
                                         select C).ToList();
                    }

                    List<string> FinancialYearList = new List<string>();
                    for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                    {
                        if (ddlFinancialYearMultiSelect.Items[i].Selected)
                        {
                            FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                        }
                    }
                    if (FinancialYearList.Count == 0)
                    {
                        Response.Write("<script>alert('Please Select Financial Year.');</script>");
                        return;
                    }
                    IsHiddenFY = string.Join(",", FinancialYearList);
                    IsHiddenCustomerId = string.Join(",", CustomerIdsList);

                    if (FinancialYearList.Count > 0)
                    {
                        dataAuditSummary = dataAuditSummary.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        dataImpliment = dataImpliment.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        Masterrecord = Masterrecord.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        PRdataAuditSummary = PRdataAuditSummary.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        PRdataImpliment = PRdataImpliment.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                    }
                    bindcount(dataAuditSummary, PRdataAuditSummary, dataImpliment, PRdataImpliment, Masterrecord, CustomerIdsList);
                    //if (dataAuditSummary.Count > 0)
                    //{
                    BindGraphData(dataAuditSummary, CustomerIdsList1, FinancialYearList);
                    //}

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            CHKBranchlist.Clear();

            if (ddlLegalEntity.SelectedValue != "-1")
            {
                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                BindSubEntityDataList(ddlSubEntity1, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                }
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //Region
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                {
                    if (ddlSubEntity1.Items[i].Selected)
                    {
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1.Items[i].Value));
                    }
                }
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            if (CHKBranchlist.Count > 0)
            {
                IsHiddenSubEntityOne = string.Join(",", CHKBranchlist);
            }
            BindSubEntityDataList(ddlSubEntity2, CHKBranchlist);
            BindSchedulingType();
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //State
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                {
                    if (ddlSubEntity2.Items[i].Selected)
                    {
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2.Items[i].Value));
                    }
                }
                BindSubEntityDataList(ddlSubEntity3, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            if (CHKBranchlist.Count > 0)
            {
                IsHiddenSubEntityTwo = string.Join(",", CHKBranchlist);
            }
            BindSchedulingType();
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //Hub
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                {
                    if (ddlSubEntity3.Items[i].Selected)
                    {
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3.Items[i].Value));
                    }
                }
                BindSubEntityDataList(ddlFilterLocation, CHKBranchlist);
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            BindSchedulingType();
            if (CHKBranchlist.Count > 0)
            {
                IsHiddenSubEntityThree = string.Join(",", CHKBranchlist);
            }
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //Loction
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            CHKBranchlist.Clear();
            for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
            {
                if (ddlFilterLocation.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocation.Items[i].Value));
                }
            }
            if (CHKBranchlist.Count > 0)
            {
                IsHiddenSubEntityFour = string.Join(",", CHKBranchlist);
            }
            BindSchedulingType();
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    IsHiddenFY = ddlFinancialYear.SelectedItem.Text;
                }
            }
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }

            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }

        public static long CheckAuditStatusReviewWorkingManagementDashboard(List<Sp_InternalAuditInstanceTransactionViewForAuditHeadDashboard_Result> InternalAuditInstanceTransactionViewRecord, String FinYear, String Period, long Customerid, int CustBranchid, int VerticalID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int Result = 0;
                int TotalATBD = 0;

                List<int?> ReviewStatusList = new List<int?> { 3, 4, 5, 6 };

                var Records = (from row in InternalAuditInstanceTransactionViewRecord
                               where row.CustomerID == Customerid
                               && row.CustomerBranchID == CustBranchid
                               && row.VerticalID == VerticalID
                               && row.FinancialYear == FinYear
                               && row.ForMonth == Period
                               && row.AuditID == AuditID
                               && row.RoleID == 4
                               select row).ToList();

                var RecordsClose = (from row in InternalAuditInstanceTransactionViewRecord
                                    where row.CustomerID == Customerid
                                    && row.CustomerBranchID == CustBranchid
                                    && row.VerticalID == VerticalID
                                    && row.FinancialYear == FinYear
                                    && row.ForMonth == Period
                                     && row.AuditID == AuditID
                                    && row.RoleID == 4 && row.AuditStatusID == 3
                                    select row).ToList();

                var Records1 = (from row in InternalAuditInstanceTransactionViewRecord
                                where row.CustomerID == Customerid
                                && row.CustomerBranchID == CustBranchid
                                && row.VerticalID == VerticalID
                                && row.FinancialYear == FinYear
                                && row.ForMonth == Period
                                 && row.AuditID == AuditID
                                && row.RoleID == 4 && ReviewStatusList.Contains(row.AuditStatusID)
                                select row).ToList();

                var auditRecords = (from row in entities.AuditClosureDetails
                                    where row.ID == AuditID
                                    select row).FirstOrDefault();

                if (auditRecords != null)
                {
                    if (auditRecords.Total != null)
                    {
                        TotalATBD = Convert.ToInt32(auditRecords.Total);
                    }
                }

                if (Records.Count > 0)
                {

                    var ReviewedATBD2 = Records.Where(Entry => Entry.AuditStatusID == 2).Count();
                    var ReviewedATBD3 = RecordsClose.Count();
                    var ReviewedATBD4 = Records1.Count();


                    var reviewedPer = ((Convert.ToDecimal(ReviewedATBD4) / Convert.ToDecimal(TotalATBD)) * 100);
                    var closedPer = ((Convert.ToDecimal(ReviewedATBD3) / Convert.ToDecimal(TotalATBD)) * 100);

                    if (closedPer >= 100)
                        Result = 4;
                    else if (reviewedPer >= 75 && reviewedPer <= 100)
                        Result = 2;
                    else if (ReviewedATBD2 > 0) //Submitted Count
                        Result = 1;
                    else
                        Result = 0;
                }

                return Result;
            }
        }
        //public static long CheckAuditStatusReviewWorkingManagementDashboard(List<AuditCountView> AuditSummaryCountRecord, String FinYear, String Period, long Customerid, int CustBranchid, int VerticalID,int auditid)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        long Result = 0;
        //        long TotalATBD = 0;
        //        long? ReviewedATBD = 0;

        //        var Records = (from row in entities.AuditClosureDetails
        //                       where row.CustomerID == Customerid
        //                       && row.BranchId == CustBranchid
        //                       && row.VerticalId == VerticalID
        //                       && row.FinancialYear == FinYear
        //                       && row.ForMonth == Period
        //                       && row.ID== auditid
        //                       select row).FirstOrDefault();



        //        if (Records != null)
        //        {
        //            if (Records.Total != null)
        //            {
        //                TotalATBD = Records.Total;
        //            }
        //            if (Records.Closed != null)
        //            {
        //                ReviewedATBD = Records.Closed;
        //            }

        //            if (TotalATBD != 0)
        //                Result = ((long) ReviewedATBD / TotalATBD) * 100;

        //            if (Result >= 100)
        //                Result = 4;
        //            else if (Result >= 75 && Result < 100)
        //                Result = 3;
        //            else
        //                Result = 2;
        //        }
        //        return Result;                
        //    }
        //}
        public static List<int> GetDistinctAuditsStatusManagementDashboard(List<AuditCountView> AuditSummaryCountRecord, String FinYear, String Period, long Customerid, int CustBranchid, int VerticalID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditStatusList = new List<int>();

                var record = (from row in entities.InternalAuditInstanceTransactionViews
                              where row.CustomerID == Customerid
                              && row.CustomerBranchID == CustBranchid
                              && row.VerticalID == VerticalID
                              && row.FinancialYear == FinYear
                              && row.ForMonth == Period
                              && row.AuditID == AuditID
                              select (int)row.AuditStatusID).Distinct().ToList();
                return record;
            }
        }
        public void GetObservationStatusDashboardCount(List<ObservationStatus_DisplayView_New> ObservationStatusDisplayViewNewMasterList, long CustomerID, List<int?> BranchList, String FinYear, String Period, int ProcessID, int VerticalID)
        {
            try
            {
                ObservationStatusChart = string.Empty;
                string ObservationStatusChartSeries = string.Empty;
                string ObservationStatusChartDrillDown = string.Empty;

                long highcount = 0;
                long mediumCount = 0;
                long lowcount = 0;
                long totalcount = 0;

                ObservationStatusChartSeries = "series: [{ name: 'Category', colorByPoint: true, data: [";
                ObservationStatusChartDrillDown = "drilldown: { series: [ ";

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var ObservationList = ObservationSubcategory.GetObservationCategoryAll(CustomerID);

                    var Records = (from row in ObservationStatusDisplayViewNewMasterList
                                   where row.CustomerID == CustomerID
                                   select row).ToList();

                    if (BranchList.Count > 0)
                        Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (FinYear != "")
                        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                    else
                    {
                        FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                    }

                    if (Period != "")
                        Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                    if (ProcessID != -1)
                        Records = Records.Where(Entry => Entry.ProcessId == ProcessID).ToList();

                    if (VerticalID != -1)
                        Records = Records.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                    foreach (Mst_ObservationCategory EachObsCat in ObservationList)
                    {
                        highcount = (from row in Records
                                     where row.ObservatioRating == 1
                                      && row.ObservationCategory == EachObsCat.ID
                                     select row).Count(); /*entities.ObservationStatus_DisplayView*/

                        mediumCount = (from row in Records
                                       where row.ObservatioRating == 2
                                        && row.ObservationCategory == EachObsCat.ID
                                       select row).Count();

                        lowcount = (from row in Records
                                    where row.ObservatioRating == 3
                                     && row.ObservationCategory == EachObsCat.ID
                                    select row).Count();

                        totalcount = highcount + mediumCount + lowcount;

                        ObservationStatusChartSeries += "{ name: '" + EachObsCat.Name + "', y: " + totalcount + ", drilldown: '" + EachObsCat.Name + "' },";

                        ObservationStatusChartDrillDown += "{ name: '" + EachObsCat.Name + "', id: '" + EachObsCat.Name + "'," +
                           " data: [['High'," + highcount + "],['Medium', " + mediumCount + "],['Low'," + lowcount + "]]," +
                            " events: {" +
                                " click: function(e){" +
                                " ShowObservationDetails(e.point.name," + CustomerID + ",-1,'" + FinYear + "'," + EachObsCat.ID + "," + ProcessID + ",'AM')}" +
                                " }},";
                    }

                    ObservationStatusChartSeries = ObservationStatusChartSeries.Trim(',');
                    ObservationStatusChartDrillDown = ObservationStatusChartDrillDown.Trim(',');
                    ObservationStatusChartSeries += "] }],";
                    ObservationStatusChartDrillDown += "] } ";

                    ObservationStatusChart = ObservationStatusChartSeries + ObservationStatusChartDrillDown;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetProcessWiseObservationDashboardCount(List<ObservationStatus_DisplayView_New> ObservationStatus_DisplayView_New, long CustomerID, List<int?> BranchList, String FinYear, String Period, int ProcessID, int VerticalID)
        {
            try
            {
                ProcessWiseObservationStatusChart = String.Empty;
                String ProcesswiseObservationSeries = String.Empty;
                String ListofProcesses = String.Empty;
                String HighCountList = String.Empty;
                String MediumCountList = String.Empty;
                String LowCountList = String.Empty;
                long highcount = 0;
                long mediumCount = 0;
                long lowcount = 0;
                long totalcount = 0;
                long UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                String newDivName = String.Empty;
                int CustomerBranchID = -1;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<Mst_Process> ProcessList = new List<Mst_Process>();
                    if (BranchList.Count > 0)
                    {
                        List<long> ListofBranch = new List<long>();
                        ListofBranch = Branchlist.Select(x => (long)x).ToList();
                        ProcessList = ProcessManagement.GetAssingedProcess(CustomerID, ListofBranch, UserID);
                    }
                    else
                    {
                        ProcessList = ProcessManagement.GetAssingedProcess(CustomerID, UserID);
                    }
                    if (ProcessID != -1)
                        ProcessList = ProcessList.Where(Entry => Entry.Id == ProcessID).ToList();

                    if (ProcessList.Count > 0)
                    {
                        int Count = 1;
                        ProcessList.ForEach(EachProcess =>
                        {
                            var Records = (from row in ObservationStatus_DisplayView_New
                                           where row.CustomerID == CustomerID
                                           && row.ProcessId == EachProcess.Id
                                           select row).ToList();

                            if (BranchList.Count > 0)
                                Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                            if (FinYear != "")
                                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                            else
                            {
                                FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                            }

                            if (Period != "")
                                Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                            if (VerticalID != -1)
                                Records = Records.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                            highcount = (from row in Records
                                         where row.ObservatioRating == 1
                                         select row).Count();

                            mediumCount = (from row in Records
                                           where row.ObservatioRating == 2
                                           select row).Count();

                            lowcount = (from row in Records
                                        where row.ObservatioRating == 3
                                        select row).Count();

                            totalcount = highcount + mediumCount + lowcount;
                            if (totalcount != 0)
                            {
                                ListofProcesses += "'" + EachProcess.Name + "',";
                                System.Web.UI.HtmlControls.HtmlGenericControl newDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                                newDivName = "DivProcessObs" + Count;
                                newDiv.ID = newDivName;
                                String newDivClientID = newDiv.ClientID;
                                newDiv.Style["height"] = "200px";
                                newDiv.Style["width"] = "20%";
                                newDiv.Style["float"] = "left";
                                DivGraphProcessObsStatus.Controls.Add(newDiv); /*format: '<b>{point.name}</b>: {y}',*/
                                ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "', { chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie',marginBottom: 30, }," +
                                "title: {text: '" + EachProcess.Name + "',style: {fontSize: '12px' }},  legend: { itemDistance:0,itemStyle:{fontSize:'10px'}, }, tooltip: { pointFormat: '{series.name}: <b>{point.y}</b>'}," +
                                "plotOptions: {pie: {size: '100%', showInLegend: true, allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, format: '{y}', distance: 1," +
                                "style: { color: (Highcharts.theme &&     Highcharts.theme.contrastTextColor) || 'black'} } } }," +
                                "series: [{ name: '" + EachProcess.Name + "', colorByPoint: true, " +
                                "data: [{ name: 'High', color: '#f45b5b', y: " + highcount + ", events:{click: function(e) { ShowObservationDetails(e.point.name," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "',-1," + EachProcess.Id + ",'AM') } } }," +
                                " { name: 'Medium', color: '#e4d354', y:" + mediumCount + ", events:{click: function(e) { ShowObservationDetails(e.point.name," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "',-1," + EachProcess.Id + ",'AM') } } }," +
                                " { name: 'Low', color: '#90ed7d', y:" + lowcount + ", events:{click: function(e) { ShowObservationDetails(e.point.name," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "',-1," + EachProcess.Id + ",'AM') } } }] }] });";
                                ProcessWiseObservationStatusChart += ProcesswiseObservationSeries;
                                Count++;
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetAuditTrackerSummary(List<Sp_AuditPlannedActualDetailDisplayView_Result> AuditPlannedActualDetailDisplayRecord, List<int> CustomerID, List<int?> BranchList, List<String> FinYearList, String Period, List<int?> VerticalIDList)
        {
            try
            {
                int TotalCount = 0;
                String PartialFromDate = String.Empty;
                String PartialToDate = String.Empty;
                String[] FinancialYear = new String[2];

                DataTable table = new DataTable();
                table.Columns.Add("Name", typeof(string));

                foreach (string FinYear in FinYearList)
                {
                    if (FinYear != "")
                    {
                        FinancialYear = FinYear.Split('-');

                        PartialFromDate = "01/04/" + FinancialYear[0];
                        PartialToDate = "31/03/" + FinancialYear[1];
                    }
                    else
                    {
                        string FinYear1 = GetCurrentFinancialYear(DateTime.Now.Date);

                        if (FinYear1 != "")
                        {
                            FinancialYear = FinYear1.Split('-');

                            PartialFromDate = "01/04/" + FinancialYear[0];
                            PartialToDate = "31/03/" + FinancialYear[1];
                        }
                    }
                }

                DateTime FROMDate = Convert.ToDateTime(GetDate(PartialFromDate));
                DateTime TODate = Convert.ToDateTime(GetDate(PartialToDate));

                for (int i = 0; i < 12; i++)
                {
                    table.Columns.Add(FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy"), typeof(String));
                }

                table.Columns.Add("Total", typeof(string));

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    //Planned Audits
                    DataRow tableRow = table.NewRow();

                    tableRow["Name"] = "Planned";

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime MonthStartDate = FROMDate.AddMonths(i);
                        DateTime MonthEndDate = InternalControlManagementDashboardRisk.GetLastDayOfMonth(MonthStartDate);

                        var transactionsQuery = (from row in AuditPlannedActualDetailDisplayRecord
                                                 where MonthStartDate <= row.StartDate
                                                 && row.StartDate <= MonthEndDate
                                                 select row).ToList();

                        if (BranchList.Count > 0)
                            transactionsQuery = transactionsQuery.Where(Entry => BranchList.Contains(Entry.CustomerBranchId)).ToList();

                        if (Period != "")
                            transactionsQuery = transactionsQuery.Where(entry => entry.ForMonth == Period).ToList();

                        if (VerticalIDList.Count > 0)
                            transactionsQuery = transactionsQuery.Where(entry => VerticalIDList.Contains(entry.VerticalID)).ToList();

                        tableRow[FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy")] = transactionsQuery.Count;

                        TotalCount += transactionsQuery.Count;
                    }

                    tableRow["Total"] = TotalCount;
                    table.Rows.Add(tableRow);


                    //Actual Audits
                    TotalCount = 0;
                    DataRow tableActualRow = table.NewRow();

                    tableActualRow["Name"] = "Actual";

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime MonthStartDate = FROMDate.AddMonths(i);
                        DateTime MonthEndDate = GetLastDayOfMonth(MonthStartDate);

                        var transactionsQuery = (from row in AuditPlannedActualDetailDisplayRecord
                                                 where MonthStartDate <= row.ExpectedStartDate
                                                  && row.ExpectedStartDate <= MonthEndDate
                                                 select row).ToList();


                        if (BranchList.Count > 0)
                            transactionsQuery = transactionsQuery.Where(Entry => BranchList.Contains(Entry.CustomerBranchId)).ToList();

                        if (Period != "")
                            transactionsQuery = transactionsQuery.Where(entry => entry.ForMonth == Period).ToList();

                        if (VerticalIDList.Count > 0)
                            transactionsQuery = transactionsQuery.Where(entry => VerticalIDList.Contains(entry.VerticalID)).ToList();

                        tableActualRow[FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy")] = transactionsQuery.Count;

                        TotalCount += transactionsQuery.Count;
                    }

                    tableActualRow["Total"] = TotalCount;
                    table.Rows.Add(tableActualRow);

                    grdAuditTrackerSummary.DataSource = table;
                    grdAuditTrackerSummary.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<Sp_GetAuditTrackerDisplayAudit_Result> GetAuditManagerDashboardAudits(List<int?> BranchList, List<string> FYearList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Sp_GetAuditTrackerDisplayAudit_Result> record = new List<Sp_GetAuditTrackerDisplayAudit_Result>();

                record = (from C in entities.Sp_GetAuditTrackerDisplayAudit(Portal.Common.AuthenticationHelper.UserID)
                          select C).Distinct().ToList();

                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains((int)Entry.CustomerBranchID)).ToList();
                if (FYearList.Count > 0)
                {
                    record = record.Where(Entry => FYearList.Contains(Entry.FinancialYear)).ToList();
                }
                if (record.Count > 0)
                    record = record.OrderBy(entry => entry.Branch)
                        .ThenBy(entry => entry.VerticalName).ToList();

                return record;
            }
        }

        public void GetAuditStatusSummary(List<AuditCountView_Dashboard> AuditSummaryCountRecord, List<int> CustomerID, List<int?> BranchList, List<String> FinYearList, String Period, List<int?> VerticalID)
        {
            try
            {
                String newDivName = String.Empty;
                String newListName = String.Empty;
                String newLableName = String.Empty;
                String newLableNamePer = String.Empty;
                string FYear = string.Empty;
                int Count = 1;
                string FinYear = string.Empty;
                String[] FinancialYear = new String[2];

                if (FinYearList.Count == 0)
                    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<long> CustomerIdList = new List<long>();
                    if (CustomerID.Count > 0)
                    {
                        foreach (var cID in CustomerID)
                        {
                            CustomerIdList.Add(Convert.ToInt32(cID));
                        }
                    }
                    var AuditList = GetAuditManagerDashboardAudits(BranchList, FinYearList);

                    if (AuditList.Count > 0)
                    {
                        AuditList = AuditList.Where(a => CustomerIdList.Contains(a.CustomerID)).ToList();
                    }
                    var MasterRecords = (from row in entities.Sp_InternalAuditInstanceTransactionViewForAuditHeadDashboard(4)
                                         where CustomerID.Contains(row.CustomerID)
                                         //&& row.RoleID == 4
                                         select row).ToList();

                    if (!string.IsNullOrEmpty(FYear))
                    {
                        MasterRecords = MasterRecords.Where(entry => entry.FinancialYear == FYear).ToList();
                    }

                    if (AuditList.Count > 0)
                    {
                        AuditList.ForEach(EachAudit =>
                        {
                            //Create New Master Div To Contains Div that Contains Label and BulletedList 
                            System.Web.UI.HtmlControls.HtmlGenericControl newDivMaster = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                            newDivName = "DivAuditStatusMaster" + Count;
                            newDivMaster.ID = newDivName;

                            newDivMaster.Style["width"] = "100%";
                            newDivMaster.Style["float"] = "left";

                            //Create New Div To Contains Label 
                            System.Web.UI.HtmlControls.HtmlGenericControl newDivLabel = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                            newDivName = "DivAuditStatusLabel" + Count;
                            newDivLabel.ID = newDivName;
                            newDivLabel.Style["margin-top"] = "25px";
                            newDivLabel.Style["width"] = "25%";//"35%";
                            newDivLabel.Style["float"] = "left";

                            //Create New Label
                            System.Web.UI.WebControls.Label newLabel = new System.Web.UI.WebControls.Label();
                            newLableName = "Label" + Count;
                            newLabel.ID = newLableName;
                            newLabel.Font.Bold = true;

                            System.Web.UI.HtmlControls.HtmlGenericControl newDivLabel1 = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                            newDivName = "DivAuditStatusLabel1" + Count;
                            newDivLabel1.ID = newDivName;
                            newDivLabel1.Style["margin-top"] = "25px";
                            newDivLabel1.Style["width"] = "5%";
                            newDivLabel1.Style["float"] = "left";

                            System.Web.UI.WebControls.Label newLabel1 = new System.Web.UI.WebControls.Label();
                            newLableNamePer = "Label1" + Count;
                            newLabel1.ID = newLableNamePer;
                            newLabel1.Font.Bold = true;
                            newLabel1.Text = DisplayObservationInPercentage(EachAudit.FinancialYear, EachAudit.ForMonth, (int)EachAudit.CustomerID, (int)EachAudit.CustomerBranchID, (int)EachAudit.VerticalId, (int)EachAudit.AuditID);

                            //Create New Div To Contains BulletedList
                            System.Web.UI.HtmlControls.HtmlGenericControl newDivBulletList = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                            newDivName = "DivAuditStatusBulletedList" + Count;
                            newDivBulletList.ID = newDivName;
                            newDivBulletList.Style["width"] = "65%"; //"65%";
                            newDivBulletList.Style["float"] = "left";

                            //Create New BulletedList
                            System.Web.UI.WebControls.BulletedList newBulletedList = new System.Web.UI.WebControls.BulletedList();
                            newListName = "BulletedList" + Count;
                            newBulletedList.ID = newListName;
                            newBulletedList.CssClass = "progtrckr";
                            newBulletedList.BulletStyle = BulletStyle.Numbered;

                            System.Web.UI.WebControls.LinkButton newLinkButton = new System.Web.UI.WebControls.LinkButton();
                            newLinkButton.Text = "Action";
                            newLinkButton.ID = "newLinkButton" + Count;
                            newLinkButton.Style["width"] = "5%";
                            newLinkButton.Style["margin-top"] = "25px";
                            newLinkButton.Style["float"] = "left";

                            string custBranchId = string.Empty;
                            string forMonth = string.Empty;
                            string financialYear = string.Empty;
                            string verticalId = string.Empty;
                            string auditId = string.Empty;
                            string customerId = string.Empty;

                            custBranchId = Convert.ToString(EachAudit.CustomerBranchID);
                            forMonth = EachAudit.ForMonth;
                            financialYear = EachAudit.FinancialYear;
                            verticalId = Convert.ToString(EachAudit.VerticalId);
                            auditId = Convert.ToString(EachAudit.AuditID);
                            customerId = Convert.ToString(EachAudit.CustomerID);

                            System.Web.UI.HtmlControls.HtmlImage newImage = new System.Web.UI.HtmlControls.HtmlImage();
                            newImage.Attributes["data-placement"] = "top";
                            newImage.Attributes["data-toggle"] = "tooltip";
                            newImage.Attributes["title"] = "View Audit Details";
                            newImage.Attributes["alt"] = "View Audit Details";
                            newImage.Attributes["src"] = "../../Images/View-icon-new.png";
                            newLinkButton.OnClientClick = "javascript:return statusclick(" + customerId + "," + custBranchId + ",'" + forMonth + "','" + financialYear + "'," + verticalId + "," + auditId + ")";

                            var ProgressResult = CheckAuditStatusReviewWorkingManagementDashboard(MasterRecords, EachAudit.FinancialYear, EachAudit.ForMonth, (int)EachAudit.CustomerID, (int)EachAudit.CustomerBranchID, (int)EachAudit.VerticalId, (int)EachAudit.AuditID);
                            newLabel.Text = EachAudit.Branch + "/" + EachAudit.VerticalName + "/" + EachAudit.FinancialYear + "/" + EachAudit.ForMonth;
                            AddSteps(items, newBulletedList);
                            SetProgress(ProgressResult, newBulletedList);
                            newDivLabel.Controls.Add(newLabel);
                            newDivLabel1.Controls.Add(newLabel1);
                            newDivBulletList.Controls.Add(newBulletedList);
                            newDivMaster.Controls.Add(newDivLabel);
                            newDivMaster.Controls.Add(newDivLabel1);
                            newDivMaster.Controls.Add(newDivBulletList);
                            newLinkButton.Controls.Add(newImage);
                            newDivMaster.Controls.Add(newLinkButton);
                            DivGraphAuditStatus.Controls.Add(newDivMaster);

                            Count++;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void GetAuditObservationAgingSummary(List<ObservationAging_DisplayView> ObservationAging_DisplayRecord, List<int> CustomerID, List<int?> BranchList, List<String> FinYearList, String Period, List<int> ProcessIDList, List<int?> VerticalIDList)
        {
            try
            {
                ObservationProcessWiseAgingChart = String.Empty;

                String ProcesswiseObservationAgingSeries = String.Empty;
                String ListofProcesses = String.Empty;

                String NotDueList = String.Empty;
                String LessThan45List = String.Empty;
                String GreaterThan45andLessThan90List = String.Empty;
                String GreaterThan90List = String.Empty;

                long NotDuecount = 0;
                long LessThan45Count = 0;
                long GreaterThan45andLessThan90count = 0;
                long GreaterThan90count = 0;
                long UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;

                String newDivName = String.Empty;
                int CustomerBranchID = -1;
                List<int?> ImplementedStatusList = new List<int?> { 2, 4, 5 };

                List<string> branchIdListStringList = new List<string>();
                string branchIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        branchIdListStringList.Add(Convert.ToString(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }
                if (branchIdListStringList.Count > 0)
                {
                    branchIdCommaSeparatedList = string.Join(",", branchIdListStringList);
                }

                List<string> SubEntityOneIdList = new List<string>();
                string subEntityOneIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                {
                    if (ddlSubEntity1.Items[i].Selected)
                    {
                        SubEntityOneIdList.Add(ddlSubEntity1.Items[i].Value);
                    }
                }
                if (SubEntityOneIdList.Count > 0)
                {
                    subEntityOneIdCommaSeparatedList = string.Join(",", SubEntityOneIdList);
                }
                else
                {
                    subEntityOneIdCommaSeparatedList = "-1";
                }

                List<string> SubEntityTwoIdList = new List<string>();
                string SubEntityTwoIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                {
                    if (ddlSubEntity2.Items[i].Selected)
                    {
                        SubEntityTwoIdList.Add(ddlSubEntity2.Items[i].Value);
                    }
                }
                if (SubEntityTwoIdList.Count > 0)
                {
                    SubEntityTwoIdCommaSeparatedList = string.Join(",", SubEntityTwoIdList);
                }
                else
                {
                    SubEntityTwoIdCommaSeparatedList = "-1";
                }


                List<string> SubEntityThreeIdList = new List<string>();
                string SubEntityThreeIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                {
                    if (ddlSubEntity3.Items[i].Selected)
                    {
                        SubEntityThreeIdList.Add(ddlSubEntity3.Items[i].Value);
                    }
                }
                if (SubEntityThreeIdList.Count > 0)
                {
                    SubEntityThreeIdCommaSeparatedList = string.Join(",", SubEntityThreeIdList);
                }
                else
                {
                    SubEntityThreeIdCommaSeparatedList = "-1";
                }

                List<string> ddlFilterLocationIdList = new List<string>();
                string FilterLocationIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
                {
                    if (ddlFilterLocation.Items[i].Selected)
                    {
                        ddlFilterLocationIdList.Add(ddlFilterLocation.Items[i].Value);
                    }
                }
                if (ddlFilterLocationIdList.Count > 0)
                {
                    FilterLocationIdCommaSeparatedList = string.Join(",", ddlFilterLocationIdList);
                }
                else
                {
                    FilterLocationIdCommaSeparatedList = "-1";
                }

                List<long> ProcessIDList1 = new List<long>();
                if (ProcessIDList.Count > 0)
                {
                    foreach (int processId in ProcessIDList)
                    {
                        ProcessIDList1.Add(Convert.ToInt32(processId));
                    }
                }
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<Mst_Process> ProcessList = new List<Mst_Process>();
                    if (BranchList.Count > 0)
                    {
                        List<long> ListofBranch = new List<long>();
                        ListofBranch = Branchlist.Select(x => (long)x).ToList();
                        ProcessList = ProcessManagement.GetAssingedProcessForAuditHeadDashBoard(CustomerID, ListofBranch, UserID);
                    }
                    else
                    {
                        ProcessList = ProcessManagement.GetAssingedProcessForAuditHeadDashBoard(CustomerID, UserID);
                    }

                    if (ProcessIDList1.Count > 0)
                        ProcessList = ProcessList.Where(Entry => ProcessIDList1.Contains(Entry.Id)).ToList();

                    string customerid = string.Join(",", CustomerID);
                    string finYear = string.Join(",", FinYearList);

                    if (ProcessList.Count > 0)
                    {
                        int Count = 1;

                        ProcessList.ForEach(EachProcess =>
                        {
                            string custId = string.Empty;
                            string verticalIdString = string.Empty;

                            var Records = (from row in ObservationAging_DisplayRecord
                                           where CustomerID.Contains(row.CustomerID)
                                           && row.ProcessId == EachProcess.Id
                                           select row).ToList();


                            if (BranchList.Count > 0)
                                Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                            if (FinYearList.Count > 0)
                                Records = Records.Where(Entry => FinYearList.Contains(Entry.FinancialYear)).ToList();
                            else
                            {
                                string FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                            }

                            if (Period != "")
                                Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                            if (VerticalIDList.Count > 0)
                                Records = Records.Where(Entry => VerticalIDList.Contains(Entry.VerticalID)).ToList();

                            if (Records.Count > 0)
                            {

                                NotDuecount = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.Date && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                                LessThan45Count = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-45) && Entry.TimeLine < DateTime.Now.Date && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                                GreaterThan45andLessThan90count = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-90) && Entry.TimeLine < DateTime.Now.AddDays(-45) && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                                GreaterThan90count = Records.Where(Entry => Entry.TimeLine <= DateTime.Now.AddDays(-90) && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();


                                ListofProcesses += "'" + EachProcess.Name + "',";

                                System.Web.UI.HtmlControls.HtmlGenericControl newDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                                newDivName = "DivObsAging" + Count;
                                newDiv.ID = newDivName;

                                String newDivClientID = newDiv.ClientID;
                                newDiv.Style["height"] = "200px";
                                newDiv.Style["width"] = "20%";
                                newDiv.Style["float"] = "left";

                                DivObsAgingProcess.Controls.Add(newDiv); /*format: '<b>{point.name}</b>: {y}',*/
                                custId = Convert.ToString(EachProcess.CustomerID);
                                int VerticalID = -1;

                                ProcesswiseObservationAgingSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "', { chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie',marginBottom: 30,  }," +
                                    "title: {text: '" + EachProcess.Name + "',style: {fontSize: '12px' }}, legend: { itemDistance:0,itemStyle:{fontSize:'10px'},}, tooltip: { pointFormat: '{series.name}: <b>{point.y}</b>'}," +
                                    "plotOptions: {pie: {size: '165%', showInLegend: true, allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, format: '{y}', distance: -25," +
                                    "style: { color: (Highcharts.theme &&     Highcharts.theme.contrastTextColor) || 'black'} }, startAngle: -90, endAngle: 90,center: ['50%', '75%'] } }," +
                                    "series: [{ type: 'pie', name: '" + EachProcess.Name + "', innerSize: '50%', colorByPoint: true, " +
                                    "data: [{ name: 'Not Due', 	color: '#7cb5ec', y: " + NotDuecount + ", events:{click: function(e) { ShowOpenObservationDetails('NotDue'," + custId + "," + branchIdCommaSeparatedList + ",'" + finYear + "','" + Period + "'," + EachProcess.Id + "," + VerticalID + ",'AM') } } }," +
                                    " { name: '0-45 Days', color: '#90ed7d', y:" + LessThan45Count + ", events:{click: function(e) { ShowOpenObservationDetails('LessThan45'," + custId + "," + branchIdCommaSeparatedList + ",'" + finYear + "','" + Period + "'," + EachProcess.Id + "," + VerticalID + ",'AM') } } }," +
                                    " { name: '46-90 Days', color: '#e4d354', y: " + GreaterThan45andLessThan90count + ", events: { click: function(e) { ShowOpenObservationDetails('GreaterThan45'," + custId + "," + branchIdCommaSeparatedList + ",'" + finYear + "','" + Period + "'," + EachProcess.Id + "," + VerticalID + ",'AM') } } }," +
                                    " { name: '>90 Days', color: '#f45b5b', y:" + GreaterThan90count + ", events:{click: function(e) { ShowOpenObservationDetails('GreaterThan90'," + custId + "," + branchIdCommaSeparatedList + ",'" + finYear + "','" + Period + "'," + EachProcess.Id + "," + VerticalID + ",'AM') } } }] }] });";

                                ObservationProcessWiseAgingChart += ProcesswiseObservationAgingSeries;

                                Count++;
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdAuditTrackerSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].Text = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int index = e.Row.RowIndex;

                    int branchid = -1;
                    int VerticalID = -1;

                    String Period = String.Empty;

                    String PartialFromDate = String.Empty;
                    String PartialToDate = String.Empty;
                    String[] FinancialYear = new String[2];

                    List<string> FinancialYearList = new List<string>();
                    List<int?> VerticalIDList = new List<int?>();
                    List<int> CustomerIdsList = new List<int>();

                    CustomerIdsList.Clear();
                    for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                    {
                        if (ddlCustomerMultiSelect.Items[i].Selected)
                        {
                            CustomerIdsList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                        }
                    }

                    List<string> BranchIdList1 = new List<string>();
                    for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                    {
                        if (ddlLegalEntityMultiSelect.Items[i].Selected)
                        {
                            BranchIdList1.Add(ddlLegalEntityMultiSelect.Items[i].Value);
                        }
                    }

                    string branchIdCommaSeparatedList = string.Empty;
                    if (BranchIdList1.Count > 0)
                    {
                        branchIdCommaSeparatedList = string.Join(",", BranchIdList1);
                    }
                    else
                    {
                        branchIdCommaSeparatedList = "-1";
                    }


                    List<string> SubEntityOneIdList = new List<string>();
                    string subEntityOneIdCommaSeparatedList = string.Empty;
                    for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                    {
                        if (ddlSubEntity1.Items[i].Selected)
                        {
                            SubEntityOneIdList.Add(ddlSubEntity1.Items[i].Value);
                        }
                    }
                    if (SubEntityOneIdList.Count > 0)
                    {
                        subEntityOneIdCommaSeparatedList = string.Join(",", SubEntityOneIdList);
                    }
                    else
                    {
                        subEntityOneIdCommaSeparatedList = "-1";
                    }

                    List<string> SubEntityTwoIdList = new List<string>();
                    string SubEntityTwoIdCommaSeparatedList = string.Empty;
                    for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                    {
                        if (ddlSubEntity2.Items[i].Selected)
                        {
                            SubEntityTwoIdList.Add(ddlSubEntity2.Items[i].Value);
                        }
                    }
                    if (SubEntityTwoIdList.Count > 0)
                    {
                        SubEntityTwoIdCommaSeparatedList = string.Join(",", SubEntityTwoIdList);
                    }
                    else
                    {
                        SubEntityTwoIdCommaSeparatedList = "-1";
                    }

                    List<string> SubEntityThreeIdList = new List<string>();
                    string SubEntityThreeIdCommaSeparatedList = string.Empty;
                    for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                    {
                        if (ddlSubEntity3.Items[i].Selected)
                        {
                            SubEntityThreeIdList.Add(ddlSubEntity3.Items[i].Value);
                        }
                    }
                    if (SubEntityThreeIdList.Count > 0)
                    {
                        SubEntityThreeIdCommaSeparatedList = string.Join(",", SubEntityThreeIdList);
                    }
                    else
                    {
                        SubEntityThreeIdCommaSeparatedList = "-1";
                    }

                    List<string> ddlFilterLocationIdList = new List<string>();
                    string FilterLocationIdCommaSeparatedList = string.Empty;
                    for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
                    {
                        if (ddlFilterLocation.Items[i].Selected)
                        {
                            ddlFilterLocationIdList.Add(ddlFilterLocation.Items[i].Value);
                        }
                    }
                    if (ddlFilterLocationIdList.Count > 0)
                    {
                        FilterLocationIdCommaSeparatedList = string.Join(",", ddlFilterLocationIdList);
                    }
                    else
                    {
                        FilterLocationIdCommaSeparatedList = "-1";
                    }

                    string verticalIDCommaSeparatedList = string.Empty;
                    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                    {
                        List<int> verticalIdsList = UserManagementRisk.VerticalgetBycustomeridListForDashboard(CustomerIdsList);
                        if (verticalIdsList.Count > 0)
                        {
                            foreach (var vId in verticalIdsList)
                            {
                                VerticalIDList.Add(Convert.ToInt32(vId));
                            }
                        }

                        if (VerticalIDList.Count > 0)
                        {
                            verticalIDCommaSeparatedList = string.Join(",", VerticalIDList);
                        }
                    }

                    for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                    {
                        if (ddlFinancialYearMultiSelect.Items[i].Selected)
                        {
                            FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                        }
                    }

                    if (FinancialYearList.Count > 0)
                    {
                        string FinancialYearCommaSeparatedList = string.Join(",", FinancialYearList);
                        foreach (string FinYear in FinancialYearList)
                        {
                            if (FinYear != "")
                            {
                                FinancialYear = FinYear.Split('-');

                                PartialFromDate = "01/04/" + FinancialYear[0];
                                PartialToDate = "31/03/" + FinancialYear[1];
                            }
                            else
                            {
                                string FinYear1 = GetCurrentFinancialYear(DateTime.Now.Date);

                                if (FinYear1 != "")
                                {
                                    FinancialYear = FinYear1.Split('-');

                                    PartialFromDate = "01/04/" + FinancialYear[0];
                                    PartialToDate = "31/03/" + FinancialYear[1];
                                }
                            }
                        }
                    }

                    if (Period == "")
                        Period = "Period";

                    DateTime FROMDate = Convert.ToDateTime(GetDate(PartialFromDate));
                    DateTime TODate = Convert.ToDateTime(GetDate(PartialToDate));
                    string customerIdsCommanSeparatedList = string.Join(",", CustomerIdsList);
                    string financialYearListCommanSeparatedList = string.Join(",", FinancialYearList);
                    for (int i = 1; i <= 12; i++)
                    {
                        DateTime MonthStartDate = FROMDate.AddMonths(i - 1);
                        DateTime MonthEndDate = GetLastDayOfMonth(MonthStartDate);

                        //e.Row.Cells[i].Style.Add("background", lowcolor.Value);
                        //e.Row.Cells[i].Style.Add("color", "white");
                        e.Row.Cells[i].Style.Add("font-weight", "500");
                        e.Row.Cells[i].Style.Add("cursor", "pointer");
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;

                        if (index == 0)
                            e.Row.Cells[i].Attributes.Add("onclick", "PlannedActualAuditDetails('Planned','" + branchIdCommaSeparatedList + "','" + Period + "','" + MonthStartDate.Date + "','" + MonthEndDate.Date + "'," + VerticalID + ",'" + customerIdsCommanSeparatedList + "','" + financialYearListCommanSeparatedList + "','AM','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "')");
                        else if (index == 1)
                            e.Row.Cells[i].Attributes.Add("onclick", "PlannedActualAuditDetails('Actual','" + branchIdCommaSeparatedList + "','" + Period + "','" + MonthStartDate.Date + "','" + MonthEndDate.Date + "'," + VerticalID + ",'" + customerIdsCommanSeparatedList + "','" + financialYearListCommanSeparatedList + "','AM','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "')");

                        if (index == 0)
                        {
                            //e.Row.Cells[13].Style.Add("background", lowcolor.Value);
                            //e.Row.Cells[13].Style.Add("color", "white");
                            e.Row.Cells[13].Style.Add("cursor", "pointer");
                            e.Row.Cells[13].HorizontalAlign = HorizontalAlign.Center;
                            e.Row.Cells[13].Attributes.Add("onclick", "PlannedActualAuditDetails('Planned','" + branchIdCommaSeparatedList + "','" + Period + "','" + FROMDate.Date + "','" + TODate.Date + "'," + VerticalID + ",'" + customerIdsCommanSeparatedList + "','" + financialYearListCommanSeparatedList + "','AM','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "')");
                        }
                        else if (index == 1)
                        {
                            //e.Row.Cells[13].Style.Add("background", lowcolor.Value);
                            //e.Row.Cells[13].Style.Add("color", "white");
                            e.Row.Cells[13].Style.Add("cursor", "pointer");
                            e.Row.Cells[13].HorizontalAlign = HorizontalAlign.Center;
                            e.Row.Cells[13].Attributes.Add("onclick", "PlannedActualAuditDetails('Actual','" + branchIdCommaSeparatedList + "','" + Period + "','" + FROMDate.Date + "','" + TODate.Date + "'," + VerticalID + ",'" + customerIdsCommanSeparatedList + "','" + financialYearListCommanSeparatedList + "','AM','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "')");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        public DateTime? GetDate(string date)
        {
            if (date != null && date != "")
            {
                string date1 = "";

                if (date.Contains("/"))
                {
                    date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains("-"))
                {
                    date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains(" "))
                {
                    date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
                }

                return Convert.ToDateTime(date1);
            }
            else
            {
                return null;
            }
        }

        public DateTime GetLastDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, DateTime.DaysInMonth(dateTime.Year, dateTime.Month));
        }

        protected void AddSteps(string[] items, BulletedList ListName)
        {
            ListName.Items.Clear();
            ListName.Attributes["data-progtrckr-steps"] = items.Length.ToString();

            for (int i = 0; i < items.Length; i++)
            {
                ListName.Items.Add(new ListItem(items[i]));
            }
        }

        protected void SetProgress(long current, BulletedList ListName)
        {
            for (int i = 0; i < ListName.Items.Count; i++)
            {
                ListName.Items[i].Attributes["class"] =
                    (i < current) ? "progtrckr-done" : (i == current) ? "progtrckr-current" : "progtrckr-todo";

                if (i == 3 && i > current)
                    ListName.Items[i].Attributes["class"] = "progtrckr-todo-closed";
            }
            if (current == 4)
                ListName.Items[Convert.ToInt32(current) - 1].Attributes["class"] = "progtrckr-closed";
        }

        //23 Jan   
        #region Count      
        public static int GetAuditCountIMP(List<ImplimentInternalAuditClosedCountView> ImplimentInternalAuditClosedCountView, int userid, int statusid, List<int> customerIDList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;

                if (statusid == 1)
                {

                    int flag = 0;
                    var OpenImplimentCount = (from row in ImplimentInternalAuditClosedCountView
                                              where row.UserID == userid && customerIDList.Contains(row.CustomerID)
                                              && row.AllClosed != flag
                                              select row).ToList();


                    count = OpenImplimentCount.Count();
                }
                else if (statusid == 3)
                {

                    int flag = 0;
                    var closedImplimentCount = (from row in ImplimentInternalAuditClosedCountView
                                                where row.UserID == userid && customerIDList.Contains(row.CustomerID)
                                                && row.AllClosed == flag
                                                select row).ToList();
                    count = closedImplimentCount.Count;


                }
                return count;
            }
        }
        public static int GetAuditCount(List<AuditCountView_Dashboard> AuditSummaryCountRecords, int userid, int statusid, List<int> CustomerIdsList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();

                if (statusid == 1)
                {
                    record = (from C in AuditSummaryCountRecords
                              where C.UserID == userid
                              && CustomerIdsList.Contains(C.CustomerID)
                              && C.ACCStatus == null
                              group C by new
                              {
                                  C.CustomerID,
                                  C.AuditID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  //C.RoleID,
                                  C.UserID,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  AuditID = GCS.Key.AuditID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  //RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                              }).ToList();

                    count = record.Count();

                }
                else if (statusid == 3)
                {
                    record = (from C in AuditSummaryCountRecords
                              where C.UserID == userid
                              && CustomerIdsList.Contains(C.CustomerID)
                              && C.ACCStatus == 1
                              group C by new
                              {
                                  C.CustomerID,
                                  C.AuditID,
                                  C.CustomerBranchID,
                                  C.Branch,
                                  C.ForMonth,
                                  //C.RoleID,
                                  C.UserID,
                                  C.FinancialYear,
                                  C.ExpectedStartDate,
                                  C.ExpectedEndDate,
                                  C.VerticalsId,
                                  C.VerticalName
                              } into GCS
                              select new OpenCloseAuditDetailsClass()
                              {
                                  CustomerID = GCS.Key.CustomerID,
                                  AuditID = GCS.Key.AuditID,
                                  CustomerBranchID = GCS.Key.CustomerBranchID,
                                  Branch = GCS.Key.Branch,
                                  ForMonth = GCS.Key.ForMonth,
                                  //RoleID = GCS.Key.RoleID,
                                  UserID = GCS.Key.UserID,
                                  FinancialYear = GCS.Key.FinancialYear,
                                  ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                  ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                  VerticalId = GCS.Key.VerticalsId,
                                  VerticalName = GCS.Key.VerticalName,
                              }).ToList();
                    count = record.Count();
                }
                return count;
            }
        }
        public static int GetAuditManagerAuditCountIMP(List<ImplimentInternalAuditClosedCountView> ImplimentInternalAuditClosedCountView, int userid, int statusid, List<int> customerIDList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                string CheckStatus = CustomerManagementRisk.GetAuditHeadOrManagerid(userid);
                if (CheckStatus == "AM" || CheckStatus == "AH")
                {
                    if (statusid == 1)
                    {
                        int flag = 0;
                        var OpenImplimentCount = (from row in ImplimentInternalAuditClosedCountView
                                                  where customerIDList.Contains(row.CustomerID) && row.RoleID == 3
                                                  && row.AllClosed != flag
                                                  select new
                                                  {
                                                      row.CustomerBranchID,
                                                      row.FinancialYear,
                                                      row.ForMonth,
                                                      row.VerticalID,
                                                      row.RoleID
                                                  }).Distinct().ToList();


                        count = OpenImplimentCount.Count();
                    }
                    else if (statusid == 3)
                    {

                        var closedImplimentCount = (from row in ImplimentInternalAuditClosedCountView
                                                    where customerIDList.Contains(row.CustomerID)
                                                    && row.AllClosed == 0 && row.RoleID == 3
                                                    select new
                                                    {
                                                        row.CustomerBranchID,
                                                        row.FinancialYear,
                                                        row.ForMonth,
                                                        row.VerticalID,
                                                        row.RoleID
                                                    }).Distinct().ToList();


                        count = closedImplimentCount.Count;

                    }
                }
                return count;
            }
        }
        public static int GetAuditManagerAuditCount(List<AuditCountView_Dashboard> AuditSummaryCountRecords, int userid, int statusid, List<int> CustomerIdsList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int count = 0;
                List<OpenCloseAuditDetailsClass> record = new List<OpenCloseAuditDetailsClass>();
                string CheckStatus = CustomerManagementRisk.GetAuditHeadOrManagerid(userid);
                if (CheckStatus == "AM" || CheckStatus == "AH")
                {
                    if (statusid == 1)
                    {
                        record = (from C in AuditSummaryCountRecords
                                  where CustomerIdsList.Contains(C.CustomerID)
                                  && (C.ACCStatus == null)
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                  }).ToList();

                        count = record.Count();
                    }
                    else if (statusid == 3)
                    {
                        record = (from C in AuditSummaryCountRecords
                                  where CustomerIdsList.Contains(C.CustomerID)
                                    && C.ACCStatus == 1
                                  group C by new
                                  {
                                      C.CustomerID,
                                      C.AuditID,
                                      C.CustomerBranchID,
                                      C.Branch,
                                      C.ForMonth,
                                      C.FinancialYear,
                                      C.ExpectedStartDate,
                                      C.ExpectedEndDate,
                                      C.VerticalsId,
                                      C.VerticalName
                                  } into GCS
                                  select new OpenCloseAuditDetailsClass()
                                  {
                                      CustomerID = GCS.Key.CustomerID,
                                      AuditID = GCS.Key.AuditID,
                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                      Branch = GCS.Key.Branch,
                                      ForMonth = GCS.Key.ForMonth,
                                      FinancialYear = GCS.Key.FinancialYear,
                                      ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                      ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                      VerticalId = GCS.Key.VerticalsId,
                                      VerticalName = GCS.Key.VerticalName,
                                  }).ToList();

                        count = record.Count();
                    }
                }
                return count;
            }
        }
        #endregion

        #region Not Used Function
        public decimal GetPercentageAuditPerformerImpClose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditIMPCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditIMPCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditPerformerImpOpen()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditIMPCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditIMPCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditPerformerProcesOpen()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditPerformerProcesClose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAuditCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAuditCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditImplementionClose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAMIMPCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAMIMPCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditImplementionOpen()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAMIMPCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAMIMPCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditprocessopen()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAMCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAMCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditOpen > 0)
            {
                if (AuditClose > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditOpen) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }
        public decimal GetPercentageAuditprocessClose()
        {
            decimal returnvalue = 0;

            int AuditOpen = Convert.ToInt32(divOpenAMCount.InnerText);

            int AuditClose = Convert.ToInt32(divCloseAMCount.InnerText);

            int total = AuditOpen + AuditClose;

            if (AuditClose > 0)
            {
                if (AuditOpen > 0)
                {
                    returnvalue = ((Convert.ToDecimal(AuditClose) / Convert.ToDecimal(total)) * 100);
                }
                else
                {
                    returnvalue = 100;
                }
            }
            return returnvalue;
        }

        // added by sagar more on 28-01-2020
        private void BindCustomerList()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDL(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomerMultiSelect.DataTextField = "CustomerName";
                ddlCustomerMultiSelect.DataValueField = "CustomerId";
                ddlCustomerMultiSelect.Items.Clear();
                ddlCustomerMultiSelect.DataSource = customerList;
                ddlCustomerMultiSelect.DataBind();
                if (String.IsNullOrEmpty(Request.QueryString["customerId"]))
                {
                    //ddlCustomerMultiSelect.SelectedIndex = 0;
                    for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
                    {
                        ddlCustomerMultiSelect.Items[i].Selected = true;
                    }
                }
                else
                {
                    ddlCustomerMultiSelect.SelectedValue = Convert.ToString(Request.QueryString["customerId"]);
                }
            }
            else
            {
                ddlCustomerMultiSelect.DataSource = null;
                ddlCustomerMultiSelect.DataBind();
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    IsHiddenCustomerId = ddlCustomer.SelectedValue.ToString();
                    //BindProcess("P");
                    //BindLegalEntityData();
                    collapseDivFilters.Attributes.Remove("class");
                    collapseDivFilters.Attributes.Add("class", "panel-collapse in");
                }
                else
                {
                    Response.Write("<script>alert('Please Select Customer.');</script>");
                }
            }
        }

        protected void ddlCustomerMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {

            List<int?> customerIdsList = new List<int?>();
            customerIdsList.Clear();
            for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
            {
                if (ddlCustomerMultiSelect.Items[i].Selected)
                {
                    customerIdsList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                }
            }

            if (customerIdsList.Count > 0)
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                }
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }

                List<string> custIdList = new List<string>();
                foreach (var item in customerIdsList)
                {
                    custIdList.Add(Convert.ToString(item));
                }
                IsHiddenCustomerId = string.Join(",", custIdList);
                BindLegalEntityData(customerIdsList);
                collapseDivFilters.Attributes.Remove("class");
                collapseDivFilters.Attributes.Add("class", "panel-collapse in");
            }
            else
            {
                ddlProcessMultiSelect.Items.Clear();
                ddlLegalEntityMultiSelect.Items.Clear();
                Response.Write("<script>alert('Please Select Customer.');</script>");
                return;
            }
        }

        protected void ddlLegalEntityMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            CHKBranchlist.Clear();

            for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
            {
                if (ddlLegalEntityMultiSelect.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                }
            }

            if (CHKBranchlist.Count > 0)
            {
                IsHiddenSubEntity = string.Join(",", CHKBranchlist);
                BindSubEntityDataList(ddlSubEntity1, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                }
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }

        protected void ddlFinancialYearMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<int?> customerIdsList = new List<int?>();
            customerIdsList.Clear();
            string commaSeparatedCustomerId = string.Empty;
            for (int i = 0; i < ddlCustomerMultiSelect.Items.Count; i++)
            {
                if (ddlCustomerMultiSelect.Items[i].Selected)
                {
                    customerIdsList.Add(Convert.ToInt32(ddlCustomerMultiSelect.Items[i].Value));
                }
            }

            List<string> FinancialYearList = new List<string>();
            FinancialYearList.Clear();
            string commaSeparatedFinancialYear = string.Empty;
            for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
            {
                if (ddlFinancialYearMultiSelect.Items[i].Selected)
                {
                    FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                }
            }
            if (customerIdsList.Count > 0)
            {
                commaSeparatedCustomerId = string.Join(",", customerIdsList);
                commaSeparatedFinancialYear = string.Join(",", FinancialYearList);
                collapseDivFilters.Attributes.Remove("class");
                collapseDivFilters.Attributes.Add("class", "panel-collapse in");
            }
        }

        private void BindProcess(string flag, List<int?> CustomerIdsList)
        {
            try
            {
                if (CustomerIdsList.Count > 0)
                {
                    ddlProcessMultiSelect.Items.Clear();
                    ddlProcessMultiSelect.DataTextField = "Name";
                    ddlProcessMultiSelect.DataValueField = "Id";
                    ddlProcessMultiSelect.DataSource = ProcessManagement.FillProcessDropdownAuditManagerForDashBoard(CustomerIdsList, Portal.Common.AuthenticationHelper.UserID);
                    ddlProcessMultiSelect.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static string DisplayObservationInPercentage(String FinYear, String Period, int customerid, int custbranchid, int verticalid, int AuditID)
        {

            long NotDonecount;
            long submitedcount;
            long TeamReviewcount;
            long AuditeeReviewcount;
            long FinalReviewcount;
            long closecount;
            long totalcount;
            long InstanceID;
            long ScheduleOnID;
            long CustomerBranchID;
            int VerticalID;
            int totalLastcount = 0;
            int totalNotDoneLastcount = 0;
            int totalsubmitedLastcount = 0;
            int totalTeamReviewLastcount = 0;
            int totalAuditeeReviewLastcount = 0;
            int totalFinalReviewLastcount = 0;
            int totalcloseLastcount = 0;
            int ProcessIDLast = 0;
            int SubProcessIDLast = 0;
            string ProcessNameLast = null;
            string SubProcessNameLast = null;
            int InstanceIDLast = 0;
            int ScheduleOnIDLast = 0;
            int CustomerBranchIDLast = 0;
            int VerticalIDLast = 0;
            string formonth = null;
            string FinancialYear = null;
            string percentage = string.Empty;
            string finalPercentage = string.Empty;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var processids = GetDistinctAuditManagerProcessIDs(Convert.ToInt32(AuditID), Portal.Common.AuthenticationHelper.UserID);
                if (processids.Count > 0)
                {
                    var AuditSummaryMasterRecords = (from row in entities.SP_AuditStatusSummary(custbranchid, verticalid, AuditID)
                                                     select row).ToList();

                    processids.ForEach(ProcessId =>
                    {
                        var subprocessList = GetSubProcessDetails(ProcessId, customerid);
                        foreach (ProcessSubProcessView cc in subprocessList)
                        {
                            var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                       where row.ProcessId == cc.ProcessID
                                                       && row.SubProcessId == cc.SubProcessID
                                                       && row.RoleID == 4 && row.AuditID == AuditID
                                                       select row).ToList();


                            NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null).Count();

                            submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                            TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                            AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                            FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                            closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                            totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                            if (AuditSummaryRecords.Count > 0)
                            {
                                InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                                ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                                CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                                VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalId).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalId).FirstOrDefault();
                                formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                                FinancialYear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                            }
                            else
                            {
                                InstanceID = 0;
                                ScheduleOnID = 0;
                                CustomerBranchID = 0;
                                VerticalID = 0;
                            }

                            if (totalcount != 0)
                            {
                                totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                                totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                                totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                                totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                                totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                                totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                                totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                                ProcessIDLast = Convert.ToInt32(cc.ProcessID);
                                SubProcessIDLast = Convert.ToInt32(cc.SubProcessID);
                                ProcessNameLast = cc.ProcessName.ToString();
                                SubProcessNameLast = cc.SubProcessName.ToString();
                                InstanceIDLast = Convert.ToInt32(InstanceID);
                                ScheduleOnIDLast = Convert.ToInt32(ScheduleOnID);
                                CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                                VerticalIDLast = Convert.ToInt32(VerticalID);

                            }
                        }
                    });
                    long sum = totalAuditeeReviewLastcount + totalFinalReviewLastcount + totalcloseLastcount;

                    long mul = sum * 100;
                    long per = mul / totalLastcount;
                    percentage = Convert.ToString(per);
                    finalPercentage = percentage + "%";
                }
            }
            return finalPercentage;
        }

        public static List<long> GetDistinctAuditManagerProcessIDs(int AuditID, int UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessList = (from row in entities.InternalControlAuditAssignments
                                   join row1 in entities.EntitiesAssignmentAuditManagerRisks
                                   on row.ProcessId equals row1.ProcessId
                                   where row.CustomerBranchID == row1.BranchID
                                   && row.AuditID == AuditID && row1.UserID == UserID
                                  && row1.ISACTIVE == true
                                   select row.ProcessId).Distinct().ToList();

                return ProcessList.ToList();
            }
        }

        public static List<ProcessSubProcessView> GetSubProcessDetails(long ProcessId, int customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var SubProcessList = (from row in entities.ProcessSubProcessViews
                                      where row.ProcessID == ProcessId && row.CustomerID == customerid
                                      select row);

                SubProcessList = SubProcessList.OrderBy(entry => entry.SubProcessName);

                return SubProcessList.ToList();
            }
        }
        #endregion
    }
}