﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditAssignment : System.Web.UI.Page
    {
        public static List<int> Verticallist = new List<int>();
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindCustomerList();
                BindLegalEntityData();
                BindAuditor();

                BindAuditorList();
                bindPageNumber();
                //GetPageDisplaySummary();
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());


                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditor.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindAuditorList();
        }


        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            // code comment by sagar on 30-01-2020
            // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int customerID = -1;
            // code added by sagar more on 30-01-2020
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }

            // code added by sagar more on 30-01-2020
            if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
            {
                if (ddlCustomerPopup.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                }
            }

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            // DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, customerID, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        public void BindLegalEntityData()
        {
            // code comment by sagar on 30-01-2020
            // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            int customerID = -1;
            // code added by sagar more on 30-01-2020
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }


            var legalentitydata = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = legalentitydata;
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));

            ddlLegalEntityPopPup.DataTextField = "Name";
            ddlLegalEntityPopPup.DataValueField = "ID";
            ddlLegalEntityPopPup.Items.Clear();
            ddlLegalEntityPopPup.DataSource = legalentitydata;
            ddlLegalEntityPopPup.DataBind();
            ddlLegalEntityPopPup.Items.Insert(0, new ListItem("Unit", "-1"));
        }


        #region Form Filter

        public void BindVerticalID(int? BranchId)
        {
            if (BranchId != null)
            {
                // code comment by sagar on 30-01-2020
                // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int customerID = -1;
                // code added by sagar more on 30-01-2020
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                // code added by sagar more on 30-01-2020
                if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
                {
                    if (ddlCustomerPopup.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                    }
                }

                ddlVerticalID.DataTextField = "VerticalName";
                ddlVerticalID.DataValueField = "ID";
                ddlVerticalID.Items.Clear();
                ddlVerticalID.DataSource = UserManagementRisk.FillVerticalList(customerID, BranchId);
                ddlVerticalID.DataBind();
                ddlVerticalID.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
        }


        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    BindVerticalID(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        #endregion

        #region PopPup Filter

        public void BindVerticalIDPopPup(int? BranchId)
        {
            if (BranchId != null)
            {
                // code comment by sagar on 30-01-2020
                // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int customerID = -1;
                // code added by sagar more on 30-01-2020
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                // code added by sagar more on 30-01-2020
                if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
                {
                    if (ddlCustomerPopup.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                    }
                }

                ddlVerticalIDPopPup.DataTextField = "VerticalName";
                ddlVerticalIDPopPup.DataValueField = "ID";
                ddlVerticalIDPopPup.Items.Clear();
                ddlVerticalIDPopPup.DataSource = UserManagementRisk.FillVerticalList(customerID, BranchId);
                ddlVerticalIDPopPup.DataBind();
                //ddlVerticalIDPopPup.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
        }
        public string GetBranchVerticalList(int CustID, int Branchid)
        {
            List<Internal_AuditAssignment> Internal_AuditAssignmentMappingList = new List<Internal_AuditAssignment>();

            string Internal_AuditAssignmentList = "";

            Internal_AuditAssignmentMappingList = RiskCategoryManagement.GetAllBInternal_AuditAssignmentList(CustID, Branchid).ToList();
            Internal_AuditAssignmentMappingList = Internal_AuditAssignmentMappingList.OrderBy(entry => entry.VerticalID).ToList();

            if (Internal_AuditAssignmentMappingList.Count > 0)
            {
                foreach (var row in Internal_AuditAssignmentMappingList)
                {
                    Internal_AuditAssignmentList += row.VerticalID + " ,";
                }
            }

            return Internal_AuditAssignmentList.Trim(',');
        }

        public void SplitString(string StrToSplit, Saplin.Controls.DropDownCheckBoxes DrpToFill)
        {
            string[] commandArgs = StrToSplit.Split(new char[] { ',' });

            string a = string.Empty;

            if (commandArgs.Length > 0)
            {
                for (int i = 0; i < commandArgs.Length; i++)
                {
                    a = commandArgs[i];
                    if (!string.IsNullOrEmpty(a))
                    {
                        if (DrpToFill.Items.Count > 0)
                        {
                            DrpToFill.Items.FindByValue(a.Trim()).Selected = true;
                        }
                    }
                }
            }
        }
        protected void ddlLegalEntityPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
            {
                if (ddlLegalEntityPopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1PopPup, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                    BindVerticalIDPopPup(Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));

                    ddlVerticalIDPopPup.ClearSelection();
                    int customerID = -1;
                    // code comment by sagar on 30-01-2020
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    // code added by sagar more on 30-01-2020
                    if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
                    {
                        if (ddlCustomerPopup.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                        }
                    }

                    SplitString(GetBranchVerticalList(customerID, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue)), ddlVerticalIDPopPup);
                }
            }
            else
            {
                if (ddlSubEntity1PopPup.Items.Count > 0)
                    ddlSubEntity1PopPup.Items.Clear();

                if (ddlSubEntity2PopPup.Items.Count > 0)
                    ddlSubEntity2PopPup.Items.Clear();

                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.Items.Clear();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.Items.Clear();
            }
        }

        protected void ddlSubEntity1PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
            {
                if (ddlSubEntity1PopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2PopPup, Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue));
                    BindVerticalIDPopPup(Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue));
                    ddlVerticalIDPopPup.ClearSelection();
                    int customerID = -1;



                    SplitString(GetBranchVerticalList(customerID, Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue)), ddlVerticalIDPopPup);
                }
            }
            else
            {
                if (ddlSubEntity2PopPup.Items.Count > 0)
                    ddlSubEntity2PopPup.ClearSelection();

                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.ClearSelection();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }
        }

        protected void ddlSubEntity2PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
            {
                if (ddlSubEntity2PopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3PopPup, Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                    BindVerticalIDPopPup(Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                    ddlVerticalIDPopPup.ClearSelection();
                    int customerID = -1;
                    //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;

                    // code comment by sagar on 30-01-2020
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    // code added by sagar more on 30-01-2020
                    if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
                    {
                        if (ddlCustomerPopup.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                        }
                    }

                    SplitString(GetBranchVerticalList(customerID, Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue)), ddlVerticalIDPopPup);

                }
            }
            else
            {
                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.ClearSelection();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }
        }

        protected void ddlSubEntity3PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
            {
                if (ddlSubEntity3PopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocationPopPup, Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                    BindVerticalIDPopPup(Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                    ddlVerticalIDPopPup.ClearSelection();

                    // code comment by sagar on 30-01-2020
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int customerID = -1;
                    // code added by sagar more on 30-01-2020
                    if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
                    {
                        if (ddlCustomerPopup.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                        }
                    }

                    SplitString(GetBranchVerticalList(customerID, Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue)), ddlVerticalIDPopPup);


                }
            }
            else
            {
                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }
        }

        protected void ddlFilterLocationPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
            {
                if (ddlFilterLocationPopPup.SelectedValue != "-1")
                {
                    BindVerticalIDPopPup(Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue));
                    ddlVerticalIDPopPup.ClearSelection();
                    // code comment by sagar on 30-01-2020
                    // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int customerID = -1;
                    // code added by sagar more on 30-01-2020
                    if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
                    {
                        if (ddlCustomerPopup.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                        }
                    }

                    SplitString(GetBranchVerticalList(customerID, Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue)), ddlVerticalIDPopPup);


                    //BindAuditorList();

                }
            }
        }

        #endregion

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void ddlLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                //txtLocationAdd.Text = ddlLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public string ShowCustomerBranchName(long branchid)
        {
            string processnonprocess = "";
            if (branchid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(branchid);
                    mst_CustomerBranch mstcustomerbranch = (from row in entities.mst_CustomerBranch
                                                            where row.ID == a && row.IsDeleted == false && row.Status == 1
                                                            select row).FirstOrDefault();

                    processnonprocess = mstcustomerbranch.Name;
                }
            }
            return processnonprocess;
        }

        public string ShowAssignedTo(string assignedto)
        {
            string processnonprocess = "";
            if (assignedto == "I")
            {
                processnonprocess = "Internal";
            }
            else
            {
                processnonprocess = "External";
            }
            return processnonprocess;
        }

        public string ShowExternalAuditor(long ExternalAuditorId)
        {
            string processnonprocess = "";
            if (ExternalAuditorId != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    Mst_AuditorMaster mstauditorMaster = (from row in entities.Mst_AuditorMaster
                                                          where row.ID == ExternalAuditorId
                                                          select row).FirstOrDefault();

                    processnonprocess = mstauditorMaster.Name;
                }
            }
            return processnonprocess;
        }

        protected void ddlAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAssignedTo.SelectedItem.Text == "Internal")
            {
                ddlAuditor.Items.Clear();
                ddlAuditor.Items.Insert(0, new ListItem("Select Auditor", "-1"));
                DivIsExternal.Visible = false;
            }
            else
            {
                DivIsExternal.Visible = true;
                BindAuditor();
            }
        }

        public bool EditAssignmentVisibleorNot(int CustBranchID, int VerticalID)
        {
            try
            {
                return RiskCategoryManagement.CheckAuditKickedOfforNot(CustBranchID, VerticalID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private void BindAuditor()
        {
            try
            {
                // code comment by sagar on 30-01-2020
                // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int customerID = -1;
                // code added by sagar more on 30-01-2020
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                ddlAuditor.Items.Clear();
                ddlAuditor.DataTextField = "Name";
                ddlAuditor.DataValueField = "ID";
                ddlAuditor.DataSource = ProcessManagement.GetAllAuditorMaster(customerID);
                ddlAuditor.DataBind();
                ddlAuditor.Items.Insert(0, new ListItem("Select Auditor", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlAuditor_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void upPromotor_Load(object sender, EventArgs e)
        {

        }

        protected void grdAuditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                DropDownListChosen.DropDownListChosen ddlReviewer = (e.Row.FindControl("ddlAuditordr") as DropDownListChosen.DropDownListChosen);
                Label lblid = (e.Row.FindControl("lblAuditor") as Label);
                CheckBox Internal = (e.Row.FindControl("ïnterInnercheck") as CheckBox);
                CheckBox External = (e.Row.FindControl("externalInnercheck") as CheckBox);
                if (ddlReviewer != null)
                {
                    if (ddlAuditor != null)
                    {
                        for (int i = 1; i < ddlAuditor.Items.Count; i++)
                        {
                            ListItem li = new ListItem(ddlAuditor.Items[i].Text, ddlAuditor.Items[i].Value);
                            ddlReviewer.Items.Add(li);
                        }
                    }
                    if (Internal.Checked == true)
                    {
                        ddlReviewer.Enabled = false;
                    }
                    else if (External.Checked == true)
                    {
                        ddlReviewer.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindAuditorList()
        {
            try
            {
                // code comment by sagar more on 30-01-2020
                //customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                int CustomerBranchId = -1;
                int VerticalListID = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    // comment by sagar more on 30-01-2020
                    //int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);

                    // added by sagar more on 30-01-2020
                    int vid = UserManagementRisk.VerticalgetBycustomerid(customerID);

                    if (vid != -1)
                    {
                        VerticalListID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
                    {
                        if (ddlVerticalID.SelectedValue != "-1")
                        {
                            VerticalListID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                        }
                    }
                }
                Branchlist.Clear();
                var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                var Branchlistloop = Branchlist.ToList();


                var AuditorMasterList = ProcessManagement.GetAllInternalAuditAssignment(customerID, Branchlist.ToList(), VerticalListID, Portal.Common.AuthenticationHelper.UserID, Portal.Common.AuthenticationHelper.Role);
                grdAuditor.DataSource = AuditorMasterList;
                Session["TotalRows"] = AuditorMasterList.Count;
                grdAuditor.DataBind();
                Branchlist.Clear();
                upPromotorList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static List<Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<Business.Data.NameValueHierarchy> hierarchy = null;
            using (Business.Data.ComplianceDBEntities entities = new Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, Business.Data.NameValueHierarchy nvp, bool isClient, Business.Data.ComplianceDBEntities entities)
        {


            IQueryable<Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                              where row.IsDeleted == false && row.CustomerID == customerid
                                                               && row.ParentID == nvp.ID
                                                              select row);
            var subEntities = query.ToList().Select(entry => new Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int AuditorId = -1;
                int CustomerBranchID = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
                {
                    if (ddlLegalEntityPopPup.SelectedValue != "-1")
                    {
                        CustomerBranchID = Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
                {
                    if (ddlSubEntity1PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchID = Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
                {
                    if (ddlSubEntity2PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchID = Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
                {
                    if (ddlSubEntity3PopPup.SelectedValue != "-1")
                    {
                        CustomerBranchID = Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
                {
                    if (ddlFilterLocationPopPup.SelectedValue != "-1")
                    {
                        CustomerBranchID = Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlAuditor.SelectedValue))
                {
                    if (ddlAuditor.SelectedValue != "-1")
                    {
                        AuditorId = Convert.ToInt32(ddlAuditor.SelectedValue);
                    }
                }
                string flag = "";
                if (ddlAssignedTo.SelectedItem.Text == "Internal")
                    flag = "I";
                else
                    flag = "E";

                if (flag == "E")
                {
                    if (AuditorId == -1)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Auditor.";
                        return;
                    }
                }
                if (CustomerBranchID != -1)
                {

                    int customerID = -1;
                    // code comment by sagar more on 30-01-2020                
                    //customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                    // code added by sagar more on 30-01-2020
                    if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
                    {
                        if (ddlCustomerPopup.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                        }
                    }
                    Branchlist.Clear();
                    List<long> CommanBranchlist = new List<long>();
                    var bracnhes = GetAllHierarchy(customerID, CustomerBranchID);
                    if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
                    {
                        CommanBranchlist = Branchlist.ToList();
                    }
                    else if (Portal.Common.AuthenticationHelper.Role.Equals("MGMT"))
                    {
                        List<long> branchids = AssignEntityManagementRisk.CheckManngementLocation(Portal.Common.AuthenticationHelper.UserID);
                        var Branchlistloop = Branchlist.ToList();
                        if (Branchlistloop.Count > 0)
                        {
                            CommanBranchlist = Branchlist.Intersect(branchids).ToList();
                        }
                        branchids.Clear();
                    }
                    else
                    {
                        List<long> branchids = AssignEntityManagementRisk.CheckAuditManagerLocation(Portal.Common.AuthenticationHelper.UserID);
                        var Branchlistloop = Branchlist.ToList();
                        if (Branchlistloop.Count > 0)
                        {
                            CommanBranchlist = Branchlist.Intersect(branchids).ToList();
                        }
                        branchids.Clear();
                    }


                    bool sucess = false;
                    if (CommanBranchlist.Count > 0)
                    {
                        sucess = RiskCategoryManagement.UpdateInternal_AuditAssignment(customerID, CommanBranchlist.ToList());
                    }

                    Verticallist.Clear();
                    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                    {
                        int vid = UserManagementRisk.VerticalgetBycustomerid(customerID);
                        if (vid != -1)
                        {
                            Verticallist.Add(vid);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < ddlVerticalIDPopPup.Items.Count; i++)
                        {
                            if (ddlVerticalIDPopPup.Items[i].Selected)
                            {
                                Verticallist.Add(Convert.ToInt32(ddlVerticalIDPopPup.Items[i].Value));
                            }
                        }
                    }
                    List<Internal_AuditAssignment> InternalAuditAssignmentlist = new List<Internal_AuditAssignment>();
                    if (Verticallist.Count > 0)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {

                            var masterdata = (from row in entities.Internal_AuditAssignment
                                              where row.CustomerId == customerID
                                              select row).ToList();

                            foreach (var aItem in Verticallist)
                            {
                                for (int i = 0; i < CommanBranchlist.Count; i++)
                                {
                                    if (RiskCategoryManagement.Internal_AuditAssignmentExists(InternalAuditAssignmentlist, CommanBranchlist[i], Convert.ToInt32(aItem)))
                                    {
                                        if (EditAssignmentVisibleorNot((int)CommanBranchlist[i], Convert.ToInt32(aItem)))
                                        {
                                            RiskCategoryManagement.EditInternal_AuditAssignmentExist(customerID, CommanBranchlist[i], Convert.ToInt32(aItem), Portal.Common.AuthenticationHelper.UserID);
                                        }
                                    }
                                    else
                                    {
                                        Internal_AuditAssignment mstauditorusermaster = new Internal_AuditAssignment();
                                        mstauditorusermaster.CustomerBranchid = CommanBranchlist[i];
                                        mstauditorusermaster.AssignedTo = flag;
                                        mstauditorusermaster.ExternalAuditorId = AuditorId;
                                        mstauditorusermaster.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                        mstauditorusermaster.CreatedOn = DateTime.Now;
                                        mstauditorusermaster.IsActive = false;
                                        mstauditorusermaster.CustomerId = customerID;
                                        mstauditorusermaster.VerticalID = aItem;
                                        InternalAuditAssignmentlist.Add(mstauditorusermaster);
                                    }
                                }
                            }
                            if (InternalAuditAssignmentlist.Count > 0)
                            {
                                sucess = RiskCategoryManagement.CreateInternal_AuditAssignmentlist(InternalAuditAssignmentlist);
                                Verticallist.Clear();

                                Branchlist.Clear();
                                CommanBranchlist.Clear();
                            }
                        }

                        if (sucess)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Audit Assignment Save Successfully.";
                            BindAuditorList();
                            bindPageNumber();
                            int count = Convert.ToInt32(GetTotalPagesCount());
                            if (count > 0)
                            {
                                int gridindex = grdAuditor.PageIndex;
                                string chkcindition = (gridindex + 1).ToString();
                                DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again.";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Vertical.";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Unit.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }



        protected void btnAddAuditAssignment_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                ddlLegalEntityPopPup.SelectedIndex = -1; ddlSubEntity1PopPup.SelectedIndex = -1;
                ddlSubEntity2PopPup.SelectedIndex = -1; ddlSubEntity3PopPup.SelectedIndex = -1;
                ddlFilterLocationPopPup.SelectedIndex = -1; ddlVerticalIDPopPup.SelectedIndex = -1;
                ddlAuditor.SelectedIndex = -1;
                ddlAssignedTo.SelectedValue = "Internal";
                ddlAssignedTo_SelectedIndexChanged(sender, e);
                upPromotor.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAuditor_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var AuditorMasterList = ProcessManagement.GetAllInternalAuditAssignmentList();
                if (direction == SortDirection.Ascending)
                {
                    AuditorMasterList = AuditorMasterList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    AuditorMasterList = AuditorMasterList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdAuditor.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdAuditor.Columns.IndexOf(field);
                    }
                }
                grdAuditor.DataSource = AuditorMasterList;
                grdAuditor.DataBind();
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public Boolean hasIndex(int index, List<int> list)
        {
            if (index < list.Count)
                return true;
            return false;
        }
        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int CustomerBranchid = -1;
                int VerticalID = -1;

                if (e.CommandName.Equals("EDIT_Assignment"))
                {
                    ViewState["Mode"] = 1;

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    if (commandArgs.Length > 1)
                    {
                        if (Convert.ToString(commandArgs[0]) != null || Convert.ToString(commandArgs[0]) != "")
                        {
                            CustomerBranchid = Convert.ToInt32(commandArgs[0]);
                            ViewState["CustomerBranchid"] = null;
                            ViewState["CustomerBranchid"] = CustomerBranchid;

                            var LocationList = CustomerBranchManagementRisk.GetLocationHierarchy(CustomerBranchid);

                            int FwdPTR = 0;
                            int RevPTR = LocationList.Count;

                            if (LocationList.Count > 0)
                            {
                                while (hasIndex(FwdPTR, LocationList))
                                {
                                    if (FwdPTR == 0)
                                    {
                                        ddlLegalEntityPopPup.ClearSelection();
                                        ddlLegalEntityPopPup.Items.FindByValue(LocationList.ElementAt(RevPTR - 1).ToString()).Selected = true;
                                        ddlLegalEntityPopPup.SelectedValue = LocationList.ElementAt(RevPTR - 1).ToString();
                                        ddlLegalEntityPopPup.Enabled = false;
                                        BindSubEntityData(ddlSubEntity1PopPup, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                                    }
                                    else if (FwdPTR == 1)
                                    {
                                        ddlSubEntity1PopPup.ClearSelection();
                                        ddlSubEntity1PopPup.Items.FindByValue(LocationList.ElementAt(RevPTR - 1).ToString()).Selected = true;
                                        ddlSubEntity1PopPup.SelectedValue = LocationList.ElementAt(RevPTR - 1).ToString();
                                        ddlSubEntity1PopPup.Enabled = false;
                                        BindSubEntityData(ddlSubEntity2PopPup, Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue));
                                    }
                                    else if (FwdPTR == 2)
                                    {
                                        ddlSubEntity2PopPup.ClearSelection();
                                        ddlSubEntity2PopPup.Items.FindByValue(LocationList.ElementAt(RevPTR - 1).ToString()).Selected = true;
                                        ddlSubEntity2PopPup.SelectedValue = LocationList.ElementAt(RevPTR - 1).ToString();
                                        ddlSubEntity2PopPup.Enabled = false;
                                        BindSubEntityData(ddlSubEntity3PopPup, Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                                    }
                                    else if (FwdPTR == 3)
                                    {
                                        ddlSubEntity3PopPup.ClearSelection();
                                        ddlSubEntity3PopPup.Items.FindByValue(LocationList.ElementAt(RevPTR - 1).ToString()).Selected = true;
                                        ddlSubEntity3PopPup.SelectedValue = LocationList.ElementAt(RevPTR - 1).ToString();
                                        ddlSubEntity3PopPup.Enabled = false;
                                        BindSubEntityData(ddlFilterLocationPopPup, Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                                    }
                                    else if (FwdPTR == 4)
                                    {
                                        ddlFilterLocationPopPup.ClearSelection();
                                        ddlFilterLocationPopPup.Items.FindByValue(LocationList.ElementAt(RevPTR - 1).ToString()).Selected = true;
                                        ddlFilterLocationPopPup.SelectedValue = LocationList.ElementAt(RevPTR - 1).ToString();
                                        ddlFilterLocationPopPup.Enabled = false;
                                    }

                                    FwdPTR++;
                                    RevPTR--;
                                };
                            }
                        }

                        if (Convert.ToString(commandArgs[1]) != null || Convert.ToString(commandArgs[1]) != "")
                        {
                            VerticalID = Convert.ToInt32(commandArgs[1]);
                            ViewState["VerticalID"] = null;
                            ViewState["VerticalID"] = VerticalID;
                        }

                        Internal_AuditAssignment AuditAssignmentDetails = ProcessManagement.InternalAuditAssignmentGetByID(CustomerBranchid, VerticalID);

                        BindVerticalIDPopPup(Convert.ToInt32(CustomerBranchid));

                        if (AuditAssignmentDetails.VerticalID != null)
                        {
                            ddlVerticalIDPopPup.ClearSelection();
                            ddlVerticalIDPopPup.Items.FindByValue(Convert.ToString(AuditAssignmentDetails.VerticalID)).Selected = true;
                            ddlVerticalIDPopPup.Enabled = false;
                        }

                        string checkassignedto = Convert.ToString(AuditAssignmentDetails.AssignedTo);

                        ddlAssignedTo.ClearSelection();

                        if (checkassignedto == "I")
                        {
                            DivIsExternal.Visible = false;
                            //ddlAssignedTo.SelectedItem.Text = "Internal";
                            ddlAssignedTo.Items.FindByText("Internal").Selected = true;
                        }
                        else
                        {
                            DivIsExternal.Visible = true;
                            //ddlAssignedTo.SelectedItem.Text = "External";
                            ddlAssignedTo.Items.FindByText("External").Selected = true;

                            ddlAuditor.ClearSelection();
                            ddlAuditor.SelectedValue = Convert.ToString(AuditAssignmentDetails.ExternalAuditorId);
                        }

                        upPromotor.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdAuditor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindAuditorList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}                
                BindAuditorList();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                // GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        BindAuditorList();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }

        //        BindAuditorList();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //////////////////Sushant Code/////////
        protected void btnSaveApplyto_Click(object sender, EventArgs e)
        {
            try
            {
                bool SaveSuccess = false;
                int Branchid = -1;
                if (!string.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
                {
                    if (ddlLegalEntityPopPup.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
                {
                    if (ddlSubEntity1PopPup.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
                {
                    if (ddlSubEntity2PopPup.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
                {
                    if (ddlSubEntity3PopPup.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
                {
                    if (ddlFilterLocationPopPup.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue);
                    }
                }

                int Count = 0;
                for (int i = 0; i < ddlBranchList.Items.Count; i++)
                {
                    if (ddlBranchList.Items[i].Selected)
                        Count++;
                }

                if (Count > 0)
                {
                    var schedulingList = UserManagementRisk.GetPopUpAudAssignmentData(Branchid);

                    if (schedulingList.Count > 0)
                    {
                        foreach (var item in schedulingList)
                        {
                            //InternalauditAssignmentList.Clear();

                            for (int i = 0; i < ddlBranchList.Items.Count; i++)
                            {
                                if (ddlBranchList.Items[i].Selected == true)
                                {
                                    Internal_AuditAssignment mstauditorusermaster = new Internal_AuditAssignment()
                                    {
                                        CustomerBranchid = Convert.ToInt32(ddlBranchList.Items[i].Value),
                                        AssignedTo = item.AssignedTo,
                                        ExternalAuditorId = item.ExternalAuditorId,
                                        CreatedBy = item.CreatedBy,
                                        CreatedOn = item.CreatedOn,
                                        UpdatedBy = item.UpdatedBy,
                                        UpdatedOn = item.UpdatedOn,
                                        IsActive = item.IsActive,
                                        CustomerId = item.CustomerId
                                    };
                                    if ((int)ViewState["Mode"] == 1)
                                    {
                                        mstauditorusermaster.CustomerBranchid = Convert.ToInt32(Branchid);
                                        mstauditorusermaster.UpdatedBy = Portal.Common.AuthenticationHelper.UserID;
                                    }

                                    if ((int)ViewState["Mode"] == 0)
                                    {
                                        int BranchId = Convert.ToInt32(ddlBranchList.Items[i].Value);
                                        int customerID = -1;
                                        //customerID = UserManagementRisk.GetByID(Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;

                                        // code comment by sagar more on 30-01-2020
                                        // customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                                        if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
                                        {
                                            if (ddlCustomerPopup.SelectedValue!="-1")
                                            {
                                                customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);

                                            }
                                        }

                                        var verticalLists = UserManagementRisk.FillVerticalList(customerID, BranchId, item.VerticalID);
                                        if (verticalLists.Count > 0)
                                        {
                                            foreach (var items in verticalLists)
                                            {
                                                mstauditorusermaster.VerticalID = Convert.ToInt32(items.ID);
                                                if (!RiskCategoryManagement.Internal_AuditAssignmentExists(mstauditorusermaster))
                                                {
                                                    ProcessManagement.CreateInternalAuditAssignment(mstauditorusermaster);
                                                    SaveSuccess = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (SaveSuccess)
                            {
                                cvDuplicateEntryApplyPop.IsValid = false;
                                cvDuplicateEntryApplyPop.ErrorMessage = "Audit Assignment Save Successfully.";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller1()", true);
                                ddlBranchList.ClearSelection();
                            }
                            BindAuditorList();
                        }
                    }
                    else
                    {
                        cvDuplicateEntryApplyPop.IsValid = false;
                        cvDuplicateEntryApplyPop.ErrorMessage = "No Previous Audit Assignment Record found for current selection.";
                    }
                }
                else
                {
                    cvDuplicateEntryApplyPop.IsValid = false;
                    cvDuplicateEntryApplyPop.ErrorMessage = "Select Unit.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryApplyPop.IsValid = false;
                cvDuplicateEntryApplyPop.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindddlBranchApplyto()
        {

            int Branchid = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
            {
                if (ddlLegalEntityPopPup.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
            {
                if (ddlSubEntity1PopPup.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
            {
                if (ddlSubEntity2PopPup.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
            {
                if (ddlSubEntity3PopPup.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
            {
                if (ddlFilterLocationPopPup.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue);
                }
            }

            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;

            // code comment by sagar more on 30-01-2020
            //customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue!="-1")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            ddlBranchList.Items.Clear();
            ddlBranchList.DataSource = AuditKickOff_NewDetails.FillSubEntityData(Branchid, customerID);
            ddlBranchList.DataTextField = "Name";
            ddlBranchList.DataValueField = "ID";
            ddlBranchList.DataBind();
            updateApplyToPopUp.Update();
        }

        protected void btnapply_Click(object sender, EventArgs e)
        {
            BindddlBranchApplyto();
        }

        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        // added by sagar more on 28-01-2020
        private void BindCustomerList()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDL(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Customer", "-1"));
                ddlCustomer.SelectedIndex = 2;

                ddlCustomerPopup.DataTextField = "CustomerName";
                ddlCustomerPopup.DataValueField = "CustomerId";
                ddlCustomerPopup.DataSource = customerList;
                ddlCustomerPopup.DataBind();
                ddlCustomerPopup.Items.Insert(0, new ListItem("Customer", "-1"));
                ddlCustomerPopup.SelectedIndex = 2;
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();

                ddlCustomerPopup.DataSource = null;
                ddlCustomerPopup.DataBind();
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    BindLegalEntityData();
                    BindAuditor();
                    BindAuditorList();
                }
            }
        }

        protected void ddlCustomerPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomerPopup.SelectedValue))
            {
                if (ddlCustomerPopup.SelectedValue != "-1")
                {
                    BindLegalEntityData();
                    BindAuditor();
                    BindAuditorList();
                }
            }
        }
    }
}