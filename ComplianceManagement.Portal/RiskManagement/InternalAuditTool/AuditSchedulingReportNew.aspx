﻿<%@ Page Title="Audit Scheduling Report" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="AuditSchedulingReportNew.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditSchedulingReportNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


     <script type="text/javascript">
         $(document).ready(function () {
             setactivemenu('leftremindersmenu');
             fhead('Audit Scheduling Report');
         });
    </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
       
    </style>

    <style type="text/css">
        .gridHeaderAlignRight {
            text-align: right;
        }
   
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        .gvclass table th {
            text-align: left;
        }

        .gvclass1 table td {
            text-align: left;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">

                            <div class="col-md-12 colpadding0"> 
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">  
                                    <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                     DataPlaceHolder="Legal Entity" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlLegalEntity" ID="RequiredFieldValidator1"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />                                    
                                </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                     DataPlaceHolder="Sub Entity" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlSubEntity1" ID="rfvProcess"
                                        runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                      DataPlaceHolder="Sub Entity" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlSubEntity2" ID="rfvSubProcess"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />  
                                </div>
                             <div style="float:right;">                  
                        <asp:Button  ID="lbtnExportExcel" runat="server" Text="Export to Excel" CssClass="btn btn-search" OnClick="lbtnExportExcel_Click" style="margin-top: 5px;  width: 126px;"/>                   
                    </div>
                                </div>
                               <div class="clearfix"></div>                                                           
                        
                            <div class="col-md-12 colpadding0"> 
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                       DataPlaceHolder="Sub Entity" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                 </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                     OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged"    DataPlaceHolder="Sub Entity">
                                    </asp:DropDownListChosen>                                  
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                <asp:DropDownListChosen runat="server" ID="ddlVerticalID" DataPlaceHolder="Verticals" AutoPostBack="true" 
                                    OnSelectedIndexChanged="ddlVerticalID_SelectedIndexChanged"  class="form-control m-bot15 select_location" Width="80%" Height="32px">
                                </asp:DropDownListChosen>
                            </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                    <asp:DropDownListChosen runat="server" ID="ddlFinancialYear" class="form-control m-bot15" Width="80%" Height="32px"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged" DataPlaceHolder="Financial Year">
                        </asp:DropDownListChosen>
                             <asp:CompareValidator ID="CompareValidator15" ErrorMessage="Please Select Financial Year."
                            ControlToValidate="ddlFinancialYear" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                            </div>
                            <div class="clearfix"></div><div class="clearfix"></div>
                           
                       </section>
                    </div>
                      <div style="float:left; margin-left:10px; margin-top:15px;">
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="a1">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlAnnually" ScrollBars="Auto" style="height:auto" Width="100%"  Visible="false">
                              <div id="pnlAnnuallyPrevious" visible="false" runat="server"><h1>Previous</h1></div>
                                 <asp:GridView runat="server" ID="grdAnnually" AutoGenerateColumns="false" OnRowDataBound="grdAnnually_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            DataKeyNames="ID" OnPageIndexChanging="grdAnnually_PageIndexChanging">
                            <%--OnRowCreated="grdAnnually_RowCreated"--%>
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Annually">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Annualy1") %>' ToolTip='<%# Eval("Annualy1") %>'></asp:Label>
                            <asp:CheckBox ID="chkAnnualy1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Annually">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Annualy2") %>' ToolTip='<%# Eval("Annualy2") %>'></asp:Label>
                            <asp:CheckBox ID="chkAnnualy2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Annually">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Annualy3") %>' ToolTip='<%# Eval("Annualy3") %>'></asp:Label>
                            <asp:CheckBox ID="chkAnnualy3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel1">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlHalfYearly" ScrollBars="Auto" style="height:auto" Visible="false">
                                <div runat="server" id="pnlHalfYearlyPrevious" visible="false"><h1>Previous</h1></div>
                            <%--GridLines="Vertical"--%>
                            <asp:GridView runat="server" ID="grdHalfYearly" AutoGenerateColumns="false"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table"
                            Width="100%" AllowSorting="true" OnRowDataBound="grdHalfYearly_RowDataBound"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdHalfYearly_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr-Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly1") %>' ToolTip='<%# Eval("Halfyearly1") %>'></asp:Label>
                            <asp:CheckBox ID="chkHalfyearly1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Mar">
                            <ItemTemplate>
                            <asp:CheckBox ID="chkHalfyearly2" runat="server" />
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly2") %>' ToolTip='<%# Eval("Halfyearly2") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr-Sep">
                            <ItemTemplate>
                            <asp:CheckBox ID="chkHalfyearly3" runat="server" />
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly3") %>' ToolTip='<%# Eval("Halfyearly3") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Mar">
                            <ItemTemplate>
                            <asp:CheckBox ID="chkHalfyearly4" runat="server" />
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly4") %>' ToolTip='<%# Eval("Halfyearly4") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr-Sep">
                            <ItemTemplate>
                            <asp:CheckBox ID="chkHalfyearly5" runat="server" />
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly5") %>' ToolTip='<%# Eval("Halfyearly5") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Mar">
                            <ItemTemplate>
                            <asp:CheckBox ID="chkHalfyearly6" runat="server" />
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly6") %>' ToolTip='<%# Eval("Halfyearly6") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel3">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlQuarterly" ScrollBars="Auto" style="height:auto" Visible="false">
                               <div runat="server" id="pnlQuarterlyPrevious" visible="false"><h1>Previous</h1></div>
                                <asp:GridView runat="server" ID="grdQuarterly" AutoGenerateColumns="false" OnRowDataBound="grdQuarterly_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdQuarterly_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr-Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter1") %>' ToolTip='<%# Eval("Quarter1") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul-Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter2") %>' ToolTip='<%# Eval("Quarter2") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter3") %>' ToolTip='<%# Eval("Quarter3") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan-Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter4") %>' ToolTip='<%# Eval("Quarter4") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter4" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Apr-Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter5") %>' ToolTip='<%# Eval("Quarter5") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter5" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul-Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter6") %>' ToolTip='<%# Eval("Quarter6") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter6" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter7") %>' ToolTip='<%# Eval("Quarter7") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter7" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan-Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter8") %>' ToolTip='<%# Eval("Quarter8") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter8" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Apr-Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter9") %>' ToolTip='<%# Eval("Quarter9") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter9" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul-Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter10") %>' ToolTip='<%# Eval("Quarter10") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter10" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter11") %>' ToolTip='<%# Eval("Quarter11") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter11" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan-Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter12") %>' ToolTip='<%# Eval("Quarter12") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter12" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel4">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlMonthly" ScrollBars="Auto" style="height:auto;width:56%" Visible="false">
                               <div runat="server" id="pnlMonthlyPrevious" visible="false"><h1>Previous</h1></div>
                                <asp:GridView runat="server" ID="grdMonthly" AutoGenerateColumns="false" OnRowDataBound="grdMonthly_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdMonthly_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly1") %>' ToolTip='<%# Eval("Monthly1") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="May">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly2") %>' ToolTip='<%# Eval("Monthly2") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly3") %>' ToolTip='<%# Eval("Monthly3") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly4") %>' ToolTip='<%# Eval("Monthly4") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly4" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Aug">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly5") %>' ToolTip='<%# Eval("Monthly5") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly5" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly6") %>' ToolTip='<%# Eval("Monthly6") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly6" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly7") %>' ToolTip='<%# Eval("Monthly7") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly7" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nov">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly8") %>' ToolTip='<%# Eval("Monthly8") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly8" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly9") %>' ToolTip='<%# Eval("Monthly9") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly9" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly10") %>' ToolTip='<%# Eval("Monthly10") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly10" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Feb">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly11") %>' ToolTip='<%# Eval("Monthly11") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly11" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly12") %>' ToolTip='<%# Eval("Monthly12") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly12" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Apr">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly13") %>' ToolTip='<%# Eval("Monthly13") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly13" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="May">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly14") %>' ToolTip='<%# Eval("Monthly14") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly14" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly15") %>' ToolTip='<%# Eval("Monthly15") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly15" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly16") %>' ToolTip='<%# Eval("Monthly16") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly16" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Aug">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly17") %>' ToolTip='<%# Eval("Monthly17") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly17" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly18") %>' ToolTip='<%# Eval("Monthly18") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly18" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly19") %>' ToolTip='<%# Eval("Monthly19") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly19" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nov">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly20") %>' ToolTip='<%# Eval("Monthly20") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly20" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly21") %>' ToolTip='<%# Eval("Monthly21") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly21" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly22") %>' ToolTip='<%# Eval("Monthly22") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly22" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Feb">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly23") %>' ToolTip='<%# Eval("Monthly23") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly23" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly24") %>' ToolTip='<%# Eval("Monthly24") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly24" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly25") %>' ToolTip='<%# Eval("Monthly25") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly25" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="May">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly26") %>' ToolTip='<%# Eval("Monthly26") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly26" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly27") %>' ToolTip='<%# Eval("Monthly27") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly27" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly28") %>' ToolTip='<%# Eval("Monthly28") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly28" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Aug">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly29") %>' ToolTip='<%# Eval("Monthly29") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly29" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly30") %>' ToolTip='<%# Eval("Monthly30") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly30" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly31") %>' ToolTip='<%# Eval("Monthly31") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly31" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nov">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly32") %>' ToolTip='<%# Eval("Monthly32") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly32" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly33") %>' ToolTip='<%# Eval("Monthly33") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly33" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly34") %>' ToolTip='<%# Eval("Monthly34") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly34" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Feb">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly35") %>' ToolTip='<%# Eval("Monthly35") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly35" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly36") %>' ToolTip='<%# Eval("Monthly36") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly36" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel5">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlphase1" ScrollBars="Auto" style="height:auto" Visible="false">
                              <div runat="server" id="pnlphase1Previous" visible="false"><h1>Previous</h1></div>
                                 <asp:GridView runat="server" ID="grdphase1" AutoGenerateColumns="false" OnRowDataBound="grdphase1_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase1_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel6">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlphase2" ScrollBars="Auto" style="height:auto" Visible="false">
                              <div runat="server" id="pnlphase2Previous" visible="false"><h1>Previous</h1></div>
                                 <asp:GridView runat="server" ID="grdphase2" AutoGenerateColumns="false" OnRowDataBound="grdphase2_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase2_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase4" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase5" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase6" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel7">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlphase3" ScrollBars="Auto" style="height:auto" Visible="false">
                               <div runat="server" id="pnlphase3Previous" visible="false"><h1>Previous</h1></div>
                                <asp:GridView runat="server" ID="grdphase3" AutoGenerateColumns="false" OnRowDataBound="grdphase3_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase3_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase4" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase5" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase6" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase7") %>' ToolTip='<%# Eval("Phase7") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase7" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase8") %>' ToolTip='<%# Eval("Phase8") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase8" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase9") %>' ToolTip='<%# Eval("Phase9") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase9" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel8">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlphase4" ScrollBars="Auto" style="height:auto" Visible="false">
                              <div runat="server" id="pnlphase4Previous"  visible="false"><h1>Previous</h1></div>
                                 <asp:GridView runat="server" ID="grdphase4" AutoGenerateColumns="false" OnRowDataBound="grdphase4_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase4_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase4">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase4" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase5" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase6" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase7") %>' ToolTip='<%# Eval("Phase7") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase7" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase4">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase8") %>' ToolTip='<%# Eval("Phase8") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase8" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase9") %>' ToolTip='<%# Eval("Phase9") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase9" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase10") %>' ToolTip='<%# Eval("Phase10") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase10" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase11") %>' ToolTip='<%# Eval("Phase11") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase11" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase4">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase12") %>' ToolTip='<%# Eval("Phase12") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase12" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel9">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlphase5" ScrollBars="Auto" style="height:auto">
                               <div runat="server" id="pnlphase5Previous" visible="false"><h1>Previous</h1></div>
                                <asp:GridView runat="server" ID="grdphase5" AutoGenerateColumns="false" OnRowDataBound="grdphase5_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase5_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase4">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase4" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase5">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase5" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase6" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase7") %>' ToolTip='<%# Eval("Phase7") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase7" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase8") %>' ToolTip='<%# Eval("Phase8") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase8" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase4">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase9") %>' ToolTip='<%# Eval("Phase9") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase9" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase5">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase10") %>' ToolTip='<%# Eval("Phase10") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase10" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase11") %>' ToolTip='<%# Eval("Phase11") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase11" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase12") %>' ToolTip='<%# Eval("Phase12") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase12" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase13") %>' ToolTip='<%# Eval("Phase13") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase13" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase4">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase14") %>' ToolTip='<%# Eval("Phase14") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase14" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase5">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase15") %>' ToolTip='<%# Eval("Phase15") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase15" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>

                            </div>    
                      <div class="clearfix"></div><div class="clearfix"></div>
                      <div style="float:left; margin-left:10px; margin-top:15px;">
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="a12">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlAnnually2" ScrollBars="Auto" style="height:auto" Visible="false">
                              <div runat="server" id="pnlAnnually2Current" visible="false"><h1>Current</h1></div>
                                 <asp:GridView runat="server" ID="grdAnnually2" AutoGenerateColumns="false" OnRowDataBound="grdAnnually2_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            DataKeyNames="ID" OnPageIndexChanging="grdAnnually2_PageIndexChanging">
                            <%--OnRowCreated="grdAnnually_RowCreated"--%>
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false" >
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Annually">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Annualy1") %>' ToolTip='<%# Eval("Annualy1") %>'></asp:Label>
                            <asp:CheckBox ID="chkAnnualy1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Annually">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Annualy2") %>' ToolTip='<%# Eval("Annualy2") %>'></asp:Label>
                            <asp:CheckBox ID="chkAnnualy2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Annually">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Annualy3") %>' ToolTip='<%# Eval("Annualy3") %>'></asp:Label>
                            <asp:CheckBox ID="chkAnnualy3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel2">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlHalfYearly2" ScrollBars="Auto" style="height:auto" Visible="false">
                            <%--GridLines="Vertical"--%>
                                <div runat="server" id="pnlHalfYearly2Current" visible="false"><h1>Current</h1></div>
                            <asp:GridView runat="server" ID="grdHalfYearly2" AutoGenerateColumns="false"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table"
                            Width="100%" AllowSorting="true" OnRowDataBound="grdHalfYearly2_RowDataBound"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdHalfYearly2_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr-Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly1") %>' ToolTip='<%# Eval("Halfyearly1") %>'></asp:Label>
                            <asp:CheckBox ID="chkHalfyearly1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Mar">
                            <ItemTemplate>
                            <asp:CheckBox ID="chkHalfyearly2" runat="server" />
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly2") %>' ToolTip='<%# Eval("Halfyearly2") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr-Sep">
                            <ItemTemplate>
                            <asp:CheckBox ID="chkHalfyearly3" runat="server" />
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly3") %>' ToolTip='<%# Eval("Halfyearly3") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Mar">
                            <ItemTemplate>
                            <asp:CheckBox ID="chkHalfyearly4" runat="server" />
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly4") %>' ToolTip='<%# Eval("Halfyearly4") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr-Sep">
                            <ItemTemplate>
                            <asp:CheckBox ID="chkHalfyearly5" runat="server" />
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly5") %>' ToolTip='<%# Eval("Halfyearly5") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Mar">
                            <ItemTemplate>
                            <asp:CheckBox ID="chkHalfyearly6" runat="server" />
                            <asp:Label runat="server" Text='<%# Eval("Halfyearly6") %>' ToolTip='<%# Eval("Halfyearly6") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel10">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlQuarterly2" ScrollBars="Auto" style="height:auto" Visible="false">
                               <div runat="server" id="pnlQuarterly2Current" visible="false"><h1>Current</h1></div>
                                <asp:GridView runat="server" ID="grdQuarterly2" AutoGenerateColumns="false" OnRowDataBound="grdQuarterly2_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdQuarterly2_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr-Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter1") %>' ToolTip='<%# Eval("Quarter1") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul-Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter2") %>' ToolTip='<%# Eval("Quarter2") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter3") %>' ToolTip='<%# Eval("Quarter3") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan-Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter4") %>' ToolTip='<%# Eval("Quarter4") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter4" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Apr-Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter5") %>' ToolTip='<%# Eval("Quarter5") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter5" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul-Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter6") %>' ToolTip='<%# Eval("Quarter6") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter6" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter7") %>' ToolTip='<%# Eval("Quarter7") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter7" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan-Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter8") %>' ToolTip='<%# Eval("Quarter8") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter8" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Apr-Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter9") %>' ToolTip='<%# Eval("Quarter9") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter9" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul-Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter10") %>' ToolTip='<%# Eval("Quarter10") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter10" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct-Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter11") %>' ToolTip='<%# Eval("Quarter11") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter11" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan-Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Quarter12") %>' ToolTip='<%# Eval("Quarter12") %>'></asp:Label>
                            <asp:CheckBox ID="chkQuarter12" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="PlaceHolder2" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel11">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlMonthly2" ScrollBars="Auto" style="height:auto;width:56%" Visible="false">
                              <div runat="server" id="pnlMonthly2Current" visible="false"><h1>Current</h1></div>
                                 <asp:GridView runat="server" ID="grdMonthly2" AutoGenerateColumns="false" OnRowDataBound="grdMonthly2_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdMonthly2_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly1") %>' ToolTip='<%# Eval("Monthly1") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="May">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly2") %>' ToolTip='<%# Eval("Monthly2") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly3") %>' ToolTip='<%# Eval("Monthly3") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly4") %>' ToolTip='<%# Eval("Monthly4") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly4" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Aug">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly5") %>' ToolTip='<%# Eval("Monthly5") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly5" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly6") %>' ToolTip='<%# Eval("Monthly6") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly6" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly7") %>' ToolTip='<%# Eval("Monthly7") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly7" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nov">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly8") %>' ToolTip='<%# Eval("Monthly8") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly8" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly9") %>' ToolTip='<%# Eval("Monthly9") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly9" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly10") %>' ToolTip='<%# Eval("Monthly10") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly10" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Feb">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly11") %>' ToolTip='<%# Eval("Monthly11") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly11" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly12") %>' ToolTip='<%# Eval("Monthly12") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly12" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Apr">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly13") %>' ToolTip='<%# Eval("Monthly13") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly13" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="May">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly14") %>' ToolTip='<%# Eval("Monthly14") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly14" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly15") %>' ToolTip='<%# Eval("Monthly15") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly15" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly16") %>' ToolTip='<%# Eval("Monthly16") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly16" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Aug">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly17") %>' ToolTip='<%# Eval("Monthly17") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly17" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly18") %>' ToolTip='<%# Eval("Monthly18") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly18" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly19") %>' ToolTip='<%# Eval("Monthly19") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly19" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nov">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly20") %>' ToolTip='<%# Eval("Monthly20") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly20" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly21") %>' ToolTip='<%# Eval("Monthly21") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly21" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly22") %>' ToolTip='<%# Eval("Monthly22") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly22" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Feb">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly23") %>' ToolTip='<%# Eval("Monthly23") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly23" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly24") %>' ToolTip='<%# Eval("Monthly24") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly24" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apr">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly25") %>' ToolTip='<%# Eval("Monthly25") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly25" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="May">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly26") %>' ToolTip='<%# Eval("Monthly26") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly26" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jun">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly27") %>' ToolTip='<%# Eval("Monthly27") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly27" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jul">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly28") %>' ToolTip='<%# Eval("Monthly28") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly28" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Aug">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly29") %>' ToolTip='<%# Eval("Monthly29") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly29" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sep">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly30") %>' ToolTip='<%# Eval("Monthly30") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly30" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Oct">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly31") %>' ToolTip='<%# Eval("Monthly31") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly31" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nov">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly32") %>' ToolTip='<%# Eval("Monthly32") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly32" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Dec">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly33") %>' ToolTip='<%# Eval("Monthly33") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly33" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Jan">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly34") %>' ToolTip='<%# Eval("Monthly34") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly34" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Feb">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly35") %>' ToolTip='<%# Eval("Monthly35") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly35" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mar">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Monthly36") %>' ToolTip='<%# Eval("Monthly36") %>'></asp:Label>
                            <asp:CheckBox ID="chkMonthly36" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="PlaceHolder3" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel12">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlphase12" ScrollBars="Auto" style="height:auto" Visible="false">
                              <div runat="server" id="pnlphase12Current" visible="false"><h1>Current</h1></div> 
                                <asp:GridView runat="server" ID="grdphase12" AutoGenerateColumns="false" OnRowDataBound="grdphase12_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase12_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="PlaceHolder4" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel13">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlphase22" ScrollBars="Auto" style="height:auto" Visible="false">
                              <div runat="server" id="pnlphase22Current" visible="false"><h1>Current</h1></div>
                                 <asp:GridView runat="server" ID="grdphase22" AutoGenerateColumns="false" OnRowDataBound="grdphase22_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase22_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox1" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox2" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox3" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase4" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase5" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase6" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="PlaceHolder5" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel14">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlphase32" ScrollBars="Auto" style="height:auto" Visible="false">
                             <div runat="server" id="pnlphase32Current" visible="false"><h1>Current</h1></div>
                                  <asp:GridView runat="server" ID="grdphase32" AutoGenerateColumns="false" OnRowDataBound="grdphase32_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase32_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox4" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox5" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox6" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox7" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox8" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox9" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase7") %>' ToolTip='<%# Eval("Phase7") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase7" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase8") %>' ToolTip='<%# Eval("Phase8") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase8" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase9") %>' ToolTip='<%# Eval("Phase9") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase9" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="PlaceHolder6" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel15">
                            <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlphase42" ScrollBars="Auto" style="height:auto" Visible="false">
                             <div runat="server" id="pnlphase42Current" visible="false"><h1>Current</h1></div>
                                  <asp:GridView runat="server" ID="grdphase42" AutoGenerateColumns="false" OnRowDataBound="grdphase42_RowDataBound"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                            ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase42_PageIndexChanging">
                            <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Process">
                            <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                            <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox10" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox11" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox12" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase4">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox13" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox14" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox15" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase7") %>' ToolTip='<%# Eval("Phase7") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox16" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase4">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase8") %>' ToolTip='<%# Eval("Phase8") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox17" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase1">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase9") %>' ToolTip='<%# Eval("Phase9") %>'></asp:Label>
                            <asp:CheckBox ID="CheckBox18" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase2">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase10") %>' ToolTip='<%# Eval("Phase10") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase10" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phase3">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase11") %>' ToolTip='<%# Eval("Phase11") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase11" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Phase4">
                            <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Phase12") %>' ToolTip='<%# Eval("Phase12") %>'></asp:Label>
                            <asp:CheckBox ID="chkPhase12" runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                            <table style="display: none">
                            <tr>
                            <td>
                            <asp:PlaceHolder ID="PlaceHolder7" runat="server"></asp:PlaceHolder>
                            </td>
                            </tr>
                            </table>
                            </PagerTemplate>
                            </asp:GridView>
                            </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel16">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlphase52" ScrollBars="Auto" style="height:auto">
                               <div runat="server" visible="false" id="pnlphase52Current"><h1>Current</h1></div>
                                    <asp:GridView runat="server" ID="grdphase52" AutoGenerateColumns="false" OnRowDataBound="grdphase52_RowDataBound"
                                    GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" AllowSorting="true"
                                    ShowFooter="false" DataKeyNames="ID" OnPageIndexChanging="grdphase52_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProcessID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Process">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                    <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phase1">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase1") %>' ToolTip='<%# Eval("Phase1") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox19" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phase2">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase2") %>' ToolTip='<%# Eval("Phase2") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox20" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Phase3">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase3") %>' ToolTip='<%# Eval("Phase3") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox21" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phase4">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase4") %>' ToolTip='<%# Eval("Phase4") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox22" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Phase5">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase5") %>' ToolTip='<%# Eval("Phase5") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox23" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Phase1">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase6") %>' ToolTip='<%# Eval("Phase6") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox24" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Phase2">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase7") %>' ToolTip='<%# Eval("Phase7") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox25" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phase3">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase8") %>' ToolTip='<%# Eval("Phase8") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox26" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Phase4">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase9") %>' ToolTip='<%# Eval("Phase9") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox27" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phase5">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase10") %>' ToolTip='<%# Eval("Phase10") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox28" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phase1">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase11") %>' ToolTip='<%# Eval("Phase11") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox29" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phase2">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase12") %>' ToolTip='<%# Eval("Phase12") %>'></asp:Label>
                                                <asp:CheckBox ID="CheckBox30" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phase3">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase13") %>' ToolTip='<%# Eval("Phase13") %>'></asp:Label>
                                                <asp:CheckBox ID="chkPhase13" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phase4">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase14") %>' ToolTip='<%# Eval("Phase14") %>'></asp:Label>
                                                <asp:CheckBox ID="chkPhase14" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phase5">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("Phase15") %>' ToolTip='<%# Eval("Phase15") %>'></asp:Label>
                                                <asp:CheckBox ID="chkPhase15" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="PlaceHolder8" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                </asp:GridView>
                            </asp:Panel>
        </ContentTemplate>
                    </asp:UpdatePanel>
                     </div>    
                </div>
            </div> <br runat="server"  id="newlinebreak"/> 
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
