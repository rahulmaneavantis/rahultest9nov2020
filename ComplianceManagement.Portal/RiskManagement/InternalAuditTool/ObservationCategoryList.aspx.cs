﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class ObservationCategoryList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerListFilter();
                BindCustomerList();
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                ViewState["ObservationID"] = Session["ObservationID"];
                //int i= 10;
                string fetchCatagoryName = ObservationSubcategory.fetchObservationcatgory(Convert.ToInt32(ViewState["ObservationID"])).ToString();
                catname.Text = fetchCatagoryName;
                BindObservation();
                BindSubObservationList();
                if (ddlCustomerFilter.SelectedValue != "-1")
                {
                    Session["CustomerId"] = ddlCustomerFilter.SelectedValue;
                    ddlCustomer.SelectedValue = Convert.ToString(Session["CustomerId"]);
                    ddlCustomer.Enabled = false;
                }
            }
        }
        protected void lnkBackToProcess_Click(object sender, EventArgs e)
        {
            try
            {                
                Response.Redirect("~/RiskManagement/InternalAuditTool/ObservationCategory.aspx");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                // customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue!="-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                        return;
                    }
                }
                Mst_ObservationSubCategory objSublist = new Mst_ObservationSubCategory()
                {
                    ObscatID = Convert.ToInt32(ddlObservation.SelectedValue),
                    Name= tbxName.Text,
                    ClientID = customerID,
                    IsActive = false
                };               

                if ((int)ViewState["Mode"] == 0)
                {
                    if (ObservationSubcategory.ObservationSubCategoryExists(customerID, objSublist.ObscatID, objSublist.Name))
                    {
                        cvDuplicateName.IsValid = false;
                        cvDuplicateName.ErrorMessage = "Observation Sub-Category Name Already Exists.";
                        return;
                    }
                    else
                    {
                        ObservationSubcategory.CreateSubobsList(objSublist);
                        cvDuplicateName.IsValid = false;
                        cvDuplicateName.ErrorMessage = "Observation Sub-Category Saved Successfully.";
                    }
                }

                else if ((int)ViewState["Mode"] == 1)
                {
                    if (ObservationSubcategory.ObservationSubCategoryExists(customerID, objSublist.ObscatID, objSublist.Name))
                    {
                        cvDuplicateName.IsValid = false;
                        cvDuplicateName.ErrorMessage = "Observation Sub-Category Name Already Exists.";
                        return;
                    }
                    else
                    {
                        objSublist.ID = Convert.ToInt32(ViewState["ID"]);
                        ObservationSubcategory.UpdateObjSubList(objSublist);
                        cvDuplicateName.IsValid = false;
                        cvDuplicateName.ErrorMessage = "Observation Sub-Category Updated Successfully.";
                    }
                }                
                //ddlObservation.Items.Insert(0, new ListItem("Select Observation", "0"));
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);
                BindSubObservationList();
                //bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdObservationList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubObservationList()
        {
            try
            {
                var SubObjList = ObservationSubcategory.GetAllSubObservationList(Convert.ToInt32(ViewState["ObservationID"]));
                grdObservationList.DataSource = SubObjList;
                Session["TotalRows"] = SubObjList.Count;
                grdObservationList.DataBind();
                upProcessList.Update();
                //  GetPageDisplaySummary();     
                bindPageNumber();           
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindObservation()
        {
            try
            {
                int customerID = -1;
                //customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
                {
                    if (ddlCustomerFilter.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
                    }
                }
                ddlObservation.DataTextField = "Name";
                ddlObservation.DataValueField = "ID";
                ddlObservation.DataSource = ObservationSubcategory.FillObservationCategory(customerID);
                ddlObservation.DataBind();
                ddlObservation.Items.Insert(0, new ListItem("Select Observation Category", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdObservationList_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
       
        protected void grdObservationList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_Observation"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["ID"] = ID;
                    ddlCustomer.Enabled = false;
                    string value = ddlCustomerFilter.SelectedValue;
                    if (!string.IsNullOrEmpty(value))
                    {
                        ddlCustomer.SelectedValue = value;
                    }
                    Mst_ObservationSubCategory objSublist = ObservationSubcategory.ObservationListGetById(ID);

                    ddlObservation.SelectedValue = Convert.ToString(objSublist.ObscatID);
                    tbxName.Text = objSublist.Name;
                    upPromotor.Update();
                }
                else if (e.CommandName.Equals("DELETE_Observation"))
                {
                    ObservationSubcategory.DeleteObservationList(ID);
                    BindSubObservationList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdObservationList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdObservationList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdObservationList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //else
                //{

                //}
                //Reload the Grid
                BindSubObservationList();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdObservationList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdObservationList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdObservationList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid                              
        //        BindSubObservationList();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdObservationList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdObservationList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid                
        //        BindSubObservationList();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        protected void btnSubcategory_Click(object sender, EventArgs e)
        {
            try
            {
                //ViewState["ObservationID"] = Session["ObservationID"];
                int OservationCatId=Convert.ToInt32(ViewState["ObservationID"]);
                BindCustomerList();
                if (ddlCustomerFilter.SelectedValue != "-1")
                {
                    Session["CustomerId"] = ddlCustomerFilter.SelectedValue;
                    ddlCustomer.SelectedValue = Convert.ToString(Session["CustomerId"]);
                    ddlCustomer.Enabled = false;
                }
                ViewState["Mode"] = 0;
                //ddlObservation.Items.Insert(0, new ListItem("Select Observation Category", "0"));
                ddlObservation.Enabled = false;
                ddlObservation.SelectedValue = OservationCatId.ToString();
                tbxName.Text = string.Empty;
                upPromotor.Update();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";               
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                DropDownListPageNo.DataBind();
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdObservationList.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdObservationList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;            
            BindSubObservationList();
        }

        protected void ddlCustomerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomerFilter.SelectedValue))
            {
                if (ddlCustomerFilter.SelectedValue != "-1")
                {
                    Session["CustomerId"] = ddlCustomerFilter.SelectedValue;
                    BindObservation();
                    BindSubObservationList();
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                }
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue == "-1")
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Customer.";
                }
            }
        }

        private void BindCustomerList()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDLForMaster(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
                //ddlCustomer.SelectedIndex = 2;
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();
            }
        }

        private void BindCustomerListFilter()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDLForMaster(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomerFilter.DataTextField = "CustomerName";
                ddlCustomerFilter.DataValueField = "CustomerId";
                ddlCustomerFilter.DataSource = customerList;
                ddlCustomerFilter.DataBind();
                ddlCustomerFilter.Items.Insert(0, new ListItem("Select Customer", "-1"));
                ddlCustomerFilter.SelectedIndex = 2;
            }
            else
            {
                ddlCustomerFilter.DataSource = null;
                ddlCustomerFilter.DataBind();
            }
        }
    }
}