﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;
using Spire.Presentation.Drawing.Animation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class FrmInternalAuditReportInWord : System.Web.UI.Page
    {
        //  AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(AuthenticationHelper.UserID);
        public static List<long> Branchlist = new List<long>();
        public static bool ApplyFilter;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        public static string linkclick;
        protected static string AuditHeadOrManagerReport;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                BindProcess("P");
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData();
              //  BindData();
                //BindData();


                ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                //ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
                ddlSchedulingType.Items.Insert(0, new ListItem("Select Month", "-1"));
                //ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }
        private void BindProcess(string flag)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                if (flag == "P")
                {
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcess("P", customerID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }
                else
                {
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcess("N", customerID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            //ddlSchedulingType.DataTextField = "Name";
            //ddlSchedulingType.DataValueField = "ID";
            //ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            //ddlSchedulingType.DataBind();
            //ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }

        //public void BindAuditSchedule(string flag, int count)
        //{
        //    try
        //    {
        //        if (flag == "A")
        //        {
        //            ddlPeriod.DataSource = null;
        //            ddlPeriod.DataBind();
        //            ddlPeriod.Items.Clear();
        //            ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
        //            ddlPeriod.Items.Insert(1, "Annually");
        //        }
        //        else if (flag == "H")
        //        {
        //            ddlPeriod.DataSource = null;
        //            ddlPeriod.DataBind();
        //            ddlPeriod.Items.Clear();
        //            ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
        //            ddlPeriod.Items.Insert(1, "Apr-Sep");
        //            ddlPeriod.Items.Insert(2, "Oct-Mar");
        //        }
        //        else if (flag == "Q")
        //        {
        //            ddlPeriod.DataSource = null;
        //            ddlPeriod.DataBind();
        //            ddlPeriod.Items.Clear();
        //            ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
        //            ddlPeriod.Items.Insert(1, "Apr-Jun");
        //            ddlPeriod.Items.Insert(2, "Jul-Sep");
        //            ddlPeriod.Items.Insert(3, "Oct-Dec");
        //            ddlPeriod.Items.Insert(4, "Jan-Mar");

        //        }
        //        else if (flag == "M")
        //        {
        //            ddlPeriod.Items.Clear();
        //            ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
        //            ddlPeriod.Items.Insert(1, "April");
        //            ddlPeriod.Items.Insert(2, "May");
        //            ddlPeriod.Items.Insert(3, "June");
        //            ddlPeriod.Items.Insert(4, "July");
        //            ddlPeriod.Items.Insert(5, "August");
        //            ddlPeriod.Items.Insert(6, "September");
        //            ddlPeriod.Items.Insert(7, "Octomber");
        //            ddlPeriod.Items.Insert(8, "November");
        //            ddlPeriod.Items.Insert(9, "December");
        //            ddlPeriod.Items.Insert(10, "January");
        //            ddlPeriod.Items.Insert(11, "February");
        //            ddlPeriod.Items.Insert(12, "March");
        //        }
        //        else
        //        {
        //            if (count == 1)
        //            {
        //                ddlPeriod.Items.Clear();
        //                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
        //                ddlPeriod.Items.Insert(1, "Phase1");
        //            }
        //            else if (count == 2)
        //            {
        //                ddlPeriod.Items.Clear();
        //                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
        //                ddlPeriod.Items.Insert(1, "Phase1");
        //                ddlPeriod.Items.Insert(2, "Phase2");
        //            }
        //            else if (count == 3)
        //            {
        //                ddlPeriod.Items.Clear();
        //                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
        //                ddlPeriod.Items.Insert(1, "Phase1");
        //                ddlPeriod.Items.Insert(2, "Phase2");
        //                ddlPeriod.Items.Insert(3, "Phase3");
        //            }
        //            else if (count == 4)
        //            {
        //                ddlPeriod.Items.Clear();
        //                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
        //                ddlPeriod.Items.Insert(1, "Phase1");
        //                ddlPeriod.Items.Insert(2, "Phase2");
        //                ddlPeriod.Items.Insert(3, "Phase3");
        //                ddlPeriod.Items.Insert(4, "Phase4");
        //            }
        //            else
        //            {
        //                ddlPeriod.Items.Clear();
        //                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
        //                ddlPeriod.Items.Insert(1, "Phase1");
        //                ddlPeriod.Items.Insert(2, "Phase2");
        //                ddlPeriod.Items.Insert(3, "Phase3");
        //                ddlPeriod.Items.Insert(4, "Phase4");
        //                ddlPeriod.Items.Insert(5, "Phase5");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindData()
        {
            try
            {
                ApplyFilter = false;
                int userID = -1;
                int RoleID = -1;
                if (PerformerFlag)
                    RoleID = 3;
                if (ReviewerFlag)
                    RoleID = 4;
                int customerID = -1;
                customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int CustBranchID = -1;
                int VerticalID = -1;
                int ProcessID = -1;
                String FinancialYear = String.Empty;
                String Period = String.Empty;


                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }

                //if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                //{
                //    if (ddlPeriod.SelectedValue != "-1")
                //    {
                //        Period = ddlPeriod.SelectedItem.Text;
                //    }
                //}

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        ProcessID = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlVertical.SelectedValue))
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }

                if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                {
                    if (RoleID == -1)
                    {
                        RoleID = 4;
                    }
                    Branchlist.Clear();
                    List<long> branchids = AssignEntityManagementRisk.CheckAuditManagerLocation(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                    Branchlist = branchids.ToList();
                }
                else
                {

                    userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(customerID, CustBranchID);
                    var Branchlistloop = Branchlist.ToList();
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProcess.SelectedValue != "-1")
            {
                // BindData();
            }
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlVertical.SelectedValue != "-1")
            {
                // BindData();
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            // BindSchedulingType();
            // BindData();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            //  BindSchedulingType();
            // BindData();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            //BindSchedulingType();
            //BindData();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            // BindSchedulingType();
            // BindData();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess("P");
            BindVertical();
            //BindSchedulingType();
            // BindData();
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFinancialYear.SelectedValue != "-1")
            {
                ddlSchedulingType.DataTextField = "Name";
                ddlSchedulingType.DataValueField = "ID";               
                ddlSchedulingType.Items.Insert(1, "Jan");
                ddlSchedulingType.Items.Insert(2, "Feb");
                ddlSchedulingType.Items.Insert(3, "March");
                ddlSchedulingType.Items.Insert(4, "Apr");
                ddlSchedulingType.Items.Insert(5, "May");
                ddlSchedulingType.Items.Insert(6, "June");
                ddlSchedulingType.Items.Insert(7, "Jully");
                ddlSchedulingType.Items.Insert(8, "Aug");
                ddlSchedulingType.Items.Insert(9, "Sept");
                ddlSchedulingType.Items.Insert(10, "Oct");
                ddlSchedulingType.Items.Insert(11, "Nov");
                ddlSchedulingType.Items.Insert(12, "Dec");
                //ddlSchedulingType.Items.Insert(0, new ListItem("Month", "-1"));
            }
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }



     

        protected void btnExportDoc_Click(object sender, EventArgs e)
        {
            try
            {
                ApplyFilter = false;
                int userID = -1;
                int RoleID = -1;
                if (PerformerFlag)
                    RoleID = 3;
                if (ReviewerFlag)
                    RoleID = 4;
                int customerID = -1;
                customerID =Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int CustBranchID = -1;
                int VerticalID = -1;
                int ProcessID = -1;
                String FinancialYear = String.Empty;
                String Period = String.Empty;
                //String months = String.Empty;
                int months=-1;
                int NextMonth = 0;
                String FirstYear = String.Empty;
                String SecondYear = String.Empty;
                String MonthYear = String.Empty;
                String MonthName = String.Empty;
                string nextyear = string.Empty;
                string nextMonth = string.Empty;    

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                        string[] words = FinancialYear.Split('-');
                        FirstYear = words[0];
                        SecondYear = words[1];
                    }
                }

                if (!String.IsNullOrEmpty(ddlSchedulingType.SelectedValue))
                {
                    if (ddlSchedulingType.SelectedValue != "-1")
                    {
                        months = Convert.ToInt32(ddlSchedulingType.SelectedIndex);
                        NextMonth = months + 1;
                         MonthName = ddlSchedulingType.SelectedItem.Text;
                        if (MonthName == "Jan" || MonthName == "Feb" || MonthName == "March")
                        {
                            MonthYear = MonthName+" "+ SecondYear;
                        }
                        else
                        {
                            MonthYear = MonthName+" "+ FirstYear;
                        }
                    }
                }

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        ProcessID = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlVertical.SelectedValue))
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }

                if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                {
                    if (RoleID == -1)
                    {
                        RoleID = 4;
                    }
                    Branchlist.Clear();
                    List<long> branchids = AssignEntityManagementRisk.CheckAuditManagerLocation(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                    Branchlist = branchids.ToList();
                }
                else
                {

                    userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(customerID, CustBranchID);
                    var Branchlistloop = Branchlist.ToList();
                }
                //for fetch next month
                if (NextMonth>0)
                {
                    if (NextMonth==13)
                    {
                        NextMonth = 1;
                    }
                     nextMonth = ddlSchedulingType.Items[NextMonth].Text;


                    if (nextMonth == "Jan" || nextMonth == "Feb" || nextMonth == "March")
                    {
                        nextyear = SecondYear;                       
                    }
                    else
                    {
                        nextyear = FirstYear;
                    }


                }

                List<ObservationStatusUserWiseNew_DisplayView_New> itemlist = new List<ObservationStatusUserWiseNew_DisplayView_New>();
                itemlist = InternalControlManagementDashboardRisk.getDatatest(RoleID, customerID, userID, VerticalID, ProcessID, FinancialYear, months, Branchlist);
                

                System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();
                System.Text.StringBuilder stringbuilderTableFirst = new System.Text.StringBuilder();
                System.Text.StringBuilder stringbuilderTableSecond = new System.Text.StringBuilder();
                System.Text.StringBuilder stringbuilderTOPLastTable = new System.Text.StringBuilder();

                stringbuilderTableFirst.Append("</br></br>"
                + "<p align='center' font - size='20.0pt' style='font-family:Arial;'><u><b>" + "Internal Audit Reports Issued during" + " " + MonthYear + " " +
                   "</b></u></p></br>" +
                    "<p>" +
                    "During the month of" + " " + MonthYear + " " + ", the following Internal Audits were conducted and reports released post agreement with the Hub & State Managers or " +
                    "Functional Heads at Corporate Office on the Audit Issues, Rectification Action Plan and Timeline for closure of Audit Issues."
                   + "</p></br>");
                stringbuilderTableFirst.Append("<table>" + "<tr bgcolor='#A3CEDC'><td>"
                   + FinancialYear + "Report No"
                   + "</td><td>" + "State/Function" + "</td><td>" + "Audit Report Of Hub"
                   + "</td><td>"
                   + "No. of locations"
                   + "</td><td>" + "Business Volumes" + " </td><td>" + "Business Value Rs. Lacs" + "</td>"
                    + "<td>" + "Rating Score" + "</td><td>" + "Audit Rating" +
                    "</td></tr>");
                for (int index = 0; index < itemlist.Count; index++)
                {
                    ObservationStatusUserWiseNew_DisplayView_New ObjItem = itemlist[index];
                    string fYear = ObjItem.FinancialYear;
                    string ReportNo = Convert.ToString(ObjItem.ScheduledOnID);
                    string state = ObjItem.State;
                    string Hub = ObjItem.HUB;
                    string LocationCount = ObjItem.Location;
                    string BusinessVolumes = string.Empty;
                    string BusinessValues = string.Empty;
                    string RatingScore = string.Empty;
                    string AuditRating = string.Empty;
                    stringbuilderTableFirst.Append("<tr><td>"
                   + ReportNo + "</td><td>" + state + "</td><td>" + Hub + "</td><td>"
                   + LocationCount + "</td><td>" + "" + " </td><td>" + "" + "</td>"
                   + "<td>" + "" + "</td><td>" + "" + "</td></tr>");
                }
                stringbuilderTableFirst.Append("<tr><td>"
                   + "" + "</td><td colspan='3' bgcolor='#A3CEDC'align='right'>" + "% Completed upto"+" "+ MonthYear + "</td><td bgcolor='#A3CEDC'>" + "" + " </td><td bgcolor='#A3CEDC'>" + "" + "</td>"
                   + "<td bgcolor='#A3CEDC'>" + "" + "</td><td>" + "" + "</td></tr>");

                stringbuilderTableFirst.Append("</table>");
                stringbuilderTOP.Append(stringbuilderTableFirst);
                //First Table End

                stringbuilderTableSecond.Append("<p>" + "# - Audit points would be summarized and added to "+ nextMonth+" "+nextyear + " internal audit update." + "</br></br>");
                //    + "</p><br><p>" + "The summary of the Audit Ratings for the reports issued during" + " " + FinancialYear + " " + "is as under:" +
                //    "</p><table><tr bgcolor='#A3CEDC'><td>"
                //    + "Sr No." + "</td><td>"
                //    + "Audit Ratings" + "</td><td>" + "Number of Hubs" +
                //    "</td></tr>");
                //int S_No_Count = 0;
                //for (int index = 0; index < itemlist.Count; index++)
                //{
                //    ObservationStatusUserWiseNew_DisplayView_New ObjItem = itemlist[index];
                //    S_No_Count = S_No_Count + 1;
                //    stringbuilderTableSecond.Append("<tr><td>" + S_No_Count + "</td><td>" + "" + "</td><td>" + "" + "</td></tr>");
                //}
                //stringbuilderTableSecond.Append("</table>" + "</br>" + " <p>" + "Summary of December16 Pending observations:" + "</p>");

                //stringbuilderTOP.Append(stringbuilderTableSecond);
                //Second Table end

                stringbuilderTableSecond.Append(" <p>" + "Summary of"+" "+ MonthYear+" "+"Pending observations:" + "</p>");
                stringbuilderTOP.Append(stringbuilderTableSecond);
                stringbuilderTOPLastTable.Append(@"</br>"
            + " <table><tr bgcolor='#A3CEDC'><td width='20px'>"
               + "#" +
               "</td><td>" + "Audit area" + "</td><td>"
                + "Report no/State/Hub" + "</td><td>"
                + "Business Vertical"
                + "</td><td>" + "Observation/Root Cause"
                + "</td><td>" + "Action Plan" + "</td></tr>");


                int S_No_Count1 = 0;
                for (int index = 0; index < itemlist.Count; index++)
                {
                    ObservationStatusUserWiseNew_DisplayView_New ObjItem = itemlist[index];
                    S_No_Count1 = S_No_Count1 + 1;
                    string AuditArea = ObjItem.SubProcessName + "-" + ObjItem.ObservationCategoryName;
                    string ReportNoStateHubIssueNo = Convert.ToString(ObjItem.ScheduledOnID) + "/" + ObjItem.FinancialYear + "/" + ObjItem.State + "/" + ObjItem.HUB + "/IssueNo:" + ObjItem.IssueNumber;
                    string VerticalName = ObjItem.VerticalName;
                    string Observation = ObjItem.Observation;
                    string RootCause = Convert.ToString(ObjItem.RootCost);
                    string ActionPlan = ObjItem.ManagementResponse;
                    string DueDate = Convert.ToDateTime(ObjItem.TimeLine).ToString("dd-MMM-yyyy");

                    stringbuilderTOPLastTable.Append(@"<tr><td>" + S_No_Count1 + "</td><td>"
                   + AuditArea + " </td><td>" + ReportNoStateHubIssueNo + "</td><td>" + VerticalName + "</td><td>"
                    + "Observation:" + Observation + "</br>" + "Root Cause:" + RootCause + " </td><td>" + ActionPlan + "</br>" + "Due Date:" + DueDate + "</td></tr>");
                }
                string d = Convert.ToDateTime((DateTime.Now)).ToString("dd-MMM-yyyy");

                stringbuilderTOPLastTable.Append(@"</table>");

                stringbuilderTOPLastTable.Append(@"<p>All issues confirmed to be closed, will be verified for closure by the Internal Audit Team during the next review.
                Submitted for information and further guidance.
                <p></br></br>" + "<p><b>" + "Head" + "-" + "Internal Audit" + "</b></p>" + "</n></br><p><b>" + d + "</b></p>");

                stringbuilderTOP.Append(stringbuilderTOPLastTable);
                ProcessRequest(stringbuilderTOP.ToString(), MonthName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ProcessRequest(string context,string MonthsName)
        {
            string FinancialYear = string.Empty;
            System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
            sbTop.Append(@"
                < html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->
                   <style>
                table {
                    border-collapse: collapse;
                }
                
                table, td, th {
                    border: 1px solid black;
                    font-family:Arial;
                }
                
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                font-family:Arial;
                mso-
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

            if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    FinancialYear = ddlFinancialYear.SelectedItem.Text;
                }
            }

            sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

            sbTop.Append(context);
            sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>");
            
            sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Internal Audit Reports
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");
            string strBody = sbTop.ToString();
            Response.AppendHeader("Content-Type", "application/msword");
            Response.AppendHeader("Content-disposition", "attachment; filename=" + "Internal Audit Update -"+ MonthsName + " "+FinancialYear+" "+"Revised" + ".doc");
            Response.Write(strBody);
    }

}
}