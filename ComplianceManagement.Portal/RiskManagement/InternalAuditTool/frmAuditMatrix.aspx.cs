﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class frmAuditMatrix : System.Web.UI.Page
    {
        public  List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                //BindCustomer();
                //BindFilterCustomer();
                BindLegalEntityData();
                BindLegalEntityDataPopPup();             
                BindAuditorList();
                
            }
        }
        private void BindFilterCustomer()
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlFilterLocation.DataTextField = "Name";
                ddlFilterLocation.DataValueField = "ID";
                ddlFilterLocation.Items.Clear();
                ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
                ddlFilterLocation.DataBind();
                ddlFilterLocation.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindLegalEntityDataPopPup()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntityPopPup.DataTextField = "Name";
            ddlLegalEntityPopPup.DataValueField = "ID";
            ddlLegalEntityPopPup.Items.Clear();
            ddlLegalEntityPopPup.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntityPopPup.DataBind();
            ddlLegalEntityPopPup.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }
        public void BindCustomer(int BranchID)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlFilterLocationPopPup.DataTextField = "Name";
            ddlFilterLocationPopPup.DataValueField = "ID";
            ddlFilterLocationPopPup.Items.Clear();
            ddlFilterLocationPopPup.DataSource = FillCustomer(customerID, BranchID);
            ddlFilterLocationPopPup.DataBind();
            ddlFilterLocationPopPup.Items.Insert(0, new ListItem(" Select Location ", "-1"));
        }


        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));                    
                }
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                    ddlSubEntity1.Items.Clear();

                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.Items.Clear();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.Items.Clear();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.Items.Clear();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }     
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));                    
                }
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                    ddlSubEntity2.ClearSelection();

                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));                    
                }
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                    ddlSubEntity3.ClearSelection();

                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                    ddlFilterLocation.ClearSelection();
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void upPromotor_Load(object sender, EventArgs e)
        {
           
        }
        protected void ddlLegalEntityPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
            {
                if (ddlLegalEntityPopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1PopPup, Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                    BindMatrixListADD(Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity1PopPup.Items.Count > 0)
                    ddlSubEntity1PopPup.Items.Clear();

                if (ddlSubEntity2PopPup.Items.Count > 0)
                    ddlSubEntity2PopPup.Items.Clear();

                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.Items.Clear();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.Items.Clear();
            }            
        }
        protected void ddlSubEntity1PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
            {
                if (ddlSubEntity1PopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2PopPup, Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue));
                    BindMatrixListADD(Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity2PopPup.Items.Count > 0)
                    ddlSubEntity2PopPup.ClearSelection();

                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.ClearSelection();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }            
        }
        protected void ddlSubEntity2PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
            {
                if (ddlSubEntity2PopPup.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3PopPup, Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                    BindMatrixListADD(Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue));
                }
            }
            else
            {
                if (ddlSubEntity3PopPup.Items.Count > 0)
                    ddlSubEntity3PopPup.ClearSelection();

                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }            
        }
        protected void ddlSubEntity3PopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
            {
                if (ddlSubEntity3PopPup.SelectedValue != "-1")
                {
                    BindCustomer(Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                    BindMatrixListADD(Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue));
                }
            }
            else
            {
                if (ddlFilterLocationPopPup.Items.Count > 0)
                    ddlFilterLocationPopPup.ClearSelection();
            }            
        }
        protected void ddlFilterLocationPopPup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
            {
                BindMatrixListADD(Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue));
            }
        }
      
        public string GetCustomerBranchName(long branchid)
        {
            string processnonprocess = "";
            if (branchid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(branchid);
                    mst_CustomerBranch mstcustomerbranch = (from row in entities.mst_CustomerBranch
                                                            where row.ID == a && row.IsDeleted == false
                                                            select row).FirstOrDefault();

                    processnonprocess = mstcustomerbranch.Name;
                }
            }
            return processnonprocess;
        }

        public string GetRatio(int lessthan, int Greaterthan)
        {
            string processnonprocess = "";
            if (lessthan != -1 && Greaterthan != -1)
            {
                processnonprocess = lessthan + " " + "BETWEEN" + " " + Greaterthan;
            }
            return processnonprocess;
        }
        private void BindAuditorList()
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int CustomerBranchId = -1;                
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
               
                //Branchlist.Clear();
                //var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
                //var Branchlistloop = Branchlist.ToList();
                var AuditorMasterList = GetMatrixGridDisplay(customerID, CustomerBranchId);
                grdAuditor.DataSource = AuditorMasterList;
                Session["TotalRows"] = AuditorMasterList.Count;
                grdAuditor.DataBind();
                upPromotorList.Update();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public  List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public  void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {


            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public static List<SP_GetMatrixGrid_Result> GetMatrixGridDisplay(int Customerid, int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SP_GetMatrixGrid_Result> auditdsplay = new List<SP_GetMatrixGrid_Result>();
                if (Branchid != -1)
                {

                    auditdsplay = entities.SP_GetMatrixGrid(Customerid).Where(x => x.CustomerBranchID == Branchid).ToList();
                }
                else
                {
                    auditdsplay = entities.SP_GetMatrixGrid(Customerid).ToList();
                }
                return auditdsplay;
            }
        }
        public static List<SP_GetAuditMatrix_Result> GetSPAuditMatrixDisplayADD(int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_GetAuditMatrix(Branchid).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_GetAuditMatrixEdit_Result> GetSPAuditMatrixDisplayEDIT(int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_GetAuditMatrixEdit(Branchid).ToList();
                return auditdsplay;
            }
        }
        private void BindMatrixListADD(int Branchid)
        {
            try
            {
                var AuditorMasterList = GetSPAuditMatrixDisplayADD(Branchid);
                if (AuditorMasterList != null)
                {
                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();
                    grdCompliances.DataSource = AuditorMasterList;
                    grdCompliances.DataBind();
                    upPromotor.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindMatrixListEDIT(int Branchid)
        {
            try
            {
                var AuditorMasterList = GetSPAuditMatrixDisplayEDIT(Branchid);
                if (AuditorMasterList != null)
                {
                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();
                    grdCompliances.DataSource = AuditorMasterList;
                    grdCompliances.DataBind();
                    upPromotor.Update();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static object FillCustomer(int Customerid,int BranchID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Sp_FillAuditMatrixDropDown_Result> query = new List<Sp_FillAuditMatrixDropDown_Result>();

                query = entities.Sp_FillAuditMatrixDropDown(Customerid, BranchID).ToList();
                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
       
        public static void DeleteAuditMatrix(int branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<Mst_AuditMatrix> AuditorMastertoDeleteList = (from row in entities.Mst_AuditMatrix
                                                                   where row.CustomerBranchID == branchid
                                                                   select row).ToList();
                if (AuditorMastertoDeleteList != null)
                {
                    AuditorMastertoDeleteList.ForEach(entry =>
                   {
                       Mst_AuditMatrix AuditorMastertoDelete = (from row in entities.Mst_AuditMatrix
                                                                where row.ID == entry.ID
                                                                select row).FirstOrDefault();
                       AuditorMastertoDelete.IsActive = true;
                       entities.SaveChanges();
                   });
                }
            }
        }
        public static bool CreateAuditMatrix(List<Mst_AuditMatrix> mstauditmatrixlist)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    mstauditmatrixlist.ForEach(entry =>
                    {
                        entities.Mst_AuditMatrix.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (grdCompliances.Rows.Count > 0)
                {
                    int customerbranchidSave = -1;
                    bool suucess = false;
                    List<Mst_AuditMatrix> mstauditmatrixlist = new List<Mst_AuditMatrix>();
                    List<Mst_AuditMatrix> mstauditmatrixlist1 = new List<Mst_AuditMatrix>();

                    if ((int)ViewState["Mode"] == 1)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomerBranchId"])))
                        {
                            customerbranchidSave = Convert.ToInt32(ViewState["CustomerBranchId"]);
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(ddlLegalEntityPopPup.SelectedValue))
                        {
                            if (ddlLegalEntityPopPup.SelectedValue != "-1")
                            {
                                customerbranchidSave = Convert.ToInt32(ddlLegalEntityPopPup.SelectedValue);
                            }
                        }

                        if (!String.IsNullOrEmpty(ddlSubEntity1PopPup.SelectedValue))
                        {
                            if (ddlSubEntity1PopPup.SelectedValue != "-1")
                            {
                                customerbranchidSave = Convert.ToInt32(ddlSubEntity1PopPup.SelectedValue);
                            }
                        }

                        if (!String.IsNullOrEmpty(ddlSubEntity2PopPup.SelectedValue))
                        {
                            if (ddlSubEntity2PopPup.SelectedValue != "-1")
                            {
                                customerbranchidSave = Convert.ToInt32(ddlSubEntity2PopPup.SelectedValue);
                            }
                        }

                        if (!String.IsNullOrEmpty(ddlSubEntity3PopPup.SelectedValue))
                        {
                            if (ddlSubEntity3PopPup.SelectedValue != "-1")
                            {
                                customerbranchidSave = Convert.ToInt32(ddlSubEntity3PopPup.SelectedValue);
                            }
                        }

                        if (!String.IsNullOrEmpty(ddlFilterLocationPopPup.SelectedValue))
                        {
                            if (ddlFilterLocationPopPup.SelectedValue != "-1")
                            {
                                customerbranchidSave = Convert.ToInt32(ddlFilterLocationPopPup.SelectedValue);
                            }
                        }                       
                    }
                    if (customerbranchidSave != -1)
                    {
                        foreach (GridViewRow g1 in grdCompliances.Rows)
                        {
                            string lblProcessName = (g1.FindControl("lblProcessName") as Label).Text;
                            string lblValue = (g1.FindControl("lblValue") as Label).Text;
                            string txtlessthan = (g1.FindControl("txtlessthan") as TextBox).Text;
                            string txtgreaterthan = (g1.FindControl("txtgreaterthan") as TextBox).Text;
                            if (!String.IsNullOrEmpty(lblProcessName))
                            {
                                Mst_AuditMatrix Mstauditfrequency = new Mst_AuditMatrix();
                                Mstauditfrequency.CustomerBranchID = customerbranchidSave;
                                Mstauditfrequency.Rating = Convert.ToInt32(lblValue);
                                Mstauditfrequency.Lessthan = Convert.ToInt32(txtlessthan);
                                Mstauditfrequency.Greaterthan = Convert.ToInt32(txtgreaterthan);
                                Mstauditfrequency.IsActive = false;
                                mstauditmatrixlist.Add(Mstauditfrequency);
                            }
                        }

                        mstauditmatrixlist1 = mstauditmatrixlist.Where(entry => entry.CustomerBranchID == null).ToList();
                        if (mstauditmatrixlist1.Count == 0)
                        {
                            DeleteAuditMatrix(customerbranchidSave);
                            suucess = CreateAuditMatrix(mstauditmatrixlist);
                            if (suucess == true)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Data Save successfully.";
                              
                                BindAuditorList();

                                int count = Convert.ToInt32(GetTotalPagesCount());
                                if (count > 0)
                                {
                                    int gridindex = grdAuditor.PageIndex;
                                    string chkcindition = (gridindex + 1).ToString();
                                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                                }
                                // GetPageDisplaySummary();
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Error.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Count Miss Match";
                        }
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Errro In Customer Branch";
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;                
                divLocationEdit.Visible = false;                
                grdCompliances.DataSource = null;
                grdCompliances.DataBind();
                upPromotor.Update();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int CustomerBranchId = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_PROMOTER"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["CustomerBranchId"] = CustomerBranchId;
                    
                    divLocationEdit.Visible = true;
                    lblLocationEdit.Text = GetCustomerBranchName(CustomerBranchId);
                    BindMatrixListEDIT(CustomerBranchId);
                    upPromotor.Update();
                    

                }
                else if (e.CommandName.Equals("DELETE_PROMOTER"))
                {                    
                    DeleteAuditMatrix(CustomerBranchId);                    
                    BindAuditorList();
                   // GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAuditor.PageIndex = e.NewPageIndex;

                BindAuditorList();
               // GetPageDisplaySummary();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
               
            
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
              
                
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };
                grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
               

                BindAuditorList();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

     
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditor.PageIndex = chkSelectedPage - 1;
            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;          
            BindAuditorList();
        }
    }
}