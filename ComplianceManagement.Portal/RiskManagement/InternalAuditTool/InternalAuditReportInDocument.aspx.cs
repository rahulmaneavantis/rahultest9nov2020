﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;
using Spire.Presentation.Drawing.Animation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class InternalAuditReportInDocument : System.Web.UI.Page
    {
       
        public static List<long> Branchlist = new List<long>();
        public static bool ApplyFilter;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        public static string linkclick;
        protected static string AuditHeadOrManagerReport;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                BindProcess("P");
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData();
            
                ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
              
                ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
                
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }
        private void BindProcess(string flag)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                if (flag == "P")
                {
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcess("P", customerID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }
                else
                {
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcess("N", customerID);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }

      

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

       
      

        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
           
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
         
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
          
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
                }
            }

            BindProcess("P");
            BindVertical();
            BindSchedulingType();
          
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProcess("P");
            BindVertical();
            BindSchedulingType();
          
        }
              
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
                    
        }


        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFinancialYear.SelectedValue != "-1")
            {
                ddlSchedulingType.DataTextField = "Name";
                ddlSchedulingType.DataValueField = "ID";
                ddlSchedulingType.Items.Insert(1, "Jan");
                ddlSchedulingType.Items.Insert(2, "Feb");
                ddlSchedulingType.Items.Insert(3, "March");
                ddlSchedulingType.Items.Insert(4, "Apr");
                ddlSchedulingType.Items.Insert(5, "May");
                ddlSchedulingType.Items.Insert(6, "June");
                ddlSchedulingType.Items.Insert(7, "Jully");
                ddlSchedulingType.Items.Insert(8, "Aug");
                ddlSchedulingType.Items.Insert(9, "Sept");
                ddlSchedulingType.Items.Insert(10, "Oct");
                ddlSchedulingType.Items.Insert(11, "Nov");
                ddlSchedulingType.Items.Insert(12, "Dec");
             
            }
        }
        


        protected void btnExportDoc_Click(object sender, EventArgs e)
        {
            try
            {
                ApplyFilter = false;
                int userID = -1;
                int RoleID = -1;
                if (PerformerFlag)
                    RoleID = 3;
                if (ReviewerFlag)
                    RoleID = 4;
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int CustBranchID = -1;
                int VerticalID = -1;
                int ProcessID = -1;
                String FinancialYear = String.Empty;
                String Period = String.Empty;
                int months = -1;
                String FirstYear = String.Empty;
                String SecondYear = String.Empty;
                String MonthYear = String.Empty;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                        string[] words = FinancialYear.Split('-');
                        FirstYear = words[0];
                        SecondYear = words[1];
                    }
                }

                if (!String.IsNullOrEmpty(ddlSchedulingType.SelectedValue))
                {
                    if (ddlSchedulingType.SelectedValue != "-1")
                    {
                        months = Convert.ToInt32(ddlSchedulingType.SelectedIndex);
                        string MonthName = ddlSchedulingType.SelectedItem.Text;
                        if (MonthName == "Jan" || MonthName == "Feb" || MonthName == "March")
                        {
                            MonthYear = MonthName + " " + SecondYear;
                        }
                        else
                        {
                            MonthYear = MonthName + " " + FirstYear;
                        }
                    }
                }

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        ProcessID = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlVertical.SelectedValue))
                {
                    if (ddlVertical.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                    }
                }

                if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                {
                    if (RoleID == -1)
                    {
                        RoleID = 4;
                    }
                    Branchlist.Clear();
                    List<long> branchids = AssignEntityManagementRisk.CheckAuditManagerLocation(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                    Branchlist = branchids.ToList();
                }
                else
                {

                    userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                    Branchlist.Clear();
                    var bracnhes = GetAllHierarchy(customerID, CustBranchID);
                    var Branchlistloop = Branchlist.ToList();
                }


                List<ObservationStatusUserWiseNew_DisplayView_New> itemlist = new List<ObservationStatusUserWiseNew_DisplayView_New>();
               
                itemlist = InternalControlManagementDashboardRisk.getDatatest(RoleID, customerID, userID, VerticalID, ProcessID, FinancialYear, months, Branchlist);
                
                System.Text.StringBuilder stringbuilderTOP = new System.Text.StringBuilder();
                System.Text.StringBuilder stringbuilderTOPLastTable = new System.Text.StringBuilder();              
                System.Text.StringBuilder stringbuilderTableFirst = new System.Text.StringBuilder();
                System.Text.StringBuilder stringbuilderTableSecond = new System.Text.StringBuilder();
                System.Text.StringBuilder stringbuilderTOPThirdTable = new System.Text.StringBuilder();
                


                stringbuilderTableFirst.Append(@"</br></br>"
                + " <p align='center' font - size='20.0pt' style='font-family:Arial;'><u><b>" + "Internal Audit Reports Issued during " + MonthYear +
                   "</b></u></p></br>" +
                    "<p>" +
                    "During the month of " + MonthYear + ", the following Internal Audits were conducted and reports released post agreement with the Hub&State Managers or " +
                    "Functional Heads at Corporate Office on the Audit Issues, Rectification Action Plan and Timeline for closure of Audit Issues."
                   + "</p></br>"
                   + "<table>" + "<tr bgcolor='#A3CEDC'><td>"
                   + FinancialYear + "Report# "
                   + "</td><td>" + "State/Function" + "</td><td>" + "Audit Report Of Hub"
                   + "</td><td>"
                   + "No. of locations"
                   + "</td><td>" + "Business Volumes" + " </td><td>" + "Business Value Rs. Lacs" + "</td>"
                    + "<td>" + "Rating Score" + "</td><td>" + "Audit Rating" +
                    "</td></tr>");
                for (int index = 0; index < itemlist.Count; index++)
                {
                    ObservationStatusUserWiseNew_DisplayView_New ObjItem = itemlist[index];
                    string fYear = ObjItem.FinancialYear;
                    string ReportNo = Convert.ToString(ObjItem.ScheduledOnID);
                    string state = ObjItem.State;
                    string Hub = ObjItem.HUB;
                    string LocationCount = ObjItem.Location;
                    string BusinessVolumes = string.Empty;
                    string BusinessValues = string.Empty;
                    string RatingScore = string.Empty;
                    string AuditRating = string.Empty;
                    stringbuilderTableFirst.Append("<tr><td>"
                   + ReportNo + "</td><td>" + state + "</td><td>" + Hub + "</td><td>"
                   + LocationCount + "</td><td>" + "" + " </td><td>" + "" + "</td>"
                   + "<td>" + "" + "</td><td>" + "" + "</td></tr>");
                }
                stringbuilderTableFirst.Append("<tr><td>"
                   + "" + "</td><td colspan='3' bgcolor='#A3CEDC'align='right'>" + "% Completed upto " + MonthYear + "</td><td bgcolor='#A3CEDC'>" + "" + " </td><td bgcolor='#A3CEDC'>" + "" + "</td>"
                   + "<td bgcolor='#A3CEDC'>" + "" + "</td><td>" + "" + "</td></tr>");

                stringbuilderTableFirst.Append("</table>");
                stringbuilderTOP.Append(stringbuilderTableFirst);
                //first table end
                //stringbuilderTableSecond.Append("<p>" + "The summary of the Audit Ratings for the reports issued during" + " " + FinancialYear + " " + "is as under:" +
                //  "</p><table><tr bgcolor='#A3CEDC'><td>"
                //  + "Sr No." + "</td><td>"
                //  + "Audit Ratings" + "</td><td>" + "Number of Hubs" +
                //  "</td></tr>");
                //int S_No_Countt = 0;
                //for (int index = 0; index < itemlist.Count; index++)
                //{
                //    ObservationStatusUserWiseNew_DisplayView_New ObjItem = itemlist[index];
                //    S_No_Countt = S_No_Countt + 1;
                //    stringbuilderTableSecond.Append("<tr><td>" + S_No_Countt + "</td><td>" + "" + "</td><td>" + "" + "</td></tr>");
                //}
                //stringbuilderTableSecond.Append("</table>" + "</br>");

                //stringbuilderTOP.Append(stringbuilderTableSecond);
                //Second Table end


                stringbuilderTOPThirdTable.Append(@"</br></br> <p style='font: strong; font - size:20px;'><u><b>" + "Summary of High & Medium risk issues" + "</b></u></p>"
                    + " <table><tr bgcolor='#A3CEDC'><td>"
                    + "#" +
                    "</td><td>" + "Observation"
                    + "</td><td>" + "Action Plan" + "</td></tr>");

                int S_No_Count1 = 0;
                string RefValue;
                for (int index = 0; index < itemlist.Count; index++)
                {
                    ObservationStatusUserWiseNew_DisplayView_New ObjItem = itemlist[index];
                    S_No_Count1 = S_No_Count1 + 1;
                    string AuditArea = ObjItem.SubProcessName + "-" + ObjItem.ObservationCategoryName;
                    string ReportNoStateHubIssueNo = Convert.ToString(ObjItem.ScheduledOnID) + "/" + ObjItem.FinancialYear + "/" + ObjItem.State + "/" + ObjItem.HUB + "/IssueNo:" + ObjItem.IssueNumber;
                    string VerticalName = ObjItem.VerticalName;
                    string Observation = ObjItem.Observation;
                    string RootCause = Convert.ToString(ObjItem.RootCost);
                    string ActionPlan = ObjItem.ManagementResponse;
                    string DueDate = Convert.ToDateTime(ObjItem.TimeLine).ToString("dd-MMM-yyyy");
                    if (ObjItem.State != null)
                    {
                         RefValue = "Ref:" + ObjItem.State.ToString() + "/" + ObjItem.Location.ToString() + "/Issue No:" + ObjItem.IssueNumber.ToString();
                    }
                    else {
                         RefValue = "Ref:" + ObjItem.Location.ToString() + "/Issue No:" + ObjItem.IssueNumber.ToString();
                    }
                    stringbuilderTOPThirdTable.Append(@"<tr><td>" + S_No_Count1 + "</td><td>"
                    + "<u><b>" + "Statutory Compliance-" + ObjItem.ObservationCategoryName + "</u></b>" + "<br>"
                        + "Observation:" + "<b>" + Observation + "</b>" + "<br>" + "<b>" + "Root Cause:" + "</b>" + RootCause + "<br>" + RefValue + "</td><td>" + ActionPlan + "<br>" + "Due Date:" + DueDate + "</td></tr>");
                }
                stringbuilderTOPThirdTable.Append(@"</table>");
                stringbuilderTOP.Append(stringbuilderTOPThirdTable);
                //third table end


                                stringbuilderTOPLastTable.Append(@"<P style='font: strong; font - size:20px;'><u><b>" + "Summary of low risk issues:" + "</b></u></P></br>"
                + " <table><tr bgcolor='#A3CEDC'><td>"
                + "#" +
                "</td><td><b>" + "Category"
                + "</b></td><td><b>" + "Sub-Category" + "</b></td><td><b>" + "Brief Notes" + "</b></td><td><b>" + "Closed" +
                "</b></td><td><b>" + "Open" + "</b></td><td><b>" + "Total" + "</b></td></tr>");

                int S_No_Count = 0;
                int closeCount = 0;
                int openCount = 0;
                int TotalCount = 0;

                int FlagcloseCount = 0;
                int FlagopenCount = 0;
                int FlagTotalCount = 0;

                for (int index = 0; index < itemlist.Count; index++)
                {
                    ObservationStatusUserWiseNew_DisplayView_New ObjItem = itemlist[index];
                    S_No_Count = S_No_Count + 1;
                    string categoryName = ObjItem.ObservationCategoryName;
                    string subCategoryName = ObjItem.ObservationSubCategoryName;
                    string status = ObjItem.Status.ToString();
                    if (status.Equals("Closed"))
                    {
                        closeCount = 1;
                        FlagcloseCount = FlagcloseCount + 1;
                    }
                    else {
                        openCount = 1;
                        FlagopenCount = FlagopenCount + 1;
                    }
                    TotalCount = Convert.ToInt32(closeCount) + Convert.ToInt32(openCount);
                    FlagTotalCount = FlagTotalCount + TotalCount;
                    stringbuilderTOPLastTable.Append(@"<tr><td>" + S_No_Count + "</td><td>"
                        + "<b>" + categoryName + "</b>" + " </td><td>" + subCategoryName + "</td><td></td><td>" + closeCount + "</td><td>" + openCount + "</td><td>" + TotalCount + "</td></tr>");
                }
                stringbuilderTOPLastTable.Append(@"<tr  bgcolor='#A3CEDC'><td></td><td>" + "<b>" + "Grand Total" + "</b>" + " </td><td></td><td></td><td>" + "<b>" + FlagcloseCount + "</b>" + "</td><td>" + "<b>" + FlagopenCount + "</b>" + "</td><td>" + "<b>" + FlagTotalCount + "</b>" + "</td></tr>");


                string TodayDate = Convert.ToDateTime((DateTime.Now)).ToString("dd-MMM-yyyy");

                stringbuilderTOPLastTable.Append(@"</table>");
                stringbuilderTOPLastTable.Append(@"<br><P>All issues confirmed to be closed, will be verified for closure by the Internal Audit Team during the next review.
                    </n>Submitted for information and further guidance.<P><br><P><b>Head-Internal Audit</b></P></n><b>" + TodayDate + "</b>");
                stringbuilderTOP.Append(stringbuilderTOPLastTable);
                ProcessRequest(stringbuilderTOP.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ProcessRequest(string context)
        {


            System.Text.StringBuilder sbTop = new System.Text.StringBuilder();
           
            sbTop.Append(@"
                < html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->
                   <style>
                table {
                    border-collapse: collapse;
                }
                
                table, td, th {
                    border: 1px solid black;
                    font-family:Arial;
                }
                
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                

                <!-- /* Style Definitions */

                @page Section1
                {
                
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header:h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                font-family:Arial;
                }


                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                -->
                </style></head>");

            sbTop.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

            sbTop.Append(context);
          

            sbTop.Append(@" <table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr><td>");

            string year = ddlFinancialYear.SelectedItem.Text;
            string month = ddlSchedulingType.SelectedItem.Text;
            string concatmonthyear = month + " " + year;
          
            sbTop.Append(@"</div>
                </td>
                <td>
                <div style='mso-element:footer' id=f1>
                <p class=MsoFooter>Internal Audit Reports
                <span style=mso-tab-count:2'></span><span style='mso-field-code:"" PAGE ""'></span>
                of <span style='mso-field-code:"" NUMPAGES ""'></span></p></div>
                </td></tr>
                </table>
                </body></html>
                ");

          

            string strBody = sbTop.ToString();
            Response.AppendHeader("Content-Type", "application/msword");
            Response.AppendHeader("Content-disposition", "attachment; filename=" + "Internal Audit Update " + concatmonthyear + ".doc");
            Response.Write(strBody);
        }
    }
}