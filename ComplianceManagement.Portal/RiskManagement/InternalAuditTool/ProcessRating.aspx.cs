﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class ProcessRating : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindCustomer();
                BindProcess();
                BindFilterCustomer();
                var CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                BindAuditorList(CustomerBranchID);
                ddlFilterLocation_SelectedIndexChanged(sender, e);
                GetPageDisplaySummary();
            }
        }
        public string ShowCustomerBranchName(long branchid)
        {
            string processnonprocess = "";
            if (branchid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(branchid);
                    mst_CustomerBranch mstcustomerbranch = (from row in entities.mst_CustomerBranch
                                                            where row.ID == a
                                                            select row).FirstOrDefault();

                    processnonprocess = mstcustomerbranch.Name;
                }
            }
            return processnonprocess;
        }
        public string ShowRating(int Rating)
        {            
            string processnonprocess = "";
            if (Rating == 1)
            {
                processnonprocess = "High";
            }
            else if (Rating == 2)
            {
                processnonprocess = "Medium";
            }
            else if (Rating == 3)
            {
                processnonprocess = "Low";
            }
            return processnonprocess;
        }
        public string ShowExternalAuditor(long ExternalAuditorId)
        {
            string processnonprocess = "";
            if (ExternalAuditorId != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    Mst_AuditorMaster mstauditorMaster = (from row in entities.Mst_AuditorMaster
                                                          where row.ID == ExternalAuditorId
                                                          select row).FirstOrDefault();

                    processnonprocess = mstauditorMaster.Name;
                }
            }
            return processnonprocess;
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditorList(Convert.ToInt32(ddlFilterLocation.SelectedValue));
        }
        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        private void BindFilterCustomer()
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlFilterLocation.DataTextField = "Name";
                ddlFilterLocation.DataValueField = "ID";
                ddlFilterLocation.Items.Clear();
                ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
                ddlFilterLocation.DataBind();
                //ddlFilterLocation.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindCustomer()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLocation.DataTextField = "Name";
            ddlLocation.DataValueField = "ID";
            ddlLocation.Items.Clear();
            ddlLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem(" Select Location ", "-1"));
        }
        private void BindProcess()
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlProcess.Items.Clear();
                ddlProcess.DataTextField = "Name";
                ddlProcess.DataValueField = "ID";
                ddlProcess.DataSource = ProcessManagement.FillProcess(customerID);
                ddlProcess.DataBind();
                ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlAuditor_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        //protected void upPromotor_Load(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        //        //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        protected void grdAuditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
        public string ShowProcessName(long processId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetProcessName(processId, customerID);
            return processnonprocess;
        }
        private void BindAuditorList(int Branchid)
        {
            try
            {
                var AuditorMasterList = ProcessManagement.GetAllMst_ProcessRating(Branchid);
                grdAuditor.DataSource = AuditorMasterList;
                Session["TotalRows"] = AuditorMasterList.Count;
                grdAuditor.DataBind();
                upPromotorList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlLocation.SelectedValue))
                {
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        if (!String.IsNullOrEmpty(ddlRating.SelectedValue))
                        {
                            int processid = -1;
                            int CustomerBranchID = -1;
                            int rating = -1;
                            CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                            processid = Convert.ToInt32(ddlProcess.SelectedValue);
                            rating = Convert.ToInt32(ddlRating.SelectedValue);
                            
                            if ((int)ViewState["Mode"] == 0)
                            {
                                CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                            }
                            else
                            {
                                CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                            }
                           
                            Mst_ProcessRating mstauditorusermaster = new Mst_ProcessRating()
                            {
                                CustomerBranchid = CustomerBranchID,
                                ProcessID = processid,
                                Rating = rating,
                                CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                UpdatedBy = null,
                                UpdatedOn = null,
                                IsActive = false
                            };
                            if ((int)ViewState["Mode"] == 1)
                            {
                                mstauditorusermaster.CustomerBranchid = Convert.ToInt32(ViewState["CustomerBranchid"]);
                                mstauditorusermaster.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            }
                            if ((int)ViewState["Mode"] == 0)
                            {
                                if(ProcessManagement.ProcessRatingGetByProcessAndRating(processid,CustomerBranchID)==0)
                                {
                                    ProcessManagement.CreateMst_ProcessRating(mstauditorusermaster);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Process Rating Added Successfully";
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Same Process Rating can not be Added";
                                }

                            }
                            else if ((int)ViewState["Mode"] == 1)
                            {
                                ProcessManagement.UpdateMst_ProcessRating(mstauditorusermaster);
                               // ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAuditorDialog\").dialog('close')", true);
                            }
                            
                            BindAuditorList(CustomerBranchID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddAuditAssignment_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                ddlLocation.SelectedValue = "-1";
                ddlProcess.SelectedValue = "-1";
                ddlRating.SelectedValue = "-1";
               // upPromotor.Update();
               // ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //var AuditorMasterList = ProcessManagement.GetAllMst_ProcessRatingList();
                //if (direction == SortDirection.Ascending)
                //{
                //    AuditorMasterList = AuditorMasterList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Descending;
                //    ViewState["SortOrder"] = "Asc";
                //    ViewState["SortExpression"] = e.SortExpression.ToString();
                //}
                //else
                //{
                //    AuditorMasterList = AuditorMasterList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Ascending;
                //    ViewState["SortOrder"] = "Desc";
                //    ViewState["SortExpression"] = e.SortExpression.ToString();
                //}
                //foreach (DataControlField field in grdAuditor.Columns)
                //{
                //    if (field.SortExpression == e.SortExpression)
                //    {
                //        ViewState["SortIndex"] = grdAuditor.Columns.IndexOf(field);
                //    }
                //}
                //grdAuditor.DataSource = AuditorMasterList;
                //grdAuditor.DataBind();

                var AuditorMasterList = ProcessManagement.GetAllMst_ProcessRatingList();
                if (direction == SortDirection.Ascending)
                {
                    AuditorMasterList = AuditorMasterList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    AuditorMasterList = AuditorMasterList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdAuditor.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdAuditor.Columns.IndexOf(field);
                    }
                }

                grdAuditor.DataSource = AuditorMasterList;
                grdAuditor.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int CustomerBranchid = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_PROMOTER"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["CustomerBranchid"] = CustomerBranchid;
                    Mst_ProcessRating mstauditorusermaster = ProcessManagement.Mst_ProcessRatingGetByID(CustomerBranchid);
                    ddlLocation.SelectedValue = Convert.ToString(mstauditorusermaster.CustomerBranchid);
                    ddlProcess.SelectedValue = Convert.ToString(mstauditorusermaster.ProcessID);
                    ddlRating.SelectedValue = Convert.ToString(mstauditorusermaster.Rating);                    
                    //upPromotor.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorDialog\").dialog('open')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                int branchid = -1;
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
                grdAuditor.PageIndex = e.NewPageIndex;
                BindAuditorList(branchid);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //protected void grdAuditor_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
        //        if (sortColumnIndex != -1)
        //        {
        //            AddSortImage(sortColumnIndex, e.Row);
        //        }
        //    }
        //}
        //protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        //{
        //    System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
        //    sortImage.ImageAlign = ImageAlign.AbsMiddle;
        //    if (direction == SortDirection.Ascending)
        //    {
        //        sortImage.ImageUrl = "../../Images/SortAsc.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "../../Images/SortDesc.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }
        //    headerRow.Cells[columnIndex].Controls.Add(sortImage);
        //}
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                ddlFilterLocation_SelectedIndexChanged(sender, e);
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {
                }
                //Reload the Grid
                ddlFilterLocation_SelectedIndexChanged(sender, e);
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }


        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                ddlFilterLocation_SelectedIndexChanged(sender, e);
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void grdAuditor_RowCommand1(object sender, GridViewCommandEventArgs e)
        {

        }
    }
}