﻿<%@ Page Title="Person Responsible" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="FrmPersonResponsible.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.FrmPersonResponsible" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .GridView1 {
            width: 200px;
            margin-left: auto;
            margin-right: auto;
        }

        .accordionContent1 {
            background-color: #D3DEEF;
            border-color: -moz-use-text-color #2F4F4F #2F4F4F;
            border-right: 1px dashed #2F4F4F;
            border-style: none dashed dashed;
            border-width: medium 1px 1px;
            padding: 10px 5px 5px;
            width: 92%;
        }

        .accordionHeaderSelected1 {
            background-color: #5078B3;
            border: 1px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 92%;
        }

        .accordionHeader1 {
            background-color: #2E4D7B;
            border: 1px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 92%;
        }

        .href1 {
            color: White;
            font-weight: bold;
            text-decoration: none;
        }

        .textbox {
            width: 100px;
            padding: 2px 4px;
            font: inherit;
            text-align: left;
            border: solid 1px #BFBFBF;
            border-radius: 2px;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            text-transform: uppercase;
        }
    </style>
    <script type="text/javascript">
        function initializeDatePicker(date) {

            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }
    </script>
    <script type="text/javascript">
        function setTabActive(id) {
            debugger;
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Person Responsible/Auditee Review');
        });

        function ShowDialog(atbdid, custbranchid, FinancialYear, Forperiod, VID) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '1000px');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '1100px');
            $('#showdetails').attr('src', "../AuditTool/InternalAuditControlPersonResponsible.aspx?FinYear=" + FinancialYear + "&ForMonth=" + Forperiod + "&ATBDID=" + atbdid + "&BID=" + custbranchid + "&VID=" + VID);
        };

        function ShowImpDialog(resultid, custbranchid, FinancialYear, Forperiod, VID) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '1000px');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '1100px');
            $('#showdetails').attr('src', "../AuditTool/IMPStatusAuditPersonResponsible.aspx?FinYear=" + FinancialYear + "&ForMonth=" + Forperiod + "&ResultID=" + resultid + "&BID=" + custbranchid + "&VID=" + VID);
        };


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                <div class="clearfix"></div>
                <section class="panel">

                 <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">                                           
                                        <li class="active" id="liProcess" runat="server">
                                            <asp:LinkButton ID="lnkProcess" OnClick="ShowProcessGrid" runat="server">Process</asp:LinkButton>                                           
                                        </li>
                                          
                                        <li class=""  id="liImplementation" runat="server">
                                            <asp:LinkButton ID="lnkImplementation" OnClick="ShowImplementationGrid"  runat="server">Implementation</asp:LinkButton>                                        
                                        </li>                                        
                                    </ul>
                                </header> 

                <div class="clearfix"></div>

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="tab-content ">                         
                                <asp:Panel ID="Panel1" runat="server">
                                    <div class="col-md-12 colpadding0">
                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" Selected="True" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>                                       
                                    </div>
                                    </div>   

                                    <div class="clearfix"></div>

                                    <div id="ProcessGrid" runat="server">
                                    <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverage" AutoGenerateColumns="false" AllowSorting="true" 
                                         CssClass="table" GridLines="None"
                                        AllowPaging="true" PageSize="5" OnPageIndexChanging="grdSummaryDetailsAuditCoverage_PageIndexChanging"
                                         DataKeyNames="TESTID">
                                        <Columns>
                                              <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                    <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                        </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Vertical">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                    <asp:Label ID="lblVertical" runat="server" Text='<%# Eval("VerticalName") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                        </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Process">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                    <asp:Label ID="lblProcess" runat="server" Text='<%# Eval("ProcessName") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                                        </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Sub Process">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                    <asp:Label ID="lblSubProcess" runat="server" Text='<%# Eval("SubProcessName") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                                        </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>  
                                            <asp:TemplateField HeaderText="Financial Year">
                                                <ItemTemplate>
                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                    <asp:Label ID="lblFinancialYear" runat="server" Text='<%# Eval("FinancialYear") %>'  data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="For Period">
                                                <ItemTemplate>
                                                      <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                    <asp:Label ID="lblForPeriod" runat="server" Text='<%# Eval("ForPerid") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ForPerid") %>'></asp:Label>
                                                          </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                                                  
                                            <asp:TemplateField HeaderText="Observation">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                        <asp:Label ID="LActivityTobeDone" runat="server" Text='<%# Eval("Observation") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("Observation") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Recommendation">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("Recomendation") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("Recomendation") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Category">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                        <asp:Label ID="lblObservationCategoryName" runat="server" Text='<%# Eval("ObservationCategoryName") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ObservationCategoryName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <%-- <asp:TemplateField HeaderText="Rating">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                        <asp:Label ID="lblObservationRatingName" runat="server" Text='<%# Eval("ObservationRatingName") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("ObservationRatingName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>

                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnChangeStatus" runat="server"
                                                        CommandName="CHANGE_STATUS" OnClick="btnChangeStatus_Click" CommandArgument='<%# Eval("TESTID") + "," + Eval("CustomerBranchID")+ "," + Eval("FinancialYear")+ "," + Eval("ForPerid")+","+ Eval("VerticalID") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Records Found.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <asp:HiddenField ID="hdnTitle" runat="server" />
                                        </div>

                                   <div id="ImplementationGrid" runat="server">
                                        <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverageIMP" AutoGenerateColumns="false" 
                                        AllowSorting="true"  CssClass="table" GridLines="None"
                                        AllowPaging="true" PageSize="5" OnPageIndexChanging="grdSummaryDetailsAuditCoverageIMP_PageIndexChanging" 
                                        DataKeyNames="TESTID">
                                        <Columns>

                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                    <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                         </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vertical">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                    <asp:Label ID="lblVertical" runat="server" Text='<%# Eval("VerticalName") %>' data-toggle="tooltip" data-placement="bottom"  ToolTip='<%# Eval("VerticalName") %>'></asp:Label>
                                                        </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="FinancialYear">
                                                <ItemTemplate>
                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                    <asp:Label ID="lblFinancialYear" runat="server" Text='<%# Eval("FinancialYear") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                         </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Period">
                                                <ItemTemplate>
                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                                   <asp:Label ID="lblForPeriod" runat="server" Text='<%# Eval("ForPerid") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ForPerid") %>'></asp:Label>
                                                         </div>
                                                </ItemTemplate>
                                            </asp:TemplateField> 

                                            <asp:TemplateField HeaderText="Actual Work Done" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                        <asp:Label ID="LActivityTobeDone" runat="server" Text='<%# Eval("ActualWorkDone") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ActualWorkDone") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnChangeStatusIMP" runat="server"
                                                        CommandName="CHANGE_STATUS" OnClick="btnChangeStatusIMP_Click" CommandArgument='<%# Eval("ResultID") + "," + Eval("CustomerBranchID")+ "," + Eval("FinancialYear")+ "," + Eval("ForPerid") +","+Eval("VerticalID") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Records Found.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                        <asp:HiddenField ID="hdnTitleIMP" runat="server" />

                                       </div>

                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-6 colpadding0"></div>
                                        <div class="col-md-6 colpadding0">
                                            <div class="table-paging" style="margin-bottom: 20px;">
                                                <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="Previous_Click" />
                                                <div class="table-paging-text">
                                                    <p>
                                                        <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                                    </p>
                                                </div>
                                                <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="Next_Click" />
                                               
                                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>                                          
                        </div>
                        <div style="margin-bottom: 4px">
                            <table width="100%" style="height: 35px;">
                                <tr>
                                    <td>
                                        <div style="margin-bottom: 4px">
                                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>

                    </section>
            </div>
        </div>
    </div>


    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="showdetails" src="about:blank" width="1100px" height="100%" frameborder="0" style="margin-left: 25px;"></iframe>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
