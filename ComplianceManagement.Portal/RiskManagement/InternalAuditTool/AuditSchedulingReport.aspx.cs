﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditSchedulingReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                BindCustomerLocationFilter(customerID);
            }
        }
        //public void BindCustomerEntityFilter()
        //{
        //    int customerID = -1;
        //    customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
        //    ddlEntityFilter.DataTextField = "Name";
        //    ddlEntityFilter.DataValueField = "ID";
        //    ddlEntityFilter.Items.Clear();
        //    ddlEntityFilter.DataSource = UserManagementRisk.FillCustomerName(customerID);
        //    ddlEntityFilter.DataBind();
        //    ddlEntityFilter.Items.Insert(0, new ListItem("< All >", "-1"));
        //    ddlFilterLocation.Items.Clear();
        //    ddlFilterLocation.Items.Insert(0, new ListItem("< All >", "-1"));
        //}
        protected void ddlEntityFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
          
            
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            int customerid = -1;
            int customerbranchid = -1;
            //if (!string.IsNullOrEmpty(ddlEntityFilter.SelectedValue))
            //{
            //    customerid = Convert.ToInt32(ddlEntityFilter.SelectedValue);
            //}
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                customerbranchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                BindGrid(customerbranchid);
            }
            //BindMainGrid(customerid, customerbranchid);
        }
        public void BindCustomerLocationFilter(int customerID)
        {            
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem(" All ", "-1"));
        }
        public void BindGrid(int BranchId)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var query = (from row in entities.AuditSchedulingReportViews
                                 where row.CustomerBranchId==BranchId
                                 select new
                                 {
                                     row.BranchName,
                                     ProcessName = row.Name,
                                     row.FinancialYear,
                                     row.Status,
                                     row.TermName
                                 }).ToList();
                    if (query !=null)
                    {
                        grdTestQ.DataSource = query.ToList();
                        grdTestQ.DataBind();
                    }
                    
                }               
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }  
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                int customerbranchid = -1;
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=AuditSchedulingReport.xls");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    using (StringWriter sw = new StringWriter())
                    {
                        HtmlTextWriter hw = new HtmlTextWriter(sw);
                        //To Export all pages
                        grdTestQ.AllowPaging = false;
                        customerbranchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        BindGrid(customerbranchid);
                        grdTestQ.HeaderRow.BackColor =Color.Blue;
                        foreach (TableCell cell in grdTestQ.HeaderRow.Cells)
                        {
                            cell.BackColor = grdTestQ.HeaderStyle.BackColor;
                        }
                        grdTestQ.RenderControl(hw);
                        string style = @"<style> .textmode { } </style>";
                        Response.Write(style);
                        Response.Output.Write(sw.ToString());
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
    }
}