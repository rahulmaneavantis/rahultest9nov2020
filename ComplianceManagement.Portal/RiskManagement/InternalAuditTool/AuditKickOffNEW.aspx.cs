﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditKickOffNEW : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerLocation();
                BindFinancialYearFilter();
                BindFinancialYearIMPFilter();
                BindLocationFilter();
                Label3.Text = string.Empty;
                BindData(-1, -1, "");
            }
        }
        public void BindCustomerLocation()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLocation.DataTextField = "Name";
            ddlLocation.DataValueField = "ID";
            ddlLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem(" Select Location ", "-1"));
        }
        protected void ddlInternalExternal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlInternalExternal.SelectedItem.Text == "Internal")
            {
                ddlAuditor.Items.Clear();
                ddlAuditor.Items.Insert(0, new ListItem(" Select Auditor ", "-1"));
            }
            else
            {
                BindAuditor();
            }
        }
        protected void ddlFilterFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {

            
            //string b = DateTimeExtensions.GetFinancialYear(DateTime.Today.Date.ToString("dd/MM/yyyy"));
            //string c = DateTimeExtensions.ToFinancialYear(DateTime.Today.Date);
            //string d = DateTimeExtensions.GetCurrentFinancialYear();
            string FinancialYear = string.Empty;
            int customerbranchid = -1;
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
            {
                customerbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
            }
            if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                {
                    FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                }
            }
            BindData(customerID, customerbranchid, FinancialYear);
        }
        protected void upCompliancesList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindAuditor()
        {
            try
            {
                ddlAuditor.Items.Clear();
                ddlAuditor.DataTextField = "Name";
                ddlAuditor.DataValueField = "ID";
                ddlAuditor.DataSource = ProcessManagement.GetAllList();
                ddlAuditor.DataBind();
                ddlAuditor.Items.Insert(0, new ListItem(" Select Auditor ", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindFinancialYearFilter()
        {
            ddlFilterFinancialYear.DataTextField = "Name";
            ddlFilterFinancialYear.DataValueField = "ID";
            ddlFilterFinancialYear.Items.Clear();
            ddlFilterFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancialYear.DataBind();
            //ddlFilterFinancialYear.Items.Insert(0, new ListItem("All", "-1"));
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowPerformerName(long ProcessId, long branchid, long Userid, string FinancialYear)
        {
            string performername = string.Empty;
            string AssignedTo = string.Empty;
            int AuditorID = -1;
            if (ddlInternalExternal.SelectedItem.Text == "Internal")
            {
                AssignedTo = "I";
                List<SP_GETPerformer_Result> a = new List<SP_GETPerformer_Result>();
                a = ProcessManagement.GETSP_GETPerformerProcedure(AssignedTo, Convert.ToInt32(ProcessId), Convert.ToInt32(branchid), Convert.ToInt32(Userid), FinancialYear).ToList();
                var remindersummary = a.OrderBy(entry => entry.Name).ToList().FirstOrDefault();
                if (remindersummary != null)
                {
                    performername = remindersummary.Name;
                }
            }
            else
            {
                AssignedTo = "E";
                if (!string.IsNullOrEmpty(ddlAuditor.SelectedValue))
                {
                    AuditorID = Convert.ToInt32(ddlAuditor.SelectedValue);
                }
                List<SP_GETPerformer_Result> a = new List<SP_GETPerformer_Result>();
                a = ProcessManagement.GETSP_GETPerformerProcedure(AssignedTo, Convert.ToInt32(ProcessId), Convert.ToInt32(branchid), Convert.ToInt32(Userid), FinancialYear).ToList();
                var remindersummary = a.OrderBy(entry => entry.Name).ToList().FirstOrDefault();
                if (remindersummary != null)
                {
                    performername = remindersummary.Name;
                }
            }
            return performername;
        }
        public string ShowReviewerName(long ProcessId, long branchid, long Userid, string FinancialYear)
        {
            string reviewername = string.Empty;
            string AssignedTo = string.Empty;
            int AuditorID = -1;
            if (ddlInternalExternal.SelectedItem.Text == "Internal")
            {
                AssignedTo = "I";


                List<SP_GETReviewer_Result> a = new List<SP_GETReviewer_Result>();
                a = ProcessManagement.GETSP_GETReviewer_ResultProcedure(AssignedTo, Convert.ToInt32(ProcessId), Convert.ToInt32(branchid), Convert.ToInt32(Userid), FinancialYear).ToList();
                var remindersummary = a.OrderBy(entry => entry.Name).ToList().FirstOrDefault();
                if (remindersummary != null)
                {
                    reviewername = remindersummary.Name;
                }
            }
            else
            {
                AssignedTo = "E";
                if (!string.IsNullOrEmpty(ddlAuditor.SelectedValue))
                {
                    AuditorID = Convert.ToInt32(ddlAuditor.SelectedValue);
                }
                List<SP_GETReviewer_Result> a = new List<SP_GETReviewer_Result>();
                a = ProcessManagement.GETSP_GETReviewer_ResultProcedure(AssignedTo, Convert.ToInt32(ProcessId), Convert.ToInt32(branchid), Convert.ToInt32(Userid), FinancialYear).ToList();
                var remindersummary = a.OrderBy(entry => entry.Name).ToList().FirstOrDefault();
                if (remindersummary != null)
                {
                    reviewername = remindersummary.Name;
                }
            }
            return reviewername;
        }
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                TreeNode node = new TreeNode("All", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter1", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter1", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindData(int customerid, int customerbranchid, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (customerid != -1 && customerbranchid != -1 && !string.IsNullOrEmpty(FinancialYear))
                {
                    var RiskActivityToBeDoneMappingGetByName =
                       (from row in entities.InternalAuditSchedulings
                        join row1 in entities.Internal_AuditAssignment
                        on row.CustomerBranchId equals row1.CustomerBranchid
                        join MCB in entities.mst_CustomerBranch
                         on row.CustomerBranchId equals MCB.ID
                        where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerid
                        && row.CustomerBranchId == customerbranchid && row.FinancialYear == FinancialYear
                        select new AuditKickOffDetails()
                        {
                            TermName = row.TermName,
                            CustomerBranchId = row.CustomerBranchId,
                            ISAHQMP = row.ISAHQMP,
                            FinancialYear = row.FinancialYear,
                            AssignedTo = row1.AssignedTo,
                            ExternalAuditorId = row1.ExternalAuditorId,
                            PhaseCount = row.PhaseCount
                        }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();

                    GetPageDisplaySummary();
                    grdCompliances.Visible = true;
                    dvAuditDetails.Visible = true;
                    dvProcessDetails.Visible = false;
                    dvImplstatus.Visible = false;

                    liAudtdetl.Attributes.Add("class", "active");
                    liprocess.Attributes.Add("class", "");
                    liimplSts.Attributes.Add("class", "");

                    dvAuditDetails.Attributes.Remove("class");
                    dvAuditDetails.Attributes.Add("class", "tab-pane active");

                    dvProcessDetails.Attributes.Remove("class");
                    dvProcessDetails.Attributes.Add("class", "tab-pane");
                    dvImplstatus.Attributes.Remove("class");
                    dvImplstatus.Attributes.Add("class", "tab-pane");
                }
                else if (customerid != -1 && customerbranchid != -1)
                {
                    var RiskActivityToBeDoneMappingGetByName =
                           (from row in entities.InternalAuditSchedulings
                            join row1 in entities.Internal_AuditAssignment
                            on row.CustomerBranchId equals row1.CustomerBranchid
                            join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                            where row.TermStatus == true && row.IsDeleted == false
                            && row.CustomerBranchId == customerbranchid
                            && MCB.CustomerID == customerid
                            select new AuditKickOffDetails()
                            {
                                TermName = row.TermName,
                                CustomerBranchId = row.CustomerBranchId,
                                ISAHQMP = row.ISAHQMP,
                                FinancialYear = row.FinancialYear,
                                AssignedTo = row1.AssignedTo,
                                ExternalAuditorId = row1.ExternalAuditorId,
                                PhaseCount = row.PhaseCount
                            }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();

                    GetPageDisplaySummary();
                    grdCompliances.Visible = true;
                    dvAuditDetails.Visible = true;
                    dvProcessDetails.Visible = false;
                    dvImplstatus.Visible = false;

                    liAudtdetl.Attributes.Add("class", "active");
                    liprocess.Attributes.Add("class", "");
                    liimplSts.Attributes.Add("class", "");

                    dvAuditDetails.Attributes.Remove("class");
                    dvAuditDetails.Attributes.Add("class", "tab-pane active");

                    dvProcessDetails.Attributes.Remove("class");
                    dvProcessDetails.Attributes.Add("class", "tab-pane");
                    dvImplstatus.Attributes.Remove("class");
                    dvImplstatus.Attributes.Add("class", "tab-pane");

                }
                else if (customerid != -1 && !string.IsNullOrEmpty(FinancialYear))
                {
                    var RiskActivityToBeDoneMappingGetByName =
                           (from row in entities.InternalAuditSchedulings
                            join row1 in entities.Internal_AuditAssignment
                            on row.CustomerBranchId equals row1.CustomerBranchid
                            join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                            where row.TermStatus == true && row.IsDeleted == false
                            && row.FinancialYear == FinancialYear
                            && MCB.CustomerID == customerid
                            select new AuditKickOffDetails()
                            {
                                TermName = row.TermName,
                                CustomerBranchId = row.CustomerBranchId,
                                ISAHQMP = row.ISAHQMP,
                                FinancialYear = row.FinancialYear,
                                AssignedTo = row1.AssignedTo,
                                ExternalAuditorId = row1.ExternalAuditorId,
                                PhaseCount = row.PhaseCount
                            }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();

                    GetPageDisplaySummary();
                    grdCompliances.Visible = true;
                    dvAuditDetails.Visible = true;
                    dvProcessDetails.Visible = false;
                    dvImplstatus.Visible = false;

                    liAudtdetl.Attributes.Add("class", "active");
                    liprocess.Attributes.Add("class", "");
                    liimplSts.Attributes.Add("class", "");

                    dvAuditDetails.Attributes.Remove("class");
                    dvAuditDetails.Attributes.Add("class", "tab-pane active");

                    dvProcessDetails.Attributes.Remove("class");
                    dvProcessDetails.Attributes.Add("class", "tab-pane");
                    dvImplstatus.Attributes.Remove("class");
                    dvImplstatus.Attributes.Add("class", "tab-pane");

                }
                else if (customerbranchid != -1 && !string.IsNullOrEmpty(FinancialYear))
                {
                    var RiskActivityToBeDoneMappingGetByName =
                           (from row in entities.InternalAuditSchedulings
                            join row1 in entities.Internal_AuditAssignment
                            on row.CustomerBranchId equals row1.CustomerBranchid
                            join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                            where row.TermStatus == true && row.IsDeleted == false
                            && row.CustomerBranchId == customerbranchid
                            && row.FinancialYear == FinancialYear
                            select new AuditKickOffDetails()
                            {
                                TermName = row.TermName,
                                CustomerBranchId = row.CustomerBranchId,
                                ISAHQMP = row.ISAHQMP,
                                FinancialYear = row.FinancialYear,
                                AssignedTo = row1.AssignedTo,
                                ExternalAuditorId = row1.ExternalAuditorId,
                                PhaseCount = row.PhaseCount
                            }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();

                    GetPageDisplaySummary();
                    grdCompliances.Visible = true;
                    dvAuditDetails.Visible = true;
                    dvProcessDetails.Visible = false;
                    dvImplstatus.Visible = false;

                    liAudtdetl.Attributes.Add("class", "active");
                    liprocess.Attributes.Add("class", "");
                    liimplSts.Attributes.Add("class", "");

                    dvAuditDetails.Attributes.Remove("class");
                    dvAuditDetails.Attributes.Add("class", "tab-pane active");

                    dvProcessDetails.Attributes.Remove("class");
                    dvProcessDetails.Attributes.Add("class", "tab-pane");
                    dvImplstatus.Attributes.Remove("class");
                    dvImplstatus.Attributes.Add("class", "tab-pane");

                }
                else if (customerid != -1)
                {
                    var RiskActivityToBeDoneMappingGetByName =
                           (from row in entities.InternalAuditSchedulings
                            join row1 in entities.Internal_AuditAssignment
                            on row.CustomerBranchId equals row1.CustomerBranchid
                            join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                            where row.TermStatus == true && row.IsDeleted == false
                            && MCB.CustomerID == customerid
                            select new AuditKickOffDetails()
                            {
                                TermName = row.TermName,
                                CustomerBranchId = row.CustomerBranchId,
                                ISAHQMP = row.ISAHQMP,
                                FinancialYear = row.FinancialYear,
                                AssignedTo = row1.AssignedTo,
                                ExternalAuditorId = row1.ExternalAuditorId,
                                PhaseCount = row.PhaseCount
                            }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();

                    GetPageDisplaySummary();
                    grdCompliances.Visible = true;
                    dvAuditDetails.Visible = true;
                    dvProcessDetails.Visible = false;
                    dvImplstatus.Visible = false;

                    liAudtdetl.Attributes.Add("class", "active");
                    liprocess.Attributes.Add("class", "");
                    liimplSts.Attributes.Add("class", "");

                    dvAuditDetails.Attributes.Remove("class");
                    dvAuditDetails.Attributes.Add("class", "tab-pane active");

                    dvProcessDetails.Attributes.Remove("class");
                    dvProcessDetails.Attributes.Add("class", "tab-pane");
                    dvImplstatus.Attributes.Remove("class");
                    dvImplstatus.Attributes.Add("class", "tab-pane");

                }
                else if (customerbranchid != -1)
                {
                    var RiskActivityToBeDoneMappingGetByName =
                           (from row in entities.InternalAuditSchedulings
                            join row1 in entities.Internal_AuditAssignment
                            on row.CustomerBranchId equals row1.CustomerBranchid
                            join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                            where row.TermStatus == true && row.IsDeleted == false
                            && row.CustomerBranchId == customerbranchid
                            select new AuditKickOffDetails()
                            {
                                TermName = row.TermName,
                                CustomerBranchId = row.CustomerBranchId,
                                ISAHQMP = row.ISAHQMP,
                                FinancialYear = row.FinancialYear,
                                AssignedTo = row1.AssignedTo,
                                ExternalAuditorId = row1.ExternalAuditorId,
                                PhaseCount = row.PhaseCount
                            }).Distinct().ToList();

                    grdCompliances.DataSource = null;
                    grdCompliances.DataBind();

                    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                    Session["TotalRows"] = RiskActivityToBeDoneMappingGetByName.Count;
                    grdCompliances.DataBind();

                    GetPageDisplaySummary();
                    grdCompliances.Visible = true;
                    dvAuditDetails.Visible = true;
                    dvProcessDetails.Visible = false;
                    dvImplstatus.Visible = false;

                    liAudtdetl.Attributes.Add("class", "active");
                    liprocess.Attributes.Add("class", "");
                    liimplSts.Attributes.Add("class", "");

                    dvAuditDetails.Attributes.Remove("class");
                    dvAuditDetails.Attributes.Add("class", "tab-pane active");

                    dvProcessDetails.Attributes.Remove("class");
                    dvProcessDetails.Attributes.Add("class", "tab-pane");
                    dvImplstatus.Attributes.Remove("class");
                    dvImplstatus.Attributes.Add("class", "tab-pane");

                }
                //else
                //{

                //    int customerID = -1;
                //    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                //    var RiskActivityToBeDoneMappingGetByName =
                //              (from row in entities.InternalAuditSchedulings
                //               join row1 in entities.Internal_AuditAssignment
                //               on row.CustomerBranchId equals row1.CustomerBranchid
                //               join MCB in entities.mst_CustomerBranch
                //                on row.CustomerBranchId equals MCB.ID
                //               where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerID
                //               select new AuditKickOffDetails()
                //               {
                //                   TermName = row.TermName,
                //                   CustomerBranchId = row.CustomerBranchId,
                //                   ISAHQMP = row.ISAHQMP,
                //                   FinancialYear = row.FinancialYear,
                //                   AssignedTo = row1.AssignedTo,
                //                   ExternalAuditorId = row1.ExternalAuditorId,
                //                   PhaseCount = row.PhaseCount
                //               }).Distinct().ToList();

                //    grdCompliances.DataSource = null;
                //    grdCompliances.DataBind();

                //    grdCompliances.DataSource = RiskActivityToBeDoneMappingGetByName.ToList();
                //    grdCompliances.DataBind();
                //}
            }
        }
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdRiskActivityMatrix.PageIndex = e.NewPageIndex;            
            ddlAuditor_SelectedIndexChanged(sender, e);
        }
        protected void grdRiskActivityMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {               
                if (ddlInternalExternal.SelectedItem.Text == "Internal")
                {
                    int customerID = -1;
                    //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                    if (ddlPerformer != null)
                    {

                        ddlPerformer.Items.Clear();
                        ddlPerformer.DataTextField = "Name";
                        ddlPerformer.DataValueField = "ID";
                        ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(Portal.Common.AuthenticationHelper.CustomerID, Convert.ToInt32(Portal.Common.AuthenticationHelper.ServiceProviderID));
                        ddlPerformer.DataBind();
                        ddlPerformer.Items.Insert(0, new ListItem(" Select Performer ", "-1"));
                    }
                    DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                    if (ddlReviewer != null)
                    {

                        ddlReviewer.Items.Clear();
                        ddlReviewer.DataTextField = "Name";
                        ddlReviewer.DataValueField = "ID";
                        ddlReviewer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(Portal.Common.AuthenticationHelper.CustomerID, Convert.ToInt32(Portal.Common.AuthenticationHelper.ServiceProviderID));
                        ddlReviewer.DataBind();
                        ddlReviewer.Items.Insert(0, new ListItem(" Select Reviewer ", "-1"));
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlAuditor.SelectedValue) != -1)
                    {
                        int customerID = -1;
                        //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                        if (ddlPerformer != null)
                        {
                            ddlPerformer.Items.Clear();
                            ddlPerformer.DataTextField = "Name";
                            ddlPerformer.DataValueField = "ID";
                            ddlPerformer.DataSource = RiskCategoryManagement.FillAuditusers(customerID, Convert.ToInt32(ddlAuditor.SelectedValue));
                            ddlPerformer.DataBind();
                            ddlPerformer.Items.Insert(0, new ListItem(" Select Performer ", "-1"));
                        }
                        DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                        if (ddlReviewer != null)
                        {
                            ddlReviewer.Items.Clear();
                            ddlReviewer.DataTextField = "Name";
                            ddlReviewer.DataValueField = "ID";
                            ddlReviewer.DataSource = RiskCategoryManagement.FillAuditusers(customerID, Convert.ToInt32(ddlAuditor.SelectedValue));
                            ddlReviewer.DataBind();
                            ddlReviewer.Items.Insert(0, new ListItem(" Select Reviewer ", "-1"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlAuditor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPeriod.SelectedValue != "Select Period")
            {
                int branchid = -1;
                string FinancialYear = string.Empty;
                string Formonth = string.Empty;
                branchid = Convert.ToInt32(ddlLocation.SelectedValue);
                FinancialYear = ddlFinancialYear.SelectedItem.Text;

                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    Formonth = "Annually";
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {

                    Formonth = ddlPeriod.SelectedItem.Text;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    Formonth = ddlPeriod.SelectedItem.Text;

                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {

                    Formonth = ddlPeriod.SelectedItem.Text;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    Formonth = ddlPeriod.SelectedItem.Text;
                }
                List<SP_FillInternalAuditAssignment_Result> a = new List<SP_FillInternalAuditAssignment_Result>();
                a = ProcessManagement.GetSP_FillInternalAuditAssignment_ResultProcedure(Convert.ToInt32(ViewState["StartEndDateProcessid"]), branchid, FinancialYear, Formonth).ToList();
                var remindersummary = a.OrderBy(entry => entry.ProcessId).ToList();
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                grdRiskActivityMatrix.DataSource = remindersummary;
                grdRiskActivityMatrix.DataBind();
            }

        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFinancialYear.SelectedItem.Text != "Select Fnancial Year")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {

                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {

                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {

                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {

                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {

                }
            }
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, "Select Period");
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem(" Select Fnancial Year ", "-1"));
        }
        public void BindSchedulingType()
        {
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingType();
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }
        private void OpenScheduleInformation(string ISAHQMP, int branchid, string FinancialYear, string AssignedTo, string ExternalAuditorId, string PhaseCount, string ForMonth)
        {
            try
            {
                BindSchedulingType();
                BindFinancialYear();
                string Formonth = string.Empty;
                if (branchid != -1)
                {
                    ddlLocation.SelectedValue = Convert.ToString(branchid);
                }
                ddlFinancialYear.SelectedItem.Text = Convert.ToString(FinancialYear);
                if (AssignedTo == "I")
                {
                    ddlInternalExternal.SelectedItem.Text = "Internal";
                    DivIsExternal.Visible = false;
                }
                else
                {
                    DivIsExternal.Visible = true;
                    ddlInternalExternal.SelectedItem.Text = "External";
                    ddlInternalExternal_SelectedIndexChanged(null, null);
                    ddlAuditor.SelectedValue = Convert.ToString(ExternalAuditorId);
                }
                if (Convert.ToString(ISAHQMP) == "A")
                {
                    Formonth = "Annually";
                    ddlSchedulingType.SelectedItem.Text = "Annually";
                    ddlSchedulingType_SelectedIndexChanged(null, null);
                    ddlPeriod.SelectedItem.Text = ForMonth;
                }
                else if (Convert.ToString(ISAHQMP) == "H")
                {
                    ddlSchedulingType.SelectedItem.Text = "Half Yearly";
                    ddlSchedulingType_SelectedIndexChanged(null, null);
                    ddlPeriod.SelectedItem.Text = ForMonth;
                }
                else if (Convert.ToString(ISAHQMP) == "Q")
                {
                    ddlSchedulingType.SelectedItem.Text = "Quarterly";
                    ddlSchedulingType_SelectedIndexChanged(null, null);
                    ddlPeriod.SelectedItem.Text = ForMonth;
                }
                else if (Convert.ToString(ISAHQMP) == "M")
                {
                    ddlSchedulingType.SelectedItem.Text = "Monthly";
                    ddlSchedulingType_SelectedIndexChanged(null, null);
                    ddlPeriod.SelectedItem.Text = ForMonth;
                }
                else if (Convert.ToString(ISAHQMP) == "P")
                {
                    ddlSchedulingType.SelectedItem.Text = "Phase";
                    BindAuditSchedule("P", Convert.ToInt32(PhaseCount));
                    ddlPeriod.SelectedItem.Text = ForMonth;
                }
                ViewState["Mode"] = 0;
                ViewState["ComplianceParameters"] = null;
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                //UserAccordion1.SelectedIndex = 1;
                BindRsikActivityGrid();
                BindTEMPGrid();
                BindTEMPGridSaved();

                BindImplementationGrid(Convert.ToString(ISAHQMP), ForMonth);
                BindTEMPImplementationGrid();
                BindTEMPImplementationGridSaved();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindTEMPGrid()
        {
            int branchid = -1;
            string FinancialYear = string.Empty;
            string Formonth = string.Empty;
            branchid = Convert.ToInt32(ddlLocation.SelectedValue);
            FinancialYear = ddlFinancialYear.SelectedItem.Text;

            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                Formonth = "Annually";
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {

                Formonth = ddlPeriod.SelectedItem.Text;
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                Formonth = ddlPeriod.SelectedItem.Text;

            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {

                Formonth = ddlPeriod.SelectedItem.Text;
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                Formonth = ddlPeriod.SelectedItem.Text;
            }
            List<SP_FillTMP_Assignment_Result> a = new List<SP_FillTMP_Assignment_Result>();
            a = ProcessManagement.GetSP_FillTMP_Assignment_ResultProcedure(branchid, FinancialYear, Formonth).ToList();
            var remindersummary = a.OrderBy(entry => entry.ProcessId).ToList();
            grdActualAssignmentSave.DataSource = null;
            grdActualAssignmentSave.DataBind();
            grdActualAssignmentSave.DataSource = remindersummary;
            grdActualAssignmentSave.DataBind();
        }
      
        public void BindTEMPGridSaved()
        {
            int branchid = -1;
            string FinancialYear = string.Empty;
            string Formonth = string.Empty;
            branchid = Convert.ToInt32(ddlLocation.SelectedValue);
            FinancialYear = ddlFinancialYear.SelectedItem.Text;

            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                Formonth = "Annually";
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {

                Formonth = ddlPeriod.SelectedItem.Text;
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                Formonth = ddlPeriod.SelectedItem.Text;

            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {

                Formonth = ddlPeriod.SelectedItem.Text;
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                Formonth = ddlPeriod.SelectedItem.Text;
            }
            List<SP_FillTMP_Assignment_Saved_Result> a = new List<SP_FillTMP_Assignment_Saved_Result>();
            a = ProcessManagement.GetSP_FillTMP_Assignment_Saved_ResultProcedure(branchid, FinancialYear, Formonth).ToList();
            var remindersummary = a.OrderBy(entry => entry.ProcessId).ToList();
            grdActualAssignmentSaved.DataSource = null;
            grdActualAssignmentSaved.DataBind();
            grdActualAssignmentSaved.DataSource = remindersummary;
            grdActualAssignmentSaved.DataBind();
        }
        public void BindRsikActivityGrid()
        {
            int branchid = -1;
            string FinancialYear = string.Empty;
            string Formonth = string.Empty;
            branchid = Convert.ToInt32(ddlLocation.SelectedValue);
            FinancialYear = ddlFinancialYear.SelectedItem.Text;

            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                Formonth = "Annually";
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {

                Formonth = ddlPeriod.SelectedItem.Text;
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                Formonth = ddlPeriod.SelectedItem.Text;

            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {

                Formonth = ddlPeriod.SelectedItem.Text;
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                Formonth = ddlPeriod.SelectedItem.Text;
            }
            List<SP_FillInternalAuditAssignmentNew_Result> a = new List<SP_FillInternalAuditAssignmentNew_Result>();
            a = ProcessManagement.GetSP_FillInternalAuditAssignmentNEW_ResultProcedure(branchid, FinancialYear, Formonth).ToList();
            var remindersummary = a.OrderBy(entry => entry.ProcessId).ToList();
            grdRiskActivityMatrix.DataSource = null;
            grdRiskActivityMatrix.DataBind();
            grdRiskActivityMatrix.DataSource = remindersummary;
            grdRiskActivityMatrix.DataBind();
        }
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;

                string FinancialYear = string.Empty;
                int customerbranchid = -1;
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    customerbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                string getCurrentFinancialYear = DateTimeExtensions.GetCurrentFinancialYear();

                if (getCurrentFinancialYear !=null)
                {
                    ddlFilterFinancialYear.SelectedItem.Text = getCurrentFinancialYear;
                    ddlFilterFinancialYear_SelectedIndexChanged(sender, e);
                }
                if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
                {
                    if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                    {
                        FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                    }
                }
                BindData(customerID, customerbranchid, FinancialYear);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static PerformerSendMailView ProcessPerformerSendMailReminder(int ProcessId,int CustomerId)
        {
         
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var CheckListReminders = (from row in entities.PerformerSendMailViews

                                          where row.CustomerID==CustomerId && row.RoleID == 3
                                          && row.ProcessId == ProcessId
                                          select row).FirstOrDefault();

                return CheckListReminders;
            }

        }
        //private void ProcessPerformerSendMailReminders(int ProcessId, int Customerid)
        //{
        //    try
        //    {
        //        List<ProcessPerformerReminderNotification> ReminderUserList = new List<ProcessPerformerReminderNotification>();
        //        List<int> updatedCheckListReminders = new List<int>();
        //        PerformerSendMailView CheckListRemindersList = new PerformerSendMailView();
        //        CheckListRemindersList = ProcessPerformerSendMailReminder(ProcessId, Customerid);
        //        if (CheckListRemindersList != null)
        //        {                   
        //            string ReplyEmailAddressName = CustomerManagement.GetByID(Customerid).Name;

        //            var EscalationEmail = Properties.Settings.Default.EmailTemplate_AuditPerformersAndAuditor
        //                                           .Replace("@User", CheckListRemindersList.UserName)
        //                                           .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
        //                                           .Replace("@From", ReplyEmailAddressName);
        //            StringBuilder details = new StringBuilder();
        //            bool chelexists = Exists(Convert.ToInt32(CheckListRemindersList.CustomerBranchID), Convert.ToInt32(CheckListRemindersList.ProcessId), Convert.ToInt32(CheckListRemindersList.UserID));
        //            if (chelexists == false)
        //            {
        //                details.AppendLine(Properties.Settings.Default.EmailTemplate_AuditPerformersAndAuditorRows
        //                  .Replace("@ProcessName", CheckListRemindersList.ProcessName)
        //                  .Replace("@Branch", CheckListRemindersList.CustomerBranchName)
        //                  .Replace("@FinancialYear", CheckListRemindersList.FinancialYear)
        //                  .Replace("@ForMonth", CheckListRemindersList.ForMonth));

        //                ReminderUserList.Add(new ProcessPerformerReminderNotification() { UserID = CheckListRemindersList.UserID, SentOn = DateTime.Now, ProcessID = Convert.ToInt32(CheckListRemindersList.ProcessId), CustomerBranchId = CheckListRemindersList.CustomerBranchID });                      
        //                string message = EscalationEmail.Replace("@User", "User").Replace("@Details", details.ToString());
        //                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { CheckListRemindersList.Email }).ToList(), null, null, "AVACOM Reminder: List of 'Process' assigned.", message);
        //            }                   
        //            AddProcessPerformerReminderNotification(ReminderUserList);                  
        //        }                
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        public static bool Exists(int CustomerBranchID, int ProcessId, int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (CustomerBranchID != -1 && ProcessId != -1 && userid != -1)
                {
                    var query = (from row in entities.ProcessPerformerReminderNotifications
                                 where row.ProcessID == ProcessId
                                     && row.CustomerBranchId == CustomerBranchID
                                     && row.UserID == userid
                                 select row);
                    return query.Select(entry => true).SingleOrDefault();
                }
                else
                {
                    return false;
                }
            }
        }
        public static void AddProcessPerformerReminderNotification(List<ProcessPerformerReminderNotification> objEscalation)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    objEscalation.ForEach(entry =>
                    {
                        bool chelexists = Exists(Convert.ToInt32(entry.CustomerBranchId), Convert.ToInt32(entry.ProcessID), Convert.ToInt32(entry.UserID));
                        if (chelexists == false)
                        {
                            entities.ProcessPerformerReminderNotifications.Add(entry);
                            entities.SaveChanges();
                        }

                    });
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlPeriod.SelectedValue == "Select Period")
                {
                    cvDuplicateEntry1.ErrorMessage = "Select Period";
                    return;
                }

                foreach (GridViewRow g1 in grdActualAssignmentSave.Rows)
                {
                    List<InternalControlAuditAssignment> Tempassignments = new List<InternalControlAuditAssignment>();
                    string lblItemTemplateID = (g1.FindControl("lblItemTemplateID") as Label).Text;
                    string lblItemTemplateProcess = (g1.FindControl("lblItemTemplateProcess") as Label).Text;
                    string lblPerformerUserID = (g1.FindControl("lblPerformerUserID") as Label).Text;
                    string lblReviewerUserID = (g1.FindControl("lblReviewerUserID") as Label).Text;


                    string AssignedTo = string.Empty;
                    if (ddlInternalExternal.SelectedItem.Text == "Internal")
                    {
                        AssignedTo = "I";
                    }
                    else
                    {
                        AssignedTo = "E";
                    }
                    var startdateenddate = CustomerBranchManagementRisk.GetByID(Convert.ToInt32(lblItemTemplateID), Convert.ToInt32(ddlLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedItem.Text, Convert.ToInt32(lblPerformerUserID));
                    if (startdateenddate != null)
                    {
                        InternalAuditInstance riskinstance = new InternalAuditInstance();
                        riskinstance.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                        riskinstance.IsDeleted = false;
                        riskinstance.CreatedOn = DateTime.Now;
                        riskinstance.ProcessId = Convert.ToInt32(lblItemTemplateID);
                        riskinstance.SubProcessId = -1;


                        DateTime a = DateTime.ParseExact(startdateenddate.ExpectedStartDate.ToString("dd-MM-yyyy").Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime b = DateTime.ParseExact(startdateenddate.ExpectedEndDate.ToString("dd-MM-yyyy").Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        UserManagementRisk.AddDetailsInternalAuditInstances(riskinstance);

                        if (lblPerformerUserID != null)
                        {
                            InternalControlAuditAssignment TempAssP = new InternalControlAuditAssignment();
                            TempAssP.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                            TempAssP.RoleID = RoleManagement.GetByCode("PERF").ID;
                            TempAssP.UserID = Convert.ToInt32(lblPerformerUserID);
                            TempAssP.IsActive = true;
                            TempAssP.CreatedOn = DateTime.Now;
                            TempAssP.ProcessId = Convert.ToInt32(lblItemTemplateID);
                            TempAssP.SubProcessId = -1;
                            TempAssP.ISInternalExternal = AssignedTo;
                            TempAssP.InternalAuditInstance = riskinstance.ID;
                            TempAssP.ExpectedStartDate = GetDate(a.ToString("dd-MM-yyyy"));
                            TempAssP.ExpectedEndDate = GetDate(b.ToString("dd-MM-yyyy"));

                            Tempassignments.Add(TempAssP);
                        }
                        if (lblReviewerUserID != null)
                        {
                            InternalControlAuditAssignment TempAssR = new InternalControlAuditAssignment();
                            TempAssR.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                            TempAssR.RoleID = RoleManagement.GetByCode("RVW1").ID;
                            TempAssR.UserID = Convert.ToInt32(lblReviewerUserID);
                            TempAssR.IsActive = true;
                            TempAssR.CreatedOn = DateTime.Now;
                            TempAssR.ProcessId = Convert.ToInt32(lblItemTemplateID);
                            TempAssR.SubProcessId = -1;
                            TempAssR.ISInternalExternal = AssignedTo;
                            TempAssR.InternalAuditInstance = riskinstance.ID;
                            TempAssR.ExpectedStartDate = GetDate(a.ToString("dd-MM-yyyy"));
                            TempAssR.ExpectedEndDate = GetDate(b.ToString("dd-MM-yyyy"));
                            Tempassignments.Add(TempAssR);
                        }

                        if (Tempassignments.Count != 0)
                        {
                            UserManagementRisk.AddDetailsInternalControlAuditAssignment(Tempassignments);
                        }
                        CreateScheduleOn(riskinstance.ID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User, ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(ddlLocation.SelectedValue), ddlPeriod.SelectedItem.Text, Convert.ToInt32(lblItemTemplateID));
                        TMP_Assignment mstauditorusermaster = new TMP_Assignment()
                        {
                            CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue),
                            FinancialYear = ddlFinancialYear.SelectedItem.Text,
                            ProcessId = Convert.ToInt32(lblItemTemplateID),
                            ForPeriod = ddlPeriod.SelectedItem.Text,
                            Status = 1,
                        };
                        ProcessManagement.UpdateTMP_Assignment(mstauditorusermaster);
                        BindTEMPGrid();
                        BindTEMPGridSaved();

                        int customerID = -1;
                        //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        //ProcessPerformerSendMailReminders(Convert.ToInt32(lblItemTemplateID),customerID);
                        ddlPeriod.SelectedIndex = 0;
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Save Sucessfully..";
                    }
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void CreateTransaction(List<InternalAuditTransaction> transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.InternalAuditTransactions.Add(entry);
                });

                entities.SaveChanges();

            }
        }
        public static void CreateTransactionImplementation(List<AuditImplementationTransaction> transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.AuditImplementationTransactions.Add(entry);
                });

                entities.SaveChanges();

            }
        }
        public static void CreateScheduleOn(long InternalAuditInstance, long createdByID, string creatdByName, string FinancialYear, long branchid, string Period, long Processid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    DateTime curruntDate = DateTime.UtcNow;

                    List<InternalAuditTransaction> transactionlist = new List<InternalAuditTransaction>();

                    InternalAuditScheduleOn auditScheduleon = new InternalAuditScheduleOn();
                    auditScheduleon.InternalAuditInstance = InternalAuditInstance;
                    auditScheduleon.ForMonth = Period;
                    auditScheduleon.FinancialYear = FinancialYear;
                    auditScheduleon.ProcessId = Processid;
                    entities.InternalAuditScheduleOns.Add(auditScheduleon);
                    entities.SaveChanges();

                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        InternalAuditInstance = InternalAuditInstance,
                        AuditScheduleOnID = auditScheduleon.ID,
                        CreatedBy = createdByID,
                        CreatedByText = creatdByName,
                        ProcessId = Processid,
                        StatusId = 1,
                        CustomerBranchId = branchid,
                        Remarks = "New Audit Steps assigned."

                    };

                    transactionlist.Add(transaction);

                    CreateTransaction(transactionlist);

                }
            }
            catch (Exception ex)
            {
                //List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.LogMessage_Risk> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.LogMessage_Risk>();
                //LogMessage msg = new LogMessage();
                //msg.ClassName = "ComplianceManagement";
                //msg.FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;
                //msg.CreatedOn = DateTime.Now;
                //msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //msg.StackTrace = ex.StackTrace;
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                //ReminderUserList.Add(msg);
                //InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("rahulmane3192@gmail.com");
                //TO.Add("manishajalnekar07@gmail.com");


                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                //    "Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

            }
        }

        public static void CreateScheduleOnImplementation(long ImplementationInstance, long createdByID, string creatdByName, string FinancialYear, long branchid, string Period)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    DateTime curruntDate = DateTime.UtcNow;

                    List<AuditImplementationTransaction> transactionlist = new List<AuditImplementationTransaction>();

                    AuditImpementationScheduleOn auditScheduleon = new AuditImpementationScheduleOn();
                    auditScheduleon.ImplementationInstance = ImplementationInstance;
                    auditScheduleon.ForMonth = Period;
                    auditScheduleon.FinancialYear = FinancialYear;
                    entities.AuditImpementationScheduleOns.Add(auditScheduleon);
                    entities.SaveChanges();

                    AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                    {
                        ImplementationInstance = ImplementationInstance,
                        ImplementationScheduleOnID = auditScheduleon.Id,
                        CreatedBy = createdByID,
                        CreatedByText = creatdByName,
                        ForPeriod = Period,
                        FinancialYear = FinancialYear,
                        StatusId = 1,
                        CustomerBranchId = branchid,
                        Remarks = "New Implementation Steps assigned."

                    };

                    transactionlist.Add(transaction);

                    CreateTransactionImplementation(transactionlist);

                }
            }
            catch (Exception ex)
            {
                //List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.LogMessage_Risk> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.LogMessage_Risk>();
                //LogMessage msg = new LogMessage();
                //msg.ClassName = "ComplianceManagement";
                //msg.FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;
                //msg.CreatedOn = DateTime.Now;
                //msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //msg.StackTrace = ex.StackTrace;
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                //ReminderUserList.Add(msg);
                //InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("rahulmane3192@gmail.com");
                //TO.Add("manishajalnekar07@gmail.com");


                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                //    "Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static bool Exists(long performerid,long reviewerID, int ProcessId, int CustomerBranchID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.TMP_Assignment
                             where row.PUserId == performerid && row.ProcessId == ProcessId
                             && row.RUserId == reviewerID
                             && row.CustomerBranchID == CustomerBranchID
                             && row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckPerformerExists(long userid, int ProcessId, int CustomerBranchID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.TMP_Assignment
                             where row.PUserId == userid && row.ProcessId == ProcessId
                             && row.CustomerBranchID == CustomerBranchID
                             && row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static bool CheckReviewerExists(long userid, int ProcessId, int CustomerBranchID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.TMP_Assignment
                             where row.RUserId == userid && row.ProcessId == ProcessId
                             && row.CustomerBranchID == CustomerBranchID
                             && row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static TMP_Assignment GETTMPAssignment(long userid, int ProcessId, int CustomerBranchID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.TMP_Assignment
                             where row.RUserId == userid && row.ProcessId == ProcessId
                             && row.CustomerBranchID == CustomerBranchID
                             && row.FinancialYear == FinancialYear
                             select row).FirstOrDefault();
                return query;

            }
        }
        protected void Add(object sender, EventArgs e)
        {
            try
            {
                Label3.Text = string.Empty;
                if (ddlPeriod.SelectedValue == "Select Period")
                {
                    cvDuplicateEntry1.ErrorMessage = "Select Period";
                    return;
                }
                var closeLink = (Control)sender;
                GridViewRow row = (GridViewRow)closeLink.NamingContainer;
                string lblFooterTemplateProcessID = (row.FindControl("lblItemTemplateID") as Label).Text;
                string ddlPerformer = (row.FindControl("ddlPerformer") as DropDownList).SelectedItem.Value;
                string ddlReviewer = (row.FindControl("ddlReviewer") as DropDownList).SelectedItem.Value;
                string AssignedTo = string.Empty;
                if (ddlInternalExternal.SelectedItem.Text == "Internal")
                {
                    AssignedTo = "I";
                }
                else
                {
                    AssignedTo = "E";
                }
                if (ddlPerformer != "-1")
                {
                    if (ddlReviewer != "-1")
                    {
                        if (ddlPerformer != ddlReviewer)
                        {
                            bool CheckExists = Exists(Convert.ToInt32(ddlPerformer), Convert.ToInt32(ddlReviewer), Convert.ToInt32(lblFooterTemplateProcessID), Convert.ToInt32(ddlLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text);
                            if (CheckExists == false)
                            {
                                bool checkexistsreviewer = CheckReviewerExists(Convert.ToInt32(ddlPerformer), Convert.ToInt32(lblFooterTemplateProcessID), Convert.ToInt32(ddlLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text);
                                if (checkexistsreviewer == false)
                                {
                                    bool checkexistsperformer = CheckPerformerExists(Convert.ToInt32(ddlReviewer), Convert.ToInt32(lblFooterTemplateProcessID), Convert.ToInt32(ddlLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text);
                                    if (checkexistsperformer == false)
                                    {

                                        List<TMP_Assignment> Tempassignments = new List<TMP_Assignment>();
                                        DateTime a = DateTime.ParseExact(tbxLoanStartDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        DateTime b = DateTime.ParseExact(tbxLoanEndDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        if (ddlPerformer != null && ddlReviewer != null)
                                        {
                                            TMP_Assignment TempAssP = new TMP_Assignment();
                                            TempAssP.FinancialYear = Convert.ToString(ddlFinancialYear.SelectedItem.Text);
                                            TempAssP.PUserId = Convert.ToInt32(ddlPerformer);
                                            TempAssP.RUserId = Convert.ToInt32(ddlReviewer);
                                            TempAssP.Status = 0;
                                            TempAssP.ProcessId = Convert.ToInt32(lblFooterTemplateProcessID);
                                            TempAssP.ISInternalExternal = AssignedTo;
                                            TempAssP.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                                            TempAssP.ExpectedStartDate = GetDate(a.ToString("dd-MM-yyyy"));
                                            TempAssP.ExpectedEndDate = GetDate(b.ToString("dd-MM-yyyy"));
                                            TempAssP.ForPeriod = ddlPeriod.SelectedItem.Text;
                                            Tempassignments.Add(TempAssP);
                                        }

                                        if (Tempassignments.Count != 0)
                                        {
                                            UserManagementRisk.AddDetailsTMPAssignments(Tempassignments);
                                        }
                                        ddlPeriod.SelectedIndex = 0;
                                        BindTEMPGrid();
                                        grdActualAssignmentSaved.DataSource = null;
                                        grdActualAssignmentSaved.DataBind();
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Save Sucessfully..";
                                    }
                                    else
                                    {
                                        Label3.Text = string.Empty;
                                        Label3.Text = "Reviewer Should not be Performer..";
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Reviewer Should not be Performer..";
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:LblClearValue()", true);
                                    }
                                }
                                else
                                {
                                    Label3.Text = string.Empty;
                                    Label3.Text = "Performer Should not be Reviewer..";
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Performer Should not be Reviewer..";
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:LblClearValue()", true);
                                }
                            }
                            else
                            {
                                Label3.Text = string.Empty;
                                Label3.Text = "Performer And Reviewer all ready Exists..";
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Performer And Reviewer all ready Exists..";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:LblClearValue()", true);
                            }
                        }
                        else
                        {
                            Label3.Text = string.Empty;
                            Label3.Text = "Performer And Reviewer Cannot be Same..";
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Performer And Reviewer Cannot be Same..";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:LblClearValue()", true);
                        }
                    }
                    else
                    {
                        Label3.Text = string.Empty;
                        Label3.Text = "Please Select Reviewer..";
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Reviewer..";
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:LblClearValue()", true);
                    }
                }
                else
                {
                    Label3.Text = string.Empty;
                    Label3.Text = "Please Select Performer..";
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Performer..";
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:LblClearValue()", true);
                }
            }
            catch (Exception ex)
            {
                Label3.Text = string.Empty;
                Label3.Text = "Server Error Occured. Please try again.";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowCustomerBranchName(long id)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.Status == 1
                             && row.IsDeleted == false
                             && row.ID == id
                             select row).FirstOrDefault();

                processnonprocess = query.Name;
            }
            return processnonprocess.Trim(',');
        }
        public string ShowStatus(string ISAHQMP)
        {
            string processnonprocess = "";
            if (Convert.ToString(ISAHQMP) == "A")
            {
                processnonprocess = "Annually";
            }
            else if (Convert.ToString(ISAHQMP) == "H")
            {
                processnonprocess = "Half Yearly";
            }
            else if (Convert.ToString(ISAHQMP) == "Q")
            {
                processnonprocess = "Quarterly";
            }
            else if (Convert.ToString(ISAHQMP) == "M")
            {
                processnonprocess = "Monthly";
            }
            else if (Convert.ToString(ISAHQMP) == "P")
            {
                processnonprocess = "Phase";
            }

            return processnonprocess.Trim(',');
        }
        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string isAHQMP = string.Empty;
                string FinancialYear = string.Empty;
                int CustomerBranchId = -1;
                string Termname = string.Empty;
                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    tbxLoanStartDate.Text = string.Empty;
                    tbxLoanEndDate.Text = string.Empty;

                    GridViewRow gvRow = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    Int32 rowind = gvRow.RowIndex;

                    Label lblLocation = (Label)gvRow.FindControl("lblLocation");

                    ViewState["Location"] = lblLocation.Text;

                    Label lblFinancialYear = (Label)gvRow.FindControl("lblFinancialYear");
                    Label lblAssignedTo = (Label)gvRow.FindControl("lblAssignedTo");
                    Label lblExternalAuditorId = (Label)gvRow.FindControl("lblExternalAuditorId");
                    Label lblPhaseCount = (Label)gvRow.FindControl("lblPhaseCount");
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    if (commandArgs.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(commandArgs[0]))
                        {
                            isAHQMP = Convert.ToString(commandArgs[0]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[1]))
                        {
                            FinancialYear = Convert.ToString(commandArgs[1]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[2]))
                        {
                            CustomerBranchId = Convert.ToInt32(commandArgs[2]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[3]))
                        {
                            Termname = Convert.ToString(commandArgs[3]);
                        }
                        ViewState["BranchID"] = CustomerBranchId;
                        OpenScheduleInformation(isAHQMP, CustomerBranchId, lblFinancialYear.Text, lblAssignedTo.Text, lblExternalAuditorId.Text, lblPhaseCount.Text, Termname);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdCompliances.PageIndex = e.NewPageIndex;
            string FinancialYear = string.Empty;
            int customerbranchid = -1;
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
            {
                customerbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
            }
            if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                {
                    FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                }
            }
            BindData(customerID, customerbranchid, FinancialYear);
        }

        #region Implementation Status
        public static bool ExistsImplementation(long performerid, long reviewerID, string ForPeriod, int CustomerBranchID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.TMP_ImplementationAssignment
                             where row.PUserId == performerid && row.RUserId == reviewerID && row.ForPeriod == ForPeriod                             
                             && row.CustomerBranchID == CustomerBranchID
                             && row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void BindTEMPImplementationGrid()
        {
            int branchid = -1;
            string FinancialYear = string.Empty;
            string Formonth = string.Empty;
            branchid = Convert.ToInt32(ddlLocation.SelectedValue);
            FinancialYear = ddlFinancialYear.SelectedItem.Text;
            List<SP_TMP_ImplementationAssignment_Result> a = new List<SP_TMP_ImplementationAssignment_Result>();
            a = ProcessManagement.GetSP_TMP_ImplementationAssignment_ResultProcedure(branchid, FinancialYear).ToList();
            var remindersummary = a.OrderBy(entry => entry.ForPeriod).ToList();
            grdImplementationStatusSave.DataSource = null;
            grdImplementationStatusSave.DataBind();
            grdImplementationStatusSave.DataSource = remindersummary;
            grdImplementationStatusSave.DataBind();
        }
        public int GetQuarter(DateTime date)
        {
            if (date.Month >= 4 && date.Month <= 6)
                return 1;
            else if (date.Month >= 7 && date.Month <= 9)
                return 2;
            else if (date.Month >= 10 && date.Month <= 12)
                return 3;
            else
                return 4;

        }


        public string GetQuarterName(int number)
        {
            string quartername = "";
            if (number == 1)
            {
                quartername = "Apr - Jun";
            }
            else if (number == 2)
            {
                quartername = "Jul - Sep";
            }
            else if (number == 3)
            {
                quartername = "Oct - Dec";
            }
            else if (number == 4)
            {
                quartername = "Jan - Mar";
            }
            return quartername;
        }
        public void BindImplementationGrid(string flag, string ForPeriod)
        {
            int branchid = -1;
            string FinancialYear = string.Empty;
            string Formonth = string.Empty;
            branchid = Convert.ToInt32(ddlLocation.SelectedValue);
            FinancialYear = ddlFinancialYear.SelectedItem.Text;
            //if (flag == "Q")
            //{
            //    int getvalue = GetQuarter(DateTime.Today.Date);
            //    string getqname = GetQuarterName(getvalue);
            //}
            //else
            //{ 
            
            //}

           
            List<Sp_ImplementationStatus_Result> a = new List<Sp_ImplementationStatus_Result>();
            a = ProcessManagement.GetSp_ImplementationStatus_ResultProcedure(branchid, FinancialYear, ForPeriod).ToList();
            var remindersummary = a.OrderBy(entry => entry.ForPeriod).ToList();
            grdImplementationStatus.DataSource = null;
            grdImplementationStatus.DataBind();
            grdImplementationStatus.DataSource = remindersummary;
            grdImplementationStatus.DataBind();
        }
        public void BindTEMPImplementationGridSaved()
        {
            int branchid = -1;
            string FinancialYear = string.Empty;
            string Formonth = string.Empty;
            branchid = Convert.ToInt32(ddlLocation.SelectedValue);
            FinancialYear = ddlFinancialYear.SelectedItem.Text;
            List<SP_TMP_ImplementationAssignment_Saved_Result> a = new List<SP_TMP_ImplementationAssignment_Saved_Result>();
            a = ProcessManagement.GetSP_TMP_ImplementationAssignment_Saved_ResultProcedure(branchid, FinancialYear).ToList();
            var remindersummary = a.OrderBy(entry => entry.ForPeriod).ToList();
            grdImplementationStatusSaved.DataSource = null;
            grdImplementationStatusSaved.DataBind();
            grdImplementationStatusSaved.DataSource = remindersummary;
            grdImplementationStatusSaved.DataBind();
        }
        protected void btnSaveNew_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlPeriod.SelectedValue == "Select Period")
                {
                    cvDuplicateEntry1.ErrorMessage = "Select Period";
                    return;
                }               
                foreach (GridViewRow g1 in grdImplementationStatusSave.Rows)
                {
                    string lblItemTemplateForPeriod = (g1.FindControl("lblItemTemplateForPeriod") as Label).Text;                   
                    string lblPerformerUserID = (g1.FindControl("lblPerformerUserID") as Label).Text;
                    string lblReviewerUserID = (g1.FindControl("lblReviewerUserID") as Label).Text;
                    string lblItemTemplateFinancialYear = (g1.FindControl("lblItemTemplateFinancialYear") as Label).Text;
                    
                    string AssignedTo = string.Empty;
                    if (ddlInternalExternal.SelectedItem.Text == "Internal")
                    {
                        AssignedTo = "I";
                    }
                    else
                    {
                        AssignedTo = "E";
                    }

                    AuditImplementationInstance riskinstance = new AuditImplementationInstance();
                    riskinstance.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                    riskinstance.IsDeleted = false;
                    riskinstance.CreatedOn = DateTime.Now;
                    riskinstance.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                    UserManagementRisk.AddDetailsAuditImplementationInstance(riskinstance);

                    List<AuditImplementationAssignment> Tempassignments = new List<AuditImplementationAssignment>();
                    if (lblPerformerUserID != null)
                    {
                        AuditImplementationAssignment TempAssP = new AuditImplementationAssignment();
                        TempAssP.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                        TempAssP.RoleID = RoleManagement.GetByCode("PERF").ID;
                        TempAssP.UserID = Convert.ToInt32(lblPerformerUserID);
                        TempAssP.IsActive = true;
                        TempAssP.CreatedOn = DateTime.Now;
                        TempAssP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                        TempAssP.ImplementationInstance = riskinstance.Id;
                        TempAssP.FinancialYear = lblItemTemplateFinancialYear.Trim();

                        Tempassignments.Add(TempAssP);
                    }
                    if (lblReviewerUserID != null)
                    {
                        AuditImplementationAssignment TempAssR = new AuditImplementationAssignment();
                        TempAssR.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                        TempAssR.RoleID = RoleManagement.GetByCode("RVW1").ID;
                        TempAssR.UserID = Convert.ToInt32(lblReviewerUserID);
                        TempAssR.IsActive = true;
                        TempAssR.CreatedOn = DateTime.Now;
                        TempAssR.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                        TempAssR.ImplementationInstance = riskinstance.Id;
                        TempAssR.FinancialYear = lblItemTemplateFinancialYear.Trim();
                        Tempassignments.Add(TempAssR);
                    }
                    if (Tempassignments.Count != 0)
                    {
                        UserManagementRisk.AddDetailsAuditImplementationAssignment(Tempassignments);
                    }
                    CreateScheduleOnImplementation(riskinstance.Id, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User, lblItemTemplateFinancialYear.Trim(), Convert.ToInt32(ddlLocation.SelectedValue), lblItemTemplateForPeriod);                   
                    TMP_ImplementationAssignment mstauditorusermaster = new TMP_ImplementationAssignment()
                    {
                        CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue),
                        FinancialYear = lblItemTemplateFinancialYear.Trim(),                     
                        ForPeriod = ddlPeriod.SelectedItem.Text,
                        Status = 1,
                    };
                    ProcessManagement.UpdateTMP_ImplementationAssignment(mstauditorusermaster);
                    BindTEMPImplementationGrid();
                    BindTEMPImplementationGridSaved();

                    ddlPeriod.SelectedIndex = 0;
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Save Sucessfully..";
                }              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdImplementationStatus_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("ADDNEW"))
            {
                try
                {
                    if (ddlPeriod.SelectedValue == "Select Period")
                    {
                        cvDuplicateEntry1.ErrorMessage = "Select Period";
                        return;
                    }
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    string lblItemTemplateForPeriod = (row.FindControl("lblItemTemplateForPeriod") as Label).Text;
                    string ddlPerformer = (row.FindControl("ddlPerformer") as DropDownList).SelectedItem.Value;
                    string ddlReviewer = (row.FindControl("ddlReviewer") as DropDownList).SelectedItem.Value;
                    string AssignedTo = string.Empty;
                    if (ddlInternalExternal.SelectedItem.Text == "Internal")
                    {
                        AssignedTo = "I";
                    }
                    else
                    {
                        AssignedTo = "E";
                    }
                    if (ddlPerformer != ddlReviewer)
                    {
                        bool CheckExists = ExistsImplementation(Convert.ToInt32(ddlPerformer), Convert.ToInt32(ddlReviewer), lblItemTemplateForPeriod, Convert.ToInt32(ddlLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text);
                        if (CheckExists == false)
                        {
                            bool checkexistsreviewer = CheckReviewerImplementationExists(Convert.ToInt32(ddlPerformer), lblItemTemplateForPeriod, Convert.ToInt32(ddlLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text);
                            if (checkexistsreviewer == false)
                            {
                                bool checkexistsperformer = CheckPerformerImplementationExists(Convert.ToInt32(ddlReviewer), lblItemTemplateForPeriod, Convert.ToInt32(ddlLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text);
                                if (checkexistsperformer == false)
                                {
                                    List<TMP_ImplementationAssignment> Tempassignments = new List<TMP_ImplementationAssignment>();
                                    if (ddlPerformer != null && ddlReviewer != null)
                                    {
                                        TMP_ImplementationAssignment TempAssP = new TMP_ImplementationAssignment();
                                        TempAssP.FinancialYear = Convert.ToString(ddlFinancialYear.SelectedItem.Text);
                                        TempAssP.PUserId = Convert.ToInt32(ddlPerformer);
                                        TempAssP.RUserId = Convert.ToInt32(ddlReviewer);
                                        TempAssP.Status = 0;
                                        TempAssP.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                                        TempAssP.ForPeriod = lblItemTemplateForPeriod;
                                        Tempassignments.Add(TempAssP);
                                    }

                                    if (Tempassignments.Count != 0)
                                    {
                                        UserManagementRisk.AddDetailsTMP_ImplementationAssignment(Tempassignments);
                                    }
                                    ddlPeriod.SelectedIndex = 0;
                                    BindTEMPImplementationGrid();
                                    grdImplementationStatusSaved.DataSource = null;
                                    grdImplementationStatusSaved.DataBind();

                                    Label4.Text = string.Empty;
                                    Label4.Text = "Save Sucessfully..";
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Save Sucessfully..";
                                }
                                else
                                {
                                    Label4.Text = string.Empty;
                                    Label4.Text = "Reviewer Should not be Performer..";
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Reviewer Should not be Performer..";
                                }
                            }
                            else
                            {
                                Label4.Text = string.Empty;
                                Label4.Text = "Performer Should not be Reviewer..";
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Performer Should not be Reviewer..";
                            }
                        }
                        else
                        {
                            Label4.Text = string.Empty;
                            Label4.Text = "Performer And Reviewer all ready Exists..";
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Performer And Reviewer all ready Exists..";
                        }
                    }
                    else
                    {
                        Label4.Text = string.Empty;
                        Label4.Text = "Performer And Reviewer Cannot be Same..";
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Performer And Reviewer Cannot be Same..";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                } 
            }
        }
        public static bool CheckPerformerImplementationExists(long userid, string ForPeriod, int CustomerBranchID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.TMP_ImplementationAssignment
                             where row.PUserId == userid && row.ForPeriod == ForPeriod
                             && row.CustomerBranchID == CustomerBranchID
                             && row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static bool CheckReviewerImplementationExists(long userid, string ForPeriod, int CustomerBranchID, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.TMP_ImplementationAssignment
                             where row.RUserId == userid && row.ForPeriod == ForPeriod
                             && row.CustomerBranchID == CustomerBranchID
                             && row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        protected void AddNew(object sender, EventArgs e)
        {
            try
            {
                if (ddlPeriod.SelectedValue == "Select Period")
                {
                    cvDuplicateEntry.ErrorMessage = "Select Period";
                    return;
                }               
                var closeLink = (Control)sender;
                GridViewRow row = (GridViewRow)closeLink.NamingContainer;
                string lblItemTemplateForPeriod = (row.FindControl("lblItemTemplateForPeriod") as Label).Text;
                string ddlPerformer = (row.FindControl("ddlPerformer") as DropDownList).SelectedItem.Value;
                string ddlReviewer = (row.FindControl("ddlReviewer") as DropDownList).SelectedItem.Value;
                string AssignedTo = string.Empty;
                if (ddlInternalExternal.SelectedItem.Text == "Internal")
                {
                    AssignedTo = "I";
                }
                else
                {
                    AssignedTo = "E";
                }
                if (ddlPerformer != ddlReviewer)
                {
                    bool CheckExists = ExistsImplementation(Convert.ToInt32(ddlPerformer), Convert.ToInt32(ddlReviewer), lblItemTemplateForPeriod, Convert.ToInt32(ddlLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text);
                    if (CheckExists == false)
                    {
                        bool checkexistsreviewer = CheckReviewerImplementationExists(Convert.ToInt32(ddlPerformer), lblItemTemplateForPeriod, Convert.ToInt32(ddlLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text);
                         if (checkexistsreviewer == false)
                         {
                             bool checkexistsperformer = CheckPerformerImplementationExists(Convert.ToInt32(ddlReviewer), lblItemTemplateForPeriod, Convert.ToInt32(ddlLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text);
                             if (checkexistsperformer == false)
                             {
                                 AuditImplementationInstance riskinstance = new AuditImplementationInstance();
                                 riskinstance.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                                 riskinstance.IsDeleted = false;
                                 riskinstance.CreatedOn = DateTime.Now;
                                 riskinstance.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                 UserManagementRisk.AddDetailsAuditImplementationInstance(riskinstance);

                                 List<AuditImplementationAssignment> Tempassignments = new List<AuditImplementationAssignment>();
                                 if (ddlPerformer != null)
                                 {
                                     AuditImplementationAssignment TempAssP = new AuditImplementationAssignment();
                                     TempAssP.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                                     TempAssP.RoleID = RoleManagement.GetByCode("PERF").ID;
                                     TempAssP.UserID = Convert.ToInt32(ddlPerformer);
                                     TempAssP.IsActive = true;
                                     TempAssP.CreatedOn = DateTime.Now;
                                     TempAssP.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                     TempAssP.ImplementationInstance = riskinstance.Id;
                                     TempAssP.FinancialYear = ddlFinancialYear.SelectedItem.Text;

                                     Tempassignments.Add(TempAssP);
                                 }
                                 if (ddlReviewer != null)
                                 {
                                     AuditImplementationAssignment TempAssR = new AuditImplementationAssignment();
                                     TempAssR.CustomerBranchID = Convert.ToInt32(ddlLocation.SelectedValue);
                                     TempAssR.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                     TempAssR.UserID = Convert.ToInt32(ddlReviewer);
                                     TempAssR.IsActive = true;
                                     TempAssR.CreatedOn = DateTime.Now;
                                     TempAssR.ForPeriod = Convert.ToString(lblItemTemplateForPeriod);
                                     TempAssR.ImplementationInstance = riskinstance.Id;
                                     TempAssR.FinancialYear = ddlFinancialYear.SelectedItem.Text;
                                     Tempassignments.Add(TempAssR);
                                 }
                                 if (Tempassignments.Count != 0)
                                 {
                                     UserManagementRisk.AddDetailsAuditImplementationAssignment(Tempassignments);
                                 }
                                 CreateScheduleOnImplementation(riskinstance.Id, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User, ddlFinancialYear.SelectedItem.Text, Convert.ToInt32(ddlLocation.SelectedValue), ddlPeriod.SelectedItem.Text);
                                 ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceDetailsDialog\").dialog('close')", true);
                                 ddlPeriod.SelectedIndex = 0;
                             }
                             else
                             {
                                 Label4.Text = string.Empty;
                                 Label4.Text = "Reviewer Should not be Performer..";
                                 cvDuplicateEntry.IsValid = false;
                                 cvDuplicateEntry.ErrorMessage = "Reviewer Should not be Performer..";
                             }
                        }
                        else
                        {
                            Label4.Text = string.Empty;
                            Label4.Text = "Performer Should not be Reviewer..";
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Performer Should not be Reviewer..";
                        }
                    }
                    else
                    {
                        Label4.Text = string.Empty;
                        Label4.Text = "Performer And Reviewer all ready Exists..";
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Performer And Reviewer all ready Exists..";
                    }
                }
                else
                {
                    Label4.Text = string.Empty;
                    Label4.Text = "Performer And Reviewer Cannot be Same..";
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Performer And Reviewer Cannot be Same..";
                }
            }
            catch (Exception ex)
            {
                Label4.Text = string.Empty;
                Label4.Text = "Server Error Occured. Please try again.";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdImplementationStatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {              
                if (ddlInternalExternal.SelectedItem.Text == "Internal")
                {
                    int customerID = -1;
                    //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                    if (ddlPerformer != null)
                    {

                        ddlPerformer.Items.Clear();
                        ddlPerformer.DataTextField = "Name";
                        ddlPerformer.DataValueField = "ID";
                        ddlPerformer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(Portal.Common.AuthenticationHelper.CustomerID, Convert.ToInt32(Portal.Common.AuthenticationHelper.ServiceProviderID));
                        ddlPerformer.DataBind();
                        ddlPerformer.Items.Insert(0, new ListItem(" Select Performer ", "-1"));
                    }
                    DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                    if (ddlReviewer != null)
                    {

                        ddlReviewer.Items.Clear();
                        ddlReviewer.DataTextField = "Name";
                        ddlReviewer.DataValueField = "ID";
                        ddlReviewer.DataSource = RiskCategoryManagement.FillAuditKickOffUsers(Portal.Common.AuthenticationHelper.CustomerID, Convert.ToInt32(Portal.Common.AuthenticationHelper.ServiceProviderID));
                        ddlReviewer.DataBind();
                        ddlReviewer.Items.Insert(0, new ListItem(" Select Reviewer ", "-1"));
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlAuditor.SelectedValue) != -1)
                    {
                        int customerID = -1;
                        //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        DropDownList ddlPerformer = (e.Row.FindControl("ddlPerformer") as DropDownList);
                        if (ddlPerformer != null)
                        {
                            ddlPerformer.Items.Clear();
                            ddlPerformer.DataTextField = "Name";
                            ddlPerformer.DataValueField = "ID";
                            ddlPerformer.DataSource = RiskCategoryManagement.FillAuditusers(customerID, Convert.ToInt32(ddlAuditor.SelectedValue));
                            ddlPerformer.DataBind();
                            ddlPerformer.Items.Insert(0, new ListItem(" Select Performer ", "-1"));
                        }
                        DropDownList ddlReviewer = (e.Row.FindControl("ddlReviewer") as DropDownList);
                        if (ddlReviewer != null)
                        {
                            ddlReviewer.Items.Clear();
                            ddlReviewer.DataTextField = "Name";
                            ddlReviewer.DataValueField = "ID";
                            ddlReviewer.DataSource = RiskCategoryManagement.FillAuditusers(customerID, Convert.ToInt32(ddlAuditor.SelectedValue));
                            ddlReviewer.DataBind();
                            ddlReviewer.Items.Insert(0, new ListItem(" Select Reviewer ", "-1"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdImplementationStatus_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        protected void ddlFinancialYearImplementation_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        public void BindFinancialYearIMPFilter()
        {
            ddlFinancialYearImplementation.DataTextField = "Name";
            ddlFinancialYearImplementation.DataValueField = "ID";
            ddlFinancialYearImplementation.Items.Clear();
            ddlFinancialYearImplementation.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYearImplementation.DataBind();
            ddlFinancialYearImplementation.Items.Insert(0, new ListItem(" All ", "-1"));
        }
        public string ShowIMPlementationPerformerName(string Period, int branchid, int Userid, string FinancialYear)
        {
            string performername = string.Empty;
            string AssignedTo = string.Empty;
            performername = GetPerformerName(branchid, FinancialYear, Userid, Period);             
            return performername;
        }

        public static string  GetReviewerName(int branchid, string FinancialYear,int Userid,string Period)
        {
            string reviewername = string.Empty; 
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var a = (from row in entities.TMP_ImplementationAssignment
                         join row1 in entities.mst_User
                         on row.RUserId equals row1.ID
                         where row.ForPeriod == Period && row.CustomerBranchID == branchid
                                      && row.RUserId == Userid && row.FinancialYear == FinancialYear
                         select new
                         {
                             Name = row1.FirstName + " " + row1.LastName
                         }).FirstOrDefault();
                if (a != null)
                {
                    reviewername = a.Name;
                }
                return reviewername;               
            }
        }
        public static string GetPerformerName(int branchid, string FinancialYear, int Userid, string Period)
        {
            string performername = string.Empty;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var a = (from row in entities.TMP_ImplementationAssignment
                         join row1 in entities.mst_User
                         on row.PUserId equals row1.ID
                         where row.ForPeriod == Period && row.CustomerBranchID == branchid
                                      && row.PUserId == Userid && row.FinancialYear == FinancialYear
                         select new
                         {
                             Name = row1.FirstName + " " + row1.LastName
                         }).FirstOrDefault();
                if (a!=null)
                {
                    performername = a.Name;
                }
                return performername;
               
            }
        }
        public string ShowIMPlementationReviewerName(string Period, int branchid, int Userid, string FinancialYear)
        {
            string reviewername = string.Empty;
            string AssignedTo = string.Empty;
            reviewername = GetReviewerName(branchid, FinancialYear, Userid, Period);                    
            return reviewername;
        }
        #endregion

        protected void liAudtdetl_Click(object sender, EventArgs e)
        {
            try
            {
                BindData(-1, -1, "");
                dvAuditDetails.Visible = true;
                dvProcessDetails.Visible = false;
                dvImplstatus.Visible = false;

                liAudtdetl.Attributes.Add("class", "active");
                liprocess.Attributes.Add("class", "");
                liimplSts.Attributes.Add("class", "");




                dvAuditDetails.Attributes.Remove("class");
                dvAuditDetails.Attributes.Add("class", "tab-pane active");

                dvProcessDetails.Attributes.Remove("class");
                dvProcessDetails.Attributes.Add("class", "tab-pane");

                dvImplstatus.Attributes.Remove("class");
                dvImplstatus.Attributes.Add("class", "tab-pane");

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void liprocess_Click(object sender, EventArgs e)
        {
            try
            {
                BindData(-1, -1, "");
                dvAuditDetails.Visible = false;
                dvProcessDetails.Visible = true;
                dvImplstatus.Visible = false;

                liAudtdetl.Attributes.Add("class", "");
                liprocess.Attributes.Add("class", "active");
                liimplSts.Attributes.Add("class", "");




                dvAuditDetails.Attributes.Remove("class");
                dvAuditDetails.Attributes.Add("class", "tab-pane");

                dvProcessDetails.Attributes.Remove("class");
                dvProcessDetails.Attributes.Add("class", "tab-pane active");

                dvImplstatus.Attributes.Remove("class");
                dvImplstatus.Attributes.Add("class", "tab-pane");

                ddlInternalExternal.Enabled = false;
                ddlFinancialYear.Enabled = false;
                ddlAuditor.Enabled = false;
                ddlSchedulingType.Enabled = false;
                ddlPeriod.Enabled = false;


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void liimplSts_Click(object sender, EventArgs e)
        {
            try
            {
                BindData(-1, -1, "");
                dvAuditDetails.Visible = false;
                dvProcessDetails.Visible = false;
                dvImplstatus.Visible = true;

                liAudtdetl.Attributes.Add("class", "");
                liprocess.Attributes.Add("class", "");
                liimplSts.Attributes.Add("class", "active");


                dvAuditDetails.Attributes.Remove("class");
                dvAuditDetails.Attributes.Add("class", "tab-pane ");

                dvProcessDetails.Attributes.Remove("class");
                dvProcessDetails.Attributes.Add("class", "tab-pane");

                dvImplstatus.Attributes.Remove("class");
                dvImplstatus.Attributes.Add("class", "tab-pane active");


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        

        /// <summary>
        /// For Grid View Page Number Binding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    grdCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                //BindProcessSubType();
                BindData(-1, -1, "");
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

       protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
       {
           try
           {
               //if (!IsValid()) { return; };

               if (Convert.ToInt32(SelectedPageNo.Text) > 1)
               {
                   SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                   grdCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                   grdCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
               }
               else
               {

               }
               //Reload the Grid
               //BindProcessSubType();
               BindData(-1, -1, "");
           }
           catch (Exception ex)
           {
               //ShowGridViewPagingErrorMessage(ex.Message.ToString());
           }
       }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
       {
           try
           {
               //if (!IsValid()) { return; };

               int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

               if (currentPageNo < GetTotalPagesCount())
               {
                   SelectedPageNo.Text = (currentPageNo + 1).ToString();
                   grdCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                   grdCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
               }
               else
               {
               }
               //Reload the Grid
               //BindProcessSubType();
               BindData(-1, -1, "");
           }
           catch (Exception ex)
           {
               //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
           }
       }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        protected void lbtEdit_Click(object sender, EventArgs e)
        {
            try
            {
                BindData(-1, -1, "");
                dvAuditDetails.Visible = false;
                dvProcessDetails.Visible = true;
                dvImplstatus.Visible = false;

                liAudtdetl.Attributes.Add("class", "");
                liprocess.Attributes.Add("class", "active");
                liimplSts.Attributes.Add("class", "");
                
                dvAuditDetails.Attributes.Remove("class");
                dvAuditDetails.Attributes.Add("class", "tab-pane");

                dvProcessDetails.Attributes.Remove("class");
                dvProcessDetails.Attributes.Add("class", "tab-pane active");

                dvImplstatus.Attributes.Remove("class");
                dvImplstatus.Attributes.Add("class", "tab-pane");

                ddlInternalExternal.Enabled = false;
                ddlFinancialYear.Enabled = false;
                ddlAuditor.Enabled = false;
                ddlSchedulingType.Enabled = false;
                ddlPeriod.Enabled = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}